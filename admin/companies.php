<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SETUPCOMPANY';
$path_to_root = "..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Sub Company Setup"));

include_once($path_to_root . "/gl/includes/siap.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/admin/db/company_db.inc");
//-------------------------------------------------------------------------------------------------

if (isset($_POST['update']) && $_POST['update'] != "")
{
	$input_error = 0;
	
	if (strlen($_POST['coy_name'])==0)
	{
		$input_error = 1;
		display_error(_("The company name must be entered."));
		set_focus('coy_name');
	}
	if (isset($_FILES['pic']) && $_FILES['pic']['name'] != '')
	{
		if ($_FILES['pic']['error'] == UPLOAD_ERR_INI_SIZE) {
				display_error(_('The file size is over the maximum allowed.'));
				$input_error = 1;
		}
		elseif ($_FILES['pic']['error'] > 0) {
				display_error(_('Error uploading logo file.'));
				$input_error = 1;
		}
		$result = $_FILES['pic']['error'];
		$filename = $path_to_root."/images";
		if (!file_exists($filename))
		{
			mkdir($filename);
		}
		$filename .= "/".clean_file_name($_FILES['pic']['name']);

		 //But check for the worst
		if (!in_array( substr($filename,-4), array('.jpg','.JPG','.png','.PNG')))
		{
			display_error(_('Only jpg and png files are supported - a file extension of .jpg or .png is expected'));
			$input_error = 1;
		}
		elseif ( $_FILES['pic']['size'] > ($SysPrefs->max_image_size * 1024))
		{ //File Size Check
			display_error(_('The file size is over the maximum allowed. The maximum size allowed in KB is') . ' ' . $SysPrefs->max_image_size);
			$input_error = 1;
		}
		elseif ( $_FILES['pic']['type'] == "text/plain" )
		{  //File type Check
			display_error( _('Only graphics files can be uploaded'));
			$input_error = 1;
		}
		elseif (file_exists($filename))
		{
			$result = unlink($filename);
			if (!$result)
			{
				display_error(_('The existing image could not be removed'));
				$input_error = 1;
			}
		}

		if ($input_error != 1)
		{
			$result  =  move_uploaded_file($_FILES['pic']['tmp_name'], $filename);
			$_POST['coy_logo'] = clean_file_name($_FILES['pic']['name']);
			
			if(!$result) 
				display_error(_('Error uploading logo file'));
		}
	}
	if (check_value('del_coy_logo'))
	{
		$filename = $path_to_root."/images/".clean_file_name($_POST['coy_logo']);
		if (file_exists($filename))
		{
			$result = unlink($filename);
			if (!$result)
			{
				display_error(_('The existing image could not be removed'));
				$input_error = 1;
			}
		}
		$_POST['coy_logo'] = "";
	}
	if ($_POST['add_pct'] == "")
		$_POST['add_pct'] = -1;
	if ($_POST['round_to'] <= 0)
		$_POST['round_to'] = 1;
	if ($input_error != 1)
	{
		/*update_company_prefs(
			get_post( array('coy_name','coy_no','gst_no','tax_prd','tax_last',
				'postal_address','phone', 'fax', 'email', 'coy_logo', 'domicile',
				'use_dimension', 'curr_default', 'f_year', 
				'no_item_list' => 0, 'no_customer_list' => 0, 
				'no_supplier_list' =>0, 'base_sales', 
				'time_zone' => 0, 'add_pct', 'round_to', 'login_tout', 'auto_curr_reval',
				'bcc_email', 'alternative_tax_include_on_docs', 'suppress_tax_rates',
				'use_manufacturing', 'use_fixed_assets'))
		);*/
		$sql="INSERT INTO `0_companies` (`id`, `nama`, `lvl`, `parent`, `kode`, `logo`) 
		VALUES (NULL, '".$_POST['coy_name']."', '".$_POST['level']."', '".$_POST['parent']."', '".$_POST['kode']."', '".$_POST['coy_logo']."')";
		db_query($sql);
		display_notification_centered(_("Company setup has been created."));
	}
	set_focus('coy_name');
	$Ajax->activate('_page_body');
} /* end of if submit */

echo '<script src="../jquery2.min.js"></script>';

start_form();
/*if (isset($_POST['Edit'])) {
	$sql = "SELECT a.*,b.nama as parentname FROM 0_companies a left join 0_companies b on a.parent=b.".$_POST['id']." ;";
	$result=db_query($sql);
	while ($myrow = db_fetch($result)) 
	{
		$_POST['id'] = $myrow["id"];
		$_POST['coy_name'] = $myrow["nama"];
		$_POST['kode'] = $myrow["kode"];
		$_POST['level'] = $myrow["lvl"];
		//hidden('id',$myrow["id"]);
	}
}*/
$myrow = get_company_prefs();
$com0=$myrow["coy_name"];
start_outer_table(TABLESTYLE2);


table_section(1);
table_section_title(_("Company Information"));
text_row_ex(_("Name:"), 'coy_name', 50, 50);
text_row_ex(_("Kode:"), 'kode', 20, 20);
echo '<style>
select{
	max-width:100% !important;
}
</style>
<script>
	function parentlvl(lvl){
		var myDiv = document.getElementById("selectparent");
		myDiv.innerHTML="";
		if(lvl>1){
			//Create and append select list

			//Create array of options to be added
			var array = ["Pilih Parent Company"];

			//Create and append select list
			var selectList = document.createElement("select");
			selectList.id = "parent";
			selectList.name = "parent";
			myDiv.appendChild(selectList);
			
			$.post("companies_ajax.php",
			{
				lvl: lvl
			},
			function(data){
				console.log(data);
				var option = document.createElement("option");
				option.value = array[0];
				option.text = array[0];
				selectList.appendChild(option);
				for (var i = 0, len = data.length; i < len; i++) {
					
					var option = document.createElement("option");
					option.value = data[i][0];
					option.text = data[i][1];
					selectList.appendChild(option);
				}
			});
			
		}else{
			var selectList = document.createElement("select");
			selectList.id = "parent";
			selectList.name = "parent";
			myDiv.appendChild(selectList);
			var option = document.createElement("option");
			option.value = 0;
			option.text = "'.$myrow["coy_name"].'";
			selectList.appendChild(option);
			
		}
	}
</script>';
echo '<tr>
		<td class="label">Access Level:</td>
		<td>
			<span id="">
				<select id="level" name="level" style="max-width:100% !important;" onchange="parentlvl(this.value)">
					<option>Pilih Level Company</option>';
					/*$getmaxlvl = "SELECT DISTINCT lvl FROM 0_companies";
					if($resultlvl = db_query($getmaxlvl)){
						while ($row = db_fetch($resultlvl)) {
							echo '<option value="'.$row['lvl'].'">'.$row['lvl'].'</option>';
							$lvlmax = $row['lvl'] + 1;
						}
						echo '<option value="'.$lvlmax.'">New Level Company</option>';
						//$newlvl = $lvlmax + 1;
						//echo '<option value="'.$newlvl.'">New Level</option>';
					}*/
					for ($a=1; $a <= 5; $a++) { 
						echo '<option value="'.$a.'">'.$a.'</option>';
					}
echo '				</select>
			</span>
		</td>
	</tr>';
echo '<tr>
		<td class="label">Parent Company:</td>
		<td>
			<span id="selectparent"></span>
		</td>
	</tr>';
/*echo '<tr>
		<td class="label">Company Code SIAP :</td>
		<td>
			<span id="">
				<select id="kode_siap" name="kode_siap" style="max-width:100% !important;">
					<option>Pilih Company Code SIAP</option>';
					$getmaxlvl = "SELECT DISTINCT lvl FROM 0_companies";
					if($resultlvl = db_query($getmaxlvl)){
						while ($row = db_fetch($resultlvl)) {
							echo '<option value="'.$row['lvl'].'">'.$row['lvl'].'</option>';
							$lvlmax = $row['lvl'] + 1;
						}
						echo '<option value="'.$lvlmax.'">New Level Company</option>';
						//$newlvl = $lvlmax + 1;
						//echo '<option value="'.$newlvl.'">New Level</option>';
					}
					$row = get_comp_code();
					$count = count($row);
					for ($a=0; $a < $count; $a++) { 
						echo '<option value="'.$row[$a]['pfoliocode'].'">'.$row[$a]['pfoliocode'].' - '.$row[$a]['pfoliofname'].'</option>';
					}*/
					
					/*for ($a=1; $a <= 5; $a++) { 
						echo '<option value="'.$a.'">'.$a.'</option>';
					}*/
file_row(_("Company Logo (.jpg)") . ":", 'pic', 'pic');
/*
textarea_row(_("Address:"), 'postal_address', $_POST['postal_address'], 34, 5);

text_row_ex(_("Phone Number:"), 'phone', 25, 55);
text_row_ex(_("Fax Number:"), 'fax', 25);
email_row_ex(_("Email Address:"), 'email', 50, 55);

text_row_ex(_("Official Company Number:"), 'coy_no', 25);
text_row_ex(_("GSTNo:"), 'gst_no', 25);

//label_row(_("Company Logo:"), $_POST['coy_logo']);
file_row(_("Company Logo (.jpg)") . ":", 'pic', 'pic');
//check_row(_("Delete Company Logo:"), 'del_coy_logo', $_POST['del_coy_logo']);
*/
end_outer_table(1);

hidden('coy_logo', @$_POST['coy_logo']);
submit_center('update', _("Update"), true, '',  'default');

echo '<br><br>';
/*end_form(1);

start_form();*/
start_table(TABLESTYLE);

$th = array(_("Company"), _("Code"), _("Parent Company"));
//,"",""
inactive_control_column($th);
table_header($th);	

$k = 0; //row colour counter

$sql = "SELECT a.*,b.nama as parentname FROM 0_companies a left join 0_companies b on a.parent=b.id ORDER BY a.kode;";
$result=db_query($sql);
while ($myrow = db_fetch($result)) 
{

	alt_table_row_color($k);
	
	
	label_cell($myrow["nama"]);
	label_cell($myrow["kode"]);
	label_cell((@$myrow["parentname"]!=''?$myrow["parentname"]:$com0));
	/*hidden('id',$myrow["id"]);
	edit_button_cell("Edit".$myrow["id"], _("Edit"));*/
 	//delete_button_cell("Delete".$myrow["id"], _("Delete"));
    /*if ($not_me)
	else
		label_cell('');*/
	
	end_row();

} //END WHILE LIST LOOP

inactive_control_row($th);
end_table(1);
end_form(2);
//-------------------------------------------------------------------------------------------------

end_page();

