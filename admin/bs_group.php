<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_REPORTBUILDER';
$path_to_root = "..";
include_once($path_to_root . "/includes/session.inc");

page(_($help_context = "Balance Sheet Report Account Setting"));

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/admin/db/group_report_db.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");


simple_page_mode(true);

if (get_post('Show')) {
	if ($_POST['company_show'] == "" )
	{
		display_error( _("Company Cannot be empty."));
		return false;
	}else if ($_POST['group_show'] == "" )
	{
		display_error( _("Subgroup Cannot be empty."));
		return false;
	}else{
		$Ajax->activate('display_bs');
	}
	//return true;
}
//-------------------------------------------------------------------------------------------------
function can_process($new) 
{
	if ($_POST['subgroup'] < 0 )
	{
		display_error( _("Class id P&L Cannot be empty."));
		return false;
	}

	if (count(@$_POST['coa']) == 0 )
	{
		display_error( _("Account Cannot be empty."));
		return false;
	}

	if ($_POST['company'] == "" )
	{
		display_error( _("Company Cannot be empty."));
		return false;
	}

	/*if (strlen($_POST['class']) > 32 )
	{
		display_error( _("keycode entered max 32 characters long."));
		return false;
	}else if (strlen($_POST['keycode']) < 1 ) {
		display_error( _("keycode cannot be empty"));
		return false;
	}

	if (strlen($_POST['label']) > 32 )
	{
		display_error( _("label entered max 32 characters long."));
		return false;
	}else if (strlen($_POST['label']) < 1 ) {
		display_error( _("label cannot be empty"));
		return false;
	}

	if (strlen($_POST['value']) > 32 )
	{
		display_error( _("value entered max 32 characters long."));
		return false;
	}else if (strlen($_POST['value']) < 1 ) {
		display_error( _("value cannot be empty"));
		return false;
	}*/


	return true;
}

//-------------------------------------------------------------------------------------------------

if (($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM'))
{

	if (can_process($Mode == 'ADD_ITEM'))
	{
    	if ($selected_id != -1) 
    	{
    		update_param($selected_id,$_POST['class'], $_POST['company'],$_POST['report']);

    		display_notification_centered(_("The selected parameter has been updated."));
    	} 
    	else 
    	{
    		$coa = $_POST['coa'];
    		$countcoa = count($_POST['coa']);
    		for ($i=0; $i < $countcoa; $i++) { 
    			add_group_coa($coa[$i], $_POST['subgroup'], $_POST['company']);
    		}

//			$id = db_insert_id();
			
			display_notification_centered(_("A new parameter has been added."));
		}
		$Mode = 'RESET';
	}
	
}

//-------------------------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
		

    	delete_group_coa($selected_id);
    	$Mode = 'RESET';
}

//-------------------------------------------------------------------------------------------------
if ($Mode == 'RESET')
{
 	$selected_id = -1;
	$sav = get_post('show_inactive', null);
	unset($_POST);	// clean all input fields
	$_POST['show_inactive'] = $sav;
	$Ajax->activate('display_bs');
}
//-------------------------------------------------------------------------------------------------
//$report = 2;
//$class = array('', 'Pendapatan Utama', 'Beban Usaha', 'Pendapatan (Beban) Lain-Lain');
//$classidhead = 1;
function control_show(){
	start_table(TABLESTYLE2);
	echo '<tr>
			<td class="label">Company :</td>
			<td>
				<span id="">
					<select id="company_show" name="company_show" style="min-width:200px !important;" onload="process()">
						<option value="" selected hidden>Select Company</option>';
						$sql = "select  id,nama
								from    (select * from ".TB_PREF."companies
								         order by parent, id) comps_sorted,
								        (select @pv := ".$_SESSION['wa_current_user']->com_id.") initialisation
								where   (CASE WHEN NOT EXISTS (SELECT id from ".TB_PREF."companies where parent = @pv) THEN
										find_in_set(id, @pv) ELSE find_in_set(parent, @pv) END)
								and 	length(@pv := concat(@pv, ',', id))
								group by parent";
						$result = db_query($sql);
						while ($row = db_fetch($result)) {
							echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
						}
						/*for ($a=1; $a <= 3; $a++) { 
							echo '<option value="'.$a.'">'.$class[$a].'</option>';
						}*/
	echo '			</select>
				</span>
			</td>
		</tr>';

	echo '<tr>
			<td class="label">Group :</td>
			<td>
				<span id="">
					<select id="group_show" name="group_show" style="min-width:200px !important;">
						<option value="" selected hidden>Select Group</option>';
	echo '			</select>
				</span>
			</td>
		</tr>';
	
	echo '<tr style="height: 40px !important;">';
	echo '<td colspan=9 align=center><button class="ajaxsubmit" type="submit" aspect="default" name="Show" style="margin-right: 15px !important;" id="Show" value="Show"><img src="../themes/default/images/ok.gif" height="12" alt=""><span>Show</span></button></td>';
	//submit_cells('Show',_("Show"),'','', 'default');
	echo "</tr>";
	end_table();
}

function control_input($selected_id,$Mode){
	start_table(TABLESTYLE2);
	if ($selected_id != -1) 
	{
	  	if ($Mode == 'Edit') {
			//editing an existing parameter
			$myrow = get_param_edit($selected_id);

			$_POST['keycode'] = $myrow["keycode"];
			$_POST['label'] = $myrow["label"];
			$_POST['value'] = $myrow["value"];
		}
		hidden('selected_id', $selected_id);

		start_row();
	} 
	//$accounts = get_chart_accounts_search('');
	//$report = 2;
	echo '<tr>
			<td class="label">Company :</td>
			<td>
				<span>
					<select id="company" name="company" style="min-width:200px !important;" required>
						<option value="" selected hidden>Select Company</option>';
						$sql = "select  id,nama
								from    (select * from ".TB_PREF."companies
								         order by parent, id) comps_sorted,
								        (select @pv := ".$_SESSION['wa_current_user']->com_id.") initialisation
								where   (CASE WHEN NOT EXISTS (SELECT id from ".TB_PREF."companies where parent = @pv) THEN
										find_in_set(id, @pv) ELSE find_in_set(parent, @pv) END)
								and 	length(@pv := concat(@pv, ',', id))
								group by parent";
						$result = db_query($sql);
						while ($row = db_fetch($result)) {
							echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
						}
	echo '				</select>
				</span>
			</td>
		</tr>';
	echo '<tr>
			<td class="label">Sub Group P&L :</td>
			<td>
				<span id="">
					<select id="subgroup" name="subgroup" style="min-width:200px !important;" required>
						<option value="" selected hidden>Select Sub Group</option>';

	echo '				</select>
				</span>
			</td>
		</tr>';
	echo '<tr>
			<td class="label">Account :</td>
			<td>
				<span id="">
					<select class="js-example-basic-multiple" id="coa" name="coa[]" multiple="multiple" style="width:500px !important;" required>';
	echo '				</select>
				</span>
			</td>
		</tr>';

	/*
	echo '<select  name="states[]" multiple="multiple">
	  <option value="AL">Alabama</option>
	  <option value="WY">Wyoming</option>
	</select>';*/
	echo "<tr>";
	if ($selected_id != -1) {
		echo "<td colspan=3 align=center>";
		echo '<button class="" type="submit" aspect="default" name="UPDATE_ITEM" id="UPDATE_ITEM" value="Update" title="Submit changes"><img src="../themes/default/images/ok.gif" height="12" alt=""><span>Update</span></button>
			<button class="" type="submit" aspect="cancel" name="RESET" id="RESET" value="Cancel" title="Cancel edition"><img src="../themes/default/images/escape.png" height="12" alt=""><span>Cancel</span></button>';
		echo "</td>";
	}else{
		echo "<td colspan=9 align=center>";
		echo '<button type="submit" aspect="default" name="ADD_ITEM" style="margin-right: 15px !important;" id="ADD_ITEM" value="ADD_ITEM"><img src="../themes/default/images/ok.gif" height="12" alt=""><span>Add Item</span></button>';
		echo "</td>";
	}
	echo "</tr>";
	end_table();
	echo '<br>';
	/*submit_add_or_update_center($selected_id == -1, '', 'both');*/
}

function display_bs($company, $subgroup){

	$result = get_group_data($company,$subgroup,true,1);

	while ($subgroup = db_fetch($result)) {

		start_table(TABLESTYLE);
		echo "<tr style='margin-top:10px;'>";
		echo "<td class='tableheader' colspan=3>".$subgroup['groupname']."</td>";
		echo "</tr>";

		$th = array(_("Account"),_("Company"), "");

		inactive_control_column($th);
		table_header($th);	

		$k = 0; //row colour counter
		$result2 = get_report_coa($subgroup['id'], $company);
		while ($coa = db_fetch($result2)) 
		{

			alt_table_row_color($k);

			label_cell($coa["account"]);
			label_cell($coa["nama"]);

			//edit_button_cell("Edit".$myrow["id"], _("Edit"));
		    //if ($myrow["id"] != 1)
		 	//delete_button_cell("Delete".$coa["id"], _("Delete"));
			echo "<td align=center>";
			echo '<button type="submit" name="Delete'.$coa["id"].'" value="1" title="Delete"><img src="../themes/default/images/delete.gif" style="vertical-align:middle;width:12px;height:12px;border:0;">
					</button>';
			echo "</td>";
			hidden('company_id', $company);
			//else
			//label_cell('');
			end_row();

		} //END WHILE LIST LOOP
	//$classid+=1;
	end_table();
	echo '<br>';
	}
}

start_form();
control_input($selected_id,$Mode);
end_form();

echo '<br>';
echo "<hr>";
echo '<br>';

start_form();
control_show();
end_form();

echo "<br>";

div_start('display_bs');
if (get_post('Show')) {
	start_form();
	display_bs($_POST['company_show'],$_POST['group_show']);
	end_form();
}
div_end();

echo '<link href="../select2/select2.min.css" rel="stylesheet" />
<script src="../themes/dashboard/js/jquery-1.3.2.js"></script>
<script src="../select2/select2.full.js"></script>
<script type="text/javascript">

function proses(){

    $(\'#subgroup\').change(function(){
    	$( "#coa" ).empty();
    	var subgroupid =  $(\'#subgroup\').val();
    	$.ajax({
		  url: "report_group_ajax.php",
		  data: { subgroupid : subgroupid},
		  dataType: "json",
		  type: "POST",
		  context: document.body
		}).done(function(data) {
		  	$.each(data, function(index, val) {
			    $(\'#coa\').append(\'<option value="\'+val.account_code+\'">\'+ val.label +\'</option>\');
			    console.log(val.label);
			});
		});
		//return true;
    });
}

function getsubgroup(){

	$(\'#company_show\').on("change",function(){
    	$( "#group_show" ).empty();
	 	$(\'#group_show\').append(\'<option value="" selected hidden>Select Group</option>\');
	 	$(\'#group_show\').append(\'<option value="0">All</option>\');
    	var compid = $(\'#company_show\').val();
    	console.log(compid);
    	$.ajax({
		  url: "get_subgroup_ajax.php",
		  data: { compid : compid, report : 1, setaccount : 1},
		  dataType: "json",
		  type: "POST",
		  context: document.body
		}).done(function(data) {
			console.log(data);
		  	$.each(data, function(index, val) {
			    $(\'#group_show\').append(\'<option value="\'+val.id+\'">\'+ val.subgroup_name +\'</option>\');
			});
		});
    });	

	$(\'#company\').on("change",function(){
		$( "#subgroup" ).empty();
	 	$(\'#subgroup\').append(\'<option value="" hidden>Select Sub Group</option>\');
		var compid = $(\'#company\').val();
		console.log(compid);
		$.ajax({
		  url: "get_subgroup_ajax.php",
		  data: { compid : compid, report : 1, setaccount : 1},
		  dataType: "json",
		  type: "POST",
		  context: document.body
		}).done(function(data) {
			console.log(data);
		  	$.each(data, function(index, val) {
			    $(\'#subgroup\').append(\'<option value="\'+val.id+\'">\'+ val.subgroup_name +\'</option>\');
			});
		});
	});
}


$(document).ready(function(){
	$(\'.js-example-basic-multiple\').select2({
		closeOnSelect: false
	});
	proses()
	getsubgroup();	
});

</script>';


end_page();

