<?php
$page_security = 'SA_DIMTRANSVIEW';
$path_to_root = "..";
include($path_to_root . "/includes/session.inc");

/*$_POST['compid'] = '11';
//$_POST['status'] = 'show';
$_POST['report'] = 1;
$_POST['setaccount'] = 1;*/
if (isset($_POST['compid'])) {
	if (isset($_POST['status'])) {
		$subgroup = getsubgroup($_POST['compid'],$_POST['status'],null,null);
	}else{
		$subgroup = getsubgroup($_POST['compid'],null,@$_POST['report'],@$_POST['setaccount']);
	}
	echo json_encode($subgroup);
}

if (isset($_POST['groupid'])) {
	$class  = getsubgroupclass($_POST['groupid'],$_POST['companyid']);
	echo json_encode($class);
}

function getsubgroupclass($groupid,$companyid){
	if ($groupid != 0) {
		$sql = "SELECT cc.cid,cc.class_name from ".TB_PREF."report_group rg 
				INNER JOIN ".TB_PREF."chart_class cc ON cc.cid = rg.class_id
				WHERE rg.company_id=".$companyid."
				AND rg.id=".$groupid;
	}else{
		$sql = "SELECT cid,class_name from ".TB_PREF."chart_class";
	}

	if($result=db_query($sql)){
		while($row=db_fetch($result)){
			$id = $row['cid'];
			$classname = $row['class_name'];
			$class[]= array('cid'=>$id, 'classname'=>$classname);
		}
	}
	return $class;
}
function getsubgroup($companyid,$status = null,$report = null,$getaccount = null){


	$sql = "SELECT rg1.id,rg1.name,rg1.class_id FROM ".TB_PREF."report_group rg1";

	if ($getaccount) {
		$sql .= " INNER JOIN ".TB_PREF."report_group as rg2 ON rg2.id = rg1.parent";
	}

	$sql .= " WHERE rg1.company_id =".$companyid." AND rg1.inactive = 0";
	
	if ($status == "show") {
		$sql.= " AND rg1.parent = 0";	
	}

	if ($report != null) {
		if($report == 1){
			$sql .= " AND rg1.class_id IN (1,2,3)";		
		}else{
			$sql .= " AND rg1.class_id IN (4,6,10)";
		}
	}

	$sql.= " ORDER BY rg1.parent,rg1.id";

	if($result=db_query($sql)){
		while($row=db_fetch($result)){
			$id = $row['id'];
			$subgroup_name = $row['name'];
			$class_id = $row['class_id'];
			$subgroup[]= array('id'=>$id, 'subgroup_name'=>$subgroup_name, 'class_id'=>$class_id);
		}
	}
	return $subgroup;
}


?>