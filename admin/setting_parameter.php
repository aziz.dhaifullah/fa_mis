<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_FORMSETUP';
$path_to_root = "..";
include_once($path_to_root . "/includes/session.inc");

page(_($help_context = "Setting Parameter"));

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/admin/db/param_db.inc");

simple_page_mode(true);
//-------------------------------------------------------------------------------------------------

function can_process($new) 
{

	if (strlen($_POST['keycode']) > 32 )
	{
		display_error( _("keycode entered max 32 characters long."));
		return false;
	}else if (strlen($_POST['keycode']) < 1 ) {
		display_error( _("keycode cannot be empty"));
		return false;
	}

	if (strlen($_POST['label']) > 32 )
	{
		display_error( _("label entered max 32 characters long."));
		return false;
	}else if (strlen($_POST['label']) < 1 ) {
		display_error( _("label cannot be empty"));
		return false;
	}

	if (strlen($_POST['value']) > 32 )
	{
		display_error( _("value entered max 32 characters long."));
		return false;
	}else if (strlen($_POST['value']) < 1 ) {
		display_error( _("value cannot be empty"));
		return false;
	}


	return true;
}

//-------------------------------------------------------------------------------------------------

if (($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') && check_csrf_token())
{

	if (can_process($Mode == 'ADD_ITEM'))
	{
    	if ($selected_id != -1) 
    	{
    		update_param($selected_id,$_POST['keycode'], $_POST['label'],$_POST['value']);

    		display_notification_centered(_("The selected parameter has been updated."));
    	} 
    	else 
    	{
    		add_param($_POST['keycode'], $_POST['label'],$_POST['value']);
//			$id = db_insert_id();
			
			display_notification_centered(_("A new parameter has been added."));
    	}
		$Mode = 'RESET';
	}
}

//-------------------------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
	
    	delete_param($selected_id);
    	$Mode = 'RESET';
}

//-------------------------------------------------------------------------------------------------
if ($Mode == 'RESET')
{
 	$selected_id = -1;
	$sav = get_post('show_inactive', null);
	unset($_POST);	// clean all input fields
	$_POST['show_inactive'] = $sav;
}

$result = get_param();
start_form();
start_table(TABLESTYLE);


$th = array(_("Keycode"), _("Label"), _("Value"), "", "");

inactive_control_column($th);
table_header($th);	

$k = 0; //row colour counter

while ($myrow = db_fetch($result)) 
{

	alt_table_row_color($k);

	
	label_cell($myrow["keycode"]);
	label_cell($myrow["label"]);
	label_cell($myrow["value"]);

	edit_button_cell("Edit".$myrow["id"], _("Edit"));
    if ($myrow["id"] != 1)
 		delete_button_cell("Delete".$myrow["id"], _("Delete"));
	else
		label_cell('');
	end_row();

} //END WHILE LIST LOOP

end_table(1);
//-------------------------------------------------------------------------------------------------
start_table(TABLESTYLE2);

if ($selected_id != -1) 
{
  	if ($Mode == 'Edit') {
		//editing an existing parameter
		$myrow = get_param_edit($selected_id);

		$_POST['keycode'] = $myrow["keycode"];
		$_POST['label'] = $myrow["label"];
		$_POST['value'] = $myrow["value"];
	}
	hidden('selected_id', $selected_id);

	start_row();
} 

text_row_ex(_("Keycode").":", 'keycode',  32);

text_row_ex(_("Label").":", 'label',  32);

text_row_ex(_("Value"), 'value', 100);


end_table(1);

submit_add_or_update_center($selected_id == -1, '', 'both');

end_form();
end_page();
