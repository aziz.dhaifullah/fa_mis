<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_REPORTBUILDER';
$path_to_root = "..";
include_once($path_to_root . "/includes/session.inc");

page(_($help_context = "Group Report Account Setting"));

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/admin/db/group_report_db.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");


simple_page_mode(true);

//var_dump($selected_id);exit;
if (get_post('default')) {
	run_default($_SESSION['wa_current_user']->com_id);
}


if (get_post('Show')) {
	if ($_POST['company_show'] == "" )
	{
		display_error( _("Company Cannot be empty."));
		return false;
	}else if ($_POST['group_show'] < 0 )
	{
		display_error( _("Subgroup Cannot be empty."));
		return false;
	}else{
		$Ajax->activate('display_group');
	}
	//return true;
}
/*if (get_post('ADD_ITEM')) {
	//$Ajax->activate('display_group');
	//echo "<script>location.reload(true)</script>";
}*/
//-------------------------------------------------------------------------------------------------
function run_default($comps){
	$sqlcekgroupcomp = "SELECT id FROM 0_report_group WHERE company_id = ".$_SESSION['wa_current_user']->com_id." AND parent = 0";
	$resultcekcomp = db_query($sqlcekgroupcomp);
	$countcekcomp = db_num_rows($resultcekcomp);
	if($countcekcomp == 0){
		$getparentgroup = "SELECT id,name,class_id,parent,pos,inactive FROM 0_report_group WHERE company_id = 11 AND parent = 0";
		$resultparentgroup = db_query($getparentgroup);
		while ($rowparentgroup = db_fetch($resultparentgroup)){
		 	$idparentbefore = $rowparentgroup['id'];
		 	
		 	$sqlinsertparent = "INSERT INTO 0_report_group(id,name,class_id,parent,company_id,pos,inactive) VALUES(null,'".$rowparentgroup['name']."',".$rowparentgroup['class_id'].",".$rowparentgroup['parent'].",".$comps.",0,0)";
		 	if(db_query($sqlinsertparent)){
		 		
		 		$idparentafter = db_insert_id();
			 	
			 	$sqlgetchild = "SELECT * FROM 0_report_group WHERE company_id = 11 AND parent = ".$idparentbefore;
			 	$resultgetchild = db_query($sqlgetchild);
			 	$cekchild = db_num_rows($resultgetchild);
			 	
			 	if ($cekchild != 0) {
				 	
				 	while ($rowgetchild = db_fetch($resultgetchild)) {
				 		$idchildbefore = $rowgetchild["id"];
				 		
				 		$sqlinsertchild = "INSERT INTO 0_report_group(id,name,class_id,parent,company_id,pos,inactive) VALUES(null,'".$rowgetchild['name']."',".$rowgetchild['class_id'].",".$idparentafter.",".$comps.",0,0)";
				 		db_query($sqlinsertchild);
				 		
				 		$idchildafter = db_insert_id();
				 		
				 		$sqlgetcoa = "SELECT * FROM 0_report_account_master WHERE company_id = 11 AND group_id = ".$idchildbefore;
				 		$resultgetcoa = db_query($sqlgetcoa);
				 		
				 		while ($rowgetcoa = db_fetch($resultgetcoa)) {
				 			$sqlinsertcoa = "INSERT INTO 0_report_account_master(id,account_code,group_id,company_id) VALUES(null,'".$rowgetcoa['account_code']."',".$idchildafter.",".$comps.")";
				 			db_query($sqlinsertcoa);
				 		}
				 	}
			 	}else{
			 		$sqlgetcoa = "SELECT * FROM 0_report_account_master WHERE company_id = 11 AND group_id = ".$idparentbefore;
			 		$resultgetcoa = db_query($sqlgetcoa);
			 		
			 		while ($rowgetcoa = db_fetch($resultgetcoa)) {
			 			$sqlinsertcoa = "INSERT INTO 0_report_account_master(id,account_code,group_id,company_id) VALUES(null,'".$rowgetcoa['account_code']."',".$idparentafter.",".$comps.")";
			 			db_query($sqlinsertcoa);
			 		}
			 	}
		 	}
		}
	} 
}

function can_process($new) 
{
	if (strlen($_POST['groupname']) < 0 )
	{
		display_error( _("Group Name Cannot be empty."));
		return false;
	}

	if (@$_POST['company'] == "" )
	{
		display_error( _("Company Cannot be empty."));
		return false;
	}

	if (@$_POST['subgroup'] == "" )
	{
		display_error( _("Sub Group Cannot be empty."));
		return false;
	}

	if ($_POST['class_id'] == "" )
	{
		display_error( _("Class Cannot be empty."));
		return false;
	}

	/*if (strlen($_POST['class']) > 32 )
	{
		display_error( _("keycode entered max 32 characters long."));
		return false;
	}else if (strlen($_POST['keycode']) < 1 ) {
		display_error( _("keycode cannot be empty"));
		return false;
	}

	if (strlen($_POST['label']) > 32 )
	{
		display_error( _("label entered max 32 characters long."));
		return false;
	}else if (strlen($_POST['label']) < 1 ) {
		display_error( _("label cannot be empty"));
		return false;
	}

	if (strlen($_POST['value']) > 32 )
	{
		display_error( _("value entered max 32 characters long."));
		return false;
	}else if (strlen($_POST['value']) < 1 ) {
		display_error( _("value cannot be empty"));
		return false;
	}*/


	return true;
}

//-------------------------------------------------------------------------------------------------

//if (($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') && check_csrf_token())
if (($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM'))
{

	if (can_process($Mode == 'ADD_ITEM'))
	{
    	if ($selected_id != -1) 
    	{
    		edit_group($selected_id,$_POST['groupname'], $_POST['company'], $_POST['subgroup'],$_POST['class_id']);

    		display_notification_centered(_("The selected parameter has been updated."));
    	} 
    	else 
    	{
    		//$coa = $_POST['coa'];
    		//$countcoa = count($_POST['coa']);
    		//for ($i=0; $i < $countcoa; $i++) { 
    		add_group($_POST['groupname'], $_POST['company'], $_POST['subgroup'],$_POST['class_id']);
    		//}

//			$id = db_insert_id();
			
			display_notification_centered(_("A new group report has been added."));
		}
		$Mode = 'RESET';
	}
	
}

//-------------------------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
	if(cekparent($selected_id,$_POST['companyid'])){
		display_error( _("This Group has a sub group"));
	}else if (cekcoa($selected_id,$_POST['companyid'])) {
		display_error( _("This Group has an Account"));
	}else{
		delete_group($selected_id,$_POST['companyid']);
	}
	$Ajax->activate('display_group');
	$Mode = 'RESET';
}

//-------------------------------------------------------------------------------------------------
if ($Mode == 'RESET')
{
 	$selected_id = -1;
	$sav = get_post('show_inactive', null);
	unset($_POST);	// clean all input fields
	$_POST['show_inactive'] = $sav;
	
}
//-------------------------------------------------------------------------------------------------
//$classidhead = 1;
function control_show(){
	start_table(TABLESTYLE2);
	echo '<tr>
			<td class="label">Company :</td>
			<td>
				<span id="">
					<select id="company_show" name="company_show" style="min-width:200px !important;" onload="process()">
						<option value="" selected hidden>Select Company</option>';
						$sql = "select  id,nama
								from    (select * from ".TB_PREF."companies
								         order by parent, id) comps_sorted,
								        (select @pv := ".$_SESSION['wa_current_user']->com_id.") initialisation
								where   (CASE WHEN NOT EXISTS (SELECT id from ".TB_PREF."companies where parent = @pv) THEN
										find_in_set(id, @pv) ELSE find_in_set(parent, @pv) END)
								and 	length(@pv := concat(@pv, ',', id))
								group by parent";
						$result = db_query($sql);
						while ($row = db_fetch($result)) {
							echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
						}
						/*for ($a=1; $a <= 3; $a++) { 
							echo '<option value="'.$a.'">'.$class[$a].'</option>';
						}*/
	echo '			</select>
				</span>
			</td>
		</tr>';

	echo '<tr>
			<td class="label">Group :</td>
			<td>
				<span id="">
					<select id="group_show" name="group_show" style="min-width:200px !important;">
						<option value="" selected hidden>Select Group</option>';
	echo '			</select>
				</span>
			</td>
		</tr>';
	
	echo '<tr style="height: 40px !important;">';
	echo '<td colspan=9 align=center><button class="ajaxsubmit" type="submit" aspect="default" name="Show" style="margin-right: 15px !important;" id="Show" value="Show"><img src="../themes/default/images/ok.gif" height="12" alt=""><span>Show</span></button></td>';
	//submit_cells('Show',_("Show"),'','', 'default');
	echo "</tr>";
	end_table();
}

function control_input($selected_id,$Mode){
	start_table(TABLESTYLE2);
	if ($selected_id != -1) 
	{
	  	if ($Mode == 'Edit') {
			//editing an existing parameter
			$myrow = get_group_edit($selected_id);

			$_POST['groupname'] = $myrow["name"];
			$_POST['company'] = $myrow["company_id"];
			$_POST['subgroup'] = $myrow["parent"];
			$_POST['class_id'] = $myrow["class_id"];
		}
		hidden('selected_id', $selected_id);

		start_row();
	} 
	//var_dump($selected_id);
	//$accounts = get_chart_accounts_search('');
	//$report = 2;
	text_row_ex(_("Group Name :"), 'groupname', 50);
	echo '<tr>
			<td class="label">Company :</td>
			<td>
				<span>
					<select id="company" name="company" style="min-width:200px !important;" onload="process()">
						<option value="" selected hidden>Select Company</option>';
						$sql = "select  id,nama
								from    (select * from ".TB_PREF."companies
								         order by parent, id) comps_sorted,
								        (select @pv := ".$_SESSION['wa_current_user']->com_id.") initialisation
								where   (CASE WHEN NOT EXISTS (SELECT id from ".TB_PREF."companies where parent = @pv) THEN
										find_in_set(id, @pv) ELSE find_in_set(parent, @pv) END)
								and 	length(@pv := concat(@pv, ',', id))
								group by parent";
						$result = db_query($sql);
						while ($row = db_fetch($result)) {
							echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
						}
	echo '			</select>
				</span>
			</td>
		</tr>';
	echo '<tr>
			<td class="label">Sub Group of :</td>
			<td>
				<span id="">
					<select id="subgroup" name="subgroup" style="min-width:200px !important;">
						<option value="" selected hidden>Select Group</option>
						<option value="0">None</option>';
	echo '			</select>
				</span>
			</td>
		</tr>';
	class_list_row(_("Class:"), 'class_id', null);
	/*echo '<tr>
			<td class="label">Account :</td>
			<td>
				<span id="">
					<select class="js-example-basic-multiple" id="coa" name="coa[]" multiple="multiple" style="width:500px !important;" required>';
						
	echo '				</select>
				</span>
			</td>
		</tr>';*/
	//hidden('report', $report);


	/*
	echo '<select  name="states[]" multiple="multiple">
	  <option value="AL">Alabama</option>
	  <option value="WY">Wyoming</option>
	</select>';*/
	echo "<tr>";
	if ($selected_id != -1) {
		echo "<td colspan=3 align=center>";
		echo '<button class="" type="submit" aspect="default" name="UPDATE_ITEM" id="UPDATE_ITEM" value="Update" title="Submit changes"><img src="../themes/default/images/ok.gif" height="12" alt=""><span>Update</span></button>
			<button class="" type="submit" aspect="cancel" name="RESET" id="RESET" value="Cancel" title="Cancel edition"><img src="../themes/default/images/escape.png" height="12" alt=""><span>Cancel</span></button>';
		echo "</td>";
	}else{
		echo "<td colspan=9 align=center>";
		echo '<button type="submit" aspect="default" name="ADD_ITEM" style="margin-right: 15px !important;" id="ADD_ITEM" value="ADD_ITEM"><img src="../themes/default/images/ok.gif" height="12" alt=""><span>Add Item</span></button>';
		echo '<button type="submit" aspect="default" name="default" style="margin-right: 15px !important;" id="default" value="default"><img src="../themes/default/images/preferences.gif" height="12" alt=""><span>Default</span></button>';
		echo "</td>";
	}
	echo "</tr>";

	end_table();
	echo '<br>';
	//submit_add_or_update_center($selected_id == -1, "", 'both');
}

function display_group($company, $group){

	echo "<br>";
	start_table(TABLESTYLE);
	$th = array(_("Group Name"),_("Sub Group of"),_("Class"), "", "");
	$countheader = count($th);
	echo "<tr style='margin-top:10px;'>";
	echo "<td class='tableheader' colspan=".$countheader.">Group Mapping</td>";
	echo "</tr>";
	
	inactive_control_column($th);
	table_header($th);	

	$result = get_group_data($company,$group, false, null);
	$k = 0; //row colour counter
	while ($rowgroup = db_fetch($result)) {


		//$result2 = get_report_coa($classid['group_id'], $report, $compsimplode);

		alt_table_row_color($k);

		label_cell($rowgroup["groupname"]);
		label_cell($rowgroup["parent"]);
		label_cell($rowgroup["class"]);
		echo "<td align=center>";
		echo '<button type="submit" class="" name="Edit'.$rowgroup["id"].'" value="1" title="Edit"><img src="../themes/default/images/edit.gif" style="vertical-align:middle;width:12px;height:12px;border:0;">
				</button>';
		echo "</td>";
		echo "<td align=center>";
		echo '<button type="submit" class="" name="Delete'.$rowgroup["id"].'" value="1" title="Delete"><img src="../themes/default/images/delete.gif" style="vertical-align:middle;width:12px;height:12px;border:0;">
				</button>';
		echo "</td>";
		//edit_button_cell("Edit".$rowgroup["id"], _("Edit"));
	    //if ($myrow["id"] != 1)
	 	//delete_button_cell("Delete".$rowgroup["id"], _("Delete"));
		//else
		//label_cell('');
		end_row();

	//$classid+=1;
	}
	hidden('companyid', $company);
	end_table();
	echo '<br>';
}

start_form();
control_input($selected_id,$Mode);
end_form();

echo "<br>";
echo "<hr>";
echo "<br>";

start_form();
control_show();
end_form();

div_start('display_group');
if (get_post('Show')) {
	start_form();
	display_group($_POST['company_show'],$_POST['group_show']);
	end_form();
}
div_end();
echo '<link href="../select2/select2.min.css" rel="stylesheet" />
		<script src="../themes/dashboard/js/jquery-1.3.2.js"></script>
		<script src="../select2/select2.full.js"></script>
		<script type="text/javascript">
	    //$("#_page_body").ready(function(){
			$(\'#company\').on("change",function(){
		    	$( "#subgroup" ).empty();
			 	$(\'#subgroup\').append(\'<option value="" hidden>Select Group</option>\');
			    $(\'#subgroup\').append(\'<option value="0">None</option>\');
		    	var compid = $(\'#company\').val();
		    	var status = "show";
				$.ajax({
				  url: "get_subgroup_ajax.php",
				  data: { compid : compid, status : status},
				  dataType: "json",
				  type: "POST",
				  context: document.body
				}).done(function(data) {
					console.log(data);
				  	$.each(data, function(index, val) {
					    $(\'#subgroup\').append(\'<option value="\'+val.id+\'">\'+ val.subgroup_name +\'</option>\');
					});
				});
		    });

		    $(\'#subgroup\').on("change",function(){
		    	var subgroupval = $("#subgroup").val();
		    	var companyid = $("#company").val();
	    		$.ajax({
				  url: "get_subgroup_ajax.php",
				  data: {groupid : subgroupval, companyid : companyid},
				  dataType: "json",
				  type: "POST",
				  context: document.body
				}).done(function(data) {
					$(\'#class_id\').empty();
					$.each(data, function(index, val) {
					    if(subgroupval != 0){
					    	$(\'#class_id\').append(\'<option value="\'+val.cid+\'" selected>\'+ val.classname +\'</option>\');
					    	$("#class_id option:not(:selected)").remove();
						}else{
							$(\'#class_id\').append(\'<option value="\'+val.cid+\'">\'+ val.classname +\'</option>\');
						}
					});
				});
		    });

		    $(\'#company_show\').on("change",function(){
		    	$( "#group_show" ).empty();
			 	$(\'#group_show\').append(\'<option value="" selected hidden>Select Group</option>\');
			 	$(\'#group_show\').append(\'<option value="0">All</option>\');
		    	var compid = $(\'#company_show\').val();
		    	var status = "show";
				console.log(compid);
		    	$.ajax({
				  url: "get_subgroup_ajax.php",
				  data: { compid : compid, status : status},
				  dataType: "json",
				  type: "POST",
				  context: document.body
				}).done(function(data) {
					console.log(data);
				  	$.each(data, function(index, val) {
					    $(\'#group_show\').append(\'<option value="\'+val.id+\'">\'+ val.subgroup_name +\'</option>\');
					});
				});
		    });	

	    //});
		</script>';


echo '<br>';
/*/--------------------------------------------------------------------------------------*/

end_page();



