<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_group($groupname, $companyid, $subgroup, $class_id)
{
	$sql = "INSERT INTO ".TB_PREF."report_group (id, name, class_id, parent, company_id, inactive)
		VALUES (NULL,".db_escape($groupname).", ".db_escape($class_id).",".db_escape($subgroup).",".db_escape($companyid).",0)";

	return db_query($sql, "could not add param for $groupname");
}

function add_group_coa($accode, $groupid, $companyid)
{
	$sql = "INSERT INTO ".TB_PREF."report_account_master (id, account_code, group_id, company_id)
		VALUES (NULL,".db_escape($accode).", ".db_escape($groupid).",".db_escape($companyid).")";

	return db_query($sql, "could not add param for $accode");
}


//-----------------------------------------------------------------------------------------------

function get_report_class($report)
{
	$sql = "SELECT group_id FROM ".TB_PREF."report_account_master WHERE report_id =".db_escape($report)." 
			GROUP BY group_id
			";
	
	return db_query($sql, "could not get parameter");
}


function get_group_data($company, $groupid, $getaccount = false, $report = null, $noparent = null, $getclass = false, $classid = null){

	$sql = "SELECT a.id,a.name as groupname,
			rgc.name as parent,
			cc.class_name as class,
			cc.cid as class_id
			FROM ".TB_PREF."report_group a
			INNER JOIN ".TB_PREF."chart_class AS cc ON cc.cid = a.class_id";
	
	if(!$getaccount){
		$sql.= " LEFT JOIN ".TB_PREF."report_group AS rgc ON rgc.id = a.parent";
	}else{
		$sql.= " INNER JOIN ".TB_PREF."report_group AS rgc ON rgc.id = a.parent";
	}
	
	$sql.=" WHERE a.company_id = ".$company." AND a.inactive = 0";
	
	if($classid != null){
		$sql .= " AND a.class_id = ".$classid;
	}else{
		if ($report != null) {
			if($report == 1){
				$sql .= " AND a.class_id IN (1,2,3)";		
			}else{
				$sql .= " AND a.class_id IN (4,6,10)";
			}
		}
	}		

	if($noparent != null){
		$sql .= " AND a.parent = ".$noparent;
	}

	if($groupid != 0){
		$sql .= " AND (a.id = ".$groupid." OR a.parent = ".$groupid.")";
	}

	if($getclass){
		$sql .= " GROUP BY CAST(cc.cid as unsigned)";
	}

	$sql .= " ORDER BY CAST(cc.cid as unsigned),rgc.id,a.id";
	//echo $sql;
	return db_query($sql, "could not get group data");
}

function get_report_coa($groupid,$companyid)
{
	
	$sql = 'SELECT gr.id,CONCAT(cm.account_code2," ", cm.account_name) as account, cm.account_code2, cm.account_name, gr.account_code,cp.nama FROM '.TB_PREF.'report_account_master gr
			INNER JOIN '.TB_PREF.'chart_master cm ON gr.account_code = cm.account_code
			INNER JOIN '.TB_PREF.'companies cp ON gr.company_id = cp.id
			WHERE gr.group_id ='.db_escape($groupid).'
			AND gr.company_id ='.$companyid; 
	
	return db_query($sql, "could not get parameter");
}

function get_report_coa_comp($groupid,$company_id)
{
	$sql = 'SELECT gr.id,CONCAT(cm.account_code2," ", cm.account_name) as account, cm.account_code2, cm.account_name, gr.account_code,cp.nama FROM '.TB_PREF.'report_account_master gr
			INNER JOIN '.TB_PREF.'chart_master cm ON gr.account_code = cm.account_code
			INNER JOIN '.TB_PREF.'companies cp ON gr.company_id = cp.id
			WHERE gr.group_id ='.$groupid.'
			AND gr.company_id ='.$company_id; 

	
	return db_query($sql, "could not get parameter");
}

function get_report_coa_pl($company_id)
{
	$sql = 'SELECT gr.id,CONCAT(cm.account_code2," ", cm.account_name) as account, cm.account_code2, cm.account_name, gr.account_code,cp.nama FROM '.TB_PREF.'report_account_master gr
			INNER JOIN '.TB_PREF.'chart_master cm ON gr.account_code = cm.account_code
			INNER JOIN '.TB_PREF.'companies cp ON gr.company_id = cp.id
			WHERE gr.company_id ='.$company_id; 

	
	return db_query($sql, "could not get parameter");
}

//-----------------------------------------------------------------------------------------------


function get_group_edit($id)
{
	$sql = "SELECT * FROM ".TB_PREF."report_group WHERE id=".db_escape($id);

	$result = db_query($sql, "could not get prameter $id");

	return db_fetch($result);
}

function edit_group($id, $groupname, $companyid, $subgroup, $class_id)
{
	$sql = "UPDATE ".TB_PREF."report_group SET 
		name=".db_escape($groupname).", 
		class_id=".db_escape($class_id).",
		parent=".db_escape($subgroup).",
		company_id=".db_escape($companyid)."
		WHERE id=" . db_escape($id);
	return db_query($sql, "could not update parameter for $id");
}

//-----------------------------------------------------------------------------------------------

function cekparent($id,$companyid){
	$sql = "SELECT COUNT(id) AS countparent FROM ".TB_PREF."report_group WHERE parent = ".$id." AND company_id =".$companyid;
	$result = db_query($sql);
	$fetch = db_fetch($result);
	$countparent = $fetch['countparent'];
	if($countparent > 0){
		return true;
	}
	return false;
}

function cekcoa($id,$companyid){
	$sql = "SELECT COUNT(account_code) AS countcoa FROM ".TB_PREF."report_account_master WHERE group_id = ".$id." AND company_id =".$companyid;
	$result = db_query($sql);
	$fetch = db_fetch($result);
	$countcoa = $fetch['countcoa'];
	if($countcoa > 0){
		return true;
	}
	return false;
}

function delete_group($id,$companyid)
{
	 
	$sql="DELETE FROM ".TB_PREF."report_group WHERE id=".db_escape($id)." AND company_id =".$companyid;

	db_query($sql, "could not delete param $id");
}

function delete_group_coa($id)
{
	$sql="DELETE FROM ".TB_PREF."report_account_master WHERE id=".db_escape($id);

	db_query($sql, "could not delete param $id");
}

//-----------------------------------------------------------------------------------------------

