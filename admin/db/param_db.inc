<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_param($keycode, $label, $value)
{
	$sql = "INSERT INTO ".TB_PREF."config_param (id, keycode, label, value)
		VALUES (NULL,".db_escape($keycode).", ".db_escape($label) .",".db_escape($value).")";

	return db_query($sql, "could not add param for $keycode");
}


//-----------------------------------------------------------------------------------------------

function update_param($id, $keycode, $label, $value)
{
	$sql = "UPDATE ".TB_PREF."config_param SET 
		keycode=".db_escape($keycode).", 
		label=".db_escape($label).",
		value=".db_escape($value)." 
		WHERE id=" . db_escape($id);
	return db_query($sql, "could not update parameter for $id");
}

//-----------------------------------------------------------------------------------------------

function get_param()
{
	$sql = "SELECT * FROM ".TB_PREF."config_param";
	
	return db_query($sql, "could not get parameter");
}

//-----------------------------------------------------------------------------------------------

function get_param_edit($id)
{
	$sql = "SELECT * FROM ".TB_PREF."config_param WHERE id=".db_escape($id);

	$result = db_query($sql, "could not get prameter $id");

	return db_fetch($result);
}

//-----------------------------------------------------------------------------------------------


function delete_param($id)
{
	$sql="DELETE FROM ".TB_PREF."config_param WHERE id=".db_escape($id);

	db_query($sql, "could not delete param $id");
}

//-----------------------------------------------------------------------------------------------

