<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root = "..";
include_once($path_to_root . "/includes/ui/items_cart.inc");
include_once($path_to_root . "/includes/session.inc");
$page_security = isset($_GET['NewPayment']) || 
	@($_SESSION['pay_items']->trans_type==ST_BANKPAYMENT)
 ? 'SA_PAYMENT' : 'SA_DEPOSIT';

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/ui/gl_bank_ui.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/gl_ui.inc");
include_once($path_to_root . "/admin/db/attachments_db.inc");

$js = '';
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

if (isset($_GET['NewPayment'])) {
	$_SESSION['page_title'] = _($help_context = "Spend Money");
	create_cart(ST_BANKPAYMENT, 0);
} else if(isset($_GET['NewDeposit'])) {
	$_SESSION['page_title'] = _($help_context = "Receive Money");
	create_cart(ST_BANKDEPOSIT, 0);
} else if(isset($_GET['ModifyPayment'])) {
	$_SESSION['page_title'] = _($help_context = "Modify Spend Money")." #".$_GET['trans_no'];
	create_cart(ST_BANKPAYMENT, $_GET['trans_no']);
} else if(isset($_GET['ModifyDeposit'])) {
	$_SESSION['page_title'] = _($help_context = "Modify Receive Money")." #".$_GET['trans_no'];
	create_cart(ST_BANKDEPOSIT, $_GET['trans_no']);
}
page($_SESSION['page_title'], false, false, '', $js);

//-----------------------------------------------------------------------------------------------
check_db_has_bank_accounts(_("There are no bank accounts defined in the system."));

if (isset($_GET['ModifyDeposit']) || isset($_GET['ModifyPayment']))
	check_is_editable($_SESSION['pay_items']->trans_type, $_SESSION['pay_items']->order_id);

//----------------------------------------------------------------------------------------
if (list_updated('PersonDetailID')) {
	$br = get_branch(get_post('PersonDetailID'));
	$_POST['person_id'] = $br['debtor_no'];
	$Ajax->activate('person_id');
}

//--------------------------------------------------------------------------------------------------
function line_start_focus() {
  global 	$Ajax;
  //$_POST['code_id']='';
  $Ajax->activate('items_table');
  $Ajax->activate('footer');
  set_focus('_code_id_edit');
}

//-----------------------------------------------------------------------------------------------

if (isset($_POST['PreviewOrder'])) {
	//print_r($_SESSION['Items']->line_items);
	//echo key($_SESSION['Items']['gls']);

$link = "<script>window.open('../reporting/rep1701_pre2.php')</script>";
echo $link;
	//meta_forward('../reporting/rep1701_pre.php');
}

if (isset($_GET['AddedID']))
{
	$trans_no = $_GET['AddedID'];
	$trans_type = ST_BANKPAYMENT;

   	display_notification_centered(sprintf(_("Payment %d has been entered"), $trans_no));

	display_note(get_gl_view_str($trans_type, $trans_no, _("&View the GL Postings")));

	hyperlink_params($_SERVER['PHP_SELF'], _("Enter Another Spend Money"), "NewPayment=yes");
	echo '<br>';
	echo '<center>
		<a id="_el587db145a48803.74073301" href="../reporting/rep1701.php?type_id='.$trans_type.'&amp;trans_no='. $trans_no.'" accesskey="P" target="_blank">Print Voucher</a>
		</center>';

	hyperlink_params($_SERVER['PHP_SELF'], _("Enter A Bank &Deposit"), "NewDeposit=yes");

	hyperlink_params("$path_to_root/admin/attachments.php", _("Add an Attachment"), "filterType=$trans_type&trans_no=$trans_no");

	display_footer_exit();
}

if (isset($_GET['UpdatedID']))
{
	$trans_no = $_GET['UpdatedID'];
	$trans_type = ST_BANKPAYMENT;

   	display_notification_centered(sprintf(_("Payment %d has been modified"), $trans_no));

	display_note(get_gl_view_str($trans_type, $trans_no, _("&View the GL Postings")));
	echo '<br>';
	echo '<center>
		<a id="_el587db145a48803.74073301" href="../reporting/rep1701.php?type_id='.$trans_type.'&amp;trans_no='. $trans_no.'" accesskey="P" target="_blank">Print Voucher</a>
		</center>';

	hyperlink_params($_SERVER['PHP_SELF'], _("Enter Another Spend Money"), "NewPayment=yes");

	hyperlink_params($_SERVER['PHP_SELF'], _("Enter Receive Money"), "NewDeposit=yes");

	display_footer_exit();
}

if (isset($_GET['AddedDep']))
{
	$trans_no = $_GET['AddedDep'];
	$trans_type = ST_BANKDEPOSIT;

   	display_notification_centered(sprintf(_("Receive Money entered"), $trans_no));

	display_note(get_gl_view_str($trans_type, $trans_no, _("View the GL Postings")));

	hyperlink_params($_SERVER['PHP_SELF'], _("Enter Another Receive Money"), "NewDeposit=yes");
	echo '<br>';
	echo '<center>
		<a id="_el587db145a48803.74073301" href="../reporting/rep1701.php?type_id='.$trans_type.'&amp;trans_no='. $trans_no.'" accesskey="P" target="_blank"><u>P</u>rint Voucher Bank Deposit</a>
		</center>';

	hyperlink_params($_SERVER['PHP_SELF'], _("Enter A Spend Money"), "NewPayment=yes");

	display_footer_exit();
}
if (isset($_GET['UpdatedDep']))
{
	$trans_no = $_GET['UpdatedDep'];
	$trans_type = ST_BANKDEPOSIT;

   	display_notification_centered(sprintf(_("Deposit %d has been modified"), $trans_no));

	display_note(get_gl_view_str($trans_type, $trans_no, _("&View the GL Postings ")));
	echo '<br>';
	echo '<center>
		<a id="_el587db145a48803.74073301" href="../reporting/rep1701.php?type_id='.$trans_type.'&amp;trans_no='. $trans_no.'" accesskey="P" target="_blank">Print Voucher</a>
		</center>';

	hyperlink_params($_SERVER['PHP_SELF'], _("Enter Another Receive Money"), "NewDeposit=yes");

	hyperlink_params($_SERVER['PHP_SELF'], _("Enter A Spend Money"), "NewPayment=yes");

	display_footer_exit();
}

//--------------------------------------------------------------------------------------------------

function create_cart($type, $trans_no)
{
	global $Refs;

	if (isset($_SESSION['pay_items']))
	{
		unset ($_SESSION['pay_items']);
	}

	$cart = new items_cart($type);
    $cart->order_id = $trans_no;

	if ($trans_no) {

		$bank_trans = db_fetch(get_bank_trans($type, $trans_no));
		$_POST['bank_account'] = $bank_trans["bank_act"];
		$_POST['PayType'] = $bank_trans["person_type_id"];
		$cart->reference = $bank_trans["ref"];

		if ($bank_trans["person_type_id"] == PT_CUSTOMER)
		{
			$trans = get_customer_trans($trans_no, $type);	
			$_POST['person_id'] = $trans["debtor_no"];
			$_POST['PersonDetailID'] = $trans["branch_code"];
		}
		elseif ($bank_trans["person_type_id"] == PT_SUPPLIER)
		{
			$trans = get_supp_trans($trans_no, $type);
			$_POST['person_id'] = $trans["supplier_id"];
		}
		elseif ($bank_trans["person_type_id"] == PT_MISC)
			$_POST['person_id'] = $bank_trans["person_id"];
		elseif ($bank_trans["person_type_id"] == PT_QUICKENTRY)
			$_POST['person_id'] = $bank_trans["person_id"];
		else 
			$_POST['person_id'] = $bank_trans["person_id"];

		$cart->memo_ = get_comments_string($type, $trans_no);
		$cart->tran_date = sql2date($bank_trans['trans_date']);

		$cart->original_amount = $bank_trans['amount'];
		$result = get_gl_trans($type, $trans_no);
		if ($result) {
			while ($row = db_fetch($result)) {
				if (is_bank_account($row['account'])) {
					// date exchange rate is currenly not stored in bank transaction,
					// so we have to restore it from original gl amounts
					$ex_rate = $bank_trans['amount']/$row['amount'];
				} else {
					$cart->add_gl_item( $row['account'], $row['dimension_id'],
						$row['dimension2_id'], $row['amount'], $row['memo_']);
				}
			}
		}

		// apply exchange rate
		foreach($cart->gl_items as $line_no => $line)
			$cart->gl_items[$line_no]->amount *= $ex_rate;

	} else {
		//$cart->reference = next_ref($cart->trans_type,array('payfrom'=>@$_POST['bank_account']));
		//$cart->reference = $Refs->get_next($cart->trans_type, null, $cart->tran_date);
		$cart->tran_date = new_doc_date();
		if (!is_date_in_fiscalyear($cart->tran_date))
			$cart->tran_date = end_fiscalyear();
	}

	$_POST['memo_'] = $cart->memo_;
	$_POST['ref'] = $cart->reference;
	$_POST['date_'] = $cart->tran_date;

	$_SESSION['pay_items'] = &$cart;
}
//-----------------------------------------------------------------------------------------------

function check_trans()
{
	global $Refs;

	$input_error = 0;
	$amnal=str_replace('.','',@$_POST['amountall']);
	$amnal=str_replace(',','.',@$amnal);
	if (@$_SESSION['pay_items']->count_gl_items() < 1) {
		display_error(_("You must enter at least one payment line."));
		set_focus('code_id');
		$input_error = 1;
	}

	if ($_SESSION['pay_items']->gl_items_total() == 0.0) {
		display_error(_("The total bank amount cannot be 0."));
		set_focus('code_id');
		$input_error = 1;
	}
	if (number_format(@$amnal,2)<number_format(@$_SESSION['pay_items']->gl_items_total(),2)) {
		display_error(_("Not Balanced"));
		set_focus('code_id');
		$input_error = 1;
	}
	$limit = get_bank_account_limit($_POST['bank_account'], $_POST['date_']);

	$amnt_chg = -$_SESSION['pay_items']->gl_items_total()-$_SESSION['pay_items']->original_amount;

	if ($limit !== null && floatcmp($limit, -$amnt_chg) < 0)
	{
		display_error(sprintf(_("The total bank amount exceeds allowed limit (%s)."), price_format($limit-$_SESSION['pay_items']->original_amount)));
		set_focus('code_id');
		$input_error = 1;
	}
	if ($trans = check_bank_account_history($amnt_chg, $_POST['bank_account'], $_POST['date_'])) {

		display_error(sprintf(_("The bank transaction would result in exceed of authorized overdraft limit for transaction: %s #%s on %s."),
			$systypes_array[$trans['type']], $trans['trans_no'], sql2date($trans['trans_date'])));
		set_focus('amount');
		$input_error = 1;
	}
	/*
	if (!check_reference($_POST['ref'], $_SESSION['pay_items']->trans_type, $_SESSION['pay_items']->order_id))
	{
		set_focus('ref');
		$input_error = 1;
	}
	*/
	if (!is_date($_POST['date_']))
	{
		display_error(_("The entered date for the payment is invalid."));
		set_focus('date_');
		$input_error = 1;
	}
	elseif (!is_date_in_fiscalyear($_POST['date_']))
	{
		display_error(_("The entered date is out of fiscal year or is closed for further data entry."));
		set_focus('date_');
		$input_error = 1;
	} 

	if (get_post('PayType')==PT_CUSTOMER && (!get_post('person_id') || !get_post('PersonDetailID'))) {
		display_error(_("You have to select customer and customer branch."));
		set_focus('person_id');
		$input_error = 1;
	} elseif (get_post('PayType')==PT_SUPPLIER && (!get_post('person_id'))) {
		display_error(_("You have to select supplier."));
		set_focus('person_id');
		$input_error = 1;
	}
	/*if (!db_has_currency_rates(get_bank_account_currency($_POST['bank_account']), $_POST['date_'], true))
		$input_error = 1;
	*/
	if (isset($_POST['settled_amount']) && in_array(get_post('PayType'), array(PT_SUPPLIER, PT_CUSTOMER)) && (input_num('settled_amount') <= 0)) {
		display_error(_("Settled amount have to be positive number."));
		set_focus('person_id');
		$input_error = 1;
	}
	return $input_error;
}
if (isset($_POST['Process']) && !check_trans())
{
	
	/*$mrkt=array(@$_POST['marketing_type']
			, @$_POST['marketing_place']
			, @$_POST['marketing_address']
			, @$_POST['marketing_name']
			, @$_POST['marketing_position']
			, @$_POST['marketing_company']
			, @$_POST['marketing_bussiness']);
			
	*/
	$nline=@$_POST['baris'];
	for($a=0;$a<=$nline;$a++){
		if(@$_POST['marketing_name_'.$a]!=''){
			$qr=add_marketing_trans($_POST['ref'],@$_POST['marketing_type_'.$a]
					, @$_POST['marketing_place_'.$a]
					, @$_POST['marketing_address_'.$a]
					, @$_POST['marketing_name_'.$a]
					, @$_POST['marketing_position_'.$a]
					, @$_POST['marketing_company_'.$a]
					, @$_POST['marketing_bussiness_'.$a]
					, @$_POST['npwp_'.$a]
					, @$_POST['bpotong_'.$a]
					, @$_POST['sklien_'.$a]
					, @$_POST['invoice_'.$a]
					, @$_POST['ssyai_'.$a], @$_POST['marketing_amount_'.$a]);
		}
	}
	/*
	if(@$_POST['marketing_type']!='' and @$_POST['marketing_place_0']==''){
		$qr=add_marketing_trans($_POST['ref'],@$_POST['marketing_type']
				, @$_POST['marketing_place']
				, @$_POST['marketing_address']
				, @$_POST['marketing_name']
				, @$_POST['marketing_position']
				, @$_POST['marketing_company']
				, @$_POST['marketing_bussiness']
				, @$_POST['npwp']
				, @$_POST['bpotong']
				, @$_POST['sklien']
				, @$_POST['invoice']
				, @$_POST['ssyai']);	
	}
	*/
	//meta_forward($_SERVER['PHP_SELF'], "AddedDep=".$qr);
	begin_transaction();

	$_SESSION['pay_items'] = &$_SESSION['pay_items'];
	$new = $_SESSION['pay_items']->order_id == 0;

	add_new_exchange_rate(get_bank_account_currency(get_post('bank_account')), get_post('date_'), input_num('_ex_rate'), input_num('_ex_rate2'));
	
	if(get_post('PayType')== PT_SUPPLIER && get_post('person_id')==6){
		$trans = write_bank_transaction_marketing(
			$_SESSION['pay_items']->trans_type, $_SESSION['pay_items']->order_id, $_POST['bank_account'],
			$_SESSION['pay_items'], $_POST['date_'],
			$_POST['PayType'], $_POST['person_id'], get_post('PersonDetailID'),
			$_POST['ref'], $_POST['memo_'], true, input_num('settled_amount', null)
			, @$_POST['marketing_type']
			, @$_POST['marketing_place']
			, @$_POST['marketing_address']
			, @$_POST['marketing_name']
			, @$_POST['marketing_position']
			, @$_POST['marketing_company']
			, @$_POST['marketing_bussiness']			
			);
	}else{
		$trans = write_bank_transaction(
			$_SESSION['pay_items']->trans_type, $_SESSION['pay_items']->order_id, $_POST['bank_account'],
			$_SESSION['pay_items'], $_POST['date_'],
			$_POST['PayType'], $_POST['person_id'], get_post('PersonDetailID'),
			$_POST['ref'], $_POST['memo_'], true, input_num('settled_amount', null));
		
	}
	$trans_type = $trans[0];
   	$trans_no = $trans[1];
	new_doc_date($_POST['date_']);

	$_SESSION['pay_items']->clear_items();
	unset($_SESSION['pay_items']);

	commit_transaction();
	
	if ($new)
		meta_forward($_SERVER['PHP_SELF'], $trans_type==ST_BANKPAYMENT ?
			"AddedID=$trans_no" : "AddedDep=$trans_no");
	else
		meta_forward($_SERVER['PHP_SELF'], $trans_type==ST_BANKPAYMENT ?
			"UpdatedID=$trans_no" : "UpdatedDep=$trans_no");
	
}

//-----------------------------------------------------------------------------------------------

function check_item_data()
{
	/*if (!check_num('amount', 0))
	{
		display_error( _("The amount entered is not a valid number or is less than zero."));
		set_focus('amount');
		return false;
	}*/
	$suppcek=array(11703, 21210, 21220, 21230);
	if(in_array($_POST['code_id'],$suppcek)){
		if($_POST['person_id']==0){
			display_error( _("Please Choose Supplier First."));
			set_focus('PayType');
			return false;
		}
	}
	$jasacek=array(21220, 21230);
	if(in_array($_POST['code_id'],$jasacek)){
		echo $_POST['code_id'];
		if($_POST['jenis_jasa']==0){
			display_error( _("Please Choose Jenis Jasa."));
			set_focus('jenis_jasa');
			return false;
		}
	}
	if (isset($_POST['_ex_rate']) && input_num('_ex_rate') <= 0)
	{
		display_error( _("The exchange rate cannot be zero or a negative number."));
		set_focus('_ex_rate');
		return false;
	}

	return true;
}

//-----------------------------------------------------------------------------------------------

function handle_update_item()
{
	$amount = ($_SESSION['pay_items']->trans_type==ST_BANKPAYMENT ? 1:-1) * input_num('amount');
    if($_POST['UpdateItem'] != "" && check_item_data())
    {
    	if(@$_POST['stock_id']=='')
    	$_SESSION['pay_items']->update_gl_item($_POST['Index'], $_POST['code_id'], $_POST['dimension_id'], $_POST['dimension2_id'], $amount , $_POST['LineMemo'], null,null,null,@$_POST['jenis_jasa']);
    	else
    	$_SESSION['pay_items']->update_gl_item($_POST['Index'], $_POST['code_id'], $_POST['dimension_id'], $_POST['dimension2_id'], $amount , $_POST['LineMemo'], null,null,$_POST['stock_id'],@$_POST['jenis_jasa']);
    }
	line_start_focus();
}

//-----------------------------------------------------------------------------------------------

function handle_delete_item($id)
{
	$_SESSION['pay_items']->remove_gl_item($id);
	line_start_focus();
}

//-----------------------------------------------------------------------------------------------

if (isset($_POST['upload']))
{

	include_once ( "excel_reader2.php");
	$file=$_FILES["fileToUpload"]["tmp_name"];
	$data = new Spreadsheet_Excel_Reader($file);
	$j = 0;
	//print_r($data);exit;
	$ttl=0;
	for ($i=2; $i <= ($data->rowcount($sheet_index=0)); $i++){ 
		$account   = $data->val($i, 1);
		if($account!=''){
			//$tgls=explode('/',$tgl);
			$sucbaccount  = str_replace('-','', @$data->val($i, 2));
			if($sucbaccount!='')
				$account=$sucbaccount;
			$name    = $data->val($i, 3);
			$npwp    = $data->val($i, 4);
			$address    = $data->val($i, 5);
			$total    =str_replace(',','', $data->val($i, 6));
			$memo=@$data->val($i, 7);

			$_SESSION['pay_items']->add_gl_item($account, 
				'','', 
				$total, 
				$memo, null,null,null,
				'',
				'');
			$_POST['marketing_address_'.$j]=$address;
			$_POST['marketing_name_'.$j]=$name;
			$_POST['npwp_'.$j]=$npwp;
			$_POST['marketing_amount_'.$j]=$total;

			$ttl+=$total;

			$j++;
		} 
			$_POST['amountall']=price_format($ttl);
	}
	/*
	$wth=(@$_POST['wth']==''?'0':@$_POST['wth']);
	add_tax_type($_POST['name'], $_POST['sales_gl_code'],
		$_POST['purchasing_gl_code'], $_POST['rate'],@$wth );
	
	display_notification(_('New tax type has been added'));
	$Mode = 'RESET';
	*/
}
function handle_new_item()
{
	if (!check_item_data())
		echo 'aa';
	$amount = ($_SESSION['pay_items']->trans_type==ST_BANKPAYMENT ? 1:-1) * input_num('amount');
	if(@$_POST['stock_id']=='')
	$_SESSION['pay_items']->add_gl_item($_POST['code_id'], $_POST['dimension_id'],$_POST['dimension2_id'], $amount, $_POST['LineMemo'], null,null,null,null,@$_POST['jenis_jasa']);
	else
	$_SESSION['pay_items']->add_gl_item($_POST['code_id'], $_POST['dimension_id'],$_POST['dimension2_id'], $amount, $_POST['LineMemo'], null,null,null,@$_POST['stock_id'],@$_POST['jenis_jasa']);
	line_start_focus();
}
//-----------------------------------------------------------------------------------------------
$ided = find_submit('Edit');
$id = find_submit('Delete');
if ($id != -1)
	handle_delete_item($id);

if (isset($_POST['AddItem']))
	handle_new_item();

if (isset($_POST['UpdateItem']))
	handle_update_item();

if (isset($_POST['CancelItemChanges']))
	line_start_focus();

if (isset($_POST['go']))
{
	display_quick_entries($_SESSION['pay_items'], $_POST['person_id'], input_num('totamount'), 
		$_SESSION['pay_items']->trans_type==ST_BANKPAYMENT ? QE_PAYMENT : QE_DEPOSIT);
	$_POST['totamount'] = price_format(0); $Ajax->activate('totamount');
	line_start_focus();
}
//-----------------------------------------------------------------------------------------------
//start_form();

if (isset($_POST['upload'])
	or isset($_POST['AddItem']) or isset($_POST['UpdateItem'])or isset($_POST['CancelItemChanges']) or $ided!=-1 or $id!=-1 or isset($_POST['_bank_account_update']) or isset($_POST['_date__changed']) or isset($_POST['_PayType_update']))
{

echo '<form method="post" id="formBank" action="">';
display_bank_header($_SESSION['pay_items']);

start_table(TABLESTYLE2, "width='100%'", 10);
start_row();
echo "<td>";

display_gl_items($_SESSION['pay_items']->trans_type==ST_BANKPAYMENT ?_("Payment Items"):_("Deposit Items"), $_SESSION['pay_items']);
//if($_POST['UpdateItem'] == "")
gl_options_controls($_SESSION['pay_items'],@$_POST['_code_id_edit'],@$_POST['marketing_sponsor']);
echo "</td>";
end_row();
end_table(1);

//submit_center_first('PreviewOrder', 'Preview',_('Check entered data and save document'));
echo '<center><a href="../reporting/rep1701_pre2.php?b='.@$_POST['bank_account'].'&d='.@$_POST['date_'].'&r='.@$_POST['ref'].'" target="_blank" class="export">Preview</a></center>';
submit_center_first('Update', _("Update"), '', null);
submit_center_last('Process', $_SESSION['pay_items']->trans_type==ST_BANKPAYMENT ?
	_("Process Payment"):_("Process Deposit"), '', 'default');

end_form();
echo '<script>document.getElementById("formBank").reset();</script>';
//------------------------------------------------------------------------------------------------
}else{

	start_table(TABLESTYLE2);
	echo '<form enctype="multipart/form-data" action="" method="post">';
	echo '<tr><td class="label">Upload Marketing:</td>
	<td><input type="file" name="fileToUpload" id="fileToUpload" ></td>
	</tr>';
	echo '<tr><td class="label" colspan="2"><a href="template_upload_marketing.xls">Download Template</a></td></tr>';
	echo '
		<style>
			.export{
			    align-items: flex-start;
			    text-align: center;
			    cursor: default;
			    color: buttontext;
			    background-color: buttonface;
			    box-sizing: border-box;
			    vertical-align: top;
			    -webkit-appearance: none;
			    border-style: solid;
			    border: 1px #0066cc solid;
			    padding-left: 10px;
			    padding-right: 10px;
			}
		</style>';
	echo '<tr><td class="label" colspan="2" align="center"><input type="submit" class="export" name="upload" Value="Upload"></td></tr>';
	echo '</form>';
	end_table(1);
}
end_page();

