<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_JOURNALENTRY';
$path_to_root = "..";
include_once($path_to_root . "/includes/ui/items_cart.inc");

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/ui/gl_journal_ui.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/gl_ui.inc");

$js = '';
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

if (isset($_GET['ModifyGL'])) {
	$_SESSION['page_title'] = sprintf(_("Modifying Journal Transaction # %d."), 
		$_GET['trans_no']);
	$help_context = "Modifying Journal Entry";
} else
	$_SESSION['page_title'] = _($help_context = "Journal Entry");

page($_SESSION['page_title'], false, false,'', $js);
//--------------------------------------------------------------------------------------------------
//192.168.10.20:81
$sql = "SELECT * FROM 0_companies where id='".$_SESSION["wa_current_user"]->com_id."' ;";
$compname='';$kode='';
if($result=db_query($sql)){
	while($row=db_fetch($result)){
		//print_r($row);
		$compname=$row['nama'];
		$kode=$row['kode'];
	}
}
//echo $compname;
$gethost = gethostbynamel(getHostName());
if($_SERVER['SERVER_ADDR'] == $gethost[2]){
	$host = $gethost[2];
}else{
	$host = "localhost";
}

//print_r($_SERVER['SERVER_PORT']);
$server_port = $_SERVER['SERVER_PORT'];
//echo $compname;
echo '<div>
    <iframe id="iframe2" src="http://'.$host.':'.$server_port.'/faimport/index.php/Import/jurnal_memorial/'.$kode.'" frameborder="0" style="overflow: hidden; height: 400px;
        width: 1000px; position: absolute;" ></iframe>
</div>';
end_page();
/*echo '<div>
    <iframe id="iframe2" src="http://localhost/faimport/index.php/Import/jurnal_memorial/'.$kode.'" frameborder="0" style="overflow: hidden; height: 400px;
        width: 1000px; position: absolute;" ></iframe>
</div>';
end_page();*/
