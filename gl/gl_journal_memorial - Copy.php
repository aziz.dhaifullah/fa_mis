<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_JOURNALENTRY';
$path_to_root = "..";
include_once($path_to_root . "/includes/ui/items_cart.inc");

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/ui/gl_journal_ui.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/gl_ui.inc");

$js = '';
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

if (isset($_GET['ModifyGL'])) {
	$_SESSION['page_title'] = sprintf(_("Modifying Journal Transaction # %d."), 
		$_GET['trans_no']);
	$help_context = "Modifying Journal Entry";
} else
	$_SESSION['page_title'] = _($help_context = "Journal Entry");

page($_SESSION['page_title'], false, false,'', $js);
//--------------------------------------------------------------------------------------------------

function line_start_focus() {
  global 	$Ajax;

  unset($_POST['Index']);
  $Ajax->activate('tabs');
  unset($_POST['_code_id_edit'], $_POST['code_id'], $_POST['AmountDebit'], 
  	$_POST['AmountCredit'], $_POST['dimension_id'], $_POST['dimension2_id']);
  set_focus('_code_id_edit');
}
//-----------------------------------------------------------------------------------------------

if (isset($_GET['AddedID'])) 
{
	$trans_no = $_GET['AddedID'];
	$trans_type = ST_JOURNAL;

   	display_notification_centered( _("Journal entry has been entered") . " #$trans_no");

    display_note(get_gl_view_str($trans_type, $trans_no, _("&View this Journal Entry")));

	reset_focus();
	hyperlink_params($_SERVER['PHP_SELF'], _("Enter &New Journal Entry"), "NewJournal=Yes");

	hyperlink_params("$path_to_root/admin/attachments.php", _("Add an Attachment"), "filterType=$trans_type&trans_no=$trans_no");

	display_footer_exit();
} elseif (isset($_GET['UpdatedID'])) 
{
	$trans_no = $_GET['UpdatedID'];
	$trans_type = ST_JOURNAL;

   	display_notification_centered( _("Journal entry has been updated") . " #$trans_no");

    display_note(get_gl_view_str($trans_type, $trans_no, _("&View this Journal Entry")));
	echo '<br>';
	echo '<center>
		<a id="_el587db145a48803.74073301" href="../reporting/rep1701.php?type_id='.$trans_type.'&amp;trans_no='. $trans_no.'" accesskey="P" target="_blank">Print Voucher</a>
		</center>';

   	hyperlink_no_params($path_to_root."/gl/inquiry/journal_inquiry.php", _("Return to Journal &Inquiry"));

	display_footer_exit();
}
//--------------------------------------------------------------------------------------------------

if (isset($_GET['NewJournal']))
{
	//create_cart(0,0);
}

if (isset($_POST['Process']))
{
	//echo @$_POST['jumlah'];
	$refsss=array();
	//db_close();

	$sql = "select max(trans_no) from 0_journal where type=0";
    $result = db_query($sql,"The next transaction number for  could not be retrieved");
    $myrow = db_fetch_row($result);

	$trans_id = @$myrow[0];
	db_free_result($result);

	$sql2 = "INSERT INTO 0_journal(
	   `type`,`trans_no`, `amount`, `currency`, `rate`, `reference`, `source_ref`, `tran_date`,
	   `event_date`, `doc_date`, `posting`)
	   VALUES";

	for ($a=0; $a<= intval(@$_POST['jumlah']); $a++){ 
		$total=0; 

		$ref   = @$_POST['ref_'.$a];

		if(!in_array($ref,$refsss)){
			$refsss[]=$ref;
		}

		$trans_id++;
		$tgls   =  explode('/',@$_POST['tgl_'.$a]);
		$tgl=@$tgls[1].'/'.@$tgls[0].'/'.@$tgls[2];
		//$tgl   =  @$_POST['tgl_'.$a];
		$memo   = @$_POST['memo_'.$a];
		$account   =  @$_POST['acc_'.$a];
		if($account!=''){

			$db    = @$_POST['db_'.$a];
			$cr    = @$_POST['cr_'.$a];
			$total=$db;
			if($db=='')
				$total=$cr;
			if($total=='* -')
				$total=0;
			$total=str_replace('* ', '', $total);
			//if($a>0)
			//	$sql2 .=",";

			$sql2 = "INSERT INTO 0_journal(
			   `type`,`trans_no`, `amount`, `currency`, `rate`, `reference`, `source_ref`, `tran_date`,
			   `event_date`, `doc_date`, `posting`)
			   VALUES";
			$sql2 .="   (".(0).",".($trans_id).",".($total).",'IDR',1,'"
			  .($ref)."','',"
			  ."'".date2sql($tgl)."',"
			  ."'".date2sql($tgl)."',"
			  ."'".date2sql($tgl)."',"
			  .db_escape('').")";

			//if($a==intval(@$_POST['jumlah'])-1)
				$sql2 .=";";
				//echo $sql2.'<br>';
			begin_transaction();
			
			db_query($sql2, 'cannot add journal entry');	
			commit_transaction();

			//add_journal2('ST_JOURNAL', $trans_id, $total,$tgl ,'IDR', $ref);
			//write_journal_upload($ref,$tgl,$account,$cr,$db,$memo);
		}
	}
	//$sql2.="COMMIT;UNLOCK TABLES;";
	//echo $sql2;
	/*
	try {
		begin_transaction();
		db_query($sql2, 'cannot add journal entry');	
		commit_transaction();
  	} catch( Exception $e ) {
  		var_dump($e);
	   	cancel_transaction();
  	}
	/*


	*/
	//meta_forward($_SERVER['PHP_SELF'], "AddedID=$trans_no");

}

//-----------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------

if (isset($_POST['upload']) or isset($_POST['_bank_account_update']) or isset($_POST['_date__changed']) or isset($_POST['_PayType_update']))
{
start_form();

	include_once ( "excel_reader2.php");
	$file=$_FILES["fileToUpload"]["tmp_name"];
	move_uploaded_file($_FILES["fileToUpload"]["name"], "$path_to_root/import/".$_FILES["fileToUpload"]["name"]);
	$data = new Spreadsheet_Excel_Reader($file);
	$j = 0;
	//print_r($data);exit;
	
	$ttl=0;

	start_table(TABLESTYLE2, "width='90%'", 10);
	start_row();
	echo "<td>";
	div_start('items_table');
	start_table(TABLESTYLE, "width='95%'");
	$th = array(_("Ref"),_("Date"),_("Account Code"),
		_("Debit"), _("Credit"), _("Description"));
	table_header($th);
	$refss=array();
	$a=0;
	for ($i=2; $i <= ($data->rowcount($sheet_index=0)); $i++){ 
		$ref   = str_replace('M', 'MI-', $data->val($i, 1));
		$tgl   = $data->val($i, 2);
		$memo   = $data->val($i, 3);
		$account   = $data->val($i, 4);
		if($account!=''){

			if(!in_array($ref,$refss)){
				$refss[]=$ref;
				alt_table_row_color($k);
				label_cell('','colspan=6 height=10');
    			end_row();
			}
			$db    =str_replace(',','', $data->val($i, 8));
			$cr    =str_replace(',','', $data->val($i, 9));
			$total=$db;
			if($db=='')
				$total=$cr*(-1);

			alt_table_row_color($k);
			echo '<input type="hidden" name="ref_'.$a.'" value="'.$ref.'">';
			echo '<input type="hidden" name="tgl_'.$a.'" value="'.$tgl.'">';
			echo '<input type="hidden" name="acc_'.$a.'" value="'.$account.'">';
			echo '<input type="hidden" name="cr_'.$a.'" value="'.$cr.'">';
			echo '<input type="hidden" name="db_'.$a.'" value="'.$db.'">';
			echo '<input type="hidden" name="memo_'.$a.'" value="'.$memo.'">';
		
			label_cell($ref);
			label_cell($tgl);
			label_cell($account);
			if($db!='')
    		{
    			amount_cell(abs($total));
    			label_cell("");
    		}
    		else
    		{
    			label_cell("");
    			amount_cell(abs($total));
    		}	
			label_cell($memo);
    		end_row();
			$ttl+=$total;

			$j++;
			$a++;
		} 
		$_POST['amountall']=price_format($ttl);
	}
			echo '<input type="hidden" name="jumlah" value="'.$a.'">';
	echo "</td>";
	end_row();
	end_table(1);
	//submit_center('Process', _("Process Journal Entry"), true , _('Process journal entry only if debits equal to credits'), 'default');
echo '<tr><td class="label" colspan="2" align="center"><input type="submit" class="export" name="Process" Value="Process"></td></tr>';
	
	echo "</br>";
end_form();
}else{

	start_table(TABLESTYLE2);
	echo '<form enctype="multipart/form-data" action="" method="post">';
	echo '<tr><td class="label">Upload Memorial:</td>
	<td><input type="file" name="fileToUpload" id="fileToUpload" ></td>
	</tr>';
	echo '<tr><td class="label" colspan="2"><a href="template_upload_memorial.xls">Download Template</a></td></tr>';
	echo '
		<style>
			.export{
			    align-items: flex-start;
			    text-align: center;
			    cursor: default;
			    color: buttontext;
			    background-color: buttonface;
			    box-sizing: border-box;
			    vertical-align: top;
			    -webkit-appearance: none;
			    border-style: solid;
			    border: 1px #0066cc solid;
			    padding-left: 10px;
			    padding-right: 10px;
			}
		</style>';
	echo '<tr><td class="label" colspan="2" align="center"><input type="submit" class="export" name="upload" Value="Upload"></td></tr>';
	echo '</form>';
	end_table(1);
}

end_page();
