<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
function display_bank_header(&$order)
{
	global $Ajax,$Refs;
	$payment = $order->trans_type == ST_BANKPAYMENT;

	$customer_error = false;
	div_start('pmt_header');

	start_outer_table(TABLESTYLE2, "width='90%'"); // outer table
	/*
	echo '
	<script type="text/javascript">
	var formatThousands = function(n, dp){
	  var s = \'\'+(Math.floor(n)), d = n % 1, i = s.length, r = \'\';
	  while ( (i -= 3) > 0 ) { r = \',\' + s.substr(i, 3) + r; }
	  return s.substr(0, i + 3) + r + 
	    (d ? \'.\' + Math.round(d * Math.pow(10, dp || 2)) : \'\');
	};
	function setamount(amount){
		var x = document.getElementsByName("amount");
		var y = document.getElementsByName("amountall");
		x.value=amount;
		y.value=formatThousands( amount );	
		console.log(formatThousands( amount ));	
	}
	</script>';
	*/
	table_section(1);
	//$_POST['bank_account']=0;
	//$Ajax->addUpdate(true, 'bank_account', $_POST['bank_account']);
    bank_accounts_list_row( $payment ? _("From:") : _("Into:"), 'bank_account', null, true);
    date_row(_("Date:"), 'date_', '', true, 0, 0, 0, null, true);
	$bank=get_bank_account(get_post('bank_account'));
	//$ref=$Refs->get_next($order->trans_type, null,array('supplier'=>$order->trans_type, 'payfrom'=>get_post('bank_account'),'date'=>$order->trans_type));
	//if(get_post('bank_account')=='13')
	//echo @$_POST['ref'];
	//echo get_post('date_');

	if (!isset($_POST['PayType']))
	{
		if (isset($_GET['PayType']))
			$_POST['PayType'] = $_GET['PayType'];
		else
			$_POST['PayType'] = "";
	}

	$ref=@$_POST['ref'];
	if (isset($_POST['_date__changed'])) {
		$ref=$Refs->get_next($order->trans_type, null,array('supplier'=>$payment, 'payfrom'=>get_post('bank_account'),'date'=>get_post('date_')));
		//$ref='aa';
		$_POST['ref'] = $ref;
		$Ajax->activate('ref');		
	}
	if (isset($_POST['_bank_account_update'])) {
		$ref=$Refs->get_next($order->trans_type, null,array('supplier'=>$payment, 'payfrom'=>get_post('bank_account'),'date'=>get_post('date_')));
		//$ref='aa';
		$_POST['ref'] = $ref;
		$Ajax->activate('ref');
	}
	ref_row(_("Reference:"), 'ref', '', @$ref, false, $order->trans_type, get_post('date_'));
    
	table_section(2, "33%");
	
	if (!isset($_POST['person_id']))
	{
		if (isset($_GET['PayPerson']))
			$_POST['person_id'] = $_GET['PayPerson'];
		else
			$_POST['person_id'] = "";
	}
	if (isset($_POST['_PayType_update'])) {
		$_POST['person_id'] = '';
		$Ajax->activate('pmt_header');
		//$Ajax->activate('code_id');
		$Ajax->activate('pagehelp');
		$Ajax->activate('editors');
		$Ajax->activate('footer');
	}
    payment_person_types_list_row( $payment ? _("Pay To:"):_("From:"),
		 'PayType', $_POST['PayType'], true);
    switch ($_POST['PayType'])
    {
		case PT_MISC :
    		text_row_ex($payment ?_("To the Order of:"):_("Name:"),
				 'person_id', 40, 50);
    		break;
		case PT_SUPPLIER :
    		supplier_list_row(_("Supplier:"), 'person_id', null, false, true, false, true);
    		break;
		case PT_CUSTOMER :
    		customer_list_row(_("Customer:"), 'person_id', null, false, true, false, true);

        	if (db_customer_has_branches($_POST['person_id']))
        	{
        		customer_branches_list_row(_("Branch:"), $_POST['person_id'], 
					'PersonDetailID', null, false, true, true, true);
        	}
        	else
        	{
				$_POST['PersonDetailID'] = ANY_NUMERIC;
        		hidden('PersonDetailID');
        	}
        	$trans = get_customer_habit($_POST['person_id']); // take care of customers on hold
        	if ($trans['dissallow_invoices'] != 0)
        	{
        		if ($payment)
        		{
        			$customer_error = true;
					display_error(_("This customer account is on hold."));
        		}
        		else
					display_warning(_("This customer account is on hold."));
        	}
    		break;

		case PT_QUICKENTRY :
			quick_entries_list_row(_("Type").":", 'person_id', null, ($payment ? QE_PAYMENT : QE_DEPOSIT), true);
			$qid = get_quick_entry(get_post('person_id'));
			if (list_updated('person_id')) {
				unset($_POST['totamount']); // enable default
				$Ajax->activate('footer');
				$Ajax->activate('totamount');
			}
			amount_row($qid['base_desc'].":", 'totamount', price_format($qid['base_amount']),
				 null, "&nbsp;&nbsp;".submit('go', _("Go"), false, false, true));
			break;	
    }

	table_section(3, "33%");

	if (!$order->order_id && !get_post('bank_account'))
	{
		if ($_POST['PayType'] == PT_CUSTOMER)
			$_POST['bank_account'] = get_default_customer_bank_account($_POST['person_id']);
		elseif ($_POST['PayType'] == PT_SUPPLIER)	
			$_POST['bank_account'] = get_default_supplier_bank_account($_POST['person_id']);
		else
			unset($_POST['bank_account']);
	}		
	
    if ($payment)
		bank_balance_row($_POST['bank_account']);

	$bank_currency = get_bank_account_currency($_POST['bank_account']);

	exchange_rate_display(get_company_currency(), $bank_currency, $_POST['date_']);
	echo '
	<script type="text/javascript">
	function setallamount(amount){		
		var ttlamount=document.getElementById("amountalls").value;
		var all=amount;
		var all2 = all.replace(".", "");
		var all2 = all2.replace(".", "");
		var all2 = all2.replace(".", "");
		var all2 = all2.replace(".", "");
		var all2 = all2.replace(".", "");
		var all2 = all2.replace(".", "");
		var all2 = all2.replace(",", ".");
		if(ttlamount<0)
			ttlamount=ttlamount*(-1);

		var ttlamount = ttlamount.replace(".", ",");
		if(parseFloat(all2)==0)
			document.getElementById("amountall").value=ttlamount;
		if(parseFloat(all2)<ttlamount)
			document.getElementById("amountall").value=ttlamount;

	}
	</script>';
	echo '<tr><td class="label">Amount:</td>
		<td>
		<input class="amount" type="text" name="amountall" id="amountall" size="15" onclick="setallamount(this.value)" dec="2" value="'.@$_POST['amountall'].'">
		</td></tr>';

	end_outer_table(1); // outer table

	div_end();
	if ($customer_error)
	{
		end_form();
		end_page();
		exit;
	}
}
//---------------------------------------------------------------------------------

function display_gl_items($title, &$order)
{
	global $path_to_root;

	$dim = get_company_pref('use_dimension');
	$colspan = ($dim == 2 ? 5 : ($dim == 1 ? 4 : 2));
	display_heading($title);

    div_start('items_table');
	start_table(TABLESTYLE, "width='95%'");
	if($order->trans_type==1){
		if ($dim == 2)
			$th = array(_("Account Code"), _("Account Description"), _("Item Description"), _("Dimension")." 1",
				_("Dimension")." 2", _("Amount"), _("Description"), _("Jenis Jasa"), "");
		elseif ($dim == 1)
			$th = array(_("Account Code"), _("Account Description"),  _("Item Description"), _("Dimension"),
				_("Amount"), _("Description"), _("Jenis Jasa"), "");
		else
			$th = array(_("Account Code"), _("Account Description"),  _("Item Description"),
				_("Amount"), _("Description"), _("Jenis Jasa"), "");
	}else{
		$colspan=$colspan-1;
		if ($dim == 2)
			$th = array(_("Account Code"), _("Account Description"),_("Dimension")." 1",
				_("Dimension")." 2", _("Amount"), _("Description"), _("Jenis Jasa"), "");
		elseif ($dim == 1)
			$th = array(_("Account Code"), _("Account Description"), _("Dimension"),
				_("Amount"), _("Description"), _("Jenis Jasa"), "");
		else
			$th = array(_("Account Code"), _("Account Description"),
				_("Amount"), _("Description"), _("Jenis Jasa"), "");		
	}
	if (count($order->gl_items)) $th[] = '';

	table_header($th);
	$k = 0;  //row colour counter

	$id = find_submit('Edit');
	$ttlamnt=0;
	//echo $id.' ';
	foreach ($order->gl_items as $line => $item)
	{
		//print_r($order->gl_items[$line]);
		//echo $id.' '.$line;
		$ttlamnt=$ttlamnt+$item->amount;
		if ($id != $line)
		{
    		alt_table_row_color($k);

			label_cell($item->code_id);
			label_cell($item->description);
			if($order->trans_type==1){
				if($item->item!=null or $item->item!=0)
					label_cell( get_gl_stock_name($item->item));
				else
					label_cell( '');
			}
    		if ($dim >= 1)
				label_cell(get_dimension_string($item->dimension_id, true));
    		if ($dim > 1)
				label_cell(get_dimension_string($item->dimension2_id, true));
			if ($order->trans_type == ST_BANKDEPOSIT)
				amount_cell(-$item->amount);
			else		
				amount_cell($item->amount);
			label_cell($item->reference);
			$taxjasa='';
			if(trim(@$item->tax_jasa)!=''){
				$sql="select * from 0_tax_jasa where id='".@$item->tax_jasa."' order by id asc";
				$query=db_query($sql, "The transactions for could not be retrieved");
				while ($myrow0 = db_fetch($query))
				{
					$taxjasa=$myrow0['jenis'];
				}
			}
			label_cell(@$taxjasa);

			edit_button_cell("Edit$line", _("Edit"),_('Edit document line'));
			delete_button_cell("Delete$line", _("Delete"),_('Remove line from document'));
    		end_row();
    		if (strpos($item->code_id, '62002') !== false){
    		//if($item->code_id==62002){
    		echo '<tr>
				<td class="tableheader">Type</td>
				<td class="tableheader">Place</td>
				<td class="tableheader">Address</td>
				<td class="tableheader">Name</td>
				<td class="tableheader">Position</td>
				<td class="tableheader">Name of Company</td>
				<td class="tableheader">Business Type</td>
				<td class="tableheader" colspan="4"></td>
				</tr>';

    		echo '<tr>
					<td>
						<select name="marketing_type_'.$line.'">
						<option value="'.@$_POST['marketing_type_'.$line].'" >'.@$_POST['marketing_type_'.$line].'</option>
						
						<option value="Lunch" '.(@$_POST['marketing_type_'.$line]=='Lunch'?'selected':'').'>Lunch</option>
						<option value="Dinner" '.(@$_POST['marketing_type_'.$line]=='Dinner'?'selected':'').'>Dinner</option>
						<option value="Goods" '.(@$_POST['marketing_type_'.$line]=='Goods'?'selected':'').'>Goods</option>
						</select>
					</td>
					<td><input type="text" name="marketing_place_'.$line.'" value="'.@$_POST['marketing_place_'.$line].'"></td>
					<td><input type="text" name="marketing_address_'.$line.'" value="'.@$_POST['marketing_address_'.$line].'"></td>
					<td><input type="text" name="marketing_name_'.$line.'" value="'.@$_POST['marketing_name_'.$line].'"></td>
					<td><input type="text" name="marketing_position_'.$line.'" value="'.@$_POST['marketing_position_'.$line].'"></td>
					<td><input type="text" name="marketing_company_'.$line.'" value="'.@$_POST['marketing_company_'.$line].'"></td>
					<td colspan="4"><input type="text" name="marketing_bussiness_'.$line.'" value="'.@$_POST['marketing_bussiness_'.$line].'" size="35">
					<input type="hidden" name="marketing_amount_'.$line.'" value="'.$item->amount.'">
					<input type="hidden" name="baris" value="'.$line.'">
						<input type="hidden" name="marketing_id_'.$line.'" value="'.@$_POST['marketing_id_'.$line].'">
					</td>
				</tr>';

			}
			if (strpos($item->code_id, '62001') !== false){
    		//if($item->code_id==62002){
    		echo '<tr>
				<td class="tableheader">Name</td>
				<td class="tableheader">NPWP</td>
				<td class="tableheader">Address</td>
				<td class="tableheader">Jenis Biaya</td>
				<td class="tableheader">No Surat Klien</td>
				<td class="tableheader">No Invoice</td>
				<td class="tableheader">No Surat KAM</td>
				<td class="tableheader" colspan="4"></td>
				</tr>';

    		echo '<tr>
    				<td><input type="text" name="marketing_name_'.$line.'" value="'.@$_POST['marketing_name_'.$line].'"></td>
					<td><input type="text" name="npwp_'.$line.'" value="'.@$_POST['npwp_'.$line].'"></td>
					<td><input type="text" name="marketing_address_'.$line.'" value="'.@$_POST['marketing_address_'.$line].'"></td>
					<td><input type="text" name="marketing_type_'.$line.'" value="'.@$_POST['marketing_type_'.$line].'">
					</td>
					<td><input type="text" name="sklien_'.$line.'" value="'.@$_POST['sklien_'.$line].'"></td>
					<td><input type="text" name="invoice_'.$line.'" value="'.@$_POST['invoice_'.$line].'"></td>
					<td colspan="4"><input type="text" name="ssyai_'.$line.'" value="'.@$_POST['ssyai_'.$line].'" size="35">
					<input type="hidden" name="marketing_amount_'.$line.'" value="'.$item->amount.'">
					<input type="hidden" name="baris" value="'.$line.'">
						<input type="hidden" name="marketing_id_'.$line.'" value="'.@$_POST['marketing_id_'.$line].'">
					</td>
				</tr>';

			}
			if (strpos($item->code_id, '62003') !== false){
	    		//if($item->code_id==62002){
	    		echo '<tr>
					<td class="tableheader">Name</td>
					<td class="tableheader">NPWP</td>
					<td class="tableheader" colspan="3">Address</td>
					<td class="tableheader" colspan="5"></td>
					</tr>';

	    		echo '<tr>
						<td><input type="text" name="marketing_name_'.$line.'" value="'.@$_POST['marketing_name_'.$line].'"></td>
						<td><input type="text" name="npwp_'.$line.'" value="'.@$_POST['npwp_'.$line].'"></td>
						<td colspan="3"><input type="text" name="marketing_address_'.$line.'" value="'.@$_POST['marketing_address_'.$line].'"></td>
						<input type="hidden" name="marketing_amount_'.$line.'" value="'.$item->amount.'">
						<input type="hidden" name="baris" value="'.$line.'">
						<input type="hidden" name="marketing_id_'.$line.'" value="'.@$_POST['marketing_id_'.$line].'">
						</td>
					</tr>';

			}
		}
		else
		{
			//echo $line;
			//echo $id.' '.$line;
			gl_edit_item_controls($order, $dim, $line);
		}
		
	}
	$amntall=str_replace('.', '', @$_POST['amountall']);
	$amntall=floatval(str_replace(',', '.', $amntall));
	//echo $order->gl_items_total();
	if ($id == -1)
		gl_edit_item_controls($order, $dim, $line);
	if ($order->count_gl_items()){
		//echo '<td></td>';
		if($amntall>=$order->gl_items_total()){
			$ttl=$order->gl_items_total();
			if ($order->trans_type == ST_BANKDEPOSIT)
				$ttl=abs($order->gl_items_total());
			label_row(_("Total"), number_format2($ttl, user_price_dec()),"colspan=" . $colspan . " align=right", "align=right",4);
			//echo 'b';
		}else{
			$ttl=$order->gl_items_total();
			label_row(_("Total"), '<font style="color:red";>'.number_format2(abs($order->gl_items_total()).'</font>', user_price_dec()),"colspan=" . $colspan . " align=right", "align=right",4);
			//echo 'a';
		}
	}
	echo '<input type="hidden" name="amountalls" id="amountalls" value="'.@$ttl.'">';
    end_table();
	div_end();
}

//---------------------------------------------------------------------------------

function gl_edit_item_controls(&$order, $dim, $Index=null)
{
	global $Ajax;
	$payment = $order->trans_type == ST_BANKPAYMENT;

	start_row();
	$id = find_submit('Edit');
	if ($Index != -1 && $Index == $id)
	{
		$item = $order->gl_items[$Index];
		//print_r($item);
		$_POST['code_id'] = $item->code_id;
		$_POST['dimension_id'] = $item->dimension_id;
		$_POST['dimension2_id'] = $item->dimension2_id;
		$_POST['amount'] = price_format(($item->amount));
		if ($order->trans_type == ST_BANKDEPOSIT)
			$_POST['amount'] = price_format(-($item->amount));
		$_POST['description'] = $item->description;
		$_POST['LineMemo'] = $item->reference;
		$_POST['stock_id'] = $item->item;
		$_POST['jenis_jasa'] = $item->tax_jasa;


		hidden('Index', $id);
		echo gl_all_accounts_list('code_id', $_POST['code_id'], false, true);
		if($order->trans_type==1){
			echo'<td>';
			stock_items_list_cells(null, 'stock_id', null, false, true, false, true, array('editable' => 30,'search_box' => false));
			echo'</td>';
		}
		if ($dim >= 1)
			dimensions_list_cells(null, 'dimension_id', null, true, " ", false, 1);
		if ($dim > 1)
			dimensions_list_cells(null, 'dimension2_id', null, true, " ", false, 2);
	    $Ajax->activate('items_table');
	}
	else
	{
		//$_POST['amount'] = price_format(0);
		$_POST['dimension_id'] = 0;
		$_POST['dimension2_id'] = 0;
		if(isset($_POST['_code_id_update'])) {
			    //$Ajax->activate('code_id');
		}

		if ($_POST['PayType'] == PT_CUSTOMER)
		{
			$acc = get_branch_accounts($_POST['PersonDetailID']);
			$_POST['code_id'] = $acc['receivables_account'];
		}
		elseif ($_POST['PayType'] == PT_SUPPLIER)
		{
			$acc = get_supplier_accounts($_POST['person_id']);
			$_POST['code_id'] = $acc['payable_account'];
		}
		else {
			$_POST['code_id'] =
				get_company_pref($payment ? 'default_cogs_act':'default_inv_sales_act');
		}

	
		echo gl_all_accounts_list('code_id', '', false, true);
		if($order->trans_type==1){
			echo'<td>';
			stock_items_list_cells(null, 'stock_id', null, false, true, false, true, array('editable' => 30,'search_box' => false));
			echo'</td>';
		}
		if ($dim >= 1)
			dimensions_list_cells(null, 'dimension_id', null, true, " ", false, 1);
		if ($dim > 1)
			dimensions_list_cells(null, 'dimension2_id', null, true, " ", false, 2);
	}
	if ($dim < 1)
		hidden('dimension_id', 0);
	if ($dim < 2)
		hidden('dimension2_id', 0);

	//amount_cells(null, 'amount',@$_POST['amount']);
	echo'<td>';
	echo '
		<script type="text/javascript">
		function cekamount(){
			//var ttlamount=document.getElementById("amountalls").value;
			var ttlamount=document.getElementById("amountalls").value;

			var all=document.getElementById("amountall").value;
			var all2 = all.replace(".", "");
			var all2 = all2.replace(".", "");
			var all2 = all2.replace(".", "");
			var all2 = all2.replace(".", "");
			var all2 = all2.replace(".", "");
			var all2 = all2.replace(".", "");
			var all2 = all2.replace(".", "");
			var all2 = all2.replace(".", "");
			var all2 = all2.replace(".", "");
			var all2 = all2.replace(",", ".");
			if(all2==false)
				all2=0;

			var amntnya= document.getElementById("amount").value;
			
			var amntnya2= document.getElementById("amount3").value;
				var amntnya2 = amntnya2.replace(".", "");
				var amntnya2 = amntnya2.replace(".", "");
				var amntnya2 = amntnya2.replace(".", "");
				var amntnya2 = amntnya2.replace(".", "");
				var amntnya2 = amntnya2.replace(".", "");
				var amntnya2 = amntnya2.replace(".", "");
				var amntnya2 = amntnya2.replace(".", "");
				var amntnya2 = amntnya2.replace(".", "");
				var amntnya2 = amntnya2.replace(".", "");
				var amntnya2 = amntnya2.replace(",", ".");
			//console.log(parseInt(amntnya));
			if(parseInt(amntnya)==0){
				//console.log(ttlamount);
				//console.log(amntnya2);
				ttlamount=parseFloat(ttlamount)-parseFloat(amntnya2);
			}

			if(!ttlamount)
				ttlamount=0;
			if(ttlamount=="")
				ttlamount=0;
			if(parseFloat(ttlamount)<0)
				var amnt=parseFloat(all2)+parseFloat(ttlamount);
			else
				var amnt=parseFloat(all2)-parseFloat(ttlamount);
			//}
			var amnt = amnt.toFixed(2).replace(".", ",");
			if(amnt=="NaN")
				amnt=0;
			if(amnt==0 && ttlamount==0)
				var amnt=all2;
			//console.log(parseInt(ttlamount));
			//console.log(amnt);
			//console.log(all2);
			//console.log(amnt);
			if(parseInt(amntnya)==0)
			document.getElementById("amount").value=amnt;
			document.getElementById("amount").select();
			//console.log(all2);
			//console.log(parseInt(ttlamount));
			//console.log(amnt);
		}
		function cekmemo(){
			document.getElementById("LineMemo").select();
		}
		</script>';
	$amnedit=0;
	if($Index == $id)
	$amnedit=@$_POST['amount'];
	echo '<input type="hidden" name="amount3" id="amount3" value="'.@$amnedit.'">';
	$cam=' onfocus="cekamount()" onchange="cekamount()"';
	echo '<input class="amount" type="text" name="amount" id="amount" '.@$cam.' size="15" maxlength="15" dec="2" value="'.@$amnedit.'">';
	echo'</td>';
	//document.getElementById("amount").select();
	//text_cells_ex(null, 'LineMemo', 35, 255);
	echo'<td>';
	echo '<input type="text" id="LineMemo" name="LineMemo" onfocus="cekmemo()" size="35" maxlength="255" value="'.@$_POST['LineMemo'].'">';
	echo'</td>';
	echo'<td>';
	//list jenis jasa
	echo '<select name="jenis_jasa"><option value="">&nbsp;</option>';
	$sql="select * from 0_tax_jasa order by id asc";
	$query=db_query($sql, "The transactions for could not be retrieved");
	while ($myrow0 = db_fetch($query))
	{
		echo '<option value="'.$myrow0['id'].'" '.(@$_POST['jenis_jasa']==$myrow0['id']?'selected':'').'>'.$myrow0['jenis'].'</option>';
	}
	echo '</select>';
	//stock_items_list_cells(null, 'stock_id', null, false, true, false, true, array('editable' => 30,'search_box' => false));
	echo'</td>';
	if ($id != -1)
	{
		button_cell('UpdateItem', _("Update"),
				_('Confirm changes'), ICON_UPDATE);
		button_cell('CancelItemChanges', _("Cancel"),
				_('Cancel changes'), ICON_CANCEL);
 		set_focus('_code_id_edit');
 		$line=$Index;
 		if (strpos($item->code_id, '62002') !== false){
    		//if($item->code_id==62002){
    		echo '<tr>
				<td class="tableheader">Type</td>
				<td class="tableheader">Place</td>
				<td class="tableheader">Address</td>
				<td class="tableheader">Name</td>
				<td class="tableheader">Position</td>
				<td class="tableheader">Name of Company</td>
				<td class="tableheader">Business Type</td>
				<td class="tableheader" colspan="4"></td>
				</tr>';

    		echo '<tr>
					<td>
						<select name="marketing_type_'.$line.'">
						<option value="'.@$_POST['marketing_type_'.$line].'" >'.@$_POST['marketing_type_'.$line].'</option>
						
						<option value="Lunch" '.(@$_POST['marketing_type_'.$line]=='Lunch'?'selected':'').'>Lunch</option>
						<option value="Dinner" '.(@$_POST['marketing_type_'.$line]=='Dinner'?'selected':'').'>Dinner</option>
						<option value="Goods" '.(@$_POST['marketing_type_'.$line]=='Goods'?'selected':'').'>Goods</option>
						</select>
					</td>
					<td><input type="text" name="marketing_place_'.$line.'" value="'.@$_POST['marketing_place_'.$line].'"></td>
					<td><input type="text" name="marketing_address_'.$line.'" value="'.@$_POST['marketing_address_'.$line].'"></td>
					<td><input type="text" name="marketing_name_'.$line.'" value="'.@$_POST['marketing_name_'.$line].'"></td>
					<td><input type="text" name="marketing_position_'.$line.'" value="'.@$_POST['marketing_position_'.$line].'"></td>
					<td><input type="text" name="marketing_company_'.$line.'" value="'.@$_POST['marketing_company_'.$line].'"></td>
					<td colspan="4"><input type="text" name="marketing_bussiness_'.$line.'" value="'.@$_POST['marketing_bussiness_'.$line].'" size="35">
					<input type="hidden" name="marketing_amount_'.$line.'" value="'.$item->amount.'">
					<input type="hidden" name="baris" value="'.$line.'">
					<input type="hidden" name="marketing_id_'.$line.'" value="'.@$_POST['marketing_id_'.$line].'">
					</td>
				</tr>';

			}
			if (strpos($item->code_id, '62001') !== false){
    		//if($item->code_id==62002){
    		echo '<tr>
				<td class="tableheader">Name</td>
				<td class="tableheader">NPWP</td>
				<td class="tableheader">Address</td>
				<td class="tableheader">Jenis Biaya</td>
				<td class="tableheader">No Surat Klien</td>
				<td class="tableheader">No Invoice</td>
				<td class="tableheader">No Surat KAM</td>
				<td class="tableheader" colspan="4"></td>
				</tr>';

    		echo '<tr>
    				<td><input type="text" name="marketing_name_'.$line.'" value="'.@$_POST['marketing_name_'.$line].'"></td>
					<td><input type="text" name="npwp_'.$line.'" value="'.@$_POST['npwp_'.$line].'"></td>
					<td><input type="text" name="marketing_address_'.$line.'" value="'.@$_POST['marketing_address_'.$line].'"></td>
					<td><input type="text" name="marketing_type_'.$line.'" value="'.@$_POST['marketing_type_'.$line].'">
					</td>
					<td><input type="text" name="sklien_'.$line.'" value="'.@$_POST['sklien_'.$line].'"></td>
					<td><input type="text" name="invoice_'.$line.'" value="'.@$_POST['invoice_'.$line].'"></td>
					<td colspan="4"><input type="text" name="ssyai_'.$line.'" value="'.@$_POST['ssyai_'.$line].'" size="35">
					<input type="hidden" name="marketing_amount_'.$line.'" value="'.$item->amount.'">
					<input type="hidden" name="baris" value="'.$line.'">
					<input type="hidden" name="marketing_id_'.$line.'" value="'.@$_POST['marketing_id_'.$line].'">
					</td>
				</tr>';

			}
			if (strpos($item->code_id, '62003') !== false){
	    		//if($item->code_id==62002){
	    		echo '<tr>
					<td class="tableheader">Name</td>
					<td class="tableheader">NPWP</td>
					<td class="tableheader" colspan="3">Address</td>
					<td class="tableheader" colspan="5"></td>
					</tr>';

	    		echo '<tr>
						<td><input type="text" name="marketing_name_'.$line.'" value="'.@$_POST['marketing_name_'.$line].'"></td>
						<td><input type="text" name="npwp_'.$line.'" value="'.@$_POST['npwp_'.$line].'"></td>
						<td colspan="3"><input type="text" name="marketing_address_'.$line.'" value="'.@$_POST['marketing_address_'.$line].'"></td>
						<input type="hidden" name="marketing_amount_'.$line.'" value="'.$item->amount.'">
						<input type="hidden" name="baris" value="'.$line.'">
						<input type="hidden" name="marketing_id_'.$line.'" value="'.@$_POST['marketing_id_'.$line].'">
						</td>
					</tr>';

			}
	}
	else
	{
		submit_cells('AddItem', _("Add Item"), "colspan=2",
		    _('Add new item to document'), true);
	}

	end_row();
}


//---------------------------------------------------------------------------------

function gl_options_controls($order,$codeedit=null,$tampil=null)
{
	//print_r($order);
	div_start('footer');
	echo "<br><table align='center'>";

	$type = get_post('PayType');
	$bank_curr = get_bank_account_currency(get_post('bank_account'));
	$person_curr = $type == PT_CUSTOMER ? get_customer_currency(get_post('person_id')) 
		: ($type == PT_SUPPLIER ? get_supplier_currency(get_post('person_id')) : $bank_curr);

	if ($person_curr != $bank_curr)
	{
		$_POST['settled_amount'] =
			price_format(abs($order->gl_items_total() / get_exchange_rate_from_to($bank_curr, $person_curr, get_post('date_'))));
		amount_row($type == PT_CUSTOMER ? _("Settled AR Amount:") :  _("Settled AP Amount:"),
			 'settled_amount', null, null, $person_curr, user_price_dec());
	}

	if($tampil!=null)
		$codeedit=$tampil;
	//or $codeedit=='62002'
	/*
	if (strpos($codeedit, '62001') !== false or strpos($codeedit, '62003') !== false or strpos($codeedit, '62004') !== false or $tampil==1){
	//if($codeedit=='62001'  or $codeedit=='62003001' or $codeedit=='62003002' or $codeedit=='62003003' or $codeedit=='62003' or $codeedit=='62004' or $tampil==1){
		echo '<input type="hidden" name="marketing_sponsor" value="'.$codeedit.'">';
		//type lunch, dinner goods
		if($codeedit=='62001' or $codeedit=='62003' or $codeedit=='62003001' or $codeedit=='62003002' or $codeedit=='62003003' or $codeedit=='62004' ){
		echo '<tr>
				<td class="label">Bentuk dan Jenis Biaya</td>
				<td><input type="text" name="marketing_type" value="'.$_POST['marketing_type'].'"></td>
			</tr>';
		}else{
			echo '<tr>
					<td class="label">Type</td>
					<td>
						<select name="marketing_type">
						<option value="Lunch" '.($_POST['marketing_type']=='Lunch'?'selected':'').'>Lunch</option>
						<option value="Dinner" '.($_POST['marketing_type']=='Dinner'?'selected':'').'>Dinner</option>
						<option value="Goods" '.($_POST['marketing_type']=='Goods'?'selected':'').'>Goods</option>
						</select>
					</td>
				</tr>';
			//place
			echo '<tr>
					<td class="label">Place</td>
					<td><input type="text" name="marketing_place" value="'.$_POST['marketing_place'].'"></td>
				</tr>';
		}
		//address
		textarea_row(_("Address"), 'marketing_address', null, 50, 3);
		//name
		echo '<tr>
				<td class="label">Name</td>
				<td><input type="text" name="marketing_name" value="'.$_POST['marketing_name'].'"></td>
			</tr>';
		//npwp		
		if (strpos($codeedit, '62001') !== false or strpos($codeedit, '62003') !== false or strpos($codeedit, '62004') !== false){	
		echo '<tr>
				<td class="label">NPWP</td>
				<td><input type="text" name="npwp" value="'.$_POST['npwp'].'"></td>
			</tr>';
		echo '<tr>
				<td class="label">No Bukti Potong</td>
				<td><input type="text" name="bpotong" value="'.$_POST['bpotong'].'"></td>
			</tr>';

			if($codeedit=='62001' ){

				echo '<tr>
						<td class="label">No Surat Klien</td>
						<td><input type="text" name="sklien" value="'.$_POST['sklien'].'"></td>
					</tr>';
				echo '<tr>
						<td class="label">No Invoice</td>
						<td><input type="text" name="invoice" value="'.$_POST['invoice'].'"></td>
					</tr>';
				echo '<tr>
						<td class="label">No Surat KAM</td>
						<td><input type="text" name="ssyai" value="'.$_POST['ssyai'].'"></td>
					</tr>';
			}
		}else{
		//position
		echo '<tr>
				<td class="label">Position</td>
				<td><input type="text" name="marketing_position" value="'.$_POST['marketing_position'].'"></td>
			</tr>';
		//company
		echo '<tr>
				<td class="label">Name of Company</td>
				<td><input type="text" name="marketing_company" value="'.$_POST['marketing_company'].'"></td>
			</tr>';
		//Business type
		echo '<tr>
				<td class="label">Business Type</td>
				<td><input type="text" name="marketing_bussiness" value="'.$_POST['marketing_bussiness'].'"></td>
			</tr>';
		}
	}
	*/
	textarea_row(_("Description"), 'memo_', '', 50, 3);

  	echo "</table>";
  	div_end();
}


//---------------------------------------------------------------------------------

