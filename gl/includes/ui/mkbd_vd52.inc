<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function display_v2($type, $typename,$no)
{
	echo "<tr><th align=center>119</th><th colspan=6 style='font-weight:bold;' align=center>A</th><th align=center>B</th></tr>
	 <tr><th align=center>120</th><th colspan=6 style='font-weight:bold;' align=center>Nama Akun</th><th align=center >Saldo</th></tr>
	 
	 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>121</td>
  <td colspan=6 class=xl127 style=''>LIABILITAS</td>
  <td class=xl388 >&nbsp;</td>  
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>122</td>
  <td colspan=6 class=xl127 style=''>Utang Jangka Pendek</td>
  <td class=xl121 >&nbsp;</td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>123</td>
  <td colspan=6 class=xl127 style=''>Surat Utang Jangka Pendek</td>
  <td class=xl121 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>124</td>
  <td colspan=6 class=xl127 style=''>Utang Repo</td>
  <td class=xl389 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>125</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl89 >Repo Surat Berharga Negara</td>
  <td class=xl388 >&nbsp;</td>  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>126</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl89 >Repo Obligasi atau Sukuk Korporasi</td>
  <td class=xl388 >&nbsp;</td>  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>127</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl89 >Repo Efek Bersifat Ekuitas</td>
  <td class=xl388 >&nbsp;</td>  
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>128</td>
  <td colspan=6 class=xl127 >Utang Lembaga Kliring Penjaminan</td>
  <td class=xl388 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>129</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl89 >Utang Transaksi Bursa</td>
  <td class=xl388 >&nbsp;</td>  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>130</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl89 >Utang Komisi</td>
  <td class=xl388 >&nbsp;</td>  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>131</td>
  <td colspan=6 class=xl127 >Utang Nasabah</td>
  <td class=xl388 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>132</td>
  <td class=xl127 >&nbsp;</td>
  <td colspan=5 class=xl89 >Utang Nasabah Pemilik Rekening Efek</td>
  <td class=xl388 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>133</td>
  <td class=xl123 >&nbsp;</td>
  <td class=xl128 style=''>&nbsp;</td>
  <td colspan=4 class=xl89 >Transaksi Jual Efek</td>
  <td class=xl121 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>134</td>
  <td class=xl123 >&nbsp;</td>
  <td class=xl128 style=''>&nbsp;</td>
  <td colspan=4 class=xl89 >Saldo Kredit</td>
  <td class=xl121 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>135</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl89 >Utang Nasabah Kelembagaan</td>
  <td class=xl388 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>136</td>
  <td class=xl123 >&nbsp;</td>
  <td class=xl129 style=''>&nbsp;</td>
  <td colspan=4 class=xl89 >Transaksi Jual Efek</td>
  <td class=xl121 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>137</td>
  <td class=xl123 >&nbsp;</td>
  <td class=xl129 style=''>&nbsp;</td>
  <td colspan=4 class=xl89 >Gagal Terima - Nasabah Kelembagaan</td>
  <td class=xl121 >&nbsp;</td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>138</td>
  <td colspan=6 class=xl127 >Utang Perusahaan Efek Lain</td>
  <td class=xl388 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>139</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl89 >Uang Jaminan untuk Peminjaman Efek</td>
  <td class=xl121 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>140</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl86 width=405 style='width:305pt'>Uang Jaminan dari PE non Anggota Kliring</td>
  <td class=xl121 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>141</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl89 >Transaksi Beli Efek</td>
  <td class=xl121 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>142</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl89 >Gagal Terima - Perusahaan Efek</td>
  <td class=xl121 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>143</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl89 >Utang Komisi</td>
  <td class=xl121 >&nbsp;</td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>144</td>
  <td colspan=6 class=xl127 >Utang Kegiatan Penjaminan Emisi Efek</td>
  <td class=xl388 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>145</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl256 width=405 style='width:305pt'>Utang Dalam Rangka Penawaran Umum/Penawaran Terbatas</td>
  <td class=xl388 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>146</td>
  <td class=xl123 >&nbsp;</td>
  <td class=xl130 width=28 style=';width:21pt'>&nbsp;</td>
  <td colspan=4 class=xl86 width=238 style=';width:179pt'>Utang Nasabah Umum</td>
  <td class=xl388 >&nbsp;</td>  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>147</td>
  <td class=xl123 >&nbsp;</td>
  <td class=xl130 width=28 style=';width:21pt'>&nbsp;</td>
  <td colspan=4 class=xl86 width=238 style=';width:179pt'>Utang Emiten</td>
  <td class=xl388 >&nbsp;</td>  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>148</td>
  <td class=xl123 >&nbsp;</td>
  <td class=xl130 width=28 style=';width:21pt'>&nbsp;</td>
  <td colspan=4 class=xl86 width=238 style=';width:179pt'>Utang Kepada Penerbit Efek</td>
  <td class=xl123 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>149</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl89 >Utang JasaEmisi Efek</td>
  <td class=xl123 >&nbsp;</td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>150</td>
  <td colspan=6 class=xl127 >Utang Kegiatan Manajer Investasi</td>
  <td class=xl388 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>151</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl89 >Utang Komisi Agen Penjual</td>
  <td class=xl123 >&nbsp;</td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>152</td>
  <td class=xl123 colspan=6 style='mso-ignore:colspan'>Utang Transaksi Beli Efek Lainnya</td>
  <td class=xl89 style=''>&nbsp;</td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>153</td>
  <td colspan=6 class=xl127 >Utang Efek Posisi Short – Sendiri </td>
  <td class=xl388 >&nbsp;</td>
 </tr>
 <tr class=xl119 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>154</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl86 width=405 style='width:305pt'>Surat Berharga Negara</td>
  <td class=xl123 >&nbsp;</td>
 </tr>
 <tr class=xl119 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;'>155</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl286 width=405 style='width:305pt'>
  	Efek Bersifat Utang yang tercatat di Bursa Efek di Indonesia
  </td>
  <td class=xl123 >&nbsp;</td>
 </tr>
 <tr class=xl119 height=60 style='mso-height-source:userset;height:45.0pt'>
  <td height=60 class=xl161 style='height:45.0pt;'>156</td>
  <td class=xl131 >&nbsp;</td>
  <td colspan=5 class=xl286 width=405 style='width:305pt'>
  Efek Bersifat Ekuitas yang tercatat di Bursa Efek di Indonesia, atau Reksa Dana yang Unit Penyertaannya diperdagangkan di Bursa Efek di Indonesia
  </td>
  <td class=xl123 >&nbsp;</td>
 </tr>
 <tr class=xl119 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>157</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl86 width=405 style='width:305pt'>Efek Lain yang Terdaftar di Bapepam dan LK</td>
  <td class=xl123 >&nbsp;</td>
 </tr>
 <tr class=xl119 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>158</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl86 width=405 style='width:305pt'>Efek Luar Negeri</td>
  <td class=xl123 >&nbsp;</td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>159</td>
  <td colspan=6 class=xl127 >Utang Jangka Pendek Lainnya</td>
  <td class=xl125 >0</td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>160</td>
  <td colspan=6 class=xl127 >Utang Jangka Panjang</td>
  <td class=xl125 >0</td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>161</td>
  <td colspan=6 class=xl127 >Utang Obligasi</td>
  <td class=xl389 >&nbsp;</td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>162</td>
  <td colspan=6 class=xl127 >Utang Lain-lain</td>
  <td class=xl125 >0 </td>
 </tr>
 
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>164</td>
  <td colspan=6 class=xl127 style='border-left:  none'>TOTAL LIABILITAS</td>
  <td class=xl496 >0 </td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>165</td>
  <td colspan=6 class=xl127 >EKUITAS</td>
  <td class=xl389 >&nbsp;</td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>166</td>
  <td colspan=6 class=xl540 width=433 >Ekuitas Yang Dapat Diatribusikan Kepada Pemilik Entitas Induk</td>
  <td class=xl389 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>167</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl86 width=405 style='width:305pt'>Modal Saham</td>
  <td class=xl132 >0</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>168</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl86 width=405 style='width:305pt'>Tambahan Modal Disetor</td>
  <td class=xl132 >0</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>169</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl89 >Ekuitas  Lainnya</td>
  <td class=xl132 >&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>170</td>
  <td class=xl123 >&nbsp;</td>
  <td colspan=5 class=xl89 >Saldo Laba</td>
  <td class=xl132 >0</td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>171</td>
  <td class=xl123 colspan=6 style='mso-ignore:colspan'>Kepentingan Non  Pengendali</td>
  <td class=xl132 >&nbsp;</td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>172</td>
  <td colspan=6 class=xl127 style=''>TOTAL EKUITAS</td>
  <td class=xl132 >0</td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl120 style='height:15.0pt;'>173</td>
  <td colspan=6 class=xl127 >TOTAL LIABILITAS DAN EKUITAS</td>
  <td class=xl497 >0</td>
  <tr>
	 ";
}


//---------------------------------------------------------------------------------

