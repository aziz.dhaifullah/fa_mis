<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

//include_once($path_to_root . "/gl/includes/gl_db.inc");
function display_v1($type, $typename,$no)
{
	echo "<tr><th align=center>6</th><th colspan=6 style='font-weight:bold;' align=center>A</th><th align=center>B</th></tr>
	 <tr><th align=center>7</th><th colspan=6 style='font-weight:bold;' align=center>Nama Akun</th><th align=center >Saldo</th></tr>
	 <tr><td>8</td><td colspan=6 style='font-weight:bold;'>Aset Lancar</td><td>&nbsp;</td></tr>
	 
	 <tr><td>9</td><td colspan=6 style='font-weight:bold;'>Kas dan Setara Kas</td><td>&nbsp;</td></tr>";

	$accounts=get_gl_accounts(false, false, '1-1100');
	$bank=0;
	while($row = db_fetch_row($accounts)){
		//print_r($row);
	 	$bank=sum_bal_account(date('Y-m-d'), $row[0]);
	}
	 echo "<tr><td>10</td><td width=10>&nbsp;</td><td colspan=5>Simpanan Giro Bank</td><td align='right'>".price_format($bank)."</td></tr>";
	 echo "
	 <tr><td>11</td><td colspan=6 style='font-weight:bold;'>Kas yang Dibatasi Penggunaannya</td><td>&nbsp;</td></tr>
	 <tr><td>12</td><td>&nbsp;</td><td colspan=5>Kas yang Dipisahkan</td><td>&nbsp;</td></tr>
	 <tr><td>13</td><td>&nbsp;</td><td colspan=5>Rekening qq. Efek Nasabah</td><td>&nbsp;</td></tr>
	 
	 <tr><td>14</td><td colspan=6 style='font-weight:bold;'>Deposito Berjangka</td><td>&nbsp;</td></tr>
	 <tr><td>15</td><td>&nbsp;</td><td colspan=5>Deposito Bank Dalam Negeri</td><td>&nbsp;</td></tr>
	 <tr><td>16</td>
	 	<td>&nbsp;</td>
	 	<td>&nbsp;</td>";
	$accounts=get_gl_accounts(false, false, '1-1200');
	$dep=0;
	while($row = db_fetch_row($accounts)){
		//print_r($row);
	 	$dep=sum_bal_account(date('Y-m-d'), $row[0]);
	}
	 echo "
	 	<td colspan=4>Deposito Bank Umum dengan jangka waktu kurang atau sama dengan 3 (tiga) bulan</td><td>".price_format($dep)."</td>
	 </tr>
	 <tr><td>17</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Deposito Bank Umum dengan jangka waktu lebih dari 3 (tiga) bulan</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr><td>18</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Dijamin oleh Lembaga Penjamin Simpanan</td>
	  <td>0 </td>
	 </tr>
	 <tr>
	  <td>19</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Tidak Dijamin oleh Lembaga Penjamin Simpanan</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>20</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=3>Tidak sedang diajukan pailit, tidak dinyatakan pailit, atau tidak dalam proses likuidasi</td>
	  <td>0 </td>
	 </tr>
	 <tr>
	  <td>21</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=3>Sedang diajukan pailit,dinyatakan pailit, atau  dalam proses likuidasi</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr><td>22</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Deposito pada Bank Perkreditan Rakyat</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>23</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Deposito Bank di Luar Negeri</td>
	  <td>&nbsp;</td>
	 </tr>
	 
	 <tr>
	  <td>24</td>
	  <td colspan=6 style='font-weight:bold;'>Piutang Reverse Repo</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>25</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Reverse Repo Surat Berharga Negara</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>26</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Reverse Repo Obligasi atau Sukuk Korporasi</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>27</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Reverse Repo Efek Bersifat Ekuitas</td>
	  <td>&nbsp;</td>
	 </tr>

	 <tr>
	  <td>28</td>
	  <td colspan=6 style='font-weight:bold;'>Piutang Lembaga Kliring dan Penjaminan</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>29</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Uang Jaminan Lembaga Kliring dan Penjaminan</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>30</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang Transaksi Bursa</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>31</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang Komisi</td>
	  <td>&nbsp;</td>
	 </tr>

	 <tr>
	  <td>32</td>
	  <td colspan=6 style='font-weight:bold;'>Piutang Nasabah</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>
	  33</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang Nasabah Pemilik Rekening Efek</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>34</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Transaksi Beli Efek</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>35</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Saldo Debit Rekening Efek Nasabah</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>36</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang Nasabah Umum</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>37</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang Nasabah Kelembagaan</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>38</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Transaksi Beli Efek</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>39</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Gagal Serah - Nasabah Kelembagaan</td>
	  <td>&nbsp;</td>
	 </tr>

	 <tr>
	  <td>40</td>
	  <td colspan=6 style='font-weight:bold;'>Piutang Perusahaan Efek Lain</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>41</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Uang Jaminan untuk Peminjaman Efek</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>42</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Uang Jaminan pada Anggota Kliring</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>43</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Transaksi Jual Efek</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>44</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Gagal Serah - Perusahaan Efek</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>45</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang Komisi</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>46</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Dana Pesanan Efek Dibayar Dimuka</td>
	  <td>&nbsp;</td>
	 </tr>

	 <tr>
	  <td>47</td>
	  <td colspan=6 style='font-weight:bold;'>Piutang Kegiatan Penjaminan Emisi Efek</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>48</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang Jasa Emisi Efek</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>49</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang Jasa <font >Arranger</font><font> </font><font >Penerbitan Efek</font></td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>50</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang Jasa Penasihat Keuangan</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>51</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang Biaya Talangan - Penjamin Emisi Efek</td>
	  <td>&nbsp;</td>
	 </tr>

	 <tr>
	  <td>52</td>
	  <td colspan=6 style='font-weight:bold;'>Piutang Kegiatan Manajer Investasi</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>53</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang Manajemen Fee</td>
	  <td>0</td>
	 </tr>
	 <tr>
	  <td>54</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang Selling Fee dan Redemption Fee</td>
	  <td>0 </td>
	 </tr>
	 <tr>
	  <td>55</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang Biaya Talangan – Manajer Investasi</td>
	  <td>&nbsp;</td>
	 </tr>

	 <tr>
	  <td>56</td>
	  <td colspan=6 style='font-weight:bold;'>Piutang Transaksi Jual Efek Lainnya</td>
	  <td>&nbsp;</td>
	 </tr>

	 <tr>
	  <td>57</td>
	  <td colspan=6 style='font-weight:bold;'>Piutang Dividen dan Bunga</td>
	  <td>0 </td>
	 </tr>

	 <tr>
	  <td>58</td>
	  <td colspan=6 style='font-weight:bold;'>Portofolio Efek</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>59</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Sertifikat Bank Indonesia</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>60</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Surat Berharga Negara</td>
	  <td class=xl385 >&nbsp;</td>
	 </tr>
	 <tr>
	  <td>61</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>0-7 tahun</td>
	  <td>0 </td>
	 </tr>
	 <tr>
	  <td>62</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>7-15 tahun</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>63</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>15 tahun ke atas</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr class=xl95 >
	  <td>64</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Obligasi Korporasi, Sukuk Korporasi, atau Efek Beragun Aset Arus Kas Tetap yang tercatat di Bursa Efek di Indonesia</td>
	  <td class=xl385 >&nbsp;</td>
	 </tr>
	 <tr>
	  <td>65</td>
	  <td>&nbsp;</td>
	  <td class=xl98 >&nbsp;</td>
	  <td colspan=4>Peringkat setara dengan AAA</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr class=xl95 >
	  <td>66</td>
	  <td>&nbsp;</td>
	  <td class=xl98 >&nbsp;</td>
	  <td colspan=4>Peringkat setara dengan AA hingga kurang dari setara dengan AAA</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr class=xl95 >
	  <td>67</td>
	  <td>&nbsp;</td>
	  <td class=xl98 >&nbsp;</td>
	  <td colspan=4>Peringkat setara dengan A atau hingga kurang dari setara dengan AA</td>
	  <td class=xl99 >&nbsp;</td>
	 </tr>
	 <tr class=xl95 >
	  <td class=xl85 >68</td>
	  <td>&nbsp;</td>
	  <td class=xl98 >&nbsp;</td>
	  <td colspan=4>Peringkat setara dengan BBB- atau hingga kurang dari setara dengan A</td>
	  <td class=xl99 >&nbsp;</td>
	 </tr>
	 <tr>
	  <td>69</td>
	  <td>&nbsp;</td>
	  <td class=xl98 >&nbsp;</td>
	  <td colspan=4>Peringkat kurang dari setara dengan BBB-</td>
	  <td class=xl99 >&nbsp;</td>
	 </tr>
	 <tr height=55 >
	  <td >70</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Efek Bersifat Ekuitas atau Efek Beragun Aset Arus Kas Tidak<font> </font>
	  <font >Tetap yang tercatat di Bursa Efek di Indonesia dan Reksa Dana yang Unit Penyertaannya diperdagangkan di Bursa Efek di Indonesia</font></td>
	  <td class=xl386 >&nbsp;</td>
	 </tr>
	 <tr>
	  <td>71</td>
	  <td>&nbsp;</td>
	  <td class=xl256 >&nbsp;</td>
	  <td colspan=4>Haircut Komite 5% dan 10%</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>72</td>
	  <td>&nbsp;</td>
	  <td class=xl256 >&nbsp;</td>
	  <td colspan=4>Haircut Komite 15% dan 20%</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>73</td>
	  <td>&nbsp;</td>
	  <td class=xl256 >&nbsp;</td>
	  <td colspan=4>Haircut Komite 25%</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>74</td>
	  <td>&nbsp;</td>
	  <td class=xl256 >&nbsp;</td>
	  <td colspan=4>Haircut Komite 30%</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>75</td>
	  <td>&nbsp;</td>
	  <td class=xl256 >&nbsp;</td>
	  <td colspan=4>Haircut Komite 35%</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>76</td>
	  <td>&nbsp;</td>
	  <td class=xl256 >&nbsp;</td>
	  <td colspan=4>Haircut Komite 40%</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>77</td>
	  <td>&nbsp;</td>
	  <td class=xl256 >&nbsp;</td>
	  <td colspan=4>Haircut Komite 45%</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>78</td>
	  <td>&nbsp;</td>
	  <td class=xl256 >&nbsp;</td>
	  <td colspan=4>Haircut Komite 50%</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>79</td>
	  <td>&nbsp;</td>
	  <td class=xl256 >&nbsp;</td>
	  <td colspan=4>Haircut Komite 55% sd 80%</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>80</td>
	  <td>&nbsp;</td>
	  <td class=xl256 >&nbsp;</td>
	  <td colspan=4>Haircut Komite 85% sd100%</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>81</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Efek Bersifat Ekuitas yang tidak lagi tercatat <font></font><font>pada Bursa Efek di Indonesia </font><font >(delist)</font></td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>82</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Efek Luar Negeri</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>83</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Unit Penyertaan Reksa Dana</td>
	  <td class=xl386 >&nbsp;</td>
	 </tr>
	 <tr class=xl100 >
	  <td>84</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Pasar uang</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr class=xl100 >
	  <td>85</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Terproteksi</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr class=xl100 >
	  <td>86</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Dengan Penjaminan</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>87</td>
	  <td class=xl101 >&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Pendapatan tetap</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>88</td>
	  <td class=xl102 >&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Campuran atau Saham</td>
	  <td class=xl103 >&nbsp;</td>
	 </tr>
	 <tr>
	  <td>89</td>
	  <td class=xl102 >&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Indeks</td>
	  <td class=xl103 >&nbsp;</td>
	 </tr>
	 <tr>
	  <td>90</td>
	  <td class=xl102 >&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Penyertaan Terbatas</td>
	  <td class=xl103 >&nbsp;</td>
	 </tr>
	 <tr>
	  <td>91</td>
	  <td class=xl102 >&nbsp;</td>
	  <td colspan=5>Investasi yang Dikelola oleh Perusahaan Efek Lain</td>
	  <td class=xl103 >&nbsp;</td>
	 </tr>
	 <tr>
	  <td>92</td>
	  <td class=xl104 >&nbsp;</td>
	  <td colspan=5>Unit Penyertaan Dana Investasi Real Estat</td>
	  <td class=xl103 >&nbsp;</td>
	 </tr>
	 <tr>
	  <td>93</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Kontrak Opsi</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>94</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Kontrak Berjangka</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>95</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Efek Lain selain baris 59 sampai dengan baris 94 yang Terdaftar di Bapepam dan LK</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>96</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Efek Repo/Dipinjamkan/Dijaminkan</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>97</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Surat Berharga Negara</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>98</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Obligasi atau Sukuk Korporasi</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>99</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td colspan=4>Efek Bersifat Ekuitas</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>100</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Total Aset Lancar :</td>
	  <td class=xl498 >0 </td>
	 </tr>

	 <tr>
	  <td>101</td>
	  <td colspan=6 style='font-weight:bold;'>Aset Keuangan Lainnya</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>102</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang Kepada Pihak Istimewa lainnya</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr >
	  <td class=xl85 >103</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang Nasabah Pemilik Rekening Efek untuk transaksi beli Efek sejak tanggal penyelesaian transaksi</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>104</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Piutang lainnya</td>
	  <td>0 </td>
	 </tr>
	 <tr>
	  <td>105</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Pajak dibayar di muka</td>
	  <td>0 </td>
	 </tr>
	 <tr>
	  <td>106</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Biaya dibayar di muka</td>
	  <td>0 </td>
	 </tr>
	 <tr>
	  <td>107</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Jaminan lainnya</td>
	  <td>0 </td>
	 </tr>

	 <tr>
	  <td>108</td>
	  <td colspan=6 style='font-weight:bold;'>Investasi Jangka Panjang</td>
	  <td>&nbsp;</td>
	 </tr>

	 <tr>
	  <td>109</td>
	  <td colspan=6 style='font-weight:bold;'>Aset Tetap</td>
	  <td>0 </td>
	 </tr>
	 <tr>
	  <td>110</td>
	  <td colspan=6 style='font-weight:bold;'>Aset Pajak Tangguhan</td>
	  <td>&nbsp;</td>
	 </tr>

	 <tr>
	  <td>111</td>
	  <td colspan=6 style='font-weight:bold;'>Aset Lain - lain</td>
	  <td>&nbsp;</td>
	 </tr>
	 <tr>
	  <td>112</td>
	  <td>&nbsp;</td>
	  <td colspan=5>Total Aset Tetap dan Aset Lainnya :</td>
	  <td class=xl498 >0 </td>
	 </tr>
	 <tr>
	  <td>113</td>
	  <td>&nbsp;</td>
	  <td colspan=5>TOTAL ASET</td>
	  <td class=xl499 > 0 </td>
	 </tr>";
}


//---------------------------------------------------------------------------------

