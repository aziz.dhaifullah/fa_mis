<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function display_v3($type, $typename,$no)
{
	echo "
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl173 style='height:15.0pt'>6</td>
  <td colspan=6 class=xl554 style='
  border-left:none;'>A</td>
  <td class=xl174 style='border-left:none'>B</td>
  <td class=xl174 style='border-top:none;border-left:none;'>C</td>
  
 </tr>
 <tr height=65 style='mso-height-source:userset;height:48.75pt'>
  <td height=65 class=xl176  style='height:48.75pt;border-top:none;
  '>7</td>
  <td colspan=6 class=xl176  style='
  '>Ranking Liabilities</td>
  <td class=xl474  style='border-top:none;'>Nilai Ranking
  Liabilities<span style='mso-spacerun:yes'> </span></td>
  <td class=xl83  style='border-top:none;'>Persentase yang
  ditambahkan sebagai Liabilitas</td>
  
 </tr>
 <tr class=xl165 height=19 style='mso-height-source:userset;height:14.25pt'>
  <td height=19 class=xl177 style='height:14.25pt;border-top:none'>8</td>
  <td colspan=6 class=xl551  style=''>Ranking Liabilities</td>
  <td class=xl393  style='border-top:none;'>&nbsp;</td>
  <td class=xl392  style='border-top:none;'>&nbsp;</td>
  
 </tr>
 <tr class=xl165 height=60 style='mso-height-source:userset;height:45.0pt'>
  <td height=60 class=xl177 style='height:45.0pt;border-top:none'>9</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Transaksi Reverse Repo atas Surat Berharga Negara</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>Kekurangan
  Nilai Pasar Wajar terhadap 105% dari nilai penjualan kembali<span
  style='mso-spacerun:yes'> </span></td>
  
 </tr>
 <tr class=xl165 height=60 style='mso-height-source:userset;height:45.0pt'>
  <td height=60 class=xl177 style='height:45.0pt;border-top:none'>10</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Transaksi Reverse Repo atas Obligasi atau Sukuk Korporasi<span
  style='mso-spacerun:yes'> </span></td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>Kekurangan
  Nilai Pasar Wajar terhadap 110% dari nilai penjualan kembali<span
  style='mso-spacerun:yes'> </span></td>
  
 </tr>
 <tr class=xl165 height=60 style='mso-height-source:userset;height:45.0pt'>
  <td height=60 class=xl177 style='height:45.0pt;border-top:none'>11</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Transaksi Reverse Repo atas Efek Bersifat Ekuitas</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>Kekurangan
  Nilai Pasar Wajar terhadap 120% dari nilai penjualan kembali<span
  style='mso-spacerun:yes'> </span></td>
  
 </tr>
 <tr class=xl165 height=38 style='mso-height-source:userset;height:28.5pt'>
  <td height=38 class=xl177 style='height:28.5pt;border-top:none'>12</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Transaksi<span style='mso-spacerun:yes'>  </span>Repo atas Surat
  Berharga Negara</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>1% dari nilai
  pembelian kembali<span style='mso-spacerun:yes'> </span></td>
  
 </tr>
 <tr class=xl165 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl177 style='height:30.0pt;border-top:none'>13</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Transaksi<span style='mso-spacerun:yes'>  </span>Repo atas
  Obligasi atau Sukuk Korporasi<span style='mso-spacerun:yes'> </span></td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>3% dari nilai
  pembelian kembali<span style='mso-spacerun:yes'> </span></td>
  
 </tr>
 <tr class=xl165 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>14</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Transaksi<span style='mso-spacerun:yes'>  </span>Repo atas Efek
  Bersifat Ekuitas</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>5% dari nilai
  pembelian kembali<span style='mso-spacerun:yes'> </span></td>
  
 </tr>
 <tr class=xl165 height=60 style='mso-height-source:userset;height:45.0pt'>
  <td height=60 class=xl177 style='height:45.0pt;border-top:none'>15</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Kontrak Penjaminan atas Pernyataan Pendaftaran yang telah
  Efektif dari Bapepam dan LK</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>25% dari
  komitmen penjaminan yang menjadi porsinya</td>
  
 </tr>
 <tr class=xl165 height=80 style='mso-height-source:userset;height:60.0pt'>
  <td height=80 class=xl177 style='height:60.0pt;border-top:none'>16</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Kontrak Penjaminan dalam Proses Penawaran Hingga Penjatahan</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>50% dari nilai
  yang belum dipesan atau setinggi-tingginya 25% dari penjaminan yang menjadi
  porsinya</td>
  
 </tr>
 <tr class=xl165 height=60 style='mso-height-source:userset;height:45.0pt'>
  <td height=60 class=xl177 style='height:45.0pt;border-top:none'>17</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Kontrak Penjaminan dalam Proses Penjatahan Hingga Pencatatan</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>100% dari nilai
  yang unsubscribe yang menjadi porsinya</td>
  
 </tr>
 <tr class=xl165 height=60 style='mso-height-source:userset;height:45.0pt'>
  <td height=60 class=xl177 style='height:45.0pt;border-top:none'>18</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Perusahaan Efek Bertindak Sebagai Pembeli Siaga</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>50% dari
  Haircut Efek dikalikan harga Penawaran yang menjadi porsinya</td>
  
 </tr>
 <tr class=xl166 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>19</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Jaminan oleh Perusahaan</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl181  style='border-top:none;'>20% dari nilai
  jaminan</td>
 </tr>
 <tr class=xl165 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl177 style='height:30.0pt;border-top:none'>20</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Komitmen Belanja Modal</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl181  style='border-top:none;'>20% dari nilai
  komitmen di atas Rp150,000,000</td>
  
 </tr>
 <tr class=xl165 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl177 style='height:30.0pt;border-top:none'>21</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Kerugian Berjalan dari<span style='mso-spacerun:yes'> 
  </span>Transaksi dalam Mata Uang Asing</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>100% dari
  kerugian berjalan</td>
  
 </tr>
 <tr class=xl165 height=24 style='mso-height-source:userset;height:18.0pt'>
  <td height=24 class=xl177 style='height:18.0pt;border-top:none'>22</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Risiko Terkonsentrasinya Portofolio Efek</td>
  <td class=xl393  style='border-top:none;'>&nbsp;</td>
  <td class=xl392  style='border-top:none;'>&nbsp;</td>
  
 </tr>
 <tr class=xl165 height=157 style='mso-height-source:userset;height:117.75pt'>
  <td height=157 class=xl177 style='height:117.75pt;border-top:none'>23</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td class=xl182  style='border-top:none;'>&nbsp;</td>
  <td colspan=4 class=xl562 2 style=''>Efek Bersifat Ekuitas, Efek Bersifat Utang, atau Sukuk yang Diterbitkan oleh Satu Emiten</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>kelebihan atas
  20% Total Modal Sendiri</td>
  
 </tr>
 <tr class=xl165 height=90 style='mso-height-source:userset;height:67.5pt'>
  <td height=90 class=xl177 style='height:67.5pt;border-top:none'>24</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td class=xl182  style='border-top:none;'>&nbsp;</td>
  <td colspan=4 class=xl560 2 style='
  '><span style='mso-spacerun:yes'> </span><font
  >Efek Bersifat Ekuitas, Efek Bersifat Utang, dan<span
  style='mso-spacerun:yes'>  </span>Sukuk yang Diterbitkan oleh Satu Emiten</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl447  style='border-top:none;'>kelebihan atas
  20% Total Modal Sendiri</td>
  
 </tr>
 <tr class=xl165 height=126 style='mso-height-source:userset;height:94.5pt'>
  <td height=126 class=xl177 style='height:94.5pt;border-top:none'>25</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td class=xl182  style='border-top:none;'>&nbsp;</td>
  <td colspan=4 class=xl557 2 style='
  '>Efek Bersifat Ekuitas, Efek Bersifat Utang,
  dan/atau<span style='mso-spacerun:yes'>  </span>Sukuk yang Diterbitkan
  oleh<span style='mso-spacerun:yes'>  </span>Beberapa Emiten dalam Satu Grup
  Perusahaan (Holding Company<font
  >)</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>kelebihan atas
  20% Total Modal Sendiri</td>
  
 </tr>
 <tr class=xl165 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl177 style='height:30.0pt;border-top:none'>26</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td class=xl182  style='border-top:none;'>&nbsp;</td>
  <td colspan=4 class=xl286 2 style='
  '>Surat Berharga Negara</td>
  <td class=xl521  style='border-top:none;'>-</td>
  <td class=xl180  style='border-top:none;'>kelebihan atas
  40% Total Modal Sendiri</td>
  
 </tr>
 <tr class=xl165 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl177 style='height:30.0pt;border-top:none'>27</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td class=xl183  style='border-top:none;'>&nbsp;</td>
  <td colspan=4 class=xl286 2 style='
  '>Efek Reksa Dana</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>nilai sesuai
  Formulir V.D.5-4 Kolom H</td>
  
 </tr>
 <tr class=xl165 height=80 style='mso-height-source:userset;height:60.0pt'>
  <td height=80 class=xl177 style='height:60.0pt;border-top:none'>28</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Risiko Terkonsentrasinya Pembiayaan Penyelesaian Transaksi
  kepada Satu Nasabah atau Beberapa Nasabah yang Saling memiliki hubungan
  afiliasi</td>
  <td class=xl179  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>kelebihan atas
  10% dari nilai pembiayaan total yang diberikan</td>
  
 </tr>
 <tr class=xl165 height=60 style='mso-height-source:userset;height:45.0pt'>
  <td height=60 class=xl177 style='height:45.0pt;border-top:none'>29</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Kelebihan Pembiayaan Marjin terhadap Jaminan Pembiayaan</td>
  <td class=xl183  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>kelebihan
  pembiayaan diatas 80% dari Jaminan Pembiayaan Transaksi Marjin</td>
  
 </tr>
 <tr class=xl165 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl177 style='height:30.0pt;border-top:none'>30</td>
  <td class=xl178  style='border-top:none;'>&nbsp;</td>
  <td colspan=5 class=xl286  style='
  '>Kekurangan Jaminan Transaksi Short Selling terhadap Posisi Short</td>
  <td class=xl183  style='border-top:none;'>&nbsp;</td>
  <td class=xl180  style='border-top:none;'>kekurangan
  nilai jaminan di bawah 120% dari Posisi Short</td>
  
 </tr>
 <tr class=xl165 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>31</td>
  <td colspan=6 class=xl563  style=''>Total Ranking
  Liabilities</td>
  <td class=xl184  style='border-top:none;border-left:none;'>-</td>
  <td class=xl394  style='border-top:none;border-left:none;'>&nbsp;</td>
  
 </tr>
 <tr class=xl165 height=47 style='mso-height-source:userset;height:35.25pt'>
  <td colspan=9 height=47 class=xl187 style='height:35.25pt;
  '>Pengisian nilai Ranking Liabilities yang ditambahkan dalam Kolom
  B didasarkan pada data Formulir V.D.5-10</td>
  
 </tr>
	 ";
}


//---------------------------------------------------------------------------------

