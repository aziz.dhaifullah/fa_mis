<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function display_v6($type, $typename,$no)
{
	echo "
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl246 style='height:15.0pt;width: 5%;'>6</td>
  <td colspan=3 class=xl580 style='border-left:
  none'>A</td>
  <td class=xl250 style='border-left:none'>B</td>
  <td class=xl249>C</td>
  <td class=xl251>D</td>
  <td class=xl250>E</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl252 style='height:30.0pt;border-top:none'>7</td>
  <td colspan=3 class=xl582 style='
  border-left:none;'>SALDO DEBIT BUKU PEMBANTU DANA</td>
  <td class=xl376 style='border-top:none;border-left:none'>Saldo</td>
  <td class=xl376 style='border-top:none;border-left:none'>Terafiliasi</td>
  <td class=xl377 width=95 style='border-top:none;border-left:none;width:71pt'>Tidak
  Terafiliasi</td>
  <td class=xl313 style='border-top:none'>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>8</td>
  <td colspan=3 class=xl253 style='
  border-left:none;'>Dana Milik Perusahaan Efek</td>
  <td class=xl518 style='border-left:none'> 0 </td>
  <td class=xl396>&nbsp;</td>
  <td class=xl397>&nbsp;</td>
  <td class=xl398>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>9</td>
  <td colspan=3 class=xl253 style='
  border-left:none;'>Dana Milik Nasabah Pemilik Rekening</td>
  <td class=xl399 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  <td class=xl397>&nbsp;</td>
  <td class=xl398>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>10</td>
  <td class=xl256 width=18 style='border-top:none;width:14pt'>&nbsp;</td>
  <td colspan=2 class=xl286 width=270 style='
  width:203pt'>Dana Bebas</td>
  <td class=xl254 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl258>&nbsp;</td>
  <td class=xl259>&nbsp;</td>
  <td class=xl398>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>11</td>
  <td class=xl260 width=18 style='width:14pt'>&nbsp;</td>
  <td colspan=2 class=xl183 width=270 style='
  width:203pt'>Dana yang Dijaminkan</td>
  <td class=xl254 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl258>&nbsp;</td>
  <td class=xl259>&nbsp;</td>
  <td class=xl398>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>12</td>
  <td colspan=3 class=xl253 style='
  border-left:none;'>Dana Milik Nasabah Umum</td>
  <td class=xl398 style='border-left:none'>&nbsp;</td>
  <td class=xl398 style='border-left:none'>&nbsp;</td>
  <td class=xl398 style='border-left:none'>&nbsp;</td>
  <td class=xl398 style='border-left:none'>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>13</td>
  <td class=xl253 width=18 style='border-top:none;border-left:none;width:14pt'>&nbsp;</td>
  <td colspan=2 class=xl256 width=270 style='
  width:203pt'>Dana Pemesanan Efek</td>
  <td class=xl254 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl258>&nbsp;</td>
  <td class=xl259>&nbsp;</td>
  <td class=xl398>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr class=xl135 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>14</td>
  <td colspan=3 class=xl253 style='
  border-left:none;'>Selisih Dana Positif</td>
  <td class=xl254 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  <td class=xl397>&nbsp;</td>
  <td class=xl398>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>15</td>
  <td class=xl262 width=18 style='width:14pt'>&nbsp;</td>
  <td colspan=2 class=xl286 width=270 style='
  width:203pt'>Total Debit</td>
  <td class=xl254 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  <td class=xl397>&nbsp;</td>
  <td class=xl398>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr class=xl244 height=42 style='mso-height-source:userset;height:31.9pt'>
  <td height=42 class=xl298 style='height:31.9pt'>16 </td>
  <td colspan=3 class=xl554 style='
  border-left:none;'>SALDO KREDIT BUKU PEMBANTU DANA</td>
  <td class=xl373 style='border-left:none'>Saldo</td>
  <td class=xl373 style='border-left:none'>Dimiliki</td>
  <td class=xl374 style='border-left:none'>Dipisahkan</td>
  <td class=xl375 width=94 style='width:71pt'>Tidak Dipisahkan</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl255 style='height:30.0pt'>17</td>
  <td colspan=3 class=xl323 style='
  border-left:none;'>Dana yang disimpan di Unit Kerja yang Menjalankan Fungsi Pembukuan</td>
  <td class=xl263 style='border-left:none'>0</td>
  <td class=xl264>0 </td>
  <td class=xl396>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>18</td>
  <td colspan=3 class=xl571 style='border-left:
  none'>Dana yang disimpan pada Bank</td>
  <td class=xl396>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>19</td>
  <td class=xl265 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl256 width=270 style='
  width:203pt'>Dana Milik Perusahaan Efek</td>
  <td class=xl518 style='border-top:none;border-left:none'>0 </td>
  <td class=xl258>0 </td>
  <td class=xl396>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>20</td>
  <td class=xl265 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl256 width=270 style='
  width:203pt'>Dana Milik Nasabah Pemilik Rekening</td>
  <td class=xl254 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  <td class=xl378 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl379>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>21</td>
  <td class=xl265 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl256 width=270 style='
  width:203pt'>Dana Milik Nasabah Umum</td>
  <td class=xl254 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  <td class=xl399 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl399 style='border-top:none;border-left:none'>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>22</td>
  <td colspan=3 class=xl253 style='
  border-left:none;'>Selisih Dana Negatif</td>
  <td class=xl266 style='border-left:none'>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>23</td>
  <td class=xl260 width=18 style='width:14pt'>&nbsp;</td>
  <td colspan=2 class=xl286 width=270 style='
  width:203pt'>Total Kredit</td>
  <td class=xl267 style='border-left:none'>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  <td class=xl396>&nbsp;</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>24</td>
  <td class=xl244 colspan=3 style='mso-ignore:colspan'>Rincian Saldo masing-masing Rekening Bank</td>
  
  
  <td class=xl147></td>
  <td class=xl147></td>
  <td class=xl147></td>
  <td class=xl147></td>
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr class=xl244 height=46 style='mso-height-source:userset;height:34.5pt'>
  <td height=46 class=xl311 style='height:34.5pt'>&nbsp;</td>
  <td colspan=2 class=xl578 style='border-left:
  none'>Nama Bank</td>
  <td class=xl312 width=60 style='border-left:none;width:45pt'>Sendiri/ Nasabah</td>
  <td class=xl312 width=120 style='border-left:none;width:90pt'>Nomor Rekening</td>
  <td class=xl312 width=95 style='border-left:none;width:71pt'>Mata Uang</td>
  <td class=xl312 width=95 style='border-left:none;width:71pt'>Saldo</td>
  <td class=xl480 width=94 style='width:71pt'>Saldo <br>(dalam Rp)</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>&nbsp;</td>
  <td colspan=2 class=xl569 style='border-left:
  none'>BII</td>
  <td class=xl519 style='border-left:none'>S</td>
  <td class=xl500 style='border-left:none'></td>
  <td class=xl513>IDR</td>
  <td class=xl514 style='border-top:none;border-left:none'>0</td>
  <td class=xl514 style='border-top:none;border-left:none'>0</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=2 class=xl571 style='border-left:
  none'>BCA</td>
  <td class=xl519 style='border-left:none'>S</td>
  <td class=xl500 style='border-left:none'></td>
  <td class=xl513>IDR</td>
  <td class=xl514 style='border-top:none;border-left:none'>0</td>
  <td class=xl514 style='border-top:none;border-left:none'>0</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>&nbsp;</td>
  <td colspan=2 class=xl573 width=228 style='border-left:none;width:172pt'>Bank
  Mandiri</td>
  <td class=xl519 style='border-left:none'>S</td>
  <td class=xl500 style='border-left:none'></td>
  <td class=xl513>IDR</td>
  <td class=xl514 style='border-top:none;border-left:none'>0</td>
  <td class=xl514 style='border-top:none;border-left:none'>0</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>&nbsp;</td>
  <td colspan=2 class=xl573 width=228 style='border-left:none;width:172pt'>Citibank</td>
  <td class=xl519 style='border-left:none'>S</td>
  <td class=xl500 style='border-left:none'></td>
  <td class=xl513>IDR</td>
  <td class=xl514 style='border-top:none;border-left:none'>0</td>
  <td class=xl514 style='border-top:none;border-left:none'>0</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=2 class=xl573 width=228 style='border-left:none;width:172pt'>CIMB
  Niaga</td>
  <td class=xl519 style='border-left:none'>S</td>
  <td class=xl500 style='border-left:none'></td>
  <td class=xl513>IDR</td>
  <td class=xl514 style='border-top:none;border-left:none'>0</td>
  <td class=xl514 style='border-top:none;border-left:none'>0</td>
  
  
  
  
  
  
  
  
  
  
  
  
 </tr>
 <tr height=17 style='mso-height-source:userset;height:12.75pt'>
  <td height=17 class=xl284 colspan=8 style='height:12.75pt;mso-ignore:colspan'>Penjelasan
  mengenai selisih dana positif atau selisih dana negatif dan penyelesaiannya:</td>
 </tr>
	 ";
}


//---------------------------------------------------------------------------------

