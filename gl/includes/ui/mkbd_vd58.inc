<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function display_v8($type, $typename,$no)
{
	echo "
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl173 style='height:15.0pt'>6</td>
  <td colspan=7 class=xl554 width=340 style='border-right:.5pt solid black;
  border-left:none;width:255pt'>A</td>
  <td class=xl175 style='border-top:none;border-left:none'>B</td>
  <td class=xl175 style='border-top:none;border-left:none'>C</td>
  <td class=xl175 style='border-top:none;border-left:none'>D</td>
  <td class=xl174 width=115 style='border-top:none;border-left:none;width:86pt'>E</td>
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl320 style='height:15.0pt'>7</td>
  <td colspan=7 class=xl591 width=340 style='border-right:.5pt solid black;
  border-left:none;width:255pt'>Keterangan</td>
  <td class=xl175 style='border-top:none;border-left:none'>Formulir</td>
  <td class=xl321 style='border-top:none'>Lajur</td>
  <td class=xl321 style='border-top:none'>Baris</td>
  <td class=xl322 width=115 style='border-left:none;width:86pt'>Nilai</td>
  
  
  
  
 </tr>
 <tr class=xl329 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>8</td>
  <td colspan=7 class=xl323 width=340 style='border-right:.5pt solid black;
  width:255pt'>Total Liabilitas</td>
  <td class=xl324>V.D.5-2</td>
  <td class=xl324>B</td>
  <td class=xl326>164</td>
  <td class=xl502 width=115 style='border-top:none;border-left:none;width:86pt'>0 </td>
  
  
  
  
 </tr>
 <tr class=xl329 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>9</td>
  <td colspan=7 class=xl323 width=340 style='border-right:.5pt solid black;
  width:255pt'>Total Ranking Liabilities</td>
  <td class=xl324>V.D.5-3</td>
  <td class=xl324>B</td>
  <td class=xl326>31</td>
  <td class=xl325 width=115 style='border-top:none;border-left:none;width:86pt'>-</td>
  
  
  
  
 </tr>
 <tr class=xl329 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl177 style='height:30.0pt;border-top:none'>10</td>
  <td colspan=7 class=xl323 width=340 style='border-right:.5pt solid black;
  width:255pt'>Total Liabilitas dan Ranking Liabilities <br>(Baris 8 + Baris 9)</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl502 width=115 style='border-top:none;border-left:none;width:86pt'>0 </td>
  
  
  
  
 </tr>
 <tr class=xl329 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>11</td>
  <td colspan=7 class=xl323 width=340 style='border-right:.5pt solid black;
  width:255pt'>Dikurangi Utang Sub-Ordinasi</td>
  <td class=xl326>V.D.5-2</td>
  <td class=xl326>B</td>
  <td class=xl370>163</td>
  <td class=xl325 width=115 style='border-top:none;border-left:none;width:86pt'>-</td>
  
  
  
  
 </tr>
 <tr class=xl329 height=34 style='mso-height-source:userset;height:25.5pt'>
  <td height=34 class=xl177 style='height:25.5pt;border-top:none'>12</td>
  <td colspan=7 class=xl323 width=340 style='border-right:.5pt solid black;
  width:255pt'>Dikurangi Utang Dalam Rangka Penawaran Umum/Penawaran Terbatas</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  
  
  
  
 </tr>
 <tr class=xl329 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>13</td>
  <td class=xl323 width=28 style='border-top:none;width:21pt'>&nbsp;</td>
  <td class=xl89 colspan=3 style='mso-ignore:colspan'>Utang Nasabah Umum</td>
  <td class=xl286 width=0 style='border-top:none'>&nbsp;</td>
  <td class=xl286 width=127 style='border-top:none;width:95pt'>&nbsp;</td>
  <td class=xl283 width=34 style='border-top:none;width:26pt'>&nbsp;</td>
  <td class=xl326>V.D.5-2</td>
  <td class=xl326>B</td>
  <td class=xl370>146</td>
  <td class=xl325 width=115 style='border-top:none;border-left:none;width:86pt'>-</td>
  
  
  
  
 </tr>
 <tr class=xl329 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>14</td>
  <td class=xl323 width=28 style='border-top:none;width:21pt'>&nbsp;</td>
  <td class=xl89 colspan=3 style='mso-ignore:colspan'>Utang Emiten</td>
  <td class=xl286 width=0 style='border-top:none'>&nbsp;</td>
  <td class=xl286 width=127 style='border-top:none;width:95pt'>&nbsp;</td>
  <td class=xl283 width=34 style='border-top:none;width:26pt'>&nbsp;</td>
  <td class=xl326>V.D.5-2</td>
  <td class=xl326>B</td>
  <td class=xl370>147</td>
  <td class=xl325 width=115 style='border-top:none;border-left:none;width:86pt'>-</td>
  
  
  
  
 </tr>
 <tr class=xl329 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>15</td>
  <td class=xl323 width=28 style='border-top:none;width:21pt'>&nbsp;</td>
  <td class=xl89 colspan=5 style='mso-ignore:colspan'>Utang Kepada Penerbit
  Efek</td>
  <td class=xl283 width=34 style='border-top:none;width:26pt'>&nbsp;</td>
  <td class=xl326>V.D.5-2</td>
  <td class=xl326>B</td>
  <td class=xl370>148</td>
  <td class=xl325 width=115 style='border-top:none;border-left:none;width:86pt'>-</td>
  
  
  
  
 </tr>
 <tr class=xl329 height=60 style='mso-height-source:userset;height:45.0pt'>
  <td height=60 class=xl177 style='height:45.0pt;border-top:none'>16</td>
  <td colspan=7 class=xl551 width=340 style='border-right:.5pt solid black;
  width:255pt'>Total Liabilitas dan Ranking Liabilities Tanpa Utang Subordinasi
  dan Utang Dalam Rangka Penawaran Umum/Penawaran Terbatas</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl502 width=115 style='border-top:none;border-left:none;width:86pt'>0 </td>
  
  
  
  
 </tr>
 <tr class=xl329 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>17</td>
  <td colspan=7 class=xl551 width=340 style='border-right:.5pt solid black;
  width:255pt'>Nilai MKBD yang diwajibkan untuk PPE atau PEE</td>
  <td class=xl406>&nbsp;</td>
  <td class=xl406>&nbsp;</td>
  <td class=xl406>&nbsp;</td>
  <td class=xl406>&nbsp;</td>
  
  
  
  
 </tr>
 <tr class=xl329 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>18</td>
  <td class=xl91 width=28 style='border-top:none;width:21pt'>&nbsp;</td>
  <td colspan=6 class=xl286 width=312 style='border-right:.5pt solid black;
  width:234pt'>Nilai Persyaratan Minimal MKBD*</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl325 width=115 style='border-top:none;border-left:none;width:86pt'>&nbsp;</td>
  
  
  
  
 </tr>
 <tr class=xl329 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>19</td>
  <td class=xl91 width=28 style='border-top:none;width:21pt'>&nbsp;</td>
  <td colspan=6 class=xl286 width=312 style='border-right:.5pt solid black;
  width:234pt'>6,25% dari baris 16</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl325 width=115 style='border-top:none;border-left:none;width:86pt'>&nbsp;</td>
  
  
  
  
 </tr>
 <tr class=xl329 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl177 style='height:30.0pt;border-top:none'>20</td>
  <td class=xl91 width=28 style='border-top:none;width:21pt'>&nbsp;</td>
  <td colspan=6 class=xl286 width=312 style='border-right:.5pt solid black;
  width:234pt'>MKBD yang dipersyaratkan <br>(nilai yang lebih tinggi antara baris 18 dan baris 19)</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl325 width=115 style='border-top:none;border-left:none;width:86pt'>&nbsp;</td>
  
  
  
  
 </tr>
 <tr class=xl329 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>21</td>
  <td colspan=7 class=xl551 width=340 style='border-right:.5pt solid black;
  width:255pt'>Nilai MKBD yang diwajibkan untuk MI</td>
  <td class=xl406>&nbsp;</td>
  <td class=xl406>&nbsp;</td>
  <td class=xl406>&nbsp;</td>
  <td class=xl406>&nbsp;</td>
  
  
  
  
 </tr>
 <tr class=xl329 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>22</td>
  <td class=xl91 width=28 style='border-top:none;width:21pt'>&nbsp;</td>
  <td colspan=6 class=xl286 width=312 style='border-right:.5pt solid black;
  width:234pt'>Nilai Persyaratan Minimal MKBD**</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl502 width=115 style='border-top:none;border-left:none;width:86pt'>0 </td>
  
  
  
  
 </tr>
 <tr class=xl329 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>23</td>
  <td class=xl91 width=28 style='border-top:none;width:21pt'>&nbsp;</td>
  <td colspan=6 class=xl286 width=312 style='border-right:.5pt solid black;
  width:234pt'>Nilai dana yang dikelola oleh MI</td>
  <td class=xl406>&nbsp;</td>
  <td class=xl406>&nbsp;</td>
  <td class=xl406>&nbsp;</td>
  <td class=xl502 width=115 style='border-top:none;border-left:none;width:86pt'>0 </td>
  
  
  
  
 </tr>
 <tr class=xl329 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl177 style='height:15.0pt;border-top:none'>24</td>
  <td class=xl91 width=28 style='border-top:none;width:21pt'>&nbsp;</td>
  <td colspan=6 class=xl286 width=312 style='border-right:.5pt solid black;
  width:234pt'>0,1 % dari baris 23</td>
  <td class=xl406>&nbsp;</td>
  <td class=xl406>&nbsp;</td>
  <td class=xl406>&nbsp;</td>
  <td class=xl502 width=115 style='border-top:none;border-left:none;width:86pt'>0 </td>
  
  
  
  
 </tr>
 <tr class=xl329 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl177 style='height:30.0pt;border-top:none'>25</td>
  <td class=xl91 width=28 style='border-top:none;width:21pt'>&nbsp;</td>
  <td colspan=6 class=xl286 width=312 style='border-right:.5pt solid black;
  width:234pt'>Nilai MKBD yang dipersyaratkan<br>
    (baris 22 ditambah baris 24)</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl405>&nbsp;</td>
  <td class=xl502 width=115 style='border-top:none;border-left:none;width:86pt'>0 </td>
  
  
  
  
 </tr>
 <tr class=xl329 height=66 style='mso-height-source:userset;height:49.5pt'>
  <td height=66 class=xl177 style='height:49.5pt;border-top:none'>26</td>
  <td colspan=7 class=xl551 width=340 style='border-right:.5pt solid black;
  width:255pt'>Nilai MKBD yang diwajibkan untuk PE sesuai dengan izin usaha
  yang dimiliki (baris 20, baris 25, atau baris 20 ditambah baris 25)</td>
  <td class=xl407 width=57 style='border-top:none;border-left:none;width:43pt'>&nbsp;</td>
  <td class=xl407 width=41 style='border-top:none;border-left:none;width:31pt'>&nbsp;</td>
  <td class=xl407 width=38 style='border-top:none;border-left:none;width:29pt'>&nbsp;</td>
  <td class=xl502 width=115 style='border-top:none;border-left:none;width:86pt'>0 </td>
  
  
  
  
 </tr>
 <tr height=40 style='mso-height-source:userset;height:30.0pt'>
  <td colspan=12 height=40 class=xl209 width=626 style='height:30.0pt;
  width:470pt'>* Rp25.000.000.000,00 untuk PEE atau PPE yang mengadministrasikan rekening Efek nasabah atau Rp200.000.000,00 untuk PPE yang tidak mengadministrasikan rekening Efek nasabah</td>
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl138 colspan=12 style='height:15.0pt;mso-ignore:colspan'>** Rp200.000.000,00 untuk Manajer Investasi</td>
  
  
  
  
  
  
  
  
 </tr>
	 ";
}


//---------------------------------------------------------------------------------

