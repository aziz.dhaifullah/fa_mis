<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function display_v7($type, $typename,$no)
{
	echo "
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl285 style='width: 5%;height:15.0pt'>6</td>
  <td class=xl247 style='border-top:none'>&nbsp;</td>
  <td class=xl248 style='border-top:none'>A</td>
  <td class=xl250 style='border-left:none'>B</td>
  <td class=xl249>C</td>
  <td class=xl251>D</td>
  <td class=xl250>E</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl252 style='height:30.0pt;border-top:none'>7</td>
  <td colspan=2 class=xl582 width=268 style='border-right:.5pt solid black;
  border-left:none;width:201pt'>SALDO DEBIT BUKU PEMBANTU EFEK</td>
  <td class=xl311 style='border-top:none;border-left:none'>Saldo</td>
  <td class=xl311 style='border-top:none;border-left:none'>Terafiliasi</td>
  <td class=xl312 width=76 style='border-top:none;border-left:none;width:57pt'>Tidak
  Terafiliasi</td>
  <td class=xl313 style='border-top:none'>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>8</td>
  <td colspan=2 class=xl323 width=268 style='border-left:none;width:201pt'>Efek
  Reverse Repo</td>
  <td class=xl287>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>9</td>
  <td colspan=2 class=xl323 width=268 style='border-left:none;width:201pt'>Portofolio
  Perusahaan Efek (Posisi Long)</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl255 style='height:30.0pt'>10</td>
  <td colspan=2 class=xl323 width=268 style='border-left:none;width:201pt'>Efek
  Dalam Rekening Efek Nasabah (Posisi Long)</td>
  <td class=xl403 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>11</td>
  <td class=xl286 width=20 style='border-top:none;width:15pt'>&nbsp;</td>
  <td class=xl257 width=248 style='width:186pt'>Efek Bebas</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl288>&nbsp;</td>
  <td class=xl289>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>12</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek Jaminan</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl288>&nbsp;</td>
  <td class=xl289>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>13</td>
  <td colspan=2 class=xl323 width=268 style='border-left:none;width:201pt'>Transaksi
  Beli Efek nasabah pemilik rekening</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>14</td>
  <td colspan=2 class=xl323 width=268 style='border-left:none;width:201pt'>Efek
  Milik Perusahaan Efek Lain</td>
  <td class=xl403 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl161 style='height:30.0pt;border-top:none'>15</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek yang Dipinjam dari
  Perusahaan Efek lain</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>16</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Transaksi Jual Efek</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>17</td>
  <td class=xl257 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl257 width=248 style='width:186pt'>Gagal Serah - Perusahaan Efek</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl255 style='height:30.0pt'>18</td>
  <td colspan=2 class=xl323 width=268 style='border-left:none;width:201pt'>Efek
  yang Akan Diserahkan ke Lembaga Kliring dan Penjaminan</td>
  <td class=xl403 style='border-top:none'>&nbsp;</td>
  <td class=xl402 style='border-left:none'>&nbsp;</td>
  <td class=xl404 style='border-left:none'>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=41 style='mso-height-source:userset;height:30.75pt'>
  <td height=41 class=xl161 style='height:30.75pt;border-top:none'>19</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek yang dipinjam dari Lembaga
  Kliring dan Penjaminan</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>20</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek Serah Atas Transaksi
  Kliring</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>21</td>
  <td colspan=2 class=xl323 width=268 style='border-left:none;width:201pt'>Efek
  Milik Nasabah Kelembagaan</td>
  <td class=xl403 style='border-top:none'>&nbsp;</td>
  <td class=xl402 style='border-left:none'>&nbsp;</td>
  <td class=xl404 style='border-left:none'>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>22</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Transaksi Beli Nasabah
  Kelembagaan</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl402 style='border-left:none'>&nbsp;</td>
  <td class=xl404 style='border-left:none'>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>23</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Gagal Serah - Nasabah
  Kelembagaan</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>24</td>
  <td colspan=2 class=xl323 width=268 style='border-left:none;width:201pt'>Efek
  yang dipinjam dari Pihak lain</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl188 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>25</td>
  <td colspan=2 class=xl323 width=268 style='border-left:none;width:201pt'>Selisih Efek Positif</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>26</td>
  <td class=xl290 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl291 width=248 style='width:186pt'>Total Debit (Nilai Pasar Wajar)</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=42 style='mso-height-source:userset;height:31.9pt'>
  <td height=42 class=xl298 style='height:31.9pt'>27</td>
  <td colspan=2 class=xl554 width=268 style='border-left:none;width:201pt'>SALDO
  KREDIT BUKU PEMBANTU EFEK</td>
  <td class=xl298>Saldo</td>
  <td class=xl298 style='border-left:none'>Dimiliki</td>
  <td class=xl314 style='border-left:none'>Dipisahkan</td>
  <td class=xl315 width=77 style='width:58pt'>Tidak Dipisahkan</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>28</td>
  <td colspan=2 class=xl323 width=268 style='border-left:none;width:201pt'>Efek
  dalam Pengendalian Langsung Perusahaan Efek</td>
  <td class=xl402>&nbsp;</td>
  <td class=xl402 style='border-left:none'>&nbsp;</td>
  <td class=xl404 style='border-left:none'>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl255 style='height:30.0pt'>29</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek yang disimpan di Unit
  Kerja yang Menjalankan Fungsi Kustodian</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl293>&nbsp;</td>
  <td class=xl294>&nbsp;</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl161 style='height:30.0pt;border-top:none'>30</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek yang disimpan di Kotak
  Penyimpanan Bank Kustodian</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl293>&nbsp;</td>
  <td class=xl294>&nbsp;</td>
  <td class=xl292>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl255 style='height:30.0pt'>31</td>
  <td class=xl257 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl257 width=248 style='width:186pt'>Efek yang ada dalam Rekening
  Efek Bank Kustodian</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl293>&nbsp;</td>
  <td class=xl294>&nbsp;</td>
  <td class=xl292>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl161 style='height:30.0pt;border-top:none'>32</td>
  <td class=xl257 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl257 width=248 style='width:186pt'>Efek yang ada dalam Rekening
  Efek Perusahaan Efek Lain</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl288>&nbsp;</td>
  <td class=xl289>&nbsp;</td>
  <td class=xl295>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>33</td>
  <td class=xl257 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl257 width=248 style='width:186pt'>Efek yang ada dalam Rekening
  Efek LPP</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl293>&nbsp;</td>
  <td class=xl294>&nbsp;</td>
  <td class=xl292>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=80 style='mso-height-source:userset;height:60.0pt'>
  <td height=80 class=xl161 style='height:60.0pt;border-top:none'>34</td>
  <td class=xl296 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl472 width=248 style='width:186pt'>Efek yang ada pada Emiten atau
  BAE (belum diterbitkan dalam waktu 5 (lima) hari kerja terhitung sejak Efek
  tersebut dimasukkan ke Emiten atau BAE)</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl288>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl295>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='page-break-before:always;mso-height-source:
  userset;height:30.0pt'>
  <td height=40 class=xl161 style='height:30.0pt;border-top:none'>35</td>
  <td class=xl371 width=20 style='border-top:none;width:15pt'>&nbsp;</td>
  <td class=xl286 width=248 style='border-top:none;width:186pt'>Efek yang ada
  dalam Rekening Efek pada Lembaga Penyimpanan Lainnya</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl299 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl372 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl299 style='border-top:none'>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl161 style='height:30.0pt;border-top:none'>36</td>
  <td class=xl183 width=20 style='border-top:none;width:15pt'>&nbsp;</td>
  <td class=xl317 width=248 style='border-top:none;width:186pt'>Total Efek
  Dalam Pengendalian Langsung PE</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  <td class=xl318 style='border-top:none'>&nbsp;</td>
  <td class=xl319 style='border-top:none'>&nbsp;</td>
  <td class=xl287 style='border-top:none'>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl297 style='height:15.0pt;border-top:none'>37</td>
  <td colspan=2 class=xl585 style='border-left:none'>&nbsp;</td>
  <td rowspan=2 class=xl587 style='border-bottom:.5pt solid black;border-top:
  none'>Saldo</td>
  <td rowspan=2 class=xl589 width=73 style='border-bottom:.5pt solid black;
  border-top:none;width:55pt'>s.d. 5 hari kerja</td>
  <td colspan=2 class=xl585 style='border-right:.5pt solid black;border-left:
  none'>Lebih Dari 5 Hari</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl297 style='height:15.0pt;border-top:none'>38</td>
  <td colspan=2 class=xl585 style='border-left:none'>&nbsp;</td>
  <td class=xl298 style='border-left:none'>Dimiliki</td>
  <td class=xl403 style='border-top:none;border-left:none'>Dipisahkan</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl255 style='height:30.0pt'>39</td>
  <td colspan=2 class=xl323 width=268 style='border-left:none;width:201pt'>Efek
  Tidak Dalam Pengendalian Langsung Perusahaan Efek</td>
  <td class=xl403 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl403 style='border-top:none'>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=60 style='mso-height-source:userset;height:45.0pt'>
  <td height=60 class=xl161 style='height:45.0pt;border-top:none'>40</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl183 width=248 style='border-top:none;width:186pt'>Efek yang
  dipakai sebagai jaminan pinjaman di bank atau di lembaga keuangan</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl403 style='border-top:none'>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl255 style='height:30.0pt'>41</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl183 width=248 style='border-top:none;width:186pt'>Efek dalam
  Perjalanan Antar Kantor dalam satu Perusahaan Efek</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl292 style='border-left:none'>&nbsp;</td>
  <td class=xl292 style='border-left:none'>&nbsp;</td>
  <td class=xl292 style='border-left:none'>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=100 style='mso-height-source:userset;height:75.0pt'>
  <td height=100 class=xl161 style='height:75.0pt;border-top:none'>42</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl183 width=248 style='border-top:none;width:186pt'>Efek dalam
  Perjalanan ke PE lain, bank Kustodian, Lembaga Kliring dan Penjaminan atau
  Lembaga Penyimpanan dan Penyelesaian dimana bukti pengiriman belum diterima</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl292 style='border-left:none'>&nbsp;</td>
  <td class=xl292 style='border-left:none'>&nbsp;</td>
  <td class=xl292 style='border-left:none'>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=60 style='mso-height-source:userset;height:45.0pt'>
  <td height=60 class=xl255 style='height:45.0pt'>43</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek yang akan diterima dari
  bank luar negeri, lembaga kliring luar negeri, atau PE luar negeri</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl403 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=80 style='mso-height-source:userset;height:60.0pt'>
  <td height=80 class=xl161 style='height:60.0pt;border-top:none'>44</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl471 width=248 style='width:186pt'>Efek pada Emiten atau BAE (belum diterbitkan dalam waktu 5 (lima)
  hari kerja terhitung sejak Efek tersebut dimasukkan ke Emiten atau BAE)</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl299 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl288>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=60 style='mso-height-source:userset;height:45.0pt'>
  <td height=60 class=xl255 style='height:45.0pt'>45</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek yang akan Diterima Dari
  Emiten sebagai Akibat adanya Pembagian Hak Dalam Rangka Aksi Korporasi</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl288>&nbsp;</td>
  <td class=xl299 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl288>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>46</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek Repo atau Re-repo</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl403 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl255 style='height:30.0pt'>47</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl183 width=248 style='border-top:none;width:186pt'>Transaksi jual
  Efek nasabah pemilik rekening</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl403 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl161 style='height:30.0pt;border-top:none'>48</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek Dijual yang Belum Dimiliki
  (Posisi Short)</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl255 style='height:30.0pt'>49</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek yang Akan Diterima dari
  Perusahaan Efek lain</td>
  <td class=xl403 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>50</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek Dipinjamkan</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>51</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Transaksi Beli Efek</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>52</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Gagal Terima - Perusahaan Efek</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl288>&nbsp;</td>
  <td class=xl299 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl288>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl255 style='height:30.0pt'>53</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek yang Akan Diterima dari
  Lembaga Kliring dan Penjaminan</td>
  <td class=xl403 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>54</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek Dipinjamkan</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>55</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek Transaksi Kliring</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl161 style='height:30.0pt;border-top:none'>56</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Efek yang Akan Diterima dari
  Nasabah Kelembagaan</td>
  <td class=xl403 style='border-top:none'>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>57</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Transaksi Jual Nasabah Kelembagaan</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>58</td>
  <td class=xl328 width=20 style='width:15pt'></td>
  <td class=xl328 width=248 style='width:186pt'>Gagal Terima - Nasabah Kelembagaan</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl288>&nbsp;</td>
  <td class=xl372 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl299 style='border-top:none'>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=41 style='page-break-before:always;mso-height-source:
  userset;height:30.75pt'>
  <td height=41 class=xl177 style='height:30.75pt;border-top:none'>59</td>
  <td class=xl178 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl441 width=248 style='width:186pt'>Posisi Short Rekening Efek
  Nasabah (Terafiliasi)</td>
  <td class=xl445 style='border-top:none'>&nbsp;</td>
  <td class=xl403 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl403 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl403 style='border-top:none;border-left:none'>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl161 style='height:30.0pt;border-top:none'>60</td>
  <td class=xl296 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl261 width=248 style='width:186pt'>Posisi Short Rekening Efek
  Nasabah (Tidak Terafiliasi)</td>
  <td class=xl292>&nbsp;</td>
  <td class=xl400>&nbsp;</td>
  <td class=xl401>&nbsp;</td>
  <td class=xl402>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl255 style='height:30.0pt'>61</td>
  <td class=xl286 width=20 style='border-top:none;width:15pt'>&nbsp;</td>
  <td class=xl316 width=248 style='width:186pt'>Total Efek Tidak Dalam
  Pengendalian Langsung Perusahaan</td>
  <td class=xl300>&nbsp;</td>
  <td class=xl300 style='border-left:none'>&nbsp;</td>
  <td class=xl301 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl299 style='border-top:none;border-left:none'>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl161 style='height:15.0pt;border-top:none'>62</td>
  <td colspan=2 class=xl323 width=268 style='border-left:none;width:201pt'>Selisih
  Efek Negatif</td>
  <td class=xl300>&nbsp;</td>
  <td class=xl300 style='border-left:none'>&nbsp;</td>
  <td class=xl300 style='border-left:none'>&nbsp;</td>
  <td class=xl299 style='border-top:none;border-left:none'>&nbsp;</td>
  
 </tr>
 <tr class=xl244 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl255 style='height:15.0pt'>63</td>
  <td class=xl261 width=20 style='width:15pt'>&nbsp;</td>
  <td class=xl291 width=248 style='width:186pt'>Total Kredit (Nilai Pasar Wajar)</td>
  <td class=xl295>&nbsp;</td>
  <td class=xl295 style='border-left:none'>&nbsp;</td>
  <td class=xl295 style='border-left:none'>&nbsp;</td>
  <td class=xl299 style='border-top:none;border-left:none'>&nbsp;</td>
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl133 style='height:15.0pt'></td>
  <td class=xl142 width=20 style='width:15pt'></td>
  <td class=xl209 width=248 style='width:186pt'></td>
  <td class=xl147></td>
  <td class=xl147></td>
  <td class=xl147></td>
  <td class=xl147></td>
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl302 colspan=7 style='height:15.0pt;mso-ignore:colspan;
  border-right:.5pt solid black'>Penjelasan mengenai selisih Efek positif atau
  selisih Efek negatif dan penyelesaiannya:</td>
  
 </tr>
	 ";
}


//---------------------------------------------------------------------------------

