<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function display_v10($type, $typename,$no)
{
	echo "
 
 <tr class=xl231 height=26 style='mso-height-source:userset;height:19.5pt'>
  <td height=26 class=xl349 colspan=13 style='height:19.5pt;mso-ignore:colspan'>V.D.5-10A:
  AKTIVITAS REPO</td>
 </tr>

 <tr height=100 style='mso-height-source:userset;height:75.0pt'>
  <td height=100 class=xl479 width=41 style='height:75.0pt;width:31pt'>No.</td>
  <td colspan=3 class=xl476 width=300 style='border-right:.5pt solid black;
  border-left:none;width:225pt'>Jenis Repo</td>
  <td class=xl476 width=176 style='border-left:none;width:132pt'>Pembeli (Lawan
  Transaksi)<span style='mso-spacerun:yes'> </span></td>
  <td class=xl479 width=125 style='width:94pt'>Tanggal Penjualan</td>
  <td class=xl479 width=155 style='border-left:none;width:116pt'>Tanggal
  Pembelian Kembali</td>
  <td class=xl479 width=120 style='border-left:none;width:90pt'>Nilai Penjualan</td>
  <td class=xl479 width=90 style='border-left:none;width:68pt'>Nilai Pembelian
  Kembali</td>
  <td class=xl476 width=127 style='border-left:none;width:95pt'>Kode Efek
  Jaminan</td>
  <td class=xl479 width=124 style='width:93pt'>Jumlah Jaminan (Lembar/ Nominal)</td>
  <td class=xl479 width=151 style='border-left:none;width:113pt'>Nilai Pasar
  Wajar Jaminan</td>
  <td colspan=2 class=xl479 width=145 style='border-left:none;width:109pt'>Nilai
  Ranking Liabilities<span style='mso-spacerun:yes'> </span></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl224 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=3 class=xl461 style='border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl461 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl224 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=3 class=xl461 style='border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl461 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl224 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=3 class=xl461 style='border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl461 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl466 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=6 class=xl617 style='border-right:.5pt solid black;border-left:
  none'>Sub Total Repo Surat Berharga Negara</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl466 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=6 class=xl617 style='border-right:.5pt solid black;border-left:
  none'>Sub Total Repo Obligasi atau<span style='mso-spacerun:yes'> 
  </span>Sukuk Korporasi</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl466 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=6 class=xl617 style='border-right:.5pt solid black;border-left:
  none'>Sub Total Repo Efek Bersifat Ekuitas</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl466 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=6 class=xl617 style='border-right:.5pt solid black;border-left:
  none'>Total Repo</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl217 colspan=14 style='height:15.0pt;mso-ignore:colspan'>Apabila
  diperlukan, baris baru dapat ditambahkan</td>
 </tr>

 <tr height=22 style='height:16.5pt'>
  <td height=22 class=xl349 colspan=14 style='height:16.5pt;mso-ignore:colspan'>V.D.5-10B:
  AKTIVITAS REVERSE REPO</td>
 </tr>

 <tr height=80 style='mso-height-source:userset;height:60.0pt'>
  <td height=80 class=xl479 width=41 style='height:60.0pt;width:31pt'>No.</td>
  <td colspan=3 class=xl476 width=300 style='border-right:.5pt solid black;
  border-left:none;width:225pt'>Jenis Reverse Repo</td>
  <td class=xl476 width=176 style='border-left:none;width:132pt'>Penjual (Lawan
  Transaksi)<span style='mso-spacerun:yes'> </span></td>
  <td class=xl418 width=125 style='width:94pt'>Tanggal Pembelian</td>
  <td class=xl418 width=155 style='border-left:none;width:116pt'>Tanggal
  Penjualan Kembali</td>
  <td class=xl418 width=120 style='border-left:none;width:90pt'>Nilai Pembelian</td>
  <td class=xl418 width=90 style='border-left:none;width:68pt'>Nilai Penjualan
  Kembali</td>
  <td class=xl476 width=127 style='border-left:none;width:95pt'>Kode Efek
  Jaminan</td>
  <td class=xl479 width=124 style='width:93pt'>Jumlah Jaminan (Lembar/ Nominal)</td>
  <td class=xl479 width=151 style='border-left:none;width:113pt'>Nilai Pasar
  Wajar Jaminan</td>
  <td colspan=2 class=xl479 width=145 style='border-left:none;width:109pt'>Nilai Ranking Liabilities</td>
  </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl224 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=3 class=xl609 style='border-right:.5pt solid black;border-left:
  none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl223 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl224 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=3 class=xl609 style='border-right:.5pt solid black;border-left:
  none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl223 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl224 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=3 class=xl609 style='border-right:.5pt solid black;border-left:
  none'>p</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl223 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl466 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=6 class=xl617 style='border-right:.5pt solid black;border-left:
  none'>Sub Total Reverse Repo Surat Berharga Negara</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl466 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=6 class=xl617 style='border-right:.5pt solid black;border-left:
  none'>Sub Total Reverse Repo Obligasi atau<span style='mso-spacerun:yes'> 
  </span>Sukuk Korporasi</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl466 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=6 class=xl617 style='border-right:.5pt solid black;border-left:
  none'>Sub Total Reverse Repo Efek Bersifat Ekuitas</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl466 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=6 class=xl617 style='border-right:.5pt solid black;border-left:
  none'>Total Reverse</font> Repo</font></td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl217 colspan=13 style='height:15.0pt;mso-ignore:colspan'>Apabila diperlukan, baris baru dapat ditambahkan</td>
 </tr>


 <tr class=xl231 height=26 style='mso-height-source:userset;height:19.5pt'>
  <td height=26 class=xl349 colspan=13 style='height:19.5pt;mso-ignore:colspan'>V.D.5-10C: AKTIVITAS  DAN KONSENTRASI PORTOFOLIO EFEK</td>
 </tr>
 
 <tr height=100 style='mso-height-source:userset;height:75.0pt'>
  <td height=100 class=xl479 width=41 style='height:75.0pt;width:31pt'>No.</td>
  <td colspan=3 class=xl476 width=300 style='border-right:.5pt solid black;
  border-left:none;width:225pt'>Jenis Efek</td>
  <td class=xl476 width=176 style='border-left:none;width:132pt'>Kode Efek</td>
  <td class=xl479 width=125 style='width:94pt'>Terafiliasi /Tidak Terafiliasi</td>
  <td class=xl479 width=155 style='border-left:none;width:116pt'>Lembar/ Nominal</td>
  <td class=xl479 width=120 style='border-left:none;width:90pt'>Harga Perolehan</td>
  <td class=xl479 width=90 style='border-left:none;width:68pt'>Harga Perolehan</td>
  <td class=xl476 width=127 style='border-left:none;width:95pt'>Nilai Pasar Wajar</td>
  <td class=xl479 width=124 style='width:93pt'> Grup Emiten</td>
  <td class=xl479 width=151 style='border-left:none;width:113pt'>Persentase Nilai Pasar Wajar Terhadap Total Modal Sendiri</td>
  <td colspan=2 class=xl479 width=145 style='border-left:none;width:109pt'>Nilai Ranking Liabilities</td>
 </tr>

 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl224 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=3 class=xl461 style='border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl461 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl224 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=3 class=xl461 style='border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl461 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl224 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=3 class=xl461 style='border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl461 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl466 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=8 class=xl617 style='border-right:.5pt solid black;border-left:
  none'>Sub Total A : Investasi melebihi 20% dari total modal sendiri hanya pada satu jenis Efek pada satu Emiten**</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl466 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=8 class=xl617 style='border-right:.5pt solid black;border-left:
  none'>Sub Total B : investasi melebihi 20% dari total modal sendiri pada satu Emiten namun dalam beberapa jenis Efek***/td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl466 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=8 class=xl617 style='border-right:.5pt solid black;border-left:
  none'>Sub Total C : investasi melebihi 20% dari total modal sendiri  pada beberapa Emiten dalam satu grup****</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl466 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=8 class=xl617 style='border-right:.5pt solid black;border-left:
  none'>Sub Total D : investasi  melebihi 40% dari total modal sendiri  pada Surat Berharga Negara*****</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl466 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=8 class=xl617 style='border-right:.5pt solid black;border-left:
  none'>Sub Total Portfolio Tidak Terkonsentrasi</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl466 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=8 class=xl617 style='border-right:.5pt solid black;border-left:
  none'>Total Portofolio</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl466 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl224 style='border-left:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl217 colspan=14 style='height:15.0pt;mso-ignore:colspan'>Apabiladiperlukan, baris baru dapat ditambahkan
<br>*) Jenis Portofolio Efek yang termasuk dalam Tabel ini adalah: Saham, Obligasi, Sukuk Korporasi, atau Surat Berharga Negara
<br>**) Diisi jika investasi Perusahaan Efek melebihi 20% dari total modal sendiri hanya pada satu jenis Efek pada satu Emiten yang tidak termasuk dalam perhitungan Subtotal B 
<br>***) Diisi  jika investasi Perusahaan Efek melebihi 20% dari total modal sendiri  pada satu Emiten namun dalam beberapa jenis Efek yang tidak termasuk dalam perhitungan Subtotal C, misalnya saham dan obligasi, saham dan Sukuk, obligasi dan Sukuk.
<br>****) Diisi  jika investasi Perusahaan Efek melebihi 20% dari total modal sendiri  pada beberapa Emiten dalam satu Grup
<br>*****) Diisi  jika investasi Perusahaan Efek melebihi 40% dari total modal sendiri  pada Surat Berharga Negara
  </td>
 </tr>
	 ";
}


//---------------------------------------------------------------------------------

