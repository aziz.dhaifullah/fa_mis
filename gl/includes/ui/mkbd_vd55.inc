<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function display_v5($type, $typename,$no)
{
	echo "
 <tr class=xl221 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl218 style='height:15.0pt'>6</td>
  <td class=xl219 style='border-top:none;border-left:none'>A</td>
  <td class=xl219 style='border-top:none;border-left:none'>B</td>
  <td class=xl219 style='border-top:none;border-left:none'>C</td>
  <td class=xl219 style='border-left:none'>D</td>
  <td class=xl219 style='border-top:none;border-left:none'>E</td>
  <td class=xl219 style='border-top:none;border-left:none'>F</td>
  <td class=xl220 width=101 style='border-left:none;width:76pt'>G</td>
  <td class=xl219 style='border-left:none'>H</td>
  
  
  
  
  
  
 </tr>
 <tr class=xl222 height=94 style='mso-height-source:userset;height:70.5pt'>
  <td height=94 class=xl220 width=40 style='height:70.5pt;border-top:none;
  width:30pt'>7</td>
  <td class=xl220 width=142 style='border-top:none;border-left:none;width:107pt'>Nama
  Efek</td>
  <td class=xl220 width=126 style='border-top:none;border-left:none;width:95pt'>Nilai
  Efek</td>
  <td class=xl220 width=144 style='border-top:none;border-left:none;width:108pt'>Nama
  Efek Lindung Nilai</td>
  <td class=xl220 width=94 style='border-top:none;border-left:none;width:71pt'>Nilai
  Efek Lindung Nilai</td>
  <td class=xl220 width=110 style='border-top:none;border-left:none;width:83pt'>Nilai
  Efek yang ditutup dengan lindung nilai</td>
  <td class=xl220 width=110 style='border-top:none;border-left:none;width:83pt'>Nilai
  Haircut Efek yang ditutup dengan Lindung Nilai</td>
  <td class=xl220 width=101 style='border-top:none;border-left:none;width:76pt'>Nilai
  Haircut Efek Lindung Nilai</td>
  <td class=xl220 width=132 style='border-top:none;border-left:none;width:99pt'>Jumlah
  Pengembalian Haircut <br>
    <font >(Kolom F + Kolom G)</font></td>
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl223 style='height:15.0pt;border-top:none'>8</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl223 style='height:15.0pt;border-top:none'>9</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl223 style='height:15.0pt;border-top:none'>10</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl223 style='height:15.0pt;border-top:none'>11</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl223 style='height:15.0pt;border-top:none'>12</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl223 style='height:15.0pt;border-top:none'>13</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  
  
  
  
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl223 style='height:15.0pt;border-top:none'>14*</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl224 style='border-top:none;border-left:none'>&nbsp;</td>
  
  
  
  
  
  
 </tr>
 <tr class=xl227 height=24 style='mso-height-source:userset;height:18.0pt'>
  <td height=24 class=xl219 style='height:18.0pt;border-top:none'>&nbsp;</td>
  <td colspan=7 class=xl568>Jumlah Pengembalian Haircut Portofolio Efek Yang
  ditutup dengan Lindung Nilai</td>
  <td class=xl226 style='border-top:none'>&nbsp;</td>
  
  
  
  
  
  
 </tr>
 <tr height=16 style='mso-height-source:userset;height:12.0pt'>
  <td height=16 class=xl217 colspan=9 style='height:12.0pt;mso-ignore:colspan'>*
  Apabila diperlukan, baris baru dapat ditambahkan</td>
  
  
  
  
  
  
  
  
  
  
  
 </tr>
	 ";
}


//---------------------------------------------------------------------------------

