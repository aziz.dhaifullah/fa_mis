<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function display_v4($type, $typename,$no)
{
	echo "
 <tr class=xl185 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl190 style='height:15.0pt'>6</td>
  <td class=xl190>A</td>
  <td class=xl387>B</td>
  <td class=xl387 style='border-left:none'>C</td>
  <td class=xl475>D</td>
  <td class=xl387>E</td>
  <td class=xl473>F</td>
  <td class=xl387>G</td>
  <td class=xl387 style='border-left:none'>H</td>
  
  1
 </tr>
 <tr height=100 style='mso-height-source:userset;height:75.0pt'>
  <td height=100 class=xl191 style='height:75.0pt;border-top:none'>7</td>
  <td class=xl383 width=212 style='border-top:none;width:159pt'>Jenis Reksa Dana</td>
  <td class=xl83 width=86 style='border-top:none;border-left:none;width:65pt'>Nama Reksa Dana</td>
  <td class=xl83 width=72 style='border-top:none;border-left:none;width:54pt'>Afiliasi/Tidak Terafiliasi</td>
  <td class=xl192 width=111 style='border-top:none;width:83pt'>Nilai Aktiva Bersih  Unit Penyertaan Reksa Dana yang dimiliki </td>
  <td class=xl193 width=84 style='border-top:none;width:63pt'>Nilai Aktiva Bersih per Reksa Dana </td>
  <td class=xl192 width=174 style='border-top:none;width:131pt'>Perhitungan Ranking Liabilities</td>
  <td class=xl193 width=113 style='border-top:none;width:85pt'>Batasan yang dapat dimiliki untuk MKBD</td>
  <td class=xl83 width=118 style='border-top:none;border-left:none;width:89pt'>Kelebihan di atas batasan <br> (Kolom D - Kolom G)</td>
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl194 style='height:15.0pt'>8</td>
  <td class=xl126 style='border-top:none'>Reksa Dana Pasar Uang</td>
  <td class=xl653 width=86 style='border-top:none;border-left:none;width:65pt'>1<br>
    2</td>
  <td class=xl195 style='border-top:none'>&nbsp;</td>
  <td class=xl196>&nbsp;</td>
  <td class=xl197>&nbsp;</td>
  <td class=xl198 width=174 style='width:131pt'>Kelebihan atas 50% NAB</td>
  <td class=xl180 width=113 style='width:85pt'>&nbsp;</td>
  <td class=xl199 style='border-top:none;border-left:none'>&nbsp;</td>
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl194 style='height:15.0pt;border-top:none'>9</td>
  <td class=xl126 style='border-top:none'>Reksa Dana Terproteksi</td>
  <td class=xl126 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl195 style='border-top:none'>&nbsp;</td>
  <td class=xl196 style='border-top:none'>&nbsp;</td>
  <td class=xl197 style='border-top:none'>&nbsp;</td>
  <td class=xl395 width=174 style='border-top:none;width:131pt'>Kelebihan atas 50% NAB</td>
  <td class=xl180 width=113 style='border-top:none;width:85pt'>&nbsp;</td>
  <td class=xl199 style='border-top:none;border-left:none'>&nbsp;</td>
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl194 style='height:15.0pt;border-top:none'>10</td>
  <td class=xl126 style='border-top:none'>Reksa Dana Dengan Penjaminan</td>
  <td class=xl126 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl195 style='border-top:none'>&nbsp;</td>
  <td class=xl200 style='border-top:none'>&nbsp;</td>
  <td class=xl197 style='border-top:none'>&nbsp;</td>
  <td class=xl198 width=174 style='border-top:none;width:131pt'>Kelebihan atas 50% NAB</td>
  <td class=xl180 width=113 style='border-top:none;width:85pt'>&nbsp;</td>
  <td class=xl199 style='border-top:none;border-left:none'>&nbsp;</td>
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl194 style='height:15.0pt;border-top:none'>11</td>
  <td class=xl126 style='border-top:none'>Reksa Dana Pendapatan Tetap</td>
  <td class=xl126 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl195 style='border-top:none'>&nbsp;</td>
  <td class=xl196 style='border-top:none'>&nbsp;</td>
  <td class=xl197 style='border-top:none'>&nbsp;</td>
  <td class=xl198 width=174 style='border-top:none;width:131pt'>Kelebihan atas 40% NAB</td>
  <td class=xl180 width=113 style='border-top:none;width:85pt'>&nbsp;</td>
  <td class=xl199 style='border-top:none;border-left:none'>&nbsp;</td>
  
  
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl194 style='height:15.0pt;border-top:none'>12</td>
  <td class=xl126 style='border-top:none'>Reksa Dana Campuran atau Saham</td>
  <td class=xl126 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl195 style='border-top:none'>&nbsp;</td>
  <td class=xl201 style='border-top:none'>&nbsp;</td>
  <td class=xl197 style='border-top:none'>&nbsp;</td>
  <td class=xl202 width=174 style='border-top:none;width:131pt'>Kelebihan atas 25% NAB</td>
  <td class=xl199 style='border-top:none;border-left:none'>&nbsp;</td>
  
  <td class=xl143></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl194 style='height:15.0pt;border-top:none'>13</td>
  <td class=xl126 style='border-top:none'>Reksa Dana Indeks</td>
  <td class=xl126 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl195 style='border-top:none'>&nbsp;</td>
  <td class=xl204 style='border-top:none'>&nbsp;</td>
  <td class=xl199 style='border-top:none'>&nbsp;</td>
  <td class=xl202 width=174 style='border-top:none;width:131pt'>Kelebihan atas 25% NAB</td>
  <td class=xl199 style='border-top:none;border-left:none'>&nbsp;</td>
  
  <td class=xl143></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl194 style='height:15.0pt;border-top:none'>14*</td>
  <td class=xl126 style='border-top:none'>Reksa Dana Penyertaan Terbatas</td>
  <td class=xl126 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl195 style='border-top:none'>&nbsp;</td>
  <td class=xl204 style='border-top:none'>&nbsp;</td>
  <td class=xl199 style='border-top:none'>&nbsp;</td>
  <td class=xl202 width=174 style='border-top:none;width:131pt'>Kelebihan atas 25% NAB</td>
  <td class=xl199 style='border-top:none;border-left:none'>&nbsp;</td>
  
  <td class=xl143></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl194 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td colspan=3 class=xl565 style='border-right:.5pt solid black'>Nilai Yang
  Ditambahkan Sebagai Ranking Liabilities</td>
  <td class=xl206 style='border-top:none'>&nbsp;</td>
  <td class=xl207 style='border-top:none'>&nbsp;</td>
  <td class=xl206 style='border-top:none'>&nbsp;</td>
  <td class=xl207 style='border-top:none'>&nbsp;</td>
  <td class=xl208 style='border-top:none;border-left:none'>&nbsp;</td>
  
  
 </tr>
 <tr height=16 style='mso-height-source:userset;height:12.0pt'>
  <td height=16 class=xl138 colspan=9 style='height:12.0pt;mso-ignore:colspan'>*
  Apabila diperlukan, baris baru dapat ditambahkan</td>
 </tr>
	 ";
}


//---------------------------------------------------------------------------------

