<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function get_prod_siar()
{	
	$serverName = "172.25.250.20";  
	$db_connections_siar = array( "UID"=>"appims", "PWD"=>"P4ssw0rd","Database"=>"KRESNA_IMSSIAR");
	$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select IDProduct,ProductCode,ProductName from TProduct order by IDProduct asc";  
	$stmt = sqlsrv_query( $conn, $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$prod=array();$prodid=array();
	while($row = sqlsrv_fetch_array($stmt)){
		$prod[]=$row[2];
		$prodid[]=$row[0];
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 
	return array($prod,$prodid);
}
function get_aum_siar($prodid,$tgl)
{	
	$serverName = "10.254.69.98";  
	$db_connections_siar = array( "UID"=>"appims", "PWD"=>"P4ssw0rd","Database"=>"KRESNA_IMSSIAR");
	$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select a.OutstandingUnits,a.navvalue
,b.ProductCode
,b.ProductName
from TMGTFeeHistory a 
left join TProduct b on a.ProductID=b.IDProduct 
where a.ProductID=".$prodid." and a.NAVDate='".$tgl."'
order by feedate desc";  
	$stmt = sqlsrv_query( $conn, $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$row = sqlsrv_fetch_array($stmt);
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 
	//return $tsql;
	return $row;
}


function get_mgtfee($tgl)
{	
	$serverName = "10.254.69.98";  
	$db_connections_siar = array( "UID"=>"appims", "PWD"=>"P4ssw0rd","Database"=>"KRESNA_IMSSIAR");
	$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select a.mgtfee,a.sharingfee,b.productname
from TMGTFeeHistory a 
inner join TProduct b on a.ProductID=b.IDProduct 
inner join TAgent c on a.agentid=c.agentid 
where a.feedate like '".$tgl."'
order by feedate desc";  
	$stmt = sqlsrv_query( $conn, $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */
	$data=array();  
	while($row = sqlsrv_fetch_array($stmt)){
		$data[]=$row;
	}
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 
	//return $tsql;
	return $data;
}
//--------------------------------------------------------------------------------
