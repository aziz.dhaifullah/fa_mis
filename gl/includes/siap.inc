<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
//192.168.10.21
//10.254.69.98

//	$serverName = "172.25.250.20";  
	//$db_connections_siap = array( "UID"=>"appims", "PWD"=>"P4ssw0rd","Database"=>"KRESNA_IMSSIAP");
function get_prod_siap()
{	
	$serverName = "172.25.250.20";  
	$db_connections_siap = array( "UID"=>"appims", "PWD"=>"P4ssw0rd","Database"=>"KRESNA_IMSSIAP");
	$conn = sqlsrv_connect( $serverName, $db_connections_siap);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select TPFolioID,PFolioCode,PFolioFName,npwp from tpfolio where isactive=1 ";  
	$stmt = sqlsrv_query( $conn, $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$prod=array();$prodid=array();$prodcode=array();$npwp=array();
	while($row = sqlsrv_fetch_array($stmt)){
		if (!in_array($row[2], $prod)){
			$prodid[]=$row[0];
			$prodcode[]=$row[1];
			$prod[]=$row[2];
			$npwp[]=$row[3];
		}
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 
	return array($prod,$prodid,$prodcode,$npwp);
}


function get_prod_syai_siap()
{	
	$serverName = "172.25.250.20";  
	$db_connections_siap = array( "UID"=>"appims", "PWD"=>"P4ssw0rd","Database"=>"KRESNA_IMSSIAP");
	$conn = sqlsrv_connect( $serverName, $db_connections_siap);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select TPFolioID,PFolioCode,PFolioFName from tpfolio 
where PFolioCode in ('RSRIF','RSCGF','MSOGF','RSMSF2','MSMAF','MSTDF','MSFIF','MSOPS','MSDAA','RSMSF1','MSPTP') ";  
	$stmt = sqlsrv_query( $conn, $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$prod=array();$prodid=array();$prodcode=array();
	while($row = sqlsrv_fetch_array($stmt)){
		if (!in_array($row[2], $prod)){
			$prodid[]=$row[0];
			$prodcode[]=$row[1];
			$prod[]=$row[2];
		}
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 
	return array($prod,$prodid,$prodcode);
}

function get_nab_siap($prodid,$tgl2)
{	
	$serverName = "172.25.250.20";  
	$db_connections_siap = array( "UID"=>"appims", "PWD"=>"P4ssw0rd","Database"=>"KRESNA_IMSSIAP");
	$conn = sqlsrv_connect( $serverName, $db_connections_siap);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select a.* from tnav a 
where a.Date  = '".$tgl2."'
and a.TPFolioID=".$prodid." ";  
	$stmt = sqlsrv_query( $conn, $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$row = sqlsrv_fetch_array($stmt);
	//print_r($prod);  

	/* Free statement and connection resources. */  
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 
	return $row;
	//return $tsql;
}

function get_nab_tgl_siap($thn,$bln)
{	
	$serverName = "172.25.250.20";  
	$db_connections_siap = array( "UID"=>"appims", "PWD"=>"P4ssw0rd","Database"=>"KRESNA_IMSSIAP");
	$conn = sqlsrv_connect( $serverName, $db_connections_siap);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select convert(varchar,max(Date),120) from tnav 
	where Date between '".$thn."-".$bln."01' and '".$thn."-".$bln."31'
	  group by TPFolioID ";  
	$stmt = sqlsrv_query( $conn, $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$row = sqlsrv_fetch_array($stmt);
	//print_r($prod);  

	/* Free statement and connection resources. */  
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 
	return $row[0];
	//return $tsql;
}
function get_rpt_pfolio_siap($tgl)
{	
	$serverName = "172.25.250.20"; 
	$db_connections_siap = array( "UID"=>"appims", "PWD"=>"P4ssw0rd","Database"=>"KRESNA_IMSSIAP");
	$conn = sqlsrv_connect( $serverName, $db_connections_siap);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "exec sp_rpt_Pfolio_Val_Rpt_BLSHEET '%', '".$tgl."' ";  
	$stmt = sqlsrv_query( $conn, $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$prod=array();$prodid=array();
	while($row = sqlsrv_fetch_array($stmt)){
		if($row['SubTypeCode']=='DEP')
		$prod[]=$row;
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 
	return $prod;
}
function get_pph_siap($thn,$bln)
{	
	$serverName = "172.25.250.20"; 
	$db_connections_siap = array( "UID"=>"appims", "PWD"=>"P4ssw0rd","Database"=>"KRESNA_IMSSIAP");
	$conn = sqlsrv_connect( $serverName, $db_connections_siap);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$jumHari = cal_days_in_month(CAL_GREGORIAN, $bln, $thn);
	$tsql = "SELECT sum(cpartyfee),sum(pph23amount),b.pfoliocode,pfoliofname,b.cpartycode,cpartyname
FROM dbo.vh_TDeal b
left join dbo.TCParty c on c.tcpartyid=b.tcpartyid
left join TPFolio d on b.tpfolioid=d.tpfolioid
WHERE 
b.DealNo IN (SELECT DISTINCT A.DealNo FROM dbo.vh_TCashJournal A WHERE A.TCOAID IN (339,567) AND A.IsReverse IS NULL)
and b.bookdate >= '".$thn."-".$bln."-01' 
and b.bookdate <= '".$thn."-".$bln."-".$jumHari."' 
group by b.pfoliocode,pfoliofname,b.cpartycode,cpartyname";  
	$stmt = sqlsrv_query( $conn, $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$data=array();  
	while($row = sqlsrv_fetch_array($stmt)){
		$data[]=$row;
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 
	return $data;
}


function get_mgtfee_siap($tgl,$tgl2)
{	
	$serverName = "172.25.250.20";  
	$db_connections_siar = array( "UID"=>"sa", "PWD"=>"P4ssw0rd","Database"=>"IMS_SIAP");
	$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tgls=explode('-',$tgl2);
	$tsql="
	select sum(a.cashposition) as mgtfee,0 as sharingfee,d.pfoliofname ,'',c.cpartyname,
convert(varchar,max(a.Date),120) as tgl,d.pfoliocode,max(a.tcurrencyid) as tcurrencyid
from tcashledgerhistory a
inner join tcoa b on a.tcoaid=b.tcoaid
left join TPFolio d on a.tpfolioid=d.tpfolioid
left join dbo.TCParty c on d.tcpartyid=c.tcpartyid
where b.tcoaid in (179)
and a.Date like '".$tgls[0]."-".$tgls[1]."-%'
group by a.date,c.cpartyname,d.pfoliocode,d.pfoliofname
order by a.Date desc";

	/*
(select max(date) from tcashledgerhistory where date <= '".$tgl2."')
'".$tgls[0]."-".$tgls[1]."-%' 
*/
//order by feedate desc
	$stmt = sqlsrv_query( $conn, $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */
	$data=array();  
	$pc=array();
	while($row = sqlsrv_fetch_array($stmt)){
		if(!in_array($row[6], $pc)){
			$data[]=$row;
			$pc[]=$row[6];

			//if($row[6]=='MSLF')
			//print_r($row);
		}
	}
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 
	//return $tsql;
	return $data;
}

function get_shrfee_siap($tgl,$tgl2,$cparty)
{	
	$serverName = "172.25.250.20"; 
	$db_connections_siap = array( "UID"=>"appims", "PWD"=>"P4ssw0rd","Database"=>"KRESNA_IMSSIAP");
	$conn = sqlsrv_connect( $serverName, $db_connections_siap);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	//$jumHari = cal_days_in_month(CAL_GREGORIAN, $bln, $thn);
	//,convert(varchar,b.settledate,120)
	$tsql = "
SELECT sum(cpartyfee),sum(pph23amount),convert(varchar,b.settledate,120),b.pfoliocode,pfoliofname,b.cpartycode,cpartyname
,b.dealno,units,unitprice
FROM dbo.vh_TDeal b
left join dbo.TCParty c on c.tcpartyid=b.tcpartyid
left join TPFolio d on b.tpfolioid=d.tpfolioid
WHERE 
b.DealNo IN (
SELECT DISTINCT A.DealNo 
FROM dbo.vh_TCashJournal A 
WHERE A.TCOAID IN (339) 
AND A.IsReverse IS NULL)
and b.settledate >= '".$tgl."'
and b.settledate <= '".$tgl2."'
and b.cpartycode='".$cparty."'
group by b.settledate,b.pfoliocode,pfoliofname,b.cpartycode,cpartyname,dealno,units,unitprice
order by pfoliofname,b.settledate asc";  
	$stmt = sqlsrv_query( $conn, $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$data=array();  
	while($row = sqlsrv_fetch_array($stmt)){
		$data[]=$row;
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 
	return $data;
}


function get_cparty_siap()
{	
	$serverName = "172.25.250.20"; 
	$db_connections_siap = array( "UID"=>"appims", "PWD"=>"P4ssw0rd","Database"=>"KRESNA_IMSSIAP");
	$conn = sqlsrv_connect( $serverName, $db_connections_siap);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	//$jumHari = cal_days_in_month(CAL_GREGORIAN, $bln, $thn);
	$tsql = "select cpartycode,cpartyname 
from tcparty a
inner join tcpartytype b on a.tcpartytypeid=b.tcpartytypeid
order by cpartyname asc";  
	$stmt = sqlsrv_query( $conn, $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$data=array();  
	while($row = sqlsrv_fetch_array($stmt)){
		$data[]=$row;
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 
	return $data;
}


function get_saham_syai_siap($tgl,$tgl2)
{	
	$serverName = "172.25.250.20";  
	$db_connections_siap = array( "UID"=>"appims", "PWD"=>"P4ssw0rd","Database"=>"KRESNA_IMSSIAP");
	$conn = sqlsrv_connect( $serverName, $db_connections_siap);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "
select convert(varchar,a.Date,120),a.tpfolioid,a.price,b.* from TMarketPricePFolio a
left join TInstrument b on a.TInstrumentID=b.TInstrumentID
left join TPFolio c on a.TPFolioID=c.TPFolioID
where 
a.TInstrumentID in (940,504,1471,1752)
and a.Date between '".$tgl."' and '".$tgl2."' order by a.Date asc 
 ";  
	$stmt = sqlsrv_query( $conn, $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$prod=array();$prodid=array();$prodcode=array();
	while($row = sqlsrv_fetch_array($stmt)){
		$prodid[$row[4]][]=$row[0];
		$prodcode[$row[4]][]=$row;
		if (!in_array($row[4], $prod)){
			$prodid[]=$row[0];
			$prod[]=$row[4];
		}
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 
	return array($prod,$prodid,$prodcode);
	//return array($prod,$prodid,$prodcode);
}


function get_formulafee_siap()
{	
	$serverName = "172.25.250.20"; 
	$db_connections_siap = array( "UID"=>"appims", "PWD"=>"P4ssw0rd","Database"=>"KRESNA_IMSSIAP");
	$conn = sqlsrv_connect( $serverName, $db_connections_siap);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "
select d.pfoliocode,FeeFormulaName,amount,AmountCode,JournalAdjustment,DaysInMonth,DaysInYear
from TPFolioFee a
left join TPFolio d on d.tpfolioid=a.tpfolioid
inner join tfeeformula b  on b.tfeeformulaid=a.tfeeformulaid
inner join TFeeFormulaRule c on c.TFeeFormulaID=a.TFeeFormulaID
inner join TIntDays e on e.TIntDaysID=b.TIntDaysID
where FeeFormulaName like '%MGT%' 
group by d.pfoliocode,FeeFormulaName,amount,AmountCode,JournalAdjustment,DaysInMonth,DaysInYear "; 
//--and  pfoliocode like 'msbof' 
	$stmt = sqlsrv_query( $conn, $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$prod=array();$prodid=array();
	while($row = sqlsrv_fetch_array($stmt)){
		$prod[$row[0]]=$row;
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 
	return $prod;
}

function get_tb2_ims($date)
{
    $serverName = "192.168.69.250";  
    $db_connections_siap = array( "UID"=>"sa", "PWD"=>"P4ssw0rd","Database"=>"MIS_IMSSIAP");
    $conn = sqlsrv_connect( $serverName, $db_connections_siap);
    if( $conn === false )  
    {  
         echo "Could not connect.\n";  
         //die( print_r( sqlsrv_errors(), true));  
    }

    $sql = "
    select sum(cashposition) as cashpos, credit, coacode, convert(varchar, ledgerdate, 23) as ledgerdate,externalcode,pfoliocode  from fn_TCashLedger_IMS((select tpfolioid from tpfolio),'".$date."',0,0) tcl
        inner join tcoa tc on tcl.tcoaid = tc.tcoaid
        inner join tcoaexternal tce on tcl.tcoaid = tce.tcoaid 
        inner join tpfolio folio on tcl.tpfolioid = folio.tpfolioid 
        group by tce.externalcode, tc.coacode, tcl.credit, tcl.ledgerdate, folio.pfoliocode
    ";
    $datatb = array();
    $stmt = sqlsrv_query( $conn, $sql);  
    if( $stmt === false )  
    {  
         echo "Error in executing query.</br>";  
         //die( print_r( sqlsrv_errors(), true));  
    }else{
	
    while($row = sqlsrv_fetch_array($stmt)){
        $tanggal = trim($row['ledgerdate']);
        $pfoliocode = $row['pfoliocode'];
        $externalcode = str_replace('.','',$row['externalcode']);
        $externalcode = str_replace('-','',$externalcode);
        $cash  = $row['cashpos'];
        if($row['credit']!=0){
        	$cash  = $cash * -1;
    	}
        $datatb[$tanggal][$pfoliocode][$externalcode] = $cash;
    }

    sqlsrv_free_stmt( $stmt);  
	}  
    
    sqlsrv_close($conn);
    
    
    return $datatb;
}

function get_comp_code()
{
    $serverName = "192.168.69.250";  
    $db_connections_siap = array( "UID"=>"sa", "PWD"=>"P4ssw0rd","Database"=>"MIS_IMSSIAP");
    $conn = sqlsrv_connect( $serverName, $db_connections_siap);
    if( $conn === false )  
    {  
         echo "Could not connect.\n";  
         //die( print_r( sqlsrv_errors(), true));  
    }
    $sql = "
    select pfoliocode, pfoliofname from tpfolio where isactive = 1
    ";
    $datacomcode = array();
    $stmt = sqlsrv_query( $conn, $sql);
    if( $stmt === false )  
    {  
         echo "Error in executing query.</br>";  
         //die( print_r( sqlsrv_errors(), true));  
    }else{ 
        while($row = sqlsrv_fetch_array($stmt)){
            $datacomcode[] = $row;
        }
        sqlsrv_free_stmt( $stmt);  
    }
    sqlsrv_close($conn);
    return $datacomcode;
}
//--------------------------------------------------------------------------------
