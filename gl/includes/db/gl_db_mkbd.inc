<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
//--------------------------------------------------------------------------------

// Base function for adding a GL transaction
// $date_ is display date (non-sql)
// $amount is in $currency currency
// if $currency is not set, then defaults to no conversion

function get_mkbd_list($lamp=null)
{
	global $SysPrefs;
	$sql2="SELECT *	FROM `".TB_PREF."mkbd` ";
	if($lamp!=null)
		$sql2.="WHERE lamp=".db_escape($lamp)." " ;
	$result=db_query($sql2, '');
	//return true;	
	return $result;
}
function get_bank_curr($account)
{
	global $SysPrefs;
	$sql2="SELECT bank_curr_code
	FROM `".TB_PREF."bank_accounts` WHERE account_code like '".db_escape($lamp)."' " ;
	$result=db_query($sql2, '');
	//return true;

	$row = db_fetch_row($result);
	return $row[0];
}
function get_mkbd_coa($id)
{
	global $SysPrefs;
	$sql2="SELECT account FROM `".TB_PREF."mkbd_coa` WHERE mkbd_id = ".db_escape($id)." " ;
	$result=db_query($sql2, '');
	//return true;
	$accounts=array();
	while($row = db_fetch_row($result)){
		$accounts[]=$row[0];
	}
	//return $sql2;
	return $accounts;
}
function add_mkbd_coa($id,$accounts)
{
	global $SysPrefs;
	$sql0="DELETE FROM `0_mkbd_coa` WHERE `mkbd_id` = ".db_escape(@$id)." ;";

	db_query($sql0, $err_msg);
	$sql="INSERT INTO `".TB_PREF."mkbd_coa` (`id`, `mkbd_id`, `account`) VALUES ";
	for($a=0;$a<count($accounts);$a++){
		if($a>0 and $a<(count($accounts)))
			$sql.=",";
		$sql.=" (NULL, ".db_escape(@$id).",".db_escape(@$accounts[$a]).")";
	}
	$err_msg = "The mkbd - coa could not be inserted";
	//return $sql;
	return db_query($sql, $err_msg);
}


