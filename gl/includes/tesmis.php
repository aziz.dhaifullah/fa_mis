<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
    Released under the terms of the GNU General Public License, GPL, 
    as published by the Free Software Foundation, either version 3 
    of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
//192.168.254.44
//10.254.69.98
$path_to_root="../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/ui/items_cart.inc");

function get_tb2_ims($date = '2019-01-24')
{
    $serverName = "192.168.69.32";  
    $db_connections_siap = array( "UID"=>"sa", "PWD"=>"P4ssw0rd","Database"=>"MIS_IMSSIAP");
    $conn = sqlsrv_connect( $serverName, $db_connections_siap);
    if( $conn === false )  
    {  
         echo "Could not connect.\n";  
         //die( print_r( sqlsrv_errors(), true));  
    }

    $sql = "
    select sum(cashposition) as cashpos, coacode, convert(varchar, ledgerdate, 23) as ledgerdate,externalcode,pfoliocode  from fn_TCashLedger_IMS((select tpfolioid from tpfolio),'2019-01-24',0,0) tcl
        left join tcoa tc on tcl.tcoaid = tc.tcoaid
        left join tcoaexternal tce on tcl.tcoaid = tce.tcoaid 
        left join tpfolio folio on tcl.tpfolioid = folio.tpfolioid 
        group by tce.externalcode, tc.coacode, tcl.ledgerdate, folio.pfoliocode
    ";
    $datatb = array();
    $datatb2 = array();
    $stmt = sqlsrv_query( $conn, $sql);  
    if( $stmt === false )  
    {  
         echo "Error in executing query.</br>";  
         //die( print_r( sqlsrv_errors(), true));  
    }  
    
    while($row = sqlsrv_fetch_array($stmt)){
        //print_r($row);
        $tanggal = trim($row['ledgerdate']);
        $pfoliocode = $row['pfoliocode'];
        $externalcode = str_replace('.','',$row['externalcode']);
        $externalcode = str_replace('-','',$externalcode);
        $datatb[$tanggal][$pfoliocode][$externalcode] = $row['cashpos'];
    }
    sqlsrv_free_stmt( $stmt);  
    sqlsrv_close($conn);
    
    return $datatb;
}

function get_comp_code()
{
    $serverName = "192.168.69.32";  
    $db_connections_siap = array( "UID"=>"sa", "PWD"=>"P4ssw0rd","Database"=>"MIS_IMSSIAP");
    $conn = sqlsrv_connect( $serverName, $db_connections_siap);
    if( $conn === false )  
    {  
         echo "Could not connect.\n";  
         //die( print_r( sqlsrv_errors(), true));  
    }
    $sql = "
    select pfoliocode, pfoliofname from tpfolio where isactive = 1
    ";
    $datacomcode = array();
    $stmt = sqlsrv_query( $conn, $sql);
    if( $stmt === false )  
    {  
         echo "Error in executing query.</br>";  
         //die( print_r( sqlsrv_errors(), true));  
    }else{ 
        while($row = sqlsrv_fetch_array($stmt)){
            //print_r($row['pfoliocode']);
            /*$datacompcode['code'] = $row['pfoliocode'];
            $datacompcode['name'] = $row['pfoliofname'];*/
            $datacomcode[] = $row;
            /*$datacomcode['pfoliocode'] = $row['pfoliocode'];
            $datacomcode['pfoliofname'] = $row['pfoliofname'];*/
        }
        sqlsrv_free_stmt( $stmt);  
    }
    sqlsrv_close($conn);
    return $datacomcode;
}

function get_journal_mis($date, $kodesiap)
{
    $serverName = "192.168.69.250";  
    $db_connections_siap = array( "UID"=>"sa", "PWD"=>"P4ssw0rd","Database"=>"MIS_IMSSIAP");
    $conn = sqlsrv_connect( $serverName, $db_connections_siap);
    if( $conn === false )  
    {  
         echo "Could not connect.\n";  
         //die( print_r( sqlsrv_errors(), true));  
    }
    $sql = "
        SELECT PF.pfoliocode,COA.ExternalCode,Amount,IsDebit,convert(varchar, CashJournalDate, 23) as CashJournalDate,TDealID FROM dbo.TCashJournal
        INNER JOIN  dbo.TCOAExternal AS COA ON COA.TCOAID = TCashJournal.TCOAID
        INNER JOIN  dbo.TPFolio AS PF ON PF.TPFolioID = TCashJournal.TPFolioID
        WHERE IsReverse IS  NULL 
        AND PF.pfoliocode = '".$kodesiap."'
        GROUP BY TDealID, PF.pfoliocode, COA.ExternalCode, Amount, IsDebit, CashJournalDate
    ";
    $data = array();
    $stmt = sqlsrv_query( $conn, $sql);
    if( $stmt === false )  
    {  
         echo "Error in executing query.</br>";  
         //die( print_r( sqlsrv_errors(), true));  
    }else{ 
        while($row = sqlsrv_fetch_array($stmt)){
            //print_r($row['pfoliocode']);
            /*$datacompcode['code'] = $row['pfoliocode'];
            $datacompcode['name'] = $row['pfoliofname'];*/
            $tanggal = $row['CashJournalDate'];
            $coa = $row['ExternalCode'];
            $coa = str_replace('.', '', $coa);
            $kodesiap = $row['pfoliocode'];
            $dealid = $row['TDealID'];
            $amount = $row['Amount'];
            if ($row['IsDebit'] == 0) {
                $amount = $amount * (-1);
            }
            $data[$tanggal][$dealid][$coa] = $amount;
            //$data[] = array('tanggal'=>$tanggal, 'coa'=>$coa, 'kodesiap'=>$kodesiap, 'ref'=>$dealid, 'amount'=>$amount); 
            /*$datacomcode['pfoliocode'] = $row['pfoliocode'];
            $datacomcode['pfoliofname'] = $row['pfoliofname'];*/
        }
        sqlsrv_free_stmt( $stmt);  
    }
    sqlsrv_close($conn);

    return $data;
}
$date = '2018-12-31';
$kodesiap = 'CBI';
//$coa = array_keys($data[$trans[0]]);
//echo $countcoa;
//print_r($_SESSION['wa_current_user']);exit;
//print_r($date);
//echo date2sql($date);exit;
/*echo "<br>";
echo $counttrans;
echo "<br>";*/
$data = get_journal_mis($date,$kodesiap);
print_r($data);
$date = array_keys($data);
$countdate = count($date);
for ($d=0; $d < $countdate; $d++) { 
    $transref = array_keys($data[$date[$d]]);
    $counttrans = count($transref);
    $datestring = explode('-', $date[$d]);
    $datestring = $datestring[2].'/'.$datestring[1].'/'.$datestring[0];
    echo '<br>'.$datestring.'<br>';
    for ($i=0; $i < $counttrans; $i++) { 
        $cart = new items_cart(ST_JOURNAL);
        $memo = 'SIAP/'.$transref[$i].'/'.$date[$d];
        echo $memo.'<br>';
        $cart->memo_ = $memo;
        $cart->reference = 'SIAP-'.$transref[$i];
        $cart->tran_date = $cart->doc_date = $cart->event_date = $datestring;
        $coa = array_keys($data[$date[$d]][$transref[$i]]);
        $countcoa = count($coa);
        echo 'SIAP-'.$transref[$i].'<br>';
        for ($a=0; $a < $countcoa; $a++) { 
            echo $coa[$a].'__';
            echo $data[$date[$d]][$transref[$i]][$coa[$a]].'<br>';
            $cart->add_gl_item($coa[$a], '1.5.1','', $data[$date[$d]][$transref[$i]][$coa[$a]], $memo, null, null, $datestring);
            /*$cart->add_gl_item(get_post('res_act'), get_post('dimension_id'),
            get_post('dimension2_id'), $am0, $cart->reference);*/
        }
        write_journal_entries($cart);
        $cart->clear_items();
    }
}


/*$count = count($data);
for ($i=0; $i < $data; $i++) { 
    echo ''
}*/
//echo "<h1>Transaksi pada tanggal ".."</h1>";
//$tanggal = '2018-12-31'
//$json = json_encode($data);
//echo $json;
/*$date = '2019-01-24';
$accode = '114.01.01';
$kodesiap = 'CBI';
print_r($data[$date][$kodesiap][$accode]);*/

