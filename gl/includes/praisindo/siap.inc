<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function get_rpt_pfolio_siap($tgl)
{	
	$server = "192.168.254.44"; 
	$port = "1433";$username = "appims";$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);mssql_select_db('POC_IMS', $conn);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "exec sp_rpt_Pfolio_Val_Rpt_BLSHEET '%', '".$tgl."' ";  
	$stmt = mssql_query( $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$prod=array();$prodid=array();
	while($row = mssql_fetch_array($stmt)){
		if($row['SubTypeCode']=='DEP')
		$prod[]=$row;
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	return $prod;
}
function get_pph_siap($thn,$bln)
{	
	$server = "192.168.254.44"; 
	$port = "1433";$username = "appims";$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);mssql_select_db('POC_IMS', $conn);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$jumHari = cal_days_in_month(CAL_GREGORIAN, $bln, $thn);
	$tsql = "SELECT sum(cpartyfee),sum(pph23amount),b.pfoliocode,pfoliofname,b.cpartycode,cpartyname
FROM dbo.vh_TDeal b
left join dbo.TCParty c on c.tcpartyid=b.tcpartyid
left join TPFolio d on b.tpfolioid=d.tpfolioid
WHERE 
b.DealNo IN (SELECT DISTINCT A.DealNo FROM dbo.vh_TCashJournal A WHERE A.TCOAID IN (339,567) AND A.IsReverse IS NULL)
and b.bookdate >= '".$thn."-".$bln."-01' 
and b.bookdate <= '".$thn."-".$bln."-".$jumHari."' 
group by b.pfoliocode,pfoliofname,b.cpartycode,cpartyname";  
	$stmt = mssql_query( $tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$data=array();  
	while($row = mssql_fetch_array($stmt)){
		$data[]=$row;
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	return $data;
}


function cekkoneksi()
{	
	$server = "192.168.254.44"; 
	$port = "1433";$username = "appims";$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);mssql_select_db('POC_IMS', $conn);
	if( $conn === false )  
	{  
	     return "Could not connect IMS DB.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	} else{
	
	mssql_close( $conn); 
	}
}

function get_mgtfee_siap($tgl,$tgl2)
{	
	$server = "192.168.254.44"; 
	$port = "1433";$username = "appims";$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);
	mssql_select_db('POC_IMS', $conn);
	
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	
	$tgls=explode('-',$tgl2);
	$tsql="select sum(a.cashposition) as mgtfee,0 as sharingfee,d.pfoliofname ,'',c.cpartyname,
convert(varchar,max(a.Date),120) as tgl,d.pfoliocode,max(a.tcurrencyid) as tcurrencyid
from tcashledgerhistory a
inner join tcoa b on a.tcoaid=b.tcoaid
left join TPFolio d on a.tpfolioid=d.tpfolioid
left join dbo.TCParty c on d.tcpartyid=c.tcpartyid
where b.tcoaid in (179)
and a.Date like '".$tgls[0]."-".$tgls[1]."-%'
group by c.cpartyname,d.pfoliocode,d.pfoliofname ";
	
	
//order by feedate desc
	$stmt = mssql_query($tsql); 
	
	
	/* Retrieve and display the results of the query. */
	$data=array();  $pc=array();
	while($row = mssql_fetch_array($stmt)){
		if(!in_array($row[6], $pc)){
			$data[]=$row;
			$pc[]=$row[6];

			//if($row[6]=='MSLF')
			//print_r($row);
		}
	}
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	return $data;

}

//--------------------------------------------------------------------------------
