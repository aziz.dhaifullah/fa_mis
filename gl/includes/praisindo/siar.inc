<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function cekkoneksi_siar()
{	
	$server = "192.168.254.44"; 
	$port = "1433";$username = "appims";$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);mssql_select_db('POC_IMSSIAR', $conn);
	if( $conn === false )  
	{  
	     return "Could not connect IMS DB.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
}
function get_prod_siar()
{	
	////$serverName = "192.168.254.12\sql\2014";  
	////$db_connections_siar = array( "UID"=>"appims", "PWD"=>"App!ims2016","Database"=>"KRESNA_IMSSIAR");
	////$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	$server = "192.168.254.44";
	$port = "1433";
	$username = "appims";
	$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);
	
	mssql_select_db('POC_IMSSIAR', $conn);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	
	$tsql = "select IDProduct,ProductCode,ProductName from TProduct order by IDProduct asc";  
	$stmt = mssql_query($tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$prod=array();$prodid=array();$prodcode=array();
	while($row = mssql_fetch_array($stmt)){
		if (!in_array($row[2], $prod)){
			$prodid[]=$row[0];
			$prodcode[]=$row[1];
			$prod[]=$row[2];
		}
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	return array($prod,$prodid,$prodcode);
}
function get_prod_siar_dp()
{	
	//$serverName = "192.168.254.12\sql\2014";  
	//$db_connections_siar = array( "UID"=>"appims", "PWD"=>"App!ims2016","Database"=>"KRESNA_IMSSIAR");
	//$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	
	$server = "192.168.254.44";
	$port = "1433";
	$username = "appims";
	$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);
	mssql_select_db('POC_IMSSIAR', $conn);
	
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select IDProduct,ProductCode,ProductName,sinvestfundcode from TProduct where idcategory in ('DP','BL') order by IDProduct asc";  
	$stmt = mssql_query($tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$prod=array();$prodid=array();$prodcode=array();
	while($row = mssql_fetch_array($stmt)){
		if (!in_array($row[2], $prod)){
			$prodid[]=$row[0];
			$prodcode[]=$row[1];
			$prodname[]=$row[2];
			$prod[]=$row[2];
		}
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	return array($prod,$prodid,$prodcode);
}
function get_nav_siar($tgl1,$tgl2)
{	
	//$serverName = "192.168.254.12\sql\2014";  
	//$db_connections_siar = array( "UID"=>"appims", "PWD"=>"App!ims2016","Database"=>"KRESNA_IMSSIAR");
	//$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	
	$server = "192.168.254.44";
	$port = "1433";
	$username = "appims";
	$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);
	mssql_select_db('POC_IMSSIAR', $conn);
	
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select a.IDProduct,ProductCode,ProductName,convert(varchar,b.navdate,120),b.value 
from TProduct a 
inner join TNAV b on a.IDProduct=b.IDProduct
where b.NAVDate>='".$tgl1."' and b.NAVDate<='".$tgl2."' order by b.navdate,ProductName";  
	$stmt = mssql_query($tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$prod=array();$prodid=array();$navdate=array();$value=array();
	$a=0;$b=0;
	while($row = mssql_fetch_array($stmt)){

		$value[$row[3]][$row[2]]=$row[4];
		if (!in_array($row[3], $navdate)){
			$navdate[]=$row[3];
			$a++;
		}
		if (!in_array($row[2], $prod)){
			$prodid[]=$row[0];
			$prod[]=$row[2];
			$b++;
		}
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	return array($prod,$prodid,$navdate,$value);
}
function get_nab_siar($prodid,$tgl2)
{	
	//$serverName = "192.168.254.12\sql\2014";  
	//$db_connections_siar = array( "UID"=>"appims", "PWD"=>"App!ims2016","Database"=>"KRESNA_IMSSIAR");
	//$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	
	$server = "192.168.254.44";
	$port = "1433";
	$username = "appims";
	$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);
	mssql_select_db('POC_IMSSIAR', $conn);
	
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select value from TNAV where idproduct=".$prodid." and NAVDate='".$tgl2."' ";  
	$stmt = mssql_query($tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$row = mssql_fetch_array($stmt);
	//print_r($prod);  

	/* Free statement and connection resources. */  
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	return $row[0];
	//return $tsql;
}
function get_unit_siar($prodid,$tgl)
{	
	//$serverName = "192.168.254.12\sql\2014";  
	//$db_connections_siar = array( "UID"=>"appims", "PWD"=>"App!ims2016","Database"=>"KRESNA_IMSSIAR");
	//$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	
	$server = "192.168.254.44";
	$port = "1433";$username = "appims";$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);mssql_select_db('POC_IMSSIAR', $conn);
	
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select sum(balanceunit) from fn_TOutstandingHistoryFIFO_GetBalancePerProduct(null,".$prodid.",'".$tgl."')";  
	$stmt = mssql_query($tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$prod=array();$prodid=array();$navdate=array();$value=array();
	$row = mssql_fetch_array($stmt);
	//print_r($prod);  

	/* Free statement and connection resources. */  
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	return $row[0];
}

function get_aum_siar($prodid,$tgl)
{	
	//$serverName = "192.168.254.12\sql\2014";  
	//$db_connections_siar = array( "UID"=>"appims", "PWD"=>"App!ims2016","Database"=>"KRESNA_IMSSIAR");
	//$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	
	$server = "192.168.254.44";
	$port = "1433";
	$username = "appims";
	$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);
	mssql_select_db('POC_IMSSIAR', $conn);
	
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select a.OutstandingUnits,a.navvalue
		from TMGTFeeHistoryStructure a 
		where a.ProductID=".$prodid." and a.NAVDate='".$tgl."'
		order by feedate desc";  
	$stmt = mssql_query($tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$row = mssql_fetch_array($stmt);
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	//return $tsql;
	return $row;
}


function get_mgtfee($tgl,$tgl2)
{	
	//$serverName = "192.168.254.12\sql\2014";  
	//$db_connections_siar = array( "UID"=>"appims", "PWD"=>"App!ims2016","Database"=>"KRESNA_IMSSIAR");
	//$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	
	$server = "192.168.254.44";
	$port = "1433";
	$username = "appims";
	$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);
	mssql_select_db('POC_IMSSIAR', $conn);
	
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql="select sum(a.mgtfee) as mgtfee,a.sharingfee,b.productname,c.nameagent,f.custodyname,convert(varchar,max(a.feedate),120) as tgl
from dbo.TMGTFeeHistoryStructure a 
inner join TProduct b on a.ProductID=b.IDProduct 
left join TAgent c on a.agentid=c.agentid 
left join TSubAccount d on a.subaccountid=d.idsubaccount
left join TCustomer e on d.idcustomer=e.IDCustomer
inner join trefbankcustody f on b.idbankcustody=f.idbankcustody 
where a.feedate >= '".$tgl."' and a.feedate <= '".$tgl2."' and a.mgtfee>0
group by a.sharingfee,b.productname,c.nameagent,f.custodyname";
	/*
	$tsql = "select sum(a.mgtfee) as mgtfee,a.sharingfee,b.productname,c.nameagent,e.idcustomer,e.firstname
from TMGTFeeHistory a 
inner join TProduct b on a.ProductID=b.IDProduct 
left join TAgent c on a.agentid=c.agentid 
left join TSubAccount d on a.subaccountid=d.idsubaccount
left join TCustomer e on d.idcustomer=e.IDCustomer
where a.feedate >= '".$tgl."' and a.feedate <= '".$tgl2."'
group by a.sharingfee,b.productname,c.nameagent,e.idcustomer,e.firstname ";  
*/
//order by feedate desc
	$stmt = mssql_query($tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */
	$data=array();  
	while($row = mssql_fetch_array($stmt)){
		$data[]=$row;
	}
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	//return $tsql;
	return $data;
}
function get_shrfee($tgl,$tgl2,$prod=null)
{	
	//$serverName = "192.168.254.12\sql\2014";  
	//$db_connections_siar = array( "UID"=>"appims", "PWD"=>"App!ims2016","Database"=>"KRESNA_IMSSIAR");
	//$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	
	$server = "192.168.254.44";
	$port = "1433";
	$username = "appims";
	$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);
	mssql_select_db('POC_IMSSIAR', $conn);
	
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	if($prod!=null)
		$where="And a.ProductID=".$prod." ";
	/*
	$tsql = "select a.sharingfee,b.productname,c.nameagent,e.idcustomer,e.firstname
from TSharingFeeHistory a 
inner join TProduct b on a.ProductID=b.IDProduct 
left join TAgent c on a.agentid=c.agentid 
left join TSubAccount d on a.subaccountid=d.idsubaccount
left join TCustomer e on d.idcustomer=e.IDCustomer
where a.feedate >= '".$tgl."' and a.feedate <= '".$tgl2."' ".$where."
group by a.sharingfee,b.productname,c.nameagent,e.idcustomer,e.firstname";  
*/
	$tsql="select convert(varchar,a.navdate,120),a.balanceunits,a.navvalue,a.sharingfee,e.unitholderidno
from TSharingFeeHistory a 
inner join TProduct b on a.ProductID=b.IDProduct 
left join TSubAccount d on a.subaccountid=d.idsubaccount
left join TCustomer e on d.idcustomer=e.IDCustomer
where a.feedate >= '".$tgl."' and a.feedate <= '".$tgl2."' 
".$where." order by a.navdate asc";
//order by feedate,b.productname desc
	$stmt = mssql_query($tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */
	$data=array();  
	while($row = mssql_fetch_array($stmt)){
		$data[]=$row;
	}
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	//return $tsql;
	return $data;
}
function get_sa_prod_shrfee($sa)
{	
	//$serverName = "192.168.254.12\sql\2014";  
	//$db_connections_siar = array( "UID"=>"appims", "PWD"=>"App!ims2016","Database"=>"KRESNA_IMSSIAR");
	//$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	
	$server = "192.168.254.44";
	$port = "1433";
	$username = "appims";
	$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);
	mssql_select_db('POC_IMSSIAR', $conn);
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select a.ProductID,b.productname
from TSharingFeeHistory a 
inner join TProduct b on a.ProductID=b.IDProduct 
left join TSubAccount d on a.subaccountid=d.idsubaccount
where d.idcustomer=".$sa."
group by a.ProductID,b.productname";  
//order by feedate,b.productname desc
	$stmt = mssql_query($tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */
	$data=array();  
	while($row = mssql_fetch_array($stmt)){
		$data[]=$row;
	}
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	//return $tsql;
	return $data;
}
function get_sa_shrfee()
{	
	//$serverName = "192.168.254.12\sql\2014";  
	//$db_connections_siar = array( "UID"=>"appims", "PWD"=>"App!ims2016","Database"=>"KRESNA_IMSSIAR");
	//$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	
	$server = "192.168.254.44";
	$port = "1433";
	$username = "appims";
	$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);
	mssql_select_db('POC_IMSSIAR', $conn);
	
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select e.idcustomer,e.firstname
from TSharingFeeHistory a 
inner join TProduct b on a.ProductID=b.IDProduct 
left join TSubAccount d on a.subaccountid=d.idsubaccount
left join TCustomer e on d.idcustomer=e.IDCustomer
group by e.idcustomer,e.firstname";  
//order by feedate,b.productname desc
	$stmt = mssql_query($tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */
	$data=array();  
	while($row = mssql_fetch_array($stmt)){
		$data[]=$row;
	}
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	//return $tsql;
	return $data;
}


function get_client_siar()
{	
	//$serverName = "192.168.254.12\sql\2014";  
	//$db_connections_siar = array( "UID"=>"appims", "PWD"=>"App!ims2016","Database"=>"KRESNA_IMSSIAR");
	//$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	
	$server = "192.168.254.44";
	$port = "1433";
	$username = "appims";
	$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);
	mssql_select_db('POC_IMSSIAR', $conn);
	
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select IDCustomer,FirstName from TCustomer";  
	$stmt = mssql_query($tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$prod=array();$prodid=array();$prodcode=array();
	while($row = mssql_fetch_array($stmt)){
		if (!in_array($row[2], $prod)){
			$prodid[]=$row[0];
			$prod[]=$row[1];
		}
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	return array($prod,$prodid);
}
function get_agent_siar()
{	
	//$serverName = "192.168.254.12\sql\2014";  
	//$db_connections_siar = array( "UID"=>"appims", "PWD"=>"App!ims2016","Database"=>"KRESNA_IMSSIAR");
	//$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	
	$server = "192.168.254.44";
	$port = "1433";
	$username = "appims";
	$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);
	mssql_select_db('POC_IMSSIAR', $conn);
	
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select agentid,nameagent from TAgent where codeagent!=''";  
	$stmt = mssql_query($tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$prod=array();$prodid=array();$prodcode=array();
	while($row = mssql_fetch_array($stmt)){
		if (!in_array($row[2], $prod)){
			$prodid[]=$row[0];
			$prod[]=$row[1];
		}
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	return array($prod,$prodid);
}
function get_clientagent_siar()
{	
	//$serverName = "192.168.254.12\sql\2014";  
	//$db_connections_siar = array( "UID"=>"appims", "PWD"=>"App!ims2016","Database"=>"KRESNA_IMSSIAR");
	//$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	
	$server = "192.168.254.44";
	$port = "1433";
	$username = "appims";
	$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);
	mssql_select_db('POC_IMSSIAR', $conn);
	
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select idcustomer,agentid from tagentcustomer order by agentid asc ";  
	$stmt = mssql_query($tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	/* Retrieve and display the results of the query. */  
	$prod=array();$prodid=array();$prodcode=array();
	while($row = mssql_fetch_array($stmt)){
		if (!in_array($row[2], $prod)){
			$prodid[]=$row[0];
			$prod[]=$row[1];
		}
	}
	//print_r($prod);  

	/* Free statement and connection resources. */  
	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	return array($prodid,$prod);
}


function n_holiday_period($tgl1,$tgl2)
{	
	//$serverName = "192.168.254.12\sql\2014";  
	//$db_connections_siar = array( "UID"=>"appims", "PWD"=>"App!ims2016","Database"=>"KRESNA_IMSSIAR");
	//$conn = sqlsrv_connect( $serverName, $db_connections_siar);
	
	$server = "192.168.254.44";
	$port = "1433";
	$username = "appims";
	$password = "App1ms!2016";
	$conn = mssql_connect($server, $username, $password);
	mssql_select_db('POC_IMSSIAR', $conn);
	
	if( $conn === false )  
	{  
	     return "Could not connect.\n";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	$tsql = "select count(idholiday) from tholiday where holidaydate >= '".$tgl1."' and holidaydate <= '".$tgl2."' ";  
	$stmt = mssql_query($tsql);  
	if( $stmt === false )  
	{  
	     return "Error in executing query.</br>";  
	     //die( print_r( sqlsrv_errors(), true));  
	}  
	 
	$row = mssql_fetch_array($stmt);
	//print_r($prod);  


	mssql_free_result( $stmt);  
	mssql_close( $conn); 
	return $row[0];
}

//--------------------------------------------------------------------------------
