<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");

page(_($help_context = "General Ledger Transaction Details"), true);

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

if (!isset($_GET['type_id']) || !isset($_GET['trans_no'])) 
{ /*Script was not passed the correct parameters */

	display_note(_("The script must be called with a valid transaction type and transaction number to review the general ledger postings for."));
	end_page();
}

function display_gl_heading($myrow)
{
	global $systypes_array;

	$trans_name = $systypes_array[$_GET['type_id']];
	$journal = $_GET['type_id'] == ST_JOURNAL;

    start_table(TABLESTYLE, "width='95%'");
    
	global $path_to_root;
	
	//$bank = get_gl_bank($myrow["reference"]);
	echo "<tr><td><table class='callout_main' border='0' cellpadding='0' cellspacing='0' style='width:15% !important;'>";
	echo "<tr><td class='menu_group_items'>";
	echo "<img src='".$path_to_root."/themes/default/images/logo_frontaccounting.png' alt='FrontAccounting' height='20'  border='0' style='margin: auto;display: block;'>";
	echo "</td></tr></table></td>";
	echo '<td><table align="right">';
	echo "<tr><td>Date</td><td>".sql2date($myrow["doc_date"])."</td></tr>";
	echo "<tr><td>Reff Number</td><td>".$myrow["kode"].'-'.$myrow["reference"]."</td></tr>";
	echo "</td></table>";
	echo "</tr>";
	echo '<tr><td colspan="2" style="text-align:center;"><strong>'.$myrow["bank_name"].' Transaction Slip</strong></td></tr>';
	
    end_table(1);
	
}

$result = get_gl_trans2($_GET['type_id'], $_GET['trans_no']);
if (db_num_rows($result) == 0)
{
    echo "<p><center>" . _("No general ledger transactions have been created for") . " " .$systypes_array[$_GET['type_id']]." " . _("number") . " " . $_GET['trans_no'] . "</center></p><br><br>";
	end_page(true);
	exit;
}

/*show a table of the transactions returned by the sql */
$dim = get_company_pref('use_dimension');

	$th = array(_("Account Code"), _("Amount"), _("Description"));

$k = 0; //row colour counter
$heading_shown = false;

$credit = $debit = 0;
while ($myrow = db_fetch($result)) 
{
	if ($myrow['amount'] == 0) continue;
	if (!$heading_shown)
	{
		display_gl_heading($myrow);
		start_table(TABLESTYLE, "width='95%'");
		table_header($th);
		$heading_shown = true;
	}

	alt_table_row_color($k);

	$counterpartyname = get_subaccount_name($myrow["account"], $myrow["person_id"]);
	$counterparty_id = $counterpartyname ? sprintf(' %05d', $myrow["person_id"]) : '';

    //label_cell(sql2date($myrow['tran_date']));
    label_cell($myrow['account'].$counterparty_id);
	//label_cell($myrow['account_name'] . ($counterpartyname ? ': '.$counterpartyname : ''));
	/*if ($dim >= 1)
		label_cell(get_dimension_string($myrow['dimension_id'], true));
	if ($dim > 1)
		label_cell(get_dimension_string($myrow['dimension2_id'], true));
*/
	label_cell(($myrow['amount']*(-1)));
	//display_debit_or_credit_cells($myrow['amount']);
	label_cell($myrow['memo_']);
	end_row();
    //if ($myrow['amount'] > 0 ) 
    	$debit += ($myrow['amount']*(-1));
    //else 
    	$credit += ($myrow['amount']*(-1));
	$ref=$myrow["reference"];
}

if ($heading_shown)
{
    start_row("class='inquirybg' style='font-weight:bold'");
    label_cell(_("Total"), "");
    /*if ($dim >= 1)
        label_cell('');
    if ($dim > 1)
        label_cell('');
	*/
    amount_cell($debit);
    //amount_cell(-$credit);
    label_cell('');
    end_row();
	end_table(1);
}

//end of while loop

is_voided_display($_GET['type_id'], $_GET['trans_no'], _("This transaction has been voided."));
echo '<center>
<a href="../../reporting/rep1701.php?ref='.$ref.'" target="_blank" aspect="default process" name="Rep1701" id="Rep1701" value="Print">
<span>Print</span></a></center>';
end_page(false, false, false, $_GET['type_id'], $_GET['trans_no']);
