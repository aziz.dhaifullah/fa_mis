<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");

page(_($help_context = "General Ledger Transaction Details"), true);

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

if (!isset($_GET['type_id']) || !isset($_GET['trans_no'])) 
{ /*Script was not passed the correct parameters */

	display_note(_("The script must be called with a valid transaction type and transaction number to review the general ledger postings for."));
	end_page();
}
if(isset($_GET['delete'])){
	start_table(TABLESTYLE, "width='95%'");
	gl_void($_GET['type_id'], $_GET['trans_no']);
	echo '<div id="msgbox">
			<div class="note_msg">
				Delete Success.
			</div>
		</div>';
}else{

	function display_gl_heading($myrow)
	{
		global $systypes_array;

		$trans_name = $systypes_array[$_GET['type_id']];
		$journal = $_GET['type_id'] == ST_JOURNAL;

	    start_table(TABLESTYLE, "width='95%'");
	    $th = array(_("General Ledger Transaction Details"), _("Reference"),
	    	_("Transaction Date"), _("Journal #"));

		if ($_GET['type_id'] == ST_JOURNAL)
			array_insert($th, 3, array(_("Document Date"), _("Event Date")));
		else
			array_insert($th, 3, array(_("Counterparty")));
		
		if($myrow['supp_reference'])
		{
			array_insert($th, 2, array(_("Supplier Reference")));
		}
	    table_header($th);	
	    start_row();	
	    label_cell("$trans_name #" . $_GET['trans_no']);
	    label_cell($myrow["reference"], "align='center'");
		if($myrow['supp_reference'])
		{
		label_cell($myrow["supp_reference"], "align='center'");
		}
		label_cell(sql2date($myrow["doc_date"]), "align='center'");
		if ($journal)
		{
			$header = get_journal($myrow['type'], $_GET['trans_no']);
			label_cell($header["doc_date"] == '0000-00-00' ? '-' : sql2date($header["doc_date"]), "align='center'");
			label_cell($header["event_date"] == '0000-00-00' ? '-' : sql2date($header["event_date"]), "align='center'");
		} else
			label_cell(get_counterparty_name($_GET['type_id'],$_GET['trans_no']));
		label_cell( get_journal_number($myrow['type'], $_GET['trans_no']), "align='center'");
		end_row();

		start_row();
		label_cells(_('Entered By'), $myrow["real_name"], "class='tableheader2'", "colspan=" .
			 ($journal ? ($header['rate']==1 ? '3':'1'):'6'));
		if ($journal)
		{
			if ($header['rate'] != 1)
				label_cells(_('Exchange rate'), $header["rate"].' ', "class='tableheader2'");
			label_cells(_('Source document'), $header["source_ref"], "class='tableheader2'");
		}
		end_row();
		comments_display_row($_GET['type_id'], $_GET['trans_no']);
	    end_table(1);
	}
	$result = get_gl_trans($_GET['type_id'], $_GET['trans_no']);

	if (db_num_rows($result) == 0)
	{
	    echo "<p><center>" . _("No general ledger transactions have been created for") . " " .$systypes_array[$_GET['type_id']]." " . _("number") . " " . $_GET['trans_no'] . "</center></p><br><br>";
		end_page(true);
		exit;
	}

	/*show a table of the transactions returned by the sql */
	$dim = get_company_pref('use_dimension');

	if ($dim == 2)
		$th = array(_("Journal Date"), _("Account Code"), _("Account Name"), _("Dimension")." 1", _("Dimension")." 2",
			_("Debit"), _("Credit"), _("Memo"));
	elseif ($dim == 1)
		$th = array(_("Journal Date"), _("Account Code"), _("Account Name"), _("Dimension"),
			_("Debit"), _("Credit"), _("Memo"));
	else		
		$th = array(_("Journal Date"), _("Account Code"), _("Account Name"),
			_("Debit"), _("Credit"), _("Memo"));

	$k = 0; //row colour counter
	$heading_shown = false;
	$last=array();$banks=array();
	$b=0;$c=0;
	$credit = $debit = 0;
	while ($myrow = db_fetch($result)) 
	{
		if ($myrow['amount'] == 0) continue;
		if (!$heading_shown)
		{
			display_gl_heading($myrow);
			start_table(TABLESTYLE, "width='95%'");
			table_header($th);
			$heading_shown = true;
		}
		$nbank=0;
		$sql0="select count(id) as nbank
		from 0_bank_accounts
		where account_code like '".@$myrow["account"]."' ";
		$query0=db_query($sql0, "The transactions for could not be retrieved");
		while ($myrow0 = db_fetch($query0))
		{
			$nbank=@$myrow0['nbank'];
			if($nbank>0)
			$banks[]=array($myrow["account"],$myrow["person_id"],$myrow['tran_date'],$myrow['account_name'],$myrow['dimension_id'],$myrow['dimension2_id'],$myrow['amount'],$myrow['memo_'],$k);
			
		}
		
		//$nbank=0;
		
		if($myrow["account"]=='93000' or $nbank>0){
			if($myrow["account"]=='93000'){
			$last[$c]=array($myrow["account"],$myrow["person_id"],$myrow['tran_date'],$myrow['account_name'],$myrow['dimension_id'],$myrow['dimension2_id'],$myrow['amount'],$myrow['memo_'],$k);
			$c++;
			}
		}else{
			alt_table_row_color($k);
			
			$counterpartyname = get_subaccount_name($myrow["account"], $myrow["person_id"]);
			$counterparty_id = $counterpartyname ? sprintf(' %05d', $myrow["person_id"]) : '';
			$counterpartyname= '';$counterparty_id= '';
		    label_cell(sql2date($myrow['tran_date']));
		    label_cell($myrow['account'].$counterparty_id);
			label_cell($myrow['account_name'] . ($counterpartyname ? ': '.$counterpartyname : ''));
			if ($dim >= 1)
				label_cell(get_dimension_string($myrow['dimension_id'], true));
			if ($dim > 1)
				label_cell(get_dimension_string($myrow['dimension2_id'], true));

			display_debit_or_credit_cells($myrow['amount']);
			label_cell($myrow['memo_']);
			end_row();
		    if ($myrow['amount'] > 0 ) 
		    	$debit += $myrow['amount'];
		    else 
		    	$credit += $myrow['amount'];
		}
	}
	//print_r($last);
	//print_r($banks);
	if(count($last)>0){
		for($a=0;$a<count($last);$a++){
			alt_table_row_color(@$last[$a][8]);
			//print_r($last[$a]);echo '<br>';
			$counterpartyname = get_subaccount_name(@$last[$a][0], @$last[$a][1]);
			$counterparty_id = $counterpartyname ? sprintf(' %05d', @$last[$a][1]) : '';
			$counterpartyname= '';$counterparty_id= '';
		    label_cell(sql2date(@$last[$a][2]));
		    label_cell(@$last[$a][0].$counterparty_id);
			label_cell(@$last[$a][3] . ($counterpartyname ? ': '.$counterpartyname : ''));
			if ($dim >= 1)
				label_cell(get_dimension_string(@$last[$a][4], true));
			if ($dim > 1)
				label_cell(get_dimension_string(@$last[$a][5], true));

			display_debit_or_credit_cells(@$last[$a][6]);
			label_cell(@$last[$a][7]);
			end_row();
		    if (@$last[$a][7] > 0 ) 
		    	$debit += @$last[$a][7];
		    else 
		    	$credit += @$last[$a][7];
		}
	}
	//print_r($banks);

	if(count($banks)>0){
		for($a=0;$a<count($banks);$a++){
			alt_table_row_color(@$banks[$a][8]);
		
			$counterpartyname = get_subaccount_name(@$banks[$a][0], @$banks[$a][1]);
			$counterparty_id = $counterpartyname ? sprintf(' %05d', @$banks[$a][1]) : '';
			$counterpartyname= '';$counterparty_id= '';
		    label_cell(sql2date(@$banks[$a][2]));
		    label_cell(@$banks[$a][0].$counterparty_id);
			label_cell(@$banks[$a][3] . ($counterpartyname ? ': '.$counterpartyname : ''));
			if ($dim >= 1)
				label_cell(get_dimension_string(@$banks[$a][4], true));
			if ($dim > 1)
				label_cell(get_dimension_string(@$banks[$a][5], true));

			display_debit_or_credit_cells(@$banks[$a][6]);
			label_cell(@$banks[$a][7]);
			end_row();
		    if (@$banks[$a][7] > 0 ) 
		    	$debit += @$banks[$a][7];
		    else 
		    	$credit += @$banks[$a][7];
		}
	}

	if ($heading_shown)
	{
	    /*start_row("class='inquirybg' style='font-weight:bold'");
	    label_cell(_("Total"), "colspan=3");
	    if ($dim >= 1)
	        label_cell('');
	    if ($dim > 1)
	        label_cell('');
	    amount_cell($debit);
	    amount_cell(-$credit);
	    label_cell('');
	    end_row();
	    */
		end_table(1);
	}

	//end of while loop

	is_voided_display($_GET['type_id'], $_GET['trans_no'], _("This transaction has been voided."));
	$voucher='<center>
		<a class="export" target="_blank" href="../../reporting/rep1701.php?type_id='.$_GET["type_id"].'&trans_no='.$_GET["trans_no"].'">
			<img src="../../themes/default/images/print.png" style="margin-top:3px;vertical-align:middle;width:12px;height:12px;border:0;" title="Print Remittance"> Print Voucher
		</a></center>';
	echo $voucher;
	$editl='';
	$sqlcekposting = "SELECT COUNT(posting) as cekpos FROM ".TB_PREF."gl_trans WHERE type =".$_GET["type_id"]." AND type_no = ".$_GET["trans_no"]." AND posting = 0";
	$result = db_query($sqlcekposting) ;
	$cekposting = db_fetch($result);
	//print_r($cekposting);
	if ($cekposting['cekpos'] != 0) {
		if($_GET['type_id']==0)
			$link='../../gl/gl_journal.php?ModifyGL=Yes&trans_no='.$_GET["trans_no"].'&trans_type='.$_GET["type_id"];
		if($_GET['type_id']==1)
			$link='../../gl/gl_bank.php?ModifyPayment=Yes&trans_no='.$_GET["trans_no"].'&trans_type='.$_GET["type_id"];
		if($_GET['type_id']==2)
			$link='../../gl/gl_bank.php?ModifyDeposit=Yes&trans_no='.$_GET["trans_no"].'&trans_type='.$_GET["type_id"];

		$linkv='../../gl/view/gl_trans_view.php?delete=yes&type_id='.$_GET["type_id"].'&trans_no='.$_GET["trans_no"];

		$editl='<center>
			<a class="export" target="_blank" href="'.$link.'">
				<img src="../../themes/default/images/edit.gif" style="margin-top:3px;vertical-align:middle;width:12px;height:12px;border:0;" title="Edit"> Edit
			</a></center>';

		$void='<center>
			<a class="export" target="_parent" href="'.$linkv.'" onclick="return confirm(\'Anda Yakin Untuk Menghapus Jurnal?\')">
				<img src="../../themes/default/images/delete.gif" style="margin-top:3px;vertical-align:middle;width:12px;height:12px;border:0;" title="Delete"> Delete
			</a></center>';
		echo $editl;
		echo $void;
	}

	end_page(false, false, false, $_GET['type_id'], $_GET['trans_no']);
}