<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_EXCHANGERATE';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/banking.inc");

$js = "";
if (user_use_date_picker())
	$js .= get_js_date_picker();
page(_($help_context = "Exchange Rates"), false, false, "", $js);

simple_page_mode(false);

//---------------------------------------------------------------------------------------------
function check_data($selected_id)
{
	if (!is_date($_POST['date_']))
	{
		display_error( _("The entered date is invalid."));
		set_focus('date_');
		return false;
	}
	if (input_num('BuyRate') <= 0)
	{
		display_error( _("The exchange rate cannot be zero or a negative number."));
		set_focus('BuyRate');
		return false;
	}
	if (!$selected_id && get_date_exchange_rate($_POST['curr_abrev'], $_POST['date_']))
	{
		display_error( _("The exchange rate for the date is already there."));
		set_focus('date_');
		return false;
	}
	return true;
}

//---------------------------------------------------------------------------------------------

function handle_submit()
{
	global $selected_id;
	//print_r($_POST['curr_abrev']);exit;
	if (!check_data($selected_id))
		return false;

	if ($selected_id != "")
	{

		update_exchange_rate($_POST['curr_abrev'], $_POST['date_'],
		input_num('BuyRate'), '', input_num('TaxRate'));
	}
	else
	{

		add_exchange_rate($_POST['curr_abrev'], $_POST['date_'],
		    input_num('BuyRate'), '', input_num('TaxRate'));
	}

	$selected_id = '';
	clear_data();
}

//---------------------------------------------------------------------------------------------

function handle_delete()
{
	global $selected_id;

	if ($selected_id == "")
		return;
	delete_exchange_rate($selected_id);
	$selected_id = '';
	clear_data();
}

//---------------------------------------------------------------------------------------------
function edit_link($row) 
{
  return button('Edit'.$row["id"], _("Edit"), true, ICON_EDIT);
}

function del_link($row) 
{
  return button('Delete'.$row["id"], _("Delete"), true, ICON_DELETE);
}

function display_rates($curr_code)
{

}

//---------------------------------------------------------------------------------------------

function display_rate_edit()
{
	global $selected_id, $Ajax, $SysPrefs;
	$xchg_rate_provider = ((isset($SysPrefs->xr_providers) && isset($SysPrefs->dflt_xr_provider))
		? $SysPrefs->xr_providers[$SysPrefs->dflt_xr_provider] : 'BI');
	start_table(TABLESTYLE2);

	if ($selected_id != "")
	{
		//editing an existing exchange rate

		$myrow = get_exchange_rate($selected_id);

		$_POST['date_'] = sql2date($myrow["date_"]);
		$_POST['BuyRate'] = maxprec_format($myrow["rate_buy"]);
		$_POST['TaxRate'] = maxprec_format($myrow["tax_rate"]);

		hidden('selected_id', $selected_id);
		hidden('date_', $_POST['date_']);

		label_row(_("Date to Use From:"), $_POST['date_']);
	}
	else
	{
		/*if (!isset($_POST['date_']))
			$_POST['date_'] = Today();*/

		$_POST['BuyRate'] = '';
		$_POST['TaxRate'] = '';
		date_row(_("Date to Use From:"), 'date_');
	}
	if (isset($_POST['get_rate']))
	{
		//print_r($_POST['date_']);exit;
		$opts = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Accept-language: en\r\n" .
					  "Content-Type: text/xml\r\n".
					  "Cookie: foo=bar\r\n"
		  )
		);

		/*$context = stream_context_create($opts);
		$response =  htmlentities(file_get_contents('http://www.bi.go.id/id/moneter/informasi-kurs/transaksi-bi/Default.aspx',false,$context));
		//$response =  htmlentities(file_get_contents('https://www.bi.go.id/id/moneter/informasi-kurs/transaksi-bi/Default.aspx',false,$context));
		//echo $response;exit;
		$pos=strpos($response,'ctl00_PlaceHolderMain_biWebKursTransaksiBI_GridView1');
		$tbl=substr($response,$pos,100000);
		$tbl=str_replace('</tr>','',$tbl);
		$pos2=14694;
		$jual=str_replace(',','',substr($tbl,$pos2,9));
		$jual=str_replace(';','',$jual);
		$jual=str_replace('t','',$jual);
		$pos3=14760;
		$beli=str_replace(',','',substr($tbl,$pos3,9));
		$beli=str_replace(';','',$beli);
		$beli=str_replace('t','',$beli);
		$tengah=(intval($jual)+intval($beli))/2;*/
		$tgls=explode('/', $_POST['date_']);
		$tanggalstring = array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$tanggal = intval($tgls[0]);
		$bulan = $tanggalstring[intval($tgls[1])];
		$tahun = $tgls[2];
		$response =  htmlentities(file_get_contents('https://kursdollar.net/history-kurs/'.$tahun.'/'.$bulan.'/'.$tanggal.'/',false,$context));
		$pos=strpos($response,'US Dollar');
		$tbl=$response;
		$pos2=$pos+180;
		$beli=str_replace(',','',substr($tbl,$pos2,8));
		$beli=str_replace(';','',$beli);
		$beli=str_replace('t','',$beli);
		$beli=str_replace('.','',$beli);
		$pos3=strpos($tbl,'Jual')+82;
		$jual=str_replace(',','',substr($tbl,$pos3,8));
		$jual=str_replace(';','',$jual);
		$jual=str_replace('t','',$jual);
		$jual=str_replace('.','',$jual);
		$tengah=(intval($jual)+intval($beli))/2;

		//echo $tengah;exit;
		if(intval($tengah)==0)
			{
			// no stored exchange rate, just return 1
			display_error(
				sprintf(_("Cannot retrieve BI rate for currency %s as of %s. Please add exchange rate manually on Exchange Rates page."),
					 $currency_code, $date_));
			$_POST['BuyRate'] = 1.000;
		}
		$_POST['BuyRate'] = maxprec_format($tengah);
		$Ajax->activate('BuyRate');
	}
	if (isset($_POST['get_rate2']))
	{
		/*$opts = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Accept-language: en\r\n" .
					  "Content-Type: text/xml\r\n".
					  "Cookie: foo=bar\r\n"
		  )
		);

		$context = stream_context_create($opts);
		$response =  htmlentities(file_get_contents('http://www.fiskal.kemenkeu.go.id/dw-kurs-db.asp',false,$context));
		
		$pos=strpos($response,'Amerika Serikat (USD)');
		$tbl=substr($response,$pos,78);
		$tbl=str_replace('Amerika Serikat (USD)','',$tbl);
		$tengah=str_replace(',','',substr($tbl,40,9));*/
		$tgls=explode('/', $_POST['date_']);
		$tanggal = $tgls[0];
		$bulan = $tgls[1];
		$tahun = $tgls[2];
		$strDate = $tahun.$bulan.$tanggal;
		$postdata = http_build_query(
		    array(
		        'strDate' => $strDate,
		        'id' => $date_
		    )
		);

		$opts2 = array('http' =>
		    array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/x-www-form-urlencoded',
		        'content' => $postdata
		    )
		);

		$context  = stream_context_create($opts2);

		$response = file_get_contents('http://www.fiskal.kemenkeu.go.id/dw-kurs-db.asp', false, $context);
		$pos=strpos($response,'Dolar Amerika Serikat');
		$tbl=$response;
		$pos2=$pos+55;
		$tengah=str_replace(',','',substr($tbl,$pos2,7));
		$tengah=str_replace(';','',$tengah);
		$tengah=str_replace('t','',$tengah);
		$tengah=str_replace('.','',$tengah);
		if(intval($tengah)==0)
			{
			// no stored exchange rate, just return 1
			display_error(
				sprintf(_("Cannot retrieve Pajak rate for currency %s as of %s. Please add exchange rate manually on Exchange Rates page."),
					 $currency_code, $date_));
			$_POST['TaxRate'] = 1.000;
		}
		$_POST['TaxRate'] = maxprec_format($tengah);
		$Ajax->activate('TaxRate');
	}
	amount_row(_("BI Rate:"), 'BuyRate', null, '',
	  	submit('get_rate',_("Get"), false, _('Get current rate from BI') , true), 'max');
	amount_row(_("Pajak Rate:"), 'TaxRate', null, '',
	  	submit('get_rate2',_("Get"), false, _('Get current rate from Pajak') , true), 'max');

	end_table(1);

	submit_add_or_update_center($selected_id == '', '', 'both');

	display_note(_("Exchange rates are entered against the company currency."), 1);
}




//---------------------------------------------------------------------------------------------

function clear_data()
{
	unset($_POST['selected_id']);
	unset($_POST['date_']);
	unset($_POST['BuyRate']);
	unset($_POST['TaxRate']);
}

//---------------------------------------------------------------------------------------------

if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
	handle_submit();

//---------------------------------------------------------------------------------------------

if ($Mode == 'Delete')
	handle_delete();


//---------------------------------------------------------------------------------------------

start_form();

if (!isset($_POST['curr_abrev']))
	$_POST['curr_abrev'] = get_global_curr_code();
	//$_POST['curr_abrev'] = get_global_curr_code();

/*echo "<center>";
echo _("Select a currency :") . "  ";
echo currencies_list('curr_abrev', null, true, true);
echo "</center>";*/
hidden('curr_abrev','USD');
// if currency sel has changed, clear the form
/*if ($_POST['curr_abrev'] != get_global_curr_code())
{
	clear_data();
	$selected_id = "";
}*/
//print_r($_POST['curr_abrev']);exit;
set_global_curr_code(get_post('curr_abrev'));

$sql = get_sql_for_exchange_rates(get_post('curr_abrev'));
//print_r($selected_id);
//print_r($sql);exit;
$cols = array(
	_("Date to Use From") => 'date', 
	_("BI Rate") => 'rate',
	_("Pajak Rate") => 'rate',
	array('insert'=>true, 'fun'=>'edit_link'),
	array('insert'=>true, 'fun'=>'del_link'),
);
$table =& new_db_pager('orders_tbl', $sql, $cols);

if (is_company_currency(get_post('curr_abrev')))
{

	display_note(_("The selected currency is the company currency."), 2);
	display_note(_("The company currency is the base currency so exchange rates cannot be set for it."), 1);
}
else
{

	br(1);
	$table->width = "40%";
	if ($table->rec_count == 0)
		$table->ready = false;
	display_db_pager($table);
   	br(1);
    display_rate_edit();
}

end_form();

end_page();

