<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLANALYTIC';
$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/gl/includes/siap.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");
//include_once($path_to_root . "/includes/types.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/inquiry/getpl.php");

$js = "";
if (user_use_date_picker())
	$js = get_js_date_picker();

if (get_post('Export')) 
{
	//include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"BS.xls\"");
	header("Cache-Control: max-age=0");
	
	
}else{
	page(_($help_context = "Balance Sheet Drilldown"), false, false, "", $js);
}

//----------------------------------------------------------------------------------------------------
// Ajax updates

if (get_post('Show')) 
{
	$Ajax->activate('balance_tbl');
}

if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];	
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["AccGrp"]))
	$_POST["AccGrp"] = $_GET["AccGrp"];	

//----------------------------------------------------------------------------------------------------

function display_type ($type, $typename, $from, $to, $convert, $dimension, $dimension2, $drilldown,$comps, $cv, $NoZero, $pltot, $pltotlast, $detail = true)
{
	global $path_to_root, $levelptr, $k;
	
	$acctstotal = 0;
	$typestotal = 0;
	$net_balance = 0.0;
	$totalNetBalance = 0.0;
	$sumtotalNetBalance = 0.0;
	//$parsing = array();
	$cekdatanetbalance = 0.0;
	$totalNetBalance2 = array();

	//Get Accounts directly under this group/type
	$result = get_gl_accounts(null, null, $type);	
		
 	while ($account=db_fetch($result))
    {
       
       /*if (!$net_balance)
          continue;*/
        
        //if (($drilldown) && ($levelptr == 0))
        //{
        //$tot = get_balance($account['account_code'], @$comps[$a]['kode'], @$_POST['Dimension'], $from, $to, false, true);
			//echo $NoZero;
			if ($NoZero == 0) {
				for($a=0;$a<count($comps);$a++){
	        		$net_balance = get_gl_trans_from_to('', $to, $account["account_code"], $comps[$a]['kode'], $dimension2);
	        		$cekdatanetbalance += $net_balance;
	        	}
	        	//echo $cekdatanetbalance;
	        	if ($cekdatanetbalance == 0) {
	        		if (($account["account_code"] != '33') AND ($account["account_code"] != '32')) {
	        			continue;
	        		}
	        	}
			}
			$cekdatanetbalance = 0;
                
            if ($detail) {
	            $url = "<a href='$path_to_root/gl/inquiry/gl_account_inquiry.php?TransFromDate=" 
	                . $from . "&TransToDate=" . $to . "&Dimension=" . $dimension . "&Dimension2=" . $dimension2 
	                . "&account=" . $account['account_code'] . "'>" . $account['account_code2'] 
	                ." ". $account['account_name'] ."</a>";                
	            
	            start_row("class='stockmankobg'");
	            if(@$_POST['AccCode']==1){
	            	if ($account["account_code"] != '33') {
	            		label_cell('&nbsp;&nbsp;&nbsp;&nbsp;'.$account['account_code2']." ".$account['account_name']);
	            	}else{
	            		$url = "<a href='$path_to_root/gl/inquiry/profit_loss.php?TransFromDate=".$from."&TransToDate=".$to. "&Dimension=" .$dimension. "&Dimension2=" . $dimension2."&Compare=0'>". $account['account_code2'] ." ". $account['account_name'] ."</a>";	
	            		label_cell('&nbsp;&nbsp;&nbsp;&nbsp;'.$url);
	            	}
	            }else{
	            	if ($account["account_code"] != '33') {
	            		label_cell('&nbsp;&nbsp;&nbsp;&nbsp;'.$account['account_name']);
	            	}else{
	            		$url = "<a href='$path_to_root/gl/inquiry/profit_loss.php?TransFromDate=".$from."&TransToDate=".$to. "&Dimension=" .$dimension. "&Dimension2=" . $dimension2."&Compare=0'>". $account['account_name'] ."</a>";	
	            		label_cell('&nbsp;&nbsp;&nbsp;&nbsp;'.$url);
	            	}
	            }
            }
        	
            for($a=0;$a<count($comps);$a++){

        		$net_balance = get_gl_trans_from_to('', $to, $account["account_code"], $comps[$a]['kode'], $dimension2);
        		/*if (!$net_balance && $NoZero == 1) {
        			continue;
        		}*/
        		if ($account["account_code"] == '33') {
        			//amount_cell($pltot[0][$a]);
					$net_balance += $pltot[0][$a];
					//echo  $pltot[0][$a];
					//echo $net_balance;exit;
					//$calculatedTotal = 0;
            		//$nilaibsperaccount = ($net_balance) * $convert;
        		}
            	if ($account["account_code"] == '32') {
            		$net_balance += $pltotlast[0][$a];
            	}
            	$nilaibsperaccount = ($net_balance) * $convert;
				
				//echo '<td class="tableheader">'.$comps[$a]["ref"].'</td>';
            	if ($cv == 1) {
					if (@$comps[$a]['kode_siap']!='') {
						$tot2 = get_tb_ims($comps[$a]['kode_siap'],$account["account_code"],date2sql($_POST["TransToDate"]));
						//echo $tot2;
						$nilaibsperaccount += $tot2 * $convert;
					}				
				}
            	if ($detail) {
            		amount_cell($nilaibsperaccount);
            	}
            	//label_cell('');
        		$net_balance = 0;
            	$totalNetBalance += $nilaibsperaccount;
            	@$totalNetBalance2[$a] += $nilaibsperaccount;
            	$acctstotal += $nilaibsperaccount;

			}
			/*if (!$net_balance && $NoZero == 1) {
        			continue;
        		}*/
        	if ($detail) {
				amount_cell($totalNetBalance);
        	}
			$totalNetBalance = 0;
	        end_row();
        //}

    }
    //if (($drilldown) && ($levelptr == 0)){
    $tnbarsum = array_sum($totalNetBalance2);
    if ($tnbarsum != 0  && $detail) {
		//if ($detail) {
			start_row("class='inquirybg' style='font-weight:bold;font-size:10'");
			label_cell(_('Total '.$typename));
		//}
		for($a=0;$a<count($comps);$a++){
		//	if ($detail) {
				amount_cell(@$totalNetBalance2[$a] );
		//	}
			$sumtotalNetBalance += @$totalNetBalance2[$a];
		}
		//if ($detail) {
			amount_cell($sumtotalNetBalance);
		//}
    } 	
	end_row();
	//}
    
	$levelptr = 1;

	//Get Account groups/types under this group/type
	$result = get_account_types(false, false, $type);
	while ($accounttype=db_fetch($result))
	{			
		$typestotal += display_type($accounttype["id"], $accounttype["name"], $from, $to, 
			$convert, $dimension, $dimension2, $drilldown);
	}

	//Display Type Summary if total is != 0  
	/*if (($acctstotal + $typestotal) != 0)
	{
		//if ($drilldown && $type == $_POST["AccGrp"])
		//{		
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_('Total') . " " . $typename);
			amount_cell(($acctstotal + $typestotal) * $convert);
			end_row();
		//}
		//START Patch#1 : Display  only direct child types
		$acctype1 = get_account_type($type);
		$parent1 = $acctype1["parent"];
		//if ($drilldown && $parent1 == $_POST["AccGrp"])
		//END Patch#2		
		//{
			$url = "<a href='$path_to_root/gl/inquiry/balance_sheet.php?TransFromDate=" 
				. $from . "&TransToDate=" . $to . "&Dimension=" . $dimension . "&Dimension2=" . $dimension2 
				. "&AccGrp=" . $type ."'>" . $type . " " . $typename ."</a>";
				
			alt_table_row_color($k);
			//label_cell($url);
			amount_cell(($acctstotal + $typestotal) * $convert);
			end_row();
		//}
	}*/

	/*if (!$drilldown) {
		for($a=0;$a<count($comps);$a++){
    		$net_balance = get_gl_trans_from_to("", $to, $account["account_code"], $comps[$a]['kode'], $dimension2);
			//echo '<td class="tableheader">'.$comps[$a]["ref"].'</td>';
        	//amount_cell(($net_balance) * $convert);
        	//$totalNetBalance += ($net_balance) * $convert;
        	//$totalNetBalance2[$a] += ($net_balance);
        	$acctstotal += $net_balance;

		}
	}*/
	//print_r($acctstotal + $typestotal);exit;
	//$acctstotal += $sumtotalNetBalance;
	$parsing = array('totalcomp'=>$totalNetBalance2,'totalall'=>$acctstotal + $typestotal);
	return ($parsing);
}

	
function inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
	echo "<center>";
    start_table(TABLESTYLE_NOBORDER);
	$date = today();
	if (!isset($_POST['TransToDate']))
		$_POST['TransToDate'] = end_month($date);
	if (!isset($_POST['TransFromDate']))
		$_POST['TransFromDate'] = add_days(end_month($date), -user_transaction_days());
    //date_cells(_("From:"), 'TransFromDate');
	date_cells(_("Date:"), 'TransToDate');
	if ($dim >= 1)
		dimensions_list_cells(_("Dimension")." 1:", 'Dimension', null, true, " ", false, 1);
	if ($dim > 1)
		dimensions_list_cells(_("Dimension")." 2:", 'Dimension2', null, true, " ", false, 2);
	//level_list(_("Level"), 'Level', null, true, " ", false, 1);
	echo "<td>Level :</td>";
	echo "<td>";
	echo '<select name="level" id="level">';
	if ($_SESSION["wa_current_user"]->com_id != 0) {
		$sql = "SELECT lvl from 0_companies where id = ".$_SESSION["wa_current_user"]->com_id;
		$result =db_query($sql);
		$lvl = db_fetch($result);
		$lvl = $lvl['lvl'] + 1;
	}else{
		$lvl = 1;
	}
		echo '<option value="-1">Default</option>';
		echo '<option value="0">All</option>';
	for ($i=$lvl; $i <= 5 ; $i++) { 
		echo '<option value="'.$i.'">'.$i.'</option>';
	}
	echo '</select>';
	echo "</td>\n";
	echo "</tr>";
	echo "<tr>";
	check_cells(_("With zero values"), 'NoZero', null);
	check_cells(_("Account Code"), 'AccCode', null);
	check_cells(_("Calculate with SIAP"), 'CalcSiap', null);
	echo "</tr>";
	echo '<tr style="height: 40px !important;">';
	echo '<td colspan=9 align=center><button class="ajaxsubmit" type="submit" aspect="default" name="Show" style="margin-right: 15px !important;" id="Show" value="Show"><img src="../../themes/default/images/ok.gif" height="12" alt=""><span>Show</span></button>';
	//submit_cells('Show',_("Show"),'','', 'default');
	echo '<input type="submit" class="export" name="Export" id="Export" Value="Export to Excel"></td>';
	/*echo '<td></td>';
	echo '<td></td>';
	echo '</tr>';
	*/
	echo "</tr>";
	end_table();
	echo "</center>";
	//hidden('TransFromDate');
	hidden('AccGrp');
}

function get_tb_ims($kodesiap,$accode,$date){
	$datatbims = get_tb2_ims($date);
	
	if(isset($datatbims[$date][$kodesiap][$accode])){
		return $datatbims[$date][$kodesiap][$accode];
	}else{
		return 0;
	}

}

function check_subcomp($subcomid){
	$sql = "SELECT count(*) as nilai from 0_companies where parent = ".$subcomid;
	//$comps=array();
	$result =db_query($sql);
	$count=db_fetch($result);
	//var_dump($comps);exit;
	return $count;
}

function subcomp($parid){
	$sql = "SELECT comp.id,comp.kode_siap,comp.kode, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = id) as nsub
	FROM ".TB_PREF."companies comp
	where comp.parent=".$parid;
	$comps=array();
	$subcomp=array();
	if($result=db_query($sql)){
		while($row=db_fetch($result)){
			$check_subcomp = check_subcomp($row['id']);
			if($check_subcomp['nilai'] > 0){
				$subcomp = subcomp($row['id']);
//				$comps = array_merge($comps,$subcomp);
			}else{
				$comps[] = $row;	
			}
		}
	}
	//var_dump($comps);exit;
	return $comps;
}

function display_balance_sheet()
{
	global $path_to_root;
	$calculatedTotal = 0.0;	
	$calculatedTotal2 = 0.0;	
	$pltot = array();
	$pltotlast = array();
	$from = $_POST['TransToDate'];
	$to = $_POST['TransToDate'];
	//print_r($from);exit;
	//$to = $_POST['TransToDate'];
	if (!isset($_POST['Dimension']))
		$_POST['Dimension'] = 0;
	if (!isset($_POST['Dimension2']))
		$_POST['Dimension2'] = 0;
	$dimension = $_POST['Dimension'];
	$dimension2 = $_POST['Dimension2'];
	if (check_value('CalcSiap')) {
		$check_value = 1;
	}else{
		$check_value = 0;
	}
	//$check_value = 1;
	$pltot[] = get_profit_and_loss($_POST['Dimension'],$_POST['Dimension2'],$from,$_POST['TransToDate'],0,$check_value);
	$pltotlast[] = get_profit_and_loss_last($_POST['Dimension'],$_POST['Dimension2'],$from,$_POST['TransToDate'],0,$check_value);
	//print_r($pltotlast);
	$lconvert = $econvert = 1;
	
	if (isset($_POST["AccGrp"]) && (strlen($_POST['AccGrp']) > 0))
		$drilldown = 1; // Deeper Level
	else
		$drilldown = 0; // Root level	

	div_start('balance_tbl');
	
	start_table(TABLESTYLE, "width='98%'");

	$comps=array();
	$subcomp=array();

	if (@$_POST['level'] == '') {
		$sql = "SELECT comp.id,comp.kode,comp.kode_siap,comp.nama,comp2.nama as namaparent, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = ".$_SESSION["wa_current_user"]->com_id.") as nsub
		FROM ".TB_PREF."companies comp
		LEFT JOIN ".TB_PREF."companies comp2 ON comp2.id = comp.parent
		where comp.id=".$_SESSION["wa_current_user"]->com_id;
		if(@$_SESSION["wa_current_user"]->com_id=='0')
		$sql = "SELECT comp.id,comp.kode,comp.nama,comp.kode_siap,comp.parent,comp2.nama as namaparent, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = comp.id ) as nsub
		FROM ".TB_PREF."companies comp
		LEFT JOIN ".TB_PREF."companies comp2 ON comp2.id = comp.parent
		order by kode asc";
		


		if($result=db_query($sql)){
			while($row=db_fetch($result)){
				$dim=$row['kode'];
				if($_POST['Dimension']!=0)
				$dim=$_POST['Dimension'];
				//print_r($comps);
				if($dim==$row['kode']){
					/*if (($row['lvl'] == 0) || ($row['lvl'] == 1)) {
						$comps[]=$row;
					}else{*/
					
					//}
					if(@$row['nsub'] > 0){
						$subcomp=subcomp($row['id']);
					}else{
						//if(!in_array($comps,$row['kode'])){
						$comps[]=$row;
						//}					
					}
				}
				
			}
			if (($_SESSION["wa_current_user"]->com_id != 0) OR ($_POST['Dimension']!=0)) {
				$comps=array_merge($comps,$subcomp);
			}
		}
	}else{
		if ($_POST['level'] == '0') {
			$_POST['level'] = 5;
		}

		if ($_POST['Dimension']!=0) {
			$sql = "select id,lvl from ".TB_PREF."companies where kode = '".$_POST['Dimension']."'";
			$result = db_query($sql);
			while ($row=db_fetch($result)) {
				$id = $row['id'];
				$lvl = $row['lvl'];
			}
			if ($_POST['level'] == $lvl) {
				$lvl = $lvl;
			}else{
				$lvl = $_POST['level'];
			}

			if ($_POST['level'] == -1) {
				$sql = "select id,lvl from ".TB_PREF."companies where id = '".$id."'";
				$result = db_query($sql);
				while ($row=db_fetch($result)) {
					$lvl = $row['lvl'];
				}
				$lvl = $lvl + 1;
			}
		}else{
			$id = $_SESSION["wa_current_user"]->com_id;
			$lvl = $_POST['level'];
		}

		$sql = "select  lvl, nama, id, kode, kode_siap, parent, CONCAT(kode,' ', nama) as ref
				from    (select * from ".TB_PREF."companies
				         order by parent, id) comps_sorted,
				        (select @pv := ".$id.") initialisation
				where   (CASE WHEN NOT EXISTS (SELECT id from ".TB_PREF."companies where parent = @pv) THEN
						find_in_set(id, @pv) ELSE find_in_set(parent, @pv) END)
				and 	length(@pv := concat(@pv, ',', id))
				and		(lvl BETWEEN lvl AND ".$lvl.")
				group by parent";
		
		if($result=db_query($sql)){
			while($row=db_fetch($result)){
				$comps[]=$row;
			}
		}
	}

	if (!$drilldown) //Root Level
	{		
		//print_r($comps);
		$equityclose = 0.0;
		$lclose = 0.0; 
		$ltotal2 =array();
		$calculateclose = 0.0;
		$classtotal2=array();	
		$net_balance = 0;
		//$acctstotal = array();

		//Get classes for BS
		$classresult = get_account_classes(false, 1);
	

		while ($class = db_fetch($classresult))
		{	
			//print_r($class["ctype"]);
			//print_r($_SESSION["wa_current_user"]);
			$pl = array();
			$classtotal=array();
			$ltotal=array();	
			$etotal=array();	
			unset($classtotal);
			unset($etotal);
			unset($ltotal);
			$classclose = 0.0;
			$asetTotal = 0.0;
			$cekTypeTotal = 0.0;
			$sumAsetTotal = 0.0;
			$TypeTotal2 = array();
			
			//$convertTotal = 0.0;
			$convert = get_class_type_convert($class["ctype"]);
			$ctype = $class["ctype"];

			//Print Class Name	
			//table_section_title($class["class_name"]);
			echo '<tr><td class="tableheader">'.$class["class_name"].'</td>';
			for($a=0;$a<count($comps);$a++){
				echo '<td class="tableheader">'.$comps[$a]["ref"].'</td>';
			}
			echo '<td class="tableheader">Total</td>';
			echo '</tr>';
			//Get Account groups/types under this group/type
			$typeresult = get_account_types(false, $class['cid'], -1);
			$k = 0;
			//$b = 0;
			while ($accounttype=db_fetch($typeresult))
			{
				if (@$_POST['NoZero'] == 0) {
					for($a=0;$a<count($comps);$a++){
						$TypeTotal = display_type($accounttype["id"], $accounttype["name"], $from, $to, $convert, @$comps[$a]['kode'], $dimension2, $drilldown, $comps, 
							$check_value, @$_POST['NoZero'], $pltot, $pltotlast, false);
						$cekTypeTotal += @$TypeTotal['totalcomp'][$a];
					}					
					if ($cekTypeTotal == 0) {
						continue;
					}
				}
				$cekTypeTotal = 0;
				
				$url = "<a href='$path_to_root/gl/inquiry/balance_sheet.php?TransFromDate=" 
					. $from . "&TransToDate=" . $to . "&Dimension=" . $dimension . "&Dimension2=" . $dimension2 
					. "&AccGrp=" . $accounttype['id'] ."'>" . $accounttype['id']." ".$accounttype['name'] ."</a>";	
				alt_table_row_color($k);
				$count2 = count($comps)+2;
				echo '<tr class="evenrow">
				<td style="vertical-align:top !important; font-size=14" colspan="'.$count2.'" >';
				if(@$_POST['AccCode']==1){
					echo $accounttype['id']." ".$accounttype['name'];
				}else{
					echo $accounttype['name'];
				}
				echo '</td>
				</tr>';

				//label_cell($url);
				//print_r($comps);exit;
				
					//Print Summary 
					//if ($TypeTotal != 0 )
					//{
					
					/*$resultakun = get_gl_accounts(null, null, $accounttype["id"]);
					while ($account3=db_fetch($resultakun)){
			    		for($c=0;$c<count($comps);$c++){
			    			$net_balance += get_gl_trans_from_to("", $to, $account3["account_code"], $comps[$a]['kode'], $dimension2);
							$acctstotal[$c] += $net_balance;
						}
						
					}
					amount_cell($acctstotal[$a] * $convert);
					$acctstotal = 0;*/
					//$convertTotal = $TypeTotal * $convert;
					//$asetTotal += $TypeTotal['totalcomp'][$a] * $convert;
					//@$classtotal[$a] += $TypeTotal['totalcomp'][$a]* $convert;
					
					//$TypeTotal = 0;
				//amount_cell($asetTotal);
				//label_cell("");

				//$asetTotal = 0;
				end_row();
				//print_r($pltot[0]);
				for($a=0;$a<count($comps);$a++){
					$TypeTotal = display_type($accounttype["id"], $accounttype["name"], $from, $to, $convert, @$comps[$a]['kode'], $dimension2, $drilldown, $comps, $check_value, @$_POST['NoZero'], $pltot, $pltotlast);
					@$classtotal[$a] += $TypeTotal['totalcomp'][$a]* $convert;
					$classclose += @$TypeTotal['totalcomp'][$a]* $convert;	
					
				}
				//$TypeTotal2[$b] += $TypeTotal;
				//print_r($TypeTotal);exit;
				//$b+=1;

			}			
			//Print Class Summary
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_('Total') . " " . $class["class_name"]);
			//print_r($classtotal);
			for($a=0;$a<count($comps);$a++){
				amount_cell(@$classtotal[$a] * $convert);

				if ($ctype == CL_EQUITY)
				{
					@$etotal[$a]+= @$classtotal[$a]*$convert;
					$equityclose += $classclose;
					$econvert = $convert;
				}
				if ($ctype == CL_LIABILITIES)
				{
					@$ltotal[$a]+= @$classtotal[$a]*$convert;
					$lclose += $classclose;
					$lconvert = $convert;
					@$ltotal2[$a] +=  @$classtotal[$a]*$convert;
				}
				if ($ctype == CL_LIABILITIES or $ctype == CL_EQUITY)
				{
					@$classtotal2[$a] += @$classtotal[$a];
				}
				$calculateclose += $classclose;
				$sumAsetTotal += @$classtotal[$a] * $convert;
			}
			
			amount_cell($sumAsetTotal);
			end_row();		
		}
			//print_r($ltotal2);
		if ($lconvert == 1)
			$calculateclose *= -1;
		//Final Report Summary

		/*$url = "<a href='$path_to_root/gl/inquiry/profit_loss.php?TransFromDate=" 
				. $from."&TransToDate=".$to . "&Dimension=" . $dimension . "&Dimension2=" . $dimension2
			."&Compare=0'>"._('Calculated Return')."</a>";		
		
		start_row("class='inquirybg' style='font-weight:bold'");
		if (get_post('Export')) {
			label_cell(_('Calculated Return'));
		}else{
			label_cell($url);
		}*/
		/*$getpl = array();
		$getpl[] = get_profit_and_loss($_POST['Dimension'],$_POST['Dimension2'],begin_fiscalyear(),$_POST['TransToDate'],$compare,$_POST["AccGrp"]);
		var_dump($getpl);*/
		//$pl[] = get_profit_and_loss(0,0,begin_fiscalyear(),$_POST['TransToDate'],$compare,$_POST["AccGrp"]);
		//$pl[] = ;
		//print_r($pltot);
		/*for($a=0;$a<count($comps);$a++){
			//$pl = 0;
			amount_cell($pltot[0][$a]);
			$calculatedTotal += $pltot[0][$a];
		}
		amount_cell($calculatedTotal);
		$calculatedTotal = 0;*/
		//print_r($pltot[0]);
		end_row();		
		
		start_row("class='inquirybg' style='font-weight:bold'");
		label_cell(_('Total') . " " . _('Liabilities') . _(' and ') . _('Equities'));

		for($a=0;$a<count($comps);$a++){

			amount_cell(@$ltotal2[$a] + @$etotal[$a]);
			$calculatedTotal2 += (@$ltotal2[$a] + @$etotal[$a]);
		}
		amount_cell($calculatedTotal2);
		end_row();
	}
	else //Drill Down
	{
		//Level Pointer : Global variable defined in order to control display of root 
		global $levelptr;
		$levelptr = 0;
		
		$accounttype = get_account_type($_POST["AccGrp"]);
		$classid = $accounttype["class_id"];
		$class = get_account_class($classid);
		$convert = get_class_type_convert($class["ctype"]); 
		
		//Print Class Name	
		//table_section_title($_POST["AccGrp"]. " " . get_account_type_name($_POST["AccGrp"]));	
		/*print_r("ASDAKSJDALSKJDLAKSD");*/
		echo '<tr><td class="tableheader">'.$class["class_name"].'</td>';
		for($a=0;$a<count($comps);$a++){
			echo '<td class="tableheader">'.$comps[$a]["ref"].'</td>';
		}
		echo '<td class="tableheader">Total</td>';
		echo '</tr>';
		
		$classclose = display_type($accounttype["id"], $accounttype["name"], $from, $to, 
			$convert, $dimension, $dimension2, $drilldown,$comps);
	}
	
	end_table(1); // outer table
	div_end();
}

//----------------------------------------------------------------------------------------------------

if (get_post('Export')) 
{
	if ($_SESSION["wa_current_user"]->com_id == 0) {
		$sql = "SELECT comp.nama FROM ".TB_PREF."companies comp
		where comp.id=1";
	}else{
		$sql = "SELECT comp.nama FROM ".TB_PREF."companies comp
		where comp.id=".$_SESSION["wa_current_user"]->com_id;
		
	}

	$result = db_query($sql);
	$fetch = db_fetch($result);

	$header='
	<style>
	.inquirybg2{
		font-weight:bold !important;
		font-size:12px !important;
	}
	.tableheader{
		background-color:#EE7600 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0">
		<tr><td align="center">&nbsp;</td></tr>
		<tr><td style="width:2%;">&nbsp;&nbsp;</td>
			<td>
			<table border="0">
				<tr>
					<td width="100%" align="center"  style="background-color:#bfbfbf;color:#ee3000;font-size:12px;"><b>'.$fetch['nama'].'</b></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:20px;color:#ee3000;font-weight:bold;">Balance Sheet</td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;color:#ee3000;font-weight:bold;">'.@$blnnya.'</td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td>
				</tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 600px;">';
	div_start('balance_tbl');
	echo display_balance_sheet();
	div_end();
	echo '</td></tr></table>
		</td></tr>
		<tr><td></td><td align="center" style="background-color:#EE7600;border:#000 1px solid;">&nbsp;</td></tr>
		</table>';
}else{

start_form();

inquiry_controls();
div_start('balance_tbl');
if (get_post('Show')) {
echo display_balance_sheet();
}
div_end();
end_form();

end_page();

}





