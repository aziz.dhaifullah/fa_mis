<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_TAXREP';
$path_to_root="../..";
include_once($path_to_root . "/includes/session.inc");


include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"Statement of Changes in Equity.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
page(_($help_context = "Statement of Changes in Equity"), false, false, '', $js);
}
//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}
	$begin=@$_POST['tahunfrom'];
	$end=@$_POST['tahunto'];
		$blnfrom='01';
		$blnto='12';
		$thnfrom=$begin;
		$thnto=$end;
	$blnnya=date('F Y',strtotime(($thnfrom-1).'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-31'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

/*
if (get_post('TransFromDate') == "" && get_post('TransToDate') == "")
{
	$date = Today();
	$row = get_company_prefs();
	$edate = add_months($date, -$row['tax_last']);
	$edate = end_month($edate);
	$bdate = begin_month($edate);
	$bdate = add_months($bdate, -$row['tax_prd'] + 1);
	$_POST["TransFromDate"] = $bdate;
	$_POST["TransToDate"] = $edate;
}	
*/
//----------------------------------------------------------------------------------------------------

function tax_inquiry_controls()
{
    start_form();

    start_table(TABLESTYLE_NOBORDER);
    echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	start_row();
	echo "</td>\n";
	submit_cells('Show',_("Show"),'','', 'default');
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">&nbsp;';
	echo '<input type="submit" class="export" name="PrintPdf" id="PrintPdf" Value="Print PDF"></td>';
	echo '<script>
	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle2").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Management_Fee",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
    end_row();

	end_table();

    end_form();
}

//----------------------------------------------------------------------------------------------------
function netprofitloss($begin=null,$to=null){
	if (isset($_POST['TransFromDate']))
	{
		$row = get_current_fiscalyear();
		if (date1_greater_date2($_POST['TransFromDate'], sql2date($row['end'])))
		{
			display_error(_("The from date cannot be bigger than the fiscal year end."));
			set_focus('TransFromDate');
			return;
		}
	}
	if (!isset($_POST['Dimension']))
		$_POST['Dimension'] = 0;
	if (!isset($_POST['Dimension2']))
		$_POST['Dimension2'] = 0;
	//start_table(TABLESTYLE,'',6);

	

	if(@$begin=='')
	$begin=date('d/m/Y',strtotime($thnfrom.'-01-01'));
	if(@$to=='')
	$to=date('d/m/Y',strtotime($thnto.'-12-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));
	/*
	if($_POST["tipe"]==0){
		$colom=3;
	}else{
		$colom=3+($blnto-$begin);
	}
	*/

	$totalaset=0;
	//$tableheader .= '<tr><td colspan="3">Kas dan Setara Kas</td></tr>';
	//get_account_from_to
	$ttlall=0;$ttlall2=0;

	//Pendapatan usaha
	$accounts=get_account_from_to('40000','46000');
	$nrow=n_account_from_to('40000','46000');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	

	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;

		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, true, true);
		$total=$total+($tot['debit']-$tot['credit']);
		$ttlclass+=$total;
		$ttlclass2+=$total;
		$ttlall+=$total;

		$n++;
	}

	//Cost Of Sales
	$accounts=get_account_from_to('51000','51005');
	$nrow=n_account_from_to('51000','51005');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;

	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;

		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, true, true);
		$total=$total+($tot['debit']-$tot['credit']);
		$ttlclass+=$total;
		$ttlclass2+=$total;
		$ttlall+=$total;

		$n++;
	}

	//Biaya Operasional
	$accounts=get_account_from_to('61000','69990');
	$nrow=n_account_from_to('61000','69990');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	
	$notacc=array('62003001','62003002','62003003','63003001','69101001','69101002','69101003','69104001','69104002','69104003','69104004','69104005');
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;
		if(!in_array($account["account_code"],$notacc)){
			
			$subaccounts=get_subaccount($account["account_code"]);
			while ($subaccount = db_fetch($subaccounts))
			{
				$tot = get_balance($subaccount["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, true, true);
				$total=$total+($tot['debit']-$tot['credit']);
			}

			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, true, true);
			$total=$total+($tot['debit']-$tot['credit']);
			$ttlclass+=$total;
			$ttlclass2+=$total;
			$ttlall2+=$total;
			
			$n++;
		}
	}

	//Other Income
	$accounts=get_account_from_to('80000','89999');
	$nrow=n_account_from_to('80000','89999');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;

		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, true, true);
		$total=$total+($tot['debit']-$tot['credit']);
		$ttlclass+=$total;
		$ttlclass2+=$total;
		$ttlall+=$total;

		$n++;
	}

	//Other Expense
	$accounts=get_account_from_to('90000','99999');
	$nrow=n_account_from_to('90000','99999');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, true, true);
		$total=$total+($tot['debit']-$tot['credit']);
		$ttlclass+=$total;
		$ttlclass2+=$total;
		$ttlall+=$total;

		$n++;
	}

	return abs($ttlall+$ttlall2);
}
function show_results()
{
    /*Now get the transactions  */
	if(get_post('PrintPdf'))
	$tableheader ='<center><table class="tablestyle" width="98.5%" cellpadding="3" border="0">';
	else
	$tableheader ='<center><table class="tablestyle" width="98%" cellpadding="3">';

	$begin=@$_POST['tahunfrom'];
	$end=@$begin+1;
	//pph21
	$tableheader .= '<tr><td class="tableheader" colspan="4" bgcolor="#dee7ec"></td>
	<td class="tableheader" colspan="2" bgcolor="#dee7ec"><b>Saldo Laba</b></td>
	<td class="tableheader" bgcolor="#dee7ec"></td></tr>';
	$tableheader .= '<tr>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b></b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Modal Saham</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Tambahan<br>Modal</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Komponen<br>Ekuitas Lainnya</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Ditentukan <br>Penggunaannya</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Tidak Ditentukan <br>Penggunaannya</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Jumlah</b></td>';
	$tableheader .= '</tr>';

	for($a=$begin;$a<=$end;$a++){
		$ttl1=0;$ttl2=0;$ttl3=0;$ttl4=0;$ttl5=0;$ttl6=0;
		$thn=$a;
		$jan='01/01/'.$thn;
		$des='31/12/'.$thn;
		$tableheader .= '<tr>';
		$tableheader .= '<td>Saldo Per 1 Januari '.($thn).'*</td>';
		$jumlah=0;
		//modal saham
		$tot = get_balance('31100', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $jan, false, true);			
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($tot['balance'])).'</td>';
		$jumlah+=($tot['balance']);
		$ttl1+=($tot['balance']);
		//Tambahan Modal Disetor
		$tot = get_balance('32000', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $jan, false, true);			
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($tot['balance'])).'</td>';
		$jumlah+=($tot['balance']);
		$ttl2+=($tot['balance']);
		//Historical Balancing Account / Ekuitas Lainnya
		$tot = get_balance('39999', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $jan, false, true);			
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($tot['balance'])).'</td>';
		$jumlah+=($tot['balance']);
		$ttl3+=($tot['balance']);
		$tableheader .= '<td style="text-align:right;">-</td>';
		//Saldo Laba / Laba ditahan	
		$tot = get_balance('38000', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $jan, false, true);			
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($tot['balance'])).'</td>';
		$jumlah+=($tot['balance']);
		$ttl5+=($tot['balance']);
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($jumlah)).'</td>';
		$ttl6+=($jumlah);
		$tableheader .= '</tr>';

		$tableheader .= '<tr>';
		$tableheader .= '<td>Laba Bersih Berjalan Tahun '.($thn).'*</td>';
		$tableheader .= '<td style="text-align:right;">-</td>';
		$tableheader .= '<td style="text-align:right;">-</td>';
		$tableheader .= '<td style="text-align:right;">-</td>';
		$tableheader .= '<td style="text-align:right;">-</td>';
		$jumlah=0;
		//Saldo Laba / Laba / (Rugi) Tahun Berjalan	
		//$tot = get_balance('39000', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);		
		$tot['balance']=netprofitloss($jan,$des);
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(abs($tot['balance'])).'</td>';
		$jumlah+=abs($tot['balance']);
		$ttl5+=abs($tot['balance']);
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(abs($jumlah)).'</td>';
		$ttl6+=($jumlah);
		$tableheader .= '</tr>';

		$tableheader .= '<tr>';
		$tableheader .= '<td>Kerugian Belum Terealisasi Atas<br>Perubahan Nilai Wajar Portofolio Efek<br>Tersedia Untuk Dijual</td>';
		$tableheader .= '<td style="text-align:right;">-</td>';
		$tableheader .= '<td style="text-align:right;">-</td>';
		$jumlah=0;
		//Saldo Laba / 	Laba/(Rugi) belum direalisasi	
		$tot = get_balance('39800', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);			
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(abs($tot['balance'])).'</td>';
		$jumlah+=abs($tot['balance']);
		$ttl3+=abs($tot['balance']);
		$tableheader .= '<td style="text-align:right;">-</td>';
		$tableheader .= '<td style="text-align:right;">-</td>';
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($jumlah)).'</td>';
		$ttl6+=($jumlah);
		$tableheader .= '</tr>';

		$tableheader .= '<tr>';
		$tableheader .= '<td>Pengukuran Kembali Imbalan Kerja</td>';
		$tableheader .= '<td style="text-align:right;">-</td>';
		$tableheader .= '<td style="text-align:right;">-</td>';
		$tableheader .= '<td style="text-align:right;">-</td>';
		$tableheader .= '<td style="text-align:right;">-</td>';
		$jumlah=0;
		//Saldo Laba / 	Laba Pengukuran Kembali - PMP	
		$tot = get_balance('39700', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);			
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($tot['balance'])).'</td>';
		$jumlah+=$tot['balance'];
		$ttl5+=($tot['balance']);
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($jumlah)).'</td>';
		$ttl6+=($jumlah);
		$tableheader .= '</tr>';

		$tableheader .= '<tr>';
		$tableheader .= '<td>Pengampunan Pajak</td>';
		$tableheader .= '<td style="text-align:right;">-</td>';
		$jumlah=0;
		//Aset Pengampunan Pajak 
		//$tot = get_balance('12902', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $jan, false, true);	
		$tot = get_balance('32000', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);			
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($tot['balance'])).'</td>';
		$jumlah+=$tot['balance'];
		$ttl2+=($tot['balance']);
		$tableheader .= '<td style="text-align:right;">-</td>';
		$tableheader .= '<td style="text-align:right;">-</td>';
		$tableheader .= '<td style="text-align:right;">-</td>';
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($jumlah)).'</td>';
		$ttl6+=($jumlah);
		$tableheader .= '</tr>';

		$tableheader .= '<tr>';
		$tableheader .= '<td>Saldo Per 31 Desember '.($thn).'*</td>';
		$jumlah=0;
		//modal saham		
		if($ttl1==0){
			$tot = get_balance('31100', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, false, true);	
			$ttl1=$tot['balance'];
		}
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($ttl1)).'</td>';
		$jumlah+=$ttl1;
		//Tambahan Modal Disetor
		if($ttl2==0){
			$tot = get_balance('32000', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, false, true);	
			$ttl2=$tot['balance'];
		}
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($ttl2)).'</td>';
		$jumlah+=$ttl2;
		//Historical Balancing Account / Ekuitas Lainnya			
		if($ttl3==0){
			$tot = get_balance('39800', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, false, true);	
			$ttl3=$tot['balance'];
		}
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($ttl3)).'</td>';
		$jumlah+=$ttl3;

		$tableheader .= '<td style="text-align:right;">-</td>';
		$jumlah+=$ttl4;
		//Saldo Laba / Laba ditahan			
		if($ttl5==0){
			$tot = get_balance('38000', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, false, true);	
			$ttl5=abs($tot['balance']);
			$tot = get_balance('39000', $_POST['Dimension'], $_POST['Dimension2'], $jan, $des, false, true);
			$ttl5=$ttl5+abs($tot['debit']-$tot['credit']);
		}
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($ttl5)).'</td>';
		$jumlah+=$ttl5;

		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($jumlah)).'</td>';
		$tableheader .= '</tr>';

		$tableheader .= '<tr>';
		$tableheader .= '<td colspan="7"></td>';
		$tableheader .= '</tr>';
	}

	$tableheader .= '<tr>';
	$tableheader .= '<td colspan="7">* Disajikan Kembali (Catatan 25)</td>';
	$tableheader .= '</tr>';
	$tableheader .= '<tr>';
	$tableheader .= '<td colspan="7" style="text-align:center;">Lihat Catatan Atas Laporan Keuangan yang merupakan<br>
	bagian tidak terpisahkan dari Laporan Keuangan Ini</td>';
	$tableheader .= '</tr>';
	$tableheader.='</center></table>';
	return $tableheader;
}

//----------------------------------------------------------------------------------------------------


if (get_post('Export')) 
{
	$header='<style>
	.tableheader{
		background-color:#000 !important;
		color:#fff !important;
	}
	.tablestyle{
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0" style="border:#000 1px solid;">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Laporan Perubahan Ekuitas</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Untuk Tahun Yang Berakhir Tanggal - Tanggal</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">'.@$blnnya.'</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">(Dinyatakan Dalam Rupiah, Kecuali Dinyatakan Lain)</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 509pt;">';
	echo show_results();
	echo '</td></tr></table>';
}elseif(get_post('PrintPdf')){

// add a page
	$header='<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Laporan Perubahan Ekuitas</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Untuk Tahun Yang Berakhir Tanggal - Tanggal</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">'.@$blnnya.'</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">(Dinyatakan Dalam Rupiah, Kecuali Dinyatakan Lain)</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	

	$header.= '<tr><td style="width: 646px;">';
	$header.=show_results();
	$header.= '</td></tr></table>';
$pdf->AddPage();
// output the HTML content
$html=$header;
//$html.=display_bs_pdf();
$pdf->writeHTML(@$html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('ringkasanPajak.pdf', 'I');	

}else{

tax_inquiry_controls();
		div_start('trans_tbl');
	if (get_post('Show')) {
		echo '<center><table border="0" cellpadding="4" width="80%">
		<tr>		
			<td align="center" style="font-weight:bold;">Statement of Changes in Equity</td>
		</tr>
		<tr>
			<td align="center" style="font-weight:bold;">Periode '.@$blnnya.'</td>
		</tr>';
		echo '<tr>		
			<td align="center">';

		echo show_results();
		echo '</td></tr></table></center>';
	}
		div_end();
end_page();
	//show_results();
}
//----------------------------------------------------------------------------------------------------


