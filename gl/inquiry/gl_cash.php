<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLANALYTIC';
$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

$js = "";
if (user_use_date_picker())
	$js = get_js_date_picker();

page(_($help_context = "Cash Flow Statement"), false, false, "", $js);



$k = 0;
$pdeb = $pcre = $cdeb = $ccre = $tdeb = $tcre = $pbal = $cbal = $tbal = 0;

//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('balance_tbl');
}


function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER,2);

	echo '<form method="POST" action="../../reporting/reports_main.php" target="_blank">
<button class="ajaxsubmit" type="submit" aspect="default process" name="Rep_cash_flow_statement" id="Rep_cash_flow_statement" value="Display: Cash Flow Statement">
	<img src="../../themes/default/images/ok.gif" height="12" alt=""><span>Display: Cash Flow Statement</span></button>
<input type="hidden" name="REP_ID" value="_cash_flow_statement">
<br><br>
Report Period:
<br>
<input type="text" name="PARAM_0" value="31/03/2017">
	<a href="javascript:date_picker(document.forms[0].PARAM_0);">	
		<img src="../../themes/default/images/cal.gif" style="vertical-align:middle;padding-bottom:4px;width:16px;height:16px;border:0;" alt="Click Here to Pick up the date"></a>
<br><br>Dimension:<br><span id="_PARAM_1_sel"><select id="PARAM_1" autocomplete="off" name="PARAM_1" class="combo" title="" _last="1"><option selected="" value="0">No Dimension Filter</option>
<option value="18">Area Manager  Area Manager</option>
<option value="17">Division  Division</option>
<option value="22">Sales1  Sales1</option>
<option value="23">Sales2  Sales2</option>
<option value="24">SYAISALES  SYAISALES</option>
<option value="21">Team Leader  Team Leader</option>
</select>
</span>
<br><br>Comments:<br><textarea rows="4" cols="30" name="PARAM_2"></textarea><br><br>Destination:<br><span id="_PARAM_3_sel"><select autocomplete="off" name="PARAM_3" class="combo" title="" _last="0"><option value="0">PDF/Printer</option>
<option value="1">Excel</option>
</select>
</span>
<br><br>
<input type="hidden" name="Class" value="5">
</form>';
    end_table();
    end_form();
}

	gl_inquiry_controls();

	end_page();


