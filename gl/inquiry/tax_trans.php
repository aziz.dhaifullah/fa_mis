<?php


$path_to_root="../..";
include_once($path_to_root . "/includes/session.inc");


include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
function show_results()
{
	
	$bdate=str_replace('_','/',@$_GET['frod']);
	$edate=str_replace('_','/',@$_GET['tod']);
	$xls=explode('_',@$_GET['xls']);
	$tp=$xls[0];
	$tpid=$xls[1];
	$_POST["TransFromDate"] = $bdate;
	$_POST["TransToDate"] = $edate;
	
    /*Now get the transactions  */
	//div_start('trans_tbl');
	//start_table(TABLESTYLE);
	echo '<table border=1>';
	if(trim($tpid)=="2"){
		$th = array(_("Kode Form Bukti Potong"), 
				_("Masa Pajak"), 
				_("Tahun Pajak"), 
				_("Pembetulan"), 
				_("NPWP WP yang Dipotong"), 
				_("Nama WP yang Dipotong"), 
				_("Alamat WP yang Dipotong"), 
				_("Nomor Bukti Potong"), 
				_("Tanggal Bukti Potong"), 
				_("Jumlah Nilai Bruto"), 
				_("Jumlah PPh Yang Dipotong"));
	}else{
		$th = array(_("FK"), 
				_("KD_JENIS_TRANSAKSI"), 
				_("FG_PENGGANTI"), 
				_("NOMOR_FAKTUR"), 
				_("MASA_PAJAK"), 
				_("TAHUN_PAJAK"), 
				_("TANGGAL_FAKTUR"), 
				_("NPWP"), 
				_("NAMA"), 
				_("ALAMAT_LENGKAP"), 
				_("JUMLAH_DPP"), 
				_("JUMLAH_PPN"), 
				_("JUMLAH_PPNBM"),
				_("ID_KETERANGAN_TAMBAHAN"), 
				_("FG_UANG_MUKA"), 
				_("UANG_MUKA_DPP"), 
				_("UANG_MUKA_PPN"), 
				_("UANG_MUKA_PPNBM"),
				_("REFERENSI"));
	}
	table_header($th);
	$k = 0;
	$total = 0;
	echo $taxes = get_tax_detail($_POST['TransFromDate'], $_POST['TransToDate'],$tpid,$tp,@$_GET['wth']);

	while ($tx = db_fetch($taxes))
	{
		$npwp='';
		$name='';
		$alamat='';
		
		if($tx['trans_type']==20){
			$wpinfo = db_fetch(npwp_supp($tx['trans_no']));
			$npwp=$wpinfo['gst_no'];
			$name=$wpinfo['supp_name'];
			$alamat=$wpinfo['supp_address'];
		}else{
			npwp_cust($tx['trans_no']);
			$wpinfo = db_fetch(npwp_cust($tx['trans_no']));
			$name=$wpinfo['name'];
			$alamat=$wpinfo['address'];
			$npwp=$wpinfo['tax_id'];
		}
		if(trim($tpid)=="2"){
			label_cell("");
			label_cell(date('m',strtotime($tx['tran_date'])));
			label_cell(date('Y',strtotime($tx['tran_date'])));
			label_cell("0");
			label_cell($npwp);
			label_cell($name);
			label_cell($alamat);
			label_cell($tx['trans_no']);
			label_cell(date('d/m/Y',strtotime($tx['tran_date'])));
			label_cell($tx['net_amount']);
			label_cell($tx['amount']);
		}else{
			if(trim($tp)=="k")
				label_cell("FK");
			else
				label_cell("FM");
			//label_cell("01");
			//label_cell($tx['trans_type']);
			label_cell("");
			label_cell('');
			label_cell($tx['reference']);
			label_cell(date('m',strtotime($tx['tran_date'])));
			label_cell(date('Y',strtotime($tx['tran_date'])));
			label_cell(date('d/m/Y',strtotime($tx['tran_date'])));
			label_cell($npwp);
			label_cell($name);
			label_cell($alamat);
			
			label_cell($tx['net_amount']);
			label_cell($tx['amount']);
			label_cell("0");
			label_cell("");
			label_cell("0");
			label_cell("0");
			label_cell("0");
			label_cell("0");
			label_cell("");
		}
		end_row();
	}	

	end_table(2);
	//div_end();
}

//----------------------------------------------------------------------------------------------------

header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header("Content-Disposition: attachment;filename=\"PPN_".date('dMY').".xls\"");
header("Cache-Control: max-age=0");

show_results();


