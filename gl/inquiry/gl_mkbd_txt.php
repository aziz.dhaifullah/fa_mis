<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/config_mssql.php");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/db/gl_db_mkbd.inc");
include_once($path_to_root . "/gl/includes/siap.inc");
include_once($path_to_root . "/gl/includes/siar.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd51.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd52.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd53.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd54.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd55.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd56.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd57.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd58.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd59.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd510.inc");
/*
*/
$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

if (get_post('Export')) 
{
	header("Content-type: text/plain");
	header('Content-Disposition: attachment; filename="mkbd.txt"');
	
}else{
page(_($help_context = "MKBD"), false, false, '', $js);
}

//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}
if (get_post('Export')) 
{
	

}

if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format(0);

//----------------------------------------------------------------------------------------------------

function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER);
	start_row();
   // gl_all_accounts_list_cells(_("Account:"), 'account', null, false, false, _("All Accounts"));
	date_cells(_("Date:"), 'TransFromDate', '', null, -user_transaction_days());
	//date_cells(_("End Date:"), 'TransToDate');

	echo "<td>"._("Direktur Bertanggung Jawab").":</td>\n";
	echo "<td>";
	echo '<input type="text" name="direktur">';
	echo "</td>\n";
    end_row();
	end_table();

	start_table(TABLESTYLE_NOBORDER);
	start_row();

	echo '<td colspan="4">&nbsp;</td>';
	submit_cells('Show',_("Show"),'align="right"','', 'default');
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to TXT"></td>';
	
	$lampir2=$_POST['lampiran']+1;
	echo '
	<style>
		.export{
			align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
		function xls(){
			//console.log("aa");
			$(".tablestyle").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "MKBD_VD5-'.$lampir2.'",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
	//submit_cells('Export Excel',_("Export"),'','', 'default');
	end_row();
	end_table();

	echo '<hr>';
    end_form();
}

//----------------------------------------------------------------------------------------------------


function show_results()
{
	global $path_to_root, $systypes_array,$SysPrefs;

	
	$lampir=$_POST['lampiran']+1;
	$ctype=$lampir;
	$colspan = ($dim == 2 ? "6" : ($dim == 1 ? "5" : "4"));

	$tgl=explode('/',$_POST['TransFromDate']);
	echo 'Kode AB|U8|||||||||';
		if (get_post('Export')) 
		{
			echo "\r\n";
		}else{
			echo '<br>';
		}
	echo 'Tanggal|'.$tgl[2].'-'.$tgl[1].'-'.$tgl[0].'|||||||||';
		if (get_post('Export')) 
		{
			echo "\r\n";
		}else{
			echo '<br>';
		}
	echo 'Direktur|'.$_POST['direktur'].'|||||||||';
		if (get_post('Export')) 
		{
			echo "\r\n";
		}else{
			echo '<br>';
		}

		echo display_v1_2();
	//if(get_post('lampiran')==1)
		echo display_v2_2();
	//if(get_post('lampiran')==2)
		echo display_v3_2();
	//if(get_post('lampiran')==3)
		echo display_v42();
	//display_v4();
	//if(get_post('lampiran')==4)
	 echo display_v5_2();
	//if(get_post('lampiran')==5)
		echo display_v62();
	//display_v6();
	//if(get_post('lampiran')==6)
		echo display_v7_2();
	//if(get_post('lampiran')==7)
		echo display_v8_2();
	
	//display_v8();
	//if(get_post('lampiran')==8)
		echo display_v9_2();
	//display_v9();
	if(get_post('lampiran')==9)
	display_v10();
/*
*/
	//end_table(1);
}
//----------------------------------------------------------------------------------------------------
function display_v1_2(){
	$lists=get_mkbd_list(1);
	$tab="";
	$tasetl=0;$tasetl2=0;
	while ($list = db_fetch($lists))
	{
		$accs=array();
		$spas='';
		$saldo=0;
		$accs=get_mkbd_coa($list['id']);
		//print_r($accs);
		if(count($accs)>0)
			$saldo=get_totals($accs,$_POST['TransFromDate']);

		if($list['no']>=8 and $list['no']<100)
			$tasetl=$tasetl+$saldo;
		if($list['no']>=102 and $list['no']<112)
			$tasetl2=$tasetl2+$saldo;
		if($list['no']==100)
			$saldo=$tasetl;
		if($list['no']==112)
			$saldo=$tasetl2;
		if($list['no']==113)
			$saldo=$tasetl2+$tasetl;
		$tab.='VD51.'.$list['no'].'|'.$saldo.'|||||||||';

		if (get_post('Export')) 
		{
			$tab.="\r\n";
		}else{
			$tab.='<br>';
		}
		//$tab.=$list['label'];
	}
	return $tab;
}
function total_v1_2(){
	$lists=get_mkbd_list(1);
	$tasetl=0;$tasetl2=0;
	while ($list = db_fetch($lists))
	{
		$accs=array();
		$spas='';
		
		$saldo=0;
		$accs=get_mkbd_coa($list['id']);
		//print_r($accs);
		if(count($accs)>0)
			$saldo=get_totals($accs,$_POST['TransFromDate']);

		if($list['no']>=8 and $list['no']<100)
			$tasetl=$tasetl+$saldo;
		if($list['no']>=102 and $list['no']<112)
			$tasetl2=$tasetl2+$saldo;
		if($list['no']==100)
			$saldo=$tasetl;
		if($list['no']==112)
			$saldo=$tasetl2;
		if($list['no']==113)
			$saldo=$tasetl2+$tasetl;
		//$tab.=$list['label'];
	}
	return array($tasetl,$tasetl2);
}
function display_v2_2(){
	$lists=get_mkbd_list(2);
	$tab="";
	$tasetl2=0;$teku=0;
	while ($list = db_fetch($lists))
	{
		$accs=array();
		$spas='';
		
		$saldo=0;
		$accs=get_mkbd_coa($list['id']);
		//print_r($accs);
		if(count($accs)>0)
			$saldo=get_totals($accs,$_POST['TransFromDate']);

		if($list['no']>=121 and $list['no']<164)
			$tasetl2=$tasetl2+$saldo;
		if($list['no']==164)
			$saldo=$tasetl2;
		if($list['no']>=167 and $list['no']<172)
			$teku=$teku+$saldo;
		if($list['no']==172)
			$saldo=$teku;
		if($list['no']==173)
			$saldo=$tasetl2+$teku;

		$tab.='VD52.'.$list['no'].'|'.$saldo.'|||||||||';

		if (get_post('Export')) 
		{
			$tab.="\r\n";
		}else{
			$tab.='<br>';
		}
		//$tab.=$list['label'];
	}
	return $tab;
}
function total_v2_2(){
	$lists=get_mkbd_list(2);
	$tasetl2=0;$teku=0;
	while ($list = db_fetch($lists))
	{
		$accs=array();
		$spas='';
		
		$saldo=0;
		$accs=get_mkbd_coa($list['id']);
		//print_r($accs);
		if(count($accs)>0)
			$saldo=get_totals($accs,$_POST['TransFromDate']);

		if($list['no']>=121 and $list['no']<164)
			$tasetl2=$tasetl2+$saldo;
		if($list['no']==164)
			$saldo=$tasetl2;
		if($list['no']>=167 and $list['no']<172)
			$teku=$teku+$saldo;
		if($list['no']==172)
			$saldo=$teku;
		if($list['no']==173)
			$saldo=$tasetl2+$teku;
		//$tab.=$list['label'];
	}
	return array($tasetl2);
}
function display_v3_2(){
	$lists=get_mkbd_list(2);
	$tab="";
	$tasetl2=0;$teku=0;
	for($a=8;$a<=31;$a++)
	{
		$accs=array();
		$spas='';
		
		$saldo=0;
		$tab.='VD53.'.$a.'|'.$saldo.'|||||||||';

		if (get_post('Export')) 
		{
			$tab.="\r\n";
		}else{
			$tab.='<br>';
		}
		//$tab.=$list['label'];
	}
	return $tab;
}
function display_v5_2(){
	$lists=get_mkbd_list(2);
	$tab="VD55.T||||||||0.00||";
		if (get_post('Export')) 
		{
			$tab.="\r\n";
		}else{
			$tab.='<br>';
		}
	$tasetl2=0;$teku=0;
	for($a=8;$a<=14;$a++)
	{
		$tab.='VD55.'.$a.'||||||||||';

		if (get_post('Export')) 
		{
			$tab.="\r\n";
		}else{
			$tab.='<br>';
		}
		//$tab.=$list['label'];
	}
	return $tab;
}
function display_v7_2(){
	$lists=get_mkbd_list(2);
	$tab="VD55.T||||||||0.00||";
		if (get_post('Export')) 
		{
			$tab.="\r\n";
		}else{
			$tab.='<br>';
		}
	$tasetl2=0;$teku=0;
	for($a=8;$a<=63;$a++)
	{
		$tab.='VD57.'.$a.'|0|||||||||';

		if (get_post('Export')) 
		{
			$tab.="\r\n";
		}else{
			$tab.='<br>';
		}
		//$tab.=$list['label'];
	}
		$tab.='VD57.P||||||||||';

		if (get_post('Export')) 
		{
			$tab.="\r\n";
		}else{
			$tab.='<br>';
		}
	return $tab;
}
function display_v62(){
	$lists=get_mkbd_list(6);
	$tab='';

	$tableheader='';
	
	//kas
	$accounts=get_gl_accounts_in(array('11111','11112','11114','11140'));
	$total2=0;
	$tgl=explode('/', $_POST['TransFromDate']);
	$begin=date('m/d/Y',strtotime($tgl[2].'-'.$tgl[1].'-01'));
	while ($account = db_fetch($accounts))
	{	
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $_POST['TransFromDate'], false, true);
		$total2=$total2+($tot['debit']-$tot['credit']);
	}
	$totalkas=$total2;
	/*
	$tableheader.='<tr>
		  <td>&nbsp;</td>
		  <td>Kas</td>
		  <td>S</td>
		  <td></td>
		  <td>IDR</td>
		  <td></td>
		  <td align="right">'.price_format_pr(($total2)).'</td>
		</tr>';
	*/
	$tableheader.="VD56.24.1|000|N|000|IDR|0|0||||";
	if (get_post('Export')) 
	{
		$tableheader.="\r\n";
	}else{
		$tableheader.='<br>';
	}	
	//bank	
	$total2=0;
	$b=2;
	$accounts=get_gl_accounts_in(array('11152','11154','11155','11156','11157','11158','11159','11160','11161','11163','11164'));
	while ($account = db_fetch($accounts))
	{	
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $_POST['TransFromDate'], false, true);
		$total2=$total2+($tot['debit']-$tot['credit']);
		$curr=get_bank_curr($account["account_code"]);
		$saldo=($tot['debit']-$tot['credit']);
		$saldo2=0;
		if($curr!='IDR'){
			$saldo=($tot['debit']-$tot['credit']);
			$saldo2=0;			
		}

		$tableheader.="VD56.24.".$b."|000|N|000|IDR|0|0||||";
		if (get_post('Export')) 
		{
			$tableheader.="\r\n";
		}else{
			$tableheader.='<br>';
		}
		$b++;
		/*
		$tableheader.='<tr>
			  <td>&nbsp;</td>
			  <td>'.$account["account_name"].'</td>
			  <td>S</td>
			  <td></td>
			  <td>IDR</td>
			  <td align="right">'.price_format_pr($saldo2).'</td>
			  <td align="right">'.price_format_pr($saldo).'</td>
			</tr>';
		*/
	}
	$totalbank=$total2;


	$tdebit=0;
	while ($list = db_fetch($lists))
	{
		$accs=array();
		$spas='';
		
		$tab.='VD56.'.$list['no'].'|0|0|0|||||||';
		if (get_post('Export')) 
		{
			$tab.="\r\n";
		}else{
			$tab.='<br>';
		}
		//$tab.=$list['label'];
	}
	$tab.=$tableheader;
	$tab.="VD56.P||||||||||";
	if (get_post('Export')) 
	{
		$tab.="\r\n";
	}else{
		$tab.='<br>';
	}
	return $tab;
}

function display_v42(){
	$lists=get_mkbd_list(6);
	$tab='';

	$prods=get_prod_siar_dp();
	$no=1;$total=0;
	for($a=0;$a<count($prods[0]);$a++){

		$tgl=explode('/', $_POST['TransFromDate']);
		$tgl2=date('Y-m-d',strtotime($tgl[2].'-'.$tgl[1].'-'.$tgl[0]));
		$nab=get_aum_siar($prods[1][$a],$tgl2);
		$aum=$nab['navvalue']*$nab['OutstandingUnits'];
		$batasan=$aum*0.25;
		$tab.='VD54.14.'.$no.'|RDPS|'.$prods[0][$a].'|Afiliasi|'.$aum.'|'.($aum).'||'.$batasan.'|'.($aum-$batasan).'||';

		if (get_post('Export')) 
		{
			$tab.="\r\n";
		}else{
			$tab.='<br>';
		}
		 $total=$total+($aum-$batasan);
		  $no++;
	}
		$tab.='VD54.14.T|RDPS|||||||'.($total).'||';
		if (get_post('Export')) 
		{
			$tab.="\r\n";
		}else{
			$tab.='<br>';
		}
	return $tab;
}
function display_v8_2(){
	$lists=get_mkbd_list(8);
	$tab="";
	$tasetl=0;$tasetl2=0;
	$t16=0;$t18=200000000;$t20=0;$t22=0;$t23=0;$t24=0;
	while ($list = db_fetch($lists))
	{
		$accs=array();
		$spas='';
		
		$nilai=0;
		$formulir='';$lajur='';$baris='';
		if($list['no']==8){
			$tl=total_v2_2();
			$nilai=$tl[0];
			$formulir='V.D.5-2';
			$lajur='B';
			$baris='164';
			$t16=$t16+$nilai;
		}
		if($list['no']==9){
			$formulir='V.D.5-3';
			$lajur='B';
			$baris='31';
			$t16=$t16+$nilai;
		}
		if($list['no']==11){
			$formulir='V.D.5-2';
			$lajur='B';
			$baris='163';
			$t16=$t16-$nilai;
		}
		if($list['no']==13){
			$formulir='V.D.5-2';
			$lajur='B';
			$baris='146';
			$t16=$t16-$nilai;
		}
		if($list['no']==14){
			$formulir='V.D.5-2';
			$lajur='B';
			$baris='147';
			$t16=$t16-$nilai;
		}
		if($list['no']==15){
			$formulir='V.D.5-2';
			$lajur='B';
			$baris='148';
			$t16=$t16-$nilai;
		}
		if($list['no']==16){
			$nilai=$t16;
		}
		if($list['no']==18){
			$nilai=$t18;
		}
		if($list['no']==19){
			$nilai=$t16*0.0625;
		}
		if($list['no']==20){
			$nilai=$t16*0.0625;
			if($nilai<$t18)
				$nilai=$t18;
			$t20=$nilai;
		}
		if($list['no']==22){
			$nilai=$t18;
			$t22=$t22+$nilai;
		}
		if($list['no']==23){
			$t23=$t23+$nilai;
		}
		if($list['no']==24){
			$nilai=$t23*0.001;
			$t24=$nilai;
		}
		if($list['no']==25){
			$nilai=$t22+$t24;
		}
		if($list['no']==26){
			$nilai=$t20+$t22+$t24;
		}
		$tab.='VD58.'.$list['no'].'||||'.$nilai.'||||||';
		if (get_post('Export')) 
		{
			$tab.="\r\n";
		}else{
			$tab.='<br>';
		}
		//$tab.=$list['label'];
	}
	return $tab;
}

function display_v9_2(){
	$lists=get_mkbd_list(9);
	$tab="";
	$tasetl=0;$tasetl2=0;
	$t13=0;$t16=0;$t18=200000000;$t20=0;$t22=0;$t23=0;$t24=0;
	while ($list = db_fetch($lists))
	{
		$accs=array();
		$spas='';
		for($a=0;$a<$list['level'];$a++){
			$spas.='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		$nilai=0;
		$faktor=0;
		$total=0;
		$formulir='';$lajur='';$baris='';
		if($list['no']==9){
			$tl=total_v1_2();
			$nilai=$tl[0];
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='100';
			$t13=$t13+$nilai;
		}
		if($list['no']==11){
			$tl=total_v2_2();
			$nilai=$tl[0];
			$formulir='V.D.5-2';
			$lajur='B';
			$baris='164';
			$t13=$t13-$nilai;
		}
		if($list['no']==12){
			$formulir='V.D.5-3';
			$lajur='B';
			$baris='31';
			$t13=$t13-$nilai;
		}
		if($list['no']==13){
			$nilai=$t13;
		}

		if($list['no']==15){
			$nilai=$t13;
		}

		if($list['no']==17){
			$formulir='V.D.5-2';
			$lajur='B';
			$baris='163';
		}
		if($list['no']==20){
			$total=$t13;
		}
		if($list['no']==24){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='16';
		}
		if($list['no']==26){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='18';
		}
		if($list['no']==28){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='20';
			$faktor=0.05;
		}
		if($list['no']==29){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='21';
			$faktor=0.05;
		}
		if($list['no']==30){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='22';
			$faktor=1;
		}
		if($list['no']==31){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='23';
			$faktor=1;
		}
		if($list['no']==33){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='59';
			$faktor=0.05;
		}
		if($list['no']==35){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='61';
			$faktor=0.05;
		}
		if($list['no']==36){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='62';
			$faktor=0.075;
		}
		if($list['no']==37){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='63';
			$faktor=0.1;
		}

		$tab.='VD59.'.$list['no'].'||||'.$nilai.'||'.$total.'||||';
		if (get_post('Export')) 
		{
			$tab.="\r\n";
		}else{
			$tab.='<br>';
		}


	}
	return $tab;
}
function get_totals($accounts,$tgl){
	$accounts=get_gl_accounts_in($accounts);
	$total2=0;
	$tgl=explode('/', $tgl);
	$begin=date('m/d/Y',strtotime($tgl[2].'-'.$tgl[1].'-01'));
	while ($account = db_fetch($accounts))
	{	
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $_POST['TransFromDate'], false, true);
		$total2=$total2+($tot['debit']-$tot['credit']);
	}
	return $total2;

}
if (get_post('Export')) 
{
}
	else{
gl_inquiry_controls();

div_start('trans_tbl');
echo '<center>';
}
if (get_post('Show') || get_post('Export'))
    show_results();

if (get_post('Export')) 
{
}
	else{
echo '</center>';
div_end();

//----------------------------------------------------------------------------------------------------

end_page();
}

