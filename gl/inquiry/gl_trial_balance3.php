<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLANALYTIC';
$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/inquiry/netprofit.php");

$js = "";
if (user_use_date_picker())
	$js = get_js_date_picker();

if (get_post('Export')) 
{
	//include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"TB.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
page(_($help_context = "Trial Balance "), false, false, "", $js);
}


$k = 0;
$pdeb = $pcre = $cdeb = $ccre = $tdeb = $tcre = $pbal = $cbal = $tbal = 0;

//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('balance_tbl');
}

if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$thnto=@$_POST['tahunfrom'];
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER,2);

	$date = today();
		if (!isset($_POST['TransToDate']))
			$_POST['TransToDate'] = end_month($date);
		if (!isset($_POST['TransFromDate']))
			$_POST['TransFromDate'] = add_days(end_month($date), -user_transaction_days());
	echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	/*
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	*/
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo '</tr>';
	echo '<tr>';
    date_cells(_("&nbsp;"), 'TransFromDate');
	date_cells(_("&nbsp;"), 'TransToDate');
	/*
	if ($dim >= 1)
		dimensions_list_cells(_("Dimension")." 1:", 'Dimension', null, true, " ", false, 1);
	if ($dim > 1)
		dimensions_list_cells(_("Dimension")." 2:", 'Dimension2', null, true, " ", false, 2);
	*/
	echo '</tr>';
	echo '<tr>';
	check_cells(_("No zero values"), 'NoZero', null);
	check_cells(_("Account Code"), 'AccCode', null);
	check_cells(_("Title"), 'headtitle', null);
	echo '</tr>';
	echo '<tr>';
	//echo '<td>&nbsp;</td>';
	submit_cells('Show',_("Show"),'align="center"','', 'default');
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel"></td>';
	echo '</tr>';
	echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}

	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
		function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);
	 		pilblnto(bln);

	 	}
	 	function pilblnto(bln){
	 		//if(bln<10)
	 		//	bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "TB",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
    end_table();
    end_form();
}
//----------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------

	if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanfrom'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

	$begin=date('d/m/Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($blnto!='')
	$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, @$blnto, @$thnto)));
	
function display_bs(){

	$cdeb = 0;
	$ccre = 0;
	$tdeb = 0;
	$tcre = 0;
	$colspan=5;
	$cols=4;
	if($_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=$_POST['bulanfrom'];
		$blnto=$_POST['bulanfrom'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$begin=date('d/m/Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));

	echo '<center><table class="tablestyle" width="100%" cellpadding="3" border="0">';
	echo '<tr>
	<td class="tableheader" bgcolor="#dee7ec"><b>Account</b></td>
	<td class="tableheader" bgcolor="#dee7ec"><b>Debit</b></td>
	<td class="tableheader" bgcolor="#dee7ec"><b>Credit</b></td>
	<td class="tableheader" bgcolor="#dee7ec"><b>YTD Debit	</b></td>
	<td class="tableheader" bgcolor="#dee7ec"><b>YTD Credit
    </b></td></tr>';
	

	$totalaset=0;
	//echo '<tr><td colspan="'.(1+$blnto).'">Kas dan Setara Kas</td></tr>';
	//get_account_from_to
	$ttlall=0;$ttlall2=0;
	//asset lancar
	//$accounts=get_account_from_to('10000','12000');
	$accounts = get_chart_accounts_search('');

	$notsub=array('16699');
	$totalsub=array('11140','11164','11208','11305','11499','11590'
					,'11699','11706','12102','12212','12222','12232'
					,'12242','12251','12262','12301','12401','12999'
					,'21140','21280','21301','21601','21701','21903'
					,'22101','22901'
					,'39999');
	$totalsub2=array('11706','12102','12262');
	$totalsub3=array('12999','22901');
	$tnetasset='11706';
	$head=array();$headparent=array();$headparent2=array();

	$ttlcol=array();$ttlcol2=array();$ttlcol3=array();$ttlcol4=array();$ttlcolsub=array();
	$ttlalls=array();$ttlalls2=array();
	$ttlclass=0;$ttlclass2=0;$ttlclass3=0;$ttlclass4=0;
	$a=0;$n=0;$hp=0;$hp2=0;
	$path_to_root="../..";

	$header=0;
	while ($account = db_fetch($accounts))
	{

		if (strlen($account["accode"])>5)
			continue;
		$curr = get_balance($account["accode"], @$_POST['Dimension'], @$_POST['Dimension2'], $begin, $to, true, true);
		$tot = get_balance($account["accode"], @$_POST['Dimension'], @$_POST['Dimension2'], $begin, $to, false, true);
		if (check_value("NoZero") && !$tot['balance'])
			continue;
		if ($account["accode"]=='39000')
			continue;

		$kls=cek_type_account(@$account["name"]);
		//print_r($kls);
		//header
		if (check_value("headtitle")){
			if(!in_array(@$kls[4],$headparent2) && @$kls[6]==''){
				echo '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';
				echo '<tr class="inquirybg2"><td><b>&nbsp;'.@$kls[5].'</td><td colspan="'.($cols).'"><b>'.@$account["headparent"].'</td></tr>';
				$headparent2[]=@$kls[4];
				$hp2++;			
				//echo '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';
			}

			if(!in_array(@$kls[2],$headparent) && @$kls[3]!=@$account["name"]){
				//echo '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';
				echo '<tr class="inquirybg2"><td><b>&nbsp;'.@$kls[3].'</td><td colspan="'.($cols).'"><b>'.@$account["headparent"].'</td></tr>';
				$headparent[]=@$kls[2];
				$hp++;			
				//echo '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';
			}else{
				$headparent[]=@$kls[2];
			}

			if(!in_array(@$account["name"],$head) && $header==0 ){
				echo '<tr class="inquirybg2"><td><b>&nbsp;'.$account["name"].'</td><td colspan="'.($cols).'"><b>'.@$account["headparent"].'</td></tr>';
				//$headparent[]=$account["headparent"];
				$head[$a]=@$account["name"];
			}
		}

		alt_table_row_color($k);

		$url = "&nbsp;&nbsp;&nbsp;&nbsp; <a href='$path_to_root/gl/inquiry/gl_account_inquiry.php?TransFromDate=" . @$_POST["TransFromDate"] . "&TransToDate=" . @$_POST["TransToDate"] . "&account=" . $account["accode"] . "&Dimension=" . @$_POST["Dimension"] . "&Dimension2=" . @$_POST["Dimension2"] . "'>" 
		. $account["account_code"] 
		. "</a>";
		
		//label_cell('');
		$acode='';
		if (strlen($account["accode"])>5)
		$acode='&nbsp;&nbsp;&nbsp;&nbsp;';
		if(@$_POST['AccCode']==1)
		$acode.=$account["account_code"].' - ';
		label_cell($acode.$account["account_name"]);
		//label_cell('_'.$account["account_code2"]);

		$dbt=0;
		$crd=0;
		$ttl=($curr['balance']);
		
		/*
		if($account["account_code"]=='38000'){
			$tot2 = get_balance('39000', $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
			$ttl+=@$tot2['balance'];
		}
		*/
		
		$notsub=array('89999');
		if(!in_array($account["accode"],$notsub)){
			$subaccounts=get_subaccount($account["accode"]);
			while ($subaccount = db_fetch($subaccounts))
			{
				$tot3 = get_balance($subaccount["account_code"], @$_POST['Dimension'], @$_POST['Dimension2'], $begin, $to, true, true);
				$ttl+=($tot3['balance']);
			}
		}
		

		if($ttl>0)
			$dbt=$ttl;
		if($ttl<0)
			$crd=abs($ttl);
		amount_cell($dbt);
		amount_cell($crd);

		$ttl2=0;
		$dbt2=0;
		$crd2=0;
		$ttl2=@$tot['debit']-@$tot['credit'];

		if($account["accode"]=='38000'){
			$tot2 = get_balance('39000', @$_POST['Dimension'], @$_POST['Dimension2'], $begin, $to, false, true);
			$ttl2+=@$tot2['balance'];
		}
		
		if(!in_array($account["accode"],$notsub)){
			$subaccounts=get_subaccount($account["accode"]);
			while ($subaccount = db_fetch($subaccounts))
			{
				$tot4 = get_balance($subaccount["account_code"], @$_POST['Dimension'], @$_POST['Dimension2'], $begin, $to, false, true);
				$ttl2+=($tot4['balance']);
			}
		}
		
		if($ttl2>0)
			$dbt2=$ttl2;
		if($ttl2<0)
			$crd2=abs($ttl2);
		amount_cell(@$dbt2);
		amount_cell(@$crd2);

		$cdeb += $dbt;
		$ccre += $crd;
		$tdeb += $dbt2;
		$tcre += $crd2;

		end_row();
		$n++;
	}	

	echo '</tr>';
	echo $tableheader='<tr class="inquirybg" style="font-weight:bold"><td colspan="'.($colspan).'">&nbsp;</td></tr>';
	
		start_row("class='inquirybg2' style='font-weight:bold'");
		label_cell(_("Total") ." - ".$_POST['TransToDate']);
		//amount_cell(@$pdeb);
		amount_cell(@$cdeb);
		amount_cell(@$ccre);
		amount_cell(@$tdeb);
		amount_cell(@$tcre);
		end_row();
		echo $tableheader='<tr class="inquirybg" style="font-weight:bold"><td colspan="6">&nbsp;</td></tr>';

	echo'</table></center>';
	//return $tableheader;
}
//----------------------------------------------------------------------------------------------------

if (get_post('Export')) 
{
	
	$header='
	<style>
	.inquirybg2{
		font-weight:bold !important;
		font-size:12px !important;
	}
	.tableheader{
		background-color:#EE7600 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0">
		<tr><td align="center">&nbsp;</td></tr>
		<tr><td style="width:2%;">&nbsp;&nbsp;</td>
			<td>
			<table border="0">
				<tr>
					<td width="100%" align="center"  style="background-color:#bfbfbf;color:#ee3000;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:20px;color:#ee3000;font-weight:bold;">Trial Balance</td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;color:#ee3000;font-weight:bold;">'.@$blnnya.'</td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td>
				</tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 600px;">';
	div_start('balance_tbl');
	echo display_bs();
	div_end();
	echo '</td></tr></table>
		</td></tr>
		<tr><td></td><td align="center" style="background-color:#EE7600;border:#000 1px solid;">&nbsp;</td></tr>
		</table>';
}elseif(get_post('PrintPdf')){

// add a page
	$header='<table border="0">
		<tr>
			<td width="80%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">Trial Balance</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	$header.= '<tr><td style="width: 648px;">';
	$header.=display_bs();
	$header.= '</td></tr></table>';
$pdf->AddPage();
// output the HTML content
$html=$header;
//$html.=display_bs();
$pdf->writeHTML(@$html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_BS.pdf', 'I');	

}else{
	gl_inquiry_controls();
	div_start('balance_tbl');

	if (get_post('Show')) {
		echo '<center><table border="0" cellpadding="4" width="80%">
		<tr>		
			<td align="center" style="font-weight:bold;">Trial Balance</td>
		</tr>
		<tr>
			<td align="center" style="font-weight:bold;">Periode '.@$blnnya.'</td>
		</tr>';
		echo '<tr>		
			<td align="center">';
		echo display_bs();
		echo '</td></tr></table></center>';
	}
	//if ($_POST['Show']) 
	//echo display_bs();
	div_end();
	end_page();
}

