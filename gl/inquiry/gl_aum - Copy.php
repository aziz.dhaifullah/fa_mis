<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/config_mssql.php");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/siar.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format(0);

if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"AUM.xls\"");
	header("Cache-Control: max-age=0");
	
	
}else{
page(_($help_context = "AUM"), false, false, "", $js);
}


//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}

//----------------------------------------------------------------------------------------------------

function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER);
    start_row();
	echo "<td>"._("From").":</td>\n";
    echo "<td>";
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
    end_row();
	start_row();
   // gl_all_accounts_list_cells(_("Account:"), 'account', null, false, false, _("All Accounts"));
	echo '<tr>';
	date_cells(_("&nbsp;"), 'TransFromDate', '', null, -user_transaction_days());
	date_cells(_("&nbsp;"), 'TransToDate');
	echo "<td>"._("Option View").":</td>\n";
	echo "<td>";

$opt_view = array(
	_("Detail"),
	_("Summary"),
	_("Summary Per Product")
);
	echo array_selector('Option', null, $opt_view);
	echo "</td>\n";
	echo "<td>"._("Category").":</td>\n";
	echo "<td>";

$cats = array(
	_("KPD"),
	_("RDPT"),
	_("Konvensional")
);
	echo array_selector('Category', null, $cats);
	echo "</td>\n";
/*
	echo "<td>"._("Product").":</td>\n";
	echo "<td>";
	
$product = array(
	_("All")
);
	echo array_selector('Product', null, $product);
	echo "</td>\n";
	*/
    end_row();
	start_row();
	echo '<td colspan="2">Rate: <input type="text" name="rate" size="3" value="0.045"> %</td>';
	echo "<td>";
	echo '<input type="checkbox" name="iuranojk" value="1">';
	echo "</td>\n";
	echo "<td>"._("Hitung Iuran OJK")."</td>\n";
    end_row();
	end_table();

	start_table(TABLESTYLE_NOBORDER);
	start_row();

	
	
	echo '</tr>';
	echo '<tr>';
	echo '<td colspan="4">&nbsp;</td>';
	submit_cells('Show',_("Show"),'align="right"','', 'default');
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel"></td>';
	echo '</tr>';
	echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle2").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "AUM",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';

	//submit_cells('Export Excel',_("Export"),'','', 'default');
	end_row();
	end_table();

	echo '<hr>';
    end_form();
}

//----------------------------------------------------------------------------------------------------

function show_results()
{
	global $path_to_root, $systypes_array;

	$nholiday=n_holiday_period(date2sql($_POST['TransFromDate']),date2sql($_POST['TransToDate']));
	$nhari = ((abs(strtotime (date2sql($_POST['TransFromDate'])) - strtotime (date2sql($_POST['TransToDate']))))/(60*60*24));
	$nbursa = $nhari-$nholiday;
	$colspan = ($dim == 2 ? "6" : ($dim == 1 ? "5" : "4"));

	if ($_POST["account"] != null)
		display_heading($_POST["account"]. "&nbsp;&nbsp;&nbsp;".$act_name);

	$garis='';$garis2='';$brdr='';
	if (get_post('Export')){ 
		$brdr='border="1"';
		$garis='style="border:#000 1px solid;"';
		$garis2='style="border:#000 1px solid;background-color:#bfbfbf;"';
	}
	start_table(TABLESTYLE2, "width='90%'".$brdr, 10);
	
	$prods=get_nav_siar2(date2sql($_POST['TransFromDate']),date2sql($_POST['TransToDate']));
	if($_POST["Option"]==0){
		$th = array(_("No"), _("Date"));
		$th=array_merge($th,$prods[0]);
		table_header($th);
	}
	elseif($_POST["Option"]==2){
		//$th = array( _("Produk"));

	}
	else{
		$th = array(_("No"), _("Tanggal Hari Bursa"));
		$th2 = array(_("KPD"), _("RD Konvensional"),_("RDPT"),_("Jumlah"));
		$th=array_merge($th,$th2);
		echo $th='<tr>
<td class="tableheader" rowspan="2" valign="middle" style="font-weight:bold;">No</td>
<td class="tableheader" rowspan="2" valign="middle" style="font-weight:bold;">Tanggal Hari Bursa</td>
<td class="tableheader" colspan="4" align="center" style="font-weight:bold;">Nilai Dalam Rupiah</td>
</tr>
<tr>
<td class="tableheader" align="center" style="font-weight:bold;">KPD</td>
<td class="tableheader" align="center" style="font-weight:bold;">RD Konvensional</td>
<td class="tableheader" align="center" style="font-weight:bold;">RDPT</td>
<td class="tableheader" align="center" style="font-weight:bold;">Jumlah</td>
</tr>';
		//table_header($th);
	}
	//print_r($th);
	//$th = array_merge($first_cols, $account_col, $dim_cols, $remaining_cols);
			
	$bfw = 0;

	$running_total = $bfw;
	$j = 1;
	$k = 0; //row colour counter
	$no=1;

	//print_r($prods[0]);
	$selisih = ceil((abs(strtotime (date2sql($_POST['TransFromDate'])) - strtotime (date2sql($_POST['TransToDate']))))/(60*60*24));
	
	$total=array();$habur=array();
	$totalkpd=0;$totalkonven=0;$totalrdpt=0;$habur2=0;
	
	if($_POST["Option"]!=2){
		for($a=0;$a<count($prods[2]);$a++){
			//$newdate = strtotime ( '+'.$a.' day' , strtotime ( date2sql($_POST['TransFromDate']) ) ) ; 
			$newdate2 = date ( 'Y-m-d' ,strtotime($prods[2][$a])).' 00:00:00';
			$tgl = date ( 'd M Y' , strtotime($prods[2][$a]) );
			echo '<tr>';
	    	label_cell($no, $garis);
			label_cell($tgl,$garis);
			if($_POST["Option"]==0){
				for($b=0;$b<count($prods[0]);$b++){
					$unit=0;
					//print_r($prods[3]);
					//print_r($prods[3]);
					//$unit=get_unit_siar($prods[1][$b],$newdate2);
					$nav=$prods[3][$newdate2][$prods[0][$b]];
					//print_r($prods[3][$newdate2]);
					$unit=$prods[4][$newdate2][$prods[0][$b]];
					//print_r($prods[4][$newdate2]);
					$aum=$nav*@$unit;
					//label_cell($unit.'-'.$nav);
					$total[$b]=$total[$b]+$aum;
					$habur[$b]=$habur[$b]+1;
					label_cell(price_format($aum),'align="right"');
					//label_cell($a.'-'.$b.'-'.$nav.'-'.$unit,'align="right"');
				}
				
			}
			elseif($_POST["Option"]==2){

			}else{
				$kpd=0;$rdpt=0;$konven=0;
				for($b=0;$b<count($prods[0]);$b++){	
					$unit=0;	
					//$unit=get_unit_siar($prods[1][$b],$newdate2);
					$nav=$prods[3][$newdate2][$prods[0][$b]];
					$unit=$prods[4][$newdate2][$prods[0][$b]];
					$aum=$nav*@$unit;		
					//RD Konvensional 
					if( strpos( strtolower($prods[0][$b]), 'KAM' ) !== false ){
						$konven=$konven+$aum;
					}
					else if( strpos( strtolower($prods[0][$b]), 'rdpt' ) !== false ){
						$rdpt=$rdpt+$aum;
					}else{
						$kpd=$kpd+$aum;
					}
				}
				$totalkonven=$totalkonven+$konven;
				$totalkpd=$totalkpd+$kpd;
				$totalrdpt=$totalrdpt+$rdpt;
				label_cell(price_format($kpd),'align="right" '.$garis);
				label_cell(price_format($konven),'align="right" '.$garis);
				label_cell(price_format($rdpt),'align="right" '.$garis);
				label_cell(price_format($rdpt+$kpd+$konven),'align="right" '.$garis);
				//label_cell('');
			}
			$no++;
		}	
		//total aum
		echo '<tr>';
			label_cell('');
    	label_cell('<b>Total</b>',$garis);
		if($_POST["Option"]==0){
			for($b=0;$b<count($prods[0]);$b++){
				label_cell('<b>'.price_format($total[$b]).'</b>','align="right"');
			}			
		}
		elseif($_POST["Option"]==2){

		}
		else{
			label_cell('<b>'.price_format($totalkpd).'</b>','align="right" '.$garis);
			label_cell('<b>'.price_format($totalkonven).'</b>','align="right" '.$garis);
			label_cell('<b>'.price_format($totalrdpt).'</b>','align="right" '.$garis);
			$ttlall=($totalrdpt+$totalkpd+$totalkonven);
			label_cell('<b>'.price_format($ttlall).'</b>','align="right" '.$garis);
		}
		if($_POST["iuranojk"]==1){
			//jml hari bursa
			if($_POST["Option"]==0){
				echo '<tr>';
				label_cell('');
	    		label_cell('<b>Jumlah <br>Hari Bursa</b>');
				for($b=0;$b<count($prods[0]);$b++){
					label_cell('<b>'.$habur[$b].'</b>','align="right"');
				}		
				echo '<tr>';
				label_cell('');
				label_cell('<b>Rata-rata Harian AUM</b>');
				for($b=0;$b<count($prods[0]);$b++){
					label_cell('<b>'.price_format(($total[$b]/$habur[$b])).'</b>','align="right"');
				}	
				echo '<tr>';
				label_cell('');
				label_cell('<b>Dasar Pengenaan Iuran</b>');
				for($b=0;$b<count($prods[0]);$b++){
					label_cell('<b>'.price_format(($total[$b]/$habur[$b])).'</b>','align="right"');
				}				
				
				echo '<tr>';
				label_cell('');
	    		label_cell('<b>Iuran OJK ('.$_POST["rate"].'%)</b>',$garis2);
				for($b=0;$b<count($prods[0]);$b++){
					$iujk=($total[$b]/$habur[$b])*$_POST["rate"];
					label_cell('<b>'.price_format(@$iujk).'</b>','align="right" '.$garis2);
				}			
			}else{
			echo '<tr>';
				label_cell('');
	    		label_cell('<b>Jumlah Hari Bursa</b>','colspan="4"');
				label_cell('<b>'.$a.'</b>','align="right"');
				//label_cell($a,'align="right"');
				//label_cell($a,'align="right"');
				//label_cell('');
				echo '<tr>';
				label_cell('',$garis);
				label_cell('<b>Rata-rata Harian AUM</b>','colspan="4" '.$garis);			
				label_cell('<b>'.price_format($ttlall/$a),'align="right" '.$garis);
				echo '<tr>';
				label_cell('',$garis);
				label_cell('<b>Dasar Pengenaan Iuran</b>','colspan="4" '.$garis);		
				label_cell('<b>'.price_format($ttlall/$a).'</b>','align="right" '.$garis);
				echo '<tr>';
					label_cell('',$garis2);
			}
			//Iuran OJK
			
			if($_POST["Option"]==0){ 

			}else{
				/*
				$iujkkpd=($totalkpd/$a)*$_POST["rate"];
				label_cell(price_format($iujkkpd),'align="right"');
				$iujkkonven=($totalkonven/$a)*$_POST["rate"];
				label_cell(price_format($iujkkonven),'align="right"');
				$iujkrdpt=($totalrdpt/$a)*$_POST["rate"];
				label_cell(price_format($iujkrdpt),'align="right"');
				*/
				label_cell('<b>Iuran OJK ('.$_POST["rate"].'%)</b>','colspan="4" '.$garis2);		
				label_cell('<b>'.price_format(($ttlall/$a)*$_POST["rate"]).'</b>','align="right" '.$garis2);
				//label_cell('');	
			}
		}
	}else{
		$kpds=array();$totkpds=array();
		$rdkonvens=array();$totrdkonvens=array();
		$rdpts=array();$totrdpts=array();
		for($b=0;$b<count($prods[0]);$b++){		
			//$ttlnya=0;	
					$aum=0;
		$kpd=0;$rdpt=0;$konven=0;
				for($a=0;$a<count($prods[2]);$a++){
				//$newdate = strtotime ( '+'.$a.' day' , strtotime ( date2sql($_POST['TransFromDate']) ) ) ; 
					$newdate2 = date ( 'Y-m-d' ,strtotime($prods[2][$a])  );
					$unit=get_unit_siar($prods[1][$b],$newdate2);
					$nav=$prods[3][$newdate2][$prods[0][$b]];
					$aum=$nav*$unit;	
					//$ttlnya=$ttlnya+$aum;	
				}
				//RD Konvensional 
				if( strpos( strtolower($prods[0][$b]), 'KAM' ) !== false ){
					$rdkonvens[]=$prods[0][$b];
					$konven=$konven+$aum;
					$totrdkonvens[]=$konven;
				}
				else if( strpos( strtolower($prods[0][$b]), 'rdpt' ) !== false ){
					$rdpts[]=$prods[0][$b];
					$rdpt=$rdpt+$aum;
					$totrdpts[]=$rdpt;
				}else{
					$kpds[]=$prods[0][$b];
					$kpd=$kpd+$aum;
					$totkpds[]=$kpd;
				}
				
			//label_cell($prods[0][$b]);
		}
		//table_header(array( _("<b>KPD</b>"),_("")));
		echo '<tr><td class="tableheader" colspan="2" align="center" style="font-weight:bold;"><b>KPD</b></td></tr>';
		$totalkpd=0;
		for($a=0;$a<count($kpds);$a++){
			echo '<tr>';
			label_cell($kpds[$a]);
			label_cell(price_format(@$totkpds[$a]),'align="right"');
			$totalkpd=$totalkpd+@$totkpds[$a];
			echo '</tr>';
		}
		echo '<tr>';
		label_cell('<b>Total KPD</b>');
		label_cell('<b>'.price_format(@$totalkpd).'</b>','align="right"');
		echo '</tr>';

		//table_header(array( _("<b>RD Konvensional</b>"),_("")));
		echo '<tr><td colspan="2" align="center" style="font-weight:bold;">&nbsp</td></tr>';
		echo '<tr><td class="tableheader" colspan="2" align="center" style="font-weight:bold;"><b>RD Konvensional</b></td></tr>';
		$totalrdkonven=0;
		for($a=0;$a<count($rdkonvens);$a++){
			echo '<tr>';
			label_cell($rdkonvens[$a]);
			label_cell(price_format(@$totrdkonvens[$a]),'align="right"');
			$totalrdkonven=$totalrdkonven+@$totrdkonvens[$a];
			echo '</tr>';
		}
		echo '<tr>';
		label_cell('<b>Total RD Konvensional</b>');
		label_cell('<b>'.price_format(@$totalrdkonven).'</b>','align="right"');
		echo '</tr>';

		//table_header(array( _("<b>RDPT</b>"),_("")));
		echo '<tr><td colspan="2" align="center" style="font-weight:bold;">&nbsp</td></tr>';
		echo '<tr><td class="tableheader" colspan="2" align="center" style="font-weight:bold;"><b>RDPT</b></td></tr>';
		$totalrdpt=0;
		for($a=0;$a<count($rdpts);$a++){
			echo '<tr>';
			label_cell($rdpts[$a]);
			label_cell(price_format(@$totrdpts[$a]),'align="right"');
			$totalrdpt=$totalrdpt+@$totrdpts[$a];
			echo '</tr>';
		}
		echo '<tr>';
		label_cell('<b>Total RDPT</b>');
		label_cell('<b>'.price_format(@$totalrdpt).'</b>','align="right"');
		echo '</tr>';

		table_header(array( _(""),_("")));
		echo '<tr>';
		label_cell('<b>Jumlah Hari Bursa</b>');
		label_cell('<b>'.$nbursa.'</b>','align="right"');
		echo '</tr>';
		echo '<tr>';
		label_cell('<b>Rata-rata Harian AUM</b>');
		label_cell('<b>'.price_format((@$totalrdpt+@$totalrdpt+@$totalrdkonven)/$nbursa).'</b>','align="right"');
		echo '</tr>';
		echo '<tr>';
		label_cell('<b>Dasar Pengenaan Iuran</b>');
		label_cell('<b>'.price_format((@$totalrdpt+@$totalrdpt+@$totalrdkonven)/$nbursa).'</b>','align="right"');
		echo '</tr>';
		echo '<tr>';
		label_cell('<b>Iuran OJK ('.$_POST["rate"].'%)</b>',$garis2);
		label_cell('<b>'.price_format(((@$totalrdpt+@$totalrdpt+@$totalrdkonven)/$nbursa)*$_POST["rate"]).'</b>','align="right" '.$garis2);
		echo '</tr>';
		echo '</table>';
	}
}
//----------------------------------------------------------------------------------------------------

if (get_post('Export')) 
{
	
	$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
	$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
	$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
	$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	$blnnya=date('M Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));

	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('M',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('M Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
	$header='
	<style>
	.tableheader{
		background-color:#000 !important;
		color:#fff !important;
	}
	.tablestyle{
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0" style="border:#000 1px solid;">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">AUM</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 851pt;">';
	show_results();

	if($_POST["Option"]==1){
	echo '</table>';
	echo '</td></tr>';
	echo '<tr><td align="right">';
	echo '<table width="851pt" border="0">
		<tr><td colspan="6">&nbsp;</td></tr>
		<tr><td colspan="4"></td><td align="center" colspan="2">Jakarta,.................</td></tr>
		<tr><td colspan="6">&nbsp;</td></tr>
		<tr><td colspan="6">&nbsp;</td></tr>
		<tr><td colspan="6">&nbsp;</td></tr>
		<tr><td colspan="4"></td><td align="center" colspan="2">&nbsp;</td></tr>
		<tr><td colspan="4"></td><td align="center" colspan="2">Direktur</td></tr>
		<tr><td colspan="6">&nbsp;</td></tr>
		</table>';
	}
	echo '</td></tr></table>';
}else{
	gl_inquiry_controls();

	div_start('trans_tbl');

	if (get_post('Show') || get_post('Excel'))
	    show_results();

	div_end();

	//----------------------------------------------------------------------------------------------------

	end_page();
}

