<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLANALYTIC';
$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

$js = "";
if (user_use_date_picker())
	$js = get_js_date_picker();

if (get_post('Export')) 
{
	//include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"TB.xls\"");
	header("Cache-Control: max-age=0");
	
	
}else{
page(_($help_context = "Trial Balance"), false, false, "", $js);
}


$k = 0;
$pdeb = $pcre = $cdeb = $ccre = $tdeb = $tcre = $pbal = $cbal = $tbal = 0;

//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('balance_tbl');
}

	if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER,2);

	$date = today();
		if (!isset($_POST['TransToDate']))
			$_POST['TransToDate'] = end_month($date);
		if (!isset($_POST['TransFromDate']))
			$_POST['TransFromDate'] = add_days(end_month($date), -user_transaction_days());
	echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo '</tr>';
	echo '<tr>';
    date_cells(_("&nbsp;"), 'TransFromDate');
	date_cells(_("&nbsp;"), 'TransToDate');
	/*
	if ($dim >= 1)
		dimensions_list_cells(_("Dimension")." 1:", 'Dimension', null, true, " ", false, 1);
	if ($dim > 1)
		dimensions_list_cells(_("Dimension")." 2:", 'Dimension2', null, true, " ", false, 2);
	*/
	check_cells(_("No zero values"), 'NoZero', null);
	check_cells(_("Account Code"), 'AccCode', null);
	echo '</tr>';
	echo '<tr>';
	echo '<td colspan="5">&nbsp;</td>';
	submit_cells('Show',_("Show"),'align="right"','', 'default');
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel"></td>';
	echo '</tr>';
	echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}

	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
		function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "TB",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
    end_table();
    end_form();
}

//----------------------------------------------------------------------------------------------------

function display_trial_balance($type, $typename)
{
	global $path_to_root, $SysPrefs,
		 $k, $pdeb, $pcre, $cdeb, $ccre, $tdeb, $tcre, $pbal, $cbal, $tbal;

	$printtitle = 0; //Flag for printing type name

	$k = 0;

	//Get Accounts directly under this group/type
	$accounts = get_gl_accounts(null, null, $type);
	/*
	$begin = get_fiscalyear_begin_for_date($_POST['TransFromDate']);
	if (date1_greater_date2($begin, $begin))
		$begin = $_POST['TransFromDate'];
	$begin = add_days($begin, -1);
	*/

	if($_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=$_POST['bulanfrom'];
		$blnto=$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

	$begin=$_POST['TransFromDate'];
	$awal = add_days($begin, -1);
	$to=$_POST['TransToDate'];
	while ($account = db_fetch($accounts))
	{
		//Print Type Title if it has atleast one non-zero account	
		if (!$printtitle)
		{
				start_row("class='inquirybg2' style='font-weight:bold'");
				$acode='';
				if(@$_POST['AccCode']==1)
				$acode=$type.' - ';
				label_cell(_("")."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$acode.$typename, "colspan=6");
				end_row();
				$printtitle = 1;

		}

		
		$offset = 0;
		//$prev = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $awal, false, true);
		$curr = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, true, true);
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		if (check_value("NoZero") && !$tot['balance'])
			continue;
		if ($account["account_code"]=='39000')
			continue;
		alt_table_row_color($k);

		$url = "&nbsp;&nbsp;&nbsp;&nbsp; <a href='$path_to_root/gl/inquiry/gl_account_inquiry.php?TransFromDate=" . $_POST["TransFromDate"] . "&TransToDate=" . $_POST["TransToDate"] . "&account=" . $account["account_code"] . "&Dimension=" . $_POST["Dimension"] . "&Dimension2=" . $_POST["Dimension2"] . "'>" 
		. $account["account_code2"] 
		. "</a>";

		//label_cell($url);
		label_cell('');
		$acode='';
		if(@$_POST['AccCode']==1)
		$acode=$account["account_code2"].' - ';
		label_cell($acode.$account["account_name"]);
		if (check_value('Balance'))
		{
			display_debit_or_credit_cells($prev['balance']);
			display_debit_or_credit_cells($curr['balance']);
			display_debit_or_credit_cells($tot['balance']);

		}
		else
		{
			//amount_cell($prev['debit']-$offset);
			//amount_cell($prev['credit']-$offset);
			$dbt=0;
			$crd=0;
			$ttl=($curr['balance']);
			
			/*
			if($account["account_code"]=='38000'){
				$tot2 = get_balance('39000', $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
				$ttl+=@$tot2['balance'];
			}
			*/
			$notsub=array('89999');
			//if(!in_array($account["account_code"],$notsub)){
				$subaccounts=get_subaccount($account["account_code"]);
				while ($subaccount = db_fetch($subaccounts))
				{
					$tot3 = get_balance($subaccount["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, true, true);
					$ttl+=($tot3['balance']);
				}
			//}

			if($ttl>0)
				$dbt=$ttl;
			if($ttl<0)
				$crd=abs($ttl);
			amount_cell($dbt);
			amount_cell($crd);
			//amount_cell($curr['debit']);
			//amount_cell($curr['credit']);
			$ttl2=0;
			$dbt2=0;
			$crd2=0;
			$ttl2=@$tot['debit']-@$tot['credit'];
			//if($ttl2==0)
			//$ttl2=@$tot['credit'];
			if($account["account_code"]=='38000'){
				$tot2 = get_balance('39000', $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
				$ttl2+=@$tot2['balance'];
			}
			
			//if(!in_array($account["account_code"],$notsub)){
				$subaccounts=get_subaccount($account["account_code"]);
				while ($subaccount = db_fetch($subaccounts))
				{
					$tot4 = get_balance($subaccount["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
					$ttl2+=($tot4['balance']);
				}
			//}
			
			//$ttl2=$ttl2-($curr['credit']-$offset);
			if($ttl2>0)
				$dbt2=$ttl2;
			if($ttl2<0)
				$crd2=abs($ttl2);
			amount_cell(@$dbt2);
			amount_cell(@$crd2);

			//amount_cell($tot['debit']-$offset);
			//amount_cell($tot['credit']-$offset);
			//$pdeb += $prev['debit'];
			//$pcre += $prev['credit'];
			$cdeb += $dbt;
			$ccre += $crd;
			$tdeb += $dbt2;
			$tcre += $crd2;
		}	
		//$pbal += $prev['balance'];
		//$cbal += $curr['balance'];
		$tbal += $tot['balance'];
		end_row();
	}

	//Get Account groups/types under this group/type
	$result = get_account_types(false, false, $type);
	while ($accounttype=db_fetch($result))
	{
		//Print Type Title if has sub types and not previously printed

		if (!$printtitle)
		{
			start_row("class='inquirybg2' style='font-weight:bold'");

			$acode='';
			if(@$_POST['AccCode']==1)
			$acode=$type.' - ';

			label_cell(_("")."&nbsp;&nbsp;&nbsp;&nbsp;".$acode.$typename, "colspan=6");
			end_row();
			$printtitle = 1;

		}
		//$cekclass=cek_class_account($accounttype["id"]);
		//if(@$cekclass[0]==''){
			$nottampil=array(
				'8-9999','6-9104','6-9101','6-8001','6-2002','6-2003','6-3003','1-1699'
			);
			if(!in_array($accounttype["id"], $nottampil))
				display_trial_balance($accounttype["id"], $accounttype["name"].' ');
		//}
	}
}

//----------------------------------------------------------------------------------------------------


function display_tb()
{
	global $path_to_root, $SysPrefs,
		 $k, $pdeb, $pcre, $cdeb, $ccre, $tdeb, $tcre, $pbal, $cbal, $tbal;
if (isset($_POST['TransFromDate']))
{
	$row = get_current_fiscalyear();
	if (date1_greater_date2($_POST['TransFromDate'], sql2date($row['end'])))
	{
		display_error(_("The from date cannot be bigger than the fiscal year end."));
		set_focus('TransFromDate');
		return;
	}
}
if (!isset($_POST['Dimension']))
	$_POST['Dimension'] = 0;
if (!isset($_POST['Dimension2']))
	$_POST['Dimension2'] = 0;
//start_table(TABLESTYLE);
	$tableheader='<div id="trans_tbl">';
	$tableheader.='<center><table class="tablestyle" cellpadding="6" border="0" width="100%">';
	$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
$tableheader .=  "
	<td class='tableheader' align='center'>" . _("Account") . "</td>
	<td class='tableheader' align='center'>" . _("Account Name") . "</td>
	<td class='tableheader' align='center'>" . _("Debit") . "</td>
	<td class='tableheader' align='center'>" . _("Credit") . "</td>
	<td class='tableheader' align='center'>" . _("YTD Debit") . "</td>
	<td class='tableheader' align='center'>" . _("YTD Credit") . "</td>
	";
	echo $tableheader.='</tr>';

	//display_trial_balance();

	//$classresult = get_account_classes(false);
	$begin=$_POST['TransFromDate'];
	$awal = add_days($begin, -1);
	$to=$_POST['TransToDate'];
	$classresult = get_all_coa();
	while ($account = db_fetch($classresult))
	{
		if (strlen($account["account_code"])>5)
			continue;
		$curr = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, true, true);
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		if (check_value("NoZero") && !$tot['balance'])
			continue;
		if ($account["account_code"]=='39000')
			continue;
		alt_table_row_color($k);

		$url = "&nbsp;&nbsp;&nbsp;&nbsp; <a href='$path_to_root/gl/inquiry/gl_account_inquiry.php?TransFromDate=" . $_POST["TransFromDate"] . "&TransToDate=" . $_POST["TransToDate"] . "&account=" . $account["account_code"] . "&Dimension=" . $_POST["Dimension"] . "&Dimension2=" . $_POST["Dimension2"] . "'>" 
		. $account["account_code2"] 
		. "</a>";
		
		label_cell('');
		$acode='';
		if (strlen($account["account_code"])>5)
		$acode='&nbsp;&nbsp;&nbsp;&nbsp;';
		if(@$_POST['AccCode']==1)
		$acode.=$account["account_code2"].' - ';
		//label_cell($acode.$account["account_name"]);
		label_cell('_'.$account["account_code2"]);

		$dbt=0;
		$crd=0;
		$ttl=($curr['balance']);
		
		/*
		if($account["account_code"]=='38000'){
			$tot2 = get_balance('39000', $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
			$ttl+=@$tot2['balance'];
		}
		*/
		
		$notsub=array('89999');
		if(!in_array($account["account_code"],$notsub)){
			$subaccounts=get_subaccount($account["account_code"]);
			while ($subaccount = db_fetch($subaccounts))
			{
				$tot3 = get_balance($subaccount["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, true, true);
				$ttl+=($tot3['balance']);
			}
		}
		

		if($ttl>0)
			$dbt=$ttl;
		if($ttl<0)
			$crd=abs($ttl);
		amount_cell($dbt);
		amount_cell($crd);
		//amount_cell($curr['debit']);
		//amount_cell($curr['credit']);
		$ttl2=0;
		$dbt2=0;
		$crd2=0;
		$ttl2=@$tot['debit']-@$tot['credit'];
		//if($ttl2==0)
		//$ttl2=@$tot['credit'];
		if($account["account_code"]=='38000'){
			$tot2 = get_balance('39000', $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
			$ttl2+=@$tot2['balance'];
		}
		
		if(!in_array($account["account_code"],$notsub)){
			$subaccounts=get_subaccount($account["account_code"]);
			while ($subaccount = db_fetch($subaccounts))
			{
				$tot4 = get_balance($subaccount["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
				$ttl2+=($tot4['balance']);
			}
		}
		
		
		//$ttl2=$ttl2-($curr['credit']-$offset);
		if($ttl2>0)
			$dbt2=$ttl2;
		if($ttl2<0)
			$crd2=abs($ttl2);
		amount_cell(@$dbt2);
		amount_cell(@$crd2);

		//amount_cell($tot['debit']-$offset);
		//amount_cell($tot['credit']-$offset);
		//$pdeb += $prev['debit'];
		//$pcre += $prev['credit'];
		$cdeb += $dbt;
		$ccre += $crd;
		$tdeb += $dbt2;
		$tcre += $crd2;

		end_row();
		/*
		start_row("class='inquirybg' style='font-weight:bold'");
		label_cell(_("").$class['class_name'], "colspan=8");
		end_row();
		*/

		//Get Account groups/types under this group/type with no parents
		//$typeresult = get_account_types(false, $class['cid'], -1);
		$nottampil=array(
			'8-9999'
		);
		/*
		while ($accounttype=db_fetch($typeresult))
		{
			if(!in_array($accounttype["id"], $nottampil))
			display_trial_balance($accounttype["id"], $accounttype["name"]);
		}
		*/
	}
	
	if (!check_value('Balance'))
	{
		echo $tableheader='<tr class="inquirybg" style="font-weight:bold"><td colspan="6">&nbsp;</td></tr>';
	
		start_row("class='inquirybg2' style='font-weight:bold'");
		label_cell(_("Total") ." - ".$_POST['TransToDate'], "colspan=2");
		//amount_cell(@$pdeb);
		//amount_cell(@$pcre);
		amount_cell(@$cdeb);
		amount_cell(@$ccre);
		amount_cell(@$tdeb);
		amount_cell(@$tcre);
		end_row();
		echo $tableheader='<tr class="inquirybg" style="font-weight:bold"><td colspan="6">&nbsp;</td></tr>';
	}	
	
	end_table(1);
	div_end();
}
//----------------------------------------------------------------------------------------------------

if (get_post('Export')) 
{
	$header='
	<style>
	.tableheader{
		background-color:#EE7600 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.inquirybg2{
		font-weight:bold !important;
		font-size:12px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0">
		<tr><td align="center">&nbsp;</td></tr>
		<tr><td style="width:2%;">&nbsp;&nbsp;</td>
			<td>
			<table border="0" style="border:#000 1px solid;">
				<tr>
					<td width="100%" align="center"  style="color:#ee3000;background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;color:#ee3000;font-size:20px;font-weight:bold;">Trial Balance</td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;color:#ee3000;font-weight:bold;">'.@$blnnya.'</td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 745pt;">';
	display_tb();
	echo '</td></tr></table>
		</td></tr>
		<tr><td></td><td align="center" style="background-color:#EE7600;border:#000 1px solid;">&nbsp;</td></tr>
		</table>';
}else{
	gl_inquiry_controls();

div_start('balance_tbl');
	if (get_post('Show')) {
		echo '<center><table border="0" cellpadding="4">
		<tr>		
			<td align="center" style="font-weight:bold;">Trial Balance</td>
		</tr>
		<tr>
			<td align="center" style="font-weight:bold;">'.@$blnnya.'</td>
		</tr>';
		echo '<tr>		
			<td align="center">';
		display_tb();
		echo '</td></tr></table></center>';
	}
	end_page();
}

