<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLANALYTIC';
$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/inquiry/netprofit.php");

$js = "";
if (user_use_date_picker())
	$js = get_js_date_picker();

if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"PL.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document

	if($_POST["tipe"]==0){
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	}else{
	$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	}

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
page(_($help_context = "Profit & Loss"), false, false, "", $js);
}


$k = 0;
$pdeb = $pcre = $cdeb = $ccre = $tdeb = $tcre = $pbal = $cbal = $tbal = 0;

//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('balance_tbl');
}


	if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER);

	$date = today();
	if (!isset($_POST['TransToDate']))
		$_POST['TransToDate'] = end_month($date);
	if (!isset($_POST['TransFromDate']))
		$_POST['TransFromDate'] = add_days(end_month($date), -user_transaction_days());
	echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo '<tr>';
    date_cells(_("&nbsp;"), 'TransFromDate');
	date_cells(_("&nbsp;"), 'TransToDate');
	echo "<td>"._("Type").":</td>\n";
	echo "<td>";
	$period = array(_("Outstanding"),_("Multiple"));
	//$period = array(_("Monthly"),_("Quarterly"),_("Semi Annyally"),_("Annually"));
	echo array_selector('tipe', null, $period);
	echo "</td>\n";
	/*
	if ($dim >= 1)
		dimensions_list_cells(_("Dimension")." 1:", 'Dimension', null, true, " ", false, 1);
	if ($dim > 1)
		dimensions_list_cells(_("Dimension")." 2:", 'Dimension2', null, true, " ", false, 2);
	*/
	//check_cells(_("No zero values"), 'NoZero', null);
	check_cells(_("Account Code"), 'AccCode', null);
	echo '</tr>';
	echo '<tr>';
	echo '<td colspan="2">&nbsp;</td>';
	submit_cells('Show',_("Show"),'align="right"','', 'default');
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">';
	//echo '&nbsp;<input type="submit" class="export" name="PrintPdf" id="PrintPdf" Value="Print PDF"></td>';
	echo '</tr>';
	echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Balance_sheet",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';

	//submit_cells('Show',_("Show"),'','', 'default');
    end_table();
    end_form();
}

//----------------------------------------------------------------------------------------------------

function display_trial_balance($type, $typename,$lvl=0)
{
	global $path_to_root, $SysPrefs,
		 $k, $pdeb, $pcre, $cdeb, $ccre, $tdeb, $tcre, $pbal, $cbal, $tbal;
	$tbals=array();

	$printtitle = 0; //Flag for printing type name

	$k = 0;

	//Get Accounts directly under this group/type
	$accounts = get_gl_accounts(null, null, $type);


	if($_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=$_POST['bulanfrom'];
		$blnto=$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

	$begin = get_fiscalyear_begin_for_date($_POST['TransFromDate']);
	if (date1_greater_date2($begin, $_POST['TransFromDate']))
		$begin = $_POST['TransFromDate'];

	$begin=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	$begin = add_days($begin, -1);
	$tbal=0;
	$t0=0;$begbal=0;
	while ($account = db_fetch($accounts))
	{
		if($account["account_code"]=='69900' or $account["account_code"]=='69990'){
			continue;
		}
		//Print Type Title if it has atleast one non-zero account	
		if (!$printtitle and $typename!='')
		{
			start_row("class='inquirybg' style='font-weight:bold'");
			$acode='';
			if(@$_POST['AccCode']==1)
			$acode=$type.' - ';
			label_cell(_("&nbsp;&nbsp;").$acode.$typename, "colspan=2");
			if($_POST["tipe"]==0){
				echo '<td align="center">'.$blnnya.'</td>';
			}else{
				for($a=$blnfrom;$a<=$blnto;$a++){
					echo '<td align="center">'.date('M Y',strtotime($thnto.'-'.$a.'-01')).'</td>';
				}
			}
			end_row();
			$printtitle = 1;
		}

		// FA doesn't really clear the closed year, therefore the brought forward balance includes all the transactions from the past, even though the balance is null.
		// If we want to remove the balanced part for the past years, this option removes the common part from from the prev and tot figures.
		if (@$SysPrefs->clear_trial_balance_opening)
		{
			$open = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin,  $begin, false, true);
			$offset = min($open['debit'], $open['credit']);
		} else
			$offset = 0;

		if (check_value("NoZero") && !$prev['balance'] && !$curr['balance'] && !$tot['balance'])
			continue;
		alt_table_row_color($k);

		label_cell('');
		$acode='';
		if(@$_POST['AccCode']==1)
		$acode=$account["account_code2"].' - ';
		$spasi='';
		if($lvl>1){
				$spasi.='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		label_cell($spasi.$acode.' '.$account["account_name"]);
		$kali=1;
		if($account["ctype"]==4 or $account["ctype"]==3)
			$kali=-1;
		if($_POST["tipe"]==0){
			$begbal=0;
			$bln1=date('d/m/Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
			$bln2=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));

			if($account["account_code"]==39000){
				$total=netprof($bln1,$bln2)*(-1);
				amount_cell_pr(($total)*$kali);
				$t0 += (@$total*$kali);
			}
			else{
				$tambah=0;
				if($account["account_code"]==38000){
					$tot = get_balance('39000', $_POST['Dimension'], $_POST['Dimension2'], $bln1, $bln2, true, true);
					//amount_cell_pr(($tot['balance']*$kali));
					$tambah=($tot['balance']*$kali);
				}
					$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $bln1, $bln2, true, true);
					amount_cell_pr(($tot['balance']*$kali)+@$tambah);
					$t0 += (@$tot['balance']*$kali)+@$tambah;
				
			}
		}else{
			for($a=$blnfrom;$a<=$blnto;$a++){

				$ttl=0;
				$bln1=date('d/m/Y',strtotime($thnfrom.'-'.@$a.'-01'));
				$bln2=date('d/m/Y',strtotime($thnto.'-'.@$a.'-'.cal_days_in_month(CAL_GREGORIAN, $a, $thnto)));
				if($account["account_code"]==39000){
					$ttl=$ttl+netprof($bln1,$bln2)*(-1)*$kali;
				}
				else{
					$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $bln1, $bln2, true, true);
					$ttl=$ttl+($tot['balance']*$kali);
				}
				if($account["account_code"]==38000){
					$tot = get_balance('39000', $_POST['Dimension'], $_POST['Dimension2'], $bln1, $bln2, false, true);
					$ttl=$ttl+($tot['balance']*$kali);
				}
				amount_cell_pr(($ttl));
				//amount_cell_pr(($tot['balance']+$prev['balance']));
				$tbals[$a] = @$tbals[$a]+($ttl);
			}
		}
		end_row();
		$tl=display_trial_balance($account["account_code2"], '',3);
		$t0+=$tl[0];
		for($a=$blnfrom;$a<=$blnto;$a++){
			$tbals[$a] = @$tbals[$a]+(@$tl[1][$a]);
		}
	}
	//echo $t0;
	//Get Account groups/types under this group/type
	$result = get_account_types(false, false, $type);
	$tbal2=0;$tbals2=array();
	while ($accounttype=db_fetch($result))
	{	
		$tbal3=0;
		$nsacc=n_subaccount(str_replace('-','',$accounttype["id"]));
		if($nsacc==0){
			//Print Type Title if has sub types and not previously printed
			if (!$printtitle)
			{
				start_row("class='inquirybg' style='font-weight:bold'");
				$acode='';
				if(@$_POST['AccCode']==1)
				$acode=$type.' - ';
				if($_POST["tipe"]==0)
					label_cell(_("").$acode.$typename, "colspan=3");
				else
					label_cell(_("").$acode.$typename, "colspan=".(3+$blnto-$blnfrom));
				end_row();
				$printtitle = 1;

			}
			$tl2=display_trial_balance($accounttype["id"], $accounttype["name"]);
			$tbal2=$tbal2+@$tl2[0];
			$tbal3=$tbal3+@$tl2[0];
			//echo $accounttype["name"].'('.$t0.' + '.$tbal2.')';
			//print_r($tl2);
			//echo '<br>';
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_("&nbsp;&nbsp;Total ").$accounttype["name"], "colspan=2");

			if($_POST["tipe"]==0){
				amount_cell_pr($tbal3);
			}else{
				for($a=$blnfrom;$a<=$blnto;$a++){
					amount_cell_pr(@$tl2[1][$a]);
					$tbals2[$a]=@$tbals2[$a]+@$tl2[1][$a];
					$tbals[$a]=@$tbals[$a]+@$tl2[1][$a];
				}
			}
			end_row();
		}
	}
	//echo $tbal;
	//if (!$printtitle)
	//{		
		/*start_row("class='inquirybg' style='font-weight:bold'");
		label_cell(_("Total ".$typename), "colspan=2");
		amount_cell(($tbal)+($tbal2));
		end_row();*/
	//}

	return array($t0+$tbal2,$tbals);
}

function display_trial_balance_pdf($type, $typename,$lvl=0)
{
	global $path_to_root, $SysPrefs,
		 $k, $pdeb, $pcre, $cdeb, $ccre, $tdeb, $tcre, $pbal, $cbal, $tbal;
	$tbals=array();
	$printtitle = 0; //Flag for printing type name

	$k = 0;

if($_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=$_POST['bulanfrom'];
		$blnto=$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

	//Get Accounts directly under this group/type
	$accounts = get_gl_accounts(null, null, $type);

	$begin = get_fiscalyear_begin_for_date($_POST['TransFromDate']);
	if (date1_greater_date2($begin, $_POST['TransFromDate']))
		$begin = $_POST['TransFromDate'];
	$begin = add_days($begin, -1);
	$tableheader='';

	if($_POST["tipe"]==0){
		$colom=2;
	}else{
		$colom=3+($blnto-$blnfrom);
	}
	while ($account = db_fetch($accounts))
	{
		//Print Type Title if it has atleast one non-zero account	
		if (!$printtitle and $typename!='')
		{
			/*start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_("")."".$type ." - ".$typename, "colspan=3");
			end_row();
			*/
			$acode='';
			if(@$_POST['AccCode']==1)
			$acode=$type.' - ';
			$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
			//$acode.$typename
				$tableheader.='<td colspan="2"> '.$acode.$typename.'</td>';
			if($_POST["tipe"]==0){
				$tableheader.='<td align="center"> '.$blnnya.'</td></tr>';
			}else{
				for($a=$blnfrom;$a<=$blnto;$a++){
					//$tbals[$a]=0;
					$tableheader.= '<td align="right">'.date('M Y',strtotime($thnto.'-'.$a.'-01')).'</td>';
				}
				$tableheader.='</tr>';
			}
			$printtitle = 1;
		}

		// FA doesn't really clear the closed year, therefore the brought forward balance includes all the transactions from the past, even though the balance is null.
		// If we want to remove the balanced part for the past years, this option removes the common part from from the prev and tot figures.
		if (@$SysPrefs->clear_trial_balance_opening)
		{
			$open = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin,  $begin, false, true);
			$offset = min($open['debit'], $open['credit']);
		} else
			$offset = 0;

		if (check_value("NoZero") && !$prev['balance'] && !$curr['balance'] && !$tot['balance'])
			continue;

		$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
			
		//alt_table_row_color($k);

		
		$tableheader.='<td width="100px"> </td>';

		$acode='';
		if(@$_POST['AccCode']==1)
		$acode=$account["account_code2"].' - ';

		$spasi='';
		if($lvl>1){
				$spasi.='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		$tableheader.='<td align="left" width="300px">'.$spasi.$acode.' '.$account["account_name"].'</td>';
		
		if($_POST["tipe"]==0){
			//$prev = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $_POST['TransFromDate'], false, false);
			//$curr = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $_POST['TransFromDate'], $_POST['TransToDate'], false, true);
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $_POST['TransToDate'], false, true);
		
			$tableheader.='<td align="right">'.price_format(($tot['balance']*$kali)).'</td>';
			$tbal += ($tot['balance']*$kali);
		}else{
			for($a=$blnfrom;$a<=$blnto;$a++){
				$bln1=date('d/m/Y',strtotime($thnto.'-'.$a.'-01'));
				$bln2=date('d/m/Y',strtotime($thnto.'-'.$a.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));
				$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $bln1, $bln2, false, true);
				$tableheader.='<td align="right">'.price_format(($tot['balance']*$kali)).'</td>';
				$tbals[$a] = @$tbals[$a]+($tot['balance']*$kali);
			}
		}
		//$tbal += $tot['balance'];

		$tableheader.='</tr>';
		$tl=display_trial_balance_pdf($account["account_code2"], '',3);
		$tableheader.=$tl[0];
		$tbal=$tbal+$tl[1];
		//end_row();
	}

	//Get Account groups/types under this group/type
	$result = get_account_types(false, false, $type);
	while ($accounttype=db_fetch($result))
	{
		if($accounttype["id"]!='1-1699' and $accounttype["id"]!='3-9800'){
			//Print Type Title if has sub types and not previously printed
			if (!$printtitle)
			{
				$acode='';
				if(@$_POST['AccCode']==1)
				$acode=$type.' - ';
				$tableheader.='<tr class="inquirybg" style="font-weight:bold"><td colspan="'.$colom.'"> '.$acode.$typename.'</td></tr>';
				/*start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_("").$type ." - ".$typename, "colspan=3");
				end_row();*/
				$printtitle = 1;

			}
			$dtb=display_trial_balance_pdf($accounttype["id"], $accounttype["name"]);
			$tableheader.=$dtb[0];

			$tableheader.='<tr class="inquirybg" style="font-weight:bold">
				<td colspan="2">&nbsp;&nbsp;Total '.$accounttype["name"].'</td>';
			if($_POST["tipe"]==0){
				$tableheader.='	<td align="right">'.price_format(($dtb[1])).'</td>';
			}else{
				for($a=$blnfrom;$a<=$blnto;$a++){
					$tableheader.='	<td align="right">'.price_format(($dtb[2][$a])).'</td>';
					$tbals[$a]=@$tbals[$a]+$dtb[2][$a];
				}
			}
			$tableheader.='</tr>';
		}
	}

	return array($tableheader,$tbal,$tbals);
}
//----------------------------------------------------------------------------------------------------

function display_bs(){
	if (isset($_POST['TransFromDate']))
	{
		$row = get_current_fiscalyear();
		if (date1_greater_date2($_POST['TransFromDate'], sql2date($row['end'])))
		{
			display_error(_("The from date cannot be bigger than the fiscal year end."));
			set_focus('TransFromDate');
			return;
		}
	}
	if (!isset($_POST['Dimension']))
		$_POST['Dimension'] = 0;
	if (!isset($_POST['Dimension2']))
		$_POST['Dimension2'] = 0;
	//start_table(TABLESTYLE,'',6);

	if($_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=$_POST['bulanfrom'];
		$blnto=$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

	$tableheader='<div id="trans_tbl">';
	$tableheader.='<center><table class="tablestyle" cellpadding="6" border="1" width="90%">';
	echo $tableheader;
	//display_trial_balance();

	$classresult = get_account_classes(false,0);
	while ($class = db_fetch($classresult))
	{

		$typeresult = get_account_types(false, $class['cid'], -1);
		while ($accounttype=db_fetch($typeresult))
		{
			$tl=display_trial_balance($accounttype["id"], $accounttype["name"]);
			$tbal=$tl[0];
		}

		if($_POST["tipe"]==0){
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_(""), "colspan=3");
		}else{			
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_(""), "colspan=".(3+$blnto-$blnfrom));
		}

		start_row("class='inquirybg' style='font-weight:bold'");
		label_cell(_("Total ").$class["class_name"], "colspan=2");

		if($_POST["tipe"]==0){
			amount_cell_pr($tbal);
		}else{
			for($a=$blnfrom;$a<=$blnto;$a++){
				amount_cell_pr(@$tl[1][$a]);
			}
		}
		//amount_cell(($tbal));
		end_row();

		if($class["cid"]==4){
			if($_POST["tipe"]==0){
				$taset=$tbal;				
				start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_(""), "colspan=3");
				end_row();
			}else{
				for($a=$blnfrom;$a<=$blnto;$a++){
					$tasets[$a]=@$tasets[$a]+@$tl[1][$a];
				}				
				start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_(""), "colspan=".(3+$blnto-$blnfrom));
				end_row();
			}
		}
		if($class["cid"]==5){
			if($_POST["tipe"]==0){
				$tliab=$tbal;
				start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_(""), "colspan=3");
			}else{
				for($a=$blnfrom;$a<=$blnto;$a++){
					$tliabs[$a]=@$tliabs[$a]+@$tl[1][$a];
				}
				start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_(""), "colspan=".(3+$blnto-$blnfrom));
			}

			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_("Gross Profit "), "colspan=2");

			if($_POST["tipe"]==0){
				//amount_cell(($taset));
				//amount_cell(($tliab));
				amount_cell(($taset)-($tliab));
				end_row();			
				start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_(""), "colspan=3");
			}else{
				for($a=$blnfrom;$a<=$blnto;$a++){
					amount_cell(($tasets[$a])-($tliabs[$a]));
				}
				end_row();			
				start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_(""), "colspan=".(3+$blnto-$blnfrom));
			}
			end_row();
		}

		if($class["cid"]==6){
			if($_POST["tipe"]==0){
				$tbop=$tbal;				
				start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_(""), "colspan=3");
				end_row();
			}else{
				for($a=$blnfrom;$a<=$blnto;$a++){
					$tbops[$a]=@$tbops[$a]+@$tl[1][$a];
				}				
				start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_(""), "colspan=".(3+$blnto-$blnfrom));
				end_row();
			}

			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_("Operating Profit "), "colspan=2");
			if($_POST["tipe"]==0){
				amount_cell(($taset)-($tliab)-$tbop);
				end_row();			
				start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_(""), "colspan=3");
			}else{
				for($a=$blnfrom;$a<=$blnto;$a++){
					amount_cell(($tasets[$a])-($tliabs[$a])-($tbops[$a]));
				}
				end_row();			
				start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_(""), "colspan=".(3+$blnto-$blnfrom));
			}
		}

		if($class["cid"]==8){
			if($_POST["tipe"]==0){
				$tin=$tbal;				
				//start_row("class='inquirybg' style='font-weight:bold'");
				//label_cell(_(""), "colspan=3");
				//end_row();
			}else{
				for($a=$blnfrom;$a<=$blnto;$a++){
					$tins[$a]=@$tins[$a]+@$tl[1][$a];
				}				
				//start_row("class='inquirybg' style='font-weight:bold'");
				//label_cell(_(""), "colspan=".(3+$blnto-$blnfrom));
				//end_row();
			}
		}
		if($class["cid"]==9){
			if($_POST["tipe"]==0){
				$tout=$tbal;				
				start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_(""), "colspan=3");
				end_row();
			}else{
				for($a=$blnfrom;$a<=$blnto;$a++){
					$touts[$a]=@$touts[$a]+@$tl[1][$a];
				}				
				start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_(""), "colspan=".(3+$blnto-$blnfrom));
				end_row();
			}

			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_("Net Profit Before Tax"), "colspan=2");
			if($_POST["tipe"]==0){
				$toutcome=($taset)-($tliab)-$tbop+$tin-$tout;
				amount_cell($toutcome);
				end_row();			
				start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_(""), "colspan=3");
			}else{
				for($a=$blnfrom;$a<=$blnto;$a++){
					$toutcomes[$a]=($tasets[$a])-($tliabs[$a])-($tbops[$a])+($tins[$a])-($touts[$a]);
					amount_cell($toutcomes[$a]);
				}
				end_row();			
				start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_(""), "colspan=".(3+$blnto-$blnfrom));
			}

			$accounts=get_gl_accounts_in(array('69900','69990'));
				$total=0;$totals=array();
				while ($account = db_fetch($accounts))
				{
					echo $grossprofit='
					<tr class="inquirybg" style="font-weight:bold"><td colspan="2">'.$account["account_name"].'</td>';
					if($_POST["tipe"]==0){
							$from=date('d/m/Y',strtotime($thnto.'-'.$blnfrom.'-01'));
							$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-31'));
						$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], @$from, @$to, true, true);
						$total=$total+(abs(@$tot['balance']));
						echo '<td align="right">'.price_format(abs(@$tot['balance'])).'</td>';
					}else{
						for($a=$blnfrom;$a<=$blnto;$a++){
							$from=date('d/m/Y',strtotime($thnto.'-'.$a.'-01'));
							$to=date('d/m/Y',strtotime($thnto.'-'.$a.'-31'));
							$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], @$from, @$to, true, true);
							$totals[$a]=@$totals[$a]+(abs(@$tot['balance']));
							//$tbals[$a]=0;
							//$totals[$a]=1000;
							echo '<td align="right">'.price_format(abs(@$tot['balance'])).'</td>';
						}
					}
					echo '</tr>';
				}
				echo $grossprofit='
				<tr class="inquirybg" style="font-weight:bold"><td colspan="2">Net Profit After Tax (LOSS)</td>';
				if($_POST["tipe"]==0){
					echo '<td align="right">'.price_format($toutcome-abs(@$total)).'</td>';
				}else{
					for($a=$blnfrom;$a<=$blnto;$a++){
						//$tbals[$a]=0;
						echo '<td align="right">'.price_format(abs(@$toutcomes[$a])-abs(@$totals[$a])).'</td>';
					}
				}
		}
	}

	end_table(1);
	if (($pbal = round2(@$pbal, user_price_dec())) != 0 && $_POST['Dimension'] == 0 && $_POST['Dimension2'] == 0)
		display_warning(_("The Opening Balance is not in balance, probably due to a non closed Previous Fiscalyear."));
	div_end();
}
//----------------------------------------------------------------------------------------------------

function display_bs_pdf(){
if (isset($_POST['TransFromDate']))
{
	$row = get_current_fiscalyear();
	if (date1_greater_date2($_POST['TransFromDate'], sql2date($row['end'])))
	{
		display_error(_("The from date cannot be bigger than the fiscal year end."));
		set_focus('TransFromDate');
		return;
	}
}
//div_start('balance_tbl');
if (!isset($_POST['Dimension']))
	$_POST['Dimension'] = 0;
if (!isset($_POST['Dimension2']))
	$_POST['Dimension2'] = 0;

if($_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=$_POST['bulanfrom'];
		$blnto=$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

	if($_POST["tipe"]==0){
		$colom=3;
	}else{
		$colom=3+($blnto-$blnfrom);
	}
//start_table(TABLESTYLE,'',6);
$tableheader ='<table class="TABLESTYLE" cellpadding="2" border="0">';

//echo $tableheader;

//display_trial_balance();

$classresult = get_account_classes(false,0);
while ($class = db_fetch($classresult))
{
	//$tableheader.='<tr class="inquirybg" style="font-weight:bold"><td colspan="3">'.$class['cid'] ." - ".$class['class_name'].'</td></tr>';
	
	//Get Account groups/types under this group/type with no parents
	$typeresult = get_account_types(false, $class['cid'], -1);
	while ($accounttype=db_fetch($typeresult))
	{
		$dtb=display_trial_balance_pdf($accounttype["id"], $accounttype["name"]);
		$tableheader.=$dtb[0];
		$tbal=$dtb[1];
	}

	$tableheader.='<tr class="inquirybg" style="font-weight:bold">
		<td colspan="2">Total '.$class['class_name'].'</td>';
	$tableheader.='	<td align="right">'.price_format(($tbal)).'</td>';
	$tableheader.='</tr>';
	if($class["cid"]==1)
		$taset=$tbal;
	if($class["cid"]==2){
		$tliab=$tbal;

		$tableheader.='<tr class="inquirybg" style="font-weight:bold">
						<td colspan="2">Net Asset</td>';
		$tableheader.='	<td>'.price_format(($taset)-($tliab)).'</td>';
		$tableheader.='</tr>';
		
	}
}	
	/*
	$tableheader.='<tr class="inquirybg" style="font-weight:bold">
	<td colspan="2">Ending Balance'.$class['cid'] ." - ".$_POST['TransToDate'].'</td>
			<td>'.price_format($tbal).'</td>
		</tr>';
	*/
	$tableheader.='</table>';

	return $tableheader;
}

if (get_post('Export')) 
{
	
	$header='
	<style>
	.inquirybg2{
		font-weight:bold !important;
		font-size:12px !important;
	}
	.tableheader{
		background-color:#EE7600 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0">
		<tr><td align="center">&nbsp;</td></tr>
		<tr><td style="width:2%;">&nbsp;&nbsp;</td>
			<td>
			<table border="0">
				<tr>
					<td width="100%" align="center"  style="background-color:#bfbfbf;color:#ee3000;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:20px;color:#ee3000;font-weight:bold;">Profit & Loss</td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;color:#ee3000;font-weight:bold;">'.@$blnnya.'</td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td>
				</tr>
				<tr><td align="center" style="background-color:#EE7600;">&nbsp;</td>
				</tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 651;">';
	display_bs();
	echo '</td></tr></table>
		</td></tr>
		<tr><td></td><td align="center" style="background-color:#EE7600;border:#000 1px solid;">&nbsp;</td></tr>
		</table>';
}elseif(get_post('PrintPdf')){

// add a page
	$header='<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#8072a9;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		<tr>
			<td align="center" height="20px" style="font-size:18px;background-color:#8072a9;">Profit & Loss</td>
		</tr>
		<tr>
			<td align="center" height="10px" style="background-color:#8072a9;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		
	';
	$header.= '<tr><td style="width: 651px;">';
	$header.=display_bs_pdf();
	$header.= '</td></tr></table>';
$pdf->AddPage();
// output the HTML content
$html=$header;
$html.=display_bs_pdf();
$pdf->writeHTML(@$html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_BS.pdf', 'I');	

}else{
	gl_inquiry_controls();

	div_start('balance_tbl');
	//if (get_post('Show'))
	//display_bs();
	if (get_post('Show')) {
		echo '<center><table border="0" cellpadding="4" width="80%">
		<tr>		
			<td align="center" style="font-weight:bold;">Profit & Loss</td>
		</tr>
		<tr>
			<td align="center" style="font-weight:bold;">Periode '.@$blnnya.'</td>
		</tr>';
		echo '<tr>		
			<td align="center">';
		display_bs();
		echo '</td></tr></table></center>';
	}
	end_page();
}

