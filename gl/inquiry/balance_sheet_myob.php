<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLANALYTIC';
$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/inquiry/netprofit.php");

$js = "";
if (user_use_date_picker())
	$js = get_js_date_picker();

if (get_post('Export')) 
{
	//include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"BS.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
page(_($help_context = "Balance Sheet "), false, true, "", $js);
}


$k = 0;
$pdeb = $pcre = $cdeb = $ccre = $tdeb = $tcre = $pbal = $cbal = $tbal = 0;

//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('balance_tbl');
}

if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$thnto=@$_POST['tahunfrom'];

	$tglto=$thnto=date('d',strtotime(date2sql($_POST['TransToDate'])));
	if($tglto==cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)){
		$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
		if($thnfrom==$thnto){
			if($blnfrom!=$blnto){		
				$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
			}
		}else{
			if($blnfrom!=$blnto){		
				$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
			}
		}
	}else{
		$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-'.$tglto));
	}

	$tglto=$thnto=date('d',strtotime(date2sql($_POST['TransToDate'])));
	if($tglto!=cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto))
	$blnnya=date('d F Y',strtotime($thnfrom.'-'.$blnfrom.'-'.$tglto));
function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER);
echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>

	<script>
	 	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Balance_sheet",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
	$date = today();
	if (!isset($_POST['TransToDate']))
		$_POST['TransToDate'] = end_month($date);
	if (!isset($_POST['TransFromDate']))
		$_POST['TransFromDate'] = add_days(end_month($date), -user_transaction_days());
	echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("Type").":</td>\n";
	echo "<td>";
	$period = array(_("Outstanding"),_("Multiple"));
	//$period = array(_("Monthly"),_("Quarterly"),_("Semi Annyally"),_("Annually"));
	echo array_selector('tipe', null, $period);
	echo "</td>\n";
	echo '<tr>';
    date_cells(_("&nbsp;"), 'TransFromDate');
	date_cells(_("&nbsp;"), 'TransToDate');
	if ($dim >= 1)
		dimensions_list_cells(_("Dimension")." 1:", 'Dimension', null, true, " ", false, 1);
	if ($dim > 1)
		dimensions_list_cells(_("Dimension")." 2:", 'Dimension2', null, true, " ", false, 2);
	//check_cells(_("No zero values"), 'NoZero', null);
	//check_cells(_("Account Code"), 'AccCode', null);
	echo '<td></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td colspan="4"></td>';
	submit_cells('Show',_("Show"),'align="right"','', 'default');
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">
	</td>';
	//<input type="submit" class="export" name="PrintPdf" id="PrintPdf" Value="Print PDF">
	echo '</tr>';


	//submit_cells('Show',_("Show"),'','', 'default');
    end_table();
    end_form();
}

//----------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------

function display_bs(){
	if (isset($_POST['TransFromDate']))
	{
		$row = get_current_fiscalyear();
		if (date1_greater_date2($_POST['TransFromDate'], sql2date($row['end'])))
		{
			display_error(_("The from date cannot be bigger than the fiscal year end."));
			set_focus('TransFromDate');
			return;
		}
	}
	if (!isset($_POST['Dimension']))
		$_POST['Dimension'] = 0;
	if (!isset($_POST['Dimension2']))
		$_POST['Dimension2'] = 0;
	//start_table(TABLESTYLE,'',6);

	if($_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=$_POST['bulanfrom'];
		$blnto=$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}


	$begin=date('d/m/Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	$tglto=date('d',strtotime(date2sql($_POST['TransToDate'])));
	//if($tglto==cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto))
	//$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));
	//else
	$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.$tglto));
	/*
	if($_POST["tipe"]==0){
		$colom=3;
	}else{
		$colom=3+($blnto-$begin);
	}
	*/
	if(get_post('PrintPdf'))
	$tableheader ='<center><table class="tablestyle" width="100%" cellpadding="3">';
	else
	$tableheader ='<center><table class="tablestyle" width="100%" cellpadding="3" border="0">';
	$colspan=3;
	$cols=1;
	if($_POST["tipe"]==0){
		$colspan=4;
		$cols=2;
		$tableheader .= '<tr><td class="tableheader" colspan="4" bgcolor="#dee7ec"><b>&nbsp;</b></td></tr>';
	}else{
		$tableheader .= '<tr><td class="tableheader" colspan="3">&nbsp;</td>';
		for($a=$blnfrom;$a<=$blnto;$a++){
			
			if($blnfrom!=$blnto){
				$tableheader .= '<td class="tableheader" ><b>'.date('M Y',strtotime($thnto.'-'.$a.'-01')).'</td>';

				$colspan++;
				$cols++;
			}
			else{
				for($c=1;$c<=$tglto;$c++){
					$tableheader .= '<td class="tableheader" ><b>'.date('d M Y',strtotime($thnto.'-'.$a.'-'.$c)).'</td>';
					
					$colspan++;
					$cols++;
				}
			}
		}
		$tableheader .= '</tr>';
	}

	$totalaset=0;
	
	$ttlall=0;$ttlall2=0;
	//asset lancar
	//$accounts=get_account_from_to('10000','12000');
	$accounts = get_chart_accounts_search('');

	$notsub=array('16699');
	$totalsub=array('11140','11165','11208','11305','11499','11590'
					,'11699','11706','12102','12212','12222','12232'
					,'12242','12251','12262','12301','12401','12999'
					,'21140','21280','21301','21601','21701','21903'
					,'22101','22901'
					,'39999');
	$totalsub2=array('11706','12102','12262','21701','22101');
	$totalsub3=array('12999','22901');
	$tnetasset='11706';
	$head=array();$headparent=array();$headparent2=array();

	$ttlcol=array();$ttlcol2=array();$ttlcol3=array();$ttlcol4=array();$ttlcolsub=array();
	$ttlalls=array();$ttlalls2=array();
	$ttlclass=0;$ttlclass2=0;$ttlclass3=0;$ttlclass4=0;
	$a=0;$n=0;$hp=0;$hp2=0;

	$header=0;$kali=1;
	while ($account = db_fetch($accounts))
	{
		//print_r($account);

		if(strlen($account["accode"])>5)
			continue;

		//if(intval($account["accode"])>4000)
		//	continue;

		$kls=cek_type_account(@$account["name"]);
		//print_r($kls);
		//header
		if($account["accode"]!='21401' && $account["accode"]!='21510' && $account["accode"]!='21601'){
			if(!in_array(@$kls[4],$headparent2) && @$kls[6]=='' && $account["accode"]<31100){
				$tableheader .= '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';
				$tableheader .= '<tr class="inquirybg2"><td>&nbsp;</td><td><b>&nbsp;'.@$kls[5].'</td><td colspan="'.($cols).'"><b>'.@$account["headparent"].'</td></tr>';
				$headparent2[]=@$kls[4];
				$hp2++;			
				//$tableheader .= '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';
			}

			if(!in_array(@$kls[2],$headparent) && @$kls[3]!=@$account["name"] && $account["accode"]<31100){
				//$tableheader .= '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';
				$tableheader .= '<tr class="inquirybg2"><td>&nbsp;</td><td><b>&nbsp;'.@$kls[3].'</td><td colspan="'.($cols).'"><b>'.@$account["headparent"].'</td></tr>';
				$headparent[]=@$kls[2];
				$hp++;			
				//$tableheader .= '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';
			}else{
				$headparent[]=@$kls[2];
			}

			if(!in_array(@$account["name"],$head) && $header==0 && @$account["name"]!='PIUTANG Pihak Ketiga'){
				$tableheader .= '<tr class="inquirybg2"><td>&nbsp;</td><td><b>&nbsp;'.$account["name"].'</td><td colspan="'.($cols).'"><b>'.@$account["headparent"].'</td></tr>';
				//$headparent[]=$account["headparent"];
				$head[$a]=@$account["name"];
			}
		}
		if($account["accode"]=='21601'){
			$tableheader .= '<tr class="inquirybg2"><td>&nbsp;</td><td><b>&nbsp;'.$account["name"].'</td><td colspan="'.($cols).'"><b>'.@$account["headparent"].'</td></tr>';
		}
		if($account["accode"]=='31100'){
			$tableheader .= '<tr><td colspan="'.$colspan.'">&nbsp;</td></tr>';
			//$tableheader .= '<tr><td colspan="'.$colspan.'">&nbsp;</td></tr>';
		}
		$total=0;
		//body
		//print_r($account);
		$subaccounts=get_subaccount($account["accode"]);
		$sn=0;$tsn=0;
		$ttl2=0;
		while ($subaccount = db_fetch($subaccounts))
		{
			$spasi='&nbsp;&nbsp;&nbsp;&nbsp;';
			//if($subaccount['account_code']>20000)
				//$kali=-1;
			//$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;'.$spasi.$subaccount["account_code"].'</td><td>'.$subaccount["account_name"].'</td>';
			if($_POST["tipe"]==0){
				$total2=0;
				$tot3 = get_balance($subaccount["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
				if($subaccount['account_code']>20000)
					$total2=($tot3['balance'])*$kali;
				else
					$total2=($tot3['balance'])*$kali;
				$tsn+=$total2;
				/*
				if(intval($subaccount["account_code"])<31100)			
				$tableheader.='<td align="right">'.price_format(($total2)).'</td>';
				else
				$tableheader.='<td align="right">'.price_format(($total2)).'</td>';
				*/
				$total+=$total2;
			}else{
				$tglawal=1;
				if($blnfrom!=$blnto)
				$tglawal=$tglto;
				for($b=$blnfrom;$b<=$blnto;$b++){
						
						for($c=$tglawal;$c<=$tglto;$c++){
							$col=$b;
							if($blnfrom==$blnto)
								$col=$c;
							$ttl=0;
							$ttl2=0;
							$bln1=date('d/m/Y',strtotime($thnfrom.'-'.$b.'-'.$c));
							$bln2=date('d/m/Y',strtotime($thnto.'-'.$b.'-'.$c));
							//$prev = get_balance($account["accode"], $_POST['Dimension'], $_POST['Dimension2'], $bln1, $begin, false, true);
							//$curr = get_balance($account["accode"], $_POST['Dimension'], $_POST['Dimension2'], $_POST['TransFromDate'], $bln2, true, true);
							$totsub = get_balance($subaccount["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $bln1, $bln2, false, true);
							if($subaccount['account_code']>20000)
								$ttl2=$ttl2+($totsub['balance']*$kali);
							else
								$ttl2=$ttl2+($totsub['balance']*$kali);
							
							$ttlcolsub[$col]=@$ttlcolsub[$col]+$ttl2;
						}
					//}
					
				}
			}

		//	$tableheader.='</tr>';
			$sn++;
		}
		if($account['accode']>20000)
			$kali=-1;
		$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;'.$account["account_code"].'</td><td>'.$account["account_name"].'</td>';
		if($_POST["tipe"]==0){
			//$tot = get_balance($account["accode"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
			///$total=$total+($tot['debit']-$tot['credit']);
			if($account["accode"]==39000){
				$total=$total+netprof($begin,$to)*$kali;
				//$total=$total-65628329;
			}
			else{
				$tot = get_balance($account["accode"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
				if($account['accode']>20000)
					$total=$total+($tot['balance'])*$kali;
				else
					$total=$total+($tot['balance'])*$kali;
			}
			if($account["accode"]==38000){
				$tot = get_balance('39000', $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
				if($account['accode']>20000)
					$total=$total+($tot['balance'])*$kali;
				else
					$total=$total+($tot['balance']);
				//$total=$total-30237777149.09;
				$total=$total+netprof('01/01/'.($thnfrom-1),'31/12/'.($thnfrom-1))*$kali; 
				//echo netprof('01/01/2017','31/12/2017')*(-1);
			}
			if(intval($account["accode"])<20000)
				$tableheader.='<td align="right">'.price_format(($total)).'</td>';
			else
				$tableheader.='<td align="right">'.price_format(($total)).'</td>';
			
		}else{				
			$tglawal=1;
			if($blnfrom!=$blnto)
			$tglawal=$tglto;

			for($b=$blnfrom;$b<=$blnto;$b++){
				
					for($c=$tglawal;$c<=$tglto;$c++){
					
						$col=$b;
						if($blnfrom==$blnto)
							$col=$c;
						$ttl=0;
						
						$ttl=@$ttlcolsub[$col];
						$bln1=date('d/m/Y',strtotime($thnfrom.'-'.$b.'-01'));
						$bln2=date('d/m/Y',strtotime($thnto.'-'.$b.'-'.$c));
						if($account["accode"]==39000){
							$ttl=$ttl+netprof($begin,$bln2)*$kali;
						}
						else{
							$tot = get_balance($account["accode"], $_POST['Dimension'], $_POST['Dimension2'], $bln1, $bln2, false, true);
							if($account['accode']>=20000 && $account['accode']<30000)
							$ttl=$ttl+($tot['debit']-$tot['credit'])*$kali;
							else
							$ttl=$ttl+($tot['debit']-$tot['credit'])*$kali;
						}

						if($account["accode"]==38000){
							$tot = get_balance('39000', $_POST['Dimension'], $_POST['Dimension2'], $bln1, $bln2, false, true);
							$ttl=$ttl+($tot['debit']-$tot['credit'])*$kali;
							//$ttl=$ttl-30237777149.09;
							$ttl=$ttl+netprof('01/01/'.($thnfrom-1),'31/12/'.($thnfrom-1))*$kali; 
						}
						if(intval($account["accode"])<20000)
							$tableheader.='<td align="right">'.price_format(($ttl)).'</td>';
						else
							$tableheader.='<td align="right">'.price_format(($ttl)).'</td>';
						$ttlcol[$col]=@$ttlcol[$col]+$ttl;
						$ttlcol2[$col]=@$ttlcol2[$col]+$ttl;
						$ttlcol3[$col]=@$ttlcol3[$col]+$ttl;
						$ttlcol4[$col]=@$ttlcol4[$col]+$ttl;
						$ttlalls[$col]=@$ttlalls[$col]+$ttl;
						$ttlcolsub[$col]=0;
					}
				//}
				
			}
		}
		$tableheader.='</tr>';
						
		$ttlall+=$total;
		$ttlclass+=($total);
		$ttlclass2+=$total;
		$ttlclass3+=$total;
		$ttlclass4+=$total;
		//if(@$account["accode"]>=21110)
						
		//$tableheader .= '<tr><td colspan="'.$colspan.'" style="text-align:right;">'.price_format($ttlclass).'</td></tr>';
		//footer
		if(in_array(@$account["accode"],$totalsub )){
			$tableheader .= '<tr class="inquirybg2"><td>&nbsp;</td><td>&nbsp;</td><td><b>Total '.@$account["name"].'</td>';

			if($_POST["tipe"]==0){

				if(intval($account["accode"])<20000)
					$tableheader .= '<td align="right"><b>'.price_format(($ttlclass)).'</td>';
				else
					$tableheader .= '<td align="right"><b>'.price_format(($ttlclass)).'</td>';
			}else{
				for($b=$blnfrom;$b<=$blnto;$b++){
					/*
					if($tglto==cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)){
						if(intval($account["accode"])<31100)
							$tableheader .= '<td align="right"><b>'.price_format(($ttlcol[$b])).'</td>';
						else
							$tableheader .= '<td align="right"><b>'.price_format(($ttlcol[$b])).'</td>';
					}else{*/
					$tglawal=1;
					if($blnfrom!=$blnto)
					$tglawal=$tglto;
					for($c=$tglawal;$c<=$tglto;$c++){
						$col=$b;
						if($blnfrom==$blnto)
							$col=$c;
							if(intval($account["accode"])<20000)
								$tableheader .= '<td align="right"><b>'.price_format(($ttlcol[$col])).'</td>';
							else
								$tableheader .= '<td align="right"><b>'.price_format(($ttlcol[$col])).'</td>';
						}
					//}
				}
				$tableheader .= '</tr>';
			}
			$tableheader .= '</tr>';
			//$tableheader .= '<tr><td colspan="'.$colspan.'" style="text-align:right;">'.price_format($ttlclass2).'</td></tr>';
			//$tableheader .= '<tr><td colspan="'.$colspan.'">&nbsp;</td></tr>';
			$ttlclass=0;
			$ttlcol=array();
			$header=0;
		}else{			
			$header=1;
		}
		
		if(in_array(@$account["accode"],$totalsub2 )){
			if(@$account["accode"]=='22101')
				$ttlclass2=0;
			$tableheader .= '<tr class="inquirybg2"><td>&nbsp;</td><td>&nbsp;</td><td><b>Total '.@$kls[3].'</td>';

			if($_POST["tipe"]==0){
				if(intval($account["accode"])<20000)
				$tableheader .= '<td align="right"><b>'.price_format(($ttlclass2)).'</td>';
				else
				$tableheader .= '<td align="right"><b>'.price_format(($ttlclass2)).'</td>';
			}else{	
					$tglawal=1;
					if($blnfrom!=$blnto)
					$tglawal=$tglto;
				for($b=$blnfrom;$b<=$blnto;$b++){
					/*if($tglto==cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)){
						//if(@$account["accode"]=='22101')
						//	$ttlcol2[$b]=0;
						if(intval($account["accode"])<31100)
							$tableheader .= '<td align="right"><b>'.price_format(($ttlcol2[$b])).'</td>';
						else
							$tableheader .= '<td align="right"><b>'.price_format(($ttlcol2[$b])).'</td>';
					}else{*/			
					for($c=$tglawal;$c<=$tglto;$c++){
						$col=$b;
						if($blnfrom==$blnto)
							$col=$c;
							if(intval($account["accode"])<20000)
								$tableheader .= '<td align="right"><b>'.price_format(($ttlcol2[$col])).'</td>';
							else
								$tableheader .= '<td align="right"><b>'.price_format(($ttlcol2[$col])).'</td>';
						}
					//}
				}
				$tableheader .= '</tr>';
			}
			$tableheader .= '</tr>';
			//$tableheader .= '<tr><td colspan="'.$colspan.'" style="text-align:right;">'.price_format($ttlclass3).'</td></tr>';
		//$tableheader .= '<tr><td colspan="'.$colspan.'">&nbsp;</td></tr>';
			$ttlclass=0;
			$ttlclass2=0;
			$ttlcol=array();
			$ttlcol2=array();
			$header=0;
		}
		
		if(in_array(@$account["accode"],$totalsub3)){
			$tableheader .= '<tr class="inquirybg2"><td>&nbsp;</td><td>&nbsp;</td><td><b>Total '.@$kls[5].'</td>';

			if($_POST["tipe"]==0){

				if(intval($account["accode"])<20000)
				$tableheader .= '<td align="right"><b>'.price_format(($ttlclass3)).'</td>';
				else
				$tableheader .= '<td align="right"><b>'.price_format(($ttlclass3)).'</td>';
			}else{
					$tglawal=1;
					if($blnfrom!=$blnto)
					$tglawal=$tglto;
				for($b=$blnfrom;$b<=$blnto;$b++){
					
					for($c=$tglawal;$c<=$tglto;$c++){
						$col=$b;
						if($blnfrom==$blnto)
							$col=$c;
							if(intval($account["accode"])<20000)
								$tableheader .= '<td align="right"><b>'.price_format(($ttlcol3[$col])).'</td>';
							else
								$tableheader .= '<td align="right"><b>'.price_format(($ttlcol3[$col])).'</td>';
						}
						
				}
				$tableheader .= '</tr>';
			}
			$tableheader .= '</tr>';
			//$tableheader .= '<tr><td colspan="'.$colspan.'">&nbsp;</td></tr>';
			$ttlclass=0;
			$ttlclass2=0;
			$ttlclass3=0;
			$ttlcol=array();
			$ttlcol2=array();
			$ttlcol3=array();
			//$ttlasets=array();
			if($account["accode"]=='12999'){
				if($_POST["tipe"]==0){
					$ttlaset=$ttlclass4;
				}else{
					$tglawal=1;
					if($blnfrom!=$blnto)
					$tglawal=$tglto;
					for($b=$blnfrom;$b<=$blnto;$b++){	
						for($c=$tglawal;$c<=$tglto;$c++){
							$col=$b;
							if($blnfrom==$blnto)
								$col=$c;
							$ttlasets[$col]=@$ttlcol4[$col];
						}
					}
				}
			}
			$header=0;
			if(@$account["accode"]=='22901'){
				//$tableheader .= '<tr><td colspan="'.$colspan.'" style="text-align:right;">'.price_format(@$ttlaset).'</td></tr>';
				//$tableheader .= '<tr><td colspan="'.$colspan.'" style="text-align:right;">'.price_format(@$ttlaset+($ttlaset-@$ttlclass4)).'</td></tr>';
				$tableheader .= '<tr><td colspan="'.$colspan.'">&nbsp;</td></tr>';
				$tableheader .= '<tr class="inquirybg2"><td>&nbsp;</td><td>&nbsp;</td><td><b>Net Assets</td>';
				if($_POST["tipe"]==0){
					$ttlnetaset=@$ttlaset+($ttlaset-@$ttlclass4);
					if(intval($account["accode"])<20000)
					$tableheader .= '<td align="right"><b>'.price_format(($ttlnetaset)).'</td>';
					else
					$tableheader .= '<td align="right"><b>'.price_format(($ttlnetaset)).'</td>';
				}else{
					$tglawal=1;
					if($blnfrom!=$blnto)
					$tglawal=$tglto;
					for($b=$blnfrom;$b<=$blnto;$b++){	
					
					for($c=$tglawal;$c<=$tglto;$c++){
						$col=$b;
						if($blnfrom==$blnto)
							$col=$c;
							$ttlnetasets[$col]=@$ttlasets[$col]+(@$ttlasets[$col]-@$ttlcol4[$col]);
							if(intval($account["accode"])<20000)
								$tableheader .= '<td align="right"><b>'.price_format((@$ttlnetasets[$col])).'</td>';
							else
								$tableheader .= '<td align="right"><b>'.price_format((@$ttlnetasets[$col])).'</td>';
							
							
								//$tableheader .= '<td align="right"><b>'.price_format(($ttlasets[$c])).'</td>';
						}
					}
					$tableheader .= '</tr>';
				}
				$ttlclass4=0;
				$ttlcol=array();
				$ttlcol2=array();
				$ttlcol3=array();
				$ttlcol4=array();
			}

		}
		
		$n++;
		if(($account["accode"])=='39999')
				break;
	}	

	$tableheader .= '</tr>';
	$tableheader .= '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';


	$tableheader .='</table></center>';
	return $tableheader;
}
//----------------------------------------------------------------------------------------------------

if (get_post('Export')) 
{
	
	$header='
	<style>
	.inquirybg2{
		font-weight:bold !important;
		font-size:12px !important;
	}
	.tableheader{
		background-color:#EE7600 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0">
		<tr><td align="center">&nbsp;</td></tr>
		<tr><td style="width:2%;">&nbsp;&nbsp;</td>
			<td>
			<table border="0">
				<tr>
					<td width="100%" align="center"  style="background-color:#bfbfbf;color:#ee3000;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:20px;color:#ee3000;font-weight:bold;">Balance Sheet</td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;color:#ee3000;font-weight:bold;">'.@$blnnya.'</td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td>
				</tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 600px;">';
	div_start('balance_tbl');
	echo display_bs();
	div_end();
	echo '</td></tr></table>
		</td></tr>
		<tr><td></td><td align="center" style="background-color:#EE7600;border:#000 1px solid;">&nbsp;</td></tr>
		</table>';
}elseif(get_post('PrintPdf')){

// add a page
	$header='<table border="0">
		<tr>
			<td width="80%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">Balance Sheet</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	$header.= '<tr><td style="width: 648px;">';
	$header.=display_bs();
	$header.= '</td></tr></table>';
$pdf->AddPage();
// output the HTML content
$html=$header;
//$html.=display_bs();
$pdf->writeHTML(@$html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_BS.pdf', 'I');	

}else{
	gl_inquiry_controls();
	div_start('balance_tbl');

	if (get_post('Show')) {
		echo '<center><table border="0" cellpadding="4" width="80%">
		<tr>		
			<td align="center" style="font-weight:bold;">Balance Sheet</td>
		</tr>
		<tr>
			<td align="center" style="font-weight:bold;">Periode '.@$blnnya.'</td>
		</tr>';
		echo '<tr>		
			<td align="center">';
		echo display_bs();
		echo '</td></tr></table></center>';
	}
	//if ($_POST['Show']) 
	//echo display_bs();
	div_end();
	end_page();
}

