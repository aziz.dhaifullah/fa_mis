<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/config_mssql.php");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/siap.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();


if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format(0);

if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"PPH 23 Broker.xls\"");
	header("Cache-Control: max-age=0");
	
	
}else{
	page(_($help_context = "PPH 23 Broker"), false, false, '', $js);
}


//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}

//----------------------------------------------------------------------------------------------------

function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER);
	start_row();
   // gl_all_accounts_list_cells(_("Account:"), 'account', null, false, false, _("All Accounts"));
	date_cells(_("Date:"), 'TransFromDate', '', null, -user_transaction_days());
	//date_cells(_("End Date:"), 'TransToDate');
/*
	echo "<td>"._("Product").":</td>\n";
	echo "<td>";
	
$product = array(
	_("All")
);
	echo array_selector('Product', null, $product);
	echo "</td>\n";
	*/
    end_row();
	end_table();

	start_table(TABLESTYLE_NOBORDER);
	start_row();

	echo '<td colspan="4">&nbsp;</td>';
	submit_cells('Show',_("Show"),'align="right"','', 'default');
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel"></td>';
	echo '</tr>';
	echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
		function xls(){
			//console.log("aa");
			$(".tablestyle2").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "PPH23_Broker_fee",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';

	//submit_cells('Export Excel',_("Export"),'','', 'default');
	end_row();
	end_table();

	echo '<hr>';
    end_form();
}

//----------------------------------------------------------------------------------------------------

function show_results()
{
	global $path_to_root, $systypes_array;

	

	$colspan = ($dim == 2 ? "6" : ($dim == 1 ? "5" : "4"));

	if ($_POST["account"] != null)
		display_heading($_POST["account"]. "&nbsp;&nbsp;&nbsp;".$act_name);

	start_table(TABLESTYLE2, "width='90%' border='1'", 10);
	
	$th = array(_("No"), _("Fund ID"));
	$th2 = array(_("Broker Code"),_("Broker Name"),_("DPP"),_("PPH 23"));
	$th=array_merge($th,$th2);
	//table_header($th);
	$bfw = 0;

	$running_total = $bfw;
	$j = 1;
	$k = 0; //row colour counter
	$no=1;
	$tgl=explode('-',date2sql($_POST['TransFromDate']));
	$brokers=get_pph_siap($tgl[0],$tgl[1]);
	//print_r($brokers);

	//echo '<tr><td colspan="7">No Data Available</td></tr>';
	
	//print_r($prods[0]);
	$produks=array();$brokerdatas=array();
	foreach($brokers as $broker=>$line){
		//print_r($line[0]);
		if($line[0]<>0 and $line[1]<>0){
			if(!in_array($line[3],$produks)){
				$produks[]=$line[3];
			}
			$brokerdatas[$line[3]][]=array($line[2],$line[4],$line[5],$line[0],$line[1]);
		}
		/*
		echo '<tr>';
    	label_cell($no);
		label_cell($line[2]);
		label_cell($line[3]);
		label_cell($line[4]);
		label_cell($line[5]);
		label_cell(price_format($line[0]));
		label_cell(price_format($line[1]));
		$no++;
		*/
	}
	for($a=0;$a<count($produks);$a++){
		//print_r($line[0]);
		$produk=$produks[$a];
		//echo $produk;
		//print_r($produks);
		//echo '<br>';
		echo '<tr><td colspan="6" class="tableheader"><b>'.$produk.'</b></td></tr>';
		$no=1;
		table_header($th);$tdp=0;$tph=0;
		for($b=0;$b<count($brokerdatas[$produk]);$b++){
			echo '<tr>';
	    	label_cell($no);
			label_cell($brokerdatas[$produk][$b][0]);
			label_cell($brokerdatas[$produk][$b][1]);
			label_cell($brokerdatas[$produk][$b][2]);
			label_cell(price_format($brokerdatas[$produk][$b][3]));
			label_cell(price_format($brokerdatas[$produk][$b][4]));
			echo '</tr>';
			$tdp+=$brokerdatas[$produk][$b][3];
			$tph+=$brokerdatas[$produk][$b][4];
			$no++;
		}		
		//echo '<tr><td colspan="6">&nbsp;</td></tr>';
		echo '<tr><td colspan="4" class="tableheader"><b>Total '.$produk.'</b></td><td><b>'.price_format($tdp).'</b></td><td><b>'.price_format($tph).'</b></td></tr>';
		echo '<tr><td colspan="6">&nbsp;</td></tr>';
	}
	/*
	if($no==1)
		echo '<tr><td colspan="7">No Data Available</td></tr>';
	*/
}
//----------------------------------------------------------------------------------------------------

if (get_post('Export')) 
{
	
	
	$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
	$blnto=date('m',strtotime(date2sql($_POST['TransFromDate'])));
	$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
	$thnto=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
	$blnnya=date('M Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));

	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('M',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('M Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
	$header='
	<style>
	.tableheader{
		background-color:#000 !important;
		color:#fff !important;
	}
	.tablestyle{
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0" style="border:#000 1px solid;">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">PPH23 Broker fee</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 651pt;">';
	show_results();
	echo '</td></tr></table>';
}else{
	gl_inquiry_controls();

	div_start('trans_tbl');

	if (get_post('Show') || get_post('account'))
	    show_results();

	div_end();

	//----------------------------------------------------------------------------------------------------

	end_page();

}
