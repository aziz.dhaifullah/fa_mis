<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

$page_security = 'SA_JOURNALPOSTING';
$path_to_root="../..";

include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/includes/db/siapjournal.php");
$js = "";
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

journal_siap_mis(null, $_SESSION['wa_current_user']->com_id);

// page(_($help_context = "Journal Inquiry"), false, false, "", $js);

//-----------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show'))
{
	$Ajax->activate('journal_tbl');
}

if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"Journal.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
	page(_($help_context = "Journal Inquiry Posting"), false, false, '', $js);
}


//posting

if (@$_GET['j_id'])
{
	$ids=explode('_',@$_GET['j_id']);
	//print_r($ids);
	posting_journal($ids[0],$ids[1],$ids[2]);
}
//--------------------------------------------------------------------------------------
if (!isset($_POST['filterType']))
	$_POST['filterType'] = -1;

start_form();

start_table(TABLESTYLE_NOBORDER);
start_row();

ref_cells(_("Reference:"), 'Ref', '',null, _('Enter reference fragment or leave empty'));

journal_types_list_cells(_("Type:"), "filterType");
date_cells(_("From:"), 'FromDate', '', null, 0, -1, 0);
date_cells(_("To:"), 'ToDate');

end_row();
start_row();
ref_cells(_("Memo:"), 'Memo', '',null, _('Enter memo fragment or leave empty'));
users_list_cells(_("User:"), 'userid', null, false);
if (get_company_pref('use_dimension') && isset($_POST['dimension'])) // display dimension only, when started in dimension mode
	dimensions_list_cells(_('Dimension:'), 'dimension', null, true, null, true);
check_cells( _("Show closed:"), 'AlsoClosed', null);
submit_cells('Search', _("Search"), '', '', 'default');
	// echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">&nbsp;';
	// echo '<input type="submit" class="export" name="PrintPdf" id="PrintPdf" Value="Print PDF"></td>';
	// echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	
	echo '
	<style>
		.export{
			align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
		function xls(){
			//console.log("aa");
			$(".tablestyle").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Journal",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
end_row();
end_table();

function journal_pos($row)
{
	return $row['gl_seq'] ? $row['gl_seq'] : '-';
}

function systype_name($dummy, $type)
{
	global $systypes_array;
	
	return $systypes_array[$type];
}

function view_link($row) 
{
	return '';
	//return get_trans_view_str($row["trans_type"], $row["trans_no"]);
}

function gl_link($row) 
{
	if($row["trans_type"]==ST_SALESINVOICE){
		$voucher='<a target="_blank" href="../../reporting/rep107.php?PARAM_0='.$row['trans_no'].'&amp;type='.ST_SALESINVOICE.'" accesskey="P">
			<img src="../../themes/default/images/print.png" style="vertical-align:middle;width:12px;height:12px;border:0;" title="Print">
		</a>';
	}else{
		$voucher='<a target="_blank" href="../../reporting/rep1701.php?type_id='.$row["trans_type"].'&trans_no='.$row["trans_no"].'"><img src="../../themes/default/images/print.png" style="vertical-align:middle;width:12px;height:12px;border:0;" title="Print Remittance">
		</a>';
		}
	return get_gl_view_str($row["trans_type"], $row["trans_no"]).' '.$voucher;
}

function edit_link($row)
{

	//print_r($row);
	$ok = true;
	if ($row['trans_type'] == ST_SALESINVOICE)
	{
		$myrow = get_customer_trans($row["trans_no"], $row["trans_type"]);
		if ($myrow['alloc'] != 0 || get_voided_entry(ST_SALESINVOICE, $row["trans_no"]) !== false)
			$ok = false;
	}
	$link=trans_editor_link( $row["trans_type"], $row["trans_no"]);
	/*$refss = strpos($row['reference'], 'Bal');
	print_r($refss);*/
	if($row['postinggl']==0){
		$link=trans_editor_link( $row["trans_type"], $row["trans_no"]).' || <a href="?j_id='.@$row["trans_type"].'_'.@$row["trans_no"].'_'.$row['postinggl'].'">Posting</a>';
	}else{
		$link='<a href="?j_id='.@$row["trans_type"].'_'.@$row["trans_no"].'_'.$row['postinggl'].'">Unposting</a>';
	}

	return $ok ? $link : '';
}

function invoice_supp_reference($row)
{
	return $row['supp_reference'];
}

function amounted($row)
{
	if($row["trans_type"]==0){
		$amnt=get_total_trans($row["trans_type"], $row["trans_no"]);
		return @$amnt;
	}
	else{
		return $row["amount"];
	}

}

function show_results(){

	$sql = get_sql_for_journal_inquiry(get_post('filterType', -1), get_post('FromDate'),
		get_post('ToDate'), get_post('Ref'), get_post('Memo'), check_value('AlsoClosed'));

	$cols = array(
		_("#") => array('fun'=>'journal_pos', 'align'=>'center'), 
		_("Date") =>array('name'=>'tran_date','type'=>'date','ord'=>'desc'),
		_("Type") => array('fun'=>'systype_name'), 
	 	//_("Counterparty") => array('ord' => ''),
		_("Supplier's Reference") => 'skip',
		_("Reference"), 
		_("Amount") => array('fun'=>'amounted','type'=>'amount'),
		_("Memo"),
		_("User"),
		_("View") => array('insert'=>true, 'fun'=>'gl_link'),
		array('insert'=>true, 'fun'=>'edit_link')
	);

	if (!check_value('AlsoClosed')) {
		$cols[_("#")] = 'skip';
	}

	if($_POST['filterType'] == ST_SUPPINVOICE) //add the payment column if shown supplier invoices only
	{
		$cols[_("Supplier's Reference")] = array('fun'=>'invoice_supp_reference', 'align'=>'center');
	}


	$table =& new_db_pager('journal_tbl', $sql, $cols);

	$table->width = "80%";

	display_db_pager($table);

}


//----------------------------------------------------------------------------------------------------


if (get_post('Export')) 
{
	
	$blnfrom=$_POST["bulanfrom"]+1;
	$blnto=$_POST["bulanto"]+1;
	$blnnya=date('M Y',strtotime($thn.'-'.$blnfrom.'-01'));
	if($blnfrom!=$blnto){		
		$blnnya=date('M',strtotime($thn.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thn.'-'.$blnto.'-01'));
	}
	$header='<table border="0" style="border:#000 1px solid;">
		<tr>
			<td width="100%" align="center"  style="background-color:#8072a9;">PT. Kresna Asset Management</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;">-</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;">Jl. Widya Chandra V</td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:20px;">Journal</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 409pt;">';
	show_results();
	echo '</td></tr></table>';
}elseif(get_post('PrintPdf')){

// add a page
	$blnfrom=$_POST["bulanfrom"]+1;
	$blnto=$_POST["bulanto"]+1;
	$blnnya=date('M Y',strtotime($thn.'-'.$blnfrom.'-01'));
	if($blnfrom!=$blnto){		
		$blnnya=date('M',strtotime($thn.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thn.'-'.$blnto.'-01'));
	}
	$header='<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#8072a9;">PT. Kresna Asset Management</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;">-</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;">Jl. Widya Chandra V</td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:20px;">Journal</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		
	';
	$header.= '<tr><td style="width: 648px;">';
	$header.=show_results();
	$header.= '</td></tr></table>';
	$pdf->AddPage();
	// output the HTML content
	$html=$header;
	//$html.=display_bs_pdf();
	$pdf->writeHTML(@$html, true, 0, true, 0);

	// reset pointer to the last page
	$pdf->lastPage();

	// ---------------------------------------------------------

	//Close and output PDF document
	$pdf->Output('print_Journal.pdf', 'I');	

}else{
	
	// gl_inquiry_controls();

	// div_start('trans_tbl');

	show_results();

	div_end();

	end_page();

}
