<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLANALYTIC';
$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

$js = "";
if (user_use_date_picker())
	$js = get_js_date_picker();

if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"asset&liab.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
page(_($help_context = "Detail Others Asset & Liab"), false, false, "", $js);
}


$k = 0;
$pdeb = $pcre = $cdeb = $ccre = $tdeb = $tcre = $pbal = $cbal = $tbal = 0;

//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('balance_tbl');
}


function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    //start_table(TABLESTYLE_NOBORDER);

    echo '<table style="padding: 10px;">';
	$date = today();
	if (!isset($_POST['TransToDate']))
		$_POST['TransToDate'] = end_month($date);
	if (!isset($_POST['TransFromDate']))
		$_POST['TransFromDate'] = add_days(end_month($date), -user_transaction_days());
	echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo '<tr>';
    date_cells(_("From:"), 'TransFromDate');
	date_cells(_("To:"), 'TransToDate');
	//check_cells(_("No zero values"), 'NoZero', null);
	//check_cells(_("Only balances"), 'Balance', null);
	echo '</tr>';
	echo '<tr>';
	echo '<td>&nbsp;</td>';
	submit_cells('Show',_("Show"),'align="right"','', 'default');
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	echo '<td colspan="3"><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">';
	//echo '&nbsp;<input type="submit" class="export" name="PrintPdf" id="PrintPdf" Value="Print PDF"></td>';
	echo '</tr>';
	echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>

	<script>
	 	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Balance_sheet",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
	//submit_cells('Show',_("Show"),'','', 'default');
    end_table();
    end_form();
}

//----------------------------------------------------------------------------------------------------

function display_bs(){
	if (isset($_POST['TransFromDate']))
	{
		$row = get_current_fiscalyear();
		if (date1_greater_date2($_POST['TransFromDate'], sql2date($row['end'])))
		{
			display_error(_("The from date cannot be bigger than the fiscal year end."));
			set_focus('TransFromDate');
			return;
		}
	}
	if (!isset($_POST['Dimension']))
		$_POST['Dimension'] = 0;
	if (!isset($_POST['Dimension2']))
		$_POST['Dimension2'] = 0;
	start_table(TABLESTYLE,'',6);

		$tglfrom=explode('/',$_POST['TransFromDate']);
		$tglto=explode('/',$_POST['TransToDate']);
	$cols=0;
	$header='<tr><td class="tableheader">Account</td>';
	for($a=1;$a<=$tglto[0];$a++){
		$header.='<td class="tableheader">'.date('d M Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a)).'</td>';
		$cols++;
	}
	$header.='</tr>';
	//echo $tableheader;
	$tableheader= $header;

	//kas
	$accounts=get_gl_accounts_in(array('11111','11112','11113','11114','11140'));
	$total2=array();
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('d/m/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			//$end=date('d/m/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end = add_days($begin, 1);
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['balance']);
			$tableheader .= '	<td align="right">'.price_format_pr(((@$tot['debit']-@$tot['credit']))).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$totalaset=@$total+@$totalaset;
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';

	//bank	
	$header='<tr><td class="tableheader" colspan="'.($cols+1).'"></td></tr>';
	$accounts=get_gl_accounts_in(array('11152','11154','11155','11156','11157','11158','11159','11160','11161','11163','11164','11165'));
	$total2=array();
	$tableheader.=$header;
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('m/d/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end=$begin;
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['debit']-@$tot['credit']);
			$tableheader .= '	<td align="right">'.price_format_pr((@$tot['debit']-@$tot['credit'])).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';


	//deposito	
	$header='<tr><td class="tableheader" colspan="'.($cols+1).'"></td></tr>';
	$accounts=get_gl_accounts_in(array('11201','11202','11207','11205','11206','11207','11208'));
	$total2=array();
	$tableheader.=$header;
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('m/d/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end=$begin;
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['debit']-@$tot['credit']);
			$tableheader .= '	<td align="right">'.price_format_pr(((@$tot['debit']-@$tot['credit']))).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$totalaset=@$total+@$totalaset;
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';


	//piutang	
	$header='<tr><td class="tableheader" colspan="'.($cols+1).'"></td></tr>';
	$accounts=get_gl_accounts_in(array('11401','11499'));
	$total2=array();
	$tableheader.=$header;
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('m/d/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end=$begin;
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['debit']-@$tot['credit']);
			$tableheader .= '	<td align="right">'.price_format_pr(((@$tot['debit']-@$tot['credit']))).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$totalaset=@$total+@$totalaset;
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';


	//11304	portofolio efek
	$header='<tr><td class="tableheader" colspan="'.($cols+1).'"></td></tr>';
	$accounts=get_gl_accounts_in(array('11304'));
	$total2=array();
	$tableheader.=$header;
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('m/d/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end=$begin;
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['debit']-@$tot['credit']);
			$tableheader .= '	<td align="right">'.price_format_pr(((@$tot['debit']-@$tot['credit']))).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$totalaset=@$total+@$totalaset;
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';

	//11305	obligasi
	$header='<tr><td class="tableheader" colspan="'.($cols+1).'"></td></tr>';
	$accounts=get_gl_accounts_in(array('11305'));
	$total2=array();
	$tableheader.=$header;
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('m/d/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end=$begin;
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['debit']-@$tot['credit']);
			$tableheader .= '	<td align="right">'.price_format_pr(((@$tot['debit']-@$tot['credit']))).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$totalaset=@$total+@$totalaset;
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';

	//11303	penyertaan reksa dana
	$header='<tr><td class="tableheader" colspan="'.($cols+1).'"></td></tr>';
	$accounts=get_gl_accounts_in(array('11303'));
	$total2=array();
	$tableheader.=$header;
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('m/d/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end=$begin;
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['debit']-@$tot['credit']);
			$tableheader .= '	<td align="right">'.price_format_pr(((@$tot['debit']-@$tot['credit']))).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$totalaset=@$total+@$totalaset;
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';

	//11301	Rek Efek pd Anggota Kliring
	$header='<tr><td class="tableheader" colspan="'.($cols+1).'"></td></tr>';
	$accounts=get_gl_accounts_in(array('11301'));
	$total2=array();
	$tableheader.=$header;
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('m/d/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end=$begin;
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['debit']-@$tot['credit']);
			$tableheader .= '	<td align="right">'.price_format_pr(((@$tot['debit']-@$tot['credit']))).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$totalaset=@$total+@$totalaset;
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';

	//Other Asset
	//Other Asset 11580,11581,11582,11583,11584,11590,1-1540
	$header='<tr><td class="tableheader" colspan="'.($cols+1).'"></td></tr>';
	$accounts=get_gl_accounts_in(array('11580','11581','11582','11583','11584','11590','11540'));
	$total2=array();
	$tableheader.=$header;
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('m/d/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end=$begin;
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['debit']-@$tot['credit']);
			$tableheader .= '	<td align="right">'.price_format_pr(((@$tot['debit']-@$tot['credit']))).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$totalaset=@$total+@$totalaset;
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';

	//Other Asset 11701,11702,11703,11704,11705,11706

	$header='<tr><td class="tableheader" colspan="'.($cols+1).'"></td></tr>';
	$accounts=get_gl_accounts_in(array('11701','11702','11703','11704','11705','11706'));
	$total2=array();
	$tableheader.=$header;
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('m/d/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end=$begin;
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['debit']-@$tot['credit']);
			$tableheader .= '	<td align="right">'.price_format_pr(((@$tot['debit']-@$tot['credit']))).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$totalaset=@$total+@$totalaset;
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';

	//11601,11602,11603,11604,11605,11606,11607,11608,11609,11610,11611,11612,11613,11614,11615,11699
	$header='<tr><td class="tableheader" colspan="'.($cols+1).'"></td></tr>';
	$accounts=get_gl_accounts_in(array('11601','11602','11603','11604','11605','11606','11607','11608','11609','11610','11611','11612','11613','11614','11615','11699'));
	$total2=array();
	$tableheader.=$header;
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('m/d/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end=$begin;
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['debit']-@$tot['credit']);
			$tableheader .= '	<td align="right">'.price_format_pr(((@$tot['debit']-@$tot['credit']))).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$totalaset=@$total+@$totalaset;
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';

	//12901
	$header='<tr><td class="tableheader" colspan="'.($cols+1).'"></td></tr>';
	$accounts=get_gl_accounts_in(array('12901'));
	$total2=array();
	$tableheader.=$header;
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('m/d/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end=$begin;
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['debit']-@$tot['credit']);
			$tableheader .= '	<td align="right">'.price_format_pr(((@$tot['debit']-@$tot['credit']))).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$totalaset=@$total+@$totalaset;
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';

	//2-1601,2-1701
	$header='<tr><td class="tableheader" colspan="'.($cols+1).'"></td></tr>';
	$accounts=get_gl_accounts_in(array('21601','21701'));
	$total2=array();
	$tableheader.=$header;
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('m/d/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end=$begin;
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['debit']-@$tot['credit']);
			$tableheader .= '	<td align="right">'.price_format_pr(((@$tot['debit']-@$tot['credit']))).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$totalaset=@$total+@$totalaset;
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';

	//3-1100
	$header='<tr><td class="tableheader" colspan="'.($cols+1).'"></td></tr>';
	$accounts=get_gl_accounts_in(array('31100'));
	$total2=array();
	$tableheader.=$header;
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('m/d/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end=$begin;
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['debit']-@$tot['credit']);
			$tableheader .= '	<td align="right">'.price_format_pr(((@$tot['debit']-@$tot['credit']))).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$totalaset=@$total+@$totalaset;
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';

	//3-1100
	$header='<tr><td class="tableheader" colspan="'.($cols+1).'"></td></tr>';
	$accounts=get_gl_accounts_in(array('39800'));
	$total2=array();
	$tableheader.=$header;
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('m/d/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end=$begin;
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['debit']-@$tot['credit']);
			$tableheader .= '	<td align="right">'.price_format_pr(((@$tot['debit']-@$tot['credit']))).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$totalaset=@$total+@$totalaset;
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';


	//38000,39000
	$header='<tr><td class="tableheader" colspan="'.($cols+1).'"></td></tr>';
	$accounts=get_gl_accounts_in(array('38000','39000'));
	$total2=array();
	$tableheader.=$header;
	while ($account = db_fetch($accounts))
	{	
		$tableheader .= '<tr><td>'.$account["account_name"].'</td>';
		for($a=0;$a<$tglto[0];$a++){
			$begin=date('m/d/Y',strtotime($tglfrom[2].'-'.$tglfrom[1].'-'.$a));
			$end=$begin;
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $end, false, true);
			$total2[$a]=@$total2[$a]+(@$tot['debit']-@$tot['credit']);
			$tableheader .= '	<td align="right">'.price_format_pr(((@$tot['debit']-@$tot['credit']))).'</td>';
		}
		$tableheader .= '</tr>';
	}
	$totalaset=@$total+@$totalaset;
	$tableheader .= '<tr><td>Total</td>';
	for($a=0;$a<$tglto[0];$a++){
		$tableheader .= '<td align="right">'.price_format_pr(($total2[$a])).'</td>';
	}
	$tableheader .= '</tr>';

	return $tableheader;
}
//----------------------------------------------------------------------------------------------------

function display_bs_pdf(){
if (isset($_POST['TransFromDate']))
{
	$row = get_current_fiscalyear();
	if (date1_greater_date2($_POST['TransFromDate'], sql2date($row['end'])))
	{
		display_error(_("The from date cannot be bigger than the fiscal year end."));
		set_focus('TransFromDate');
		return;
	}
}
//div_start('balance_tbl');
if (!isset($_POST['Dimension']))
	$_POST['Dimension'] = 0;
if (!isset($_POST['Dimension2']))
	$_POST['Dimension2'] = 0;
//start_table(TABLESTYLE,'',6);
$tableheader ='<table class="TABLESTYLE" cellspacing="6" border="0">';

//echo $tableheader;

//display_trial_balance();

$classresult = get_account_classes(false,1);
while ($class = db_fetch($classresult))
{
	$tableheader.='<tr class="inquirybg" style="font-weight:bold"><td colspan="3">'.$class['cid'] ." - ".$class['class_name'].'</td></tr>';
	
	//Get Account groups/types under this group/type with no parents
	$typeresult = get_account_types(false, $class['cid'], -1);
	while ($accounttype=db_fetch($typeresult))
	{
		$dtb=display_trial_balance_pdf($accounttype["id"], $accounttype["name"]);
		$tableheader.=$dtb[0];
		$tbal=$dtb[1];
	}
	if($class["cid"]==1)
		$taset=$tbal;
	if($class["cid"]==2){
		$tliab=$tbal;

		$tableheader.='<tr class="inquirybg" style="font-weight:bold">
	<td colspan="2">Net Asset</td>
			<td>'.price_format_pr(($taset)-abs($tliab)).'</td>
		</tr>';
		
	}
}	

	$tableheader.='</table>';

	return $tableheader;
}
if (get_post('Export')) 
{
	
	$blnfrom=$_POST["bulanfrom"]+1;
	$blnto=$_POST["bulanto"]+1;
	$blnnya=date('M Y',strtotime($thn.'-'.$blnfrom.'-01'));
	if($blnfrom!=$blnto){		
		$blnnya=date('M',strtotime($thn.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thn.'-'.$blnto.'-01'));
	}
	$header='
	<style>
	.inquirybg2{
		font-weight:bold !important;
		font-size:12px !important;
	}
	.tableheader{
		background-color:#EE7600 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0">
		<tr><td align="center">&nbsp;</td></tr>
		<tr><td style="width:2%;">&nbsp;&nbsp;</td>
			<td>
	<table border="1">
		<tr>
			<td width="100%" align="left"  style="background-color:#d3d3d3;">PT. Kresna Asset Management</td>
		</tr>
		<tr>
			<td align="left" style="background-color:#d3d3d3;">-</td>
		</tr>
		<tr>
			<td align="left" style="background-color:#d3d3d3;">Jl. Widya Chandra V</td>
		</tr>
		<tr><td align="left" style="background-color:#d3d3d3;">&nbsp;</td></tr>
		<tr>
			<td align="left" style="background-color:#d3d3d3;">Balance Sheet</td>
		</tr>
		<tr>
			<td align="left" style="background-color:#d3d3d3;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="left" style="background-color:#d3d3d3;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 651;">';
	echo display_bs();
	echo '</td></tr></table>
		</td></tr>
		<tr><td></td><td align="center" style="background-color:#EE7600;border:#000 1px solid;">&nbsp;</td></tr>
		</table>';
}elseif(get_post('PrintPdf')){

// add a page
	$blnfrom=$_POST["bulanfrom"]+1;
	$blnto=$_POST["bulanto"]+1;
	$blnnya=date('M Y',strtotime($thn.'-'.$blnfrom.'-01'));
	if($blnfrom!=$blnto){		
		$blnnya=date('M',strtotime($thn.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thn.'-'.$blnto.'-01'));
	}
	$header='<table border="0">
		<tr>
			<td width="651px" align="center" height="20px"  bgcolor="#d3d3d3">PT. Kresna Asset Management</td>
		</tr>
		<tr>
			<td align="center" height="20px" style="background-color:#d3d3d3;">-</td>
		</tr>
		<tr>
			<td align="center" height="30px" style="background-color:#d3d3d3;">Jl. Widya Chandra V</td>
		</tr>
		<tr><td align="center" style="background-color:#d3d3d3;">&nbsp;</td></tr>
		<tr>
			<td align="center" height="20px" style="font-size:18px;background-color:#d3d3d3;">Balance Sheet</td>
		</tr>
		<tr>
			<td align="center" height="10px" style="background-color:#d3d3d3;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#d3d3d3;">&nbsp;</td></tr>
		
	';
	$header.= '<tr><td style="width: 651px;">';
	$header.=display_bs_pdf();
	$header.= '</td></tr></table>';
$pdf->AddPage();
// output the HTML content
$html=$header;
//$html.=display_bs_pdf();
$pdf->writeHTML(@$html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_BS.pdf', 'I');	

}else{
	gl_inquiry_controls();

	div_start('balance_tbl');
	if (get_post('Show'))
	echo display_bs();
	end_page();
}

