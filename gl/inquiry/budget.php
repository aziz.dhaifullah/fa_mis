<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/config_mssql.php");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
/*
*/
$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

if (isset($_GET["period"]))
	$_POST["period"] = $_GET["period"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];


if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"budget.xls\"");
	header("Cache-Control: max-age=0");
	
	
}else{
	page(_($help_context = "Budget"), false, false, '', $js);
}
//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}


//----------------------------------------------------------------------------------------------------

function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER,7);
	start_row();

	echo "<td>"._("Type").":</td>\n";
	echo "<td>";
	$period = array(_("Outstanding"),_("Multiple"));
	//$period = array(_("Monthly"),_("Quarterly"),_("Semi Annyally"),_("Annually"));
	echo array_selector('tipe', null, $period);
	echo "</td>\n";
	
	echo "<td>"._("From").":</td>\n";
    echo "<td>";
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("October"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'" '.($t==@$_POST['bulanfrom']?'selected':'').'>'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'" '.($t==@$_POST['bulanto']?'selected':'').'>'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
    end_row();
    echo '<tr>';
	echo "<td colspan=3></td>\n";
	date_cells(_(""), 'TransFromDate', '', null, -user_transaction_days());
	date_cells(_(" "), 'TransToDate');

	echo "<td>";
	echo '<input type="checkbox" name="realisasi" value="1" '.(@$_POST['realisasi']==1?'checked':'').'>';
	echo "</td>\n";
	echo "<td>"._("Vs Realisasi")."</td>\n";


	//date_cells(_("From:"), 'TransFromDate', '', null, -user_transaction_days());
	//date_cells(_("To:"), 'TransToDate', '', null, -user_transaction_days());
	//date_cells(_("End Date:"), 'TransToDate');
    end_row();
	end_table();

	start_table(TABLESTYLE_NOBORDER);
	start_row();

	submit_cells('Show',_("Show"),'','', 'default');
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel"></td>';
	
	//echo '<td><a class="ajaxsubmit export" href="export/budget.php" name="Export Excel" id="Export Excel" value="Export" target="_blank"><span>Export Excel</span></a></td>';
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	
	echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle2").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Budget",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';

	//submit_cells('Export Excel',_("Export"),'','', 'default');
	end_row();
	end_table();

	echo '<hr>';
    //end_form();
}

//----------------------------------------------------------------------------------------------------


function show_results()
{
	global $path_to_root, $systypes_array,$SysPrefs;

	
	$lampir=@$_POST['lampiran']+1;
	$ctype=$lampir;
	$colspan = (@$dim == 2 ? "6" : (@$dim == 1 ? "5" : "4"));
	start_table(TABLESTYLE, "width='90%'", 6);
	
	start_table(TABLESTYLE2, "width='90%'", 10);

	$th = array( _(""));

	$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
	$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
	$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
	$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	$blnnya=date('M Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));

	$bln=array();$colspan=2;
	$thn=date('Y');
	/*
	for($a=3;$a>=1;$a--){
		$thn2=$thn-$a;
		if(@$_POST["realisasi"]==1){
			$blnnya=date('M Y',strtotime($thn2.'-'.$blnfrom.'-01'));
			if($blnfrom!=$blnto){		
				$blnnya=date('M',strtotime($thn2.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thn2.'-'.$blnto.'-01'));
			}
			$bln[]='Realisasi<br>'.$blnnya;
			$colspan++;
		}
	}
	*/
	$blnnya=date('M Y',strtotime($thn.'-'.$blnfrom.'-01'));
	if($blnfrom!=$blnto){		
		$blnnya=date('M',strtotime($thn.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thn.'-'.$blnto.'-01'));
	}
	if(@$_POST["realisasi"]==1)		
		$colspan++;
	if($_POST["tipe"]==0){
		$bln[]='<b>Budget<br>'.$blnnya.'</br>';
		if(@$_POST["realisasi"]==1){
			$bln[]='<b>Realisasi<br>'.$blnnya.'</br>';			
			$bln[]='<b>Selisih<br>'.$blnnya.'</br>';			
			$colspan++;
		}

	}
	if($_POST["tipe"]==1){
		for($a=$blnfrom;$a<=$blnto;$a++){
			$bln[]='<b>Budget<br>'.date('M Y',strtotime($thn.'-'.$a.'-01')).'</br>';
			//$colspan++;
			if(@$_POST["realisasi"]==1){
				$bln[]='<b>Realisasi<br>'.date('M Y',strtotime($thn.'-'.$a.'-01')).'</br>';
				$bln[]='<b>Selisih<br>'.date('M Y',strtotime($thn.'-'.$a.'-01')).'</br>';
				$colspan++;
				$colspan++;
			}
				$colspan++;
		}
		if(@$_POST["realisasi"]==0){
			$bln[]='<b>Jumlah Budget</br>';
		}
	}
	//$colspan++;
	//print_r($bln);
	$th=array_merge($th,$bln);
	//table_header($th);
	$tableheader='<div id="trans_tbl">';
	$tableheader.='<table class="tablestyle" cellpadding="6" border="1" width="90%">';
	$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
	for($a=0;$a<count($th);$a++){
		$tableheader.='<td class="tableheader" align="center">'.$th[$a].'</td>';
	}
	echo $tableheader.='</tr>';
	//$budgets=get_budgets(date2sql($_POST['TransFromDate']),date2sql($_POST['TransToDate']),$_POST['period']);
	//print_r($budgets);
	//print_r($mfees);
	$no=1;
		$acctypes1 = get_account_classes(false, 0);
		//$acctypes1=get_account_types($all=false, $class_id=false,false);

		while($row1 = db_fetch_row($acctypes1)){
			//print_r($row1);
			if($row1[0]!=5 and $row1[0]!=6 and $row1[0]!=9)
				continue;
			echo '<tr>';
			$spasi='';
			if($row1[3]!='')
			$spasi='&nbsp;&nbsp;';
	    	label_cell($spasi.'<b>'.$row1[1].'</br>','colspan="'.$colspan.'"');

	    	$typeresult = get_account_types(false, $row1[0], -1);
			$k = 0; // row color
			$total1=0;$total2=0;
			//print_r($row1);
			while ($accounttype=db_fetch($typeresult))
			{
				//print_r($accounttype);

	    		$typeresult2 = get_account_types(false,false, $accounttype[0]);
				while ($accounttype2=db_fetch($typeresult2))
				{
					//print_r($accounttype2);
					echo '<tr>';
			    	label_cell('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>'.$accounttype2[0].' '.$accounttype2[1].'</b>','colspan="'.$colspan.'"');
					$accounts2=get_gl_accounts(false, false, $accounttype2[0]);
					while($row2 = db_fetch_row($accounts2)){
						echo '<tr>';
				    	label_cell('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$row2[1].' '.$row2[2]);
				    	/*
						if(@$_POST["realisasi"]==1){
					    	for($a=3;$a>=1;$a--){				
								$budamount2=sum_budgets(($thn-$a).'-'.$blnfrom.'-01',($thn-$a).'-'.$blnto.'-31',$row[0]);		
								label_cell(price_format(@$budamount2),'align="right"');
								$totbud2[$a]=$totbud2[$a]+$budamount2;
							}
						}				
						*/			
						if($_POST["tipe"]==1){
								$tbud=0;
							for($a=$blnfrom;$a<=$blnto;$a++){
								$budamount=sum_budgets($thn.'-'.$a.'-01',$thn.'-'.$a.'-'.cal_days_in_month(CAL_GREGORIAN, $a, $thn),$row2[0]);	
								label_cell(price_format(abs(@$budamount)),'align="right"');
								$totbud2[$a]=@$totbud2[$a]+$budamount;
								$tbud=@$tbud+$budamount;
								if(@$_POST["realisasi"]==1){
									$realamount=sum_bal2_account($thn.'-'.$a.'-01',$thn.'-'.$a.'-'.cal_days_in_month(CAL_GREGORIAN, $a, $thn),$row2[0]);
									//$realamount=1;	
									label_cell(price_format(abs(@$realamount)),'align="right"');
									$totreal2[$a]=@$totreal2[$a]+$realamount;
									$selisih=abs(@$realamount)-abs(@$budamount);
									label_cell(price_format(@$selisih),'align="right"');
								}
							}
							if(@$_POST["realisasi"]==0){
								label_cell(price_format(@$tbud),'align="right"');
							}
						}else{
							$budamount=sum_budgets($thn.'-'.$blnfrom.'-01',$thn.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thn),$row2[0]);	
							label_cell(price_format(abs(@$budamount)),'align="right"');
							$totbud=@$totbud+$budamount;
							if(@$_POST["realisasi"]==1){
								$realamount=sum_bal2_account($thn.'-'.$blnfrom.'-01',$thn.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thn),$row2[0]);	
								//$realamount=1;
								label_cell(price_format(abs(@$realamount)),'align="right"');
								$totreal=@$totreal+$realamount;
								$selisih=abs(@$realamount)-abs(@$budamount);
								label_cell(price_format(@$selisih),'align="right"');
								echo '<input type="hidden" name="acc[]" value="'.@$row2[0].'">';
								echo '<input type="hidden" name="budamount[]" value="'.@$realamount.'">';
							}
						}
						$subaccounts=get_chart_accounts_search(str_replace('-','',$row2[1]));
						while($rowsub = db_fetch_row($subaccounts)){
							if(strlen($rowsub[0])<6)
								continue;
							echo '<tr>';
				    		label_cell('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
				    			.$rowsub[1].' '.$rowsub[2]);
				    		if($_POST["tipe"]==1){
				    			$tbud=0;
								for($a=$blnfrom;$a<=$blnto;$a++){
									$budamount=sum_budgets($thn.'-'.$a.'-01',$thn.'-'.$a.'-'.cal_days_in_month(CAL_GREGORIAN, $a, $thn),$rowsub[0]);	
									label_cell(price_format(abs(@$budamount)),'align="right"');
									$totbud2[$a]=@$totbud2[$a]+$budamount;
									$tbud=@$tbud+$budamount;
									if(@$_POST["realisasi"]==1){
										$realamount=sum_bal2_account($thn.'-'.$a.'-01',$thn.'-'.$a.'-'.cal_days_in_month(CAL_GREGORIAN, $a, $thn),$rowsub[0]);
										//$realamount=1;	
										label_cell(price_format(abs(@$realamount)),'align="right"');
										$totreal2[$a]=@$totreal2[$a]+$realamount;
										$selisih=abs(@$realamount)-abs(@$budamount);
										label_cell(price_format(@$selisih),'align="right"');
									}
								}
								if(@$_POST["realisasi"]==0){
									label_cell(price_format(@$tbud),'align="right"');
								}
							}else{
								$budamount=sum_budgets($thn.'-'.$blnfrom.'-01',$thn.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thn),$rowsub[0]);	
								label_cell(price_format(abs(@$budamount)),'align="right"');
								$totbud=@$totbud+$budamount;
								if(@$_POST["realisasi"]==1){
									$realamount=sum_bal2_account($thn.'-'.$blnfrom.'-01',$thn.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thn),$rowsub[0]);	
									//$realamount=1;
									label_cell(price_format(abs(@$realamount)),'align="right"');
									$totreal=@$totreal+$realamount;
									$selisih=abs(@$realamount)-abs(@$budamount);
									label_cell(price_format(@$selisih),'align="right"');
									echo '<input type="hidden" name="acc[]" value="'.@$rowsub[0].'">';
									echo '<input type="hidden" name="budamount[]" value="'.@$realamount.'">';
								}
							}
						}
				    }
				}
				$accounts=get_gl_accounts(false, false, $accounttype[0]);
				while($row = db_fetch_row($accounts)){
					//print_r($row);
					echo '<tr>';
			    	label_cell('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$row[1].' '.$row[2]);
			    	/*
					if(@$_POST["realisasi"]==1){
				    	for($a=3;$a>=1;$a--){				
							$budamount2=sum_budgets(($thn-$a).'-'.$blnfrom.'-01',($thn-$a).'-'.$blnto.'-31',$row[0]);		
							label_cell(price_format(@$budamount2),'align="right"');
							$totbud2[$a]=$totbud2[$a]+$budamount2;
						}
					}				
					*/			
					if($_POST["tipe"]==1){
		    			$tbud=0;
						for($a=$blnfrom;$a<=$blnto;$a++){
							$budamount=sum_budgets($thn.'-'.$a.'-01',$thn.'-'.$a.'-'.cal_days_in_month(CAL_GREGORIAN, $a, $thn),$row[0]);	
							label_cell(price_format(abs(@$budamount)),'align="right"');
							$totbud2[$a]=@$totbud2[$a]+$budamount;
							$tbud=@$tbud+$budamount;
							if(@$_POST["realisasi"]==1){
								$realamount=sum_bal2_account($thn.'-'.$a.'-01',$thn.'-'.$a.'-'.cal_days_in_month(CAL_GREGORIAN, $a, $thn),$row[0]);
								//$realamount=1;	
								label_cell(price_format(abs(@$realamount)),'align="right"');
								$totreal2[$a]=@$totreal2[$a]+$realamount;
								$selisih=abs(@$realamount)-abs(@$budamount);
								label_cell(price_format(@$selisih),'align="right"');
							}
						}
						if(@$_POST["realisasi"]==0){
							label_cell(price_format(@$tbud),'align="right"');
						}
					}else{
						$budamount=sum_budgets($thn.'-'.$blnfrom.'-01',$thn.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thn),$row[0]);	
						label_cell(price_format(abs(@$budamount)),'align="right"');
						$totbud=@$totbud+$budamount;
						if(@$_POST["realisasi"]==1){
							$realamount=sum_bal2_account($thn.'-'.$blnfrom.'-01',$thn.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thn),$row[0]);	
							//$realamount=1;
							label_cell(price_format(abs(@$realamount)),'align="right"');
							$totreal=@$totreal+$realamount;
							$selisih=abs(@$realamount)-abs(@$budamount);
							label_cell(price_format(@$selisih),'align="right"');
							echo '<input type="hidden" name="acc[]" value="'.@$row[0].'">';
							echo '<input type="hidden" name="budamount[]" value="'.@$realamount.'">';
						}
					}
					//label_cell(@$budamount);
			    }
		    }		    
			echo '<tr>';
	    	label_cell($spasi.'<b>Total '.$row1[1].'</b>');
	    	
			if(@$_POST["tipe"]==1){
    			$tbud=0;
				for($a=$blnfrom;$a<=$blnto;$a++){
					$tbud=@$tbud+abs($totbud2[$a]);	
					label_cell('<b>'.price_format(abs($totbud2[$a])).'</b>','align="right"');	
					if(@$_POST["realisasi"]==1){
						label_cell('<b>'.price_format(abs(@$totreal2[$a])).'</b>','align="right"');
						label_cell('<b>'.price_format(abs(@$totreal2[$a])-abs(@$totbud2[$a])).'</b>','align="right"');

					}
				}
				if(@$_POST["realisasi"]==0){
					label_cell('<b>'.price_format(@$tbud).'</b>','align="right"');
				}
			}else{				
				label_cell('<b>'.price_format(abs(@$totbud)).'</b>','align="right"');	
				if(@$_POST["realisasi"]==1){
					label_cell('<b>'.price_format(abs(@$totreal)).'</b>','align="right"');
					label_cell('<b>'.price_format(abs(@$totreal)-abs(@$totbud)).'</b>','align="right"');

				}
			}
			$totbud=0;$totbud2=array();
			$totreal=0;$totreal2=array();
			    	$no++;
		}
		if($no>1){
			if(@$_POST["realisasi"]==1 && @$_POST["tipe"]==0)
	    	echo '<tr><td colspan="'.($colspan).'"><center>
	    		<input type="submit" class="export" name="Budget" id="Budget" value="Realisasi To Budget">
    		</center></td></tr>';
	    }
	    if($no==1){
	    	echo '<tr><td colspan="'.($colspan).'">No Data Available</td></tr>';
	    }
/*
*/
	end_table(1);
}
//----------------------------------------------------------------------------------------------------


if (get_post('Export')) 
{
	$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
	$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
	$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
	$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	$blnnya=date('M Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));

	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('M',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('M Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
	$header='
	<style>
	.inquirybg{
		background-color:#000 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	</style>
	<table border="0">

		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">BUDGET</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="min-width:400px;">';
	show_results();
	echo '</td></tr></table>';
}else{
	$accs=@$_POST['acc'];
	$budamounts=@$_POST['budamount'];

	if (count($accs)>0){
		for($a=0;$a<count($accs);$a++){
			if($accs[$a]!='' and abs($budamounts[$a])>0)
			add_update_gl_budget_trans($_POST["TransToDate"], $accs[$a], '', '', abs($budamounts[$a]));
		}
	}
	//echo count($accs);
	gl_inquiry_controls();
	div_start('trans_tbl');

	if (get_post('Show') || count($accs)>0)
	    show_results();

	div_end();

	//----------------------------------------------------------------------------------------------------

	end_page();

}

