<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/siar.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format(0);


if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"Sharing Fee.xls\"");
	header("Cache-Control: max-age=0");
	
	
}else{
	page(_($help_context = "Sharing Fee"), false, false, '', $js);
}


//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}
//----------------------------------------------------------------------------------------------------

function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER);
	start_row();
   // gl_all_accounts_list_cells(_("Account:"), 'account', null, false, false, _("All Accounts"));
	//, -user_transaction_days()
	date_cells(_("From:"), 'TransFromDate', '', null);
	date_cells(_("To:"), 'TransToDate', '', null);

	$sa=get_sa_shrfee();
	echo "<td>"._("Selling Agent").":</td>\n";
	echo "<td>";
	echo '<select autocomplete="off" name="sagent" class="combo" title="" _last="0">';
	for($a=0;$a<count($sa);$a++)
	{
		echo '<option value="'.$sa[$a][0].'">'.$sa[$a][1].'</option>';
	}
	echo '</select>';
	echo "</td>\n";
	echo "<td>"._("Fee Rate").":</td>\n";
	echo "<td>";
	echo '<input type="text" name="feerate" size="5">&nbsp;%';
	echo "</td>\n";
	//date_cells(_("to:"), 'TransToDate');
    end_row();
	end_table();

	start_table(TABLESTYLE_NOBORDER);
	start_row();

	submit_cells('Show',_("Show"),'','', 'default');
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel"></td>';
	echo '</tr>';
	echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
		function xls(){
			//console.log("aa");
			$(".tablestyle2").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Sharing_Fee",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
	//submit_cells('Export Excel',_("Export"),'','', 'default');
	end_row();
	end_table();

	echo '<hr>';
    end_form();
}

//----------------------------------------------------------------------------------------------------

function show_results()
{
	global $path_to_root, $systypes_array;


	start_table(TABLESTYLE2, "width='90%'", 10);
	
	$th = array(_("Trade Date"), _("Investor No"),  _("Shares"),  _("NAVToday"),  _("Return"),  _("Amount"));
	    			
	//table_header($th);
	$prods=get_sa_prod_shrfee($_POST['sagent']);
	$prod=array();$dppprod=array();
	for($b=0;$b<count($prods);$b++)
	{
		//table_header(array(_($prods[$b][1])));
		echo '<tr class="inquirybg" style="font-weight:bold"><td class="tableheader" align="center">'.$prods[$b][1].'</td></tr>';
		echo '<tr><td>';
			start_table(TABLESTYLE2, "width='100%' border='1' cellpadding='6'", 5);
			$tableheader='';
			//$tableheader.='<table class="tablestyle" cellpadding="6" border="1" width="90%">';
			$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
			for($a=0;$a<count($th);$a++){
				$tableheader.='<td class="tableheader" align="center">'.$th[$a].'</td>';
			}
			echo $tableheader.='</tr>';
			//table_header($th);
			//echo $prods[$b][0];
			$mfees=get_shrfee(date2sql($_POST['TransFromDate']),date2sql($_POST['TransToDate']),$prods[$b][0],$_POST['sagent']);
			$total=0;
			for($a=0;$a<count($mfees);$a++)
			{
				echo '<tr>';
				//$aum=$mfees[$a][3]*$mfees[$a][1];
				$aum=$mfees[$a][1]*$mfees[$a][2];
				$amount=$aum*$mfees[$a][3];
				$tgl = date ( 'd M Y' , strtotime($mfees[$a][0]) );
				label_cell($tgl);	
				label_cell($mfees[$a][4]);	
	    		label_cell(price_format($aum),'align="right"');
	    		label_cell(price_format($mfees[$a][2]),'align="right"');
	    		label_cell(price_format($mfees[$a][3]),'align="right"');
	    		label_cell(price_format($amount),'align="right"');
	    		$total=$total+$amount;
				echo '</tr>';
			}
				echo '<tr>';
				label_cell('','colspan="5"');	
	    		label_cell('<b>'.price_format($total).'</b>','align="right"');
				echo '</tr>';
				echo '<tr>';
				label_cell('<b>'.$_POST['feerate'].'%</b>','colspan="5" align="center"');	
	    		label_cell('<b>'.price_format($total*($_POST['feerate']/100)).'</b>','align="right"');
				echo '</tr>';
			echo '</table>';
		echo '</td></tr>';
		$prod[]=$prods[$b][1];
		$dppprod[]=$total*($_POST['feerate']/100);
	}
		//echo '<tr><td><td></tr>';
		echo '<tr><td align="center">';
		//start_table(TABLESTYLE2, "width='30%'", 5);
		start_table(TABLESTYLE2, "width='50%' border='1' cellpadding='6'", 5);

		$tgl2 = date ( 'M Y' , strtotime(date2sql($_POST['TransToDate'])) );
		table_header(array(_(''),_($tgl2)));
		//print_r($prod);
		$dpp=0;
		for($a=0;$a<count($prod);$a++)
		{
			echo '<tr>';
			label_cell($prod[$a]);	
    		label_cell(price_format($dppprod[$a]),'align="right"');
			echo '</tr>';
			$dpp=$dpp+$dppprod[$a];
		}
			echo '<tr>';
			label_cell('DPP');	
    		label_cell(price_format($dpp),'align="right"');
			echo '</tr>';
			echo '<tr>';

			$ppn=$dpp*0.1;
			label_cell('PPN');	
    		label_cell(price_format($ppn),'align="right"');
			echo '</tr>';
			echo '<tr>';

			$pph=$dpp*0.02;
			label_cell('PPH 23');	
    		label_cell('('.price_format($pph).')','align="right"');
			echo '</tr>';

			$ttl=$dpp+$ppn-$pph;
			label_cell('<b>Total</b>');	
    		label_cell('<b>'.price_format($ttl).'</b>','align="right"');
			echo '</tr>';
		echo '</table>';
		echo '</td></tr>';
		//echo '<tr><td><td></tr>';
	/*
	$mfees=get_shrfee(date2sql($_POST['TransFromDate']),date2sql($_POST['TransToDate']));
	//print_r($mfees);
	if(count($mfees)>0){
		$no=1;
		for($a=0;$a<count($mfees);$a++)
		{
			//print_r($mfees[$a]);
			$mgtfee=$mfees[$a][0];
			$shrfee=0;
			$custid=get_customer_id($mfees[$a][4]);
			$invoice='<form method="post" action="../../purchasing/po_entry_items.php?NewInvoice=Yes">
				<input type="hidden" name="mfee_cust_id" value="'.$custid.'">
				<input type="hidden" name="mfee_amount" value="'.$mgtfee.'">
				<input type="hidden" name="type" value="shrfee">
				<input type="submit" name="invoice" value="Set Invoice">
				</form>
			';
			echo '<tr>';
	    	label_cell($no);
	    	label_cell($mfees[$a][1]);
	    	label_cell($mfees[$a][2]);
	    	label_cell(price_format($mgtfee),'align="right"');
	    	//label_cell(price_format($shrfee),'align="right"');
	    	//label_cell('');
	    	label_cell($invoice);
	    	$no++;
	    }
    }else{
    	echo '<tr><td colspan="5">No Data Available</td></tr>';
    }
    */
}
//----------------------------------------------------------------------------------------------------

if (get_post('Export')) 
{
	
	$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
	$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
	$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
	$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	$blnnya=date('M Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));

	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('M',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('M Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
	$header='<table border="0" style="border:#000 1px solid;">
		<tr>
			<td width="100%" align="center"  style="background-color:#8072a9;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:20px;">Sharing Fee</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 651pt;">';
	show_results();
	echo '</td></tr></table>';
}else{
	
	gl_inquiry_controls();

	div_start('trans_tbl');

	if (get_post('Show') || get_post('account'))
	    show_results();

	div_end();

	//----------------------------------------------------------------------------------------------------

	end_page();

}

