<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/


$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

// page(_($help_context = "List Nominatif Marketing"), false, false, '', $js);

//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}

if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format(0);

if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"Entertainment_detail.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
	page(_($help_context = "List Nominatif Entertainment"), false, false, '', $js);
}

//----------------------------------------------------------------------------------------------------

function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER);
	start_row();
   // gl_all_accounts_list_cells(_("Account:"), 'account', null, false, false, _("All Accounts"));
	date_cells(_("from:"), 'TransFromDate', '', null, -user_transaction_days());
	date_cells(_("to:"), 'TransToDate');
    end_row();
	end_table();

	start_table(TABLESTYLE_NOBORDER);
	start_row();

	submit_cells('Show',_("Show"),'','', 'default');
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">&nbsp;';
	//echo '<input type="submit" class="export" name="PrintPdf" id="PrintPdf" Value="Print PDF"></td>';
	// echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	//submit_cells('Export Excel',_("Export"),'','', 'default');
	
	echo '
	<style>
		.export{
			align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
		function xls(){
			//console.log("aa");
			$(".tablestyle").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "marketing_detail",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
	end_row();
	end_table();

	echo '<hr>';
    end_form();
}

//----------------------------------------------------------------------------------------------------

function show_results()
{
	global $path_to_root, $systypes_array;

	$result = get_gl_transactions_marketing2(@$_POST['TransFromDate'], @$_POST['TransToDate'], -1,
    	@$_POST["account"], @$_POST['Dimension'], @$_POST['Dimension2'], null,
    	input_num('amount_min'), input_num('amount_max'));

	$colspan = (@$dim == 2 ? "6" : (@$dim == 1 ? "5" : "4"));

	if (@$_POST["account"] != '')
		display_heading($_POST["account"]. "&nbsp;&nbsp;&nbsp;".$act_name);
	
	//div_start('trans_tbl');
	//start_table(TABLESTYLE,'border="1" width="90%"',6);
	
	
	$tableheader='<center><div id="trans_tbl">';
	$tableheader.='<table class="tablestyle" cellpadding="6" border="1">';
	$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
	$tableheader.='<td align="center" rowspan="2">No</td>';
	$tableheader.='<td align="center" colspan="5">Entertainment</td>';
	$tableheader.='<td align="center" colspan="6">To Entertain</td>';
	$tableheader.='</tr>';
	$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
	$tableheader.='<td align="center">Date</td>';
	$tableheader.='<td align="center">Place</td>';
	$tableheader.='<td align="center">Address</td>';
	$tableheader.='<td align="center">Type</td>';
	$tableheader.='<td align="center">Total</td>';

	$tableheader.='<td align="center">Name</td>';
	$tableheader.='<td align="center">Position</td>';
	$tableheader.='<td align="center">Name of The Company</td>';
	$tableheader.='<td align="center">Business Type</td>';
	$tableheader.='<td align="center">No Voucher</td>';
	$tableheader.='<td align="center">Keterangan</td>';
	$tableheader.='</tr>';
	//$th = array(_("No"), _("Date"), _("Place"), _("Address"), _("Type")	, _("Total"), _("Name"),_("Position"), _("Name of Company"), _("Business Type"), _("Reff"));
	    
	//$th = array_merge($first_cols, $account_col, $dim_cols, $remaining_cols);
	echo $tableheader;		
	//table_header($th);
	$bfw = 0;

	$running_total = $bfw;
	$j = 1;
	$k = 0; //row colour counter
	$no=0;
	$ttl=0;
	while ($myrow = db_fetch($result))
	{
		$ttl=$ttl+$myrow['gl_amount'];
    	$no++;  	

		$tgl = date('d/m/Y',strtotime(@$myrow['tran_date']));
		echo '<tr>';
    	label_cell($no);
		label_cell(($tgl!=''?$tgl:'-'));
		label_cell(($myrow['marketing_place']!=''?$myrow['marketing_place']:'-'));
		label_cell(($myrow['marketing_address']!=''?$myrow['marketing_address']:'-'));
		label_cell(($myrow['marketing_type']!=''?$myrow['marketing_type']:'-'));
		amount_cell(($myrow['gl_amount']!=''?$myrow['amount']:'-'));
		//label_cell($myrow['tran_date']);
		label_cell(($myrow['marketing_name']!=''?$myrow['marketing_name']:'-'));
		label_cell(($myrow['marketing_position']!='0'?$myrow['marketing_position']:'-'));
		label_cell(($myrow['marketing_company']!='0'?$myrow['marketing_company']:'-'));
		label_cell(($myrow['marketing_business']!='0'?$myrow['marketing_business']:'-'));
		label_cell($myrow['ref']);
		//label_cell(($myrow['memo_']!=''?$myrow['memo_']:'-'));
		$memo=get_gl_memo($myrow['type_no'], $myrow['type'],$myrow['marketing_name']);
		if($memo=='')
		$memo=get_gl_memo($myrow['type_no'], $myrow['type'],$myrow['marketing_place']);
		label_cell($memo);
		echo '</tr>';
	}
	if($no==0){
		echo '<tr><td colspan="11"><center>No data</center></td></tr>';
	}
	else{
		echo '<tr><td colspan="5"><b>Total</b></td>';
		amount_cell($ttl);
		echo '<td colspan="6"><center></center></td></tr>';
	}
	end_table(1); // outer table
	div_end();
}
//----------------------------------------------------------------------------------------------------

function show_results_pdf()
{
	global $path_to_root, $systypes_array;

	$result = get_gl_transactions_marketing($_POST['TransFromDate'], $_POST['TransToDate'], -1,
    	$_POST["account"], $_POST['Dimension'], $_POST['Dimension2'], null,
    	input_num('amount_min'), input_num('amount_max'));

	$colspan = ($dim == 2 ? "6" : ($dim == 1 ? "5" : "4"));

	if ($_POST["account"] != null)
		display_heading($_POST["account"]. "&nbsp;&nbsp;&nbsp;".$act_name);
	
	// div_start('trans_tbl');
	// start_table(TABLESTYLE,'border="1" width="90%"',6);

	$tableheader='<div id="trans_tbl">';
	$tableheader.='<table class="tablestyle" cellpadding="6" border="1" style="margin: auto;font-size:8px !important;">';
	$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
	$tableheader.='<td align="center" rowspan="2">No</td>';
	$tableheader.='<td align="center" colspan="4">Entertainment</td>';
	$tableheader.='<td align="center" colspan="6">To Entertain</td>';
	$tableheader.='</tr>';
	$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
	$tableheader.='<td align="center">Place</td>';
	$tableheader.='<td align="center">Address</td>';
	$tableheader.='<td align="center">Type</td>';
	$tableheader.='<td align="center">Total</td>';

	$tableheader.='<td align="center">Name</td>';
	$tableheader.='<td align="center">Position</td>';
	$tableheader.='<td align="center">Name of The Company</td>';
	$tableheader.='<td align="center">Business Type</td>';
	$tableheader.='<td align="center">No Voucher</td>';
	$tableheader.='<td align="center">Keterangan</td>';
	$tableheader.='</tr>';

	// $th = array(_("No"), _("Date"), _("Place"), _("Address"), _("Type")
	// 	, _("Total"), _("Name"),_("Position"), _("Name of Company"), _("Business Type"), _("Reff"));
	    
	//$th = array_merge($first_cols, $account_col, $dim_cols, $remaining_cols);
			
	// table_header($th);
	$bfw = 0;

	$running_total = $bfw;
	$j = 1;
	$k = 0; //row colour counter
	$no=0;
	$ttl=0;
	while ($myrow = db_fetch($result))
	{
		$ttl=$ttl+$myrow['gl_amount'];
    	$no++;  	
		echo '<tr>';
    	label_cell($no);
		label_cell(($myrow['marketing_place']!=''?$myrow['marketing_place']:'-'));
		label_cell(($myrow['marketing_address']!=''?$myrow['marketing_address']:'-'));
		label_cell(($myrow['marketing_type']!=''?$myrow['marketing_type']:'-'));
		label_cell(($myrow['gl_amount']!=''?$myrow['gl_amount']:'-'));
		label_cell($myrow['tran_date']);
		label_cell(($myrow['marketing_name']!=''?$myrow['marketing_name']:'-'));
		label_cell(($myrow['marketing_position']!=''?$myrow['marketing_position']:'-'));
		label_cell(($myrow['marketing_company']!=''?$myrow['marketing_company']:'-'));
		label_cell(($myrow['marketing_business']!=''?$myrow['marketing_business']:'-'));
		label_cell($myrow['ref']);
		label_cell(($myrow['memo_']!=''?$myrow['memo_']:'-'));

		echo '</tr>';
	}
	if($no==0){
		$tableheader.= '<tr><td colspan="11"><center>No data</center></td></tr>';
	}
	else{
		echo '<tr><td colspan="7"><b>Total</b></td>';
		amount_cell($ttl);
		echo '<td colspan="3"><center></center></td></tr>';
	}
	// end_table(1); // outer table
	// div_end();
	$tableheader.= '</table></div>';
	return $tableheader;
}

//----------------------------------------------------------------------------------------------------


	$blnfrom=date('m',strtotime(date2sql(@$_POST['TransFromDate'])));
	$blnto=date('m',strtotime(date2sql(@$_POST['TransToDate'])));
	$thnfrom=date('Y',strtotime(date2sql(@$_POST['TransFromDate'])));
	$thnto=date('Y',strtotime(date2sql(@$_POST['TransToDate'])));
	$blnnya=date('M Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));

	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('M',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('M Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
if (get_post('Export')) 
{
	$header='
	<style>
	.inquirybg2{
		font-weight:bold !important;
		font-size:12px !important;
	}
	.tableheader{
		background-color:#EE7600 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0">
		<tr><td align="center">&nbsp;</td></tr>
		<tr><td style="width:2%;">&nbsp;&nbsp;</td>
			<td>
	<table border="0" style="border:#000 1px solid;">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">List Nominatif Entertainment</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 409pt;">';
	show_results();
	echo '</td></tr></table>
		</td></tr>
		<tr><td></td><td align="center" style="background-color:#EE7600;border:#000 1px solid;">&nbsp;</td></tr>
		</table>';
}elseif(get_post('PrintPdf')){

// add a page
	$header='<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">List Nominatif Entertainment</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	$header.= '<tr><td style="width: 638px;">';
	$header.=show_results_pdf();
	$header.= '</td></tr></table>';
	$pdf->AddPage();
	// output the HTML content
	$html=$header;
	//$html.=display_bs_pdf();
	$pdf->writeHTML(@$html, true, 0, true, 0);

	// reset pointer to the last page
	$pdf->lastPage();

	// ---------------------------------------------------------

	//Close and output PDF document
	$pdf->Output('print_Entertainment_detail.pdf', 'I');	

}else{
	
	gl_inquiry_controls();

	div_start('trans_tbl');

	if (get_post('Show') || get_post('account'))
	    show_results();

	if (get_post('Export')){
	    show_results();
	}
	div_end();

	end_page();

}

