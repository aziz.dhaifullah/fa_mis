<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLANALYTIC';
$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

$js = "";
if (user_use_date_picker())
	$js = get_js_date_picker();

if (get_post('Export')) 
{
	//include_once('head.php');
	
	header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
	header("Content-Disposition: inline;filename=\"Profit & Loss.xls\"");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
page(_($help_context = "Profit & Loss "), false, false, "", $js);
}


$k = 0;
$pdeb = $pcre = $cdeb = $ccre = $tdeb = $tcre = $pbal = $cbal = $tbal = 0;

//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('balance_tbl');
}

if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER);

	$date = today();
	if (!isset($_POST['TransToDate']))
		$_POST['TransToDate'] = end_month($date);
	if (!isset($_POST['TransFromDate']))
		$_POST['TransFromDate'] = add_days(end_month($date), -user_transaction_days());
	echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("Type").":</td>\n";
	echo "<td>";
	$period = array(_("Outstanding"),_("Multiple"));
	//$period = array(_("Monthly"),_("Quarterly"),_("Semi Annyally"),_("Annually"));
	echo array_selector('tipe', null, $period);
	echo "</td>\n";
	echo '<tr>';
	echo '<tr>';
    date_cells(_("&nbsp;"), 'TransFromDate');
	date_cells(_("&nbsp;"), 'TransToDate');
	if ($dim >= 1)
		dimensions_list_cells(_("Dimension")." 1:", 'Dimension', null, true, " ", false, 1);
	if ($dim > 1)
		dimensions_list_cells(_("Dimension")." 2:", 'Dimension2', null, true, " ", false, 2);
	//check_cells(_("No zero values"), 'NoZero', null);
	//check_cells(_("Account Code"), 'AccCode', null);
	echo '<td></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td colspan="4"></td>';
	submit_cells('Show',_("Show"),'align="right"','', 'default');
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">
	<input type="submit" class="export" name="PrintPdf" id="PrintPdf" Value="Print PDF"></td>';
	echo '</tr>';
	echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Balance_sheet",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';

	//submit_cells('Show',_("Show"),'','', 'default');
    end_table();
    end_form();
}

//----------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------

function display_bs(){
	if (isset($_POST['TransFromDate']))
	{
		$row = get_current_fiscalyear();
		if (date1_greater_date2($_POST['TransFromDate'], sql2date($row['end'])))
		{
			display_error(_("The from date cannot be bigger than the fiscal year end."));
			set_focus('TransFromDate');
			return;
		}
	}
	if (!isset($_POST['Dimension']))
		$_POST['Dimension'] = 0;
	if (!isset($_POST['Dimension2']))
		$_POST['Dimension2'] = 0;
	//start_table(TABLESTYLE,'',6);

	if($_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=$_POST['bulanfrom'];
		$blnto=$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

	$begin=date('d/m/Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	//$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));
	$tglto=date('d',strtotime(date2sql($_POST['TransToDate'])));
	if($tglto==cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto))
	$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));
	else
	$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.$tglto));
	/*
	if($_POST["tipe"]==0){
		$colom=3;
	}else{
		$colom=3+($blnto-$begin);
	}
	*/
	if(get_post('PrintPdf'))
	$tableheader ='<center><table class="tablestyle" width="98.5%" cellpadding="3">';
	else
	$tableheader ='<center><table class="tablestyle" width="100%" cellpadding="3">';

	$colspan=4;
	$cols=2;
	if($_POST["tipe"]==0){
		$tableheader .= '<tr><td class="tableheader" colspan="'.($colspan).'" bgcolor="#dee7ec"><b>&nbsp;</b></td></tr>';
	}else{
		$colspan+=$blnto-1;
		$cols+=$blnto-1;
		$tableheader .= '<tr><td>&nbsp;</td><td class="tableheader" colspan="2">&nbsp;</td>';
		for($a=$blnfrom;$a<=$blnto;$a++){
			$tableheader .= '<td class="tableheader" ><b>'.date('M Y',strtotime($thnto.'-'.$a.'-01')).'</td>';
		}
		$tableheader .= '</tr>';
	}

	$totalaset=0;
	//$tableheader .= '<tr><td colspan="3">Kas dan Setara Kas</td></tr>';
	//get_account_from_to
	$ttlall=0;$ttlall2=0;

	//Pendapatan usaha
	$accounts=get_account_from_to('40000','46000');
	$nrow=n_account_from_to('40000','46000');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	$ttlcol=array();$ttlcol2=array();
	$ttlalls=array();$ttlalls2=array();
	
	$tableheader .= '<tr><td>&nbsp;</td><td><b>4-0000</td><td colspan="'.($cols).'"><b>Pendapatan Usaha</td></tr>';
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;
		$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;'.$account["account_code2"].'</td><td>'.$account["account_name"].'</td>';
		if($_POST["tipe"]==0){
			
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
			$total=$total+($tot['debit']-$tot['credit']);
			$ttlclass+=$total;
			$ttlclass2+=$total;
			$ttlall+=$total;
			$tableheader .= '<td align="right">'.price_format_pr(($total)*(-1)).'</td>';
			
		}else{
			for($b=$blnfrom;$b<=$blnto;$b++){

				$ttl=0;
				$bln1=date('d/m/Y',strtotime($thnfrom.'-'.$b.'-01'));
				$bln2=date('d/m/Y',strtotime($thnto.'-'.$b.'-'.cal_days_in_month(CAL_GREGORIAN, $b, $thnto) ));
				//$prev = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $bln1, $begin, false, true);
				//$curr = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $_POST['TransFromDate'], $bln2, false, true);
				$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $bln1, $bln2, false, true);
				$ttl=$ttl+($tot['debit']-$tot['credit']);
				$tableheader.='<td align="right">'.price_format_pr(($ttl)*(-1)).'</td>';
				$ttlcol[$b]=@$ttlcol[$b]+$ttl;
				$ttlcol2[$b]=@$ttlcol2[$b]+$ttl;
				$ttlalls[$b]=@$ttlalls[$b]+$ttl;
			}
		}

		$tableheader .= '</tr>';

		$n++;
	}
	$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;</td><td><b>Total Pendapatan Usaha</td>';
	if($_POST["tipe"]==0){
		$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlclass)*(-1)).'</td>';
		
	}else{
		for($b=$blnfrom;$b<=$blnto;$b++){
			$tableheader .= '<td align="right"><b>'.price_format_pr((@$ttlcol[$b])*(-1)).'</td>';
		}
	}
	$ttlcol=array();
	//$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlclass)).'</td>';
	$tableheader .= '</tr>';
	$tableheader .= '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';

	//Cost Of Sales
	$accounts=get_account_from_to('51000','51005');
	$nrow=n_account_from_to('51000','51005');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	
	$tableheader .= '<tr><td>&nbsp;</td><td><b>5-0000</td><td colspan="'.($cols).'"><b>Cost Of Sales</td></tr>';
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;
		$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;'.$account["account_code2"].'</td><td>'.$account["account_name"].'</td>';
		if($_POST["tipe"]==0){
			
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
			$total=$total+($tot['debit']-$tot['credit']);
			$ttlclass+=$total;
			$ttlclass2+=$total;
			$ttlall+=$total;
			$tableheader .= '<td align="right">'.price_format_pr(($total)).'</td>';
			
		}else{
			for($b=$blnfrom;$b<=$blnto;$b++){

				$ttl=0;
				$bln1=date('d/m/Y',strtotime($thnfrom.'-'.$b.'-01'));
				$bln2=date('d/m/Y',strtotime($thnto.'-'.$b.'-'.cal_days_in_month(CAL_GREGORIAN, $b, $thnto) ));
				$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $bln1, $bln2, false, true);
				$ttl=$ttl+($tot['debit']-$tot['credit']);
				$tableheader.='<td align="right">'.price_format_pr(($ttl)).'</td>';
				$ttlcol[$b]=@$ttlcol[$b]+$ttl;
				$ttlcol2[$b]=@$ttlcol2[$b]+$ttl;
				$ttlalls[$b]=@$ttlalls[$b]+$ttl;
			}
		}

		$tableheader .= '</tr>';
		$n++;
	}
	$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;</td><td><b>Total Cost Of Sales</td>';
	if($_POST["tipe"]==0){
		$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlclass)).'</td>';
		
	}else{
		for($b=$blnfrom;$b<=$blnto;$b++){
			$tableheader .= '<td align="right"><b>'.price_format_pr((@$ttlcol[$b])).'</td>';
		}
	}
	$ttlcol=array();
	//$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlclass)).'</td>';
	$tableheader .= '</tr>';
	$tableheader .= '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';

	$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;</td><td><b>Gross Profit</td>';
	if($_POST["tipe"]==0){
		$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlall)*(-1)).'</td>';
		
	}else{
		for($b=$blnfrom;$b<=$blnto;$b++){
			$tableheader .= '<td align="right"><b>'.price_format_pr((@$ttlalls[$b])*(-1)).'</td>';
		}
	}
	//$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlclass)).'</td>';
	$tableheader .= '</tr>';
	$tableheader .= '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';


	//Biaya Operasional
	$ttlcol=array();$ttlcol2=array();$ttlalls3=array();
	$ttlclass=0;$ttlclass2=0;
	$accounts=get_account_from_to('61000','69990');
	$nrow=n_account_from_to('61000','69990');
	$tableheader .= '<tr><td>&nbsp;</td><td><b>6-0000</td><td colspan="'.($cols).'"><b>Biaya Operasional</td></tr>';
	$notacc=array(
		'62002001','62002002','62002003','62002004','62002005',
		'62002006','62002007','62002008','62002099',
		'62003001','62003002','62003003','63003001','69101001'
		,'69101002','69101003','69104001','69104002','69104003','69104004'
		,'69104005');
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;
		if(strlen($account["account_code"])<6){

			$paracc=get_parent_subaccount($account["account_code"]);
			if(@$paracc[0]==''){
				if(!in_array($account["head"],$head) and $account["account_type"]!='6-0000'){	

					if(@$head[($a-1)]!=@$head[($a)] ){
						//print_r($ttlcol);
						$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;</td><td><b>Total '.@$head[($a-1)].'</td>';
						if($_POST["tipe"]==0){
							$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlclass)).'</td>';
						}else{
							for($b=$blnfrom;$b<=$blnto;$b++){
								//$ttlcol[$b]=0;
								$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlcol[$b])).'</td>';
								$ttlcol[$b]=0;
							}	
							$tableheader .= '</tr>';
						}
						$tableheader .= '</tr>';
						//$tableheader .= '<tr><td colspan="'.(2+$blnto).'">&nbsp;</td></tr>';
						$ttlclass=0;

					}
					$tableheader .= '<tr><td>&nbsp;</td><td><b>&nbsp;'.$account["account_type"].'</td><td colspan="'.($cols).'"><b>'.$account["head"].'</td></tr>';
					$head[]=$account["head"];
					$a++;
				}

				$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;'.$account["account_code2"].'</td><td>'.$account["account_name"].'</td>';
				$subaccounts=get_subaccount($account["account_code"]);
				if($_POST["tipe"]==0){
					
					while ($subaccount = db_fetch($subaccounts))
					{
						//print_r($subaccount);echo '<br><br>';
						$tot = get_balance($subaccount["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
						$total=$total+($tot['debit']-$tot['credit']);
					}
					
					$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
					$total=$total+($tot['debit']-$tot['credit']);
					$ttlclass+=$total;
					$ttlclass2+=$total;
					$ttlall+=$total;
					$tableheader .= '<td align="right">'.price_format_pr(($total)).' </td>';
					//$tableheader .= '<td align="right">'.price_format_pr(($ttlclass2)).' </td>';
					
				}else{
					for($b=$blnfrom;$b<=$blnto;$b++){

						$ttl=0;
						$bln1=date('d/m/Y',strtotime($thnfrom.'-'.$b.'-01'));
						$bln2=date('d/m/Y',strtotime($thnto.'-'.$b.'-'.cal_days_in_month(CAL_GREGORIAN, $b, $thnto) ));
						
						$subaccounts=get_subaccount($account["account_code"]);
						while ($subaccount = db_fetch($subaccounts))
						{
							$tot2 = get_balance($subaccount["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $bln1, $bln2, false, true);
							$ttl=$ttl+($tot2['debit']-$tot2['credit']);	
						}
						$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $bln1, $bln2, false, true);
						$ttl=$ttl+($tot['debit']-$tot['credit']);
						$tableheader.='<td align="right">'.price_format_pr(($ttl)).' </td>';
						$ttlcol[$b]=@$ttlcol[$b]+$ttl;
						$ttlcol2[$b]=@$ttlcol2[$b]+$ttl;
						$ttlalls[$b]=@$ttlalls[$b]+$ttl;
						$ttlalls2[$b]=@$ttlalls2[$b]+$ttl;
					}
				}
				

				$tableheader .= '</tr>';

				if($account["account_code"]=='69004'){
					$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;</td><td><b>Total '.@$head[($a-1)].'</td>';
					if($_POST["tipe"]==0){
						$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlclass)).'</td>';
					}else{
						for($b=$blnfrom;$b<=$blnto;$b++){
							//$ttlcol[$b]=0;
							$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlcol[$b])).'</td>';
							$ttlcol[$b]=0;
						}	
						$tableheader .= '</tr>';
					}
					$tableheader .= '</tr>';
				}

				$n++;
			}
		}
	}
	$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;</td><td><b>Total Biaya Operasional</td>';
	if($_POST["tipe"]==0){
		$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlclass2)).'</td>';
		
	}else{
		for($b=$blnfrom;$b<=$blnto;$b++){
			$tableheader .= '<td align="right"><b>'.price_format_pr((@$ttlcol2[$b])).'</td>';
		}
	}
	$ttlcol=array();
	//$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlclass)).'</td>';
	$tableheader .= '</tr>';
	//<td align="right"><b>'.price_format_pr(($ttlall2)).'</td></tr>';
	$tableheader .= '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';

	$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;</td><td><b>Operating Profit</td>';
	if($_POST["tipe"]==0){
		$tableheader .= '<td align="right"><b>'.price_format_pr((@$ttlall-@$ttlall2)*(-1)).'</td>';
		
	}else{
		for($b=$blnfrom;$b<=$blnto;$b++){
			$tableheader .= '<td align="right"><b>'.price_format_pr((@$ttlalls[$b])*(-1)).'</td>';
		}
	}
	$ttlcol=array();
	//$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlclass)).'</td>';
	$tableheader .= '</tr>';
	//<td align="right"><b>'.price_format_pr(($ttlall+$ttlall2)).'</td></tr>';
	$tableheader .= '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';

	//Other Income
	$accounts=get_account_from_to('80000','89999');
	$nrow=n_account_from_to('80000','89999');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	
	$tableheader .= '<tr><td>&nbsp;</td><td><b>8-0000</td><td colspan="'.($cols).'"><b>Other Income</td></tr>';
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;

		$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;'.$account["account_code2"].'</td><td>'.$account["account_name"].'</td>';
		if($_POST["tipe"]==0){
			
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
			$total=$total+($tot['debit']-$tot['credit']);
			$ttlclass+=$total;
			$ttlclass2+=$total;
			$ttlall+=$total;
			$tableheader .= '<td align="right">'.price_format_pr(($total)*(-1)).'</td>';
			
		}else{
			for($b=$blnfrom;$b<=$blnto;$b++){

				$ttl=0;
				$bln1=date('d/m/Y',strtotime($thnfrom.'-'.$b.'-01'));
				$bln2=date('d/m/Y',strtotime($thnto.'-'.$b.'-'.cal_days_in_month(CAL_GREGORIAN, $b, $thnto) ));
				//$prev = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $bln1, $begin, false, true);
				//$curr = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $_POST['TransFromDate'], $bln2, false, true);
				$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $bln1, $bln2, false, true);
				$ttl=$ttl+($tot['debit']-$tot['credit']);
				$tableheader.='<td align="right">'.price_format_pr(($ttl)*(-1)).'</td>';
				$ttlcol[$b]=@$ttlcol[$b]+$ttl;
				$ttlcol2[$b]=@$ttlcol2[$b]+$ttl;
				$ttlalls[$b]=@$ttlalls[$b]+$ttl;
			}
		}

		$tableheader .= '</tr>';

		$n++;
	}
	$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;</td><td><b>Total Other Income</td>';
	if($_POST["tipe"]==0){
		$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlclass)*(-1)).'</td>';
		
	}else{
		for($b=$blnfrom;$b<=$blnto;$b++){
			$tableheader .= '<td align="right"><b>'.price_format_pr((@$ttlcol[$b])*(-1)).'</td>';
		}
	}
	$ttlcol=array();
	//$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlclass)).'</td>';
	$tableheader .= '</tr>';
	$tableheader .= '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';

	//Other Expense
	$accounts=get_account_from_to('90000','99999');
	$nrow=n_account_from_to('90000','99999');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	$ttlcol=array();
	$tableheader .= '<tr><td>&nbsp;</td><td><b>9-0000</td><td colspan="'.($cols).'"><b>Other Expense</td></tr>';
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;
		$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;'.$account["account_code2"].'</td><td>'.$account["account_name"].'</td>';
		if($_POST["tipe"]==0){
			
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
			$total=$total+($tot['debit']-$tot['credit']);
			$ttlclass+=$total;
			$ttlclass2+=$total;
			$ttlall+=$total;
			$tableheader .= '<td align="right">'.price_format_pr(($total)).'</td>';
			
		}else{
			for($b=$blnfrom;$b<=$blnto;$b++){

				$ttl=0;
				$bln1=date('d/m/Y',strtotime($thnfrom.'-'.$b.'-01'));
				$bln2=date('d/m/Y',strtotime($thnto.'-'.$b.'-'.cal_days_in_month(CAL_GREGORIAN, $b, $thnto) ));
				//$prev = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $bln1, $begin, false, true);
				//$curr = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $_POST['TransFromDate'], $bln2, false, true);
				$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $bln1, $bln2, false, true);
				$ttl=$ttl+($tot['debit']-$tot['credit']);
				$tableheader.='<td align="right">'.price_format_pr(($ttl)).'</td>';
				$ttlcol[$b]=@$ttlcol[$b]+$ttl;
				$ttlcol2[$b]=@$ttlcol2[$b]+$ttl;
				$ttlalls[$b]=@$ttlalls[$b]+$ttl;
			}
		}

		$tableheader .= '</tr>';

		$n++;
	}
	//$tableheader .= '<tr><td colspan="'.($blnto).'">&nbsp;</td></tr>';

	$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;</td><td><b>Total Other Expense</td>';
	if($_POST["tipe"]==0){
		$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlclass)).'</td>';
		
	}else{
		for($b=$blnfrom;$b<=$blnto;$b++){
			$tableheader .= '<td align="right"><b>'.price_format_pr((@$ttlcol[$b])*(-1)).'</td>';
		}
	}
	$ttlcol=array();
	//$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlclass)).'</td>';
	$tableheader .= '</tr>';
	$tableheader .= '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';


	$tableheader .= '<tr><td>&nbsp;</td><td>&nbsp;</td><td><b>Net Profit/(Loss)</td>';

	if($_POST["tipe"]==0){
		$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlall)*(-1)).'</td>';
		
	}else{
		for($b=$blnfrom;$b<=$blnto;$b++){
			$tableheader .= '<td align="right"><b>'.price_format_pr((@$ttlalls[$b])*(-1)).'</td>';
		}
	}
	$ttlcol=array();
	//$tableheader .= '<td align="right"><b>'.price_format_pr(($ttlclass)).'</td>';
	$tableheader .= '</tr>';
	$tableheader .= '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';

	$tableheader .='</center></table>';
	return $tableheader;
}
//----------------------------------------------------------------------------------------------------

if (get_post('Export')) 
{
	
	$header='
	<style>
	.inquirybg2{
		font-weight:bold !important;
		font-size:12px !important;
	}
	.tableheader{
		background-color:#EE7600 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0">
		<tr><td align="center">&nbsp;</td></tr>
		<tr><td style="width:2%;">&nbsp;&nbsp;</td>
			<td>
			<table border="0">
				<tr>
					<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;color:#ee3000;font-weight:bold;"><b>PT. Kresna Asset Management</b></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:20px;color:#ee3000;font-weight:bold;">Profit & Loss</td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;color:#ee3000;font-weight:bold;">'.@$blnnya.'</td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td>
				</tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 638px;">';
	div_start('balance_tbl');
	echo display_bs();
	div_end();
	echo '</td></tr></table>
		</td></tr>
		<tr><td></td><td align="center" style="background-color:#EE7600;border:#000 1px solid;">&nbsp;</td></tr>
		</table>';
}elseif(get_post('PrintPdf')){

// add a page
	$header='<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">Profit & Loss</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	$header.= '<tr><td style="width: 648px;">';
	$header.=display_bs();
	$header.= '</td></tr></table>';
$pdf->AddPage();
// output the HTML content
$html=$header;
//$html.=display_bs();
$pdf->writeHTML(@$html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_BS.pdf', 'I');	

}else{
	gl_inquiry_controls();
	div_start('balance_tbl');

	if (get_post('Show')) {
		echo '<center><table border="0" cellpadding="4" width="80%">
		<tr>		
			<td align="center" style="font-weight:bold;">Profit & Loss</td>
		</tr>
		<tr>
			<td align="center" style="font-weight:bold;">Periode '.@$blnnya.'</td>
		</tr>';
		echo '<tr>		
			<td align="center">';
		echo display_bs();
		echo '</td></tr></table></center>';
	}
	//if ($_POST['Show']) 
	//echo display_bs();
	div_end();
	end_page();
}

