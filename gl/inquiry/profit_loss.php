<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLANALYTIC';
$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/gl/includes/siap.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/admin/db/group_report_db.inc");
include_once($path_to_root . "/includes/db/siapjournal.php");

journal_siap_mis(null, $_SESSION['wa_current_user']->com_id);

$js = "";
if (user_use_date_picker())
	$js = get_js_date_picker();
if (get_post('Export')) 
{
	//include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"PL.xls\"");
	header("Cache-Control: max-age=0");
	
	
}else{
page(_($help_context = "Profit & Loss Drilldown"), false, false, "", $js);
}
$compare_types = array(
	_("Accumulated"),
	_("Period Y-1"),
	_("Budget")
);
//----------------------------------------------------------------------------------------------------
// Ajax updates

if (get_post('Show')) 
{
	$Ajax->activate('pl_tbl');
}

if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];	
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Compare"]))
	$_POST["Compare"] = $_GET["Compare"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["AccGrp"]))
	$_POST["AccGrp"] = $_GET["AccGrp"];

//----------------------------------------------------------------------------------------------------

function display_type ($type, $report = null, $typename, $from, $to, $begin, $end, $compare, $convert,
	$dimension=0, $dimension2=0, $compid, $drilldown, $path_to_root,$comps,$cv, $NoZero, $detail = true)
{
	global $levelptr, $k;
		
	$code_per_balance = 0;
	$code_acc_balance = 0;
	$per_balance_total = 0;
	$acc_balance_total = 0;
	unset($totals_arr);
	$totals_arr = array();
	$sum_total = 0;
	$totalperbalance = 0;
	$cekdataperbalance = 0;
	$sum_total2 = array();
	
	//print_r($convert);
	/*
	echo "-";
	print_r(date('Y-m-d',strtotime(sql2date($from))));
	echo "_";*/
	//Get Accounts directly under this group/type
	//print_r($compid);
	$result = get_report_coa_comp($type, $compid);
	//$result = get_report_coa($type, $report);	
	$total=array();
	$total2=array();
	while ($account=db_fetch($result))
	{
		
		//$per_balance = get_gl_trans_from_to($from, $to, $account["account_code"], $dimension, $dimension2);
		/*
		if ($compare == 2)
			$acc_balance = get_budget_trans_from_to($begin, $end, $account["account_code"], $dimension, $dimension2);
		else
			$acc_balance = get_gl_trans_from_to($begin, $end, $account["account_code"], $dimension, $dimension2);
			*/
		//if (!$per_balance && !$acc_balance)
		//	continue;
		
		//if ($drilldown && $levelptr == 0){
			if ($NoZero == 0) {
				for($a=0;$a<count($comps);$a++){
	        		$per_balance = get_gl_trans_from_to($from, $to, $account["account_code"], $comps[$a]['kode'], $dimension2);
					//echo $account["account_code"]."-".$per_balance."<br>";
	        		$cekdataperbalance += $per_balance;
	        	}
	        	//echo $cekdatanetbalance;
	        	if ($cekdataperbalance == 0) {
	        		//if (($account["account_code"] != '33') AND ($account["account_code"] != '32')) {
	        			continue;
	        		//}
	        	}
			}
			if($account["account_code"] == '561')
				continue;


			$cekdataperbalance = 0;
			$nilaicoasiap = 0;
			$nilaicoanosiap = 0;
			$a = 0;
			/*if ($detail) {
				$url = "<a href='$path_to_root/gl/inquiry/gl_account_inquiry.php?TransFromDate=" 
					. $from . "&TransToDate=" . $to . "&Dimension=" . $dimension . "&Dimension2=" . $dimension2 
					. "&account=" . $account['account_code'] . "'>" . $account['account_code'] 
					." ". $account['account_name'] ."</a>";				
					
				start_row("class='stockmankobg'");
				if(@$_POST['AccCode']==1){
					label_cell('&nbsp;&nbsp;&nbsp;&nbsp;'.$account['account_code2']." ".$account['account_name']);
				}else{
					label_cell('&nbsp;&nbsp;&nbsp;&nbsp;'.$account['account_name']);
				}
			}*/
			//$ceakaccount = substr($account["account_code"], 0,1);

			//for($a=0;$a<count($comps);$a++){
				/*$get_gl_posting = get_gl_posting($from, $to, $account["account_code"], $comps[$a]['kode'], $dimension2);
				print_r($get_gl_posting);
				if ($get_gl_posting == 1)*/
				//print_r($account['account_code']);
				if($account['account_code'] == '42201'){
					//echo $account['account_code'];
					$getcoasiap = "SELECT amount FROM 0_gl_trans WHERE account = '".$account['account_code']."' AND memo_ LIKE '%SIAP%' AND dimension_id = '".$comps[$a]['kode']."' AND tran_date ='".sql2date($to)."' ORDER BY tran_date DESC";
					//echo $sql;
					//$endfiscallast = date('d/m/Y', strtotime('-1 year', strtotime(sql2date(end_fiscalyear()))));
					$datacoasiap = db_query($getcoasiap);
					while ($amount2 = db_fetch($datacoasiap)) {
						$nilaicoasiap = $amount2['amount'];
					}
					$getcoanosiap = "SELECT amount FROM 0_gl_trans WHERE account = '".$account['account_code']."' AND memo_ NOT LIKE '%SIAP%' AND dimension_id = '".$comps[$a]['kode']."' AND (tran_date BETWEEN '".sql2date($from)."' AND '".sql2date($to)."') ORDER BY tran_date DESC";
					$datacoanosiap = db_query($getcoanosiap);
					while ($amount3 = db_fetch($datacoanosiap)) {
						$nilaicoanosiap += $amount3['amount'];
					}
					$per_balance = $nilaicoanosiap + $nilaicoasiap;
					//echo $nilaicoanosiap ."+". $nilaicoasiap;
				}else{
					$per_balance = get_gl_trans_from_to($from, $to, $account["account_code"], $comps[$a]['kode'], $dimension2);
				}
				//echo $per_balance."+";
				
				/*if($type == 3){
					$per_balance = $per_balance * -1;
				}*/
				
				//var_dump($convert);
				$totalperbalance += $per_balance * $convert;
				if ($cv == 1) {
					if (@$comps[$a]['kode_siap']!='') {
						$tot2 = get_tb_ims($comps[$a]['kode_siap'],$account["account_code"],date2sql($_POST["TransToDate"]));
						//echo $tot2;
						$totalperbalance += $tot2;
					}				
				}
				/*if ($detail) {
					amount_cell($totalperbalance);
				}*/
				$sum_total += ($totalperbalance);
				@$total[$a]+= $totalperbalance;
				$totalperbalance = 0;
			//}
			//$totalperbalance = 0;
			/*if ($detail) {
				//amount_cell($sum_total);
				end_row();
			}*/
			$sum_total = 0;
			//$totalperbalance = 0;
			//$tot2 = 0;
			//amount_cell($acc_balance * $convert);
			//amount_cell(Achieve($per_balance, $acc_balance));
			
		//}
			
		$code_per_balance += @$per_balance;
		@$code_acc_balance += $acc_balance;
	}

	$levelptr = 1;
	
	//Get Account groups/types under this group/type
	/*$result = get_account_types(false, false, $type);
	while ($accounttype=db_fetch($result))
	{	

		for($a=0;$a<count($comps);$a++){
			$totals_arr = display_type($accounttype["id"], $accounttype["name"], $from, $to, $begin, $end, 
				$compare, $convert, $comps[$a]['kode'], $dimension2, $drilldown, $path_to_root,$comps);
			$per_balance_total += $totals_arr[0];
			$acc_balance_total += $totals_arr[1];
			$total[$a]+=$totals_arr[0];
		}
	}
	//Display Type Summary if total is != 0 
	//if (($code_per_balance + $per_balance_total + $code_acc_balance + $acc_balance_total) != 0)
	//{
		//if ($drilldown && $type == $_POST["AccGrp"]){		
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_('Total') . " " . $typename);

			for($a=0;$a<count($comps);$a++){
				amount_cell((@$total[$a] + @$total2[$a]) * $convert);
				$sum_total += ((@$total[$a] + @$total2[$a]) * $convert);
			}
			amount_cell($sum_total);
			//amount_cell(($code_acc_balance + $acc_balance_total) * $convert);
			//amount_cell(Achieve(($code_per_balance + $per_balance_total), ($code_acc_balance + $acc_balance_total)));
			end_row();
		//}
		//START Patch#1 : Display  only direct child types
		$acctype1 = get_account_type($type);
		$parent1 = $acctype1["parent"];
		//if ($drilldown && $parent1 == $_POST["AccGrp"])
		//END Patch#2		
		//elseif ($drilldown && $type != $_POST["AccGrp"])
		//{	
			$url = "<a href='$path_to_root/gl/inquiry/profit_loss.php?TransFromDate=" 
				. $from . "&TransToDate=" . $to . "&Compare=" . $compare . "&Dimension=" . $dimension . "&Dimension2=" . $dimension2
				. "&AccGrp=" . $type ."'>" . $type . " " . $typename ."</a>";
				
			alt_table_row_color($k);
			label_cell($url);
			for($a=0;$a<count($comps);$a++){
				amount_cell(($total[$a] + $total2[$a]) * $convert);
				$sum_total += (($total[$a] + $total2[$a]) * $convert);
			}	
			amount_cell($sum_total);
			//amount_cell(($code_per_balance + $per_balance_total) * $convert);
			//amount_cell(($code_acc_balance + $acc_balance_total) * $convert);
			//amount_cell(Achieve(($code_per_balance + $per_balance_total), ($code_acc_balance + $acc_balance_total)));
			end_row();
		//}
	//}
	*/
	
	$totals_arr[0] = $code_per_balance + $per_balance_total;
	$totals_arr[1] = $code_acc_balance + $acc_balance_total;
	$totals_arr[3] = $total;
	//print_r($comps);
	return $totals_arr;
}	
	
function Achieve($d1, $d2)
{
	if ($d1 == 0 && $d2 == 0)
		return 0;
	elseif ($d2 == 0)
		return 999;
	$ret = ($d1 / $d2 * 100.0);
	if ($ret > 999)
		$ret = 999;
	return $ret;
}

function inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
	echo "<center>";
    start_table(TABLESTYLE_NOBORDER);
	$date = today();
	if (!isset($_POST['TransToDate']))
		$_POST['TransToDate'] = end_month($date);
	if (!isset($_POST['TransFromDate']))
		$_POST['TransFromDate'] = add_days(end_month($date), -user_transaction_days());
    //date_cells(_("From:"), 'TransFromDate');
	date_cells(_("Date:"), 'TransToDate');
	if ($dim >= 1)
		dimensions_list_cells(_("Dimension")." 1:", 'Dimension', null, true, " ", false, 1);
	if ($dim > 1)
		dimensions_list_cells(_("Dimension")." 2:", 'Dimension2', null, true, " ", false, 2);
	//level_list(_("Level"), 'Level', null, true, " ", false, 1);
	echo "<td>Level :</td>";
	echo "<td>";
	echo '<select name="level" id="level">';
	if ($_SESSION["wa_current_user"]->com_id != 0) {
		$sql = "SELECT lvl from 0_companies where id = ".$_SESSION["wa_current_user"]->com_id;
		$result =db_query($sql);
		$lvl = db_fetch($result);
		$lvl = $lvl['lvl'] + 1;
	}else{
		$lvl = 1;
	}
		echo '<option value="-1">Default</option>';
		//echo '<option value="0">All</option>';
	/*for ($i=$lvl; $i <= 5 ; $i++) { 
		echo '<option value="'.$i.'">'.$i.'</option>';
	}*/
	echo '</select>';
	echo "</td>\n";
	echo "</tr>";
	echo "<tr>";
	/*check_cells(_("With zero values"), 'NoZero', null);
	check_cells(_("Account Code"), 'AccCode', null);*/
	/*check_cells(_("Calculate with SIAP"), 'CalcSiap', null);*/
	echo "</tr>";
	echo '<tr style="height: 40px !important;">';
	echo '<td colspan=9 align=center><button class="ajaxsubmit" type="submit" aspect="default" name="Show" style="margin-right: 15px !important;" id="Show" value="Show"><img src="../../themes/default/images/ok.gif" height="12" alt=""><span>Show</span></button>';
	//submit_cells('Show',_("Show"),'','', 'default');
	echo '<input type="submit" class="export" name="Export" id="Export" Value="Export to Excel"></td>';
	/*echo '<td></td>';
	echo '<td></td>';
	echo '</tr>';
	*/
	echo "</tr>";
	end_table();
	echo "</center>";
	//hidden('TransFromDate');
	hidden('AccGrp');
}

//----------------------------------------------------------------------------------------------------
function get_tb_ims($kodesiap,$accode,$date){
	$datatbims = get_tb2_ims($date);
	
	if(isset($datatbims[$date][$kodesiap][$accode])){
		return $datatbims[$date][$kodesiap][$accode];
	}else{
		return 0;
	}

}

function check_subcomp($subcomid){
	$sql = "SELECT count(*) as nilai from 0_companies where parent = ".$subcomid;
	//$comps=array();
	$result =db_query($sql);
	$count=db_fetch($result);
	//var_dump($comps);exit;
	return $count;
}

function subcomp($parid){
	$sql = "SELECT comp.id,comp.kode, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = id) as nsub
	FROM ".TB_PREF."companies comp
	where comp.parent=".$parid;
	$comps=array();
	$subcomp=array();
	if($result=db_query($sql)){
		while($row=db_fetch($result)){
			$check_subcomp = check_subcomp($row['id']);
			if($check_subcomp['nilai'] > 0){
				$subcomp = subcomp($row['id']);
				$comps = array_merge($comps,$subcomp);
			}else{
				$comps[] = $row;	
			}
		}
	}
	//var_dump($comps);exit;
	return $comps;
}
function display_profit_and_loss($compare)
{
	global $path_to_root, $compare_types;
	$totaltype2 = 0.0;
	$totalparse = array();
	$totaltype3 = 0.0;

	if (!isset($_POST['Dimension']))
		$_POST['Dimension'] = 0;
	if (!isset($_POST['Dimension2']))
		$_POST['Dimension2'] = 0;
	$dimension = $_POST['Dimension'];
	$dimension2 = $_POST['Dimension2'];

	$to = $_POST['TransToDate'];
	$from  = begin_fiscalyear();
	//echo $from;
	if (date('Y',strtotime(sql2date($to))) < date('Y',strtotime(sql2date($from)))) {
	 	$from = date('d/m/Y', strtotime('-1 year', strtotime(sql2date($from))));
	}
	if (isset($_POST["AccGrp"]) && (strlen($_POST['AccGrp']) > 0))
		$drilldown = 1; // Deeper Level
	else
		$drilldown = 0; // Root level
	
	if ($compare == 0 || $compare == 2)
	{
		$end = $to;
		if ($compare == 2)
		{
			$begin = $from;
		}
		else
			$begin = begin_fiscalyear();
	}
	elseif ($compare == 1)
	{
		$begin = add_months($from, -12);
		$end = add_months($to, -12);
	}
	
	div_start('pl_tbl');

	start_table(TABLESTYLE, "width='90%'");

	$comps=array();
	$subcomp=array();
	if (@$_POST['level'] == '') {
		$sql = "SELECT comp.id,comp.kode,comp.nama,comp2.nama as namaparent, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = ".$_SESSION["wa_current_user"]->com_id.") as nsub
		FROM ".TB_PREF."companies comp
		LEFT JOIN ".TB_PREF."companies comp2 ON comp2.id = comp.parent
		where comp.id=".$_SESSION["wa_current_user"]->com_id;
		if(@$_SESSION["wa_current_user"]->com_id=='0')
		$sql = "SELECT comp.id,comp.kode,comp.nama,comp.parent,comp2.nama as namaparent, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = comp.id ) as nsub
		FROM ".TB_PREF."companies comp
		LEFT JOIN ".TB_PREF."companies comp2 ON comp2.id = comp.parent
		order by kode asc";
		
		if($result=db_query($sql)){
			while($row=db_fetch($result)){
				$dim=$row['kode'];
				if($_POST['Dimension']!=0)
				$dim=$_POST['Dimension'];
				//print_r($comps);
				if($dim==$row['kode']){
					/*if (($row['lvl'] == 0) || ($row['lvl'] == 1)) {
						$comps[]=$row;
					}else{*/
					
					//}
					if(@$row['nsub'] > 0){
						$subcomp=subcomp($row['id']);
					}else{
						//if(!in_array($comps,$row['kode'])){
						$comps[]=$row;
						//}					
					}
				}
				
			}
			if (($_SESSION["wa_current_user"]->com_id != 0) OR ($_POST['Dimension']!=0)) {
				$comps=array_merge($comps,$subcomp);
			}
		}
	}else{
		if ($_POST['level'] == '0') {
			$_POST['level'] = 5;
		}

		if ($_POST['Dimension']!=0) {
			$sql = "select id,lvl from ".TB_PREF."companies where kode = '".$_POST['Dimension']."'";
			$result = db_query($sql);
			while ($row=db_fetch($result)) {
				$id = $row['id'];
				$lvl = $row['lvl'];
			}
			if ($_POST['level'] == $lvl) {
				$lvl = $lvl;
			}else{
				$lvl = $_POST['level'];
			}

			if ($_POST['level'] == -1) {
				$sql = "select id,lvl from ".TB_PREF."companies where id = '".$id."'";
				$result = db_query($sql);
				while ($row=db_fetch($result)) {
					$lvl = $row['lvl'];
				}
				$lvl = $lvl + 1;
			}
		}else{
			$id = $_SESSION["wa_current_user"]->com_id;
			$lvl = $_POST['level'];

			if ($_POST['level'] == -1) {
				$sql = "select id,lvl from ".TB_PREF."companies where id = '".$id."'";
				$result = db_query($sql);
				while ($row=db_fetch($result)) {
					$lvl = $row['lvl'];
				}
				$lvl = $lvl + 1;
			}
		}
		if ($_SESSION["wa_current_user"]->com_id == 0 AND $_POST['Dimension'] == 0 AND $_POST['level'] == -1) {
			$id = $_SESSION["wa_current_user"]->com_id;
			$lvl = 2;
		}

		$sql = "select  lvl, nama, id, kode, parent, CONCAT(kode,' ', nama) as ref
				from    (select * from ".TB_PREF."companies
				         order by parent, id) comps_sorted,
				        (select @pv := ".$id.") initialisation
				where   (CASE WHEN NOT EXISTS (SELECT id from ".TB_PREF."companies where parent = @pv) THEN
						find_in_set(id, @pv) ELSE find_in_set(parent, @pv) END)
				and 	length(@pv := concat(@pv, ',', id))
				and		(lvl BETWEEN lvl AND ".$lvl.")
				group by parent";
		
		if($result=db_query($sql)){
			while($row=db_fetch($result)){
				$comps[]=$row;
			}
		}
	}

	/*$tableheader =  "<tr><td class='tableheader'>" . _("Group/Account Name") . "</td>";
	for($a=0;$a<count($comps);$a++){
		$tableheader .= '<td class="tableheader">'.$comps[$a]["ref"].'</td>';
	}
	$tableheader .= '<td class="tableheader">Total</td>';
    //$tableheader .="<td class='tableheader'>" . _("Period") . "</td>";
    $tableheader .="</tr>";	*/
	
	if (!$drilldown) //Root Level
	{
		$salesper = 0.0;
		$salesacc = 0.0;	
		$totaltype = 0.0;	
		$totalpendapatan = array();
		$totalbiaya = array();
		$totallabausaha = array();
		$totallainlain = array();
		$totallabasebelumpajak = array();
		$manfaatpajak = array();
		$totallaba = array();
		$k = 0;
		//Get classes for PL
		$report = 2;

		$getclassdb = get_group_data($comps[0]['id'], 0, FALSE, 2, '0', TRUE);
		//$classhead = array('', 'Pendapatan Utama', 'Beban Usaha', 'Pendapatan (Beban) Lain-Lain');
		//$result = ;
		while ($getclass = db_fetch($getclassdb)) {
			//print_r($getclass['class_id']);
			$class_per_total = 0;
			$class_acc_total = 0;
			$total2=array();
			$data = array();
			$a = 0;
			$k = 0;
			$convert = get_class_type_convert($getclass['class_id']); 		
			if (check_value('CalcSiap')) {
				$check_value = 1;
			}else{
				$check_value = 0;
			}
			$ctype = $getclass['class_id'];
			$cekTypeTotal = 0.0;

			$getgroup = get_group_data($comps[0]['id'], 0, FALSE, 2, '0', FALSE, $getclass['class_id']);
			$getgroup2 = get_group_data($comps[0]['id'], 0, FALSE, 2, '0', FALSE, $getclass['class_id']);
			$countgetgroup = db_num_rows($getgroup);
			//print_r($countgetgroup);
			if ($countgetgroup != 0) {
			
				$total=array();
				
				$c = 1;
				$i = 0;
				$t = 0;
				
				//print_r($data);
				while ($group2=db_fetch($getgroup2)){
					$TypeTotal = display_type($group2['id'],$report, null, $from, $to, $begin, $end, $compare, $convert, 
							@$comps[0]['kode'], $dimension2, @$comps[0]['id'], @$drilldown,  $path_to_root, $comps, $check_value, @$_POST['NoZero'], false);
					if (@$TypeTotal[3][0] != NULL) {
						$data[] = $c;
						$c++;
					}
				}
				$countdata = count($data);
				while ($group = db_fetch($getgroup))
				{
					
					
					//$getsubgroup = get_group_data($comps[0]['id'], $group['id'], TRUE, 2, $group['id']);
					//$getsubgroup2 = get_group_data($comps[0]['id'], $group['id'], TRUE, 2, $group['id']);
					//$number = db_num_rows($getsubgroup);
					
					//while ($subgroup=db_fetch($getsubgroup)){
						
						//echo @$data[$i]."=".$countdata."<br>";
					if ($t==0) {
							table_section_title($getclass['class'],(1+count($comps)));	
					}	
					//for($a=0;$a<count($comps);$a++){
					
					$TypeTotal = display_type($group['id'],$report, null, $from, $to, $begin, $end, $compare, $convert, 
						@$comps[0]['kode'], $dimension2, @$comps[0]['id'], @$drilldown,  $path_to_root, $comps, $check_value, @$_POST['NoZero'], false);
					
					if (@$TypeTotal[3][0] != NULL) {
						
						start_row();
						label_cell($group['groupname']);
						if ($data[$i]==$countdata) {
							amount_cell(@$TypeTotal[3][0],false,'class="totalgroup"');
						}else{
							amount_cell(@$TypeTotal[3][0]);
						}
						@$total2[0]+=@$TypeTotal[3][0];
						end_row();
						//echo $subgroup['groupname']."---".$number."---".$i."<br>";	
						$i++;
					}
					$t++;
						//}
					/*}*/

					
				}
				if ($getclass['class_id'] != 4) {
					//print_r($group['class_id'] );
					start_row("class='inquirybg' style='font-weight:bold'");
					label_cell(_('Total') . " " . $getclass['class']);
					//print_r($total2[$a]);
					for($a=0;$a<count($comps);$a++){	
						if($getclass['class_id'] == 6){
							@$totalbiaya[$a] += @$total2[$a];
							amount_cell(@$totalbiaya[$a]);
						}
						if($getclass['class_id'] == 10){
							@$totallainlain[$a] += @$total2[$a] * -1;
							amount_cell(@$totallainlain[$a]);
						}
						
						$totaltype2 += @$total2[$a] * -1;
						@$total[$a]+= $total2[$a] * -1;
					}
					$totaltype2 = 0;
					
					end_row();
					alt_table_row_color($k);
					if ($getclass['class_id'] == 6) {
						start_row("class='inquirybg' style='font-weight:bold'");
						label_cell(_('TOTAL LABA USAHA'));
						
						for($a=0;$a<count($comps);$a++){	
							$totallabausaha[$a] = @$totalpendapatan[$a] - @$totalbiaya[$a]; 
							amount_cell($totallabausaha[$a],true);
						}
						
						end_row();
						alt_table_row_color($k);
					}
				}else{

					start_row("class='inquirybg' style='font-weight:bold'");
					label_cell(_('Total') . " " . $getclass['class']);
					//print_r($total2[$a]);
					for($a=0;$a<count($comps);$a++){	
						if ($getclass['class_id'] == 4) {
							@$totalpendapatan[$a] += @$total2[$a];
							amount_cell(@$totalpendapatan[$a]);
						}
					}
					
					end_row();
					alt_table_row_color($k);
				}
				

			}else{
				display_error( _("Please setup report group and report account group on setup menu"));
			}
		}
		start_row("class='inquirybg' style='font-weight:bold'");
		label_cell(_('LABA SEBELUM PAJAK'));
		
		for($a=0;$a<count($comps);$a++){	
			$totallabasebelumpajak[$a] = @$totallabausaha[$a] + @$totallainlain[$a];
			amount_cell(@$totallabasebelumpajak[$a],true);
		}
		end_row();
		alt_table_row_color($k);
		

		$manfaatpajak[0] = get_gl_trans_from_to($from, $to, 561, $comps[0]['kode'], $dimension2);
		if($manfaatpajak[0] != 0){
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_('MANFAAT (BEBAN) PAJAK'));
			amount_cell($manfaatpajak[$a],true);
			end_row();
			alt_table_row_color($k);
		}

		start_row("class='inquirybg' style='font-weight:bold'");
		label_cell(_('LABA TAHUN BERJALAN'));
		for($a=0;$a<count($comps);$a++){	
			@$totallaba[$a] += @$totallabasebelumpajak[$a] + @$manfaatpajak[$a];
			amount_cell(@$totallaba[$a],true);
		}
		//amount_cell($totaltype3);
		$totaltype3 = 0;
		end_row();
		//$classresult = get_report_class($report);
			

	}

	end_table(); // outer table
	div_end();
}


//----------------------------------------------------------------------------------------------------
if (get_post('Export')) 
{
	if ($_SESSION["wa_current_user"]->com_id == 0) {
		$sql = "SELECT comp.nama FROM ".TB_PREF."companies comp
		where comp.id=1";
	}else{
		$sql = "SELECT comp.nama FROM ".TB_PREF."companies comp
		where comp.id=".$_SESSION["wa_current_user"]->com_id;
		
	}

	$result = db_query($sql);
	$fetch = db_fetch($result);
	$dateexport = date('d',strtotime(sql2date($_POST['TransToDate'])));
	$month = array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
	$monthexport = date('m',strtotime(sql2date($_POST['TransToDate'])));
	$monthexport = $month[intval($monthexport)];
	$yearexport = date('Y',strtotime(sql2date($_POST['TransToDate'])));
	$date = $dateexport." ".$monthexport." ".$yearexport;
	$tahuntoexport = date('Y',strtotime(sql2date($_POST['TransToDate'])));
	$header='
	<style>
	table{
		font-family:"Times New Roman";
		font-size:10pt !important;
	}
	.inquirybg{
		font-weight:normal !important;
		font-size:10pt !important;
	}
	.tableheader{
		margin-top = 100px !important;
		font-size:10pt !important;
	}
	.tablestyle{
		font-size:10pt !important;
	}
	td{
		vertical-align:top !important;
	}
	.link{
		pointer-events: none;
		cursor: default;
		text-decoration: none;
		color: black;
	}
	.headle{
		text-decoration: underline;
	}
	.class1{
		text-decoration: underline;
	}
	.totalle2, .totalaset{
		border-bottom:0.1em double black !important;
	}
	.totalgroup{
		border-bottom:0.1em solid black !important;
	}	

	</style>
	<table border="0">
		<tr><td align="center">&nbsp;</td></tr>
		<tr><td style="width:2%;">&nbsp;&nbsp;</td>
			<td>
			<table border="0">
				<tr>
					<td width="100%" style="font-size:10pt;text-transform: uppercase;"><b>'.strtoupper($fetch['nama']).'</b></td>
				</tr>
				
				<tr>
					<td style="font-size:10pt;">LAPORAN LABA RUGI</td>
				</tr>
				<tr style="border-bottom:1px solid black">
					<td style="font-size:10pt;" align="left">'.@$date.'</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<table border="0">
							<tr>
								<td style="font-size:10pt;" align="right">&nbsp;</td>
								<td style="font-size:10pt;border-bottom:1px solid black;" align="center">'.@$tahuntoexport.'</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>';

	echo $header;
	echo '<tr><td style="width: 600px;">';
	div_start('pl_tbl');
	echo display_profit_and_loss(get_post('Compare'));
	div_end();
	echo '</td></tr></table>
	</td></tr>
	</table>';
}else{

start_form();

inquiry_controls();
div_start('pl_tbl');
if (get_post('Show')) {
	echo display_profit_and_loss(get_post('Compare'));
}
div_end();

end_form();

end_page();

}