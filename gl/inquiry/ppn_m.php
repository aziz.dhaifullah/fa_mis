<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/siar.inc");
include_once($path_to_root . "/gl/includes/db/gl_db_banking.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

	//include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"Template Import PPN M.xls\"");
	header("Cache-Control: max-age=0");
	
//----------------------------------------------------------------------------------------------------
// Ajax updates
//


if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format(0);


	if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
		$tgl1=$_POST['TransFromDate'];
		$tgl2=$_POST['TransToDate'];		
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=date('Y');
		$thnto=date('Y');
		$tgl1='01/'.$blnfrom.'/'.$thnfrom;
		$tgl2='31/'.$blnto.'/'.$thnto;		
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

//----------------------------------------------------------------------------------------------------

function show_results()
{
	global $Refs,$path_to_root, $systypes_array;

	/*
	start_table(TABLESTYLE2, "width='90%'", 10);
	
	$th = array(_("No"), _("Kode"), _("NPWP"),  _("Nama"));
	    			
	table_header($th);
	*/


	$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
	$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
	$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
	$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime(date2sql($_POST['TransToDate'])));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime(date2sql($_POST['TransToDate'])));
		}
	}

	$begin=date('d/m/Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));

	$tableheader='<center><table class="tablestyle" cellpadding="6" border="1" width="98%" style="font-size:10px !important;">';

	$sql="select a.*,d.*,c.reference
from 0_gl_trans a
inner join 0_fp_trans c on c.type=a.type and c.id=a.type_no
left join 0_suppliers d on a.person_id=d.supplier_id
where 
a.account like '11703' 
and c.reference!=''
and a.tran_date between '".date2sql($_POST['TransFromDate'])."' and '".date2sql($_POST['TransToDate'])."' 
order by a.counter asc";
	//$sql = get_pajak_masukan(-1, $tgl1,$tgl2);
	$result=db_query($sql, '');

	$no=1;$a=0;
	$ttljml=0;$ttlbayar=0;$ttlselisih=0;
	while($mfees = db_fetch_assoc($result))
	{
			//print_r($mfees);echo'<br>';

			$nfp=@$mfees['reference'];
			$nfp=str_replace('-', '', @$mfees['reference']);
			$nfp=str_replace('.', '', @$nfp);
			$nama=@$mfees['supp_name'];
			$npwp=str_replace('-', '', @$mfees['gst_no']);
			$npwp=str_replace('.', '', @$npwp);
			$alamat=@$mfees['address'];

			$jasa=@$mfees['jasa'];

			$masa=date('m',strtotime($mfees['tran_date']));
			$thn=date('Y',strtotime($mfees['tran_date']));
			$tgl=date('d/m/Y',strtotime($mfees['tran_date']));

			$dpp=abs($mfees['amount']*10);
			$ppn=abs($mfees['amount']);

			$memo=($mfees['memo_']);

			$rom=array('I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII');
			$noinv=@$mfees['kode2'].'/AFT-SC-M'.$rom[intval($masa)-1].'/'.$thn;
			$nomor='Management Fee Invoice No. '.$noinv;
			$tableheader.='<tr class="oddrow">';
			$tableheader .="
				<td align='left'>FM</td>
				<td align='left'>01</td>
				<td align='right'>0</td>
				<td align='left'>&nbsp;" . @$nfp . "&nbsp;</td>
				<td align='left'>&nbsp;" . @$masa . "</td>
				<td align='left'>" . @$thn . "</td>
				<td align='left'>" . @$tgl . "</td>
				<td align='left'>&nbsp;" . @$npwp . "&nbsp;</td>
				<td align='left'>" . @$nama . "</td>";
			$tableheader .="
				<td align='left'>" . @$alamat . "</td>
				<td align='right'>" . @$dpp . "</td>
				<td align='right'>" . @$ppn . "</td>
				<td align='right'>0</td>
				<td align='right'>1</td>
				<td align='left'>&nbsp;</td>";
			$tableheader .="</tr>";

			$no++;
			$a++;
    }

    return $tableheader;
}
//----------------------------------------------------------------------------------------------------
	
	
	echo show_results();


