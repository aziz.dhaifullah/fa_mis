<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();


//----------------------------------------------------------------------------------------------------
// Ajax updates
//

if (isset($_GET["Show"]))
	$_POST["Show"] = $_GET["Show"];
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}

if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format_pr(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format_pr(0);

if (get_post('Export')) 
{
	//include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"Disbursement.xls\"");
	header("Cache-Control: max-age=0");
	
	
}else{
	page(_($help_context = "Export To MYOB (Disbursement)"), false, false, '', $js);
}

//----------------------------------------------------------------------------------------------------

function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER);
	start_row();
    //gl_all_accounts_list_cells(_("Account:"), 'account', null, false, false, _("All Accounts"));
	$accounts = get_chart_accounts_search('');
	
	echo "<td>"._("From").":</td>\n";
    echo "<td>";
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
    end_row();
    echo '<tr>';
	echo "<td></td>\n";
	date_cells(_(""), 'TransFromDate', '', null, -user_transaction_days());
	date_cells(_(" "), 'TransToDate');
	echo '<tr>';
	end_table();

	start_table(TABLESTYLE_NOBORDER);
	start_row();
	//small_amount_cell_crs(_("Amount min:"), 'amount_min', null, " ");
	//small_amount_cell_crs(_("Amount max:"), 'amount_max', null, " ");
	submit_cells('Show',_("Show"),'','', 'default');
	// echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel"></td>';
	echo '
	<style>
		.export{
			align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    font-size: 11px;
		    border: 1px #0066cc solid;
		    padding: 1px 4px;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "General_Ledger",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
	end_row();
	end_table();

	echo '<hr>';
    //end_form();
}

//----------------------------------------------------------------------------------------------------

function show_results()
{
	global $path_to_root, $systypes_array;

	if (!isset($_POST["account"]))
		$_POST["account"] = null;

	$act_name = $_POST["account"] ? get_gl_account_name($_POST["account"]) : "";
	$dim = get_company_pref('use_dimension');

    /*Now get the transactions  */
    if (!isset($_POST['Dimension']))
    	$_POST['Dimension'] = 0;
    if (!isset($_POST['Dimension2']))
    	$_POST['Dimension2'] = 0;
   	
	$accs=@$_POST["accou"];
	//if(@$_POST["accs"]!='')
    $accs2=@$_POST["accs"];
    //print_r($accs2);
    $accont=null;

    if(count($accs2)>0){
    	$accont='(';
    	for($a=0;$a<count($accs2);$a++){
    		if($a>0 and $a<=count($accs2))
    		$accont.=',';
    		$accont.="'".$accs2[$a]."'";
    	}
    	$accont.=')';
    }else{
	    if(count($accs)>0){
	    	$accont='(';
	    	for($a=0;$a<count($accs);$a++){
	    		echo '<input type="hidden" name="accs[]" value="'.$accs[$a].'">';
	    		if($a>0 and $a<=count($accs))
	    		$accont.=',';
	    		$accont.="'".$accs[$a]."'";
	    	}
	    	$accont.=')';
	    }
	}

    $accont;
	$result = get_all_coa2($accont);

	$colspan = ($dim == 2 ? "6" : ($dim == 1 ? "5" : "4"));

	//if ($_POST["account"] != null)
	//	display_heading($_POST["account"]. "&nbsp;&nbsp;&nbsp;".$act_name);

	// Only show balances if an account is specified AND we're not filtering by amounts
	$show_balances = $_POST["account"] != null && 
                     input_num("amount_min") == 0 && 
                     input_num("amount_max") == 0;
		
	//start_table(TABLESTYLE,'width="90%"',6);
	
	$tableheader='<div id="trans_tbl">';
	if (get_post('Export'))
	$tableheader.='<center><table class="tablestyle" width="90%" cellpadding="6" border="0">';
	else
	$tableheader.='<center><table class="tablestyle" width="90%" cellpadding="6" border="1">';
	$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
	//$first_cols = array(_("Type"), _("#"), _("Date"));
	//$tableheader.='<td align="center">Type</td>';
	$tableheader.='<td align="center" style="width:10px !important;">&nbsp;&nbsp;</td>';
	$tableheader.='<td align="center">#</td>';
	$tableheader.='<td align="center">Src</td>';
	$tableheader.='<td align="center">Date</td>';
	$tableheader.='<td align="center" width="150px;">Memo</td>';

	$tableheader.='<td align="center">Deposit</td>';
	$tableheader.='<td align="center">Withdrawal</td>';
	$tableheader.='<td align="center">Balance</td>';

	if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=date('Y');
		$thnto=date('Y');
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

	$from=date('d/m/Y',strtotime(date2sql($_POST['TransFromDate'])));
	$to=date('d/m/Y',strtotime(date2sql($_POST['TransToDate'])));
	//table_header($th);  
	echo $tableheader;
	if ($_POST["account"] != null && is_account_balancesheet($_POST["account"]))
		$begin = "";
	else
	{
		$begin = get_fiscalyear_begin_for_date($from);
		if (date1_greater_date2($begin, $from))
			$begin = $from;
		$begin = add_days($begin, -1);
	}
	$begin = add_days($from, -1);

	$bfw = 0;

	
	$running_total = $bfw;
	$j = 1;
	$k = 0; //row colour counter

	$ttlall1=0;
	$ttlall2=0;
	$ttlall3=0;
	while ($myrow0 = db_fetch($result))
	{
		$ttl1=0;
		$ttl2=0;
		//beg bal
		$open=get_balance($myrow0["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $from, $begin, false, true);
		$curr = get_balance($myrow0["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $from, $to, true, true);
		$bfw=$open['balance'];
		if (check_value("NoZero") && !$open['balance'] && !$curr['balance'])
			continue;
		//$bfw = get_gl_balance_from_to($begin, $from, $myrow0["account_code"], $_POST['Dimension'], $_POST['Dimension2']);
	    //var_dump($bfw);
		//if (@$_POST['NoZero']=='' && $bfw<>0)
		//{
	    	start_row("");
	    	label_cell("&nbsp;", "colspan=8");
	    	end_row();
	    	start_row("class='inquirybg2'");
	    	label_cell("<b>".$myrow0["account_code2"].'</b>');
	    	label_cell("<b>".$myrow0["account_name"]."</b>", "colspan=7");
	    	end_row();
	    //}
		
		$result2 = get_gl_transactions2($from, $to, -1,
    	$myrow0["account_code"], $_POST['Dimension'], $_POST['Dimension2'], null,
    	input_num('amount_min'), input_num('amount_max')); 
		$running_total=$bfw;
		$ttlall3+=$bfw;
		/*
		if($bfw>0)
			$ttl1=$bfw;
		if($bfw<0)
			$ttl2=$bfw;
		*/
		$ttl1=0;$ttl2=0;
		while ($myrow = db_fetch($result2))
		{
    	
	    	
			if(($myrow["type"])!=1)
				continue;
			//if(($myrow["amount"])>0)
			//	continue;
	    	alt_table_row_color($k);

	    	$running_total += $myrow["amount"];
			if($myrow["amount"]>0)
				$ttl1+=$myrow["amount"];
			if($myrow["amount"]<0)
				$ttl2+=$myrow["amount"];
			if($myrow["amount"]>0)
				$ttlall1+=$myrow["amount"];
			if($myrow["amount"]<0)
				$ttlall2+=$myrow["amount"];
			$ttlall3+=$myrow["amount"];

	    	$trandate = sql2date($myrow["tran_date"]);

	    	label_cell("&nbsp;");
	    	//label_cell([$myrow["reference"]);
	    	if(@$_POST['Export'])
			label_cell($myrow["reference"]);
			else
			label_cell(get_gl_view_str($myrow["type"], $myrow["type_no"], $myrow["reference"], true));
			if($myrow["type"]==0)
	    	label_cell("GJ");
	    	else if($myrow["type"]==1)
	    	label_cell("CD");
	    	else
	    	label_cell("CR");
	    	label_cell($trandate);
			if ($myrow['memo_'] == "")
				$myrow['memo_'] = get_comments_string($myrow['type'], $myrow['type_no']);
	    	label_cell($myrow['memo_']);
	    	


	    	if(@$_POST['Export'])
			display_debit_or_credit_cells($myrow["amount"]);
	    	else
			display_debit_or_credit_cells($myrow["amount"],false,$myrow["type"], $myrow["type_no"]);
			//if ($show_balances)
		    amount_cell_cr($running_total);
	    	end_row();

	    	$j++;
	    	
	    	/*if ($j == 12)
	    	{
	    		$j = 1;
	    		table_header($th);
	    	}*/
	    	
	    }
	    
		//if (@$_POST['NoZero']!=1 && $bfw<>0)
		//{
	    	start_row("class='inquirybg2'");
	    	label_cell("", "colspan=5");
	    	//display_debit_or_credit_cells($running_total, true);
		    amount_cell_cr($ttl1);
		    amount_cell_cr(abs($ttl2));
		    amount_cell_cr($running_total);
	    	//label_cell("");
	    	//label_cell("");
	    	end_row();
	    //}
	}
    	start_row("");
    	label_cell("&nbsp;", "colspan=9");
    	end_row();
    	
	end_table(2);
	/*
	if (db_num_rows($result) == 0)
		display_note(_("No general ledger transactions have been created for the specified criteria."), 0, 1);
	*/
}

//----------------------------------------------------------------------------------------------------
	
if (get_post('Export')) 
{
	
	/*
	$header='
	<style>
	.inquirybg{
		background-color:#EE7600 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.inquirybg2{
		font-weight:bold !important;
		font-size:12px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	</style>
	<table border="0">
		<tr><td align="center">&nbsp;</td></tr>
		<tr><td style="width:2%;">&nbsp;&nbsp;</td>
			<td>
			<table border="0" style="border:#000 1px solid;">
				<tr>
					<td width="100%" align="center" style="background-color:#bfbfbf;font-size:12px;color:#ee3000;"><b>PT. Kresna Asset Management</b></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:20px;color:#ee3000;font-weight:bold;">Export To MYOB (Disbursement)</td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;color:#ee3000;font-weight:bold;">'.$_POST["TransFromDate"].' To '.$_POST["TransToDate"].'</td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
	';
	echo $header;
	echo '<tr><td style="width: 409pt;font-size:11px !important;">';
	*/
	show_results();
	/*
	echo '</td></tr></table>
		</td></tr>

		<tr><td></td><td align="center" style="background-color:#EE7600;border:#000 1px solid;">&nbsp;</td></tr>
		</table>';
	*/
}else{
	
	gl_inquiry_controls();

	div_start('trans_tbl');

	if (get_post('Show') || get_post('account'))
	    show_results();

	if (get_post('Export')){
	    show_results();
	}
	div_end();

	end_page();

}
