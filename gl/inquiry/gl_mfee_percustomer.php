<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/siar.inc");
include_once($path_to_root . "/gl/includes/siap.inc");
include_once($path_to_root . "/gl/includes/db/gl_db_banking.inc");
include_once($path_to_root . "/sales/includes/db/customers_db.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

if (get_post('Export')) 
{
	//include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"Management Fee.xls\"");
	header("Cache-Control: max-age=0");
	
	
}else{
	page(_($help_context = "Management Fee"), false, false, '', $js);
}
//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}

if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format(0);

//----------------------------------------------------------------------------------------------------

		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
		$tgl1=$_POST['TransFromDate'];
		$tgl2=$_POST['TransToDate'];	

	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    
    start_table(TABLESTYLE_NOBORDER);

	echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("Customer").":</td>\n";
	echo "<td>";

	$custs=get_cust();
	echo '<select name="cust" id="cust">';
		echo '<option value="0" selected>All</option>';
	for($t=0;$t<count($custs);$t++){
		//if($t==date('Y'))
		echo '<option value="'.$custs[$t][0].'">'.$custs[$t][1].'</option>';
		//else
		//echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("Product").":</td>\n";
	echo "<td>";

	$products=get_prods();
	//print_r($products);
	echo '<select name="product" id="product">';
		echo '<option value="0" selected>All</option>';
	for($t=0;$t<count($products);$t++){
		//if($t==date('Y'))
		echo '<option value="'.$products[$t][2].'">['.$products[$t][2].'] '.$products[$t][3].'</option>';
		//else
		//echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo'<script src="../../jquery2.min.js"></script>';
	echo '<script type="text/javascript">
		$(document).ready(function() {
		  $("#cust").select2();
		  $("#product").select2();
		});
		</script>';
	echo "</td>\n";
	start_row();
   // gl_all_accounts_list_cells(_("Account:"), 'account', null, false, false, _("All Accounts"));
	//, -user_transaction_days()
	date_cells(_("From :"), 'TransFromDate', '', null);
	date_cells(_("to :"), 'TransToDate', '', null);
    end_row();
	end_table();
	

	start_table(TABLESTYLE_NOBORDER);
	start_row();

	//echo '<td colspan="6" style="text-align:center;">&nbsp;</td>';
	submit_cells('Show',_("Show"),'align="center" colspan="6"','', 'default');
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">
	</td>';
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	
	echo '
	<style>
		.export{
			align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle2").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Management_Fee",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
	//submit_cells('Export Excel',_("Export"),'','', 'default');
	end_row();
	end_table();

	echo '<hr>';
    end_form();
}

//----------------------------------------------------------------------------------------------------

function show_results()
{
	global $path_to_root, $systypes_array;


	start_table(TABLESTYLE2, "width='90%'", 10);
		

		$cust=$_POST['cust'];
		$feeformulas=get_formulafee_siap();
		//print_r($feeformulas);
		//print_r($feeformulas);
			
    	//echo '<tr><td colspan="4" class="tableheader">'.$custs[$t][1].'</td></tr>';
    	if($cust==0){

			 
    		$fee=@$feeformulas[$_POST['product']];
			//$lastmfees=get_total_mfee2(date2sql($_POST['TransToDate']),0,$_POST['product'],$fee);
    		//print_r(($lastmfees));
    		$intday=@$fee[5];
    		$feeamn=@$fee[2];
    		$adjamn=@$fee[4];
    		$days=@$fee[6];
    		$tgl2=date('Y-m-d', strtotime(date2sql($_POST['TransToDate'])));
    		$tgl3=date('Y-m-d', strtotime(date2sql($_POST['TransToDate'])));
    		//$tgl3=date('Y-m-d', strtotime($intday.' days', strtotime(date2sql($_POST['TransToDate']))));
			$mfees=get_mgtfee_custall($_POST['product'],$cust,date2sql($_POST['TransFromDate']),@$tgl2,$intday,$feeamn,$adjamn,$days,$tgl3);
			//print_r($mfees);
			$isi=array();$cusids=array();$custs=array();$pc=array();$prods=array();
			if(count($mfees)>0){
				for($a=0;$a<count($mfees);$a++)
				{
					if(!in_array($mfees[$a][4], $cusids)){
						$cusids[]=$mfees[$a][4];
						$custs[$mfees[$a][4]]=$mfees[$a][5];
					}
			    	$isi[$mfees[$a][4]][]=$mfees[$a];
				}
			}
			$ttlall=0;
			$ttlall2=0;
			$ttlall3=0;
			$tnav=0;
			$tunit=0;
		    for($a=0;$a<count($cusids);$a++){
		    	
				$cusid=$cusids[$a];

	    		//$lastmfee=$lastmfees[$_POST['product']][$cusid];

		    	echo '<tr><td colspan="6" class="tableheader">'.$custs[$cusid].'</td></tr>';
		    	$th = array(_("Product"),  _("NAV "),  _("Unit "),  _("AUM "),  _("Management Fee (Moving)"),  _("Management Fee (Total)"));		
				table_header($th);
				$ttlpc=0;$ttlpc2=0;$ttlpc3=0;
		    	for($b=0;$b<count($isi[$cusid]);$b++){
		    		if($_POST['product']==0){
			    		$fee=@$feeformulas[$isi[$cusid][$b][1]];
						//$lastmfees=get_total_mfee2(date2sql($_POST['TransToDate']),0,$_POST['product'],$fee);
			    		//print_r(($fee));
			    		$intday=@$fee[5];
			    		$feeamn=@$fee[2];
			    		$adjamn=@$fee[4];
			    		$days=@$fee[6];
			    	}
			    	if($days<0)
			    		$days=365;
		    		//print_r($isi[$cusid][$b]);
		    		$nav=$isi[$cusid][$b][2];
		    		$unit=$isi[$cusid][$b][3];
		    		$aum=$nav*$unit*3*$adjamn/100/$days;
		    		$aum2=$nav*$unit*$feeamn*1/100/$days;
		    		//$aum=$nav*$unit*@$isi[$cusid][$b][9]/$days;
		    		//$mfee=@$isi[$cusid][$b][9];
		    		$mfee=$aum;
		    		//$aum=0;
		    		//$mfee=0;
					echo '<tr>';
					label_cell($isi[$cusid][$b][1]);
					label_cell(price_format(($isi[$cusid][$b][2])),'align="right"');
					label_cell(price_format(($isi[$cusid][$b][3])),'align="right"');
					label_cell(price_format(($isi[$cusid][$b][7])),'align="right"');
					label_cell(price_format(($aum2)),'align="right"');
					label_cell(price_format(($mfee)),'align="right"');
					$tnav+=$nav;
					$tunit+=$unit;
					$ttlpc2+=$aum2;
					$ttlpc+=$isi[$cusid][$b][7];
					$ttlpc3+=$mfee;
					$ttlall2+=$aum2;
					$ttlall+=$isi[$cusid][$b][7];
					$ttlall3+=$mfee;
		    	}

		    	echo '<tr><td colspan="3" class="tableheader">Jumlah '.$custs[$cusid].'</td>';
		    	echo '<td class="tableheader" style="text-align:right !important;">'.price_format($ttlpc).'</td>
		    	<td class="tableheader" style="text-align:right !important;">'.price_format($ttlpc2).'</td>
		    		  <td class="tableheader" style="text-align:right !important;">'.price_format($ttlpc3).'</td></tr>';
		    	echo '<tr><td colspan="6" class="">&nbsp;</td></tr>';
			}

			$fee=$feeformulas['MSEOF'];
		//	print_r($fee);
    		$intday=@$fee[5];
    		$feeamn=@$fee[2];
    		$adjamn=@$fee[4];
    		$days=@$fee[6];

	    	echo '<tr><td colspan="6" class="tableheader">Formula MSEOF : '.price_format($nav).' x '.price_format($tunit).' x '.$feeamn.' x '.$adjamn.' / 100 / '.$days.'</td></tr>';
	    	echo '<tr><td colspan="3" class="tableheader">Total </td>';
	    	echo '<td class="tableheader" style="text-align:right !important;">'.price_format($ttlall).'</td>
		    	<td class="tableheader" style="text-align:right !important;">'.price_format($ttlall2).'</td>

		    		  <td class="tableheader" style="text-align:right !important;">'.price_format($ttlall3).'</td></tr>';
		    
	    		$taum=380167390840.69;
	    		$nav=4135.14;
	    		$unit=91935721.3330;
	    		$tmfee=$taum*$feeamn*$adjamn/100/$days;
	    		$tmfee2=$taum*3*$adjamn/100/$days;
	    	echo '<tr><td colspan="6" class="tableheader">Formula MSEOF : NAV x Unit x '.$feeamn.' x '.$adjamn.' / 100 / '.$days.'</td></tr>';
	    	echo '<tr><td colspan="3" class="tableheader">Total AUM Dari SIAP ( '.price_format($nav).' x '.price_format($unit).' )</td>';
	    	echo '<td class="tableheader" style="text-align:right !important;">'.price_format($taum).'</td>
		    	<td class="tableheader" style="text-align:right !important;">'.price_format($tmfee).'</td>
				<td class="tableheader" style="text-align:right !important;">'.price_format($tmfee2).'</td></tr>';
		    $taum=380167318502.59;
    		//$taum=280366192448.606;
    		$nav=4135.14;
    		$unit=91935703.73;
    		$tmfee=$taum*$feeamn*$adjamn/100/$days;
    		$tmfee2=$taum*3*$adjamn/100/$days;
    		echo '<tr><td colspan="3" class="tableheader">Total AUM Dari SIAR ( '.price_format($nav).' x '.price_format($unit).' )</td>';
    		echo '<td class="tableheader" style="text-align:right !important;">'.price_format($taum).'</td>
		    	<td class="tableheader" style="text-align:right !important;">'.price_format($tmfee).'</td>
				<td class="tableheader" style="text-align:right !important;">'.price_format($tmfee2).'</td></tr>';
			
		}
		else{
			$mfees=get_mgtfee_cust($_POST['product'],$cust,date2sql($_POST['TransFromDate']),date2sql($_POST['TransToDate']));
			//print_r($mfees);
			$isi=array();$pc=array();$prods=array();
			if(count($mfees)>0){
				$no=1;
				for($a=0;$a<count($mfees);$a++)
				{
					if(!in_array($mfees[$a][2], $pc)){
						$pc[]=$mfees[$a][2];
						$prods[$mfees[$a][2]]=$mfees[$a][3];
					}
			    	$no++;
			    	$isi[$mfees[$a][2]][]=$mfees[$a];
			    	$customer=$mfees[$a][6];
			    }
		    }else{
		    	continue;
		    }
		    //print_r($pc);
		    $ttlall=0; $ttlall2=0; $ttlall3=0;
		    for($a=0;$a<count($pc);$a++){
		    	
				$prod=$pc[$a];

    			$fee=$feeformulas[$prod];
    		//	print_r($fee);
	    		$intday=@$fee[5];
	    		$feeamn=@$fee[2];
	    		$adjamn=@$fee[4];
	    		$days=@$fee[6];
	    		//$tgl2=date('Y-m-d', strtotime($intday.' days', strtotime(date2sql($_POST['TransToDate']))));
	    		$tgl2=date('Y-m-d',strtotime(date2sql($_POST['TransToDate'])));

		    	echo '<tr><td colspan="6" class="tableheader">'.$prod.'</td></tr>';
		    	$th = array(_("Tanggal"),  _("NAV "),  _("Unit "),  _("AUM "),  _("Management Fee (Moving)"),  _("Management Fee (Total)"));		
				table_header($th);
				$ttlpc=0;$ttlpc2=0;$ttlpc3=0;
				$unit=0;

    			//$lastmfee=get_total_mfee(date2sql($_POST['TransToDate']),$cust,$prod,$fee);
		    	for($b=0;$b<count($isi[$prod]);$b++){
			    		$nav=$isi[$prod][$b][3];
			    		$unit=$isi[$prod][$b][4];
			    		$aum=$nav*$unit;
		    		if(strtotime($isi[$prod][$b][0])<=strtotime($tgl2) && $aum>0){

			    		$mfee=$aum*$feeamn*$adjamn/100/$days;
			    		$mfee2=$mfee;

						echo '<tr>';
						label_cell(date('d M Y',strtotime($isi[$prod][$b][0])));
						label_cell(price_format(($isi[$prod][$b][3])),'align="right"');
						label_cell(price_format(($unit)),'align="right"');
						label_cell(price_format(($aum)),'align="right"');
						label_cell(price_format(($mfee)),'align="right"');
						label_cell(price_format(($mfee2)),'align="right"');
						$ttlpc2=$mfee;
						$ttlpc=$aum;
						$ttlpc3=$mfee2;
					}
					//$ttlall3+=$isi[$cusid][$b][7];
		    	}
						$ttlall+=$ttlpc;
						$ttlall2+=$ttlpc2;
						$ttlall3+=$ttlpc3;

		    	echo '<tr><td colspan="3" class="tableheader">Jumlah '.$prod.'</td>';
		    	echo '<td class="tableheader" style="text-align:right !important;">'.price_format($ttlpc).'</td>
	    		    	<td class="tableheader" style="text-align:right !important;">'.price_format($ttlpc2).'</td>
		    		  <td class="tableheader" style="text-align:right !important;">'.price_format($ttlpc3).'</td></tr>';
		    	echo '<tr><td colspan="6" class="">&nbsp;</td></tr>';
		    }
	    	echo '<tr><td colspan="3" class="tableheader">Jumlah '.$customer.'</td>';
	    	echo '<td class="tableheader" style="text-align:right !important;">'.price_format($ttlall).'</td>
	    		    	<td class="tableheader" style="text-align:right !important;">'.price_format($ttlall2).'</td>
		    		  <td class="tableheader" style="text-align:right !important;">'.price_format($ttlall3).'</td></tr>';
	    }
   
end_table(1);
}
//----------------------------------------------------------------------------------------------------
if (get_post('Export')) 
{
	
	$header='
	<style>
	.tableheader{
		background-color:#000 !important;
		color:#fff !important;
	}
	.tablestyle{
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">Management Fee</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Masa '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 651;">';
	echo show_results();
	echo '</td></tr></table>';
}else{
	gl_inquiry_controls();

	div_start('trans_tbl');

	if (get_post('Show') || get_post('account'))
	    show_results();

	div_end();

	//----------------------------------------------------------------------------------------------------

	end_page();
}

