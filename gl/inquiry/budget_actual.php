<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/config_mssql.php");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
/*
*/
$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

if (isset($_GET["period"]))
	$_POST["period"] = $_GET["period"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];


if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"budget.xls\"");
	header("Cache-Control: max-age=0");
	
	
}else{
	page(_($help_context = "Actual Vs Actual"), false, false, '', $js);
}
//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}


//----------------------------------------------------------------------------------------------------

function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER,7);
	start_row();

	echo "<td>"._("Type").":</td>\n";
	echo "<td>";
	$period = array(_("Outstanding"),_("Multiple"));
	//$period = array(_("Monthly"),_("Quarterly"),_("Semi Annyally"),_("Annually"));
	echo array_selector('tipe', null, $period);
	echo "</td>\n";
	
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_("January"),_("February"),_("March"),_("April"),_("May"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));
	

	echo array_selector('bulanfrom', null, $bulan);
	echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select autocomplete="off" name="tahunfrom" class="combo" title="" _last="0">';
	for($t=date('Y');$t>2000;$t--){
		if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo '<select autocomplete="off" name="tahunto" class="combo" title="" _last="0">';
	for($t=date('Y');$t>2000;$t--){
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>';
	//echo array_selector('tahunto', null, $tahun);
	echo "</td>\n";

	echo "<td>";
	echo '<input type="checkbox" name="realisasi" value="1">';
	echo "</td>\n";
	echo "<td>"._("Vs Budget ".date('Y'))."</td>\n";


	//date_cells(_("From:"), 'TransFromDate', '', null, -user_transaction_days());
	//date_cells(_("To:"), 'TransToDate', '', null, -user_transaction_days());
	//date_cells(_("End Date:"), 'TransToDate');
    end_row();
	end_table();

	start_table(TABLESTYLE_NOBORDER);
	start_row();

	submit_cells('Show',_("Show"),'','', 'default');
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel"></td>';
	
	//echo '<td><a class="ajaxsubmit export" href="export/budget.php" name="Export Excel" id="Export Excel" value="Export" target="_blank"><span>Export Excel</span></a></td>';
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	
	echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
		function xls(){
			//console.log("aa");
			$(".tablestyle2").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Budget",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';

	//submit_cells('Export Excel',_("Export"),'','', 'default');
	end_row();
	end_table();

	echo '<hr>';
    end_form();
}

//----------------------------------------------------------------------------------------------------


function show_results()
{
	global $path_to_root, $systypes_array,$SysPrefs;

	
	$lampir=@$_POST['lampiran']+1;
	$ctype=$lampir;
	$colspan = (@$dim == 2 ? "6" : (@$dim == 1 ? "5" : "4"));
	//start_table(TABLESTYLE, "width='90%'", 6);
	
	//start_table(TABLESTYLE2, "width='90%'", 10);

	$th = array( _(""));
	$blnfrom=$_POST["bulanfrom"]+1;	
	$thnfrom=$_POST["tahunfrom"];
	$blnto=$_POST["bulanto"]+1;
	$bln=array();$colspan=2;
	$thn=$_POST["tahunfrom"];
	$thnto=$_POST["tahunto"];
	/*
	for($a=3;$a>=1;$a--){
		$thn2=$thn-$a;
		if(@$_POST["realisasi"]==1){
			$blnnya=date('M Y',strtotime($thn2.'-'.$blnfrom.'-01'));
			if($blnfrom!=$blnto){		
				$blnnya=date('M',strtotime($thn2.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thn2.'-'.$blnto.'-01'));
			}
			$bln[]='Realisasi<br>'.$blnnya;
			$colspan++;
		}
	}
	*/
	$blnnya=date('M Y',strtotime($thn.'-'.$blnfrom.'-01'));
	if($blnfrom!=$blnto){		
		$blnnya=date('M',strtotime($thn.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thnto.'-'.$blnto.'-01'));
	}
	//outstanding
	if($_POST["tipe"]==0){
		for($t=$thn;$t<=$thnto;$t++){
			$blnnya=date('M Y',strtotime($t.'-'.$blnfrom.'-01'));
			if($blnfrom!=$blnto){		
				$blnnya=date('M',strtotime($t.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($t.'-'.$blnto.'-01'));
			}

			//$colspan++;
			$bln[]='<b>Actual<br>'.$blnnya.'</br>';
			if(@$_POST["realisasi"]==1 && $t==date('Y')){
				$bln[]='<b>Budget<br>'.$blnnya.'</br>';			
				$bln[]='<b>Selisih<br>'.$blnnya.'</br>';			
				//$colspan++;
			}
		}
	}
	//multiple
	if($_POST["tipe"]==1){
		for($a=$blnfrom;$a<=$blnto;$a++){
			for($t=$thn;$t<=$thnto;$t++){
				$bln[]='<b>Actual<br>'.date('M Y',strtotime($t.'-'.$a.'-01')).'</br>';
				$colspan++;
				if(@$_POST["realisasi"]==1 && $t==date('Y')){
					$bln[]='<b>Budget<br>'.date('M Y',strtotime($t.'-'.$a.'-01')).'</br>';
					$bln[]='<b>Selisih<br>'.date('M Y',strtotime($t.'-'.$a.'-01')).'</br>';
					//$colspan++;
				}
			}
		}
	}
	//$colspan++;
	//print_r($bln);
	$th=array_merge($th,$bln);
	//table_header($th);
	$tableheader='<table class="tablestyle" cellpadding="6" border="1" width="90%">';
	$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
	for($a=0;$a<count($th);$a++){
		$tableheader.='<td class="tableheader" align="center">'.$th[$a].'</td>';
	}
	echo $tableheader.='</tr>';
	//$budgets=get_budgets(date2sql($_POST['TransFromDate']),date2sql($_POST['TransToDate']),$_POST['period']);
	//print_r($budgets);
	//print_r($mfees);
	$no=1;
		$acctypes1 = get_account_classes(false, 0);
		//$acctypes1=get_account_types($all=false, $class_id=false,false);

		while($row1 = db_fetch_row($acctypes1)){
			echo '<tr>';
			$spasi='';
			if($row1[3]!='')
			$spasi='&nbsp;&nbsp;';
	    	label_cell($spasi.'<b>'.$row1[1].'</br>','colspan="'.$colspan.'"');

	    	$typeresult = get_account_types(false, $row1[0], -1);
			$k = 0; // row color
			$total1=0;$total2=0;
			//print_r($row1);
			while ($accounttype=db_fetch($typeresult))
			{
				//print_r($accounttype);
				$accounts=get_gl_accounts(false, false, $accounttype[0]);
				while($row = db_fetch_row($accounts)){
					//print_r($row);
					echo '<tr>';
			    	label_cell('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$row[2]);
			    	/*
					if(@$_POST["realisasi"]==1){
				    	for($a=3;$a>=1;$a--){				
							$budamount2=sum_budgets(($thn-$a).'-'.$blnfrom.'-01',($thn-$a).'-'.$blnto.'-31',$row[0]);		
							label_cell(price_format(@$budamount2),'align="right"');
							$totbud2[$a]=$totbud2[$a]+$budamount2;
						}
					}				
					*/			
					//multiple
					if($_POST["tipe"]==1){
						for($t=$thn;$t<=$thnto;$t++){
							for($a=$blnfrom;$a<=$blnto;$a++){
								$budamount=sum_bal2_account($thn.'-'.$a.'-01',$t.'-'.$a.'-31',$row[0]);	
								label_cell(price_format(abs(@$budamount)),'align="right"');
								$totbud2[$t][$a]=@$totbud2[$t][$a]+$budamount;
								if(@$_POST["realisasi"]==1 && $t==date('Y')){
									$realamount=sum_budgets($t.'-'.$a.'-01',$t.'-'.$a.'-31',$row[0]);
									//$realamount=1;	
									label_cell(price_format(abs(@$realamount)),'align="right"');
									$totreal2[$t][$a]=@$totreal2[$t][$a]+$realamount;
									$selisih=abs(@$realamount)-abs(@$budamount);
									label_cell(price_format(@$selisih),'align="right"');
								}
							}
						}
					}else{
					//outstanding

					for($t=$thn;$t<=$thnto;$t++){
							$budamount=sum_bal2_account($t.'-'.$blnfrom.'-01',$t.'-'.$blnto.'-31',$row[0]);	
							label_cell(price_format(abs(@$budamount)),'align="right"');
							$totbud[$t]=@$totbud[$t]+$budamount;
							if(@$_POST["realisasi"]==1 && $t==date('Y')){
								$realamount=sum_budgets($t.'-'.$blnfrom.'-01',$t.'-'.$blnto.'-31',$row[0]);	
								//$realamount=1;
								label_cell(price_format(abs(@$realamount)),'align="right"');
								$totreal[$t]=@$totreal[$t]+$realamount;
								$selisih=abs(@$realamount)-abs(@$budamount);
								label_cell(price_format(@$selisih),'align="right"');
							}
						}
					}
					//label_cell(@$budamount);
			    }
		    }		    
			echo '<tr>';
	    	label_cell($spasi.'<b>Total '.$row1[1].'</b>');
	    		
			//multiple
			if(@$_POST["tipe"]==1){
				for($t=$thn;$t<=$thnto;$t++){
					for($a=$blnfrom;$a<=$blnto;$a++){	
						label_cell('<b>'.price_format(abs($totbud2[$t][$a])).'</b>','align="right"');	
						if(@$_POST["realisasi"]==1 && $t==date('Y')){
							label_cell('<b>'.price_format(abs(@$totreal2[$t][$a])).'</b>','align="right"');
							label_cell('<b>'.price_format(abs(@$totreal2[$t][$a])-abs(@$totbud2[$t][$a])).'</b>','align="right"');

						}
					}
				}
			}else{				
				for($t=$thn;$t<=$thnto;$t++){
					label_cell('<b>'.price_format(abs(@$totbud[$t])).'</b>','align="right"');	
					if(@$_POST["realisasi"]==1 && $t==date('Y')){
						label_cell('<b>'.price_format(abs(@$totreal[$t])).'</b>','align="right"');
						label_cell('<b>'.price_format(abs(@$totreal[$t])-abs(@$totbud[$t])).'</b>','align="right"');

					}
				}
			}
			$totbud=array();$totbud2=array();
			$totreal=array();$totreal2=array();
			    	$no++;
		}
    if($no==1){
    	echo '<tr><td colspan="'.($colspan).'">No Data Available</td></tr>';
    }
/*
*/
	end_table(1);
}
//----------------------------------------------------------------------------------------------------


if (get_post('Export')) 
{
	$blnfrom=$_POST["bulanfrom"]+1;	
	$thnfrom=$_POST["tahunfrom"];
	$blnto=$_POST["bulanto"]+1;
	$thn=$_POST["tahunfrom"];
	$thnto=$_POST["tahunto"];
	$blnnya=date('M Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));

	if($thnfrom==$thnto){
		$blnnya=date('M',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thnto.'-'.$blnto.'-01'));
	}else{
		//if($blnfrom!=$blnto){		
			$blnnya=date('M',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('M',strtotime($thnto.'-'.$blnto.'-01')).'  '.date('Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('Y',strtotime($thnto.'-'.$blnto.'-01'));
		
	}
	$header='
	<style>
	.inquirybg{
		background-color:#000 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	</style>
	<table border="0">

		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">BUDGET</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td>';
	show_results();
	echo '</td></tr></table>';
}else{
	gl_inquiry_controls();
	div_start('trans_tbl');
	echo '<center>';
	if (get_post('Show') || get_post('account'))
	    show_results();
	echo '</center>';

	div_end();

	//----------------------------------------------------------------------------------------------------

	end_page();

}

