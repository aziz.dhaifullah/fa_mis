<?php

$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

function netprof($from,$to,$dimension=null,$dimension2=null){
	
		$blnfrom=date('m',strtotime(date2sql($from)));
		$blnto=date('m',strtotime(date2sql($to)));
		$thnfrom=date('Y',strtotime(date2sql($from)));
		$thnto=date('Y',strtotime(date2sql($to)));
		$tglto=date('d',strtotime(date2sql($to)));
	
	$begin=date('d/m/Y',strtotime($thnfrom.'-01-01'));

	//$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $tglto)));

	//if($tglto==cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto))
	//$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));
	//else
	$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.$tglto));
	/*
	if($_POST["tipe"]==0){
		$colom=3;
	}else{
		$colom=3+($blnto-$begin);
	}
	*/
	

	$colspan=4;
	$cols=2;

	$totalaset=0;
	//$tableheader .= '<tr><td colspan="3">Kas dan Setara Kas</td></tr>';
	//get_account_from_to
	$ttlall=0;$ttlall2=0;

	//Pendapatan usaha
	$accounts=get_account_from_to('40000','46000');
	$nrow=n_account_from_to('40000','46000');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	$ttlcol=array();$ttlcol2=array();
	$ttlalls=array();$ttlalls2=array();
	
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;
			$tot = get_balance($account["account_code"], $dimension, $dimension2, $begin, $to, true, true);
			$total=$total+($tot['debit']-$tot['credit']);
			$ttlclass+=$total;
			$ttlclass2+=$total;
			$ttlall+=$total;

		$n++;
	}
	
	$ttlcol=array();


	//Cost Of Sales
	$accounts=get_account_from_to('51000','51005');
	$nrow=n_account_from_to('51000','51005');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;
			
			$tot = get_balance($account["account_code"], $dimension, $dimension2, $begin, $to, true, true);
			$total=$total+($tot['debit']-$tot['credit']);
			$ttlclass+=$total;
			$ttlclass2+=$total;
			$ttlall+=$total;

		$n++;
	}
	
	$ttlcol=array();

	//Biaya Operasional
	$ttlcol=array();$ttlcol2=array();$ttlalls3=array();
	$ttlclass=0;
	$accounts=get_account_from_to('61000','69990');
	$nrow=n_account_from_to('61000','69990');
	$notacc=array(
		'62002001','62002002','62002003','62002004','62002005','62002006','62002007','62002008','62002099',
		'62003001','62003002','62003003','63003001'
		,'68001001','68001002','68001003','68001004','68001005','68001006','68001007','68001008','68001009','68001099'
		,'69101001','69101002','69101003','69104001','69104002','69104003','69104004','69104005');
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;
		if(!in_array($account["account_code"],$notacc)){

			$paracc=get_parent_subaccount($account["account_code"]);
			if(@$paracc[0]==''){
				if(!in_array($account["head"],$head) and $account["account_type"]!='6-0000'){	

					if(@$head[($a-1)]!=@$head[($a)] ){
						//print_r($ttlcol);

						$ttlclass=0;

					}
					$head[]=$account["head"];
					$a++;
				}
			
				
				$subaccounts=get_subaccount($account["account_code"]);
					
					while ($subaccount = db_fetch($subaccounts))
					{
						$tot = get_balance($subaccount["account_code"], $dimension, $dimension2, $begin, $to, true, true);
						$total=$total+($tot['debit']-$tot['credit']);
					}
					
					$tot = get_balance($account["account_code"], $dimension, $dimension2, $begin, $to, true, true);
					$total=$total+($tot['debit']-$tot['credit']);
					$ttlclass+=$total;
					$ttlclass2+=$total;
					$ttlall+=$total;


				$n++;
			}
		}
	}
	
	$ttlcol=array();
	//Other Income
	$accounts=get_account_from_to('80000','89999');
	$nrow=n_account_from_to('80000','89999');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;

		$tot = get_balance($account["account_code"], $dimension, $dimension2, $begin, $to, true, true);
		$total=$total+($tot['debit']-$tot['credit']);
		$ttlclass+=$total;
		$ttlclass2+=$total;
		$ttlall+=$total;

		$n++;
	}

	$ttlcol=array();


	//Other Expense
	$accounts=get_account_from_to('90000','99999');
	$nrow=n_account_from_to('90000','99999');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	$ttlcol=array();

	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;
			$tot = get_balance($account["account_code"], $dimension, $dimension2, $begin, $to, true, true);
			$total=$total+($tot['debit']-$tot['credit']);
			$ttlclass+=$total;
			$ttlclass2+=$total;
			$ttlall+=$total;
		$n++;
	}

	return ($ttlall);
}


function netproftgl($from,$to,$dimension=null,$dimension2=null){
	
		$tglfrom=date('d',strtotime(date2sql($from)));
		$tglto=date('d',strtotime(date2sql($to)));
		$blnfrom=date('m',strtotime(date2sql($from)));
		$blnto=date('m',strtotime(date2sql($to)));
		$thnfrom=date('Y',strtotime(date2sql($from)));
		$thnto=date('Y',strtotime(date2sql($to)));
	
	$begin=date('d/m/Y',strtotime($thnfrom.'-01-01'));
	$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.$tglto));
	/*
	if($_POST["tipe"]==0){
		$colom=3;
	}else{
		$colom=3+($blnto-$begin);
	}
	*/
	

	$colspan=4;
	$cols=2;

	$totalaset=0;
	//$tableheader .= '<tr><td colspan="3">Kas dan Setara Kas</td></tr>';
	//get_account_from_to
	$ttlall=0;$ttlall2=0;

	//Pendapatan usaha
	$accounts=get_account_from_to('40000','46000');
	$nrow=n_account_from_to('40000','46000');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	$ttlcol=array();$ttlcol2=array();
	$ttlalls=array();$ttlalls2=array();
	
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;
			$tot = get_balance($account["account_code"], $dimension, $dimension2, $begin, $to, true, true);
			$total=$total+($tot['debit']-$tot['credit']);
			$ttlclass+=$total;
			$ttlclass2+=$total;
			$ttlall+=$total;

		$n++;
	}
	
	$ttlcol=array();


	//Cost Of Sales
	$accounts=get_account_from_to('51000','51005');
	$nrow=n_account_from_to('51000','51005');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;
			
			$tot = get_balance($account["account_code"], $dimension, $dimension2, $begin, $to, true, true);
			$total=$total+($tot['debit']-$tot['credit']);
			$ttlclass+=$total;
			$ttlclass2+=$total;
			$ttlall+=$total;

		$n++;
	}
	
	$ttlcol=array();

	//Biaya Operasional
	$ttlcol=array();$ttlcol2=array();$ttlalls3=array();
	$ttlclass=0;
	$accounts=get_account_from_to('61000','69990');
	$nrow=n_account_from_to('61000','69990');
	$notacc=array(
		'62002001','62002002','62002003','62002004','62002005',
		'62002006','62002007','62002008','62002099',
		'62003001','62003002','62003003','63003001','69101001'
		,'69101002','69101003','69104001','69104002','69104003','69104004'
		,'69104005');
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;
		if(!in_array($account["account_code"],$notacc)){

			$paracc=get_parent_subaccount($account["account_code"]);
			if(@$paracc[0]==''){
				if(!in_array($account["head"],$head) and $account["account_type"]!='6-0000'){	

					if(@$head[($a-1)]!=@$head[($a)] ){
						//print_r($ttlcol);

						$ttlclass=0;

					}
					$head[]=$account["head"];
					$a++;
				}
			

				$subaccounts=get_subaccount($account["account_code"]);
					
					while ($subaccount = db_fetch($subaccounts))
					{
						$tot = get_balance($subaccount["account_code"], $dimension, $dimension2, $begin, $to, true, true);
						$total=$total+($tot['debit']-$tot['credit']);
					}
					
					$tot = get_balance($account["account_code"], $dimension, $dimension2, $begin, $to, true, true);
					$total=$total+($tot['debit']-$tot['credit']);
					$ttlclass+=$total;
					$ttlclass2+=$total;
					$ttlall+=$total;


				$n++;
			}
		}
	}
	
	$ttlcol=array();
	//Other Income
	$accounts=get_account_from_to('80000','89999');
	$nrow=n_account_from_to('80000','89999');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	
	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;

		$tot = get_balance($account["account_code"], $dimension, $dimension2, $begin, $to, true, true);
		$total=$total+($tot['debit']-$tot['credit']);
		$ttlclass+=$total;
		$ttlclass2+=$total;
		$ttlall+=$total;

		$n++;
	}

	$ttlcol=array();


	//Other Expense
	$accounts=get_account_from_to('90000','99999');
	$nrow=n_account_from_to('90000','99999');
	$head=array();$headparent=array();
	$ttlclass=0;$ttlclass2=0;$a=0;$n=0;
	$ttlcol=array();

	while ($account = db_fetch($accounts))
	{
		//print_r($account);
		$total=0;
			$tot = get_balance($account["account_code"], $dimension, $dimension2, $begin, $to, true, true);
			$total=$total+($tot['debit']-$tot['credit']);
			$ttlclass+=$total;
			$ttlclass2+=$total;
			$ttlall+=$total;
		$n++;
	}

	return abs($ttlall);
}
//----------------------------------------------------------------------------------------------------
/*
$from='2017-01-31';
$to='2017-08-31';
echo netprof($from,$to);
*/
