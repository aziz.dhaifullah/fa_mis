<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLANALYTIC';
$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/inquiry/netprofit.php");

$js = "";
if (user_use_date_picker())
	$js = get_js_date_picker();

if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"Laporan Posisi Keuangan.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
page(_($help_context = "Laporan Posisi Keuangan "), false, false, "", $js);
}


$k = 0;
$pdeb = $pcre = $cdeb = $ccre = $tdeb = $tcre = $pbal = $cbal = $tbal = 0;

//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('balance_tbl');
}

if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER);

	$date = today();
	if (!isset($_POST['TransToDate']))
		$_POST['TransToDate'] = end_month($date);
	if (!isset($_POST['TransFromDate']))
		$_POST['TransFromDate'] = add_days(end_month($date), -user_transaction_days());
	echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo '</tr>';

	echo '<tr>';
    date_cells(_("&nbsp;"), 'TransFromDate');
	date_cells(_("&nbsp;"), 'TransToDate');
	if ($dim >= 1)
		dimensions_list_cells(_("Dimension")." 1:", 'Dimension', null, true, " ", false, 1);
	if ($dim > 1)
		dimensions_list_cells(_("Dimension")." 2:", 'Dimension2', null, true, " ", false, 2);
	//check_cells(_("No zero values"), 'NoZero', null);
	//check_cells(_("Account Code"), 'AccCode', null);
	echo '<td></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td colspan="4"></td>';
	submit_cells('Show',_("Show"),'align="right"','', 'default');
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">
	<input type="submit" class="export" name="PrintPdf" id="PrintPdf" Value="Print PDF"></td>';
	echo '</tr>';
	echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
	 	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Balance_sheet",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';

	//submit_cells('Show',_("Show"),'','', 'default');
    end_table();
    end_form();
}

//----------------------------------------------------------------------------------------------------

function display_trial_balance($type, $typename)
{
	global $path_to_root, $SysPrefs,
		 $k, $pdeb, $pcre, $cdeb, $ccre, $tdeb, $tcre, $pbal, $cbal, $tbal;

	$printtitle = 0; //Flag for printing type name

	$k = 0;

	//Get Accounts directly under this group/type
	$accounts = get_gl_accounts(null, null, $type);

	$begin = get_fiscalyear_begin_for_date($_POST['TransFromDate']);
	if (date1_greater_date2($begin, $_POST['TransFromDate']))
		$begin = $_POST['TransFromDate'];
	$begin = add_days($begin, -1);

	while ($account = db_fetch($accounts))
	{
		//Print Type Title if it has atleast one non-zero account	
		if (!$printtitle)
		{
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_("")."".$type ." - ".$typename, "colspan=3");
			end_row();
			$printtitle = 1;
		}

		// FA doesn't really clear the closed year, therefore the brought forward balance includes all the transactions from the past, even though the balance is null.
		// If we want to remove the balanced part for the past years, this option removes the common part from from the prev and tot figures.
		if (@$SysPrefs->clear_trial_balance_opening)
		{
			$open = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin,  $begin, false, true);
			$offset = min($open['debit'], $open['credit']);
		} else
			$offset = 0;

		$prev = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $_POST['TransFromDate'], false, false);
		$curr = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $_POST['TransFromDate'], $_POST['TransToDate'], true, true);
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		if (check_value("NoZero") && !$prev['balance'] && !$curr['balance'] && !$tot['balance'])
			continue;
		alt_table_row_color($k);

		$url = "<a href='$path_to_root/gl/inquiry/gl_account_inquiry.php?TransFromDate=" . $_POST["TransFromDate"] . "&TransToDate=" . $_POST["TransToDate"] . "&account=" . $account["account_code"] . "&Dimension=" . $_POST["Dimension"] . "&Dimension2=" . $_POST["Dimension2"] . "'>" . $account["account_code"] . "</a>";

		label_cell('');
		label_cell($account["account_code2"].' '.$account["account_name"]);
		if (check_value('Balance'))
		{
			display_debit_or_credit_cells($prev['balance']);
			display_debit_or_credit_cells($curr['balance']);
			display_debit_or_credit_cells($tot['balance']);

		}
		else
		{
			//amount_cell($prev['debit']-$offset);
			//amount_cell($prev['credit']-$offset);
			//amount_cell($curr['debit']);
			//amount_cell($curr['credit']);
			//amount_cell($tot['debit']-$offset);
			amount_cell($tot['debit']-$tot['credit']);
			$pdeb += $prev['debit'];
			$pcre += $prev['credit'];
			$cdeb += $curr['debit'];
			$ccre += $curr['credit'];
			$tdeb += $tot['debit'];
			$tcre += $tot['credit'];
		}	
		$pbal += $prev['balance'];
		$cbal += $curr['balance'];
		$tbal += $tot['balance'];
		end_row();
	}

	//Get Account groups/types under this group/type
	$result = get_account_types(false, false, $type);
	while ($accounttype=db_fetch($result))
	{
		//Print Type Title if has sub types and not previously printed
		if (!$printtitle)
		{
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_("").$type ." - ".$typename, "colspan=3");
			end_row();
			$printtitle = 1;

		}
		display_trial_balance($accounttype["id"], $accounttype["name"]);
	}
}

//----------------------------------------------------------------------------------------------------

function display_bs(){
	if (isset($_POST['TransFromDate']))
	{
		$row = get_current_fiscalyear();
		if (date1_greater_date2($_POST['TransFromDate'], sql2date($row['end'])))
		{
			display_error(_("The from date cannot be bigger than the fiscal year end."));
			set_focus('TransFromDate');
			return;
		}
	}
	if (!isset($_POST['Dimension']))
		$_POST['Dimension'] = 0;
	if (!isset($_POST['Dimension2']))
		$_POST['Dimension2'] = 0;
	//start_table(TABLESTYLE,'',6);

	if($_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=$_POST['bulanfrom'];
		$blnto=$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

	$begin=date('d/m/Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));	
	/*
	if($_POST["tipe"]==0){
		$colom=3;
	}else{
		$colom=3+($blnto-$begin);
	}
	*/
	if(get_post('PrintPdf'))
	$tableheader ='<center><table class="tablestyle" width="98.5%" cellpadding="3">';
	else
	$tableheader ='<center><table class="tablestyle" width="80%" cellpadding="3">';
	$tableheader .= '<tr><td class="tableheader" colspan="3" bgcolor="#dee7ec"><b>Aset</b></td></tr>';

	$totalaset=0;
	//$tableheader .= '<tr><td colspan="3">Kas dan Setara Kas</td></tr>';
	$accounts=get_gl_accounts(null, null, '1-1100');
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+($tot['debit']-$tot['credit']);
	}
	//$totalaset=$total+$totalaset;

	$accounts=get_gl_accounts(null, null, '1-1150');
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+($tot['debit']-$tot['credit']);
	}
	//$totalaset=$total+$totalaset;

	$accounts=get_gl_accounts(null, null, '1-1200');
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+($tot['debit']-$tot['credit']);
	}
	$totalaset=$total+$totalaset;

	$tableheader .= '<tr><td colspan="2">Kas dan Setara Kas</td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';
	$accounts=get_gl_accounts(null, null, '1-1300');
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+($tot['debit']-$tot['credit']);

	}
	$totalaset=$total+$totalaset;
	$tableheader .= '<tr><td colspan="2">Portofolio Efek</td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';

	$accounts=get_gl_accounts(null, null, '1-1400');
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+($tot['debit']-$tot['credit']);
	}
	$totalaset=$total+$totalaset;
	$tableheader .= '<tr><td colspan="2">Piutang Management Fee</td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';

	//$tableheader .= '<tr><td colspan="3">Piutang Lain Lain</td></tr>';
	$accounts=get_gl_accounts(null, null, '1-1598');
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+($tot['debit']-$tot['credit']);
	}
	//$totalaset=$total+$totalaset;

	$accounts=get_gl_accounts(null, null, '1-1599');
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+($tot['debit']-$tot['credit']);
	}
	$totalaset=$total+$totalaset;
	
	$tableheader .= '<tr><td colspan="2">Piutang Lain Lain</td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';

	$total=0;
	$accounts=get_gl_accounts_in(array('11699'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+($tot['debit']-$tot['credit']);
		$subaccounts=get_subaccount($account["account_code"]);
		while ($subaccount = db_fetch($subaccounts))
		{
			$tot4 = get_balance($subaccount["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
			$total+=($tot4['balance']);
		}

	}
	//$totalaset=$total+$totalaset;
	
	$accounts=get_gl_accounts_in(array('11601','11602','11603','11604','11605','11606','11607','11608','11609','11610'
		,'11611','11612','11613','11614','11615','11616','11617','11618'
	));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+($tot['debit']-$tot['credit']);
	}
	//$totalaset=$total+$totalaset;
	/*
	$accounts=get_gl_accounts_in(array('11601','11602','11612'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+($tot['debit']-$tot['credit']);
	}
	//$totalaset=$total+$totalaset;

	$accounts=get_gl_accounts_in(array('11603','11604','11605','11610','11611','11613','11614','11615','11616','11617','11618','12999'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+($tot['debit']-$tot['credit']);
	}
	*/
	$totalaset=$total+$totalaset;
	$tableheader .= '<tr><td colspan="2">Uang Muka dan Biaya Dibayar Di Muka</td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';


	//$accounts=get_gl_accounts_in(array('11705'));
	$accounts=get_gl_accounts(null, null, '1-1700');
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+($tot['debit']-$tot['credit']);
	}
	$totalaset=$total+$totalaset;
	$tableheader .= '<tr><td colspan="2">Pajak Dibayar Di Muka</td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';

	$accounts=get_gl_accounts_in(array('12211','12251','12241','12221','12231','12212','12242','12222','12232','12261','12262'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+($tot['debit']-$tot['credit']);
	}
	$totalaset=$total+$totalaset;
	$tableheader .= '<tr><td colspan="2">Aset Tetap</td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';
	$accounts=get_gl_accounts_in(array('12301'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+($tot['debit']-$tot['credit']);
	}
	$totalaset=$total+$totalaset;
	$tableheader .= '<tr><td colspan="2">Aset Pajak Tangguhan</td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';
	//$accounts=get_gl_accounts_in(array('12401','12901','12902','12999','12101','12102'));
	$accounts=get_gl_accounts_in(array('12901','12902','12999','12101','12102'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+abs($tot['debit']-$tot['credit']);
	}
	$totalaset=$total+$totalaset;
	$tableheader .= '<tr><td colspan="2">Aset Lain Lain</td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';

	$tableheader .= '<tr><td colspan="2"><b>Jumlah Aset</td>
		<td align="right"><b>'.price_format($totalaset).'</b></td>
	</tr>';


	$total=0;
	$tableheader .= '<tr><td class="tableheader" colspan="3" bgcolor="#dee7ec"><b>Liabilitas</b></td></tr>';
	$totalliab=0;
	//$accounts=get_gl_accounts_in(array('21210','21220','21260','21270','21250','21230','21240','21280'));
	$accounts=get_gl_accounts_in(array('21210','21220','21260','21270','21250','21230','21240','21280'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+abs($tot['debit']-$tot['credit']);
	}
	$totalliab=$total+$totalliab;
	$tableheader .= '<tr><td colspan="2">Utang Pajak</td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';

	//$accounts=get_gl_accounts_in(array('21140','21901','21301'));
	$accounts=get_gl_accounts_in(array('21140','21110','21120','21130'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+abs($tot['debit']-$tot['credit']);
	}
	$totalliab=$total+$totalliab;
	$tableheader .= '<tr><td colspan="2">Beban Masih Harus Dibayar </td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';

	$accounts=get_gl_accounts_in(array('21601'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+abs($tot['debit']-$tot['credit']);
	}
	$totalliab=$total+$totalliab;
	$tableheader .= '<tr><td colspan="2">Liabilitas Imbalan Kerja  </td>
		<td align="right">'.price_format(abs($total)).'</td>
	</tr>';

	//$tableheader .= '<tr><td colspan="3">Utang Lain-lain   </td></tr>';
	$accounts=get_gl_accounts_in(array('22901'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+abs($tot['debit']-$tot['credit']);
	}
	//$totalliab=$total+$totalliab;

	//$accounts=get_gl_accounts_in(array('21701'));
	$accounts=get_gl_accounts_in(array('21701','21901','21902','21903'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+abs($tot['debit']-$tot['credit']);
	}
	$totalliab=$total+$totalliab;
	$tableheader .= '<tr><td colspan="2">Utang Lain-lain</td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';
	$tableheader .= '<tr><td colspan="2"><b>Jumlah Liabilitas</td>
		<td align="right"><b>'.price_format(($totalliab)).'</b></td>
	</tr>';


	$tableheader .= '<tr><td class="tableheader" colspan="3" bgcolor="#dee7ec"><b>Ekuitas</b></td></tr>';
	$totaleku=0;
	$accounts=get_gl_accounts_in(array('31100'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+abs($tot['debit']-$tot['credit']);
	}
	$totaleku=$total+$totaleku;
	$tableheader .= '<tr><td colspan="2">Modal Saham, Modal Dasar </td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';
	$accounts=get_gl_accounts_in(array('32000'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+abs($tot['debit']-$tot['credit']);
	}
	$totaleku=$total+$totaleku;
	/*
	$tableheader .= '<tr><td colspan="2">Modal Saham, Modal Dasar </td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';
	*/
	$accounts=get_gl_accounts_in(array('32000','39800','39700'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+abs($tot['debit']-$tot['credit']);
	}
	$totaleku=$total+$totaleku;
	$tableheader .= '<tr><td colspan="2">Komponen Ekuitas Lainnya  </td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';

	//$tableheader .= '<tr><td colspan="3">Saldo Laba  </td></tr>';
	$accounts=get_gl_accounts_in(array('38000'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+abs($tot['debit']-$tot['credit']);
	}
	//$totaleku=$total+$totaleku;

	$accounts=get_gl_accounts_in(array('39000'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+abs($tot['debit']-$tot['credit']);
	}	
	$total=$total+netprof($begin,$to);
	//$totaleku=$total+$totaleku;

	$accounts=get_gl_accounts_in(array('37000'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$total=$total+abs($tot['debit']-$tot['credit']);
	}
	$totaleku=$total+$totaleku;

	$tableheader .= '<tr><td colspan="2">Saldo Laba  </td>
		<td align="right">'.price_format(($total)).'</td>
	</tr>';
	$tableheader .= '<tr><td colspan="2"><b>Jumlah Ekuitas</b></td>
		<td align="right"><b>'.price_format(($totaleku)).'</b></td>
	</tr>';
	$tableheader .= '<tr><td colspan="2"><b>Jumlah Liabilitas & Ekuitas</b></td>
		<td align="right"><b>'.price_format(($totaleku)+($totalliab)).'</b></td>
	</tr>';


	$tableheader .='</center></table>';
	return $tableheader;
}
//----------------------------------------------------------------------------------------------------

if (get_post('Export')) 
{
	
	$header='
	<style>
	.tableheader{
		background-color:#000 !important;
		color:#fff !important;
	}
	.tablestyle{
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">Laporan Posisi Keuangan</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td>
		</tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 638px;">';
	div_start('balance_tbl');
	echo display_bs();
	div_end();
	echo '</td></tr></table>';
}elseif(get_post('PrintPdf')){

// add a page
	$header='<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">Laporan Posisi Keuangan</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	$header.= '<tr><td style="width: 648px;">';
	$header.=display_bs();
	$header.= '</td></tr></table>';
$pdf->AddPage();
// output the HTML content
$html=$header;
//$html.=display_bs();
$pdf->writeHTML(@$html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_BS.pdf', 'I');	

}else{
	gl_inquiry_controls();
	div_start('balance_tbl');

	if (get_post('Show')) {
		echo '<center><table border="0" cellpadding="4" width="80%">
		<tr>		
			<td align="center" style="font-weight:bold;">Laporan Posisi Keuangan</td>
		</tr>
		<tr>
			<td align="center" style="font-weight:bold;">Periode '.@$blnnya.'</td>
		</tr>';
		echo '<tr>		
			<td align="center">';
		echo display_bs();
		echo '</td></tr></table></center>';
	}
	//if ($_POST['Show']) 
	//echo display_bs();
	div_end();
	end_page();
}

