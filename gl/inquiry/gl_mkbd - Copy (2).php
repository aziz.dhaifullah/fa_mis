<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/config_mssql.php");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/db/gl_db_mkbd.inc");
include_once($path_to_root . "/gl/includes/siap.inc");
include_once($path_to_root . "/gl/includes/siar.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd51.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd52.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd53.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd54.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd55.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd56.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd57.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd58.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd59.inc");
include_once($path_to_root . "/gl/includes/ui/mkbd_vd510.inc");
/*
*/
$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

if (get_post('Export')) 
{
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"MKBD.xls\"");
	header("Cache-Control: max-age=0");
	
	
}else{
page(_($help_context = "MKBD"), false, false, '', $js);
}

//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}
if (get_post('Export')) 
{
	

}

if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format(0);

//----------------------------------------------------------------------------------------------------

function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER);
	start_row();
   // gl_all_accounts_list_cells(_("Account:"), 'account', null, false, false, _("All Accounts"));
	date_cells(_("Date:"), 'TransFromDate', '', null, -user_transaction_days());
	//date_cells(_("End Date:"), 'TransToDate');

	echo "<td>"._("Lampiran").":</td>\n";
	echo "<td>";
	
$lampiran = array(
	_("V.D.5-1"),_("V.D.5-2"),_("V.D.5-3"),_("V.D.5-4"),_("V.D.5-5"),_("V.D.5-6"),_("V.D.5-7"),_("V.D.5-8"),_("V.D.5-9"),_("V.D.5-10")
);
	echo array_selector('lampiran', null, $lampiran);
	echo "</td>\n";
	echo "<td>"._("Direktur Bertanggung Jawab").":</td>\n";
	echo "<td>";
	echo '<input type="text" name="direktur">';
	echo "</td>\n";
    end_row();
	end_table();

	start_table(TABLESTYLE_NOBORDER);
	start_row();

	echo '<td colspan="4">&nbsp;</td>';
	submit_cells('Show',_("Show"),'align="right"','', 'default');
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel"></td>';
	
	$lampir2=$_POST['lampiran']+1;
	echo '
	<style>
		.export{
			align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
		function xls(){
			//console.log("aa");
			$(".tablestyle").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "MKBD_VD5-'.$lampir2.'",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
	//submit_cells('Export Excel',_("Export"),'','', 'default');
	end_row();
	end_table();

	echo '<hr>';
    end_form();
}

//----------------------------------------------------------------------------------------------------


function show_results()
{
	global $path_to_root, $systypes_array,$SysPrefs;

	
	$lampir=$_POST['lampiran']+1;
	$ctype=$lampir;
	$colspan = ($dim == 2 ? "6" : ($dim == 1 ? "5" : "4"));

	if ($_POST["account"] != null)
		display_heading($_POST["account"]. "&nbsp;&nbsp;&nbsp;".$act_name);
	if(get_post('lampiran')==0)
	$tentang='tentang Laporan Neraca Percobaan Harian - Aset';
	if(get_post('lampiran')==1)
	$tentang='tentang Laporan Neraca Percobaan Harian - Liabilitas dan Ekuitas';
	if(get_post('lampiran')==2)
	$tentang='tentang Laporan Ranking Liabilities';
	if(get_post('lampiran')==3)
	$tentang='tentang Perhitungan Risiko Terkonsentrasinya Efek Reksa Dana	';
	if(get_post('lampiran')==4)
	$tentang='tentang Perhitungan Pengembalian Haircut Atas Portofolio Efek yang Ditutup Dengan Lindung Nilai	';
	if(get_post('lampiran')==5)
	$tentang='tentang Laporan Buku Pembantu Dana	';
	if(get_post('lampiran')==6)
	$tentang='tentang Laporan Buku Pembantu Efek	';
	if(get_post('lampiran')==7)
	$tentang='tentang Perhitungan Persyaratan Minimal Modal Kerja Bersih Disesuaikan	';
	if(get_post('lampiran')==8)
	$tentang='tentang Laporan Perhitungan Modal Kerja Bersih Disesuaikan	';
	if(get_post('lampiran')==9)
	$tentang='tentang  Laporan  Data Pendukung Modal Kerja Bersih Disesuaikan	';
	$title='Formulir Nomor V.D.5-'.$lampir.' '.$tentang;		
		

	$company = get_company_prefs();

	start_table(TABLESTYLE, "width='90%'", 10);
	echo '<tr>';
	echo '<td colspan="3" align="right">Lampiran :  '.$lampir.'</td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td colspan="3" align="right">Peraturan Nomor :  V.D.5</td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td colspan="3" align="center">'.$title.'</td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td>Perusahaan Efek</td><td>:</td><td>'.$company['coy_name'].'</td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td>Tanggal</td><td width="5%">:</td><td>'.$_POST['TransFromDate'].'</td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td width="30%">Direktur yang Bertanggung Jawab Atas <br>Laporan Ini</td><td>:</td><td>'.$_POST['direktur'].'</td>';
	echo '</tr>';
	end_table(1);
	start_table(TABLESTYLE, "width='90%'", 6);
	/*
	$no=6;
	echo '<tr>';
	echo '<th align="center">'.$no.'</th><th align="center">A</th><th align="center">B</th>';
	echo '</tr>';
	$no++;
	echo '<tr>';
	echo '<th align="center">'.$no.'</th><th width="60%" align="center">Nama Akun</th><th width="30%" align="center">Saldo</th>';
	echo '</tr>';
	*/
	if(get_post('lampiran')==0)
		echo display_v1_2();
	if(get_post('lampiran')==1)
		echo display_v2_2();
	if(get_post('lampiran')==2)
	display_v3();
	if(get_post('lampiran')==3)
		echo display_v42();
	//display_v4();
	if(get_post('lampiran')==4)
	display_v5();
	if(get_post('lampiran')==5)
		echo display_v62();
	//display_v6();
	if(get_post('lampiran')==6)
	display_v7();
	if(get_post('lampiran')==7)
		echo display_v8_2();
	
	//display_v8();
	if(get_post('lampiran')==8)
		echo display_v9_2();
	//display_v9();
	if(get_post('lampiran')==9)
	display_v10();
/*
*/
	end_table(1);
}
//----------------------------------------------------------------------------------------------------
function display_v1_2(){
	$lists=get_mkbd_list(1);
	$tab="<tr><th align=center>6</th><th colspan=3 style='font-weight:bold;' align=center>A</th><th align=center>B</th></tr>
	 <tr><th align=center>7</th><th colspan=3 style='font-weight:bold;' align=center>Nama Akun</th><th align=center >Saldo</th></tr>";
	$tasetl=0;$tasetl2=0;
	while ($list = db_fetch($lists))
	{
		$accs=array();
		$spas='';
		for($a=0;$a<$list['level'];$a++){
			$spas.='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		$tab.='<tr height=20>
		  <td height=20 class=xl161>'.$list['no'].'</td>
		  <td colspan=3 class=xl253>'.$spas.$list['label'].'</td>
		  ';
		$saldo=0;
		$accs=get_mkbd_coa($list['id']);
		//print_r($accs);
		if(count($accs)>0)
			$saldo=get_totals($accs,$_POST['TransFromDate']);

		if($list['no']>=8 and $list['no']<100)
			$tasetl=$tasetl+$saldo;
		if($list['no']>=102 and $list['no']<112)
			$tasetl2=$tasetl2+$saldo;
		if($list['no']==100)
			$saldo=$tasetl;
		if($list['no']==112)
			$saldo=$tasetl2;
		if($list['no']==113)
			$saldo=$tasetl2+$tasetl;
		$tab.=' <td class=xl518 align="right">'.price_format_pr(($saldo)).'</td>';
		$tab.='</tr>';
		//$tab.=$list['label'];
	}
	return $tab;
}
function total_v1_2(){
	$lists=get_mkbd_list(1);
	$tasetl=0;$tasetl2=0;
	while ($list = db_fetch($lists))
	{
		$accs=array();
		$spas='';
		
		$saldo=0;
		$accs=get_mkbd_coa($list['id']);
		//print_r($accs);
		if(count($accs)>0)
			$saldo=get_totals($accs,$_POST['TransFromDate']);

		if($list['no']>=8 and $list['no']<100)
			$tasetl=$tasetl+$saldo;
		if($list['no']>=102 and $list['no']<112)
			$tasetl2=$tasetl2+$saldo;
		if($list['no']==100)
			$saldo=$tasetl;
		if($list['no']==112)
			$saldo=$tasetl2;
		if($list['no']==113)
			$saldo=$tasetl2+$tasetl;
		//$tab.=$list['label'];
	}
	return array($tasetl,$tasetl2);
}
function display_v2_2(){
	$lists=get_mkbd_list(2);
	$tab="<tr><th align=center>6</th><th colspan=3 style='font-weight:bold;' align=center>A</th><th align=center>B</th></tr>
	 <tr><th align=center>7</th><th colspan=3 style='font-weight:bold;' align=center>Nama Akun</th><th align=center >Saldo</th></tr>";
	$tasetl2=0;$teku=0;
	while ($list = db_fetch($lists))
	{
		$accs=array();
		$spas='';
		for($a=0;$a<$list['level'];$a++){
			$spas.='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		$tab.='<tr height=20>
		  <td height=20 class=xl161>'.$list['no'].'</td>
		  <td colspan=3 class=xl253>'.$spas.$list['label'].'</td>
		  ';
		$saldo=0;
		$accs=get_mkbd_coa($list['id']);
		//print_r($accs);
		if(count($accs)>0)
			$saldo=get_totals($accs,$_POST['TransFromDate']);

		if($list['no']>=121 and $list['no']<164)
			$tasetl2=$tasetl2+$saldo;
		if($list['no']==164)
			$saldo=$tasetl2;
		if($list['no']>=167 and $list['no']<172)
			$teku=$teku+$saldo;
		if($list['no']==172)
			$saldo=$teku;
		if($list['no']==173)
			$saldo=$tasetl2+$teku;
		$tab.=' <td class=xl518 align="right">'.price_format_pr(($saldo)).'</td>';
		$tab.='</tr>';
		//$tab.=$list['label'];
	}
	return $tab;
}
function total_v2_2(){
	$lists=get_mkbd_list(2);
	$tasetl2=0;$teku=0;
	while ($list = db_fetch($lists))
	{
		$accs=array();
		$spas='';
		
		$saldo=0;
		$accs=get_mkbd_coa($list['id']);
		//print_r($accs);
		if(count($accs)>0)
			$saldo=get_totals($accs,$_POST['TransFromDate']);

		if($list['no']>=121 and $list['no']<164)
			$tasetl2=$tasetl2+$saldo;
		if($list['no']==164)
			$saldo=$tasetl2;
		if($list['no']>=167 and $list['no']<172)
			$teku=$teku+$saldo;
		if($list['no']==172)
			$saldo=$teku;
		if($list['no']==173)
			$saldo=$tasetl2+$teku;
		$tab.=' <td class=xl518 align="right">'.price_format_pr(($saldo)).'</td>';
		$tab.='</tr>';
		//$tab.=$list['label'];
	}
	return array($tasetl2);
}
function display_v62(){
	$lists=get_mkbd_list(6);
	$tab='<tr height=20>
  <td height=20 class=xl246>6</td>
  <td colspan=3 class=xl580>A</td>
  <td class=xl250>B</td>
  <td class=xl249>C</td>
  <td class=xl251>D</td>
  <td class=xl250>E</td>
 </tr>
 <tr class=xl244 height=40>
  <td height=40 class=xl252>7</td>
  <td colspan=3 class=xl582 style=>SALDO DEBIT BUKU PEMBANTU DANA</td>
  <td class=xl376>Saldo</td>
  <td class=xl376>Terafiliasi</td>
  <td class=xl377>Tidak
  Terafiliasi</td>
  <td class=xl313>&nbsp;</td>
 </tr>';
 $cols=7;

	$tableheader='<table class="TABLESTYLE" cellpadding="6" width="100%">';
	//$cols=0;
	$header='<tr>
		  <td>&nbsp;</td>
		  <td>Nama Bank</td>
		  <td>Sendiri/ Nasabah</td>
		  <td>Nomor Rekening</td>
		  <td>Mata Uang</td>
		  <td>Saldo</td>
		  <td>Saldo <br>(dalam Rp)</td>
		</tr>';
	
	$tableheader.=$header;
	//kas
	$accounts=get_gl_accounts_in(array('11111','11112','11114','11140'));
	$total2=0;
	$tgl=explode('/', $_POST['TransFromDate']);
	$begin=date('m/d/Y',strtotime($tgl[2].'-'.$tgl[1].'-01'));
	while ($account = db_fetch($accounts))
	{	
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $_POST['TransFromDate'], false, true);
		$total2=$total2+($tot['debit']-$tot['credit']);
	}
	$totalkas=$total2;

	$tableheader.='<tr>
		  <td>&nbsp;</td>
		  <td>Kas</td>
		  <td>S</td>
		  <td></td>
		  <td>IDR</td>
		  <td></td>
		  <td align="right">'.price_format_pr(($total2)).'</td>
		</tr>';	
	//bank	
	$total2=0;
	$accounts=get_gl_accounts_in(array('11152','11154','11155','11156','11157','11158','11159','11160','11161','11163','11164'));
	while ($account = db_fetch($accounts))
	{	
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $_POST['TransFromDate'], false, true);
		$total2=$total2+($tot['debit']-$tot['credit']);
		$curr=get_bank_curr($account["account_code"]);
		$saldo=($tot['debit']-$tot['credit']);
		$saldo2=0;
		if($curr!='IDR'){
			$saldo=($tot['debit']-$tot['credit']);
			$saldo2=0;			
		}
		$tableheader.='<tr>
			  <td>&nbsp;</td>
			  <td>'.$account["account_name"].'</td>
			  <td>S</td>
			  <td></td>
			  <td>IDR</td>
			  <td align="right">'.price_format_pr($saldo2).'</td>
			  <td align="right">'.price_format_pr($saldo).'</td>
			</tr>';
	}
	$totalbank=$total2;
	$tableheader .= '</table>';


	$tdebit=0;
	while ($list = db_fetch($lists))
	{
		$accs=array();
		$spas='';
		for($a=0;$a<$list['level'];$a++){
			$spas.='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		$tab.='<tr height=20>
		  <td height=20 class=xl161>'.$list['no'].'</td>
		  <td colspan=3 class=xl253>'.$spas.$list['label'].'</td>
		  ';
		$saldo=0;
		/*
		if($list['no']==8 or $list['no']==23)
			$saldo=$totalkas+$totalbank;
		if($list['no']==17)
			$saldo=$totalkas;
		if($list['no']==19)
			$saldo=$totalbank;
		//echo $list['id'];
		print_r($accs);
		*/
		$accs=get_mkbd_coa($list['id']);
		//print_r($accs);
		if(count($accs)>0)
			$saldo=get_totals($accs,$_POST['TransFromDate']);

		if($list['no']>=8 and $list['no']<15)
			$tdebit=$tdebit+$saldo;
		if($list['no']==15)
			$saldo=$tdebit;
		$tab.=' <td class=xl518 align="right">'.price_format_pr(($saldo)).'</td>';
		$tab.='
		  <td class=xl396>&nbsp;</td>
		  <td class=xl397>&nbsp;</td>
		  <td class=xl398>&nbsp;</td>
		 </tr>';
		//$tab.=$list['label'];
	}
	$tab.='<tr height=20>
		  <td height=20 class=xl161>24</td>
		  <td colspan='.$cols.' class=xl253 >Rincian Saldo masing-masing Rekening Bank </td>
		  ';

	$tab.=' </tr>';
	$tab.='<tr height=20>
		  <td colspan="'.($cols+2).'">';
	$tab.=$tableheader;
	$tab.=' </tr>';
	return $tab;
}

function display_v42(){
	$lists=get_mkbd_list(6);
	$tab='<tr>
  <td align="center">6</td>
  <td align="center">A</td>
  <td align="center">B</td>
  <td align="center">C</td>
  <td align="center">D</td>
  <td align="center">E</td>
  <td align="center">F</td>
  <td align="center">G</td>
  <td align="center">H</td>
 </tr>
 <tr>
  <td>7</td>
  <td>Jenis Reksa Dana</td>
  <td>Nama Reksa Dana</td>
  <td>Afiliasi/Tidak Terafiliasi</td>
  <td>Nilai Aktiva Bersih Unit Penyertaan Reksa Dana yang dimiliki</td>
  <td>Nilai Aktiva Bersih per Reksa Dana</td>
  <td>Perhitungan Ranking Liabilities</td>
  <td>Batasan yang dapat dimiliki untuk MKBD</td>
  <td>Kelebihan di atas batasan <br> (Kolom D - Kolom G)</td>
 </tr>';

	$prods=get_prod_siar_dp();
	$no=1;
	for($a=0;$a<count($prods[0]);$a++){

		$tgl=explode('/', $_POST['TransFromDate']);
		$tgl2=date('Y-m-d',strtotime($tgl[2].'-'.$tgl[1].'-'.$tgl[0]));
		$nab=get_aum_siar($prods[1][$a],$tgl2);
		$aum=$nab['navvalue']*$nab['OutstandingUnits'];
		$batasan=$aum*0.25;
		 $tab.='
		  <td>14.'.$no.'</td>
		  <td>RDPS</td>
		  <td>'.$prods[0][$a].'</td>
		  <td>Afiliasi</td>
		  <td align="right">'.price_format_pr($aum).'</td>
		  <td align="right">'.price_format_pr($aum).'</td>
		  <td>Kelebihan atas 25% NAB</td>
		  <td align="right">'.price_format_pr($batasan).'</td>
		  <td align="right">'.price_format_pr($aum-$batasan).'</td>
		  </tr>';
		  $no++;
	}
	return $tab;
}
function display_v8_2(){
	$lists=get_mkbd_list(8);
	$tab="
	<tr>
  <td>6</td>
  <td>A</td>
  <td>B</td>
  <td>C</td>
  <td>D</td>
  <td>E</td>
 </tr>
 <tr>
  <td>7</td>
  <td>Keterangan</td>
  <td>Formulir</td>
  <td>Lajur</td>
  <td>Baris</td>
  <td>Nilai</td>
 </tr>";
	$tasetl=0;$tasetl2=0;
	$t16=0;$t18=200000000;$t20=0;$t22=0;$t23=0;$t24=0;
	while ($list = db_fetch($lists))
	{
		$accs=array();
		$spas='';
		for($a=0;$a<$list['level'];$a++){
			$spas.='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		$nilai=0;
		$formulir='';$lajur='';$baris='';
		if($list['no']==8){
			$tl=total_v2_2();
			$nilai=$tl[0];
			$formulir='V.D.5-2';
			$lajur='B';
			$baris='164';
			$t16=$t16+$nilai;
		}
		if($list['no']==9){
			$formulir='V.D.5-3';
			$lajur='B';
			$baris='31';
			$t16=$t16+$nilai;
		}
		if($list['no']==11){
			$formulir='V.D.5-2';
			$lajur='B';
			$baris='163';
			$t16=$t16-$nilai;
		}
		if($list['no']==13){
			$formulir='V.D.5-2';
			$lajur='B';
			$baris='146';
			$t16=$t16-$nilai;
		}
		if($list['no']==14){
			$formulir='V.D.5-2';
			$lajur='B';
			$baris='147';
			$t16=$t16-$nilai;
		}
		if($list['no']==15){
			$formulir='V.D.5-2';
			$lajur='B';
			$baris='148';
			$t16=$t16-$nilai;
		}
		if($list['no']==16){
			$nilai=$t16;
		}
		if($list['no']==18){
			$nilai=$t18;
		}
		if($list['no']==19){
			$nilai=$t16*0.0625;
		}
		if($list['no']==20){
			$nilai=$t16*0.0625;
			if($nilai<$t18)
				$nilai=$t18;
			$t20=$nilai;
		}
		if($list['no']==22){
			$nilai=$t18;
			$t22=$t22+$nilai;
		}
		if($list['no']==23){
			$t23=$t23+$nilai;
		}
		if($list['no']==24){
			$nilai=$t23*0.001;
			$t24=$nilai;
		}
		if($list['no']==25){
			$nilai=$t22+$t24;
		}
		if($list['no']==26){
			$nilai=$t20+$t22+$t24;
		}
		$tab.='<tr height=20>
		  <td height=20 class=xl161>'.$list['no'].'</td>
		  <td>'.$spas.$list['label'].'</td>
		  <td>'.$formulir.'</td>
		  <td>'.$lajur.'</td>
		  <td>'.$baris.'</td>
		  ';
		$tab.=' <td class=xl518 align="right">'.price_format_pr(($nilai)).'</td>';
		$tab.='</tr>';
		//$tab.=$list['label'];
	}
	return $tab;
}

function display_v9_2(){
	$lists=get_mkbd_list(9);
	$tab="
	<tr>
  <td>6</td>
  <td>A</td>
  <td>B</td>
  <td>C</td>
  <td>D</td>
  <td>E</td>
  <td>F</td>
  <td>G</td>
 </tr>
 <tr>
  <td>7</td>
  <td>Keterangan</td>
  <td>Formulir</td>
  <td>Lajur</td>
  <td>Baris</td>
  <td>Jumlah</td>
  <td>Faktorisasi</td>
  <td>Total</td>
 </tr>";
	$tasetl=0;$tasetl2=0;
	$t13=0;$t16=0;$t18=200000000;$t20=0;$t22=0;$t23=0;$t24=0;
	while ($list = db_fetch($lists))
	{
		$accs=array();
		$spas='';
		for($a=0;$a<$list['level'];$a++){
			$spas.='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		$nilai=0;
		$faktor=0;
		$total=0;
		$formulir='';$lajur='';$baris='';
		if($list['no']==9){
			$tl=total_v1_2();
			$nilai=$tl[0];
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='100';
			$t13=$t13+$nilai;
		}
		if($list['no']==11){
			$tl=total_v2_2();
			$nilai=$tl[0];
			$formulir='V.D.5-2';
			$lajur='B';
			$baris='164';
			$t13=$t13-$nilai;
		}
		if($list['no']==12){
			$formulir='V.D.5-3';
			$lajur='B';
			$baris='31';
			$t13=$t13-$nilai;
		}
		if($list['no']==13){
			$nilai=$t13;
		}

		if($list['no']==15){
			$nilai=$t13;
		}

		if($list['no']==17){
			$formulir='V.D.5-2';
			$lajur='B';
			$baris='163';
		}
		if($list['no']==20){
			$total=$t13;
		}
		if($list['no']==24){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='16';
		}
		if($list['no']==26){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='18';
		}
		if($list['no']==28){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='20';
			$faktor=0.05;
		}
		if($list['no']==29){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='21';
			$faktor=0.05;
		}
		if($list['no']==30){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='22';
			$faktor=1;
		}
		if($list['no']==31){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='23';
			$faktor=1;
		}
		if($list['no']==33){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='59';
			$faktor=0.05;
		}
		if($list['no']==35){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='61';
			$faktor=0.05;
		}
		if($list['no']==36){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='62';
			$faktor=0.075;
		}
		if($list['no']==37){
			$formulir='V.D.5-1';
			$lajur='B';
			$baris='63';
			$faktor=0.1;
		}
		$tab.='<tr height=20>
		  <td height=20 class=xl161>'.$list['no'].'</td>
		  <td>'.$spas.$list['label'].'</td>
		  <td>'.$formulir.'</td>
		  <td>'.$lajur.'</td>
		  <td>'.$baris.'</td>
		  <td align="right">'.price_format_pr(($nilai)).'</td>
		  <td align="right">'.price_format_pr(($faktor)).'</td>
		  <td align="right">'.price_format_pr(($total)).'</td>
		  ';
		//$tab.=' <td class=xl518 align="right">'.price_format_pr(($nilai)).'</td>';
		$tab.='</tr>';
		//$tab.=$list['label'];
	}
	return $tab;
}
function get_totals($accounts,$tgl){
	$accounts=get_gl_accounts_in($accounts);
	$total2=0;
	$tgl=explode('/', $tgl);
	$begin=date('m/d/Y',strtotime($tgl[2].'-'.$tgl[1].'-01'));
	while ($account = db_fetch($accounts))
	{	
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $_POST['TransFromDate'], false, true);
		$total2=$total2+($tot['debit']-$tot['credit']);
	}
	return $total2;

}
if (get_post('Export')) 
{
}
	else{
gl_inquiry_controls();

div_start('trans_tbl');
}
if (get_post('Show') || get_post('Export'))
    show_results();

if (get_post('Export')) 
{
}
	else{
div_end();

//----------------------------------------------------------------------------------------------------

end_page();
}

