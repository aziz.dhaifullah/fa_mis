<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLANALYTIC';
$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/gl/includes/siap.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

$js = "";
if (user_use_date_picker())
	$js = get_js_date_picker();
if (get_post('Export')) 
{
	//include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"PL.xls\"");
	header("Cache-Control: max-age=0");
	
	
}else{
page(_($help_context = "Profit & Loss Drilldown"), false, false, "", $js);
}
$compare_types = array(
	_("Accumulated"),
	_("Period Y-1"),
	_("Budget")
);
//----------------------------------------------------------------------------------------------------
// Ajax updates

if (get_post('Show')) 
{
	$Ajax->activate('pl_tbl');
}

if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];	
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Compare"]))
	$_POST["Compare"] = $_GET["Compare"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["AccGrp"]))
	$_POST["AccGrp"] = $_GET["AccGrp"];

//----------------------------------------------------------------------------------------------------

function display_type ($type, $typename, $from, $to, $begin, $end, $compare, $convert,
	$dimension=0, $dimension2=0, $drilldown, $path_to_root,$comps,$cv, $NoZero, $detail = true)
{
	global $levelptr, $k;
		
	$code_per_balance = 0;
	$code_acc_balance = 0;
	$per_balance_total = 0;
	$acc_balance_total = 0;
	unset($totals_arr);
	$totals_arr = array();
	$sum_total = 0;
	$totalperbalance = 0;
	$cekdataperbalance = 0;
	$sum_total2 = array();
	$from  = begin_fiscalyear();
	if (date('Y',strtotime(sql2date($to))) < date('Y',strtotime(sql2date($from)))) {
	 	$from = date('d/m/Y', strtotime('-1 year', strtotime(sql2date($from))));
	}
	//print_r($from);
	/*
	echo "-";
	print_r(date('Y-m-d',strtotime(sql2date($from))));
	echo "_";*/
	//Get Accounts directly under this group/type
	$result = get_gl_accounts(null, null, $type);	

	$total=array();
	$total2=array();
	while ($account=db_fetch($result))
	{
		//$per_balance = get_gl_trans_from_to($from, $to, $account["account_code"], $dimension, $dimension2);
		/*
		if ($compare == 2)
			$acc_balance = get_budget_trans_from_to($begin, $end, $account["account_code"], $dimension, $dimension2);
		else
			$acc_balance = get_gl_trans_from_to($begin, $end, $account["account_code"], $dimension, $dimension2);
			*/
		//if (!$per_balance && !$acc_balance)
		//	continue;
		
		//if ($drilldown && $levelptr == 0){
			if ($NoZero == 0) {
				for($a=0;$a<count($comps);$a++){
	        		$per_balance = get_gl_trans_from_to($from, $to, $account["account_code"], $comps[$a]['kode'], $dimension2);
	        		$cekdataperbalance += $per_balance;
	        	}
	        	//echo $cekdatanetbalance;
	        	if ($cekdataperbalance == 0) {
	        		//if (($account["account_code"] != '33') AND ($account["account_code"] != '32')) {
	        			continue;
	        		//}
	        	}
			}
			$cekdataperbalance = 0;
			if ($detail) {
				$url = "<a href='$path_to_root/gl/inquiry/gl_account_inquiry.php?TransFromDate=" 
					. $from . "&TransToDate=" . $to . "&Dimension=" . $dimension . "&Dimension2=" . $dimension2 
					. "&account=" . $account['account_code'] . "'>" . $account['account_code'] 
					." ". $account['account_name'] ."</a>";				
					
				start_row("class='stockmankobg'");
				if(@$_POST['AccCode']==1){
					label_cell('&nbsp;&nbsp;&nbsp;&nbsp;'.$account['account_code']." ".$account['account_name']);
				}else{
					label_cell('&nbsp;&nbsp;&nbsp;&nbsp;'.$account['account_name']);
				}
			}

			for($a=0;$a<count($comps);$a++){
				$per_balance = get_gl_trans_from_to($from, $to, $account["account_code"], $comps[$a]['kode'], $dimension2);
				$totalperbalance += $per_balance;
				if ($cv == 1) {
					if (@$comps[$a]['kode_siap']!='') {
						$tot2 = get_tb_ims($comps[$a]['kode_siap'],$account["account_code"],date2sql($_POST["TransToDate"]));
						//echo $tot2;
						$totalperbalance += $tot2;
					}				
				}
				if ($detail) {
					amount_cell($totalperbalance);
				}
				$sum_total += ($totalperbalance);
				@$total[$a]+= $totalperbalance;
				$totalperbalance = 0;
			}
			if ($detail) {
				amount_cell($sum_total);
			}
			$sum_total = 0;
			//$totalperbalance = 0;
			//$tot2 = 0;
			//amount_cell($acc_balance * $convert);
			//amount_cell(Achieve($per_balance, $acc_balance));
			end_row();
		//}
			
		$code_per_balance += $per_balance;
		@$code_acc_balance += $acc_balance;
	}

	$levelptr = 1;
	
	//Get Account groups/types under this group/type
	/*$result = get_account_types(false, false, $type);
	while ($accounttype=db_fetch($result))
	{	

		for($a=0;$a<count($comps);$a++){
			$totals_arr = display_type($accounttype["id"], $accounttype["name"], $from, $to, $begin, $end, 
				$compare, $convert, $comps[$a]['kode'], $dimension2, $drilldown, $path_to_root,$comps);
			$per_balance_total += $totals_arr[0];
			$acc_balance_total += $totals_arr[1];
			$total[$a]+=$totals_arr[0];
		}
	}
	//Display Type Summary if total is != 0 
	//if (($code_per_balance + $per_balance_total + $code_acc_balance + $acc_balance_total) != 0)
	//{
		//if ($drilldown && $type == $_POST["AccGrp"]){		
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_('Total') . " " . $typename);

			for($a=0;$a<count($comps);$a++){
				amount_cell((@$total[$a] + @$total2[$a]) * $convert);
				$sum_total += ((@$total[$a] + @$total2[$a]) * $convert);
			}
			amount_cell($sum_total);
			//amount_cell(($code_acc_balance + $acc_balance_total) * $convert);
			//amount_cell(Achieve(($code_per_balance + $per_balance_total), ($code_acc_balance + $acc_balance_total)));
			end_row();
		//}
		//START Patch#1 : Display  only direct child types
		$acctype1 = get_account_type($type);
		$parent1 = $acctype1["parent"];
		//if ($drilldown && $parent1 == $_POST["AccGrp"])
		//END Patch#2		
		//elseif ($drilldown && $type != $_POST["AccGrp"])
		//{	
			$url = "<a href='$path_to_root/gl/inquiry/profit_loss.php?TransFromDate=" 
				. $from . "&TransToDate=" . $to . "&Compare=" . $compare . "&Dimension=" . $dimension . "&Dimension2=" . $dimension2
				. "&AccGrp=" . $type ."'>" . $type . " " . $typename ."</a>";
				
			alt_table_row_color($k);
			label_cell($url);
			for($a=0;$a<count($comps);$a++){
				amount_cell(($total[$a] + $total2[$a]) * $convert);
				$sum_total += (($total[$a] + $total2[$a]) * $convert);
			}	
			amount_cell($sum_total);
			//amount_cell(($code_per_balance + $per_balance_total) * $convert);
			//amount_cell(($code_acc_balance + $acc_balance_total) * $convert);
			//amount_cell(Achieve(($code_per_balance + $per_balance_total), ($code_acc_balance + $acc_balance_total)));
			end_row();
		//}
	//}
	*/
	
	$totals_arr[0] = $code_per_balance + $per_balance_total;
	$totals_arr[1] = $code_acc_balance + $acc_balance_total;
	$totals_arr[3] = $total;
	//print_r($comps);
	return $totals_arr;
}	
	
function Achieve($d1, $d2)
{
	if ($d1 == 0 && $d2 == 0)
		return 0;
	elseif ($d2 == 0)
		return 999;
	$ret = ($d1 / $d2 * 100.0);
	if ($ret > 999)
		$ret = 999;
	return $ret;
}

function inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
	echo "<center>";
    start_table(TABLESTYLE_NOBORDER);
	$date = today();
	if (!isset($_POST['TransToDate']))
		$_POST['TransToDate'] = end_month($date);
	if (!isset($_POST['TransFromDate']))
		$_POST['TransFromDate'] = add_days(end_month($date), -user_transaction_days());
    //date_cells(_("From:"), 'TransFromDate');
	date_cells(_("Date:"), 'TransToDate');
	if ($dim >= 1)
		dimensions_list_cells(_("Dimension")." 1:", 'Dimension', null, true, " ", false, 1);
	if ($dim > 1)
		dimensions_list_cells(_("Dimension")." 2:", 'Dimension2', null, true, " ", false, 2);
	//level_list(_("Level"), 'Level', null, true, " ", false, 1);
	echo "<td>Level :</td>";
	echo "<td>";
	echo '<select name="level" id="level">';
	if ($_SESSION["wa_current_user"]->com_id != 0) {
		$sql = "SELECT lvl from 0_companies where id = ".$_SESSION["wa_current_user"]->com_id;
		$result =db_query($sql);
		$lvl = db_fetch($result);
		$lvl = $lvl['lvl'] + 1;
	}else{
		$lvl = 1;
	}
		echo '<option value="0">All</option>';
	for ($i=$lvl; $i <= 5 ; $i++) { 
		echo '<option value="'.$i.'">'.$i.'</option>';
	}
	echo '</select>';
	echo "</td>\n";
	echo "</tr>";
	echo "<tr>";
	check_cells(_("With zero values"), 'NoZero', null);
	check_cells(_("Account Code"), 'AccCode', null);
	/*check_cells(_("Calculate with SIAP"), 'CalcSiap', null);*/
	echo "</tr>";
	echo '<tr style="height: 40px !important;">';
	echo '<td colspan=9 align=center><button class="ajaxsubmit" type="submit" aspect="default" name="Show" style="margin-right: 15px !important;" id="Show" value="Show"><img src="../../themes/default/images/ok.gif" height="12" alt=""><span>Show</span></button>';
	//submit_cells('Show',_("Show"),'','', 'default');
	echo '<input type="submit" class="export" name="Export" id="Export" Value="Export to Excel"></td>';
	/*echo '<td></td>';
	echo '<td></td>';
	echo '</tr>';
	*/
	echo "</tr>";
	end_table();
	echo "</center>";
	//hidden('TransFromDate');
	hidden('AccGrp');
}

//----------------------------------------------------------------------------------------------------
function get_tb_ims($kodesiap,$accode,$date){
	$datatbims = get_tb2_ims($date);
	
	if(isset($datatbims[$date][$kodesiap][$accode])){
		return $datatbims[$date][$kodesiap][$accode];
	}else{
		return 0;
	}

}

function check_subcomp($subcomid){
	$sql = "SELECT count(*) as nilai from 0_companies where parent = ".$subcomid;
	//$comps=array();
	$result =db_query($sql);
	$count=db_fetch($result);
	//var_dump($comps);exit;
	return $count;
}

function subcomp($parid){
	$sql = "SELECT comp.id,comp.kode_siap,comp.kode, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = id) as nsub
	FROM ".TB_PREF."companies comp
	where comp.parent=".$parid;
	$comps=array();
	$subcomp=array();
	if($result=db_query($sql)){
		while($row=db_fetch($result)){
			$check_subcomp = check_subcomp($row['id']);
			if($check_subcomp['nilai'] > 0){
				$subcomp = subcomp($row['id']);
				$comps = array_merge($comps,$subcomp);
			}else{
				$comps[] = $row;	
			}
		}
	}
	//var_dump($comps);exit;
	return $comps;
}
function display_profit_and_loss($compare)
{
	global $path_to_root, $compare_types;
	$totaltype2 = 0.0;
	$totalparse = array();
	$totaltype3 = 0.0;

	if (!isset($_POST['Dimension']))
		$_POST['Dimension'] = 0;
	if (!isset($_POST['Dimension2']))
		$_POST['Dimension2'] = 0;
	$dimension = $_POST['Dimension'];
	$dimension2 = $_POST['Dimension2'];

	$from = $_POST['TransToDate'];
	$to = $_POST['TransToDate'];
	
	if (isset($_POST["AccGrp"]) && (strlen($_POST['AccGrp']) > 0))
		$drilldown = 1; // Deeper Level
	else
		$drilldown = 0; // Root level
	
	if ($compare == 0 || $compare == 2)
	{
		$end = $to;
		if ($compare == 2)
		{
			$begin = $from;
		}
		else
			$begin = begin_fiscalyear();
	}
	elseif ($compare == 1)
	{
		$begin = add_months($from, -12);
		$end = add_months($to, -12);
	}
	
	div_start('pl_tbl');

	start_table(TABLESTYLE, "width='90%'");

	$comps=array();
	$subcomp=array();
	if (@$_POST['level'] == '') {
		$sql = "SELECT comp.id,comp.kode,comp.kode_siap,comp.nama,comp2.nama as namaparent, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = ".$_SESSION["wa_current_user"]->com_id.") as nsub
		FROM ".TB_PREF."companies comp
		LEFT JOIN ".TB_PREF."companies comp2 ON comp2.id = comp.parent
		where comp.id=".$_SESSION["wa_current_user"]->com_id;
		if(@$_SESSION["wa_current_user"]->com_id=='0')
		$sql = "SELECT comp.id,comp.kode,comp.nama,comp.kode_siap,comp.parent,comp2.nama as namaparent, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = comp.id ) as nsub
		FROM ".TB_PREF."companies comp
		LEFT JOIN ".TB_PREF."companies comp2 ON comp2.id = comp.parent
		order by kode asc";
		
		if($result=db_query($sql)){
			while($row=db_fetch($result)){
				$dim=$row['kode'];
				if($_POST['Dimension']!=0)
				$dim=$_POST['Dimension'];
				//print_r($comps);
				if($dim==$row['kode']){
					/*if (($row['lvl'] == 0) || ($row['lvl'] == 1)) {
						$comps[]=$row;
					}else{*/
					
					//}
					if(@$row['nsub'] > 0){
						$subcomp=subcomp($row['id']);
					}else{
						//if(!in_array($comps,$row['kode'])){
						$comps[]=$row;
						//}					
					}
				}
				
			}
			if (($_SESSION["wa_current_user"]->com_id != 0) OR ($_POST['Dimension']!=0)) {
				$comps=array_merge($comps,$subcomp);
			}
		}
	}else{
		if ($_POST['level'] == '0') {
			$_POST['level'] = 5;
		}

		if ($_POST['Dimension']!=0) {
			$sql = "select id,lvl from ".TB_PREF."companies where kode = '".$_POST['Dimension']."'";
			$result = db_query($sql);
			while ($row=db_fetch($result)) {
				$id = $row['id'];
				$lvl = $row['lvl'];
			}
			if ($_POST['level'] <= $lvl) {
				$lvl = $lvl;
			}else{
				$lvl = $_POST['level'];
			}
		}else{
			$id = $_SESSION["wa_current_user"]->com_id;
			$lvl = $_POST['level'];
		}

		$sql = "select  lvl, nama, id, kode, kode_siap, parent, CONCAT(kode,' ', nama) as ref
				from    (select * from ".TB_PREF."companies
				         order by parent, id) comps_sorted,
				        (select @pv := ".$id.") initialisation
				where   (CASE WHEN NOT EXISTS (SELECT id from ".TB_PREF."companies where parent = @pv) THEN
						find_in_set(id, @pv) ELSE find_in_set(parent, @pv) END)
				and 	length(@pv := concat(@pv, ',', id))
				and		(lvl BETWEEN lvl AND ".$lvl.")
				group by parent";
		
		if($result=db_query($sql)){
			while($row=db_fetch($result)){
				$comps[]=$row;
			}
		}
	}

	/*$tableheader =  "<tr><td class='tableheader'>" . _("Group/Account Name") . "</td>";
	for($a=0;$a<count($comps);$a++){
		$tableheader .= '<td class="tableheader">'.$comps[$a]["ref"].'</td>';
	}
	$tableheader .= '<td class="tableheader">Total</td>';
    //$tableheader .="<td class='tableheader'>" . _("Period") . "</td>";
    $tableheader .="</tr>";	*/
	
	if (!$drilldown) //Root Level
	{
		$salesper = 0.0;
		$salesacc = 0.0;	
		$totaltype = 0.0;	
		$totalpendapatan = array();
		$totalbiaya = array();
		//Get classes for PL
		$classresult = get_account_classes(false, 0);
		$total=array();
		while ($class = db_fetch($classresult))
		{
			$class_per_total = 0;
			$class_acc_total = 0;
			$total2=array();
			$convert = get_class_type_convert($class["ctype"]); 		
			$ctype = $class["ctype"];
			$cekTypeTotal = 0.0;
			
			//Print Class Name	
			table_section_title($class["class_name"],(2+count($comps)));	
			//echo $tableheader;
			
			//Get Account groups/types under this group/type
			$typeresult = get_account_types(false, $class['cid'], -1);
			$k = 0; // row color
			while ($accounttype=db_fetch($typeresult))
			{

				//if ($TypeTotal[0] != 0 || $TypeTotal[1] != 0 )
				//{
					if (@$_POST['NoZero'] == 0) {
						for($a=0;$a<count($comps);$a++){
							$TypeTotal = display_type($accounttype["id"], $accounttype["name"], $from, $to, $begin, $end, $compare, $convert, 
						@$comps[$a]['kode'], $dimension2, @$drilldown,  $path_to_root, $comps, @$check_value, @$_POST['NoZero'], false);
							$cekTypeTotal += @$TypeTotal[3][$a];
						}					
						if ($cekTypeTotal == 0) {
							continue;
						}
					}
					$cekTypeTotal = 0; 
					$url = "<a href='$path_to_root/gl/inquiry/profit_loss.php?TransFromDate=" 
						. $from . "&TransToDate=" . $to . "&Compare=" . $compare . "&Dimension=" . $dimension . "&Dimension2=" . $dimension2
						. "&AccGrp=" . $accounttype['id'] ."'>" . $accounttype['id'] . " " . $accounttype['name'] ."</a>";
						
					alt_table_row_color($k);
					//label_cell($url);
					$count2 = count($comps)+2;
					if(@$_POST['AccCode']==1){
						echo '<tr class="evenrow">
						<td style="vertical-align:top !important;" colspan="'.$count2.'" >'.$accounttype['id']." ".$accounttype['name'].'</td>
						</tr>';
					}else{
						echo '<tr class="evenrow">
						<td style="vertical-align:top !important;" colspan="'.$count2.'" >'.$accounttype['name'].'</td>
						</tr>';
					}
					/*for($a=0;$a<count($comps);$a++){	
						$class_per_total += $TypeTotal[0];
						$class_acc_total += $TypeTotal[1];	
						@$total[$a]+=$TypeTotal[0];
						@$total2[$a]+=$TypeTotal[0];
						//amount_cell($TypeTotal[0] * $convert);
						label_cell('');
						$totaltype += ($TypeTotal[0] * $convert);
						//$totaltype += 1;
					}*/
					//amount_cell($totaltype);
					//label_cell('');
					//$totaltype = 0;
					//amount_cell($TypeTotal[1] * $convert);
					//amount_cell(Achieve($TypeTotal[0], $TypeTotal[1]));
					end_row();
					//for($a=0;$a<count($comps);$a++){
					if (check_value('CalcSiap')) {
						$check_value = 1;
					}else{
						$check_value = 0;
					}	
					$TypeTotal = display_type($accounttype["id"], $accounttype["name"], $from, $to, $begin, $end, $compare, $convert, 
						@$comps[$a]['kode'], $dimension2, @$drilldown,  $path_to_root, $comps, $check_value, @$_POST['NoZero']);
					for($a=0;$a<count($comps);$a++){
						@$total2[$a]+=$TypeTotal[3][$a];
					}
				//}
			}
			
			//Print Class Summary
			
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_('Total') . " " . $class["class_name"]);
			//print_r($total2[$a]);
			for($a=0;$a<count($comps);$a++){	
				amount_cell(@$total2[$a]);
				if ($ctype == 4) {
					@$totalpendapatan[$a] += @$total2[$a];
				}else if($ctype == 6){
					@$totalbiaya[$a] += @$total2[$a]  * -1;
				}
				$totaltype2 += (@$total2[$a]);
				@$total[$a]+= $total2[$a];
			}
			amount_cell($totaltype2);
			$totaltype2 = 0;
			//print_r($totalpendapatan);
			//amount_cell($class_acc_total * $convert);
			//amount_cell(Achieve($class_per_total, $class_acc_total));
			end_row();			
			
			$salesper += $class_per_total;
			$salesacc += $class_acc_total;
		}
		
		start_row("class='inquirybg' style='font-weight:bold'");
		label_cell(_('Calculated Return'));
		for($a=0;$a<count($comps);$a++){	
			amount_cell($totalpendapatan[$a] - $totalbiaya[$a]);
			//print_r("Pend".$totalpendapatan[$a]);
			//print_r("Biaya".$totalbiaya[$a]);
			//amount_cell($total[$a] *$convert);
			$totaltype3 += ($totalpendapatan[$a] - $totalbiaya[$a]);
		}
		amount_cell($totaltype3);
		$totaltype3 = 0;
		//amount_cell($salesper *-1);
		//amount_cell($salesacc * -1);
		//amount_cell(achieve($salesper, $salesacc));
		end_row();		

	}
	/*else 
	{
		//Level Pointer : Global variable defined in order to control display of root 
		global $levelptr;
		$levelptr = 0;
		
		$accounttype = get_account_type($_POST["AccGrp"]);
		$classid = $accounttype["class_id"];
		$class = get_account_class($classid);
		$convert = get_class_type_convert($class["ctype"]); 
		
		//Print Class Name	
		table_section_title($_POST["AccGrp"] . " " . get_account_type_name($_POST["AccGrp"]),(4+count($comps)));	
		echo $tableheader;
		
		$classtotal = display_type($accounttype["id"], $accounttype["name"], $from, $to, $begin, $end, $compare, $convert, 
			$dimension, $dimension2, $drilldown, $path_to_root,$comps);
		
	}*/
		

	end_table(1); // outer table
	div_end();
}


//----------------------------------------------------------------------------------------------------
if (get_post('Export')) 
{
	if ($_SESSION["wa_current_user"]->com_id == 0) {
		$sql = "SELECT comp.nama FROM ".TB_PREF."companies comp
		where comp.id=1";
	}else{
		$sql = "SELECT comp.nama FROM ".TB_PREF."companies comp
		where comp.id=".$_SESSION["wa_current_user"]->com_id;
		
	}

	$result = db_query($sql);
	$fetch = db_fetch($result);

	$header='
	<style>
	.inquirybg2{
		font-weight:bold !important;
		font-size:12px !important;
	}
	.tableheader{
		background-color:#EE7600 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0">
		<tr><td align="center">&nbsp;</td></tr>
		<tr><td style="width:2%;">&nbsp;&nbsp;</td>
			<td>
			<table border="0">
				<tr>
					<td width="100%" align="center"  style="background-color:#bfbfbf;color:#ee3000;font-size:12px;"><b>'.$fetch['nama'].'</b></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:20px;color:#ee3000;font-weight:bold;">Profit Loss</td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;color:#ee3000;font-weight:bold;">'.@$blnnya.'</td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td>
				</tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 600px;">';
	div_start('pl_tbl');
	echo display_profit_and_loss();
	div_end();
	echo '</td></tr></table>
		</td></tr>
		<tr><td></td><td align="center" style="background-color:#EE7600;border:#000 1px solid;">&nbsp;</td></tr>
	</table>';
}else{

start_form();

inquiry_controls();
div_start('pl_tbl');
if (get_post('Show')) {
	echo display_profit_and_loss(get_post('Compare'));
}
div_end();

end_form();

end_page();

}