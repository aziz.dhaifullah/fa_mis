<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLANALYTIC';
$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/gl/includes/siap.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/inquiry/netprofit.php");
include_once($path_to_root . "/gl/inquiry/getpl_detail.php");
include_once($path_to_root . "/includes/db/siapjournal.php");

journal_siap_mis(null, $_SESSION['wa_current_user']->com_id);
//print_r(begin_fiscalyear());exit;
$js = "";
if (user_use_date_picker())
	$js = get_js_date_picker();

if (get_post('Export')) 
{
	//include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"TB.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
page(_($help_context = "Trial Balance "), false, false, "", $js);
}


$k = 0;
$pdeb = $pcre = $cdeb = $ccre = $tdeb = $tcre = $pbal = $cbal = $tbal = 0;

//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('balance_tbl');
}

if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$thnto=@$_POST['tahunfrom'];
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER,2);

	$date = today();
		if (!isset($_POST['TransToDate']))
			$_POST['TransToDate'] = end_month($date);
		if (!isset($_POST['TransFromDate']))
			$_POST['TransFromDate'] = add_days(end_month($date), -user_transaction_days());
	echo '<tr>';
	echo "<td>"._("Date").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	/*
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	*/
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo '</tr>';
	echo '<tr>';
    //label_cells(_("&nbsp;"), 'TransToDate');
    //hidden('TransToDate');
	echo '<input type="hidden" id="TransFromDate" name="TransFromDate" value="">';
	//echo "<td>"._("Date To").":</td>\n";
	date_cells(_("&nbsp;"), 'TransToDate');
	dimensions_list_cells(_("Dimension")." 1:", 'Dimension', null, true, " ", false, 1);
	/*
	if ($dim >= 1)
		dimensions_list_cells(_("Dimension")." 1:", 'Dimension', null, true, " ", false, 1);
	if ($dim > 1)
		dimensions_list_cells(_("Dimension")." 2:", 'Dimension2', null, true, " ", false, 2);
	*/
	echo '</tr>';
	echo "<td>Level :</td>";
	echo "<td>";
	echo '<select name="level" id="level">';
	if ($_SESSION["wa_current_user"]->com_id != 0) {
		$sql = "SELECT lvl from 0_companies where id = ".$_SESSION["wa_current_user"]->com_id;
		$result =db_query($sql);
		$lvl = db_fetch($result);
		$lvl = $lvl['lvl'] + 1;
	}else{
		$lvl = 1;
	}
		echo '<option value="-1">Default</option>';
		echo '<option value="0">All</option>';
	for ($i=$lvl; $i <= 5 ; $i++) { 
		echo '<option value="'.$i.'">'.$i.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo '<tr>';
	check_cells(_("With zero values"), 'NoZero', null);
	check_cells(_("Account Code"), 'AccCode', null);
	//check_cells(_("Calculate with SIAP"), 'CalcSiap', null);
	//check_cells(_("Title"), 'headtitle', null);
	echo "</tr>";
	echo '<tr style="height: 40px !important;">';
	echo '<td colspan=9 align=center><button class="ajaxsubmit" type="submit" aspect="default" name="Show" style="margin-right: 15px !important;" id="Show" value="Show"><img src="../../themes/default/images/ok.gif" height="12" alt=""><span>Show</span></button>';
	//submit_cells('Show',_("Show"),'','', 'default');
	echo '<input type="submit" class="export" name="Export" id="Export" Value="Export to Excel"></td>';
	/*echo '<td></td>';
	echo '<td></td>';
	echo '</tr>';
	*/
	echo "</tr>";
	echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}

	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
		function pilblnfrom(bln){
	 		console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		console.log(a.value);
	 		pilblnto(bln);

	 	}
	 	function pilblnto(bln){
	 		//if(bln<10)
	 		//	bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 		console.log(a.value);
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;
	 		console.log(a.value);	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	
	 		console.log(a.value); 		
	 		//var e = document.getElementById("bulanto");
			//e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "TB",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
    end_table();
    end_form();
}
//----------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------

	if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanfrom'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

	$begin=date('d/m/Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	//print_r($begin);
	if($blnto!='')
	$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, @$blnto, @$thnto)));

function get_tb_ims($kodesiap,$accode,$date){
	$datatbims = get_tb2_ims($date);
	
	if(isset($datatbims[$date][$kodesiap][$accode])){
		return $datatbims[$date][$kodesiap][$accode];
	}else{
		return 0;
	}

}

function check_subcomp($subcomid){
	$sql = "SELECT count(*) as nilai from 0_companies where parent = ".$subcomid;
	//$comps=array();
	$result =db_query($sql);
	$count=db_fetch($result);
	//var_dump($comps);exit;
	return $count;
}

function subcomp($parid){
	$sql = "SELECT comp.id,comp.kode, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = id) as nsub
	FROM ".TB_PREF."companies comp
	where comp.parent=".$parid;
	$comps=array();
	$subcomp=array();
	if($result=db_query($sql)){
		while($row=db_fetch($result)){
			$check_subcomp = check_subcomp($row['id']);
			if($check_subcomp['nilai'] > 0){
				$subcomp = subcomp($row['id']);
				$comps = array_merge($comps,$subcomp);
			}else{
				$comps[] = $row;	
			}
		}
	}
	//var_dump($comps);exit;
	return $comps;
}

function display_bs(){
	//print_r(subcomp(8));
	//print_r(check_subcomp(8));
	$totalnilai = 0;
	$cdeb = 0;
	$ccre = 0;
	$tdeb = 0;
	$tcre = 0;
	$total = 0;
	$colspan=1;
	$cols=4;
	//echo @$_POST['Dimension'];
	if($_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=$_POST['bulanfrom'];
		$blnto=$_POST['bulanfrom'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$begin=date('d/m/Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	//$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));
	$tglto=date('d',strtotime(date2sql($_POST['TransToDate'])));
	if($tglto==cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto))
	$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));
	else
	$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.$tglto));

	echo '<center><table class="tablestyle" width="100%" cellpadding="3" border="0">';
	echo '<tr>
	<td class="tableheader" bgcolor="#dee7ec" rowspan="2"><b>Account</b></td>';
	$th2='';
	$comps=array();
	$subcomp=array();

	if (@$_POST['level'] == '') {
		$sql = "SELECT comp.id,comp.kode,comp.nama,comp2.nama as namaparent, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = ".$_SESSION["wa_current_user"]->com_id.") as nsub
		FROM ".TB_PREF."companies comp
		LEFT JOIN ".TB_PREF."companies comp2 ON comp2.id = comp.parent
		where comp.id=".$_SESSION["wa_current_user"]->com_id;
		if(@$_SESSION["wa_current_user"]->com_id=='0')
		$sql = "SELECT comp.id,comp.kode,comp.nama,comp.parent,comp2.nama as namaparent, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = comp.id ) as nsub
		FROM ".TB_PREF."companies comp
		LEFT JOIN ".TB_PREF."companies comp2 ON comp2.id = comp.parent
		order by kode asc";
		


		if($result=db_query($sql)){
			while($row=db_fetch($result)){
				$dim=$row['kode'];
				if($_POST['Dimension']!=0)
				$dim=$_POST['Dimension'];
				//print_r($comps);
				if($dim==$row['kode']){
					/*if (($row['lvl'] == 0) || ($row['lvl'] == 1)) {
						$comps[]=$row;
					}else{*/
					
					//}

					if(@$row['nsub'] > 0){
//						@$_POST['level'] == '';
						$subcomp=subcomp($row['id']);
					}else{
						//if(!in_array($comps,$row['kode'])){
						$comps[]=$row;
						//}					
					}
				}
				
			}
			if (($_SESSION["wa_current_user"]->com_id != 0) OR ($_POST['Dimension']!=0)) {
				$comps=array_merge($comps,$subcomp);
			}
		}
	}else{
		if ($_POST['level'] == '0') {
			$_POST['level'] = 5;
		}

		if ($_POST['Dimension']!=0) {
			$sql = "select id,lvl from ".TB_PREF."companies where kode = '".$_POST['Dimension']."'";
			$result = db_query($sql);
			while ($row=db_fetch($result)) {
				$id = $row['id'];
				$lvl = $row['lvl'];
			}
			if ($_POST['level'] == $lvl) {
				$lvl = $lvl;
			}else{
				$lvl = $_POST['level'];
			}

			if ($_POST['level'] == -1) {
				$sql = "select id,lvl from ".TB_PREF."companies where id = '".$id."'";
				$result = db_query($sql);
				while ($row=db_fetch($result)) {
					$lvl = $row['lvl'];
				}
				$lvl = $lvl + 1;
			}
		}else{
			$id = $_SESSION["wa_current_user"]->com_id;
			$lvl = $_POST['level'];

			/*if ($_POST['level'] == -1) {
				$sql = "select id,lvl from ".TB_PREF."companies where id = '".$id."'";
				$result = db_query($sql);
				while ($row=db_fetch($result)) {
					$lvl = $row['lvl'];
				}
				$lvl = $lvl + 1;
			}*/
		}
		if ($_SESSION["wa_current_user"]->com_id == 0 AND $_POST['Dimension'] == 0 AND $_POST['level'] == -1) {
			$id = $_SESSION["wa_current_user"]->com_id;
			$lvl = 1;
		}

		$sql = "select  lvl, nama, id, kode,parent, CONCAT(kode,' ', nama) as ref
				from    (select * from ".TB_PREF."companies
				         order by parent, id) comps_sorted,
				        (select @pv := ".$id.") initialisation
				where   (CASE WHEN NOT EXISTS (SELECT id from ".TB_PREF."companies where parent = @pv) THEN
						find_in_set(id, @pv) ELSE find_in_set(parent, @pv) END)
				and 	length(@pv := concat(@pv, ',', id))
				and		(lvl BETWEEN lvl AND ".$lvl.")
				group by parent";
		
		if($result=db_query($sql)){
			while($row=db_fetch($result)){
				$comps[]=$row;
			}
		}
	}
	//print_r($lvl);
	//var_dump($comps);
	//echo $_POST['Dimension'];
	$th2.= '<tr>';
	for($a=0;$a<count($comps);$a++){
		$colspan++;
		echo '<td class="tableheader" bgcolor="#dee7ec"><b>'.$comps[$a]['ref'].'</b></td>';
		//echo '<td class="tableheader" bgcolor="#dee7ec" colspan="4"><b>'.$comps[$a]['ref'].'</b></td>';
		/*$th2.='<td class="tableheader" bgcolor="#dee7ec"><b>Debit</b></td>
			<td class="tableheader" bgcolor="#dee7ec"><b>Credit</b></td>
			<td class="tableheader" bgcolor="#dee7ec"><b>YTD Debit	</b></td>
			<td class="tableheader" bgcolor="#dee7ec"><b>YTD Credit
		    </b></td>';*/
	}
	echo '<td class="tableheader" bgcolor="#dee7ec"><b>Total</b></td>';
    echo '</tr>';

	$th2.='</tr>';
    echo $th2;
	
	

	$totalaset=0;
	//echo '<tr><td colspan="'.(1+$blnto).'">Kas dan Setara Kas</td></tr>';
	//get_account_from_to
	$ttlall=0;$ttlall2=0;
	//asset lancar
	//$accounts=get_account_from_to('10000','12000');
	$accounts = get_chart_accounts_search('');

	$notsub=array('16699');
	$totalsub=array('11140','11164','11208','11305','11499','11590'
					,'11699','11706','12102','12212','12222','12232'
					,'12242','12251','12262','12301','12401','12999'
					,'21140','21280','21301','21601','21701','21903'
					,'22101','22901'
					,'39999');
	$totalsub2=array('11706','12102','12262');
	$totalsub3=array('12999','22901');
	$tnetasset='11706';
	$head=array();$headparent=array();$headparent2=array();

	$ttlcol=array();$ttlcol2=array();$ttlcol3=array();$ttlcol4=array();$ttlcolsub=array();
	$ttlalls=array();$ttlalls2=array();
	$ttlclass=0;$ttlclass2=0;$ttlclass3=0;$ttlclass4=0;
	$a=0;$n=0;$hp=0;$hp2=0;
	$path_to_root="../..";

	$header=0;
	$cdeb2=array();
	$ccre2=array();
	$tdeb2=array();
	$tcre2=array();
	$total2=array();
	$pltot[] = get_profit_and_loss($_POST['Dimension'],@$_POST['Dimension2'],$begin,$to,0,0,@$_POST['level']);
	$pltotlast[] = get_profit_and_loss_last($_POST['Dimension'],@$_POST['Dimension2'],$begin,$to,0,0,@$_POST['level']);
	/*
	$labaditahan = 0.0;
	$labaditahanp = 0.0;*/
	while ($account = db_fetch($accounts))
	{
		
		//if (strlen($account["accode"])>5)
		//continue;
		//print_r($comps);exit;
		//print_r(@$comps[$a]['kode']);exit;
		$cektot = 0;
		$nilaicoasiap = 0;
		$nilaicoanosiap = 0;
		for($a=0;$a<count($comps);$a++){
			$tot = get_balance($account["accode"], @$comps[$a]['kode'], @$_POST['Dimension2'], $begin, $to, false, true);
			
			$yearbegin = date('Y', strtotime(date2sql($to)));
			$fiscalyear = date('Y', strtotime(date2sql(begin_fiscalyear())));
			if ($yearbegin >= $fiscalyear) {
				$straccount = substr($account["accode"], 0,1);
				if ($straccount == 4 OR $straccount == 5) {
					//if ($ttl2 == 0) {
						$tot = get_balance($account["accode"], @$comps[$a]['kode'], @$_POST['Dimension2'], begin_fiscalyear(), $to, true, true);
					//}
				}
			}
			$cektot += $tot['debit']-$tot['credit'];
		
		}

		if (check_value("NoZero") == 0 && $cektot == 0 /*&& $account["accode"] != 33*/)
			continue;
		if ($account["accode"]=='39000')
			continue;
		if ($account["accode"]=='39000')
			continue;
		if ($account["inactive"]==1)
			continue;

		//echo @$account["inactive"]; 

		$kls=cek_type_account(@$account["name"]);
		//header
		//print_r(@$kls);
		if (check_value("headtitle")){
			if(!in_array(@$kls[4],$headparent2) && @$kls[6]==''){
				echo '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';
				echo '<tr class="inquirybg2"><td><b>&nbsp;'.@$kls[5].'</td><td colspan="'.($cols).'"><b>'.@$account["headparent"].'</td></tr>';
				$headparent2[]=@$kls[4];
				$hp2++;			
				//echo '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';
			}

			if(!in_array(@$kls[2],$headparent) && @$kls[3]!=@$account["name"]){
				//echo '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';
				echo '<tr class="inquirybg2"><td><b>&nbsp;'.@$kls[3].'</td><td colspan="'.($cols).'"><b>'.@$account["headparent"].'</td></tr>';
				$headparent[]=@$kls[2];
				$hp++;			
				//echo '<tr><td colspan="'.($colspan).'">&nbsp;</td></tr>';
			}else{
				$headparent[]=@$kls[2];
			}

			if(!in_array(@$account["name"],$head) && $header==0 ){
				echo '<tr class="inquirybg2"><td><b>&nbsp;'.$account["name"].'</td><td colspan="'.($cols).'"><b>'.@$account["headparent"].'</td></tr>';
				//$headparent[]=$account["headparent"];
				$head[$a]=@$account["name"];
			}
		}

		alt_table_row_color($k);

		$url = "&nbsp;&nbsp;&nbsp;&nbsp; <a href='$path_to_root/gl/inquiry/gl_account_inquiry.php?TransFromDate=" . @$_POST["TransFromDate"] . "&TransToDate=" . @$_POST["TransToDate"] . "&account=" . $account["accode"] . "&Dimension=" . @$_POST["Dimension"] . "&Dimension2=" . @$_POST["Dimension2"] . "'>" 
		. $account["account_code"] 
		. "</a>";
		
		//label_cell('');
		$acode='';
		//if (strlen($account["accode"])>5)
		//$acode='&nbsp;&nbsp;&nbsp;&nbsp;';
		if(@$_POST['AccCode']==1)
		$acode.=$account["account_code"].' - ';
		
		//label_cell('_'.$account["account_code2"]);

		
		//print_r($begin);exit;
		for($a=0;$a<count($comps);$a++){
			/*$curr = get_balance($account["accode"], @$comps[$a]['kode'], @$_POST['Dimension2'], $begin, $to, true, true);
			$dbt=0;
			$crd=0;
			$ttl=($curr['balance']);	

			if($ttl>0)
				$dbt=$ttl;
			if($ttl<0)
				$crd=abs($ttl);
			amount_cell($dbt);
			amount_cell($crd);*/
			//print_r($account);
			$ttl2=0;
			$dbt2=0;
			$crd2=0;
			$tot2 = 0;
			//echo date2sql($_POST["TransFromDate"]);exit;

			if($account['accode'] == '1140201' OR $account['accode'] == '42201' OR $account['accode'] == '1140101'){
				$getcoasiap = "SELECT amount FROM 0_gl_trans WHERE account = '".$account['accode']."' AND memo_ LIKE '%SIAP%' AND dimension_id = '".$comps[$a]['kode']."' AND tran_date ='".sql2date($to)."' ORDER BY tran_date DESC";
				//echo $sql;
				$endfiscallast = date('d/m/Y', strtotime('-1 year', strtotime(sql2date(begin_fiscalyear()))));
				$datacoasiap = db_query($getcoasiap);
				while ($amount2 = db_fetch($datacoasiap)) {
					$nilaicoasiap = $amount2['amount'];
				}
				if ($account['accode'] == '42201') {
					//echo $begin;
					$yearbegin = date('Y', strtotime(date2sql($to)));
					$fiscalyear = date('Y', strtotime(date2sql(begin_fiscalyear())));
					if ($yearbegin >= $fiscalyear) {
						$getcoanosiap = "SELECT amount FROM 0_gl_trans WHERE account = '".$account['accode']."' AND memo_ NOT LIKE '%SIAP%' AND dimension_id = '".$comps[$a]['kode']."' AND (tran_date BETWEEN '".sql2date(begin_fiscalyear())."' AND '".sql2date($to)."') ORDER BY tran_date DESC";	
					}else{
						$getcoanosiap = "SELECT amount FROM 0_gl_trans WHERE account = '".$account['accode']."' AND memo_ NOT LIKE '%SIAP%' AND dimension_id = '".$comps[$a]['kode']."' AND (tran_date BETWEEN '".sql2date($endfiscallast)."' AND '".sql2date($to)."') ORDER BY tran_date DESC";
					}
				}else{
					$getcoanosiap = "SELECT amount FROM 0_gl_trans WHERE account = '".$account['accode']."' AND memo_ NOT LIKE '%SIAP%' AND dimension_id = '".$comps[$a]['kode']."' AND (tran_date BETWEEN '".sql2date($endfiscallast)."' AND '".sql2date($to)."') ORDER BY tran_date DESC";
				}
				$datacoanosiap = db_query($getcoanosiap);
				while ($amount3 = db_fetch($datacoanosiap)) {
					$nilaicoanosiap += $amount3['amount'];
				}
				$ttl2 = $nilaicoanosiap + $nilaicoasiap;
				//echo  $nilaicoanosiap."+".$nilaicoasiap."=".$ttl2."<br>";
				//print_r($nilaicoasiap);
			}else{
				$yearbegin = date('Y', strtotime(date2sql($to)));
				$fiscalyear = date('Y', strtotime(date2sql(begin_fiscalyear())));
				if ($yearbegin >= $fiscalyear) {
					$straccount = substr($account["accode"], 0,1);
					if ($straccount == 4 OR $straccount == 5) {
						//if ($ttl2 == 0) {
							$tot = get_balance($account["accode"], @$comps[$a]['kode'], @$_POST['Dimension2'], begin_fiscalyear(), $to, true, true);
						//}
					}else{
						$tot = get_balance($account["accode"], @$comps[$a]['kode'], @$_POST['Dimension2'], $begin, $to, false, true);
					}
				}else{
					$tot = get_balance($account["accode"], @$comps[$a]['kode'], @$_POST['Dimension2'], $begin, $to, false, true);
				}
				$ttl2 = $tot['debit']-$tot['credit'];
			}

			
			/*if($account['accode'] == 1140201 OR $account['accode'] == 42201 OR $account['accode'] == 1140101){
				$sql = "SELECT amount FROM 0_gl_trans WHERE account = '".$account['accode']."' AND memo_ NOT LIKE '%SIAP%' AND dimension_id = '".$comps[$a]['kode']."' ORDER BY tran_date DESC";
				$result = db_query($sql);
				while ($amount = db_fetch($result)) {
					$ttl2 = $amount['amount'];
				}
			}else{*/
			/*}*/
			//echo $pltotlast[0][$a]; 
			if ($account["accode"] == 32) 
			{
				//if ($yearbegin >= $fiscalyear) {
					$ttl2 += $pltotlast[0][$a];
				//}else{
				//	$ttl2 += $pltot[0][$a];
				//}
			}
			/*if ($account["accode"] == 33 AND $to != date('d/m/Y', strtotime('-1 year', strtotime(sql2date(end_fiscalyear()))))) 
			{
				$ttl2 += $pltot[0][$a];
			}*/
			//print_r($tot);
			
			if (check_value("CalcSiap")) {
				if (@$comps[$a]['kode_siap']!='') {
					$tot2 = get_tb_ims($comps[$a]['kode_siap'],$account["accode"],date2sql($_POST["TransToDate"]));
					//echo $tot2;
					$ttl2 = $ttl2 + $tot2;
				}				
			}
			//$tot = get_tb_ims($comps[$a]['kode_siap'],$account["account_code"],$_POST["TransToDate"]);
			
			
			/*if($ttl2>0)
				$nilai=$ttl2;
			if($ttl2<0)
				$nilai="-".$ttl2;*/
			label_cell($acode.$account["account_name"]);
			amount_cell($ttl2);
			//amount_cell(@$crd2);

			//@$cdeb2[$a]+= $dbt;
			//$cdeb += $dbt;
			//@$ccre2[$a]+= $crd;
			//$ccre += $crd;
			@$total2[$a] += $ttl2;
			$total += $ttl2;
			//@$tcre2[$a]+= $crd2;
			//$tcre += $crd2;
		}
		//label_cell('');
		amount_cell($total);
		$totalnilai += $total;
		$total = 0;
		end_row();
		$n++;
	}	
	//print_r($tot);exit;
	echo '</tr>';
	echo $tableheader='<tr class="inquirybg" style="font-weight:bold"><td colspan="'.($colspan*$cols).'">&nbsp;</td></tr>';
	
		start_row("class='inquirybg2' style='font-weight:bold'");
		label_cell(_("Diff") ." - ".$_POST['TransToDate']);
		//amount_cell(@$pdeb);

		for($a=0;$a<count($comps);$a++){
			//amount_cell(@$cdeb2[$a]);
			//amount_cell(@$ccre2[$a]);
			amount_cell(@$total2[$a]);
			//amount_cell(@$tcre2[$a]);
		}
		amount_cell($totalnilai);
		end_row();
		echo $tableheader='<tr class="inquirybg" style="font-weight:bold"><td colspan="'.($colspan*$cols).'">&nbsp;</td></tr>';

	echo'</table></center>';
	//return $tableheader;
}
//----------------------------------------------------------------------------------------------------

if (get_post('Export')) 
{
	if ($_SESSION["wa_current_user"]->com_id == 0) {
		$sql = "SELECT comp.nama FROM ".TB_PREF."companies comp
		where comp.id=1";
	}else{
		$sql = "SELECT comp.nama FROM ".TB_PREF."companies comp
		where comp.id=".$_SESSION["wa_current_user"]->com_id;
		
	}

	$result = db_query($sql);
	$fetch = db_fetch($result);
	
	$header='
	<style>
	.inquirybg2{
		font-weight:bold !important;
		font-size:12px !important;
	}
	.tableheader{
		background-color:#EE7600 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0">
		<tr><td align="center">&nbsp;</td></tr>
		<tr><td style="width:2%;">&nbsp;&nbsp;</td>
			<td>
			<table border="0">
				<tr>
					<td width="100%" align="center"  style="background-color:#bfbfbf;color:#ee3000;font-size:12px;"><b>'.$fetch['nama'].'</b></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;font-size:20px;color:#ee3000;font-weight:bold;">Trial Balance</td>
				</tr>
				<tr>
					<td align="center" style="background-color:#bfbfbf;color:#ee3000;font-weight:bold;">>Tanggal '.@$to.'</td>
				</tr>
				<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td>
				</tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 600px;">';
	div_start('balance_tbl');
	echo display_bs();
	div_end();
	echo '</td></tr></table>
		</td></tr>
		<tr><td></td><td align="center" style="background-color:#EE7600;border:#000 1px solid;">&nbsp;</td></tr>
		</table>';
}elseif(get_post('PrintPdf')){

// add a page
	$header='<table border="0">
		<tr>
			<td width="80%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">Trial Balance</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">>Tanggal '.@$to.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	$header.= '<tr><td style="width: 648px;">';
	$header.=display_bs();
	$header.= '</td></tr></table>';
$pdf->AddPage();
// output the HTML content
$html=$header;
//$html.=display_bs();
$pdf->writeHTML(@$html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_BS.pdf', 'I');	

}else{
	gl_inquiry_controls();
	div_start('balance_tbl');

	if (get_post('Show')) {
		echo '<center><table border="0" cellpadding="4" width="80%">
		<tr>		
			<td align="center" style="font-weight:bold;">Trial Balance</td>
		</tr>
		<tr>
			<td align="center" style="font-weight:bold;">Tanggal '.@$to.'</td>
		</tr>';
		echo '<tr>		
			<td align="center">';
		echo display_bs();
		echo '</td></tr></table></center>';
	}
	//if ($_POST['Show']) 
	//echo display_bs();
	div_end();
	end_page();
}

