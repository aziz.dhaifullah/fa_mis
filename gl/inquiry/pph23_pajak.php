<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/siar.inc");
include_once($path_to_root . "/gl/includes/db/gl_db_banking.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

	//include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"Summary PPh Ps.23.xls\"");
	header("Cache-Control: max-age=0");
	
//----------------------------------------------------------------------------------------------------
// Ajax updates
//


if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format(0);


	if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
		$tgl1=$_POST['TransFromDate'];
		$tgl2=$_POST['TransToDate'];		
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=date('Y');
		$thnto=date('Y');
		$tgl1='01/'.$blnfrom.'/'.$thnfrom;
		$tgl2='31/'.$blnto.'/'.$thnto;		
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

//----------------------------------------------------------------------------------------------------

function show_results()
{
	global $Refs,$path_to_root, $systypes_array;

	/*
	start_table(TABLESTYLE2, "width='90%'", 10);
	
	$th = array(_("No"), _("Kode"), _("NPWP"),  _("Nama"));
	    			
	table_header($th);
	*/


	$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
	$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
	$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
	$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime(date2sql($_POST['TransToDate'])));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime(date2sql($_POST['TransToDate'])));
		}
	}

	$begin=date('d/m/Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));

	$tableheader='<center><table class="tablestyle" cellpadding="6" border="1" width="98%" style="font-size:10px !important;">';

	$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
	$tableheader .=  "
	<td class='tableheader' align='center'>" . _("Kode Form Bukti Potong") . "</td>
	<td class='tableheader' align='center'>" . _("Masa Pajak") . "</td>
	<td class='tableheader' align='center'>" . _("Tahun Pajak") . "</td>
	<td class='tableheader' align='center'>" . _("Pembetulan") . "</td>
	<td class='tableheader' align='center'>" . _("NPWP WP yang Dipotong") . "</td>
	<td class='tableheader' align='center'>" . _("Nama WP yang Dipotong") . "</td>
	<td class='tableheader' align='center'>" . _("Alamat WP yang Dipotong") . "</td>
	<td class='tableheader' align='center'>" . _("Nomor Bukti Potong") . "</td>
	<td class='tableheader' align='center'>" . _("Tanggal Bukti Potong") . "</td>";
	for($a=1;$a<=19;$a++){
		$no=$a;
		if($a==6)
			$no='6a';
		if($a==7)
			$no='6b';
		if($a==8)
			$no='6c';
		if($a==14)
			$no='6d1';
		if($a==15)
			$no='6d2';
		if($a==16)
			$no='6d3';
		if($a==17)
			$no='6d4';
		if($a==18)
			$no='6d5';
		if($a==19)
			$no='6d6';
		$tableheader .=  "
		<td class='tableheader' align='center'>" . _("Nilai Bruto ".$no) . "</td>
		<td class='tableheader' align='center'>" . _("Tarif ".$no) . "</td>
		<td class='tableheader' align='center'>" . _("PPh Yang Dipotong ".$no) . "</td>";
	}   
		$tableheader .=  "
		<td class='tableheader' align='center'>" . _("Jumlah Nilai Bruto ") . "</td>
		<td class='tableheader' align='center'>" . _(" Jumlah PPh Yang Dipotong ") . "</td>";

	$tableheader .=  "	
	</tr>
	";
	$tableheader.='</tr>';
	$sql="select a.*
from 0_gl_trans a
left join 0_voided c on a.type=c.type and a.type_no=c.id
where c.id IS NULL and account like '21220' and amount<0 and tran_date between '".date2sql($_POST['TransFromDate'])."' and '".date2sql($_POST['TransToDate'])."' order by a.tran_date asc";
	$result=db_query($sql, '');

		$no=1;$a=0;
		$ttljml=0;$ttlbayar=0;$ttlselisih=0;
		while($mfees = db_fetch_assoc($result))
		{
			//print_r($mfees);echo'<br>';
			if(($a % 2) == 1)
			$tableheader.='<tr class="evenrow">';
			else
			$tableheader.='<tr class="oddrow">';
			//print_r($mfees);echo '<br>';

			$person=get_counterparty_name(@$mfees['type'],@$mfees['types_no']);
			$noinv=@$mfees['reference'];

			$nama=@$mfees['supp_name'];

			//$nama=@$person['supp_name'];
			$npwp=@$mfees['gst_no'];
			$alamat=@$mfees['address'];
			$npwp=str_replace('.','',$npwp);
			$npwp=str_replace('-','',$npwp);
			

			$jasa=@$mfees['jasa'];
			$masa=date('m',strtotime($mfees['tran_date']));
			$thn=date('Y',strtotime($mfees['tran_date']));
			$tgl=date('d/m/Y',strtotime($mfees['tran_date']));

			$nomor=$Refs->gen_pajak($no,'23',$masa,$thn);

			$ppn=abs($mfees['amount']);
			$dpp=0;
			//$ppn=0;
			//var_dump($nama);
			/*$sql="select id,net_amount,fp from 0_trans_tax_details where trans_type='".@$mfees['trans_type']."' and trans_no='".@$mfees['trans_no']."' order by id asc";
			$query=db_query($sql, "The transactions for could not be retrieved");
			while ($myrow0 = db_fetch($query))
			{*/
				$id=@$mfees['idtax'];
				$dpp=abs(@$mfees['amount']);
				$fp=@$mfees['fp'];
				$fps=explode('.', $fp);
			//}
			//$nomor=$Refs->gen_pajak($no,'4(2)',date('m',strtotime($mfees['tran_date'])),date('Y',strtotime($mfees['tran_date'])));
			
			$memo=($mfees['memo_']);
			$kdpajak='F113306';
			$tableheader .="
				<td align='left'>&nbsp;" . @$kdpajak . "</td>
				<td align='left'>&nbsp;" . @$masa . "</td>
				<td align='right'>" . @$thn . "</td>
				<td align='right'>0</td>
				<td align='left'>&nbsp;" . @$npwp . "&nbsp;</td>
				<td align='left'>" . $nama ."</td>
				<td align='left'>" . @$alamat . "</td>
				<td align='left'>" . @$nomor . "</td>
				<td align='center'>" . @$tgl . "</td>";
			$dpps=array();$pphs=array();
			for($a=1;$a<=19;$a++){
				$dpps[$a]=0;$pphs[$a]=0;
				$tarif=15;
				if($a>=5)
					$tarif=2;

				if($jasa==9 && $a==5){
					$dpps[$a]=$dpp;
					$pphs[$a]=$dpp;
				}
				elseif($jasa==4 && $a==7){
					$dpps[$a]=$dpp;
					$pphs[$a]=$dpp;
				}
				elseif($jasa==6 && $a==8){
					$dpps[$a]=$dpp;
					$pphs[$a]=$dpp;
				}
				/*
				elseif($jasa==5 && $a==14){
					$dpps[$a]=$dpp;
					$pphs[$a]=$dpp;
				}
				else{
					if($a==14){
						$dpps[$a]=$dpp;
						$pphs[$a]=$dpp;
					}
				}
				*/
					if($a==14){
						if($jasa!=9 && $jasa!=4 && $jasa!=6){
							$dpps[$a]=$dpp;
							$pphs[$a]=$dpp;
						}
					}

				$tableheader .="
					<td align='right'>" .price_format(@$dpps[$a]) . "</td>
					<td align='right'>" . @$tarif . "</td>
					<td align='right'>" . price_format($pphs[$a]) . "</td>";
				//echo $jasa.' '.$dpp.' '.print_r($dpps);echo '<br>';
			}
				$tableheader .="
					<td align='right'>" .price_format(array_sum(@$dpps)) . "</td>
					<td align='right'>" .price_format(array_sum(@$pphs)). "</td>";
			$tableheader .="
				</tr>";
			$ttljml=$ttljml+$dpp;
			$ttlbayar=$ttlbayar+$ppn;
			$no++;
			$a++;
	    }/*
			$tableheader .="
				<td align='left'>&nbsp;</td>
				<td align='left'><b>Total</b> </td>
				<td align='left'>&nbsp;</td>
				<td align='left'>&nbsp;</td>
				<td align='right'><b>" . price_format($ttljml) . "</b></td>
				<td align='left'>&nbsp;</td>
				<td align='right'><b>" . price_format($ttlbayar) . "</b></td>
				<td align='left'>&nbsp;</td>
				</tr>";*/
    //}else{
    if($a==0){
    	$tableheader .= '<tr><td colspan="3" align="center"><b>No Data Available</b></td></tr>';
    }

    return $tableheader;
}
//----------------------------------------------------------------------------------------------------
	
	
	echo show_results();


