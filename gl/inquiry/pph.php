<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_TAXREP';
$path_to_root="../..";
include_once($path_to_root . "/includes/session.inc");


include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"Ringkasan Pajak.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
page(_($help_context = "Ringkasan Pajak"), false, false, '', $js);
}
//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}
		$blnfrom=date('m',strtotime(date2sql(@$_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql(@$_POST['TransFromDate'])));
		$thnfrom=date('Y',strtotime(date2sql(@$_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql(@$_POST['TransFromDate'])));
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

/*
if (get_post('TransFromDate') == "" && get_post('TransToDate') == "")
{
	$date = Today();
	$row = get_company_prefs();
	$edate = add_months($date, -$row['tax_last']);
	$edate = end_month($edate);
	$bdate = begin_month($edate);
	$bdate = add_months($bdate, -$row['tax_prd'] + 1);
	$_POST["TransFromDate"] = $bdate;
	$_POST["TransToDate"] = $edate;
}	
*/
//----------------------------------------------------------------------------------------------------

function tax_inquiry_controls()
{
    start_form();

    start_table(TABLESTYLE_NOBORDER);
    echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";

	echo '<tr>';
	echo '<tr>';
    date_cells(_("&nbsp;"), 'TransFromDate');
	date_cells(_("&nbsp;"), 'TransToDate');
	//check_cells(_("No zero values"), 'NoZero', null);
	//check_cells(_("Account Code"), 'AccCode', null);
	echo '<td></td>';
	echo '</tr>';
	start_row();

	echo '<td></td>';
	echo '<td></td>';
	//date_cells(_("Periode:"), 'TransFromDate', '', null, -user_transaction_days());
	//date_cells(_("to:"), 'TransToDate');
	submit_cells('Show',_("Show"),'','', 'default');
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">&nbsp;';
	echo '<input type="submit" class="export" name="PrintPdf" id="PrintPdf" Value="Print PDF"></td>';

    end_row();
echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Balance_sheet",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
	end_table();

    end_form();
}

//----------------------------------------------------------------------------------------------------

function show_results()
{
    /*Now get the transactions  */
	if(get_post('PrintPdf'))
	$tableheader ='<center><table class="tablestyle" width="98.5%" cellpadding="3" border="0">';
	else
	$tableheader ='<center><table class="tablestyle" width="80%" cellpadding="3">';

	$begin=$_POST['TransFromDate'];
	$to=$_POST['TransToDate'];

	$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
	$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
	$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
	$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime(date2sql($_POST['TransToDate'])));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime(date2sql($_POST['TransToDate'])));
		}
	}

	$begin=date('d/m/Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));
	$tglan=explode('/',$_POST['TransFromDate']);
	//pph21
	if(get_post('Export'))
	$tableheader .= '<tr><td class="tableheader" colspan="6" bgcolor="#dee7ec"><b>PPH 21</b> 
	</td></tr>';
	else
	$tableheader .= '<tr><td class="tableheader" colspan="6" bgcolor="#dee7ec"><b>PPH 21</b> 
	<a href="pph21.php?TransFromDate='.$_POST['TransFromDate'].'&TransToDate='.$_POST['TransToDate'].'&Show=Show" target="_blank">(Click To Detail)</a>
	</td></tr>';
	$tableheader .= '<tr>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>No</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec" colspan="2"><b>Golongan Penerima Penghasilan</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Jumlah Penerima Penghasilan</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Jumlah Penghasilan Bruto (Rp)</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>PPH yang dipotong (Rp)</b></td>';
	$tableheader .= '</tr>';
	$sql="select a.*
from 0_gl_trans a
left join 0_voided b on a.type=b.type and a.type_no=b.id
where account like '21210' and a.amount<0 
and tran_date between '".date2sql($_POST['TransFromDate'])."' 
and '".date2sql($_POST['TransToDate'])."' 
and b.id is null
order by a.counter asc";
	//$sql="select trans_type,trans_no,sum(net_amount) as dpp,sum(amount) as amount, memo from 0_trans_tax_details where tax_type_id=4 and tran_date like '".@$tglan[2]."-".@$tglan[1]."%' group by trans_type,trans_no order by id asc";
	$query=db_query($sql, "The transactions for could not be retrieved");
	$no=1;$jml=0;$jml2=0;$jmlgross=0;

		$tableheader .= '<tr>';
		$tableheader .= '<td colspan="6">&nbsp;</td>';
		$tableheader .= '</tr>';
	$counter=0;
	while ($myrow0 = db_fetch($query))
	{
		$amnt=$myrow0['amount'];
		$gross=$myrow0['gross'];
		$person=get_counterparty_name(@$myrow0['type'],@$myrow0['type_no']);
		$persons=explode('//',@$myrow0['person_id']);

		if($person=='')
			$person=@$persons[0];
		if($person=='')
			$person=@$myrow0['memo_'];
		if($person=='')
			$person=@$myrow0['reference'];
		/*
		$sql4="SELECT net_amount,rate 
		FROM `0_trans_tax_details` a
		left join 0_voided b on a.trans_type=b.type and a.trans_no=b.id
		WHERE `trans_type` = ".@$myrow0['type']."
		 and b.id is null AND `trans_no` = ".@$myrow0['type_no']." AND `amount` = ".abs($amnt)." ";
		$query4=db_query($sql4, "The transactions for could not be retrieved");
		$myrow4 = db_fetch($query4);
		$dpp=abs(@$myrow4[0]);
		*/
		$wh='';
		if($counter!=0)
			$wh=" and counter > ".$counter." ";
		$sql4="SELECT sum(amount) FROM `0_gl_trans` a 
		left join 0_voided b on a.type=b.type and a.type_no=b.id 
		WHERE a.`type` = ".$myrow0['type']." and b.id is null AND a.`type_no` = ".$myrow0['type_no']." AND `amount` > 0
		".$wh." and counter < ".$myrow0['counter']."
		order by counter asc";
		$counter=$myrow0['counter'];
		$query4=db_query($sql4, "The transactions for could not be retrieved");
		$myrow4 = db_fetch($query4);
		$dpp=abs(@$myrow4[0]);
		if(@$person==''){
				$memos=get_gl_memo2($myrow0['type_no'], $myrow0['type']);
				$person=($memos[1]);
		}
		$tableheader .= '<tr>';
		$tableheader .= '<td >'.$no.'</td>';
		$tableheader .= '<td colspan="2" >'.(@$person).'</td>';
		$tableheader .= '<td style="text-align:center;">-</td>';
		$tableheader .= '<td style="text-align:right;">'.price_format(abs($dpp)).'</td>';
		$tableheader .= '<td style="text-align:right;">'.price_format(abs($amnt)).'</td>';
		$tableheader .= '</tr>';
		$jml=$jml+($amnt);
		$jmlgross=$jmlgross+($gross);
		$jml2=$jml2+abs($amnt);
		$no++;
	}	


	$tableheader .= '<tr>';
	$tableheader .= '<td></td>';
	$tableheader .= '<td colspan="2"  ><b>Jumlah</b></td>';
	$tableheader .= '<td ></td>';
	$tableheader .= '<td style="text-align:right;">'.price_format(abs($jmlgross)).'</td>';
	$tableheader .= '<td style="text-align:right;">'.price_format(abs($jml)).'</td>';
	$tableheader .= '</tr>';

	//pph23
	$tableheader .= '<tr><td colspan="6"></td></tr>';
	if(get_post('Export'))
	$tableheader .= '<tr><td class="tableheader" colspan="6" bgcolor="#dee7ec"><b>PPH 23</b> 
	</td></tr>';
	else
	$tableheader .= '<tr><td class="tableheader" colspan="6" bgcolor="#dee7ec"><b>PPH 23</b> 
	<a href="pph23.php?TransFromDate='.$_POST['TransFromDate'].'&TransToDate='.$_POST['TransToDate'].'&Show=Show" target="_blank">(Click To Detail)</a>
	<a href="pph23_pajak.php?TransFromDate='.$_POST['TransFromDate'].'&TransToDate='.$_POST['TransToDate'].'" target="_blank">(Click For Template Import)</a>
	</td></tr>';
	$tableheader .= '<tr>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>No</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Jenis Penghasilan</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Jumlah Penghasilan Bruto (Rp)</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Perkiraan Penghasilan Neto</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Tarif (%)</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>PPH yang dipotong (Rp)</b></td>';
	$tableheader .= '</tr>';

	//$sql="select * from 0_trans_tax_details where tax_type_id=2 and tran_date like '".@$tglan[2]."-".@$tglan[1]."%'order by id asc";
	$sql="select * from 0_tax_jasa where tax_type_id=2 order by id asc";
	$query=db_query($sql, "The transactions for could not be retrieved");
	$no=1;$jml=0;$jmlgross=0;
	$huruf=array('','','','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s');
		$tableheader .= '<tr>';
		$tableheader .= '<td colspan="6">&nbsp;</td>';
		$tableheader .= '</tr>';
	while ($myrow0 = db_fetch($query))
	{
		$sql2="select sum(amount) as amount,sum(gross) as gross 
		from 0_gl_trans a 
		left join 0_voided b on a.type=b.type and a.type_no=b.id
		where b.id IS NULL
		and jasa=".@$myrow0['id']." and tran_date between '".date2sql($_POST['TransFromDate'])."' and '".date2sql($_POST['TransToDate'])."' order by counter asc";
		$query2=db_query($sql2, "The transactions for could not be retrieved");
		$dpp=0;$amnt=0;
		while ($myrow2 = db_fetch($query2))
		{
			//$dpp=$myrow2['dpp']*(-1);
			//$amnt=$myrow2['amount']*(-1);
			$gross=$myrow2['gross'];
			$amnt=$myrow2['amount'];
			
			if($gross==0){
				
				$sql6="select b.amount
					from 0_refs a
					inner join 0_gl_trans b on a.id=b.type_no and a.type=b.type
					left join 0_voided c on a.id=c.id and a.type=c.type
					where a.type=".$mfees['type']." and a.id=".$mfees['type_no']." and c.id IS NULL limit 0,1";
				$query6=db_query($sql6, "The transactions for could not be retrieved");
				$myrow6 = db_fetch($query6);
				//print_r($myrow6);
				$dpp=(@$myrow6[0]);
				//echo $dpp0=$dpp0+@$myrow6[0];
				//echo ' ';
				$rate='2%';
			}
		}

		$sql4="SELECT sum(net_amount) 
		FROM `0_trans_tax_details` a
		left join 0_voided b on a.trans_type=b.type and a.trans_no=b.id
		WHERE `jasa` = ".@$myrow0['id']." and b.id is null 
		and tran_date between '".date2sql($_POST['TransFromDate'])."' and '".date2sql($_POST['TransToDate'])."' ";
		$query4=db_query($sql4, "The transactions for could not be retrieved");
		$myrow4 = db_fetch($query4);
		$gross=abs(@$myrow4[0]);

		$tarif=@$myrow0['tarif'];
		
			
		$tableheader .= '<tr>';
		$tableheader .= '<td >'.($no<3?$no:'').'</td>';
		$tableheader .= '<td>'.($no>2?$huruf[$no].'. ':'').' '.$myrow0['jenis'].'</td>';
		$tableheader .= '<td style="text-align:right;">'.price_format(abs($gross)).'</td>';
		$tableheader .= '<td style="text-align:center;">20%</td>';
		$tableheader .= '<td style="text-align:right;">'.$tarif.'%</td>';
		$tableheader .= '<td style="text-align:right;">'.price_format(abs($amnt)).'</td>';
		$tableheader .= '</tr>';
		if($no==2){
			$tableheader .= '<tr>';
			$tableheader .= '<td >3</td>';
			$tableheader .= '<td>Jasa Lainnya SK Dirjen Pajak <br> No. PER-70/PJ/2007</td>';
			$tableheader .= '<td style="text-align:center;">-</td>';
			$tableheader .= '<td style="text-align:center;">-</td>';
			$tableheader .= '<td style="text-align:right;">-</td>';
			$tableheader .= '<td style="text-align:right;">-</td>';
			$tableheader .= '</tr>';

		}
		$jmlgross=$jmlgross+($gross);
		$jml=$jml+(@$amnt);
		$jml2=$jml2+abs(@$amnt);
		$no++;
	}	
	/*
	$tableheader .= '<tr>';
	$tableheader .= '<td colspan="6">&nbsp;</td>';
	$tableheader .= '</tr>';

	$sql2="select sum(amount) as amount,sum(gross) as gross 
	from 0_gl_trans a 
	left join 0_voided b on a.type=b.type and a.type_no=b.id
	where b.id IS NULL
	and jasa IS NULL and a.amount<0 and tran_date between '".date2sql($_POST['TransFromDate'])."' and '".date2sql($_POST['TransToDate'])."' order by counter asc";
	$query2=db_query($sql2, "The transactions for could not be retrieved");
	$dpp=0;$amnt=0;
	$myrow2 = db_fetch($query2);
	$gross=$myrow2['gross'];
	$amnt=$myrow2['amount'];
	$jmlgross=$jmlgross+($gross);
	$jml=$jml+(@$amnt);
	$jml2=$jml2+abs(@$amnt);
	$tableheader .= '<tr>';
	$tableheader .= '<td >'.($no<3?$no:'').'</td>';
	$tableheader .= '<td> Belum Ditentukan Jenis Jasa</td>';
	$tableheader .= '<td style="text-align:right;">'.price_format(abs($gross)).'</td>';
	$tableheader .= '<td style="text-align:center;">20%</td>';
	$tableheader .= '<td style="text-align:right;">'.$tarif.'%</td>';
	$tableheader .= '<td style="text-align:right;">'.price_format(abs($amnt)).'</td>';
	$tableheader .= '</tr>';
	*/
	$tableheader .= '<tr>';
	$tableheader .= '<td></td>';
	$tableheader .= '<td  ><b>Jumlah</b></td>';
	$tableheader .= '<td style="text-align:right;">'.price_format(abs($jmlgross)).'</td>';
	$tableheader .= '<td ></td>';
	$tableheader .= '<td style="text-align:right;"></td>';
	$tableheader .= '<td style="text-align:right;">'.price_format(abs($jml)).'</td>';
	$tableheader .= '</tr>';

	//pph4(2)
	$tableheader .= '<tr><td colspan="6"></td></tr>';
	if(get_post('Export'))
	$tableheader .= '<tr><td class="tableheader" colspan="6" bgcolor="#dee7ec"><b>PPH 4(2)</b> 
	</td></tr>';
	else
	$tableheader .= '<tr><td class="tableheader" colspan="6" bgcolor="#dee7ec"><b>PPH 4(2)</b> 
	<a href="pph4.php?TransFromDate='.$_POST['TransFromDate'].'&TransToDate='.$_POST['TransToDate'].'&Show=Show" target="_blank">(Click To Detail)</a>
	</td></tr>';
	$tableheader .= '<tr>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>No</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Jenis Penghasilan</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Jumlah Penghasilan Bruto (Rp)</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Perkiraan Penghasilan Neto</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>Tarif (%)</b></td>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>PPH yang dipotong (Rp)</b></td>';
	$tableheader .= '</tr>';

	//$sql="select * from 0_trans_tax_details where tax_type_id=3 and tran_date like '".@$tglan[2]."-".@$tglan[1]."%'order by id asc";
	$sql="select * from 0_tax_jasa where tax_type_id=3 order by id asc";
	$query=db_query($sql, "The transactions for could not be retrieved");
	$no=1;$jml=0;$jmlgross=0;

		$tableheader .= '<tr>';
		$tableheader .= '<td colspan="6">&nbsp;</td>';
		$tableheader .= '</tr>';
	while ($myrow0 = db_fetch($query))
	{
		$sql2="select sum(amount) as amount,sum(gross) as gross from 0_gl_trans  a 
		left join 0_voided b on a.type=b.type and a.type_no=b.id
		where b.id IS NULL
		and jasa=".@$myrow0['id']." and tran_date between '".date2sql($_POST['TransFromDate'])."' and '".date2sql($_POST['TransToDate'])."' order by counter asc";
		$query2=db_query($sql2, "The transactions for could not be retrieved");
		$dpp=0;$amnt=0;
		while ($myrow2 = db_fetch($query2))
		{
			//$dpp=$myrow2['dpp'];
			$amnt=$myrow2['amount'];
			$gross=$myrow2['gross'];
		}

		$tarif=@$myrow0['tarif'];
		if($amnt<0)
			$amnt=$amnt*(-1);
		$gross=abs(@$amnt)*100/$tarif;
		$tableheader .= '<tr>';
		$tableheader .= '<td >'.$no.'</td>';
		$tableheader .= '<td>'.$myrow0['jenis'].'</td>';
		$tableheader .= '<td style="text-align:right;">'.price_format(abs($gross)).'</td>';
		$tableheader .= '<td style="text-align:right;">-</td>';
		$tableheader .= '<td style="text-align:center;">'.$tarif.'</td>';
		$tableheader .= '<td style="text-align:right;">'.price_format(abs($amnt)).'</td>';
		$tableheader .= '</tr>';
		$jmlgross=$jmlgross+abs($gross);
		$jml=$jml+@$amnt;
		$jml2=$jml2+abs(@$amnt);
		$no++;
	}	

	$tableheader .= '<tr>';
	$tableheader .= '<td></td>';
	$tableheader .= '<td><b>Jumlah</b></td>';
	$tableheader .= '<td style="text-align:right;">'.price_format(abs($jmlgross)).'</td>';
	$tableheader .= '<td ></td>';
	$tableheader .= '<td style="text-align:right;"></td>';
	$tableheader .= '<td style="text-align:right;">'.price_format(abs($jml)).'</td>';
	$tableheader .= '</tr>';

	$tableheader .= '<tr><td colspan="6"></td></tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td colspan="2"  ><b>PPH Ps. 25</b></td>';
	$tableheader .= '<td colspan="3" ></td>';
	$tableheader .= '<td style="text-align:right;"></td>';
	$tableheader .= '</tr>';
	//$sql="select trans_type,trans_no,sum(net_amount) as dpp,sum(amount) as amount, memo from 0_trans_tax_details where tax_type_id=6 and tran_date like '".@$tglan[2]."-".@$tglan[1]."%' group by trans_type,trans_no order by id asc";
	$sql="select *
from 0_gl_trans a
left join 0_voided b on a.type=b.type and a.type_no=b.id
where account like '21260' and b.id IS NULL and amount<0 and tran_date between '".date2sql($_POST['TransFromDate'])."' and '".date2sql($_POST['TransToDate'])."' order by a.counter asc";
	//$sql="select trans_type,trans_no,sum(net_amount) as dpp,sum(amount) as amount, memo from 0_trans_tax_details where tax_type_id=4 and tran_date like '".@$tglan[2]."-".@$tglan[1]."%' group by trans_type,trans_no order by id asc";
	$query=db_query($sql, "The transactions for could not be retrieved");
	$no=1;$jml=0;
	while ($myrow0 = db_fetch($query))
	{
		$amnt=$myrow0['amount'];
		$jenis=@$myrow0['memo'];
		$person=get_counterparty_name(@$myrow0['trans_type'],@$myrow0['trans_no']);
		$tableheader .= '<tr>';
		$tableheader .= '<td colspan="2"></td>';
		$tableheader .= '<td>'.price_format(abs($amnt)).'</td>';
		$tableheader .= '<td colspan="2"></td>';
		$tableheader .= '<td style="text-align:right;">'.price_format(abs($amnt)).'</td>';
		$tableheader .= '</tr>';
		$jml2=$jml2+abs(@$amnt);
	}

	$tableheader .= '<tr><td colspan="6"></td></tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td colspan="2"  ><b>PPH Ps. 26</b></td>';
	$tableheader .= '<td colspan="3" ></td>';
	$tableheader .= '<td style="text-align:right;"></td>';
	$tableheader .= '</tr>';
	
	//$sql="select trans_type,trans_no,sum(net_amount) as dpp,sum(amount) as amount, memo from 0_trans_tax_details where tax_type_id=5 and tran_date like '".@$tglan[2]."-".@$tglan[1]."%' group by trans_type,trans_no order by id asc";
	$sql="select a.*
from 0_gl_trans a
left join 0_voided b on a.type=b.type and a.type_no=b.id
where account like '21270' and b.id IS NULL and amount<0 and tran_date between '".date2sql($_POST['TransFromDate'])."' and '".date2sql($_POST['TransToDate'])."' order by a.counter asc";
	$query=db_query($sql, "The transactions for could not be retrieved");
	$no=1;$jml=0;
	while ($myrow0 = db_fetch($query))
	{
		//$jenis=@$myrow0['memo_'];
		$memos=get_gl_memo2($myrow0['type_no'], $myrow0['type']);
		$jenis=@$memos[1];
		//$amnt=$myrow0['amount']*$myrow0['ex_rate'];
		$amnt=$myrow0['amount'];
		$person=get_counterparty_name(@$myrow0['type'],@$myrow0['type_no']);
		if($jenis=='' && @$myrow0['type_no']!=''){
			$sql2="select reference from 0_refs a where a.type=".@$myrow0['type']." AND a.id = '".@$myrow0['type_no']."'";
			$query2=db_query($sql2, "The transactions for could not be retrieved");
			while ($myrow2 = db_fetch($query2))
			{
				$jenis=$myrow2['reference'];
			}
		}

		$tableheader .= '<tr>';
		$tableheader .= '<td colspan="2"></td>';
		$tableheader .= '<td colspan="3">'.$jenis.' ['.@$person.'] 
		'.(@$myrow0['ex_rate']>1?'Rate '.date('d/m/Y',strtotime(@$myrow0['tran_date'])).':'.price_format(@$myrow0['ex_rate']):'').'</td>';
		$tableheader .= '<td style="text-align:right;">'.price_format(abs($amnt)).'</td>';
		$tableheader .= '</tr>';
		$jml2=$jml2+abs(@$amnt);
	}

	$tableheader .= '<tr><td colspan="6"></td></tr>';
	
	$tableheader .= '<tr>';
	$tableheader .= '<td colspan="2"  ><b>PPN LN</b></td>';
	$tableheader .= '<td colspan="3" ></td>';
	$tableheader .= '<td style="text-align:right;"></td>';
	$tableheader .= '</tr>';
	
	$sql="select a.*
from 0_gl_trans a
left join 0_voided b on a.type=b.type and a.type_no=b.id
where account like '21270' and b.id IS NULL and amount<0 and tran_date between '".date2sql($_POST['TransFromDate'])."' and '".date2sql($_POST['TransToDate'])."' order by a.counter asc";
	$query=db_query($sql, "The transactions for could not be retrieved");
	$no=1;$jml=0;
	while ($myrow0 = db_fetch($query))
	{
		//$jenis=@$myrow0['memo_'];
		$memos=get_gl_memo2($myrow0['type_no'], $myrow0['type']);
		$jenis=@$memos[1];
		//$amnt=$myrow0['amount']*$myrow0['ex_rate'];
		$amnt=$myrow0['amount'];
		$person=get_counterparty_name(@$myrow0['type'],@$myrow0['type_no']);
		if($jenis=='' && @$myrow0['type_no']!=''){
			$sql2="select reference from 0_refs a where a.type=".@$myrow0['type']." AND a.id = '".@$myrow0['type_no']."'";
			$query2=db_query($sql2, "The transactions for could not be retrieved");
			while ($myrow2 = db_fetch($query2))
			{
				$jenis=$myrow2['reference'];
			}
		}

		$tableheader .= '<tr>';
		$tableheader .= '<td colspan="2"></td>';
		$tableheader .= '<td colspan="3">'.$jenis.' ['.@$person.'] 
		'.(@$myrow0['ex_rate']>1?'Rate '.date('d/m/Y',strtotime(@$myrow0['tran_date'])).':'.price_format(@$myrow0['ex_rate']):'').'</td>';
		$tableheader .= '<td style="text-align:right;">'.price_format(abs($amnt)).'</td>';
		$tableheader .= '</tr>';
		$jml2=$jml2+abs(@$amnt);
	}

	$tableheader .= '<tr><td colspan="6"></td></tr>';
	$tableheader .= '<tr>
	<td class="tableheader" colspan="5" bgcolor="#dee7ec"><b>Total Pajak Penghasilan Yang Harus Dibayar</b></td>
	<td style="text-align:right;">'.price_format(abs($jml2)).'</td>
	</tr>';
	$tableheader.='</center></table>';
	return $tableheader;
}

//----------------------------------------------------------------------------------------------------


if (get_post('Export')) 
{
	$header='
	<style>
	.tableheader{
		background-color:#000 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0" style="border:#000 1px solid;">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" " style="background-color:#bfbfbf;">Ringkasan Pajak Penghasilan 21, 23, 4(2), dan 25</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 509pt;">';
	echo show_results();
	echo '</td></tr></table>';
}elseif(get_post('PrintPdf')){

// add a page
	$header='<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Ringkasan Pajak Penghasilan 21, 23, 4(2), dan 25</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	

	$header.= '<tr><td style="width: 646px;">';
	$header.=show_results();
	$header.= '</td></tr></table>';
$pdf->AddPage();
// output the HTML content
$html=$header;
//$html.=display_bs_pdf();
$pdf->writeHTML(@$html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('ringkasanPajak.pdf', 'I');	

}else{

tax_inquiry_controls();
		div_start('trans_tbl');
	if (get_post('Show')) {
		echo '<center><table border="0" cellpadding="4" width="80%">
		<tr>		
			<td align="center" style="font-weight:bold;">Ringkasan Pajak Penghasilan 21, 23, 4(2), dan 25</td>
		</tr>
		<tr>
			<td align="center" style="font-weight:bold;">Periode '.@$blnnya.'</td>
		</tr>';
		echo '<tr>		
			<td align="center">';

		echo show_results();
		echo '</td></tr></table></center>';
	}
		div_end();
end_page();
	//show_results();
}
//----------------------------------------------------------------------------------------------------


