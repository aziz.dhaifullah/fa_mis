<?php
//$page_security = 'SA_GLANALYTIC';
$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/gl/includes/siap.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/admin/db/group_report_db.inc");

$compare_types = array(
	_("Accumulated"),
	_("Period Y-1"),
	_("Budget")
);

function display_typepl ($type, $report, $typename, $from, $to, $begin, $end, $compare, $convert=null,
	$dimension=0, $dimension2=0, $compid, $drilldown, $path_to_root,$comps,$check_value)
{
	global $levelptr, $k;
		
	$code_per_balance = 0;
	$code_acc_balance = 0;
	$per_balance_total = 0;
	$acc_balance_total = 0;
	unset($totals_arr);
	$totals_arr = array();
	$sum_total = 0;
	$totperbalance = 0;
	$sum_total2 = array();
	//Get Accounts directly under this group/type
	$result = get_report_coa_comp($type, $compid);

	$total=array();
	$total2=array();
	while ($account=db_fetch($result))
	{
		//$per_balance = get_gl_trans_from_to($from, $to, $account["account_code"], $dimension, $dimension2);
		/*
		if ($compare == 2)
			$acc_balance = get_budget_trans_from_to($begin, $end, $account["account_code"], $dimension, $dimension2);
		else
			$acc_balance = get_gl_trans_from_to($begin, $end, $account["account_code"], $dimension, $dimension2);
			*/
		//if (!$per_balance && !$acc_balance)
		//	continue;
		
		//if ($drilldown && $levelptr == 0){
			
			for($a=0;$a<count($comps);$a++){
				
				if($account['account_code'] == '42201'){
					$getcoasiap = "SELECT amount FROM 0_gl_trans WHERE account = '".$account['account_code']."' AND memo_ LIKE '%SIAP%' AND dimension_id = '".$comps[$a]['kode']."' AND tran_date ='".sql2date($to)."' ORDER BY tran_date DESC";
					//echo $sql;
					$endfiscallast = date('d/m/Y', strtotime('-1 year', strtotime(sql2date(begin_fiscalyear()))));
					$datacoasiap = db_query($getcoasiap);
					while ($amount2 = db_fetch($datacoasiap)) {
						$nilaicoasiap = $amount2['amount'];
					}
					$getcoanosiap = "SELECT amount FROM 0_gl_trans WHERE account = '".$account['account_code']."' AND memo_ NOT LIKE '%SIAP%' AND dimension_id = '".$comps[$a]['kode']."' AND (tran_date BETWEEN '".sql2date($from)."' AND '".sql2date($to)."') ORDER BY tran_date DESC";
					$datacoanosiap = db_query($getcoanosiap);
					while ($amount3 = db_fetch($datacoanosiap)) {
						$nilaicoanosiap += $amount3['amount'];
					}
					$per_balance = $nilaicoanosiap + $nilaicoasiap;
				}else{
					$per_balance = get_gl_trans_from_to($from, $to, $account["account_code"], $comps[$a]['kode'], $dimension2);
				}
				$totperbalance += $per_balance;
				if ($check_value == 1) {
					if (@$comps[$a]['kode_siap']!='') {
						$tot2 = get_tbpl_ims($comps[$a]['kode_siap'],$account["account_code"],date2sql($to));
						//echo $tot2."<br>";
						$totperbalance += $tot2;
					}				
				}
				$sum_total += $totperbalance;
				@$total[$a]+= $totperbalance;
				$totperbalance = 0;
			}
			$sum_total = 0;

			//amount_cell($acc_balance * $convert);
			//amount_cell(Achieve($per_balance, $acc_balance));
		//}
			
		$code_per_balance += @$per_balance;
		$code_acc_balance += @$acc_balance;
	}

	$levelptr = 1;
	$totals_arr[0] = $code_per_balance + $per_balance_total;
	$totals_arr[1] = $code_acc_balance + $acc_balance_total;
	$totals_arr[3] = $total;
	return $totals_arr;
	//print_r($totals_arr);
}

function display_typepl_last($type, $report, $typename, $from, $to, $begin, $end, $compare, $convert=null,
	$dimension=0, $dimension2=0, $compid, $drilldown, $path_to_root, $comps, $check_value)
{
	global $levelptr, $k;
		
	$code_per_balance = 0;
	$code_acc_balance = 0;
	$per_balance_total = 0;
	$acc_balance_total = 0;
	unset($totals_arr);
	$totals_arr = array();
	$sum_total = 0;
	$totperbalance = 0;
	$sum_total2 = array();
	
	//Get Accounts directly under this group/type
	$result = get_report_coa_comp($type, $compid);

	$total=array();
	$total2=array();
	$tahun = date('Y', strtotime(sql2date($to)));
	$tahun--;
	while ($account=db_fetch($result))
	{
		//$per_balance = get_gl_trans_from_to($from, $to, $account["account_code"], $dimension, $dimension2);
		/*
		if ($compare == 2)
			$acc_balance = get_budget_trans_from_to($begin, $end, $account["account_code"], $dimension, $dimension2);
		else
			$acc_balance = get_gl_trans_from_to($begin, $end, $account["account_code"], $dimension, $dimension2);
			*/
		//if (!$per_balance && !$acc_balance)
		//	continue;
		
		//if ($drilldown && $levelptr == 0){

			for($a=0;$a<count($comps);$a++){
				/*if($account['account_code'] == '42201'){
					$getcoasiap = "SELECT amount FROM 0_gl_trans WHERE account = '".$account['account_code']."' AND memo_ LIKE '%SIAP%' AND dimension_id = '".$comps[$a]['kode']."' AND tran_date ='".sql2date($to)."' ORDER BY tran_date DESC";
					//echo $sql;
					$endfiscallast = date('d/m/Y', strtotime('-1 year', strtotime(sql2date(end_fiscalyear()))));
					$datacoasiap = db_query($getcoasiap);
					while ($amount2 = db_fetch($datacoasiap)) {
						$nilaicoasiap = $amount2['amount'];
					}
					$getcoanosiap = "SELECT amount FROM 0_gl_trans WHERE account = '".$account['account_code']."' AND memo_ NOT LIKE '%SIAP%' AND dimension_id = '".$comps[$a]['kode']."' AND tran_date >= '".sql2date($endfiscallast)."' ORDER BY tran_date DESC";
					$datacoanosiap = db_query($getcoanosiap);
					while ($amount3 = db_fetch($datacoanosiap)) {
						$nilaicoanosiap += $amount3['amount'];
					}
					$per_balance = $nilaicoanosiap + $nilaicoasiap;
				}else{*/
					$per_balance = get_gl_trans_from_to('', '31/12/'.$tahun, $account["account_code"], $comps[$a]['kode'], $dimension2);
				/*}*/
				/*if($type == 3){
					$per_balance = $per_balance * -1;
				}*/
				/*if($type == 1 OR $type == 3){
					$per_balance = $per_balance * -1;
				}*/
				$totperbalance += $per_balance;
				if ($check_value == 1) {
					if (@$comps[$a]['kode_siap']!='') {
						$tot2 = get_tbpl_ims($comps[$a]['kode_siap'],$account["account_code"],date2sql($to));
						//echo $tot2."<br>";
						$totperbalance += $tot2;
					}				
				}
				$sum_total += ($totperbalance);
				@$total[$a]+= $totperbalance;
				$totperbalance = 0;
			}
			$sum_total = 0;

			//amount_cell($acc_balance * $convert);
			//amount_cell(Achieve($per_balance, $acc_balance));
		//}
			
		$code_per_balance += @$per_balance;
		$code_acc_balance += @$acc_balance;
	}

	$levelptr = 1;
	$totals_arr[0] = $code_per_balance + $per_balance_total;
	$totals_arr[1] = $code_acc_balance + $acc_balance_total;
	$totals_arr[3] = $total;
	return $totals_arr;
	//print_r($totals_arr);
}		

function get_tbpl_ims($kodesiap,$accode,$date){
	$datatbims = get_tb2_ims($date);
	
	if(isset($datatbims[$date][$kodesiap][$accode])){
		return $datatbims[$date][$kodesiap][$accode];
	}else{
		return 0;
	}

}	

function check_subcomppl($subcomid){
	$sql = "SELECT count(*) as nilai from 0_companies where parent = ".$subcomid;
	//$comps=array();
	$result =db_query($sql);
	$count=db_fetch($result);
	//var_dump($comps);exit;
	return $count;
}

function subcomppl($parid){
	$sql = "SELECT comp.id,comp.kode, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = id) as nsub
	FROM ".TB_PREF."companies comp
	where comp.parent=".$parid;
	$comps=array();
	$subcomp=array();
	if($result=db_query($sql)){
		while($row=db_fetch($result)){
			$check_subcomp = check_subcomppl($row['id']);
			if($check_subcomp['nilai'] > 0){
				$subcomp = subcomppl($row['id']);
				$comps = array_merge($comps,$subcomp);
			}else{
				$comps[] = $row;	
			}
		}
	}
	//var_dump($comps);exit;
	return $comps;
}

function get_profit_and_loss($dimension,$dimension2,$frombs,$tobs,$compare = 0,$check_value,$level)
{
	global $path_to_root, $compare_types;
	$totaltype2 = 0.0;
	$totalparse = array();
	$totaltype3 = 0.0;
	$totalpendapatan = array();
	$totalbiaya = array();
	$totallabausaha = array();
	$totallainlain = array();
	$totallabasebelumpajak = array();
	$manfaatpajak = array();
	$totallaba = array();

	if (!isset($dimension))
		$dimension = 0;
	if (!isset($dimension2))
		$dimension2 = 0;
	$dimension = $dimension;
	$dimension2 = $dimension2;

	$to = $tobs;
	$from  = begin_fiscalyear();
	if (date('Y',strtotime(sql2date($to))) < date('Y',strtotime(sql2date($from)))) {
	 	$from = date('d/m/Y', strtotime('-1 year', strtotime(sql2date($from))));
	}
	
	/*if (isset($accgrp) && (strlen($accgrp) > 0))
		$drilldown = 1; // Deeper Level
	else
		$drilldown = 0; // Root level*/

	
	if ($compare == 0 || $compare == 2)
	{
		$end = $to;
		if ($compare == 2)
		{
			$begin = $from;
		}
		else
			$begin = begin_fiscalyear();
	}
	elseif ($compare == 1)
	{
		$begin = add_months($from, -12);
		$end = add_months($to, -12);
	}

	$comps=array();
	$subcomp=array();
	/*if (@$level == '') {
		$sql = "SELECT comp.id,comp.kode,comp.nama,comp2.nama as namaparent, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = ".$_SESSION["wa_current_user"]->com_id.") as nsub
		FROM ".TB_PREF."companies comp
		LEFT JOIN ".TB_PREF."companies comp2 ON comp2.id = comp.parent
		where comp.id=".$_SESSION["wa_current_user"]->com_id;
		if(@$_SESSION["wa_current_user"]->com_id=='0')
		$sql = "SELECT comp.id,comp.kode,comp.nama,comp.parent,comp2.nama as namaparent, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = comp.id ) as nsub
		FROM ".TB_PREF."companies comp
		LEFT JOIN ".TB_PREF."companies comp2 ON comp2.id = comp.parent
		order by kode asc";
		


		if($result=db_query($sql)){
			while($row=db_fetch($result)){
				$dim=$row['kode'];
				if($dimension!=0)
				$dim=$dimension;
				//print_r($comps);
				if($dim==$row['kode']){
					//if (($row['lvl'] == 0) || ($row['lvl'] == 1)) {
					//	$comps[]=$row;
					//}else{
					
					//}
					if(@$row['nsub'] > 0){
						$subcomp=subcomp($row['id']);
					}else{
						//if(!in_array($comps,$row['kode'])){
						$comps[]=$row;
						//}					
					}
				}
				
			}
			if (($_SESSION["wa_current_user"]->com_id != 0) OR ($dimension!=0)) {
				$comps=array_merge($comps,$subcomp);
			}
		}
	}else{*/
	if ($level == '0') {
		$level = 5;
	}

	if ($dimension!=0) {
		$sql = "select id,lvl from ".TB_PREF."companies where kode = '".$dimension."'";
		$result = db_query($sql);
		while ($row=db_fetch($result)) {
			$id = $row['id'];
			$lvl = $row['lvl'];
		}
		if ($level == $lvl) {
			$lvl = $lvl;
		}else{
			$lvl = $level;
		}

		if ($level == -1) {
			$sql = "select id,lvl from ".TB_PREF."companies where id = '".$id."'";
			$result = db_query($sql);
			while ($row=db_fetch($result)) {
				$lvl = $row['lvl'];
			}
			$lvl = $lvl + 1;
		}
	}else{
		$id = $_SESSION["wa_current_user"]->com_id;
		$lvl = $level;

		/*if ($level == -1) {
			$sql = "select id,lvl from ".TB_PREF."companies where id = '".$id."'";
			$result = db_query($sql);
			while ($row=db_fetch($result)) {
				$lvl = $row['lvl'];
			}
			$lvl = $lvl + 1;
		}*/
	}
	if ($_SESSION["wa_current_user"]->com_id == 0 AND $dimension == 0 AND $level == -1) {
		$id = $_SESSION["wa_current_user"]->com_id;
		$lvl = 1;
	}

	$sql = "select  lvl, nama, id, kode, parent, CONCAT(kode,' ', nama) as ref
			from    (select * from ".TB_PREF."companies
			         order by parent, id) comps_sorted,
			        (select @pv := ".$id.") initialisation
			where   (CASE WHEN NOT EXISTS (SELECT id from ".TB_PREF."companies where parent = @pv) THEN
					find_in_set(id, @pv) ELSE find_in_set(parent, @pv) END)
			and 	length(@pv := concat(@pv, ',', id))
			and		(lvl BETWEEN lvl AND ".$lvl.")
			group by parent";
	
	if($result=db_query($sql)){
		while($row=db_fetch($result)){
			$comps[]=$row;
		}
	}
	/*}*/
	/*if (!$drilldown) //Root Level
	{*/
		$salesper = 0.0;
		$salesacc = 0.0;	
		$totaltype = 0.0;	
	
		//Get classes for PL
		$report = 2;
		$sql = "SELECT id from 0_companies where kode='".$dimension."'";
		$result = db_query($sql);
		$fetch = db_fetch($result);
		$classresult = get_group_data($fetch['id'],0,false,2,null);
		$total=array();
		while ($class = db_fetch($classresult))
		{
			$class_per_total = 0;
			$class_acc_total = 0;
			$total2=array();
			//$convert = get_class_type_convert($class["ctype"]); 		
			//Get Account groups/types under this group/type
			//$ctype = $class["ctype"];
			//$typeresult = get_account_types(false, $class['cid'], -1);
			//$k = 0; // row color
			/*while ($accounttype=db_fetch($typeresult))
			{*/
					//echo $check_values;
					if (@$check_value > 0) {
						$check_value = 1;
					}else{
						$check_value = 0;
					}	
					$count2 = count($comps)+2;
					for($a=0;$a<count($comps);$a++){
						$TypeTotal = display_typepl($class["id"], $report, null, $from, $to, $begin, $end, $compare, null, 
						@$comps[$a]['kode'], $dimension2, @$comps[$a]['id'], @$drilldown,  $path_to_root,$comps,$check_value);
						@$total2[$a]+=$TypeTotal[3][$a];
					}
			//}
			
			//Print Class Summary
			
			for($a=0;$a<count($comps);$a++){	
				if ($class["class_id"] == 4) {
					@$totalpendapatan[$a] += $total2[$a];
				}else if($class["class_id"] == 6){
					@$totalbiaya[$a] += $total2[$a];
				}else{
					@$totallainlain[$a] += $total2[$a];
				}
				$totaltype2 += (@$total2[$a]);
				@$total[$a]+= $total2[$a];
			}
			$totaltype2 = 0;
			$salesper += $class_per_total;
			$salesacc += $class_acc_total;
		}
		
		for($a=0;$a<count($comps);$a++){	
			$totaltype3 += ((@$totalpendapatan[$a] + @$totalbiaya[$a]) + @$totallainlain[$a]);
			@$totalparse[$a] += ((@$totalpendapatan[$a] + @$totalbiaya[$a]) + @$totallainlain[$a]);
		}
		return $totalparse;
		$totaltype3 = 0;
		$totalparse = 0;
		//amount_cell($salesper *-1);
		//amount_cell($salesacc * -1);
		//amount_cell(achieve($salesper, $salesacc));	

	//}
	/*else 
	{
		//Level Pointer : Global variable defined in order to control display of root 
		global $levelptr;
		$levelptr = 0;
		
		$accounttype = get_account_type($accgrp);
		$classid = $accounttype["class_id"];
		$class = get_account_class($classid);
		$convert = get_class_type_convert($class["ctype"]); 
		
		//Print Class Name	
		echo $tableheader;
		
		$classtotal = display_typepl($accounttype["id"], $accounttype["name"], $from, $to, $begin, $end, $compare, $convert, $dimension, $dimension2, $drilldown, $path_to_root,$comps);
		
	}*/
		
	//return $totalparse;
}

function get_profit_and_loss_last($dimension,$dimension2,$frombs,$tobs,$compare = 0,$check_value,$level)
{
	global $path_to_root, $compare_types;
	$totaltype2 = 0.0;
	$totalparse = array();
	$totaltype3 = 0.0;
	$totalpendapatan = array();
	$totalbiaya = array();
	//print_r($dimension);
	if (!isset($dimension))
		$dimension = 0;
	if (!isset($dimension2))
		$dimension2 = 0;
	$dimension = $dimension;
	$dimension2 = $dimension2;

	$from = $tobs;
	$to = $tobs;
	
	/*if (isset($accgrp) && (strlen($accgrp) > 0))
		$drilldown = 1; // Deeper Level
	else
		$drilldown = 0; // Root level*/

	
	if ($compare == 0 || $compare == 2)
	{
		$end = $to;
		if ($compare == 2)
		{
			$begin = $from;
		}
		else
			$begin = begin_fiscalyear();
	}
	elseif ($compare == 1)
	{
		$begin = add_months($from, -12);
		$end = add_months($to, -12);
	}

	$comps=array();
	$subcomp=array();
	/*if (@$level == '') {
		$sql = "SELECT comp.id,comp.kode,comp.nama,comp2.nama as namaparent, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = ".$_SESSION["wa_current_user"]->com_id.") as nsub
		FROM ".TB_PREF."companies comp
		LEFT JOIN ".TB_PREF."companies comp2 ON comp2.id = comp.parent
		where comp.id=".$_SESSION["wa_current_user"]->com_id;
		if(@$_SESSION["wa_current_user"]->com_id=='0')
		$sql = "SELECT comp.id,comp.kode,comp.nama,comp.parent,comp2.nama as namaparent, CONCAT(comp.kode,'  ',comp.nama) as ref,comp.lvl,(select count(*) from 0_companies where parent = comp.id ) as nsub
		FROM ".TB_PREF."companies comp
		LEFT JOIN ".TB_PREF."companies comp2 ON comp2.id = comp.parent
		order by kode asc";
		


		if($result=db_query($sql)){
			while($row=db_fetch($result)){
				$dim=$row['kode'];
				if($dimension!=0)
				$dim=$dimension;
				//print_r($comps);
				if($dim==$row['kode']){
					//if (($row['lvl'] == 0) || ($row['lvl'] == 1)) {
					//	$comps[]=$row;
					//}else{
					
					//}
					if(@$row['nsub'] > 0){
						$subcomp=subcomp($row['id']);
					}else{
						//if(!in_array($comps,$row['kode'])){
						$comps[]=$row;
						//}					
					}
				}
				
			}
			if (($_SESSION["wa_current_user"]->com_id != 0) OR ($dimension!=0)) {
				$comps=array_merge($comps,$subcomp);
			}
		}
	}else{*/
		if ($level == '0') {
			$level = 5;
		}

		if ($dimension!=0) {
			$sql = "select id,lvl from ".TB_PREF."companies where kode = '".$dimension."'";
			$result = db_query($sql);
			while ($row=db_fetch($result)) {
				$id = $row['id'];
				$lvl = $row['lvl'];
			}
			if ($level == $lvl) {
				$lvl = $lvl;
			}else{
				$lvl = $level;
			}

			if ($level == -1) {
				$sql = "select id,lvl from ".TB_PREF."companies where id = '".$id."'";
				$result = db_query($sql);
				while ($row=db_fetch($result)) {
					$lvl = $row['lvl'];
				}
				$lvl = $lvl + 1;
			}
		}else{
			$id = $_SESSION["wa_current_user"]->com_id;
			$lvl = $level;

			/*if ($level == -1) {
				$sql = "select id,lvl from ".TB_PREF."companies where id = '".$id."'";
				$result = db_query($sql);
				while ($row=db_fetch($result)) {
					$lvl = $row['lvl'];
				}
				$lvl = $lvl + 1;
			}*/
		}
		if ($_SESSION["wa_current_user"]->com_id == 0 AND $dimension == 0 AND $level == -1) {
			$id = $_SESSION["wa_current_user"]->com_id;
			$lvl = 1;
		}

		$sql = "select  lvl, nama, id, kode, parent, CONCAT(kode,' ', nama) as ref
				from    (select * from ".TB_PREF."companies
				         order by parent, id) comps_sorted,
				        (select @pv := ".$id.") initialisation
				where   (CASE WHEN NOT EXISTS (SELECT id from ".TB_PREF."companies where parent = @pv) THEN
						find_in_set(id, @pv) ELSE find_in_set(parent, @pv) END)
				and 	length(@pv := concat(@pv, ',', id))
				and		(lvl BETWEEN lvl AND ".$lvl.")
				group by parent";
		
		if($result=db_query($sql)){
			while($row=db_fetch($result)){
				$comps[]=$row;
			}
		}
	/*}*/
	/*if (!$drilldown) //Root Level
	{*/
		$salesper = 0.0;
		$salesacc = 0.0;	
		$totaltype = 0.0;	
	
		//Get classes for PL
		$sql = "SELECT id from 0_companies where kode='".$dimension."'";
		$result = db_query($sql);
		$fetch = db_fetch($result);
		$classresult = get_group_data($fetch['id'],0,false,2,null);
		$total=array();
		while ($class = db_fetch($classresult))
		{
			$class_per_total = 0;
			$class_acc_total = 0;
			$total2=array();
			//$convert = get_class_type_convert($class["ctype"]); 		
			//Get Account groups/types under this group/type
			//$ctype = $class["ctype"];
			//$typeresult = get_account_types(false, $class['cid'], -1);
			//$k = 0; // row color
			/*while ($accounttype=db_fetch($typeresult))
			{*/
					//echo $check_values;
					if (@$check_value > 0) {
						$check_value = 1;
					}else{
						$check_value = 0;
					}	
					$count2 = count($comps)+2;
					for($a=0;$a<count($comps);$a++){
						$TypeTotal = display_typepl_last($class["id"],null, null, $from, $to, $begin, $end, $compare, null, 
						@$comps[$a]['kode'], $dimension2, @$comps[$a]['id'], @$drilldown,  $path_to_root,$comps,$check_value);
						@$total2[$a]+=$TypeTotal[3][$a];
					}
			//}
			
			//Print Class Summary
			
			for($a=0;$a<count($comps);$a++){	
				if ($class["class_id"] == 4) {
					@$totalpendapatan[$a] += $total2[$a];
				}else if($class["class_id"] == 6){
					@$totalbiaya[$a] += $total2[$a];
				}else{
					@$totallainlain[$a] += $total2[$a];
				}
				$totaltype2 += (@$total2[$a]);
				@$total[$a]+= $total2[$a];
			}
			$totaltype2 = 0;
			$salesper += $class_per_total;
			$salesacc += $class_acc_total;
		}
		
		for($a=0;$a<count($comps);$a++){	
			$totaltype3 += ((@$totalpendapatan[$a] + @$totalbiaya[$a]) + @$totallainlain[$a]);
			@$totalparse[$a] += ((@$totalpendapatan[$a] + @$totalbiaya[$a]) + @$totallainlain[$a]);
		}
		return $totalparse;
		$totaltype3 = 0;
		$totalparse = 0;
		//amount_cell($salesper *-1);
		//amount_cell($salesacc * -1);
		//amount_cell(achieve($salesper, $salesacc));	

	//}
	/*else 
	{
		//Level Pointer : Global variable defined in order to control display of root 
		global $levelptr;
		$levelptr = 0;
		
		$accounttype = get_account_type($accgrp);
		$classid = $accounttype["class_id"];
		$class = get_account_class($classid);
		$convert = get_class_type_convert($class["ctype"]); 
		
		//Print Class Name	
		echo $tableheader;
		
		$classtotal = display_typepl($accounttype["id"], $accounttype["name"], $from, $to, $begin, $end, $compare, $convert, $dimension, $dimension2, $drilldown, $path_to_root,$comps);
		
	}*/
		
	//return $totalparse;
}
 /*$test[] = get_profit_and_loss_last('1.5.1',0,'31/12/2019','31/12/2019',0,0,0);
 print_r($test);
*/