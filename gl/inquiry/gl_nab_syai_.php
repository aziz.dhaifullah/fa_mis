<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/config_mssql.php");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/siar.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();


if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format(0);


if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"NAB.xls\"");
	header("Cache-Control: max-age=0");
	
	
}else{
page(_($help_context = "NAB"), false, false, "", $js);
}

//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}
//----------------------------------------------------------------------------------------------------

function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER);
	start_row();
   // gl_all_accounts_list_cells(_("Account:"), 'account', null, false, false, _("All Accounts"));
	date_cells(_("Begining Date:"), 'TransFromDate', '', null, -user_transaction_days());
	date_cells(_("End Date:"), 'TransToDate');
/*
	echo "<td>"._("Product").":</td>\n";
	echo "<td>";
	
$product = array(
	_("All")
);
	echo array_selector('Product', null, $product);
	echo "</td>\n";
	*/
    end_row();
	end_table();

	start_table(TABLESTYLE_NOBORDER);
	start_row();
	echo '<td colspan="4">&nbsp;</td>';
	submit_cells('Show',_("Show"),'align="right"','', 'default');
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel"></td>';
	echo '</tr>';
	echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
		function xls(){
			//console.log("aa");
			$(".tablestyle2").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "NAB",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';

	//submit_cells('Export Excel',_("Export"),'','', 'default');
	end_row();
	end_table();

	echo '<hr>';
    end_form();
}

//----------------------------------------------------------------------------------------------------

function show_results()
{
	global $path_to_root, $systypes_array;

	

	$colspan = ($dim == 2 ? "6" : ($dim == 1 ? "5" : "4"));

	if ($_POST["account"] != null)
		display_heading($_POST["account"]. "&nbsp;&nbsp;&nbsp;".$act_name);

	start_table(TABLESTYLE2, "width='90%'", 10);
	
	$th = array(_("No"), _(""));
	$th2 = array(_("Jumlah Unit"), _("NAB Awal"),_("Jumlah Awal"),_("NAB AKhir ".$_POST['TransToDate']),_("Jumlah Akhir"),_("Selisih"));
	$th=array_merge($th,$th2);
	//table_header($th);

	//$tableheader='<div id="trans_tbl">';
	$tableheader.='<table class="tablestyle" cellpadding="6" border="1" width="90%">';
	$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
	for($a=0;$a<count($th);$a++){
		$tableheader.='<td class="tableheader" align="center">'.$th[$a].'</td>';
	}
	echo $tableheader.='</tr>';
	$bfw = 0;

	$running_total = $bfw;
	$j = 1;
	$k = 0; //row colour counter
	$no=1;
	$prods=get_prod_siar();
	//print_r($prods[0]);
	
	for($a=0;$a<count($prods[0]);$a++){
		$prodid=$prods[1][$a];
		$tgl=date2sql($_POST['TransFromDate']);
		$tgl2=date2sql($_POST['TransToDate']);
		if($tgl2==$tgl){
			$newdate=strtotime ( '-1 day' , strtotime ( date2sql($_POST['TransFromDate']) ) ) ; 
			$tgl=date('Y-m-d',$newdate);
		}
		//$nabawal=get_nab_siar($prodid,$tgl);
		//$nabakhir=get_nab_siar($prodid,$tgl2);
		$nabawal=get_aum_siar($prods[1][$a],$tgl);
		$nabakhir=get_aum_siar($prods[1][$a],$tgl2);
		$jmlawal=$nabawal['OutstandingUnits'];$awal=$nabawal['navvalue'];
		$jmlakhir=$nabakhir['OutstandingUnits'];$akhir=$nabakhir['navvalue'];
		echo '<tr>';
		$jml=$awal*$jmlakhir;
		$jml2=$akhir*$jmlakhir;
    	label_cell($no);
		label_cell($prods[0][$a]);
		label_cell(price_format($jmlakhir));
		label_cell(price_format($awal));
		label_cell(price_format($jml));
		label_cell(price_format($akhir));
		label_cell(price_format($jml2));
		label_cell(price_format($jml2-$jml));
		$no++;
	}
	
}
//----------------------------------------------------------------------------------------------------

if (get_post('Export')) 
{
	
	
	$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
	$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
	$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
	$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	$blnnya=date('M Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));

	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('M',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('M Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('M Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
	$header='<table border="0" style="border:#000 1px solid;">
		<tr>
			<td width="100%" align="center"  style="background-color:#8072a9;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:20px;">NAB</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 651pt;">';
	show_results();
	echo '</td></tr></table>';
}else{
	
	gl_inquiry_controls();

	div_start('trans_tbl');

	if (get_post('Show') || get_post('account'))
	    show_results();

	div_end();

	//----------------------------------------------------------------------------------------------------

	end_page();
}

