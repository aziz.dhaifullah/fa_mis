<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/config_mssql.php");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/siap.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

page(_($help_context = "MKBD"), false, false, '', $js);

//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}
if (get_post('Export')) 
{
	

}

if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format(0);

//----------------------------------------------------------------------------------------------------

function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER);
	start_row();
   // gl_all_accounts_list_cells(_("Account:"), 'account', null, false, false, _("All Accounts"));
	date_cells(_("Date:"), 'TransFromDate', '', null, -user_transaction_days());
	//date_cells(_("End Date:"), 'TransToDate');

	echo "<td>"._("Lampiran").":</td>\n";
	echo "<td>";
	
$lampiran = array(
	_("V.D.5-1"),_("V.D.5-2"),_("V.D.5-3"),_("V.D.5-4"),_("V.D.5-5"),_("V.D.5-6"),_("V.D.5-7"),_("V.D.5-8"),_("V.D.5-9"),_("V.D.5-10")
);
	echo array_selector('lampiran', null, $lampiran);
	echo "</td>\n";
	echo "<td>"._("Direktur Bertanggung Jawab").":</td>\n";
	echo "<td>";
	echo '<input type="text" name="direktur">';
	echo "</td>\n";
    end_row();
	end_table();

	start_table(TABLESTYLE_NOBORDER);
	start_row();

	submit_cells('Show',_("Show"),'','', 'default');
	submit_cells('Excel',_("Export Excel"),'','', 'default');

	//submit_cells('Export Excel',_("Export"),'','', 'default');
	end_row();
	end_table();

	echo '<hr>';
    end_form();
}

//----------------------------------------------------------------------------------------------------


function display_trial_balance($type, $typename,$no)
{
	global $path_to_root, $SysPrefs,
		 $k, $pdeb, $pcre, $cdeb, $ccre, $tdeb, $tcre, $pbal, $cbal, $tbal;

	$printtitle = 0; //Flag for printing type name

	$k = 0;

	//Get Accounts directly under this group/type
	$accounts = get_gl_accounts(null, null, $type);

	$begin = get_fiscalyear_begin_for_date($_POST['TransFromDate']);
	if (date1_greater_date2($begin, $_POST['TransFromDate']))
		$begin = $_POST['TransFromDate'];
	$begin = add_days($begin, -1);

	while ($account = db_fetch($accounts))
	{
		//Print Type Title if it has atleast one non-zero account	
		if (!$printtitle)
		{
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell($no,'align="center"');
			label_cell($typename);
			label_cell('');
			end_row();
			$printtitle = 1;
			$no++;
		}

		// FA doesn't really clear the closed year, therefore the brought forward balance includes all the transactions from the past, even though the balance is null.
		// If we want to remove the balanced part for the past years, this option removes the common part from from the prev and tot figures.
		if (@$SysPrefs->clear_trial_balance_opening)
		{
			$open = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin,  $begin, false, true);
			$offset = min($open['debit'], $open['credit']);
		} else
			$offset = 0;

		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $_POST['TransFromDate'], false, true);
		alt_table_row_color($k);

		label_cell($no,'align="center"');
		label_cell($account["account_name"]);
		amount_cell($tot['debit']-$tot['credit']);
		end_row();
		$no++;
	}

	//Get Account groups/types under this group/type
	$result = get_account_types(false, false, $type);
	while ($accounttype=db_fetch($result))
	{
		//$no++;
		$printtitle = 1;
		if($no2=='')
			$no2=$no;
		$no2=display_trial_balance($accounttype["id"], $accounttype["name"].' ('.$typename.')',$no2);
	}
	return $no;
}
function show_results()
{
	global $path_to_root, $systypes_array,$SysPrefs;

	
	$lampir=$_POST['lampiran']+1;
	$ctype=$lampir;
	$colspan = ($dim == 2 ? "6" : ($dim == 1 ? "5" : "4"));

	if ($_POST["account"] != null)
		display_heading($_POST["account"]. "&nbsp;&nbsp;&nbsp;".$act_name);
	$title='Formulir Nomor V.D.5-'.$lampir.' tentang Laporan Neraca Percobaan Harian - Aset';				

	$company = get_company_prefs();
	start_table(TABLESTYLE2, "width='90%'", 10);
	echo '<tr>';
	echo '<td colspan="3" align="right">Lampiran :  '.$lampir.'</td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td colspan="3" align="right">Peraturan Nomor :  V.D.5</td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td colspan="3" align="center">'.$title.'</td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td>Perusahaan Efek</td><td>:</td><td>'.$company['coy_name'].'</td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td>Tanggal</td><td width="5%">:</td><td>'.$_POST['TransFromDate'].'</td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td width="30%">Direktur yang Bertanggung Jawab Atas <br>Laporan Ini</td><td>:</td><td>'.$_POST['direktur'].'</td>';
	echo '</tr>';
	end_table(1);
	start_table(TABLESTYLE, "width='90%'", 6);
	$no=6;
	echo '<tr>';
	echo '<th align="center">'.$no.'</th><th align="center">A</th><th align="center">B</th>';
	echo '</tr>';
	$no++;
	echo '<tr>';
	echo '<th align="center">'.$no.'</th><th width="60%" align="center">Nama Akun</th><th width="30%" align="center">Saldo</th>';
	echo '</tr>';
	$typeresult = get_account_types(false, $ctype, -1);
	while ($accounttype=db_fetch($typeresult))
	{
		$no++;
		display_trial_balance($accounttype["id"], $accounttype["name"],$no);
	}

	end_table(1);
}
//----------------------------------------------------------------------------------------------------

gl_inquiry_controls();

div_start('trans_tbl');

if (get_post('Show') || get_post('account'))
    show_results();

div_end();

//----------------------------------------------------------------------------------------------------

end_page();

