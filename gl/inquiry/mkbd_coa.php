<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/config_mssql.php");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/db/gl_db_mkbd.inc");
include_once($path_to_root . "/gl/includes/siap.inc");
/*
*/
$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

page(_($help_context = "MKBD"), false, false, '', $js);

//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Load')) 
{
	$Ajax->activate('trans_tbl');
}
if (get_post('Export')) 
{
	

}

if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format(0);

//----------------------------------------------------------------------------------------------------

function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    //start_form();
    echo '
	<style>
		.export{
			align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		}
	</style>
	<script>
		function filter(isi){
			var y = document.getElementById("isinya").childNodes; 
			//console.log(y);
			for(a=0;a<y.length;a++){
				if(isi!=""){
					y[a].style.display = "none";
					x=y[a].id; 
					if(isi===x) 
						y[a].style.display = "table-row"; 
				}else{					
					y[a].style.display = "table-row";
				}
			}
			//x.style.display = "table-row";  
		}
	</script>
	';
	echo '<form action="" method="post">';
    start_table(TABLESTYLE_NOBORDER);
	start_row();
   // gl_all_accounts_list_cells(_("Account:"), 'account', null, false, false, _("All Accounts"));
	//date_cells(_("Date:"), 'TransFromDate', '', null, -user_transaction_days());
	//date_cells(_("End Date:"), 'TransToDate');

	echo "<td>"._("Lampiran").":</td>\n";
	echo "<td>";
	
	$lampiran = array();
	$lamp=@$_POST['lampiran'];
	$lists=get_mkbd_list();
	$opt='<select autocomplete="off" name="lampiran" class="combo" title="" _last="0">';
	while ($list = db_fetch($lists))
	{
		$opt.='<option value="'.$list['id'].'" '.($lamp==$list['id']?'selected':'').'>V.D.5-'.$list['lamp'].' No.'.$list['no'].' ['.@$list['label'].']</option>';
		
	}
	$opt.='</select>';
	echo $opt;
	//echo array_selector('lampiran', null, $lampiran);
	echo "</td>\n";
	echo '<td align="right">
			<input type="submit" class="export" name="Load" id="Load" value="Load">
		</td>';
	echo '<td align="right">
			<input type="submit" class="export" name="Save" id="Save" value="Save">
		</td>';
	end_row();
	end_table();

	

	echo '<hr>';
	div_start('trans_tbl');
	if (get_post('Load')) 
    echo show_results();
	div_end();
    end_form();
}

//----------------------------------------------------------------------------------------------------


function show_results()
{
	global $path_to_root, $systypes_array,$SysPrefs;
	
	$tableheader='<div id="balance_tbl"><center><table class="tablestyle" cellpadding="6" cellspacing="0">';
	//$cols=0;
	$header='<tr>
			<td class="tableheader">Account Code</td>
		  	<td class="tableheader" colspan="2">
		  		<input type="text" name="filters" id="filters" onkeyup="filter(this.value)">
	 		</td>
		</tr>';	
	$header.='<tr>
		  <td class="tableheader">&nbsp;</td>
		  <td class="tableheader">Account</td>
		  <td class="tableheader">Desc</td>
		</tr><tbody id="isinya">';	
	$tableheader.=$header;
	$result = get_chart_accounts_search(get_post("description"));
	while ($myrow = db_fetch_assoc($result)) {
		
		$sql="select count(id) as nid from 0_mkbd_coa where account='".@$myrow["accode"]."' and mkbd_id=".@$_POST['lampiran']." order by id asc";
		$query=db_query($sql, "The transactions for could not be retrieved");
		while ($myrow0 = db_fetch($query))
		{
			$nid=$myrow0['nid'];
		}
		$ckbox='<input type="checkbox" name="account[]" '.($nid>0?'checked':'').' value="'.str_replace('-','',$myrow["account_code"]).'">';
		$tableheader.='<tr id="'.str_replace('-','',$myrow["account_code"]).'">
			  <td>'.$ckbox.'</td>
			  <td>'.$myrow["account_code"].'</td>
			  <td>'.$myrow["account_name"].'</td>
			</tr>';	
	}
	$tableheader.='</tbody></table></div>';
	return $tableheader;
}
//----------------------------------------------------------------------------------------------------

if(@$_POST['Save']=='Save'){
	$lamp=$_POST['lampiran'];
	$accounts=$_POST['account'];
	add_mkbd_coa($lamp,$accounts);
	//print_r($accounts);
}
gl_inquiry_controls();

//----------------------------------------------------------------------------------------------------

end_page();

