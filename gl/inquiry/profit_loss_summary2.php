<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLANALYTIC';
$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

$js = "";
if (user_use_date_picker())
	$js = get_js_date_picker();

if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];	
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Compare"]))
	$_POST["Compare"] = $_GET["Compare"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["AccGrp"]))
	$_POST["AccGrp"] = $_GET["AccGrp"];

if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"LAPORAN LABA RUGI KOMPREHENSIF.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
page(_($help_context = "LAPORAN LABA RUGI KOMPREHENSIF "), false, false, "", $js);
}

$compare_types = array(
	_("Accumulated"),
	_("Period Y-1"),
	_("Budget")
);
//----------------------------------------------------------------------------------------------------
// Ajax updates

if (get_post('Show')) 
{
	$Ajax->activate('pl_tbl');
}

	if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

//----------------------------------------------------------------------------------------------------

function inquiry_controls()
{  
	global $compare_types;

	$dim = get_company_pref('use_dimension');
    start_table(TABLESTYLE_NOBORDER);
    
	$date = today();
	if (!isset($_POST['TransToDate']))
		$_POST['TransToDate'] = end_month($date);
	if (!isset($_POST['TransFromDate']))
		$_POST['TransFromDate'] = add_days(end_month($date), -user_transaction_days());
	echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	check_cells(_("Expand"), 'expand', null);
	echo "</td>\n";
	echo '<tr>';
	echo '<tr>';
    date_cells(_("&nbsp;"), 'TransFromDate');
	date_cells(_("&nbsp;"), 'TransToDate');
	/*
	echo "<td>"._("Compare to").":</td>\n";
	echo "<td>";
	echo array_selector('Compare', null, $compare_types);
	echo "</td>\n";	
	*/
	if ($dim >= 1)
		dimensions_list_cells(_("Dimension")." 1:", 'Dimension', null, true, " ", false, 1);
	if ($dim > 1)
		dimensions_list_cells(_("Dimension")." 2:", 'Dimension2', null, true, " ", false, 2);
	
	
	echo '</tr>';
	echo '<tr>';
	echo '<td colspan="4">&nbsp;</td>';
	submit_cells('Show',_("Show"),'align="right"','', 'default');
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">&nbsp;';
	echo '<input type="submit" class="export" name="PrintPdf" id="PrintPdf" Value="Print PDF"></td>';
	echo '</tr>';
	echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>
	<script>
	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
	</script>';
	end_table();
}

//----------------------------------------------------------------------------------------------------

function display_pln(){
	if(get_post('PrintPdf'))
	$tableheader ='<center><table class="tablestyle" width="98.5%" cellpadding="3" border="0">';
	else
	$tableheader ='<center><table class="tablestyle" width="80%" cellpadding="3">';

	
	if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
	$begin=date('d/m/Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	$awal = add_days($begin, -1);
	$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));
	$plus=array('4','8');
	$minus=array('6','5','9');

	$tableheader .= '<tr><td class="tableheader" colspan="3" bgcolor="#dee7ec"><b>PENDAPATAN USAHA</b></td></tr>';
	$totalaset0=0;
	$totalall=0;$totalall2=0;
	//$accounts=get_gl_accounts_in(array('41000','42000','51004'));
	$accounts=get_gl_accounts_in(array('41000','42000','43000','43001','43002','43003','43004','45000','46000','51004'));
	$total=0;
	while ($account = db_fetch($accounts))
	{

		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);

		if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset0=$total+$totalaset0;
	$totalall=$totalall+$total;
	$tableheader .= '<tr class="inquirybg" style="font-weight:bold"><td colspan="2">PENDAPATAN USAHA</td><td align="right">'.price_format_pr(($total)).'</td></tr>';
if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr((@$totalall)).'</td></tr>';

	
	//$tableheader .= '<tr><td colspan="3">BEBAN USAHA</td></tr>';
	$tableheader .= '<tr><td class="tableheader" colspan="3" bgcolor="#dee7ec"><b>BEBAN USAHA</b></td></tr>';
	
	$totalaset=0;
	$total=0;
	$accounts=get_gl_accounts(null, null, '6-1000');
	while ($account = db_fetch($accounts))
	{
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
			$amn=0;
			$amn=$tot['balance']*(-1);
			$total=$total+$amn;$totalall2=$totalall2+($amn);			
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts_in(array('62004'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset=$total+$totalaset;
	$totalall=$totalall+$total;
	$tableheader .= '<tr><td colspan="2">Beban Kepegawaian</td><td align="right">'.price_format_pr(($total)).'</td></tr>';
if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';

	$total=0;
	$accounts=get_gl_accounts(null, null, '6-3000');
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+$amn;$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts(null, null, '6-3001');
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+$amn;$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts(null, null, '6-3002');
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+$amn;$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts(null, null, '6-3003');
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+$amn;$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset=$total+$totalaset;
	$totalall=$totalall+$total;
	$tableheader .= '<tr><td colspan="2">Sewa</td>
		<td align="right">'.price_format_pr(($total)).'</td>
	</tr>';
if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';

	$total=0;
	$accounts=get_gl_accounts(null, null, '6-5000');
	$not=array('51005','65112');
	while ($account = db_fetch($accounts))
	{
		if(!in_array($account['account_code'],$not)){
			$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
			$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+$amn;$totalall2=$totalall2+($amn);			
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
		}
	}
	$accounts=get_gl_accounts_in(array('64002','64003','51000'
		,'51001','51002','51003','51005','68002','69199'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts_in(array('69104'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance_like($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts_in(array('62003'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance_like($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset=$total+$totalaset;
	$totalall=$totalall+$total;
	$tableheader .= '<tr><td colspan="2">Beban Umum dan Administrasi</td><td align="right">'.price_format_pr(($total)).'</td></tr>';
	if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';

	//,'66002'
	$accounts=get_gl_accounts_in(array('66001','66002'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset=$total+$totalaset;
	$totalall=$totalall+$total;

	$tableheader .= '<tr><td colspan="2">Penyusutan</td><td align="right">'.price_format_pr(($total)).'</td></tr>';
if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';

	$accounts=get_gl_accounts_in(array('69101'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts(null, null, '6-9101');
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+$amn;$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset=$total+$totalaset;
	$totalall=$totalall+$total;
	
	$tableheader .= '<tr><td colspan="2">Perjalanan Dinas</td><td align="right">'.price_format_pr(($total)).'</td></tr>';
	if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';

	$total=0;
	$accounts=get_gl_accounts_in(array('62001'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts(null, null, '6-2001');
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+$amn;$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset=$total+$totalaset;
	$totalall=$totalall+$total;
	$tableheader .= '<tr><td colspan="2">Iklan & Promosi</td><td align="right">'.price_format_pr(($total)).'</td>
	</tr>';
	if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';

	$total=0;
	$accounts=get_gl_accounts_in(array('65112'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts(null, null, '6-8000');
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+$amn;$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts(null, null, '6-8001');
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+$amn;$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset=$total+$totalaset;
	$totalall=$totalall+$total;
	$tableheader .= '<tr><td colspan="2">Jasa Profesional</td><td align="right">'.price_format_pr(($total)).'</td>
	</tr>';
	if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';

	$total=0;
	$accounts=get_gl_accounts_in(array('62002','69102'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts(null, null, '6-2002');
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to, false, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+$amn;$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset=$total+$totalaset;
	$totalall=$totalall+$total;
	$tableheader .= '<tr><td colspan="2">Perjamuan dan Sumbangan </td><td align="right">'.price_format_pr(($total)).'</td>
	</tr>';
	if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';

	$accounts=get_gl_accounts_in(array('64001'));
	//,'64002','64003'
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset=$total+$totalaset;
	$totalall=$totalall+$total;
	$tableheader .= '<tr><td colspan="2">Telepon dan Internet </td><td align="right">'.price_format_pr(($total)).'</td>
	</tr>';
	if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';

	$accounts=get_gl_accounts_in(array('67001','67002'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset=$total+$totalaset;
	$totalall=$totalall+$total;
	$tableheader .= '<tr><td colspan="2">Pelatihan dan Seminar </td><td align="right">'.price_format_pr(($total)).'</td>
	</tr>';
	if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';
	$jmlbeban=($totalall2-$totalaset0);
	$tableheader .= '<tr><td colspan="2"><b>Jumlah Beban Usaha</b></td><td align="right"><b>'.price_format_pr($jmlbeban).'</b></td>
	</tr>';


	$tableheader .= '<tr><td colspan="3" class="tableheader">LABA USAHA</td></tr>';
	$tableheader .= '<tr><td colspan="3">PENGHASILAN DAN BEBAN LAIN-LAIN BERSIH</td></tr>';
	$totalaset2=0;
	$accounts=get_gl_accounts_in(array('84000','94000'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset2=$total+$totalaset2;
	$totalall=$totalall+$total;
	
	$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp; Laba Penjualan Aset Tetap </td><td align="right">'.price_format_pr(($total)).'</td>
	</tr>';
	if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';

	$accounts=get_gl_accounts_in(array('83000','93000'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset2=$total+$totalaset2;
	$totalall=$totalall+$total;
	
	$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp; Laba (Rugi) Selisih Kurs </td><td align="right">'.price_format_pr(($total)).'</td>
	</tr>';
	if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';

	$accounts=get_gl_accounts_in(array('69001','69002','69003','69004'));
	//$accounts=get_gl_accounts_in(array('92000'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts_in(array('69103','69105','69106','69199','92000','95000'));
	//$accounts=get_gl_accounts_in(array('92000'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts_in(array('99999','99000'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset2=$total+$totalaset2;
	$totalall=$totalall+$total;
	
	$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp; Beban Bunga Pinjaman & Keuangan </td><td align="right">'.price_format_pr(($total)).'</td>
	</tr>';
	if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';

	$total=0;
	$accounts=get_gl_accounts_in(array('81000','82000','84000','85000','87000','88000','89000'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts_in(array('86000'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts_in(array('89000'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts_in(array('96000'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts_in(array('89900','89980','89990','89991','89992','89999'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts_in(array('89999001','89999002'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$accounts=get_gl_accounts_in(array('89980'));
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset2=($total)+$totalaset2;
	$totalall=$totalall+$total;

	$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp; Penghasilan Lain Lain - Bersih</td><td align="right">'.price_format_pr(($total)).'</td>
	</tr>';
	if(@$_POST['expand']==1){
		$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';
		$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';

	}
	
	$tableheader .= '<tr><td colspan="2"><b>Jumlah Penghasilan Lain Lain - Bersih</b></td><td align="right"><b>'.price_format_pr($totalaset2).'</b></td>
	</tr>';

	$lb1=($totalall2);
	$tableheader .= '<tr><td colspan="2"><b>LABA SEBELUM TAKSIRAN PAJAK PENGHASILAN</b></td><td align="right"><b>'.price_format_pr($lb1).'</b></td></tr>';
	$totalaset2=0;
	$tableheader .= '<tr><td colspan="3"><b>TAKSIRAN PAJAK PENGHASILAN </b></td></tr>';
	$accounts=get_gl_accounts_in(array('69900'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+($amn);$totalall2=$totalall2+($amn);		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset2=$total+$totalaset2;
	
	$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp; Pajak Kini</td><td align="right">'.price_format_pr(($total)).'</td>
	</tr>';
	if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';

	$accounts=get_gl_accounts_in(array('69990'));
	$total=0;
	while ($account = db_fetch($accounts))
	{
		$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $to,true, true);
		$amn=0;
		$amn=$tot['balance']*(-1);
		$total=$total+(($amn));		
			if(@$_POST['expand']==1)
			$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;'.$account["account_code"].' '.$account["account_name"].'</td><td align="right">'.price_format_pr(($amn)).'</td></tr>';
	}
	$totalaset2=$total+$totalaset2;
	
	$tableheader .= '<tr><td colspan="2">&nbsp;&nbsp;&nbsp; Biaya Pajak Tangguhan</td><td align="right">'.price_format_pr(($total)).'</td>
	</tr>';
	if(@$_POST['expand']==1)
	$tableheader .= '<tr><td colspan="3">'.price_format_pr(($totalall2)).'</td></tr>';
	
	$tableheader .= '<tr><td colspan="2"><b>LABA BERSIH PERIODE BERJALAN </b></td><td align="right">'.price_format_pr(($lb1+$totalaset2)).'</td></tr>';
	$tableheader .= '<tr><td colspan="2"><b>PENDAPATAN (RUGI) KOMPREHENSIF LAIN</b></td><td align="right">'.price_format_pr(0).'</td></tr>';
	$tableheader .= '<tr><td colspan="2"><b>JUMLAH LABA KOMPREHENSIF PERIODE BERJALAN</b></td><td align="right"><b>'.price_format_pr(($lb1+$totalaset2)).'</b></td></tr>';

	$tableheader.='</center></table>';
	return $tableheader;
}

//----------------------------------------------------------------------------------------------------

if (get_post('Export')) 
{
	$header='
	<style>
	.inquirybg2{
		font-weight:bold !important;
		font-size:12px !important;
	}
	.tableheader{
		background-color:#EE7600 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0">
		<tr><td align="center">&nbsp;</td></tr>
		<tr><td style="width:2%;">&nbsp;&nbsp;</td>
			<td>
	<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">LAPORAN LABA RUGI KOMPREHENSIF</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td>
		</tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 509pt;">';
	echo display_pln();
	echo '</td></tr></table>
		</td></tr>
		<tr><td></td><td align="center" style="background-color:#EE7600;border:#000 1px solid;">&nbsp;</td></tr>
		</table>';
}elseif(get_post('PrintPdf')){

// add a page
	$header='<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">LAPORAN LABA RUGI KOMPREHENSIF</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	$header.= '<tr><td style="width: 646px;">';
	$header.=display_pln();
	$header.= '</td></tr></table>';
$pdf->AddPage();
// output the HTML content
$html=$header;
//$html.=display_bs_pdf();
$pdf->writeHTML(@$html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_PL.pdf', 'I');	

}else{
	
	start_form();

	inquiry_controls();
	div_start('pl_tbl');

	if (get_post('Show')) {
		echo '<center><table border="0" cellpadding="4" width="80%">
		<tr>		
			<td align="center" style="font-weight:bold;">LAPORAN LABA RUGI KOMPREHENSIF</td>
		</tr>
		<tr>
			<td align="center" style="font-weight:bold;">Periode '.@$blnnya.'</td>
		</tr>';
		echo '<tr>		
			<td align="center">';
		echo display_pln();
		echo '</td></tr></table></center>';
	}
	//if ($_POST['Show']) 
	//echo display_pln();

	end_form();

	end_page();

}

