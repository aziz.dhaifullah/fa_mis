<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLANALYTIC';
$path_to_root="../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

$js = "";
if (user_use_date_picker())
	$js = get_js_date_picker();

if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];	
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Compare"]))
	$_POST["Compare"] = $_GET["Compare"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["AccGrp"]))
	$_POST["AccGrp"] = $_GET["AccGrp"];

if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"PL.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
	page(_($help_context = "Profit & Loss Statement"), false, false, "", $js);
}

$compare_types = array(
	_("Accumulated"),
	_("Period Y-1"),
	_("Budget")
);
//----------------------------------------------------------------------------------------------------
// Ajax updates

if (get_post('Show')) 
{
	$Ajax->activate('pl_tbl');
}


	if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
//----------------------------------------------------------------------------------------------------

function display_type ($type, $typename, $from, $to, $begin, $end, $compare, $convert,
	$dimension=0, $dimension2=0, $drilldown, $path_to_root)
{
	global $levelptr, $k;
		
	$code_per_balance = 0;
	$code_acc_balance = 0;
	$per_balance_total = 0;
	$acc_balance_total = 0;
	unset($totals_arr);
	$totals_arr = array();
	
	//Get Accounts directly under this group/type
	$result = get_gl_accounts(null, null, $type);	

	while ($account=db_fetch($result))
	{
		$per_balance = get_gl_trans_from_to($from, $to, $account["account_code"], $dimension, $dimension2);

		if ($compare == 2)
			$acc_balance = get_budget_trans_from_to($begin, $end, $account["account_code"], $dimension, $dimension2);
		else
			$acc_balance = get_gl_trans_from_to($begin, $end, $account["account_code"], $dimension, $dimension2);
		if (!$per_balance && !$acc_balance)
			continue;
		
		if ($drilldown && $levelptr == 0)
		{
			$url = "<a href='$path_to_root/gl/inquiry/gl_account_inquiry.php?TransFromDate=" 
				. $from . "&TransToDate=" . $to . "&Dimension=" . $dimension . "&Dimension2=" . $dimension2 
				. "&account=" . $account['account_code'] . "'>" . $account['account_code'] 
				." ". $account['account_name'] ."</a>";				
				
			start_row("class='stockmankobg'");
			label_cell($account['account_code2']);
			amount_cell($per_balance * $convert);
			amount_cell($acc_balance * $convert);
			amount_cell(Achieve($per_balance, $acc_balance));
			end_row();
		}
			
		$code_per_balance += $per_balance;
		$code_acc_balance += $acc_balance;
	}

	$levelptr = 1;
	
	//Get Account groups/types under this group/type
	$result = get_account_types(false, false, $type);
	while ($accounttype=db_fetch($result))
	{	
		$totals_arr = display_type($accounttype["id"], $accounttype["name"], $from, $to, $begin, $end, 
			$compare, $convert, $dimension, $dimension2, $drilldown, $path_to_root);
		$per_balance_total += $totals_arr[0];
		$acc_balance_total += $totals_arr[1];
	}

	//Display Type Summary if total is != 0 
	if (($code_per_balance + $per_balance_total + $code_acc_balance + $acc_balance_total) != 0)
	{
		if ($drilldown && $type == $_POST["AccGrp"])
		{		
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_('Total') . " " . $typename);
			amount_cell(($code_per_balance + $per_balance_total) * $convert);
			amount_cell(($code_acc_balance + $acc_balance_total) * $convert);
			amount_cell(Achieve(($code_per_balance + $per_balance_total), ($code_acc_balance + $acc_balance_total)));
			end_row();
		}
		//START Patch#1 : Display  only direct child types
		$acctype1 = get_account_type($type);
		$parent1 = $acctype1["parent"];
		if ($drilldown && $parent1 == $_POST["AccGrp"])
		//END Patch#2		
		//elseif ($drilldown && $type != $_POST["AccGrp"])
		{	
			$url = "<a href='$path_to_root/gl/inquiry/profit_loss.php?TransFromDate=" 
				. $from . "&TransToDate=" . $to . "&Compare=" . $compare . "&Dimension=" . $dimension . "&Dimension2=" . $dimension2
				. "&AccGrp=" . $type ."'>" . $type . " " . $typename ."</a>";
				
			alt_table_row_color($k);
			label_cell($url);
			amount_cell(($code_per_balance + $per_balance_total) * $convert);
			amount_cell(($code_acc_balance + $acc_balance_total) * $convert);
			amount_cell(Achieve(($code_per_balance + $per_balance_total), ($code_acc_balance + $acc_balance_total)));
			end_row();
		}
	}
	
	$totals_arr[0] = $code_per_balance + $per_balance_total;
	$totals_arr[1] = $code_acc_balance + $acc_balance_total;
	return $totals_arr;
}	
	
function Achieve($d1, $d2)
{
	if ($d1 == 0 && $d2 == 0)
		return 0;
	elseif ($d2 == 0)
		return 999;
	$ret = ($d1 / $d2 * 100.0);
	if ($ret > 999)
		$ret = 999;
	return $ret;
}
function display_pl($type, $typename,$lvl=0)
{	
	$accounts = get_gl_accounts(null, null, $type);
	$path_to_root='../..';
	if (!isset($_POST['Dimension']))
		$_POST['Dimension'] = 0;
	if (!isset($_POST['Dimension2']))
		$_POST['Dimension2'] = 0;
	$dimension = $_POST['Dimension'];
	$dimension2 = $_POST['Dimension2'];

	if($_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=$_POST['bulanfrom'];
		$blnto=$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$ttls=array();
	$total=0;
	//print_r($accounts);
	while ($account = db_fetch($accounts))
	{
		if($account["account_code"]!='69900' && $account["account_code"]!='69990'){

			alt_table_row_color($k);
			$url = "<a href='".$path_to_root."/gl/inquiry/gl_account_inquiry.php?TransFromDate=" . $_POST["TransFromDate"] . "&TransToDate=" . $_POST["TransToDate"] . "&account=" . $account["account_code"] . "&Dimension=" . $_POST["Dimension"] . "&Dimension2=" . $_POST["Dimension2"] . "'>" . $account["account_code"]." ".$account["account_name"]. "</a>";
			//label_cell($account["account_code2"]." ".$account["account_name"]);
			$acode='';
			if(@$_POST['AccCode']==1)
			$acode=$account["account_code2"].' - ';
			$spasi='';
			if($lvl>1){
					$spasi.='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			}
			echo '<td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$spasi.$acode." ".$account["account_name"].'</td>';
			//echo '<td colspan="2">&nbsp;</td>';
			if($_POST["tipe"]==0){
				$from=date('d/m/Y',strtotime($thnto.'-'.$blnfrom.'-01'));
				$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-'.cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto)));
				$per_balance = get_gl_trans_from_to($from, $to, $account["account_code"], $dimension, $dimension2);
				amount_cell(abs(@$per_balance));
			}else{
				for($a=$blnfrom;$a<=$blnto;$a++){
					//$tbals[$a]=0;
					$from=date('d/m/Y',strtotime($thnto.'-'.$a.'-01'));
					$to=date('d/m/Y',strtotime($thnto.'-'.$a.'-'.cal_days_in_month(CAL_GREGORIAN, $a, $thnto)));
					$per_balance = get_gl_trans_from_to($from, $to, $account["account_code"], $dimension, $dimension2);
					amount_cell(abs(@$per_balance));
					$ttls[$a]=@$ttls[$a]+abs(@$per_balance);
				}
			}
			//amount_cell($TypeTotal[1] * $convert);
			//amount_cell(Achieve($TypeTotal[0], $TypeTotal[1]));
			end_row();
			$total=$total+@$per_balance;
			//$tableheader.='<tr><td colspan="4">&nbsp;</td></tr>';
			$pl=display_pl($account['account_code2'], '',3);
			if($_POST["tipe"]!=0){
				for($a=$blnfrom;$a<=$blnto;$a++){
					$ttls[$a]=@$ttls[$a]+abs(@$pl[$a]);
				}
			}
			$totala=$pl[0];

			$total=$total+$totala;
		}
	}
	return array($total,$ttls);
}
function display_pl_pdf($type, $typename,$lvl=0)
{	
	$accounts = get_gl_accounts(null, null, $type);

	if (!isset($_POST['Dimension']))
		$_POST['Dimension'] = 0;
	if (!isset($_POST['Dimension2']))
		$_POST['Dimension2'] = 0;
	$dimension = $_POST['Dimension'];
	$dimension2 = $_POST['Dimension2'];

	$from = $_POST['TransFromDate'];
	$to = $_POST['TransToDate'];
	$total=0;
	//print_r($accounts);
	$tableheader='';
	while ($account = db_fetch($accounts))
	{
		if($account["account_code"]!='69900' && $account["account_code"]!='69990'){
			$per_balance = get_gl_trans_from_to($from, $to, $account["account_code"], $dimension, $dimension2);

			$tableheader.='<tr class="inquirybg">';
			$acode='';
			if(@$_POST['AccCode']==1)
			$acode=$account["account_code2"].' - ';
			$spasi='';
			if($lvl>1){
					$spasi.='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			}
			$tableheader.='<td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$spasi.$acode." ".$account["account_name"].'</td>';

			//$tableheader.='<td colspan="2">&nbsp;</td>';
			$tableheader.='<td align="right">'. price_format(abs($per_balance)).'</td>';
			$tableheader.='</tr>';

			
			$total=$total+$per_balance;
			$totala=display_pl_pdf($account['account_code2'], '',3);
			$tableheader.=$totala[1];
			$total=$total+$totala[0];

		}

		//$tableheader.='<tr><td colspan="4">&nbsp;</td></tr>';
	}
	return array($total,$tableheader);
}
function display_pl0($type, $typename)
{	
	$typeresult = get_account_types(false, false, $type);
	$k = 0; // row color
	$total=0;
	if($_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=$_POST['bulanfrom'];
		$blnto=$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$ttls=array();
	while ($accounttype=db_fetch($typeresult))
	{
		$atas=0;
		$acode='';
		if(@$_POST['AccCode']==1)
		$acode=$accounttype['id'].' - ';
		$cekclassacc=cek_class_account($accounttype['id']);
		//echo $accounttype['name'].' '.count($cekclassacc);
		if(count($cekclassacc)>1)
			continue;
		//$per_balance = get_gl_trans_from_to($from, $to, $account["account_code"], $dimension, $dimension2);
		if($atas==0){
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_("").$acode.$accounttype['name'], "colspan=".(4+($blnto-$blnfrom)));
			end_row();
		}
		$pl=display_pl($accounttype['id'], $typename);
		$total=$pl[0];
		if($atas==0){
			start_row("class='inquirybg' style='font-weight:bold'");
			//label_cell(_('Total') . " " . $accounttype['name']);
			echo '<td colspan="3">Total '. $accounttype['name'].'</td>';
			if($total==0)
			$amnt=abs(@$per_balance);
			else
			$amnt=abs($total);
			if($_POST["tipe"]==0){
				amount_cell(abs(@$amnt));
			}else{
				for($a=$blnfrom;$a<=$blnto;$a++){
					//$tbals[$a]=0;
					amount_cell(abs(@$pl[1][$a]));
					$ttls[$a]=@$ttls[$a]+abs(@$pl[1][$a]);
				}
			}
			//amount_cell($class_acc_total * $convert);
			//amount_cell(Achieve($class_per_total, $class_acc_total));
			end_row();	
		}
		$total=$total+@$per_balance;

		//$tableheader.='<tr><td colspan="4">&nbsp;</td></tr>';
	}
	return array($total,$ttls);
}
function display_pl0_pdf($type, $typename)
{	
	$typeresult = get_account_types(false, false, $type);
	$k = 0; // row color
	$total=0;
	$tableheader='';
	while ($accounttype=db_fetch($typeresult))
	{
		//$per_balance = get_gl_trans_from_to($from, $to, $account["account_code"], $dimension, $dimension2);

		$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
		$acode='';
		if(@$_POST['AccCode']==1)
		$acode=$accounttype['id'].' - ';
		$tableheader.='<td colspan="4">'.$acode.$accounttype['name'].'</td>';
		$tableheader.='</tr>';

		//start_row("class='inquirybg' style='font-weight:bold'");
		//label_cell(_("").$accounttype['id'] ." - ".$accounttype['name'], "colspan=4");
		//end_row();
		$dpl=display_pl_pdf($accounttype['id'], @$typename);
		$total=$dpl[0];
		$tableheader.=$dpl[1];
		$tableheader.='<tr class="inquirybg">';
		//$tableheader.='<td></td>';
		$tableheader.='<td colspan="3">Total '. $accounttype['name'].'</td>';
		if($total==0)
		$tableheader.='<td align="right">'. price_format(abs($per_balance)).'</td>';
		else
		$tableheader.='<td align="right">'. price_format(abs($total)).'</td>';
		$tableheader.='</tr>';

		$total=$total+$per_balance;
	}
	return array($total,$tableheader);
}
function inquiry_controls()
{  
	global $compare_types;

	$dim = get_company_pref('use_dimension');
    start_table(TABLESTYLE_NOBORDER);
    
	$date = today();
	if (!isset($_POST['TransToDate']))
		$_POST['TransToDate'] = end_month($date);
	if (!isset($_POST['TransFromDate']))
		$_POST['TransFromDate'] = add_days(end_month($date), -user_transaction_days());

	echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("Type").":</td>\n";
	echo "<td>";
	$period = array(_("Outstanding"),_("Multiple"));
	//$period = array(_("Monthly"),_("Quarterly"),_("Semi Annyally"),_("Annually"));
	echo array_selector('tipe', null, $period);
	echo "</td>\n";

	echo '<tr>';
    date_cells(_("From:"), 'TransFromDate');
	date_cells(_("To:"), 'TransToDate');

	/*
	echo "<td>"._("Compare to").":</td>\n";
	echo "<td>";
	echo array_selector('Compare', null, $compare_types);
	echo "</td>\n";	
	*/
	if ($dim >= 1)
		dimensions_list_cells(_("Dimension")." 1:", 'Dimension', null, true, " ", false, 1);
	if ($dim > 1)
		dimensions_list_cells(_("Dimension")." 2:", 'Dimension2', null, true, " ", false, 2);
	
	check_cells(_("Account Code"), 'AccCode', null);
	echo '</tr>';
	echo '<tr>';
	echo '<td colspan="4">&nbsp;</td>';
	submit_cells('Show',_("Show"),'align="right"','', 'default');
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">&nbsp;';
	echo '<input type="submit" class="export" name="PrintPdf" id="PrintPdf" Value="Print PDF"></td>';
	echo '</tr>';
	echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "PL",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
    end_table();

	hidden('AccGrp');
}

//----------------------------------------------------------------------------------------------------

function display_profit_and_loss($compare)
{
	global $path_to_root, $compare_types;

	if (!isset($_POST['Dimension']))
		$_POST['Dimension'] = 0;
	if (!isset($_POST['Dimension2']))
		$_POST['Dimension2'] = 0;
	$dimension = $_POST['Dimension'];
	$dimension2 = $_POST['Dimension2'];

	if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

	
	start_table(TABLESTYLE, "width='70%',6");

		$salesper = 0.0;
		$salesacc = 0.0;	
	
		//Get classes for PL
		$classresult = get_account_classes(false, 0);
		$total3=array();$urut=0;
		$ttls=array();$ttls2=array();$totalprofits=array();$totalnetprofits=array();
		$totalprofit=0;$totalnetprofit=0;$tincome=0;$toutcome=0;
		
			
		while ($class = db_fetch($classresult))
		{
			$class_per_total = 0;
			$class_acc_total = 0;
			$ttlclass=0;
			$convert = get_class_type_convert($class["ctype"]); 		
			
			//Print Class Name	
			//table_section_title($class["class_name"],4);	
			echo @$tableheader;
			
			//Get Account groups/types under this group/type
			$typeresult = get_account_types(false, $class['cid'], -1);
			$k = 0; // row color
			$total1=0;$total2=0;$ttls=array();
			while ($accounttype=db_fetch($typeresult))
			{
				start_row("class='inquirybg' style='font-weight:bold'");
				$acode='';
				if(@$_POST['AccCode']==1)
				$acode=$accounttype['id'].' - ';
				label_cell(_("").$acode.$accounttype['name'], "colspan=3");
				if($_POST["tipe"]==0){
					echo '<td align="center">'.$blnnya.'</td>';
				}else{
					for($a=$blnfrom;$a<=$blnto;$a++){
						//$tbals[$a]=0;
						echo '<td align="center">'.date('M Y',strtotime($thnto.'-'.$a.'-01')).'</td>';
					}
				}
				end_row();
				$pl0=display_pl0($accounttype['id'], @$typename);
				$pl=display_pl($accounttype['id'], @$typename);
				$total1=$total1+abs($pl0[0]);
				$total2=$total2+abs($pl[0]);

				$ttlclass+=abs($pl0[0])+abs($pl[0]);
				if($_POST["tipe"]!=0){
					for($a=$blnfrom;$a<=$blnto;$a++){
						$ttls[$a]=@$ttls[$a]+@$pl0[1][$a];
						$ttls[$a]=@$ttls[$a]+@$pl[1][$a];
						$ttls2[$a]=@$ttls2[$a]+@$ttls[$a];
					}
				}
			}
			
			//Print Class Summary
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_('Total') . " " . $class["class_name"]);
			echo '<td colspan="2">&nbsp;</td>';
			if($_POST["tipe"]==0){
				amount_cell(abs($ttlclass));

			}else{
				for($a=$blnfrom;$a<=$blnto;$a++){
					//$tbals[$a]=0;
					amount_cell(abs($ttls[$a]));
				}
			}
			if($class['ctype']==4)
				$tincome=$tincome+abs($total1)+abs($total2);
			if($class['ctype']==6)
				$toutcome=$toutcome+abs($total1)+abs($total2);
			//amount_cell($class_acc_total * $convert);
			//amount_cell(Achieve($class_per_total, $class_acc_total));
			end_row();	
			//if($urut>0)		
			$total3[]=($total1+$total2);
			//print_r($total3);
			//echo $tableheader='<tr><td colspan="4">&nbsp;</td></tr>';
			if($class['cid']==5){
				echo $grossprofit='
				<tr style="height: 24px;" class="inquirybg"><td colspan="'.(4+($blnto-$blnfrom)).'"></td></tr>
				<tr class="inquirybg" style="font-weight:bold"><td>GROSS PROFIT</td><td colspan="2">&nbsp;</td>';
				$tgp=abs(@$total3[0])-abs(@$total3[1]);
				if($_POST["tipe"]==0){
					echo '<td align="right">'.price_format($tgp).'</td>';
				}else{
					for($a=$blnfrom;$a<=$blnto;$a++){
						//$tbals[$a]=0;
						echo '<td align="right">'.price_format(abs(@$ttls2[$a])).'</td>';
					}
				}
				echo '</tr><tr style="height: 24px;" class="inquirybg"><td colspan="'.(4+($blnto-$blnfrom)).'"></td></tr>';
				$total3=array();$ttls2=array();
			}
			if($class['cid']==6){
				$totalprofit=$totalprofit+($total1+$total2);
				$top=abs($tgp)-abs(@$totalprofit);
				echo $grossprofit='
				<tr style="height: 24px;" class="inquirybg"><td colspan="'.(4+($blnto-$blnfrom)).'"></td></tr>
				<tr class="inquirybg" style="font-weight:bold"><td>OPERATING PROFIT</td><td colspan="2">&nbsp;</td>';
				if($_POST["tipe"]==0){
					echo '<td align="right">'.price_format($top).'</td>';
				}else{
					for($a=$blnfrom;$a<=$blnto;$a++){
						//$tbals[$a]=0;
						$totalprofits[$a]=@$totalprofits[$a]+abs(@$ttls2[$a]);
						echo '<td align="right">'.price_format(abs(@$totalprofits[$a])).'</td>';
					}
				}
				echo '</tr><tr style="height: 24px;" class="inquirybg"><td colspan="'.(4+($blnto-$blnfrom)).'"></td></tr>';
			}
			if($class['cid']==8 or $class['cid']==9){
				$totalnetprofit=$totalnetprofit+($total1+$total2);
				if($_POST["tipe"]!=0){
					for($a=$blnfrom;$a<=$blnto;$a++){
						//$tbals[$a]=0;
						$totalnetprofits[$a]=@$totalnetprofits[$a]+abs(@$ttls2[$a]);
					}
				}
			}
				
			if($class['cid']==9){
				echo $grossprofit='
				<tr style="height: 24px;" class="inquirybg"><td colspan="'.(4+($blnto-$blnfrom)).'"></td></tr>
				<tr class="inquirybg" style="font-weight:bold"><td>NET PROFIT Before Tax</td><td colspan="2">&nbsp;</td>';
				if($_POST["tipe"]==0){
					echo '<td align="right">'.price_format($tincome-$toutcome).'</td>';
				}else{
					for($a=$blnfrom;$a<=$blnto;$a++){
						//$tbals[$a]=0;
						echo '<td align="right">'.price_format(abs(@$totalprofits[$a])+abs(@$totalnetprofits[$a])).'</td>';
					}
				}
				echo '</tr>';
				$accounts=get_gl_accounts_in(array('69900','69990'));
				$total=0;$totals=array();
				while ($account = db_fetch($accounts))
				{
					echo $grossprofit='
					<tr class="inquirybg" style="font-weight:bold"><td>'.$account["account_name"].'</td><td colspan="2">&nbsp;</td>';
					if($_POST["tipe"]==0){
							$from=date('d/m/Y',strtotime($thnto.'-'.$blnfrom.'-01'));
							$to=date('d/m/Y',strtotime($thnto.'-'.$blnto.'-31'));
						$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], @$from, @$to, true, true);
						$total=$total+(abs(@$tot['balance']));
						echo '<td align="right">'.price_format(abs(@$tot['balance'])).'</td>';
					}else{
						for($a=$blnfrom;$a<=$blnto;$a++){
							$from=date('d/m/Y',strtotime($thnto.'-'.$a.'-01'));
							$to=date('d/m/Y',strtotime($thnto.'-'.$a.'-31'));
							$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], @$from, @$to, true, true);
							$totals[$a]=@$totals[$a]+(abs(@$tot['balance']));
							//$tbals[$a]=0;
							//$totals[$a]=1000;
							echo '<td align="right">'.price_format(abs(@$tot['balance'])).'</td>';
						}
					}
					echo '</tr>';
				}
				echo $grossprofit='
				<tr class="inquirybg" style="font-weight:bold"><td>NET PROFIT After Tax (LOSS)</td><td colspan="2">&nbsp;</td>';
				if($_POST["tipe"]==0){
					echo '<td align="right">'.price_format($tincome-$toutcome-abs(@$total)).'</td>';
				}else{
					for($a=$blnfrom;$a<=$blnto;$a++){
						//$tbals[$a]=0;
						echo '<td align="right">'.price_format(abs(@$totalprofits[$a])+abs(@$totalnetprofits[$a])-abs(@$totals[$a])).'</td>';
					}
				}
				echo '</tr><tr style="height: 24px;" class="inquirybg"><td colspan="'.(4+($blnto-$blnfrom)).'"></td></tr>';
			}
			
			$salesper += $class_per_total;
			$salesacc += $class_acc_total;
			$urut++;
		}

	
	end_table(1); // outer table
	div_end();
}

function display_profit_and_loss_pdf($compare)
{
	global $path_to_root, $compare_types;

	if (!isset($_POST['Dimension']))
		$_POST['Dimension'] = 0;
	if (!isset($_POST['Dimension2']))
		$_POST['Dimension2'] = 0;
	$dimension = $_POST['Dimension'];
	$dimension2 = $_POST['Dimension2'];

	$from = $_POST['TransFromDate'];
	$to = $_POST['TransToDate'];
	
	if (isset($_POST["AccGrp"]) && (strlen($_POST['AccGrp']) > 0))
		$drilldown = 1; // Deeper Level
	else
		$drilldown = 0; // Root level
	
	if ($compare == 0 || $compare == 2)
	{
		$end = $to;
		if ($compare == 2)
		{
			$begin = $from;
		}
		else
			$begin = begin_fiscalyear();
	}
	elseif ($compare == 1)
	{
		$begin = add_months($from, -12);
		$end = add_months($to, -12);
	}
	
	//div_start('pl_tbl');
	$tableheader='<div id="pl_tbl">';
	$tableheader.='<table class="TABLESTYLE" cellpadding="2" border="0" style="margin: auto;">';
	//start_table(TABLESTYLE, "width='70%',6");
	/*
	$tableheader =  "<tr>
        <td class='tableheader'>" . _("Group/Account Name") . "</td>
        <td class='tableheader'>" . _("Period") . "</td>
		<td class='tableheader'>" . $compare_types[$compare] . "</td>
		<td class='tableheader'>" . _("Achieved %") . "</td>
        </tr>";	
	*/
	if (!$drilldown) //Root Level
	{
		$salesper = 0.0;
		$salesacc = 0.0;	
	
		//Get classes for PL
		$classresult = get_account_classes(false, 0);
		$total3=array();$urut=0;
		$totalprofit=0;$totalnetprofit=0;
		while ($class = db_fetch($classresult))
		{
			$class_per_total = 0;
			$class_acc_total = 0;
			$convert = get_class_type_convert($class["ctype"]); 		
			
			//Print Class Name	
			//table_section_title($class["class_name"],4);	
			//echo $tableheader;
			
			//Get Account groups/types under this group/type
			$typeresult = get_account_types(false, $class['cid'], -1);
			$k = 0; // row color
			$total1=0;$total2=0;
			while ($accounttype=db_fetch($typeresult))
			{
				/*
				$TypeTotal = display_type($accounttype["id"], $accounttype["name"], $from, $to, $begin, $end, $compare, $convert, 
					$dimension, $dimension2, $drilldown, $path_to_root);
				$class_per_total += $TypeTotal[0];
				$class_acc_total += $TypeTotal[1];	
				if ($TypeTotal[0] != 0 || $TypeTotal[1] != 0 )
				{
					$url = "<a href='$path_to_root/gl/inquiry/profit_loss.php?TransFromDate=" 
						. $from . "&TransToDate=" . $to . "&Compare=" . $compare . "&Dimension=" . $dimension . "&Dimension2=" . $dimension2
						. "&AccGrp=" . $accounttype['id'] ."'>" . $accounttype['id'] . " " . $accounttype['name'] ."</a>";
						
					alt_table_row_color($k);
					label_cell($url);
					amount_cell($TypeTotal[0] * $convert);
					amount_cell($TypeTotal[1] * $convert);
					amount_cell(Achieve($TypeTotal[0], $TypeTotal[1]));
					end_row();
				}
				start_row("class='inquirybg' style='font-weight:bold'");
				label_cell(_("").$accounttype['id'] ." - ".$accounttype['name'], "colspan=4");
				end_row();
				*/

				$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
				$acode='';
				if(@$_POST['AccCode']==1)
				$acode=$accounttype['id'].' - ';
				$tableheader.='<td align="left" colspan="4">'.$acode.$accounttype['name'].'</td>';
				$tableheader.='</tr>';
				$dpl0=display_pl0_pdf($accounttype['id'], @$typename);
				$tableheader.=$dpl0[1];
				$total1=$total1+$dpl0[0];
				$dpl=display_pl_pdf($accounttype['id'], @$typename);
				$tableheader.=$dpl[1];
				$total2=$total2+$dpl[0];
				//$tableheader.='<tr><td colspan="4">&nbsp;</td></tr>';

			}
			
			//Print Class Summary
			/*
			start_row("class='inquirybg' style='font-weight:bold'");
			label_cell(_('Total') . " " . $class["class_name"]);
			echo '<td colspan="2">&nbsp;</td>';
			amount_cell($total1+$total2);
			end_row();	
			*/

			$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
			$tableheader.='<td align="left">Total '.$class["class_name"].'</td>';
			$tableheader.='<td align="left" colspan="2">&nbsp;</td>';
			$tableheader.='<td align="right">'.price_format(abs($total1)+abs($total2)).'</td>';
			$tableheader.='</tr>';


			$total3[]=($total1+$total2);
			//print_r($total3);
			//$tableheader.='<tr style="font-weight:bold"><td colspan="4">&nbsp;</td></tr>';
			if($class['cid']==5){
				$tableheader.='
				<tr class="inquirybg" style="line-height:30px;font-weight:bold;">
				<td>GROSS PROFIT</td><td colspan="2">&nbsp;</td>
				<td align="right">'.price_format(abs(@$total3[0])-abs(@$total3[1])).'</td></tr>
				';
				//$tableheader.='<tr><td colspan="4">&nbsp;</td></tr>';
				$total3=array();
			}
			if($class['cid']==6){

				$totalprofit=$totalprofit+($total1+$total2);
				$tableheader.='
				<tr class="inquirybg" style="line-height:30px;font-weight:bold;">
				<td>OPERATING PROFIT</td><td colspan="2">&nbsp;</td>
				<td align="right">'.price_format(abs(@$totalprofit)).'</td></tr>';
				//$tableheader.='<tr><td colspan="4">&nbsp;</td></tr>';
			}
			if($class['cid']==8 or $class['cid']==9){
				$totalnetprofit=$totalnetprofit+($total1+$total2);
			}
				
			if($class['cid']==9){
				$tableheader.='
				<tr class="inquirybg" style="font-weight:bold;"><td colspan="4">&nbsp;</td></tr>
				<tr class="inquirybg" style="font-weight:bold;">
				<td>Net Profit Before Tax</td><td colspan="2">&nbsp;</td>
				<td align="right">'.price_format(abs($totalprofit)+abs(@$totalnetprofit)).'</td></tr>';
				$accounts=get_gl_accounts_in(array('69900','69990'));
				$total=0;
				while ($account = db_fetch($accounts))
				{
					$tot = get_balance($account["account_code"], $_POST['Dimension'], $_POST['Dimension2'], $begin, $_POST['TransToDate'], false, true);
					$total=$total+($tot['debit']-$tot['credit']);
					$tableheader.='					
					<tr class="inquirybg" style="font-weight:bold;">
					<td>'.$account["account_name"].'</td><td colspan="2">&nbsp;</td>
					<td align="right">'.price_format(abs(($tot['debit']-$tot['credit']))).'</td></tr>
					';
				}
				$tableheader.='
					<tr class="inquirybg" style="font-weight:bold;">
					<td>Net Profit After Tax (Loss)</td><td colspan="2">&nbsp;</td>
				<td align="right">'.price_format(abs($totalprofit)+abs(@$totalnetprofit)-abs(@$total)).'</td></tr>';
				$tableheader.='<tr><td colspan="4">&nbsp;</td></tr>';
			}
			$salesper += $class_per_total;
			$salesacc += $class_acc_total;
		}
		/*
		start_row("class='inquirybg' style='font-weight:bold'");
		label_cell(_('Calculated Return'));
		amount_cell($salesper *-1);
		amount_cell($salesacc * -1);
		amount_cell(achieve($salesper, $salesacc));
		end_row();		
		*/
	}
	else 
	{
		//Level Pointer : Global variable defined in order to control display of root 
		global $levelptr;
		$levelptr = 0;
		
		$accounttype = get_account_type($_POST["AccGrp"]);
		$classid = $accounttype["class_id"];
		$class = get_account_class($classid);
		$convert = get_class_type_convert($class["ctype"]); 
		
		//Print Class Name	
		table_section_title($_POST["AccGrp"] . " " . get_account_type_name($_POST["AccGrp"]),4);	
		//echo $tableheader;
		
		$classtotal = display_type($accounttype["id"], $accounttype["name"], $from, $to, $begin, $end, $compare, $convert, 
			$dimension, $dimension2, $drilldown, $path_to_root);
		
	}
		
	$tableheader.= '</table></div>';
	//end_table(1); // outer table
	//div_end();
	return $tableheader;
}

//----------------------------------------------------------------------------------------------------

if (get_post('Export')) 
{
	
	$header='<table border="0" style="border:#000 1px solid;">
		<tr>
			<td width="100%" align="center"  style="background-color:#8072a9;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:20px;">Profit & Loss Statement</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 409pt;">';
	display_profit_and_loss(get_post('Compare'));
	echo '</td></tr></table>';
}elseif(get_post('PrintPdf')){

// add a page
	$header='<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#8072a9;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:20px;">Profit & Loss Statement</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		
	';
	$header.= '<tr><td style="width: 648px;">';
	$header.=display_profit_and_loss_pdf();
	$header.= '</td></tr></table>';
$pdf->AddPage();
// output the HTML content
$html=$header;
//$html.=display_bs_pdf();
$pdf->writeHTML(@$html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_PL.pdf', 'I');	

}else{
	
	start_form();

	inquiry_controls();

	div_start('pl_tbl');
	//if ($_POST['Show']) 
	if (get_post('Show')) {
		echo '<center><table border="0" cellpadding="4" width="80%">
		<tr>		
			<td align="center" style="font-weight:bold;">Profit & Loss</td>
		</tr>
		<tr>
			<td align="center" style="font-weight:bold;">Periode '.@$blnnya.'</td>
		</tr>';
		echo '<tr>		
			<td align="center">';
		display_profit_and_loss(get_post('Compare'));
		echo '</td></tr></table></center>';
	}
	//display_profit_and_loss(get_post('Compare'));

	end_form();

	end_page();

}

