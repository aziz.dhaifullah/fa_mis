<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/siar.inc");
include_once($path_to_root . "/gl/includes/db/gl_db_banking.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();
if (get_post('Export')) 
{
//	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"Pajak Masukan.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif (get_post('Pajak')) 
{
//	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"Template Pajak Masukan.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
page(_($help_context = "Pajak Masukan"), false, false, '', $js);
}
//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}
if (get_post('Update')) 
{
	/*
	for($a=1;$a<@$_POST['nfp'];$a++){
		echo @$_POST['fp'.$a];
	}*/
	$Ajax->activate('trans_tbl');
}



if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format(0);

//----------------------------------------------------------------------------------------------------

	if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
		$tgl1=$_POST['TransFromDate'];
		$tgl2=$_POST['TransToDate'];		
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=date('Y');
		$thnto=date('Y');
		$tgl1='01/'.$blnfrom.'/'.$thnfrom;
		$tgl2='31/'.$blnto.'/'.$thnto;		
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
function gl_inquiry_controls()
{
	if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
		$tgl1=$_POST['TransFromDate'];
		$tgl2=$_POST['TransToDate'];		
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=date('Y');
		$thnto=date('Y');
		$tgl1='01/'.$blnfrom.'/'.$thnfrom;
		$tgl2='31/'.$blnto.'/'.$thnto;		
	}
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER);
    echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'" '.($t==intval($blnfrom)?'selected':'').'>'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'" '.($t==intval($blnto)?'selected':'').'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==$thnfrom)
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	start_row();
   // gl_all_accounts_list_cells(_("Account:"), 'account', null, false, false, _("All Accounts"));
	//, -user_transaction_days()
	date_cells(_("&nbsp;"), 'TransFromDate', '', null);
	date_cells(_("&nbsp;"), 'TransToDate', '', null);
    end_row();
	end_table();

	start_table(TABLESTYLE_NOBORDER);
	start_row();

	echo '<td colspan="4">&nbsp;</td>';
	submit_cells('Show',_("Show"),'align="right"','', 'default');
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">
	</td>';
	echo '<td><input type="submit" class="export" name="Pajak" id="Pajak" Value="Export to Template Pajak">
	</td>';
	echo '
	<style>
		.export{
			align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle2").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Management_Fee",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
	//submit_cells('Export Excel',_("Export"),'','', 'default');
	end_row();
	end_table();

	echo '<hr>';
    end_form();
}

//----------------------------------------------------------------------------------------------------

function show_results()
{
	global $path_to_root, $systypes_array;

	/*
	start_table(TABLESTYLE2, "width='90%'", 10);
	
	$th = array(_("No"), _("Kode"), _("NPWP"),  _("Nama"));
	    			
	table_header($th);
	*/

	if($_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
		$tgl1=$_POST['TransFromDate'];
		$tgl2=$_POST['TransToDate'];		
	}else{
		$blnfrom=$_POST['bulanfrom'];
		$blnto=$_POST['bulanto'];
		$thnfrom=date('Y');
		$thnto=date('Y');
		$tgl1='01/'.$blnfrom.'/'.$thnfrom;
		$tgl2='31/'.$blnto.'/'.$thnto;		
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

	for($a=1;$a<@$_POST['nfp'];$a++){
		$idfp=@$_POST['type'.$a];
		$fpnya=@$_POST['trans'.$a];
		$fp=@$_POST['fp'.$a];
		$tglfp=@$_POST['tglfp'.$a];
		if($fp=='')
			$tglfp=date('Y-m-d');
		$sql="select count(id) as n from 0_fp_trans where id =  '".$fpnya."' and type= '".$idfp."' ";
	
		$result=db_query($sql, '');
		$dfp = db_fetch_assoc($result);
		//$sqlupd="UPDATE 0_trans_tax_details SET fp ='".$fpnya."'  WHERE `0_trans_tax_details`.`id` = ".$idfp." ";
		if($fp!=''){
			if($dfp['n']<1)
			$sqlupd="Insert into 0_fp_trans (id,type,reference,tgl) values ('".$fpnya."' ,'".$idfp."','".$fp."','".date2sql($tglfp)."') ";
			else
			$sqlupd="UPDATE 0_fp_trans SET reference ='".$fp."',tgl ='".date('Y-m-d h:i:s',strtotime(date2sql($tglfp)))."'  WHERE `id` = ".$fpnya." and `type` = ".$idfp." ";
			db_query($sqlupd, "could not add");
		}
	}
	$tableheader='<form method="post" action="/fa/gl/inquiry/ppnm.php">
	<center><table class="tablestyle" cellpadding="6" border="1" width="98%">';

	$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
	$tableheader .=  "
	<td class='tableheader' align='center'>" . _("No") . "</td>
	<td class='tableheader' align='center'>" . _("PKP") . "</td>
	<td class='tableheader' align='center'>" . _("NPWP") . "</td>
	<td class='tableheader' align='center'>" . _("") . "</td>
	<td class='tableheader' align='center'>" . _("") . "</td>
	<td class='tableheader' align='center'>" . _("") . "</td>
	<td class='tableheader' align='center'>" . _("FP") . "</td>
	<td class='tableheader' align='center'>" . _("Tgl FP") . "</td>
	<td class='tableheader' align='center'>" . _("Tgl") . "</td>
	<td class='tableheader' align='center'>" . _("DPP") . "</td>
	<td class='tableheader' align='center'>" . _("PPN") . "</td>
	<td class='tableheader' align='center'>" . _("Notes") . "</td>
	</tr>
	";
	$tableheader.='</tr>';
	$sql="select a.*,d.*,c.reference,c.tgl as tglfp
from 0_gl_trans a
left join 0_fp_trans c on a.type=c.type and a.type_no=c.id 
left join 0_suppliers d on a.person_id=d.supplier_id 
left join 0_voided b on a.type=b.type and a.type_no=b.id
where  b.id IS NULL and
a.account like '11703' 
and a.amount >0
and a.tran_date between '".date2sql($_POST['TransFromDate'])."' and '".date2sql($_POST['TransToDate'])."' 
order by a.tran_date asc";
	//$sql = get_pajak_masukan(-1, $tgl1,$tgl2);
	$result=db_query($sql, '');

		$no=1;$a=0;
		$ttljml=0;$ttlbayar=0;$ttlselisih=0;
		while($mfees = db_fetch_assoc($result))
		{
			//print_r($mfees);echo'<br>';
			if(($a % 2) == 1)
			$tableheader.='<tr class="evenrow">';
			else
			$tableheader.='<tr class="oddrow">';
			//print_r($mfees);echo '<br>';
			$noinv=@$mfees['reference'];

			$sql2 = "SELECT supp.*
			FROM ".TB_PREF."supp_trans trans, ".TB_PREF."suppliers supp
			WHERE trans_no=".db_escape(@$mfees['type_no'])." AND type=".db_escape(@$mfees['type'])
			." AND trans.supplier_id=supp.supplier_id ";
			$result2 = db_query($sql2,"The next transaction number for  could not be retrieved");
		    $person = db_fetch($result2);
			$nama=@$person['supp_name'];
			$npwp=@$person['gst_no'];
			$alamat=@$person['address'];
			$ppn=abs($mfees['amount']);

			$sql3 = "select c.* from ".TB_PREF."gl_trans gl 
			inner join ".TB_PREF."refs b on b.id=gl.type_no and b.type=gl.type 
			inner join ".TB_PREF."marketing_trans c on c.ref=b.reference and c.amount=gl.amount 
			left join 0_voided d on gl.type_no=d.id and gl.type=d.type
			where gl.type_no = ".db_escape(@$mfees['type_no'])."
			AND gl.type = ".db_escape(@$mfees['type'])."
			AND d.id IS NULL ";
			$result3 = db_query($sql3,"The next transaction number for  could not be retrieved");
		    $person2 = db_fetch($result3);
		    if(@$person2['marketing_name']!=''){

				$nama=@$person2['marketing_name'];
				$npwp=@$person2['npwp'];
				$alamat=@$person2['address'];
		    }
		    if($nama==''){
				$person2=explode('//',get_counterparty_name(@$mfees['type'],@$mfees['type_no']));
				//print_r($person2);
				$nama=@$person2[0];
				$npwp=@$person2[1];
				$alamat=@$person2[2];
		    }
			$notes=$mfees['memo_'];
			if($notes==''){				
				$memos=get_gl_memo2($mfees['type_no'], $mfees['type']);
				$notes=($memos[1]);
			}
			
			$npwp=str_replace('.','',$npwp);
			$npwp=str_replace('-','',$npwp);
			$curr=$mfees['curr_code'];
			$masa=date('d M Y',strtotime($mfees['tran_date']));
			$ppn=abs($mfees['amount']);
			$dpp=$ppn*10;
			//$ppn=0;
			//var_dump($nama);
			/*$sql="select id,net_amount,fp from 0_trans_tax_details where trans_type='".@$mfees['trans_type']."' and trans_no='".@$mfees['trans_no']."' order by id asc";
			$query=db_query($sql, "The transactions for could not be retrieved");
			while ($myrow0 = db_fetch($query))
			{*/
				//$id=@$mfees['idtax'];
				$tglfp='';
				$fp=@$mfees['reference'];
				if($fp!='')
				$tglfp=@$mfees['tglfp'];
				$fps=explode('.', @$fp);
			//}

			if(trim(@$nama)==''){
				$nama=@$mfees['memo_'];
				$sql3 = "select name,debtor_ref,address,address2,tax_id from ".TB_PREF."debtors_master";
				$result3 = db_query($sql3,"The next transaction number for  could not be retrieved");
			    while($debtors = db_fetch($result3)){
			    	//echo $debtors[1].' ';
			    	if(strpos($nama,$debtors[1])>0){
			    		//echo strpos($nama,$debtors[1]).',';
				    	$nama=$debtors[0];
				    	$npwp=$debtors[4];
				    }
			    }
			}
			$tableheader .="
				<td align='left'>" . $no . "</td>
				<td align='left' width='150px;'>" . $nama . "</td>
				<td align='left'>" . @$npwp . "&nbsp;</td>
				<td align='left'>" . @$fps[0] . "</td>
				<td align='left'>" . @$fps[1] . "</td>
				<td align='left'>" . @$fps[2] . "</td>
				<td align='center'>";
			if (get_post('Export')) {
				$tableheader .=$fp;
			}
			else{
			$tableheader .="
				<input type='text' name='fp".$no."'  size='20' value='".@$fp."'>
				<input type='hidden' name='type".$no."'  size='20' value='".@$mfees['type']."'>
				<input type='hidden' name='trans".$no."'  size='20' value='".@$mfees['type_no']."'>";
			}				
			$tableheader .="
				</td><td align='center'>";

			if (get_post('Export')) {
				$tableheader .=$tglfp;
			}
			else{
			$tableheader .='<input type="text" name="tglfp'.$no.'" size="10" id="tglfp'.$no.'" class="date" value="'.($tglfp!=''?date('d/m/Y',strtotime(@$tglfp)):'').'" autocomplete="off">
			<a tabindex="-1" href="javascript:date_picker(document.getElementsByName(\'tglfp'.$no.'\')[0]);">	
			<img src="../../themes/default/images/cal.gif" style="vertical-align:middle;padding-bottom:4px;width:16px;height:16px;border:0;" alt="Click Here to Pick up the date">
			</a>';
			}		
			$tableheader .="</td>
				<td align='left'>" . @$masa . "</td>
				<td align='right'>" .price_format($dpp) . "</td>
				<td align='right'>" . price_format($ppn) . "</td>
				<td align='left' width='100px;'>" . @$notes . "</td>
				</tr>";
			$ttljml=$ttljml+$dpp;
			$ttlbayar=$ttlbayar+$ppn;
			$no++;
			$a++;
	    }
			$tableheader .="
				<td align='left'>&nbsp;</td>
				<td align='left'><b>Total</b> </td>
				<td align='left'>&nbsp;</td>
				<td align='left'>&nbsp;</td>
				<td align='left'>&nbsp;</td>
				<td align='left'>&nbsp;</td>
				<td align='left'>&nbsp;</td>
				<td align='left'>&nbsp;</td>
				<td align='left'>&nbsp;</td>
				<td align='right'><b>" . price_format($ttljml) . "</b></td>
				<td align='right'><b>" . price_format($ttlbayar) . "</b></td>
				<td align='left'>&nbsp;</td>
				</tr>";
    //}else{
    if($a==0){
    	$tableheader .= '<tr><td colspan="12" align="center"><b>No Data Available</b></td></tr>';
    }
	$tableheader .= '<tr><td colspan="12" align="center">
		<button type="submit" class="ajaxsubmit" aspect="default" name="Update" id="Update" value="Update">
		<img src="../../themes/default/images/ok.gif" height="12" alt=""><span>Update</span></button>
	</td></tr>';
    $tableheader.='
    <input type="hidden" name="nfp" value="'.$no.'">
    <input type="hidden" name="TransFromDate" value="'.$tgl1.'">
    <input type="hidden" name="TransToDate" value="'.$tgl2.'">
    <input type="hidden" name="bulanfrom" value="'.$blnfrom.'">
    <input type="hidden" name="bulanto" value="'.$blnto.'">
    <input type="hidden" name="Show" value="Show">
    </form>';
    return $tableheader;
}

function show_results2()
{
	global $path_to_root, $systypes_array;

	/*
	start_table(TABLESTYLE2, "width='90%'", 10);
	
	$th = array(_("No"), _("Kode"), _("NPWP"),  _("Nama"));
	    			
	table_header($th);
	*/

	if($_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
		$tgl1=$_POST['TransFromDate'];
		$tgl2=$_POST['TransToDate'];		
	}else{
		$blnfrom=$_POST['bulanfrom'];
		$blnto=$_POST['bulanto'];
		$thnfrom=date('Y');
		$thnto=date('Y');
		$tgl1='01/'.$blnfrom.'/'.$thnfrom;
		$tgl2='31/'.$blnto.'/'.$thnto;		
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

	for($a=1;$a<@$_POST['nfp'];$a++){
		$idfp=@$_POST['id'.$a];
		$fpnya=@$_POST['fp'.$a];
		if($fpnya!=''){
			$sqlupd="UPDATE 0_trans_tax_details SET fp ='".$fpnya."'  WHERE `0_trans_tax_details`.`id` = ".$idfp." ";
			db_query($sqlupd, "");
		}
	}
	$tableheader='<form method="post" action="/fa/gl/inquiry/ppnm.php">
	<center><table class="tablestyle" cellpadding="6" border="1" width="98%">';

	$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
	$tableheader .=  "
	<td class='tableheader' align='center'>" . _("KD_JENIS_TRANSAKSI") . "</td>
	<td class='tableheader' align='center'>" . _("FG_PENGGANTI") . "</td>
	<td class='tableheader' align='center'>" . _("NOMOR_FAKTUR") . "</td>
	<td class='tableheader' align='center'>" . _("MASA_PAJAK") . "</td>
	<td class='tableheader' align='center'>" . _("TAHUN_PAJAK") . "</td>
	<td class='tableheader' align='center'>" . _("TANGGAL_FAKTUR") . "</td>
	<td class='tableheader' align='center'>" . _("NPWP") . "</td>
	<td class='tableheader' align='center'>" . _("NAMA") . "</td>
	<td class='tableheader' align='center'>" . _("ALAMAT_LENGKAP") . "</td>
	<td class='tableheader' align='center'>" . _("JUMLAH_DPP") . "</td>
	<td class='tableheader' align='center'>" . _("JUMLAH_PPN") . "</td>
	<td class='tableheader' align='center'>" . _("JUMLAH_PPNBM") . "</td>
	<td class='tableheader' align='center'>" . _("IS_CREDITABLE") . "</td>
	</tr>
	";
	$tableheader.='</tr>';
	$sql = get_pajak_masukan(-1, $tgl1,$tgl2);
	$result=db_query($sql, '');

		$no=1;$a=0;
		$ttljml=0;$ttlbayar=0;$ttlselisih=0;
		while($mfees = db_fetch_assoc($result))
		{
			//print_r($mfees);echo'<br>';
			if(($a % 2) == 1)
			$tableheader.='<tr class="evenrow">';
			else
			$tableheader.='<tr class="oddrow">';
			//print_r($mfees);echo '<br>';
			$noinv=$mfees['reference'];

			$nama=@$mfees['supp_name'];
			$npwp=@$mfees['gst_no'];
			$alamat=$mfees['address'];
			if($nama==''){
				$persons=get_counterparty_name(@$mfees['trans_type'],@$mfees['trans_no']);
				$person=explode('//',$persons);
				$nama=@$person[0];
				$npwp=@$person[1];
				$alamat=@$person[2];
			}
			$npwp=str_replace('.','',$npwp);
			$npwp=str_replace('-','',$npwp);
			$curr=$mfees['curr_code'];
			$masa=date('d M Y',strtotime($mfees['tran_date']));
			$ppn=$mfees['amount'];
			$dpp=0;
			//$ppn=0;
			//var_dump($nama);
			/*$sql="select id,net_amount,fp from 0_trans_tax_details where trans_type='".@$mfees['trans_type']."' and trans_no='".@$mfees['trans_no']."' order by id asc";
			$query=db_query($sql, "The transactions for could not be retrieved");
			while ($myrow0 = db_fetch($query))
			{*/
				$id=@$mfees['idtax'];
				$dpp=abs(@$mfees['net_amount']);
				$fp=@$mfees['fp'];
				$fps=explode('.', $fp);
			//}
			$tableheader .="
				<td align='center'>01</td>
				<td align='center'>0</td>
				<td align='center'>" . $fp . "&nbsp;</td>
				<td align='center'>" . date('m',strtotime($mfees['tran_date'])) . "</td>
				<td align='center'>" . date('Y',strtotime($mfees['tran_date'])) . "</td>
				<td align='center'>" . $masa . "</td>
				<td align='left'>" . @$npwp . "&nbsp;</td>
				<td align='left'>" . $nama . "</td>
				<td align='left'>" . $alamat . "</td>
				<td align='right'>" .price_format($dpp) . "</td>
				<td align='right'>" . price_format($ppn) . "</td>
				<td align='right'>0</td>
				<td align='right'>1</td>
				</tr>";
			$ttljml=$ttljml+$dpp;
			$ttlbayar=$ttlbayar+$ppn;
			$no++;
			$a++;
	    }
			

    return $tableheader;
}
//----------------------------------------------------------------------------------------------------

if (get_post('Export')) 
{
	
	$header='
	<style>
	.inquirybg{
		background-color:#000 !important;
		color:#fff !important;
		font-size:11px !important;
	}
	.tablestyle{
		font-size:11px !important;
	}
	td{
		vertical-align:top !important;
	}
	</style>
	<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">Pajak Masukan</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Masa '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 351;">';
	echo show_results();
	echo '</td></tr></table>';
}
elseif (get_post('Pajak')) 
{
	echo show_results2();
}
else{
	gl_inquiry_controls();

	div_start('trans_tbl');

		if (get_post('Show')) {
			echo '<style>
	td{
		vertical-align:top !important;
	}
	</style>
	<center><table border="0" cellpadding="4" width="98%">
			<tr>		
			<td align="center" style="font-weight:bold;">Pajak Masukan</td>
			</tr>
			<tr>
				<td align="center" style="font-weight:bold;">Masa '.@$blnnya.'</td>
			</tr>';
			echo '<tr>		
				<td align="center">';
			echo show_results();
			echo '</td></tr></table></center>';
		}
	   // show_results();

	div_end();

	//----------------------------------------------------------------------------------------------------

	end_page();
	}

