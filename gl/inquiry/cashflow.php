<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_TAXREP';
$path_to_root="../..";
include_once($path_to_root . "/includes/session.inc");


include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"Laporan Arus Kas.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
page(_($help_context = "Laporan Arus Kas"), false, false, '', $js);
}
//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}

	
		$blnfrom=date('m',strtotime(date2sql(@$_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql(@$_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql(@$_POST['TransFromDate'])))-1;
		$thnto=date('Y',strtotime(date2sql(@$_POST['TransToDate'])));
	
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F',strtotime($thnto.'-'.$blnfrom.'-01')).'-'.date('F',strtotime($thnto.'-'.$blnto.'-01')).' - '.date('Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).'-'.date('Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}

/*
if (get_post('TransFromDate') == "" && get_post('TransToDate') == "")
{
	$date = Today();
	$row = get_company_prefs();
	$edate = add_months($date, -$row['tax_last']);
	$edate = end_month($edate);
	$bdate = begin_month($edate);
	$bdate = add_months($bdate, -$row['tax_prd'] + 1);
	$_POST["TransFromDate"] = $bdate;
	$_POST["TransToDate"] = $edate;
}	
*/
//----------------------------------------------------------------------------------------------------

function tax_inquiry_controls()
{
    start_form();

    start_table(TABLESTYLE_NOBORDER);
    echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";

	echo '<tr>';
    date_cells(_("&nbsp;"), 'TransFromDate');
	date_cells(_("&nbsp;"), 'TransToDate');
	start_row();
	submit_cells('Show',_("Show"),'','', 'default');
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">&nbsp;';
	echo '<input type="submit" class="export" name="PrintPdf" id="PrintPdf" Value="Print PDF"></td>';
	echo '<script>
	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle2").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Management_Fee",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
    end_row();

	end_table();

    end_form();
}

//----------------------------------------------------------------------------------------------------

function show_results()
{
    /*Now get the transactions  */
	if(get_post('PrintPdf'))
	$tableheader ='<table class="tablestyle" width="98.5%" cellpadding="3" border="0">';
	else
	$tableheader ='<table class="tablestyle" width="60%" cellpadding="10" border="1">';

	$begin=@$_POST['tahunfrom']-1;
	$end=@$_POST['tahunfrom'];
	//$thn=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
	$spasi='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	$tableheader .= '<tr>';
	$tableheader .= '<td class="tableheader" bgcolor="#dee7ec">&nbsp;</td>';	

	for($a=$begin;$a<=$end;$a++){		
		$tableheader .= '<td class="tableheader" bgcolor="#dee7ec"><b>'.$a.'</b></td>';
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td colspan="3"><b>Arus Kas Dari Aktivitas Operasi</b></td>';
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$ttlall=0;
	$tableheader .= '<td>'.$spasi.'Penerimaan atas Jasa Pengelolaan Investasi</td>';
	for($a=$begin;$a<=$end;$a++){
		$thn=$a;
		$jan='01/01/'.$thn;
		$des='31/12/'.$thn;
		//Piutang Jasa Management
		$tot = get_balance('41000', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);	

		$total=0;
		$total+=$tot['balance'];
		if($thn==2016)
			$total=73738072269;
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(abs($total)).'</td>';
		$ttlall+=$total;
		//$jumlah+=abs($tot['balance']);
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td>'.$spasi.'Pembayaran Operasi Lainnya</td>';
	$accounts = get_gl_accounts(null, null, '6-0000');
	for($a=$begin;$a<=$end;$a++){
		$jumlah=0;
		while ($account = db_fetch($accounts))
		{
			$thn=$a;
			$jan='01/01/'.$thn;
			$des='31/12/'.$thn;
			$tot = get_balance($account['account_code'], @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);
			$jumlah+=abs($tot['balance']);
		}			
		$jumlah=0;
		if($a==2016)
			$jumlah=-52312979572;
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($jumlah)).'</td>';
		$ttlall+=$jumlah;
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td>'.$spasi.'Pembayaran Pajak Penghasilan</td>';
	for($a=$begin;$a<=$end;$a++){
		$thn=$a;
		$jan='01/01/'.$thn;
		$des='31/12/'.$thn;
		//PPh Ps.23 dibayar dimuka
		$tot = get_balance('11701', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);	

		$total=0;
		$total+=$tot['balance'];
		if($a==2016)
			$total=-4040253452;
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($total)).'</td>';
		$ttlall+=$total;
		//$jumlah+=abs($tot['balance']);
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td>'.$spasi.'Pembelian Portofolio Efek</td>';
	$accounts = get_gl_accounts(null, null, '1-1300');
	for($a=$begin;$a<=$end;$a++){
		$jumlah=0;
		while ($account = db_fetch($accounts))
		{
			$thn=$a;
			$jan='01/01/'.$thn;
			$des='31/12/'.$thn;
			$tot = get_balance($account['account_code'], @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);
			$jumlah+=abs($tot['balance']);
		}			
		$jumlah=0;
		if($a==2016)
			$jumlah=-33000000000;
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($jumlah)).'</td>';
		$ttlall+=$jumlah;
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td>'.$spasi.'Penerimaan dari Penjualan Portofolio Efek </td>';
	for($a=$begin;$a<=$end;$a++){
		$thn=$a;
		$jan='01/01/'.$thn;
		$des='31/12/'.$thn;
		//Keuntungan Perdagangan Efek
		$tot = get_balance('43000', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);

		$total=0;
		$total+=$tot['balance'];
		if($a==2016)
			$total=28325372366;
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($total)).'</td>';
		$ttlall+=$total;
		//$jumlah+=abs($tot['balance']);
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td>'.$spasi.'Penerimaan Penghasilan Bunga</td>';
	for($a=$begin;$a<=$end;$a++){
		$thn=$a;
		$jan='01/01/'.$thn;
		$des='31/12/'.$thn;
		//Laba/(Rugi) belum direalisasi
		$tot = get_balance('43002', @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);	

		$total=0;
		$total+=$tot['balance'];
		if($a==2016)
			$total=707448771;
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($total)).'</td>';
		$ttlall+=$total;
		//$jumlah+=abs($tot['balance']);
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td>'.$spasi.$spasi.'Kas Bersih Diperoleh dari (Digunakan Untuk)<br>'.$spasi.$spasi.' Aktivitas Operasi</td>';
	for($a=$begin;$a<=$end;$a++){
		$ttlall=0;
		if($a==2016)
			$ttlall=13417660382;

		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($ttlall)).'</td>';
	}
	$tableheader .= '</tr>';
	$ttlall=0;
	$tableheader .= '<tr>';
	$tableheader .= '<td colspan="3"><b>Arus Kas Dari Aktivitas Investasi</b></td>';
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td>'.$spasi.'Perolehan Aset Tetap</td>';
	$accounts = get_gl_accounts(null, null, '1-2200');
	for($a=$begin;$a<=$end;$a++){
		$jumlah=0;
		while ($account = db_fetch($accounts))
		{
			$thn=$a;
			$jan='01/01/'.$thn;
			$des='31/12/'.$thn;
			$tot = get_balance($account['account_code'], @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);
			$jumlah+=abs($tot['balance']);
		}			
		$jumlah=0;
		if($a==2016)
			$jumlah=-926341746;
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($jumlah)).'</td>';
		$ttlall+=$jumlah;
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td>'.$spasi.'Penjualan Aset Tetap</td>';
	$accounts = get_gl_accounts(null, null, '1-2200');
	for($a=$begin;$a<=$end;$a++){
		$jumlah=0;
		while ($account = db_fetch($accounts))
		{
			$thn=$a;
			$jan='01/01/'.$thn;
			$des='31/12/'.$thn;
			$tot = get_balance($account['account_code'], @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);
			$jumlah+=abs($tot['balance']);
		}			
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(abs($jumlah)).'</td>';
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td>'.$spasi.'Uang Muka Pembelian Gedung</td>';
	for($a=$begin;$a<=$end;$a++){
		$tableheader .= '<td style="text-align:right;">-</td>';
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td>'.$spasi.$spasi.'Kas Bersih Diperoleh dari Aktivitas Investasi</td>';
	for($a=$begin;$a<=$end;$a++){
		$ttlall=0;
		if($a==2016)
			$ttlall=-926341746;
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($ttlall)).'</td>';
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td colspan="3"><b>Arus Kas Dari Aktivitas Pendanaan</b></td>';
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td>'.$spasi.'Penurunan Piutang Lain-lain - Pihak-pihak Berelasi</td>';

	$accounts = get_gl_accounts(null, null, '1-1599');
	for($a=$begin;$a<=$end;$a++){
		$jumlah=0;
		while ($account = db_fetch($accounts))
		{
			$thn=$a;
			$jan='01/01/'.$thn;
			$des='31/12/'.$thn;
			$tot = get_balance($account['account_code'], @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);
			$jumlah+=abs($tot['balance']);
		}		
		if($a==2016)
			$jumlah=71317524;
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($jumlah)).'</td>';
		$ttlall+=$jumlah;
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td>'.$spasi.'Pembayaran Dividen</td>';
	$accounts = get_gl_accounts(null, null, '1-1598');
	for($a=$begin;$a<=$end;$a++){
		$jumlah=0;
		while ($account = db_fetch($accounts))
		{
			$thn=$a;
			$jan='01/01/'.$thn;
			$des='31/12/'.$thn;
			$tot = get_balance($account['account_code'], @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);
			$jumlah+=abs($tot['balance']);
		}			
		if($a==2016)
			$jumlah=-6600000000;
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($jumlah)).'</td>';
		$ttlall+=$jumlah;
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td>'.$spasi.'Penerimaan Utang Lain-lain</td>';
	$accounts = get_gl_accounts(null, null, '1-1598');
	for($a=$begin;$a<=$end;$a++){
		$jumlah=0;
		while ($account = db_fetch($accounts))
		{
			$thn=$a;
			$jan='01/01/'.$thn;
			$des='31/12/'.$thn;
			$tot = get_balance($account['account_code'], @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);
			$jumlah+=abs($tot['balance']);
		}			
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($jumlah)).'</td>';
		$ttlall+=$jumlah;
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td>'.$spasi.'Penurunan Utang Lain-lain</td>';
	$accounts = get_gl_accounts(null, null, '1-1500');
	for($a=$begin;$a<=$end;$a++){
		$jumlah=0;
		while ($account = db_fetch($accounts))
		{
			$thn=$a;
			$jan='01/01/'.$thn;
			$des='31/12/'.$thn;
			$tot = get_balance($account['account_code'], @$_POST['Dimension'], @$_POST['Dimension2'], $jan, $des, true, true);
			$jumlah+=abs($tot['balance']);
		}			
		if($a==2016)
			$jumlah=-2207554625;
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($jumlah)).'</td>';
		$ttlall+=$jumlah;
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td>'.$spasi.$spasi.'Kas Bersih Diperoleh dari Aktivitas Pendanaan</td>';
	for($a=$begin;$a<=$end;$a++){
		$ttlall=0;
		if($a==2016)
			$ttlall=-8736237101;
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($ttlall)).'</td>';
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td><b>Peningkatan Bersih Kas dan Setara Kas</b></td>';
	for($a=$begin;$a<=$end;$a++){
		$ttlall=0;
		if($a==2016)
			$ttlall=3755081535;
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($ttlall)).'</td>';
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td><b>Kas dan Setara Kas, Awal Tahun</b></td>';
	for($a=$begin;$a<=$end;$a++){
		$ttlall=0;
		if($a==2016)
			$ttlall=136628838570;
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($ttlall)).'</td>';
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td><b>Kas dan Setara Kas, Akhir Tahun</b></td>';
	for($a=$begin;$a<=$end;$a++){
		$ttlall=0;
		if($a==2016)
			$ttlall=17377920105;
		$tableheader .= '<td style="text-align:right;">'.price_format_pr(($ttlall)).'</td>';
	}
	$tableheader .= '</tr>';

	$tableheader .= '<tr>';
	$tableheader .= '<td colspan="3" style="text-align:center;">Lihat Catatan Atas Laporan Keuangan yang merupakan<br>
	bagian tidak terpisahkan dari Laporan Keuangan Ini</td>';
	$tableheader .= '</tr>';
	$tableheader.='</table></center>';
	return $tableheader;
}

//----------------------------------------------------------------------------------------------------


if (get_post('Export')) 
{
	$header='
	<style>
	.tableheader{
		background-color:#000 !important;
		color:#fff !important;
	}
	.tablestyle{
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0" style="border:#000 1px solid;">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Laporan Arus Kas</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Untuk Tahun Yang Berakhir Tanggal - Tanggal</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">'.@$blnnya.'</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">(Dinyatakan Dalam Rupiah, Kecuali Dinyatakan Lain)</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>	
	';
	echo $header;
	echo '<tr><td style="width: 609px;">';
	echo show_results();
	echo '</td></tr></table>';
}elseif(get_post('PrintPdf')){

// add a page
	$header='<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Laporan Arus Kas</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Untuk Tahun Yang Berakhir Tanggal - Tanggal</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">'.@$blnnya.'</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">(Dinyatakan Dalam Rupiah, Kecuali Dinyatakan Lain)</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	

	$header.= '<tr><td style="width: 646px;">';
	$header.=show_results();
	$header.= '</td></tr></table>';
$pdf->AddPage();
// output the HTML content
$html=$header;
//$html.=display_bs_pdf();
$pdf->writeHTML(@$html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('ringkasanPajak.pdf', 'I');	

}else{

tax_inquiry_controls();
		div_start('trans_tbl');
	if (get_post('Show')) {
		echo '<center><table border="0" cellpadding="0" width="80%">
		<tr>		
			<td align="center" style="font-weight:bold;" colspan="3">Laporan Arus Kas</td>
		</tr>
		<tr>
			<td align="center" style="font-weight:bold;" colspan="3">Periode '.@$blnnya.'</td>
		</tr>';
		echo '<tr>		
			<td align="center" colspan="3">';
		echo show_results();
		echo '</td></tr></table></center>';
	}
		div_end();
end_page();
	//show_results();
}
//----------------------------------------------------------------------------------------------------


