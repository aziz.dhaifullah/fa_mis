<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/siar.inc");
include_once($path_to_root . "/gl/includes/siap.inc");
include_once($path_to_root . "/gl/includes/db/gl_db_banking.inc");
include_once($path_to_root . "/sales/includes/db/customers_db.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();

if (get_post('Export')) 
{
	//include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"Management Fee.xls\"");
	header("Cache-Control: max-age=0");
	
	
}else{
	page(_($help_context = "Management Fee"), false, false, '', $js);
}
//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}

if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format(0);

//----------------------------------------------------------------------------------------------------

		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
		$tgl1=$_POST['TransFromDate'];
		$tgl2=$_POST['TransToDate'];	

	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    
    start_table(TABLESTYLE_NOBORDER);

	echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	start_row();
   // gl_all_accounts_list_cells(_("Account:"), 'account', null, false, false, _("All Accounts"));
	//, -user_transaction_days()
	date_cells(_("From :"), 'TransFromDate', '', null);
	date_cells(_("to :"), 'TransToDate', '', null);
    end_row();
	end_table();
	

	start_table(TABLESTYLE_NOBORDER);
	start_row();

	//echo '<td colspan="6" style="text-align:center;">&nbsp;</td>';
	submit_cells('Show',_("Show"),'align="center" colspan="6"','', 'default');
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">
	</td>';
	//echo '<td><a class="ajaxsubmit export" onclick="xls()" name="Export Excel" id="Export Excel" value="Export"><span>Export Excel</span></a></td>';
	
	echo '
	<style>
		.export{
			align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle2").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Management_Fee",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
	//submit_cells('Export Excel',_("Export"),'','', 'default');
	end_row();
	end_table();

	echo '<hr>';
    end_form();
}

//----------------------------------------------------------------------------------------------------

function show_results()
{
	global $path_to_root, $systypes_array;


	start_table(TABLESTYLE2, "width='90%'", 10);
	
	$th = array(_("No"), _("Code"), _("Product"), _("Publish Date"), _("Management Fee (DPP)"), _("Management Fee (Gross)"), _("Last Date"),  _(""));
	    			
	table_header($th);
	//$mfees=get_mgtfee(date2sql($_POST['TransFromDate']),date2sql($_POST['TransToDate']));
	$mfees=get_mgtfee_siap(date2sql($_POST['TransFromDate']),date2sql($_POST['TransToDate']));
	//print_r($mfees);
	$isi=array();$nos=array();
	if(count($mfees)>0){
		$no=1;
		for($a=0;$a<count($mfees);$a++)
		{
			//print_r($mfees[$a]);
			//$mgtfee=(100/98)*$mfees[$a][0];
			//$mgtfee=$mgtfee/1.1;
			$mgtfee=$mfees[$a][0]/1.1;
			//$pph=$mgtfee*0.02;
			//$mgtfee=$mfees[$a][0]-$pph;
			//$mgtfee=(100/98)*$mfees[$a][0];
			//$mgtfee=$mfees[$a][0]/1.1;
			$custid2='';
			$shrfee=0;
			$custid=get_customer_id_kode($mfees[$a][6]);
			$custid2=get_customer_ref(@$custid);
			
			$invoice='<form method="post" action="../../sales/sales_order_entry.php?NewInvoice=0">
				<input type="hidden" name="mfee_cust_id" value="'.$custid.'">
				<input type="hidden" name="mfee_amount" value="'.$mgtfee.'">
				<input type="hidden" name="mfee_custody" value="'.$mfees[$a][4].'">
				<input type="hidden" name="mfee_tgl" value="'.$mfees[$a][5].'">
				<input type="hidden" name="mfee_periode" value="'.date('M Y',strtotime(date2sql($_POST['TransToDate']))).'">
				<input type="hidden" name="type" value="mfee">
				<input type="submit" name="invoice" value="Set Invoice">
				</form>
			';

	    	if(@$custid!=''){
	    		//$nos[]=$custid2;
	    		$isi[$custid]=array($mgtfee,$mfees[$a]);

	    	}else{
	    		$nos[]=array($mgtfee,$mfees[$a]);
	    	}
	    	
	    	$no++;
	    }
    }else{
    	echo '<tr><td colspan="5">No Data Available</td></tr>';
    }
    //sort($nos);

$no = 1;
$result = get_customers_search(get_post("customer"));
while ($myrow = db_fetch_assoc($result)) {
	$custid=$myrow["debtor_no"];
	$mfees=$isi[$custid][1];
	if(count($mfees)<1)
		continue;
	$mgtfee=$mfees[0]/1.1;
	$custid2='';
	$pdate='';
	$shrfee=0;
	$custid2=get_customer_ref(@$custid);
	$pdate=get_customer_pdate(@$custid);
	
	if($mfees[7]==1){
		$mgtfee=floor($mgtfee);
		$gross=floor($mfees[0]);
	}else{
		$mgtfee=($mgtfee);
		$gross=($mfees[0]);
	}

	$invoice='<form method="post" action="../../sales/sales_order_entry.php?NewInvoice=0">
		<input type="hidden" name="mfee_cust_id" value="'.$custid.'">
		<input type="hidden" name="mfee_amount" value="'.$mgtfee.'">
		<input type="hidden" name="mfee_custody" value="'.$mfees[4].'">
		<input type="hidden" name="mfee_tgl" value="'.$mfees[5].'">
		<input type="hidden" name="mfee_periode" value="'.date('M Y',strtotime(date2sql($_POST['TransToDate']))).'">
		<input type="hidden" name="type" value="mfee">
		<input type="submit" name="invoice" value="Set Invoice">
		</form>
	';
	
	echo '<tr>';
	label_cell(@$no);
	label_cell((@$custid2!=''?$custid2:$mfees[2]));
	label_cell(@$mfees[2]);
	label_cell(date('d/m/Y',strtotime(@$pdate)));
	//label_cell($mfees[$a][5]);
	label_cell(price_format(($mgtfee)),'align="right"');
	label_cell(price_format(($gross)),'align="right"');
	//label_cell(price_format($shrfee),'align="right"');
	//label_cell('');
	label_cell(date('d M Y',strtotime($mfees[5])));
	if (get_post('Export')) 
		label_cell('');
	else
		label_cell($invoice);
	$no++;
}
for($a=0;$a<count($nos);$a++){
	$mfees=$nos[$a][1];

	if(count($mfees)<1)
		continue;
	$mgtfee=$mfees[0]/1.1;
	$custid2='';
	$shrfee=0;
	
	if($mfees[7]==1){
		$mgtfee=round($mgtfee);
		$gross=round($mfees[0]);
	}else{
		$mgtfee=round($mgtfee);
		$gross=round($mfees[0]);
	}

	$invoice='<form method="post" action="../../sales/sales_order_entry.php?NewInvoice=0">
		<input type="hidden" name="mfee_cust_id" value="'.$custid.'">
		<input type="hidden" name="mfee_amount" value="'.$mgtfee.'">
		<input type="hidden" name="mfee_custody" value="'.$mfees[4].'">
		<input type="hidden" name="mfee_tgl" value="'.$mfees[5].'">
		<input type="hidden" name="mfee_periode" value="'.date('M Y',strtotime(date2sql($_POST['TransToDate']))).'">
		<input type="hidden" name="type" value="mfee">
		<input type="submit" name="invoice" value="Set Invoice">
		</form>
	';

	echo '<tr>';
	label_cell(@$no);
	label_cell((@$custid2!=''?$custid2:$mfees[6].' '.$mfees[2]));
	label_cell(@$mfees[2]);
	label_cell('');
	//label_cell($mfees[$a][5]);
	label_cell(price_format(($mgtfee)),'align="right"');
	label_cell(price_format(($gross)),'align="right"');
	//label_cell(price_format($shrfee),'align="right"');
	//label_cell('');
	label_cell(date('d M Y',strtotime($mfees[5])));
	if (get_post('Export')) 
		label_cell('');
	else
		label_cell($invoice);
	$no++;
}

end_table(1);
}
//----------------------------------------------------------------------------------------------------
if (get_post('Export')) 
{
	
	$header='
	<style>
	.tableheader{
		background-color:#000 !important;
		color:#fff !important;
	}
	.tablestyle{
	}
	td{
		vertical-align:top !important;
	}
	td a{
		display:none !important;
	}
	</style>
	<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#bfbfbf;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;font-size:20px;">Management Fee</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#bfbfbf;">Masa '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#bfbfbf;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 651;">';
	echo show_results();
	echo '</td></tr></table>';
}else{
	gl_inquiry_controls();

	div_start('trans_tbl');

	if (get_post('Show') || get_post('account'))
	    show_results();

	div_end();

	//----------------------------------------------------------------------------------------------------

	end_page();
}

