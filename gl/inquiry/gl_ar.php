<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");


include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/siar.inc");
include_once($path_to_root . "/gl/includes/db/gl_db_banking.inc");

$js = '';
set_focus('account');
if ($SysPrefs->use_popup_windows)
	$js .= get_js_open_window(800, 500);
if (user_use_date_picker())
	$js .= get_js_date_picker();
if (get_post('Export')) 
{
	include_once('head.php');
	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-Disposition: attachment;filename=\"AR Management Fee.xls\"");
	header("Cache-Control: max-age=0");
	
	
}elseif(get_post('PrintPdf')){
	require_once('../../reporting/tcpdf/tcpdf_include.php');
	//create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('KAM');
	$pdf->SetTitle('KAM');
	$pdf->SetSubject('KAM');

	// set default header data
	//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 9);
}else{
page(_($help_context = "AR Management Fee"), false, false, '', $js);
}
//----------------------------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show')) 
{
	$Ajax->activate('trans_tbl');
}



if (isset($_GET["account"]))
	$_POST["account"] = $_GET["account"];
if (isset($_GET["TransFromDate"]))
	$_POST["TransFromDate"] = $_GET["TransFromDate"];
if (isset($_GET["TransToDate"]))
	$_POST["TransToDate"] = $_GET["TransToDate"];
if (isset($_GET["Dimension"]))
	$_POST["Dimension"] = $_GET["Dimension"];
if (isset($_GET["Dimension2"]))
	$_POST["Dimension2"] = $_GET["Dimension2"];
if (isset($_GET["amount_min"]))
	$_POST["amount_min"] = $_GET["amount_min"];
if (isset($_GET["amount_max"]))
	$_POST["amount_max"] = $_GET["amount_max"];

if (!isset($_POST["amount_min"]))
	$_POST["amount_min"] = price_format(0);
if (!isset($_POST["amount_max"]))
	$_POST["amount_max"] = price_format(0);

//----------------------------------------------------------------------------------------------------

	if(@$_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
		$tgl1=$_POST['TransFromDate'];
		$tgl2=$_POST['TransToDate'];		
	}else{
		$blnfrom=@$_POST['bulanfrom'];
		$blnto=@$_POST['bulanto'];
		$thnfrom=date('Y');
		$thnto=date('Y');
		$tgl1='01/'.$blnfrom.'/'.$thnfrom;
		$tgl2='31/'.$blnto.'/'.$thnto;		
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
function gl_inquiry_controls()
{
	$dim = get_company_pref('use_dimension');
    start_form();

    start_table(TABLESTYLE_NOBORDER);
    echo '<tr>';
	echo "<td>"._("Dated From").":</td>\n";
	echo "<td>";
	
	$bulan = array(
	_(""),_("January"),_("February"),_("March"),_("April"),_("Mei"),_("June"),_("July"),_("Augustus"),_("September"),_("Oktober"),_("November"),_("Desember"));

	//echo array_selector('bulanfrom', 'onchange="selectblnfrom(this.value)', $bulan);
	echo '<select name="bulanfrom" id="bulanfrom" onchange="pilblnfrom(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	echo "<td>"._("To").":</td>\n";
	echo "<td>";
	echo '<select name="bulanto" id="bulanto" onchange="pilblnto(this.value)">';
	for($t=0;$t<=12;$t++){
		//if($t!=date('Y'))
		echo '<option value="'.$t.'">'.$bulan[$t].'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	//echo array_selector('bulanto', null, $bulan);
	echo "</td>\n";
	echo "<td>"._("Year").":</td>\n";
	echo "<td>";
	echo '<select name="tahunfrom" id="tahunfrom" onchange="pilthn(this.value)">';
	for($t=date('Y');$t>2000;$t--){
		if($t==date('Y'))
		echo '<option value="'.$t.'" selected>'.$t.'</option>';
		else
		echo '<option value="'.$t.'">'.$t.'</option>';
	}
	echo '</select>&nbsp;&nbsp;';
	echo "</td>\n";
	start_row();
   // gl_all_accounts_list_cells(_("Account:"), 'account', null, false, false, _("All Accounts"));
	//, -user_transaction_days()
	date_cells(_("&nbsp;"), 'TransFromDate', '', null);
	date_cells(_("&nbsp;"), 'TransToDate', '', null);
    end_row();
	end_table();

	start_table(TABLESTYLE_NOBORDER);
	start_row();

	echo '<td colspan="4">&nbsp;</td>';
	submit_cells('Show',_("Show"),'align="right"','', 'default');
	echo '<td><input type="submit" class="export" name="Export" id="Export" Value="Export to Excel">
	</td>';
	echo '
	<style>
		.export{
			align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		}
	</style>
	<script src="../../jquery.min.js"></script>
	<script src="../../jquery.table2excel.js"></script>

	<script>
	function pilblnfrom(bln){
	 		//console.log(bln);
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransFromDate");
	 		var tgl="01/"+bln+"/"+thn;
	 		a.value=tgl;
	 		//console.log(a.value);

	 	}
	 	function pilblnto(bln){
	 		if(bln<10)
	 			bln="0"+bln;
	 		var e = document.getElementById("tahunfrom");
			var thn = e.options[e.selectedIndex].value;
	 		var a = document.getElementById("TransToDate");
	 		var lastDateofTheMonth = new Date(thn, bln, 0).getDate();
	 		var tgl=lastDateofTheMonth+"/"+bln+"/"+thn;
	 		a.value=tgl;
	 	}
	 	function pilthn(thn){

	 		var a = document.getElementById("TransFromDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanfrom");
			e.options[parseInt(tgls[1])].selected = true;

	 		var a = document.getElementById("TransToDate");
	 		var tgls=a.value.split("/");
	 		var tgl=tgls[0]+"/"+tgls[1]+"/"+thn;
	 		a.value=tgl;	 		
	 		var e = document.getElementById("bulanto");
			e.options[parseInt(tgls[1])].selected = true;

	 	}
		function xls(){
			//console.log("aa");
			$(".tablestyle2").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "Management_Fee",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
	</script>';
	//submit_cells('Export Excel',_("Export"),'','', 'default');
	end_row();
	end_table();

	echo '<hr>';
    end_form();
}

//----------------------------------------------------------------------------------------------------

function format_npwp($npwp){
	$npwp0=str_replace('.','',$npwp);
	$npwp0=str_replace('-','',$npwp0);

	$npwp1=substr($npwp0,0,2);
	$npwp2=substr($npwp0,5,3);
	$npwp3=substr($npwp0,8,1);
	$npwp4=substr($npwp0,8,1);
	$npwp5=substr($npwp0,9,3);
	$npwp6=substr($npwp0,12,3);
	if(@$npwp!='')
	return $npwp1.'.'.$npwp2.'.'.$npwp3.'.'.$npwp4.'-'.$npwp5.'.'.$npwp6;

}
function show_results()
{
	global $path_to_root, $systypes_array;

	/*
	start_table(TABLESTYLE2, "width='90%'", 10);
	
	$th = array(_("No"), _("Kode"), _("NPWP"),  _("Nama"));
	    			
	table_header($th);
	*/

	if($_POST['bulanfrom']=='0'){
		$blnfrom=date('m',strtotime(date2sql($_POST['TransFromDate'])));
		$blnto=date('m',strtotime(date2sql($_POST['TransToDate'])));
		$thnfrom=date('Y',strtotime(date2sql($_POST['TransFromDate'])));
		$thnto=date('Y',strtotime(date2sql($_POST['TransToDate'])));
		$tgl1=$_POST['TransFromDate'];
		$tgl2=$_POST['TransToDate'];		
	}else{
		$blnfrom=$_POST['bulanfrom'];
		$blnto=$_POST['bulanto'];
		$thnfrom=@$_POST['tahunfrom'];
		$thnto=@$_POST['tahunfrom'];
		$tgl1='01/'.$blnfrom.'/'.$thnfrom;
		$tgl2=cal_days_in_month(CAL_GREGORIAN, $blnto, $thnto).'/'.$blnto.'/'.$thnto;		
	}
	$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01'));
	if($thnfrom==$thnto){
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnto.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}else{
		if($blnfrom!=$blnto){		
			$blnnya=date('F Y',strtotime($thnfrom.'-'.$blnfrom.'-01')).' - '.date('F Y',strtotime($thnto.'-'.$blnto.'-01'));
		}
	}
	$tableheader='<center><table class="tablestyle" cellpadding="6" border="1" width="98%">';
	$tableheader.='<tr class="inquirybg" style="font-weight:bold">';
	//<td class='tableheader' align='center'>" . _("Kode") . "</td>
	$tableheader .=  "
	<td class='tableheader' align='center'>" . _("No") . "</td>
	<td class='tableheader' align='center'>" . _("NPWP") . "</td>
	<td class='tableheader' align='center'>" . _("Nama") . "</td>
	<td class='tableheader' align='center'>" . _("Alamat") . "</td>
	<td class='tableheader' align='center'>" . _("Referensi") . "</td>
	<td class='tableheader' align='center'>" . _("AR") . "</td>
	<td class='tableheader' align='center'>" . _("Jenis") . "</td>
	<td class='tableheader' align='center'>" . _("Masa") . "</td>
	<td class='tableheader' align='center'>" . _("Bruto") . "</td>
	<td class='tableheader' align='center'>" . _("DPP") . "</td>
	<td class='tableheader' align='center'>" . _("PPN") . "</td>
	<td class='tableheader' align='center'>" . _("PPH 23") . "</td>
	<td class='tableheader' align='center'>" . _("Jumlah") . "</td>
	<td class='tableheader' align='center'>" . _("Pembayaran") . "</td>
	<td class='tableheader' align='center'>" . _("Selisih") . "</td>
	</tr>
	";
	$tableheader.='</tr>';
	//$mfees=get_mgtfee(date2sql($_POST['TransFromDate']),date2sql($_POST['TransToDate']));
	//print_r($mfees);
	//echo $tgl1.'-'.$_POST['bulanfrom'];

	//$sql = get_sql_for_armfee_inquiry(-1, $tgl1,$tgl2);
	$sql="select a.*,d.*,f.id
from 0_gl_trans a
left join 0_voided f on a.type=f.type and a.type_no=f.id
left join 0_debtors_master d on a.person_id=d.debtor_no
where 
a.account like '21240' and f.id IS NULL and a.amount<0
and a.tran_date between '".date2sql($_POST['TransFromDate'])."' and '".date2sql($_POST['TransToDate'])."' 
order by a.counter asc";
	//and f.id IS NULL
	//$sql="select trans_type,trans_no,sum(net_amount) as dpp,sum(amount) as amount, memo from 0_trans_tax_details where tax_type_id=4 and tran_date like '".@$tglan[2]."-".@$tglan[1]."%' group by trans_type,trans_no order by id asc";
	//$query=db_query($sql, "The transactions for could not be retrieved");
	$result=db_query($sql, '');
	//return true;
	//$mfees = db_fetch_assoc($result);
	//if(count($mfees)>0){
		$no=1;$a=0;
		$ttljml=0;$ttlbayar=0;$ttlselisih=0;
		$ttldpp=0;
		$ttlppn=0;
		$ttlpph=0;
		while($mfees = db_fetch_assoc($result))
		{
			if(@$mfees['type']!=0)
				continue;
			$sql2 = "SELECT supp.*
			FROM ".TB_PREF."debtor_trans trans, ".TB_PREF."debtors_master supp
			WHERE trans_no=".db_escape(@$mfees['type_no'])." AND trans.debtor_no=supp.debtor_no ";
			$result2 = db_query($sql2,"The next transaction number for  could not be retrieved");
		    $person = db_fetch($result2);
			$ar=@$person['name'];
			$npwp=@$person['tax_id'];
			$alamat=@$person['address'];

			$memo=$mfees['memo_'];
			if(@$memo==''){
				$memos=get_gl_memo_mfee($mfees['type_no'], $mfees['type']);
				$memo=($memos[1]);
			}
			//print_r($descs);
			//echo '<br>';

			if(($a % 2) == 1)
			$tableheader.='<tr class="evenrow">';
			else
			$tableheader.='<tr class="oddrow">';
			//print_r($mfees);echo '<br>';
			$rom=array('I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII');
			$person=get_counterparty_name(@$mfees['type'],@$mfees['type_no']);
			$persons=explode('//',@$myrow0['person_id']);
			$bln=(intval(date('m',strtotime($mfees['tran_date'])))-1);
			$ar=@$mfees['debtor_ref'];
			
			$alamat='';
			$npwp='';
			$nama='';

			if($ar!=''){
				$noinv=@$mfees['debtor_ref'].'/AFT-SC-M'.$rom[$bln].'/'.date('Y',strtotime($mfees['tran_date']));
				//$noinv=@$mfees['reference'];
				$kode=explode('/',$noinv);
				$kode2=explode('/',$noinv);
				$alamat=@$mfees['address2'];
				$npwp=@$mfees['tax_id'];
				$nama=@$mfees['name'];
				$curr=@$mfees['curr_code'];
			}else{
				$descs=explode(' ',str_replace('- ', '', @$memo));
				$kodep=$descs[3];

				if(($kodep=='PT.' or $kodep=='PT'))
					$kodep=$descs[4].'%'.$descs[5];
				if($kodep=='SCPF' or $kodep=='PSCPF')
					$kodep=$descs[3].'%'.$descs[4];
				if($kodep=='DP')
					$kodep=$descs[3].'%'.$descs[4];
				if($kodep==''){
					$kodep=$descs[4];
					//echo '<br>';
					//print_r($descs);
					if(($kodep=='PT.' or $kodep=='PT'))
						$kodep=$descs[4].'%'.$descs[5];
					if($kodep=='DP')
						$kodep=$descs[4].'%'.$descs[5];
					if($kodep=='DP%RNI')
						$kodep='KPD-RNI-SC';
				}
				$sql2="select * 
				from 0_debtors_master a 
				where a.name like '%".trim($kodep)."%' or a.debtor_ref like '%".trim($kodep)."%' ";
				$query2=db_query($sql2, "The transactions for could not be retrieved");
				//$kd=$sql2;
				while ($myrow2 = db_fetch($query2))
				{
					if(@$myrow2['debtor_ref']!=''){
						$alamat=@$myrow2['address2'];
						$npwp=@$myrow2['tax_id'];
						$nama=@$myrow2['name'];
						$curr=@$myrow2['curr_code'];
						$kodep=@$myrow2['debtor_ref'];
					}else{

						$alamat='';
						$npwp='';
						$nama='';
						$curr='';
						$kodep='';
					}
					$ar=$kodep;
				}
				$kodep=str_replace('%', '', @$kodep);
				$noinv=@$kodep.'/AFT-SC-M'.$rom[$bln].'/'.date('Y',strtotime($mfees['tran_date']));
				
			}
			$jenis='Mfee';
			if(@$curr!='IDR' and @$curr!='')
			$jenis=@$curr;
			$masa=date('d M Y',strtotime($mfees['tran_date']));
			$ppn=abs(@$mfees['amount']);
			$dpp=abs(@$mfees['amount']*10);
			//$dpp=$bruto/1.1;
			$pph=abs(@$dpp*2/100);
			$bruto=abs(@$mfees['amount']);
			
			$jumlah=$dpp+$ppn-$pph;

			$bayar=0;
			$sql="select b.amount 
			from 0_cust_allocations a 
			inner join 0_gl_trans b on a.trans_no_from=b.type_no and a.trans_type_from=b.type and b.amount>0 
			where a.trans_no_to='".$mfees['type_no']."' ";
			$query=db_query($sql, "The transactions for could not be retrieved");
			while ($myrow0 = db_fetch($query))
			{
				$bayar=$myrow0['amount'];
			}
			$selisih=0;
			if($bayar>0)
			$selisih=$bayar-$bruto;

			//<td align='center'>" .$kode[0]. "</td>
			$tableheader .="
				<td align='center'>" .$no.@$kd. "</td>
				<td align='center'>" .format_npwp(@$npwp) . "</td>
				<td align='center'>" . @$memo . "</td>
				<td align='center'>" . @$alamat . "</td>
				<td align='center'>" . $noinv . "</td>
				<td align='center'>" . $ar . "</td>
				<td align='center'>" . $jenis . "</td>
				<td align='center'>" . $masa . "</td>
				<td align='right'>" . price_format(abs($bruto)) . "</td>
				<td align='right'>" .price_format(($dpp)) . "</td>
				<td align='right'>" . price_format($ppn) . "</td>
				<td align='right'>" . price_format_pr($pph) . "</td>
				<td align='right'>" . price_format($jumlah) . "</td>
				<td align='right'>" . price_format($bayar) . "</td>
				<td align='right'>" . price_format_pr($selisih) . "</td>
				</tr>";
			$ttljml=$ttljml+$jumlah;
			$ttlbayar=$ttlbayar+$bayar;
			$ttlselisih=$ttlselisih+$selisih;
			$ttldpp=$ttldpp+$dpp;
			$ttlppn=$ttlppn+$ppn;
			$ttlpph=$ttlpph+$pph;
			$no++;
			$a++;
	    }
	    if($a>0){
			$tableheader .="
				<td align='right' colspan='10'><b>Jumlah</b> </td>
				<td align='right'><b>" . price_format($ttlppn) . "</b></td>
				<td align='right'><b>" . price_format($ttlpph) . "</b></td>
				<td align='right'><b>" . price_format($ttljml) . "</b></td>
				<td align='right'><b>" . price_format($ttlbayar) . "</b></td>
				<td align='right'><b>" . price_format($ttlselisih) . "</b></td>
				</tr>";
    }else{
    	$tableheader .= '<tr><td colspan="16" align="center"><b>No Data Available</b></td></tr>';
    }
    return $tableheader;
}
//----------------------------------------------------------------------------------------------------

if (get_post('Export')) 
{
	
	$header='<table border="0">
		<tr>
			<td width="100%" align="center"  style="background-color:#8072a9;font-size:12px;"><b>PT. Kresna Asset Management</b></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:11px;"><i>-</i></td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:11px;"><i>Jl. Widya Chandra V</i></td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		<tr>
			<td align="center" style="background-color:#8072a9;font-size:20px;">AR Management Fee</td>
		</tr>
		<tr>
			<td align="center" style="background-color:#8072a9;">Periode '.@$blnnya.'</td>
		</tr>
		<tr><td align="center" style="background-color:#8072a9;">&nbsp;</td></tr>
		
	';
	echo $header;
	echo '<tr><td style="width: 651;">';
	echo show_results();
	echo '</td></tr></table>';
}else{
	gl_inquiry_controls();

	div_start('trans_tbl');

		if (get_post('Show')) {
			echo '<center><table border="0" cellpadding="4" width="98%">
			<tr>		
				<td align="center" style="font-weight:bold;">AR Management Fee</td>
			</tr>
			<tr>
				<td align="center" style="font-weight:bold;">Periode '.@$blnnya.'</td>
			</tr>';
			echo '<tr>		
				<td align="center">';
			echo show_results();
			echo '</td></tr></table></center>';
		}
	   // show_results();

	div_end();

	//----------------------------------------------------------------------------------------------------

	end_page();
	}

