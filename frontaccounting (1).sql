-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2018 at 10:16 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `frontaccounting`
--

-- --------------------------------------------------------

--
-- Table structure for table `0_areas`
--

CREATE TABLE `0_areas` (
  `area_code` int(11) NOT NULL,
  `description` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_areas`
--

INSERT INTO `0_areas` (`area_code`, `description`, `inactive`) VALUES
(1, 'Jakarta', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_attachments`
--

CREATE TABLE `0_attachments` (
  `id` int(11) UNSIGNED NOT NULL,
  `description` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type_no` int(11) NOT NULL DEFAULT '0',
  `trans_no` int(11) NOT NULL DEFAULT '0',
  `unique_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `filename` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `filesize` int(11) NOT NULL DEFAULT '0',
  `filetype` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_audit_trail`
--

CREATE TABLE `0_audit_trail` (
  `id` int(11) NOT NULL,
  `type` smallint(6) UNSIGNED NOT NULL DEFAULT '0',
  `trans_no` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `user` smallint(6) UNSIGNED NOT NULL DEFAULT '0',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fiscal_year` int(11) NOT NULL DEFAULT '0',
  `gl_date` date NOT NULL DEFAULT '0000-00-00',
  `gl_seq` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_bank_accounts`
--

CREATE TABLE `0_bank_accounts` (
  `account_code` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `account_type` smallint(6) NOT NULL DEFAULT '0',
  `bank_account_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bank_account_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bank_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bank_address` tinytext COLLATE utf8_unicode_ci,
  `bank_curr_code` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dflt_curr_act` tinyint(1) NOT NULL DEFAULT '0',
  `id` smallint(6) NOT NULL,
  `bank_charge_act` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_reconciled_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ending_reconcile_balance` double NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `kode` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_bank_trans`
--

CREATE TABLE `0_bank_trans` (
  `id` int(11) NOT NULL,
  `type` smallint(6) DEFAULT NULL,
  `trans_no` int(11) DEFAULT NULL,
  `bank_act` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ref` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trans_date` date NOT NULL DEFAULT '0000-00-00',
  `amount` double DEFAULT NULL,
  `dimension_id` int(11) NOT NULL DEFAULT '0',
  `dimension2_id` int(11) NOT NULL DEFAULT '0',
  `person_type_id` int(11) NOT NULL DEFAULT '0',
  `person_id` tinyblob,
  `reconciled` date DEFAULT NULL,
  `ref2` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_bom`
--

CREATE TABLE `0_bom` (
  `id` int(11) NOT NULL,
  `parent` char(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `component` char(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `workcentre_added` int(11) NOT NULL DEFAULT '0',
  `loc_code` char(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `quantity` double NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_budget_trans`
--

CREATE TABLE `0_budget_trans` (
  `id` int(11) NOT NULL,
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `amount` double NOT NULL DEFAULT '0',
  `dimension_id` int(11) DEFAULT '0',
  `dimension2_id` int(11) DEFAULT '0',
  `memo_` tinytext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_chart_class`
--

CREATE TABLE `0_chart_class` (
  `cid` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `class_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ctype` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_chart_class`
--

INSERT INTO `0_chart_class` (`cid`, `class_name`, `ctype`, `inactive`) VALUES
('1', 'ASET', 1, 0),
('2', 'LIABILITAS', 2, 0),
('3', 'EQUITY', 3, 0),
('4', 'PENDAPATAN USAHA', 4, 0),
('5', 'COST OF SALES', 5, 0),
('6', 'BIAYA OPERASIONAL', 6, 0),
('8', 'OTHER INCOME', 4, 0),
('9', 'OTHER EXPENSES', 6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_chart_master`
--

CREATE TABLE `0_chart_master` (
  `account_code` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `account_code2` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `account_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `account_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_chart_types`
--

CREATE TABLE `0_chart_types` (
  `id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `class_id` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-1',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_comments`
--

CREATE TABLE `0_comments` (
  `type` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL DEFAULT '0',
  `date_` date DEFAULT '0000-00-00',
  `memo_` tinytext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_credit_status`
--

CREATE TABLE `0_credit_status` (
  `id` int(11) NOT NULL,
  `reason_description` char(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dissallow_invoices` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_credit_status`
--

INSERT INTO `0_credit_status` (`id`, `reason_description`, `dissallow_invoices`, `inactive`) VALUES
(1, 'Good History', 0, 0),
(3, 'No more work until payment received', 1, 0),
(4, 'In liquidation', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_crm_categories`
--

CREATE TABLE `0_crm_categories` (
  `id` int(11) NOT NULL COMMENT 'pure technical key',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `description` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `system` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'nonzero for core system usage',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_crm_categories`
--

INSERT INTO `0_crm_categories` (`id`, `type`, `action`, `name`, `description`, `system`, `inactive`) VALUES
(1, 'cust_branch', 'general', 'General', 'General contact data for customer branch (overrides company setting)', 1, 0),
(2, 'cust_branch', 'invoice', 'Invoices', 'Invoice posting (overrides company setting)', 1, 0),
(3, 'cust_branch', 'order', 'Orders', 'Order confirmation (overrides company setting)', 1, 0),
(4, 'cust_branch', 'delivery', 'Deliveries', 'Delivery coordination (overrides company setting)', 1, 0),
(5, 'customer', 'general', 'General', 'General contact data for customer', 1, 0),
(6, 'customer', 'order', 'Orders', 'Order confirmation', 1, 0),
(7, 'customer', 'delivery', 'Deliveries', 'Delivery coordination', 1, 0),
(8, 'customer', 'invoice', 'Invoices', 'Invoice posting', 1, 0),
(9, 'supplier', 'general', 'General', 'General contact data for supplier', 1, 0),
(10, 'supplier', 'order', 'Orders', 'Order confirmation', 1, 0),
(11, 'supplier', 'delivery', 'Deliveries', 'Delivery coordination', 1, 0),
(12, 'supplier', 'invoice', 'Invoices', 'Invoice posting', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_crm_contacts`
--

CREATE TABLE `0_crm_contacts` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL DEFAULT '0' COMMENT 'foreign key to crm_contacts',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `entity_id` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_crm_contacts`
--

INSERT INTO `0_crm_contacts` (`id`, `person_id`, `type`, `action`, `entity_id`) VALUES
(1, 1, 'cust_branch', 'general', '1'),
(2, 1, 'customer', 'general', '1'),
(3, 2, 'cust_branch', 'general', '2'),
(4, 2, 'customer', 'general', '2'),
(5, 3, 'cust_branch', 'general', '3'),
(6, 3, 'customer', 'general', '3'),
(7, 4, 'cust_branch', 'general', '4'),
(8, 4, 'customer', 'general', '4'),
(9, 5, 'cust_branch', 'general', '5'),
(10, 5, 'customer', 'general', '5'),
(11, 6, 'cust_branch', 'general', '6'),
(12, 6, 'customer', 'general', '6'),
(13, 7, 'cust_branch', 'general', '7'),
(14, 7, 'customer', 'general', '7'),
(15, 8, 'cust_branch', 'general', '8'),
(16, 8, 'customer', 'general', '8'),
(17, 9, 'cust_branch', 'general', '9'),
(18, 9, 'customer', 'general', '9'),
(19, 10, 'cust_branch', 'general', '10'),
(20, 10, 'customer', 'general', '10'),
(21, 11, 'cust_branch', 'general', '11'),
(22, 11, 'customer', 'general', '11'),
(23, 12, 'cust_branch', 'general', '12'),
(24, 12, 'customer', 'general', '12'),
(25, 13, 'cust_branch', 'general', '13'),
(26, 13, 'customer', 'general', '13'),
(27, 14, 'cust_branch', 'general', '14'),
(28, 14, 'customer', 'general', '14'),
(29, 15, 'cust_branch', 'general', '15'),
(30, 15, 'customer', 'general', '15'),
(31, 16, 'cust_branch', 'general', '16'),
(32, 16, 'customer', 'general', '16'),
(33, 17, 'cust_branch', 'general', '17'),
(34, 17, 'customer', 'general', '17'),
(35, 18, 'cust_branch', 'general', '18'),
(36, 18, 'customer', 'general', '18'),
(37, 19, 'cust_branch', 'general', '19'),
(38, 19, 'customer', 'general', '19'),
(39, 20, 'cust_branch', 'general', '20'),
(40, 20, 'customer', 'general', '20'),
(41, 21, 'cust_branch', 'general', '21'),
(42, 21, 'customer', 'general', '21'),
(43, 22, 'cust_branch', 'general', '22'),
(44, 22, 'customer', 'general', '22'),
(45, 23, 'cust_branch', 'general', '23'),
(46, 23, 'customer', 'general', '23'),
(47, 24, 'cust_branch', 'general', '24'),
(48, 24, 'customer', 'general', '24'),
(49, 25, 'cust_branch', 'general', '25'),
(50, 25, 'customer', 'general', '25'),
(51, 26, 'cust_branch', 'general', '26'),
(52, 26, 'customer', 'general', '26'),
(53, 27, 'cust_branch', 'general', '27'),
(54, 27, 'customer', 'general', '27'),
(55, 28, 'cust_branch', 'general', '28'),
(56, 28, 'customer', 'general', '28'),
(57, 29, 'cust_branch', 'general', '29'),
(58, 29, 'customer', 'general', '29'),
(59, 30, 'cust_branch', 'general', '30'),
(60, 30, 'customer', 'general', '30'),
(61, 31, 'cust_branch', 'general', '31'),
(62, 31, 'customer', 'general', '31'),
(63, 32, 'cust_branch', 'general', '32'),
(64, 32, 'customer', 'general', '32'),
(65, 33, 'cust_branch', 'general', '33'),
(66, 33, 'customer', 'general', '33'),
(67, 34, 'cust_branch', 'general', '34'),
(68, 34, 'customer', 'general', '34'),
(69, 36, 'cust_branch', 'general', '36'),
(70, 36, 'customer', 'general', '36'),
(71, 37, 'cust_branch', 'general', '37'),
(72, 37, 'customer', 'general', '37'),
(73, 38, 'cust_branch', 'general', '38'),
(74, 38, 'customer', 'general', '38'),
(75, 39, 'cust_branch', 'general', '39'),
(76, 39, 'customer', 'general', '39'),
(77, 40, 'cust_branch', 'general', '40'),
(78, 40, 'customer', 'general', '40'),
(79, 41, 'cust_branch', 'general', '41'),
(80, 41, 'customer', 'general', '41'),
(81, 42, 'cust_branch', 'general', '42'),
(82, 42, 'customer', 'general', '42'),
(83, 43, 'cust_branch', 'general', '43'),
(84, 43, 'customer', 'general', '43'),
(85, 44, 'cust_branch', 'general', '44'),
(86, 44, 'customer', 'general', '44'),
(87, 45, 'cust_branch', 'general', '45'),
(88, 45, 'customer', 'general', '45'),
(89, 46, 'cust_branch', 'general', '46'),
(90, 46, 'customer', 'general', '46'),
(91, 47, 'cust_branch', 'general', '47'),
(92, 47, 'customer', 'general', '47'),
(93, 48, 'cust_branch', 'general', '48'),
(94, 48, 'customer', 'general', '48'),
(95, 49, 'cust_branch', 'general', '49'),
(96, 49, 'customer', 'general', '49'),
(97, 50, 'cust_branch', 'general', '50'),
(98, 50, 'customer', 'general', '50'),
(99, 51, 'cust_branch', 'general', '51'),
(100, 51, 'customer', 'general', '51'),
(101, 52, 'cust_branch', 'general', '52'),
(102, 52, 'customer', 'general', '52'),
(103, 53, 'cust_branch', 'general', '53'),
(104, 53, 'customer', 'general', '53'),
(105, 54, 'cust_branch', 'general', '54'),
(106, 54, 'customer', 'general', '54'),
(107, 55, 'cust_branch', 'general', '55'),
(108, 55, 'customer', 'general', '55'),
(109, 56, 'cust_branch', 'general', '56'),
(110, 56, 'customer', 'general', '56'),
(111, 58, 'cust_branch', 'general', '58'),
(112, 58, 'customer', 'general', '58'),
(113, 59, 'cust_branch', 'general', '59'),
(114, 59, 'customer', 'general', '59'),
(115, 60, 'cust_branch', 'general', '60'),
(116, 60, 'customer', 'general', '60'),
(117, 61, 'cust_branch', 'general', '61'),
(118, 61, 'customer', 'general', '61'),
(119, 62, 'cust_branch', 'general', '62'),
(120, 62, 'customer', 'general', '62'),
(121, 63, 'cust_branch', 'general', '63'),
(122, 63, 'customer', 'general', '63'),
(123, 64, 'supplier', 'general', '42'),
(124, 65, 'cust_branch', 'general', '64'),
(125, 65, 'customer', 'general', '64'),
(126, 66, 'supplier', 'general', '44'),
(127, 67, 'supplier', 'general', '0'),
(128, 68, 'supplier', 'general', '0'),
(129, 69, 'supplier', 'general', '47'),
(130, 70, 'supplier', 'general', '48'),
(131, 71, 'supplier', 'general', '49'),
(132, 72, 'supplier', 'general', '50'),
(133, 73, 'supplier', 'general', '51'),
(134, 74, 'supplier', 'general', '52'),
(135, 75, 'supplier', 'general', '53'),
(136, 76, 'supplier', 'general', '54'),
(137, 77, 'supplier', 'general', '55'),
(138, 78, 'supplier', 'general', '56'),
(139, 79, 'supplier', 'general', '57'),
(140, 80, 'cust_branch', 'general', '75'),
(141, 80, 'customer', 'general', '104'),
(142, 81, 'cust_branch', 'general', '76'),
(143, 81, 'customer', 'general', '105'),
(144, 82, 'cust_branch', 'general', '77'),
(145, 82, 'customer', 'general', '106'),
(146, 83, 'cust_branch', 'general', '78'),
(147, 83, 'customer', 'general', '107'),
(148, 84, 'cust_branch', 'general', '79'),
(149, 84, 'customer', 'general', '108');

-- --------------------------------------------------------

--
-- Table structure for table `0_crm_persons`
--

CREATE TABLE `0_crm_persons` (
  `id` int(11) NOT NULL,
  `ref` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `name2` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` tinytext COLLATE utf8_unicode_ci,
  `phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone2` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_currencies`
--

CREATE TABLE `0_currencies` (
  `currency` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `curr_abrev` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `curr_symbol` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `hundreds_name` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `auto_update` tinyint(1) NOT NULL DEFAULT '1',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_currencies`
--

INSERT INTO `0_currencies` (`currency`, `curr_abrev`, `curr_symbol`, `country`, `hundreds_name`, `auto_update`, `inactive`) VALUES
('Euro', 'EUR', '?', 'Europe', 'Cents', 1, 0),
('Rupiah', 'IDR', 'Rp', 'Indonesia', 'Sen', 1, 0),
('Ringgit Malaysia', 'MYR', 'RM', 'Malaysia', 'Sen', 1, 0),
('US Dollars', 'USD', '$', 'United States', 'Cents', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_cust_allocations`
--

CREATE TABLE `0_cust_allocations` (
  `id` int(11) NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  `amt` double UNSIGNED DEFAULT NULL,
  `date_alloc` date NOT NULL DEFAULT '0000-00-00',
  `trans_no_from` int(11) DEFAULT NULL,
  `trans_type_from` int(11) DEFAULT NULL,
  `trans_no_to` int(11) DEFAULT NULL,
  `trans_type_to` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_cust_branch`
--

CREATE TABLE `0_cust_branch` (
  `branch_code` int(11) NOT NULL,
  `debtor_no` int(11) NOT NULL DEFAULT '0',
  `br_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `branch_ref` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `br_address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `area` int(11) DEFAULT NULL,
  `salesman` int(11) NOT NULL DEFAULT '0',
  `default_location` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_group_id` int(11) DEFAULT NULL,
  `sales_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sales_discount_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `receivables_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payment_discount_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `default_ship_via` int(11) NOT NULL DEFAULT '1',
  `br_post_address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `group_no` int(11) NOT NULL DEFAULT '0',
  `notes` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `bank_account` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_debtors_master`
--

CREATE TABLE `0_debtors_master` (
  `debtor_no` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `debtor_ref` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address` tinytext COLLATE utf8_unicode_ci,
  `address2` tinytext COLLATE utf8_unicode_ci,
  `tax_id` varchar(55) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `curr_code` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sales_type` int(11) NOT NULL DEFAULT '1',
  `dimension_id` int(11) NOT NULL DEFAULT '0',
  `dimension2_id` int(11) NOT NULL DEFAULT '0',
  `credit_status` int(11) NOT NULL DEFAULT '0',
  `payment_terms` int(11) DEFAULT NULL,
  `discount` double NOT NULL DEFAULT '0',
  `pymt_discount` double NOT NULL DEFAULT '0',
  `credit_limit` float NOT NULL DEFAULT '1000',
  `notes` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `kode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_debtor_trans`
--

CREATE TABLE `0_debtor_trans` (
  `trans_no` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `type` smallint(6) UNSIGNED NOT NULL DEFAULT '0',
  `version` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `debtor_no` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `branch_code` int(11) NOT NULL DEFAULT '-1',
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `due_date` date NOT NULL DEFAULT '0000-00-00',
  `reference` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tpe` int(11) NOT NULL DEFAULT '0',
  `order_` int(11) NOT NULL DEFAULT '0',
  `ov_amount` double NOT NULL DEFAULT '0',
  `ov_gst` double NOT NULL DEFAULT '0',
  `ov_freight` double NOT NULL DEFAULT '0',
  `ov_freight_tax` double NOT NULL DEFAULT '0',
  `ov_discount` double NOT NULL DEFAULT '0',
  `alloc` double NOT NULL DEFAULT '0',
  `prep_amount` double NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '1',
  `ship_via` int(11) DEFAULT NULL,
  `dimension_id` int(11) NOT NULL DEFAULT '0',
  `dimension2_id` int(11) NOT NULL DEFAULT '0',
  `payment_terms` int(11) DEFAULT NULL,
  `tax_included` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_debtor_trans_details`
--

CREATE TABLE `0_debtor_trans_details` (
  `id` int(11) NOT NULL,
  `debtor_trans_no` int(11) DEFAULT NULL,
  `debtor_trans_type` int(11) DEFAULT NULL,
  `stock_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` tinytext COLLATE utf8_unicode_ci,
  `unit_price` double NOT NULL DEFAULT '0',
  `unit_tax` double NOT NULL DEFAULT '0',
  `quantity` double NOT NULL DEFAULT '0',
  `discount_percent` double NOT NULL DEFAULT '0',
  `standard_cost` double NOT NULL DEFAULT '0',
  `qty_done` double NOT NULL DEFAULT '0',
  `src_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_dimensions`
--

CREATE TABLE `0_dimensions` (
  `id` int(11) NOT NULL,
  `reference` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type_` tinyint(1) NOT NULL DEFAULT '1',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `date_` date NOT NULL DEFAULT '0000-00-00',
  `due_date` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_dimension_link`
--

CREATE TABLE `0_dimension_link` (
  `id` int(11) NOT NULL,
  `clientid` varchar(32) DEFAULT NULL,
  `agentid` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `0_exchange_rates`
--

CREATE TABLE `0_exchange_rates` (
  `id` int(11) NOT NULL,
  `curr_code` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rate_buy` double NOT NULL DEFAULT '0',
  `rate_sell` double NOT NULL DEFAULT '0',
  `date_` date NOT NULL DEFAULT '0000-00-00',
  `tax_rate` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_fiscal_year`
--

CREATE TABLE `0_fiscal_year` (
  `id` int(11) NOT NULL,
  `begin` date DEFAULT '0000-00-00',
  `end` date DEFAULT '0000-00-00',
  `closed` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_fiscal_year`
--

INSERT INTO `0_fiscal_year` (`id`, `begin`, `end`, `closed`) VALUES
(1, '2016-01-01', '2016-12-31', 1),
(2, '2017-01-01', '2017-12-31', 0),
(3, '2018-01-01', '2018-12-31', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_fp_trans`
--

CREATE TABLE `0_fp_trans` (
  `id` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `reference` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_gl_trans`
--

CREATE TABLE `0_gl_trans` (
  `counter` int(11) NOT NULL,
  `type` smallint(6) NOT NULL DEFAULT '0',
  `type_no` int(11) NOT NULL DEFAULT '0',
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `memo_` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `dimension_id` int(11) NOT NULL DEFAULT '0',
  `dimension2_id` int(11) NOT NULL DEFAULT '0',
  `person_type_id` int(11) DEFAULT NULL,
  `person_id` tinyblob,
  `posting` tinyint(1) NOT NULL,
  `jasa` int(11) DEFAULT NULL,
  `gross` double DEFAULT NULL,
  `rate` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_grn_batch`
--

CREATE TABLE `0_grn_batch` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `purch_order_no` int(11) DEFAULT NULL,
  `reference` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `delivery_date` date NOT NULL DEFAULT '0000-00-00',
  `loc_code` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rate` double DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_grn_items`
--

CREATE TABLE `0_grn_items` (
  `id` int(11) NOT NULL,
  `grn_batch_id` int(11) DEFAULT NULL,
  `po_detail_item` int(11) NOT NULL DEFAULT '0',
  `item_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` tinytext COLLATE utf8_unicode_ci,
  `qty_recd` double NOT NULL DEFAULT '0',
  `quantity_inv` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_groups`
--

CREATE TABLE `0_groups` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `description` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_item_codes`
--

CREATE TABLE `0_item_codes` (
  `id` int(11) UNSIGNED NOT NULL,
  `item_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `stock_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `category_id` smallint(6) UNSIGNED NOT NULL,
  `quantity` double NOT NULL DEFAULT '1',
  `is_foreign` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_item_tax_types`
--

CREATE TABLE `0_item_tax_types` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `exempt` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_item_tax_types`
--

INSERT INTO `0_item_tax_types` (`id`, `name`, `exempt`, `inactive`) VALUES
(1, 'PPN dan PPH Ps 23', 0, 1),
(2, 'PPN &amp; WTH', 0, 1),
(3, 'PPH Ps 23', 0, 0),
(4, 'PPN.', 0, 1),
(6, 'PPN dan PPh Ps 4(2)', 0, 0),
(7, 'PPN dan PPh Ps 21', 0, 1),
(8, 'PPH Ps 21', 0, 0),
(9, 'PPH Ps 26', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `0_item_tax_type_exemptions`
--

CREATE TABLE `0_item_tax_type_exemptions` (
  `item_tax_type_id` int(11) NOT NULL DEFAULT '0',
  `tax_type_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_item_tax_type_exemptions`
--

INSERT INTO `0_item_tax_type_exemptions` (`item_tax_type_id`, `tax_type_id`) VALUES
(3, 2),
(4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `0_item_units`
--

CREATE TABLE `0_item_units` (
  `abbr` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `decimals` tinyint(2) NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_item_units`
--

INSERT INTO `0_item_units` (`abbr`, `name`, `decimals`, `inactive`) VALUES
('each', 'Each', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_journal`
--

CREATE TABLE `0_journal` (
  `type` smallint(6) NOT NULL DEFAULT '0',
  `trans_no` int(11) NOT NULL DEFAULT '0',
  `tran_date` date DEFAULT '0000-00-00',
  `reference` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `source_ref` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `event_date` date DEFAULT '0000-00-00',
  `doc_date` date NOT NULL DEFAULT '0000-00-00',
  `currency` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `amount` double NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '1',
  `posting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_locations`
--

CREATE TABLE `0_locations` (
  `loc_code` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `location_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `delivery_address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone2` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fax` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fixed_asset` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `memo` tinytext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_locations`
--

INSERT INTO `0_locations` (`loc_code`, `location_name`, `delivery_address`, `phone`, `phone2`, `fax`, `email`, `contact`, `fixed_asset`, `inactive`, `memo`) VALUES
('DEF', 'Default', 'N/A', '', '', '', '', '', 0, 0, ''),
('OF01', 'Office Empu Sendok', 'Jln. Empu Sendok', '', '', '', '', 'Panito', 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `0_loc_stock`
--

CREATE TABLE `0_loc_stock` (
  `loc_code` char(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `stock_id` char(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reorder_level` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_loc_stock`
--

INSERT INTO `0_loc_stock` (`loc_code`, `stock_id`, `reorder_level`) VALUES
('DEF', '1554545', 0),
('DEF', '23', 0),
('DEF', 'Akta', 0),
('DEF', 'ATK01', 0),
('DEF', 'BLD01', 0),
('DEF', 'BLD02', 0),
('DEF', 'Bloomberg', 0),
('DEF', 'GE001', 0),
('DEF', 'GE002', 0),
('DEF', 'Genset', 0),
('DEF', 'genset2', 0),
('DEF', 'M', 0),
('DEF', 'P', 0),
('DEF', 'PDPT001', 0),
('DEF', 'PDPT002', 0),
('DEF', 'SOF01', 0),
('DEF', 'Tanaman', 0),
('DEF', 'TB', 0),
('DEF', 'tes', 0),
('OF01', '1554545', 0),
('OF01', '23', 0),
('OF01', 'Akta', 0),
('OF01', 'ATK01', 0),
('OF01', 'BLD01', 0),
('OF01', 'BLD02', 0),
('OF01', 'Bloomberg', 0),
('OF01', 'GE001', 0),
('OF01', 'GE002', 0),
('OF01', 'Genset', 0),
('OF01', 'genset2', 0),
('OF01', 'M', 0),
('OF01', 'P', 0),
('OF01', 'PDPT001', 0),
('OF01', 'PDPT002', 0),
('OF01', 'SOF01', 0),
('OF01', 'Tanaman', 0),
('OF01', 'TB', 0),
('OF01', 'tes', 0),
('OF02', '1554545', 0),
('OF02', '23', 0),
('OF02', 'Akta', 0),
('OF02', 'ATK01', 0),
('OF02', 'BLD01', 0),
('OF02', 'BLD02', 0),
('OF02', 'Bloomberg', 0),
('OF02', 'GE001', 0),
('OF02', 'GE002', 0),
('OF02', 'Genset', 0),
('OF02', 'genset2', 0),
('OF02', 'M', 0),
('OF02', 'P', 0),
('OF02', 'PDPT001', 0),
('OF02', 'PDPT002', 0),
('OF02', 'SOF01', 0),
('OF02', 'Tanaman', 0),
('OF02', 'TB', 0),
('OF02', 'tes', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_marketing_trans`
--

CREATE TABLE `0_marketing_trans` (
  `id` int(11) NOT NULL,
  `ref` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marketing_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marketing_place` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marketing_address` text COLLATE utf8_unicode_ci,
  `marketing_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marketing_position` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marketing_company` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marketing_business` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npwp` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bpotong` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sklien` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ssyai` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `line` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_mkbd`
--

CREATE TABLE `0_mkbd` (
  `id` int(11) NOT NULL,
  `lamp` int(11) NOT NULL,
  `no` int(11) NOT NULL,
  `label` varchar(100) NOT NULL,
  `parent` int(11) NOT NULL,
  `level` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_mkbd`
--

INSERT INTO `0_mkbd` (`id`, `lamp`, `no`, `label`, `parent`, `level`) VALUES
(1, 1, 8, 'Aset Lancar', 0, 0),
(2, 1, 9, 'Kas dan Setara Kas', 0, 0),
(3, 1, 10, 'Simpanan Giro Bank', 0, 1),
(4, 1, 11, 'Kas yang Dibatasi Penggunaannya', 0, 0),
(5, 1, 12, 'Kas yang Dipisahkan', 0, 1),
(6, 1, 13, 'Rekening qq. Efek Nasabah', 0, 1),
(7, 1, 14, 'Deposito Berjangka', 0, 0),
(8, 1, 15, 'Deposito Bank Dalam Negeri', 0, 1),
(9, 1, 16, 'Deposito Bank Umum dengan jangka waktu kurang  atau sama dengan 3 (tiga) bulan', 0, 2),
(10, 1, 17, 'Deposito Bank Umum dengan jangka waktu lebih dari 3 (tiga) bulan', 0, 2),
(11, 1, 18, '     Dijamin oleh Lembaga Penjamin Simpanan', 0, 2),
(12, 1, 19, '     Tidak Dijamin oleh Lembaga Penjamin Simpanan', 0, 2),
(13, 1, 20, 'Tidak Sedang Diajukan atau Dinyatakan Pailit/Dilikuidasi', 0, 3),
(14, 1, 21, 'Sedang diajukan atau Dinyatakan Pailit/Dilikuidasi', 0, 3),
(15, 1, 22, 'Deposito pada Bank Perkreditan Rakyat', 0, 2),
(16, 1, 23, 'Deposito Bank di Luar Negeri', 0, 1),
(17, 1, 24, 'Piutang Reverse Repo', 0, 0),
(18, 1, 25, 'Reverse Repo Surat Berharga Negara', 0, 1),
(19, 1, 26, 'Reverse Repo Obligasi Korporasi', 0, 1),
(20, 1, 27, 'Reverse Repo Efek Bersifat Ekuitas', 0, 1),
(21, 1, 28, 'Piutang Lembaga Kliring dan Penjaminan', 0, 0),
(22, 1, 29, 'Uang Jaminan Lembaga Kliring dan Penjaminan', 0, 1),
(23, 1, 30, 'Piutang Transaksi Bursa', 0, 1),
(24, 1, 31, 'Piutang Komisi', 0, 1),
(25, 1, 32, 'Piutang Nasabah', 0, 0),
(26, 1, 33, 'Piutang Nasabah Pemilik Rekening Efek', 0, 1),
(27, 1, 34, 'Transaksi Beli Efek', 0, 2),
(28, 1, 35, 'Saldo Debit Rekening Efek Nasabah', 0, 2),
(29, 1, 36, 'Piutang Nasabah Umum', 0, 1),
(30, 1, 37, 'Piutang Nasabah Kelembagaan', 0, 1),
(31, 1, 38, 'Transaksi Beli Efek', 0, 2),
(32, 1, 39, 'Gagal Serah - Nasabah Kelembagaan', 0, 2),
(33, 1, 40, 'Piutang Perusahaan Efek Lain', 0, 0),
(34, 1, 41, 'Uang Jaminan untuk Peminjaman Efek ', 0, 1),
(35, 1, 42, 'Uang Jaminan pada Anggota Kliring', 0, 1),
(36, 1, 43, 'Transaksi Jual Efek', 0, 1),
(37, 1, 44, 'Gagal Serah - Perusahaan Efek', 0, 1),
(38, 1, 45, 'Piutang Komisi ', 0, 1),
(39, 1, 46, 'Dana Pesanan Efek Dibayar Dimuka', 0, 1),
(40, 1, 47, 'Piutang Kegiatan Penjaminan Emisi Efek', 0, 0),
(41, 1, 48, 'Piutang Jasa Emisi Efek', 0, 1),
(42, 1, 49, 'Piutang Jasa Arranger Penerbitan Efek', 0, 1),
(43, 1, 50, 'Piutang Jasa Penasihat Keuangan ', 0, 1),
(44, 1, 51, 'Piutang Biaya Talangan - Penjamin Emisi Efek', 0, 1),
(45, 1, 52, 'Piutang Kegiatan Manajer Investasi', 0, 0),
(46, 1, 53, 'Piutang Manajemen Fee ', 0, 1),
(47, 1, 54, 'Piutang Selling Fee dan Redemption Fee', 0, 1),
(48, 1, 55, 'Piutang Biaya Talangan – Manajer Investasi', 0, 1),
(49, 1, 56, 'Piutang Transaksi Jual Efek Lainnya', 0, 0),
(50, 1, 57, 'Piutang Dividen dan Bunga', 0, 0),
(51, 1, 58, 'Portofolio Efek', 0, 0),
(52, 1, 59, 'Sertifikat Bank Indonesia', 0, 1),
(53, 1, 60, 'Surat Berharga Negara', 0, 1),
(54, 1, 61, '0-7 tahun', 0, 2),
(55, 1, 62, '7-15 tahun', 0, 2),
(56, 1, 63, '15 tahun ke atas', 0, 2),
(57, 1, 64, 'Obligasi Korporasi, Sukuk Korporasi, atau Efek Beragun Aset Arus Kas Tetap yang tercatat di Bursa Ef', 0, 1),
(58, 1, 65, 'Peringkat setara dengan AAA ', 0, 2),
(59, 1, 66, 'Peringkat setara dengan AA hingga kurang dari setara dengan AAA ', 0, 2),
(60, 1, 67, 'Peringkat setara dengan A atau hingga kurang dari setara dengan AA ', 0, 2),
(61, 1, 68, 'Peringkat setara dengan BBB- atau hingga kurang dari setara dengan A ', 0, 2),
(62, 1, 69, 'Peringkat kurang dari setara dengan BBB-', 0, 2),
(63, 1, 70, 'Efek Bersifat Ekuitas atau Efek Beragun Aset Arus Kas Tidak Tetap yang tercatat di Bursa Efek di Ind', 0, 1),
(64, 1, 71, 'Haircut Komite 5% dan 10%', 0, 2),
(65, 1, 72, 'Haircut Komite 15% dan 20%', 0, 2),
(66, 1, 73, 'Haircut Komite 25%', 0, 2),
(67, 1, 74, 'Haircut Komite 30% ', 0, 2),
(68, 1, 75, 'Haircut Komite 35% ', 0, 2),
(69, 1, 76, 'Haircut Komite 40% ', 0, 2),
(70, 1, 77, 'Haircut Komite 45% ', 0, 2),
(71, 1, 78, 'Haircut Komite 50% ', 0, 2),
(72, 1, 79, 'Haircut Komite 55% sd 80%', 0, 2),
(73, 1, 80, 'Haircut Komite 85% sd100%', 0, 2),
(74, 1, 81, 'Efek Bersifat Ekuitas yang tidak lagi tercatat  pada Bursa Efek di Indonesia (delist)', 0, 1),
(75, 1, 82, 'Efek Luar Negeri', 0, 1),
(76, 1, 83, 'Unit Penyertaan Reksa Dana', 0, 1),
(77, 1, 84, 'Pasar uang', 0, 2),
(78, 1, 85, 'Terproteksi', 0, 2),
(79, 1, 86, 'Dengan Penjaminan', 0, 2),
(80, 1, 87, 'Pendapatan tetap', 0, 2),
(81, 1, 88, 'Campuran atau Saham', 0, 2),
(82, 1, 89, 'Indeks', 0, 2),
(83, 1, 90, 'Penyertaan Terbatas', 0, 2),
(84, 1, 91, 'Investasi yang Dikelola oleh Perusahaan Efek Lain', 0, 1),
(85, 1, 92, 'Unit Penyertaan Dana Investasi Real Estat', 0, 1),
(86, 1, 93, 'Kontrak Opsi', 0, 1),
(87, 1, 94, 'Kontrak Berjangka', 0, 1),
(88, 1, 95, 'Efek Lain yang Terdaftar di Bapepam dan LK', 0, 1),
(89, 1, 96, 'Efek Repo, Dijaminkan, atau Dipinjamkan', 0, 1),
(90, 1, 97, 'Surat Berharga Negara', 0, 2),
(91, 1, 98, 'Obligasi Korporasi ', 0, 2),
(92, 1, 99, 'Efek Bersifat Ekuitas', 0, 2),
(93, 1, 100, 'Total Aset Lancar :', 0, 1),
(94, 1, 101, 'Aset Keuangan Lainnya', 0, 0),
(95, 1, 102, 'Piutang Kepada Pihak Istimewa lainnya', 0, 1),
(96, 1, 103, 'Piutang Nasabah Pemilik Rekening Efek untuk transaksi beli Efek sejak tanggal penyelesaian transaksi', 0, 1),
(97, 1, 104, 'Piutang lainnya', 0, 1),
(98, 1, 105, 'Pajak dibayar di muka', 0, 1),
(99, 1, 106, 'Biaya dibayar di muka', 0, 1),
(100, 1, 107, 'Jaminan lainnya', 0, 1),
(101, 1, 108, 'Investasi Jangka Panjang ', 0, 0),
(102, 1, 109, 'Aset Tetap', 0, 0),
(103, 1, 110, 'Aset Pajak Tangguhan', 0, 0),
(104, 1, 111, 'Aset Lain - lain', 0, 0),
(105, 1, 112, 'Total Aset Tetap dan Aset Lainnya :', 0, 1),
(106, 1, 113, 'TOTAL ASET', 0, 1),
(107, 2, 121, 'LIABILITAS', 0, 0),
(108, 2, 122, 'Utang Jangka Pendek', 0, 0),
(109, 2, 123, 'Surat Utang Jangka Pendek', 0, 0),
(110, 2, 124, 'Utang Repo', 0, 0),
(111, 2, 125, 'Repo Surat Berharga Negara', 0, 1),
(112, 2, 126, 'Repo Obligasi Korporasi', 0, 1),
(113, 2, 127, 'Repo Efek Bersifat Ekuitas', 0, 1),
(114, 2, 128, 'Utang Lembaga Kliring Penjaminan', 0, 0),
(115, 2, 129, 'Utang Transaksi Bursa', 0, 1),
(116, 2, 130, 'Utang Komisi', 0, 1),
(117, 2, 131, 'Utang Nasabah ', 0, 0),
(118, 2, 132, 'Utang Nasabah Pemilik Rekening Efek', 0, 1),
(119, 2, 133, 'Transaksi Jual Efek', 0, 2),
(120, 2, 134, 'Saldo Kredit', 0, 2),
(121, 2, 135, 'Utang Nasabah Kelembagaan', 0, 1),
(122, 2, 136, 'Transaksi Jual Efek', 0, 2),
(123, 2, 137, 'Gagal Terima - Nasabah Kelembagaan', 0, 2),
(124, 2, 138, 'Utang Perusahaan Efek Lain', 0, 0),
(125, 2, 139, 'Uang Jaminan untuk Peminjaman Efek', 0, 1),
(126, 2, 140, 'Uang Jaminan dari PE non Anggota Kliring', 0, 1),
(127, 2, 141, 'Transaksi Beli Efek', 0, 1),
(128, 2, 142, 'Gagal Terima - Perusahaan Efek', 0, 1),
(129, 2, 143, 'Utang Komisi', 0, 1),
(130, 2, 144, 'Utang Kegiatan Penjaminan Emisi Efek', 0, 0),
(131, 2, 145, 'Utang Dalam Rangka Penawaran Umum/Penawaran Terbatas', 0, 1),
(132, 2, 146, 'Utang Nasabah Umum', 0, 2),
(133, 2, 147, 'Utang Emiten', 0, 2),
(134, 2, 148, 'Utang Kepada Penerbit Efek', 0, 2),
(135, 2, 149, 'Utang Jasa Emisi Efek', 0, 1),
(136, 2, 150, 'Utang Kegiatan Manajer Investasi', 0, 0),
(137, 2, 151, 'Utang Komisi Agen Penjual', 0, 1),
(138, 2, 152, 'Utang Transaksi Beli Efek Lainnya', 0, 0),
(139, 2, 153, 'Utang Efek Posisi Short – Sendiri ', 0, 0),
(140, 2, 154, 'Surat Berharga Negara', 0, 1),
(141, 2, 155, 'Efek Bersifat Utang yang tercatat di Bursa Efek di Indonesia', 0, 1),
(142, 2, 156, 'Efek Bersifat Ekuitas yang tercatat di Bursa Efek di Indonesia, atau Reksa Dana yang Unit Penyertaan', 0, 1),
(143, 2, 157, 'Efek Lain yang Terdaftar di Bapepam dan LK', 0, 1),
(144, 2, 158, 'Efek Luar Negeri', 0, 1),
(145, 2, 159, 'Utang Jangka Pendek Lainnya', 0, 0),
(146, 2, 160, 'Utang Jangka Panjang', 0, 0),
(147, 2, 161, 'Utang Obligasi', 0, 0),
(148, 2, 162, 'Utang Lain-lain', 0, 0),
(149, 2, 163, 'Utang Sub-Ordinasi', 0, 0),
(150, 2, 164, 'TOTAL LIABILITAS', 0, 0),
(151, 2, 165, 'EKUITAS', 0, 0),
(152, 2, 166, 'Ekuitas Yang Dapat Diatribusikan Kepada Pemilik Entitas Induk', 0, 0),
(153, 2, 167, 'Modal Saham', 0, 1),
(154, 2, 168, 'Tambahan Modal Disetor', 0, 1),
(155, 2, 169, 'Ekuitas Lainnya', 0, 1),
(156, 2, 170, 'Saldo Laba', 0, 1),
(157, 2, 171, 'Kepentingan Non Pengendali', 0, 0),
(158, 2, 172, 'TOTAL EKUITAS', 0, 0),
(159, 2, 173, 'TOTAL LIABILITAS DAN EKUITAS', 0, 0),
(160, 6, 8, 'Dana Milik Perusahaan Efek ', 0, 0),
(161, 6, 9, 'Dana Milik Nasabah Pemilik Rekening', 0, 0),
(162, 6, 10, 'Dana Bebas', 0, 1),
(163, 6, 11, 'Dana yang Dijaminkan', 0, 1),
(164, 6, 12, 'Dana Milik Nasabah Umum', 0, 0),
(165, 6, 13, 'Dana Pemesanan Efek', 0, 1),
(166, 6, 14, 'Selisih Dana Positif ', 0, 0),
(167, 6, 15, 'Total Debit ', 0, 1),
(168, 6, 17, 'Dana yang disimpan di Unit Kerja yang Menjalankan Fungsi Pembukuan', 0, 0),
(169, 6, 18, 'Dana yang disimpan pada Bank', 0, 0),
(170, 6, 19, 'Dana Milik Perusahaan Efek ', 0, 1),
(171, 6, 20, 'Dana Milik Nasabah Pemilik Rekening', 0, 1),
(172, 6, 21, 'Dana Milik Nasabah Umum', 0, 1),
(173, 6, 22, 'Selisih Dana Negatif', 0, 0),
(174, 6, 23, 'Total Kredit ', 0, 1),
(175, 8, 8, 'Total Liabilitas', 0, 0),
(176, 8, 9, 'Total Ranking Liabilities', 0, 0),
(177, 8, 10, 'Total Liabilitas dan Ranking Liabilities \n(Baris 8 + Baris 9)', 0, 0),
(178, 8, 11, 'Dikurangi Utang Sub-Ordinasi', 0, 0),
(179, 8, 12, 'Dikurangi Utang Dalam Rangka Penawaran Umum/Penawaran Terbatas', 0, 0),
(180, 8, 13, 'Utang Nasabah Umum', 0, 1),
(181, 8, 14, 'Utang Emiten', 0, 1),
(182, 8, 15, 'Utang Kepada Penerbit Efek', 0, 1),
(183, 8, 16, 'Total Liabilitas dan Ranking Liabilities Tanpa Utang Subordinasi dan Utang Dalam Rangka Penawaran Um', 0, 0),
(184, 8, 17, 'Nilai MKBD yang diwajibkan untuk PPE atau PEE', 0, 0),
(185, 8, 18, 'Nilai Persyaratan Minimal MKBD*', 0, 1),
(186, 8, 19, '6,25% dari baris 16', 0, 1),
(187, 8, 20, 'MKBD yang dipersyaratkan \n(nilai yang lebih tinggi antara baris 18 dan baris 19)', 0, 1),
(188, 8, 21, 'Nilai MKBD yang diwajibkan untuk MI', 0, 0),
(189, 8, 22, 'Nilai Persyaratan Minimal MKBD**', 0, 1),
(190, 8, 23, 'Nilai dana yang dikelola oleh MI', 0, 1),
(191, 8, 24, '0,1 % dari baris 23', 0, 1),
(192, 8, 25, 'Nilai MKBD yang dipersyaratkan\n(baris 22 ditambah baris 24)', 0, 1),
(193, 8, 26, 'Nilai MKBD (baris 20 ditambah baris 25)', 0, 0),
(194, 9, 8, 'MODAL KERJA', 0, 0),
(195, 9, 9, 'Total Aset Lancar', 0, 1),
(196, 9, 10, 'Kurang :', 0, 1),
(197, 9, 11, 'Total Liabilitas', 0, 1),
(198, 9, 12, 'Total Ranking Liabilities', 0, 1),
(199, 9, 13, 'Total Modal Kerja\n(Baris 9 dikurangi Baris 11 dan Baris 12)', 0, 2),
(200, 9, 14, 'MODAL KERJA BERSIH', 0, 0),
(201, 9, 15, 'Total Modal Kerja (Baris 13)', 0, 1),
(202, 9, 16, 'Tambah :', 0, 1),
(203, 9, 17, 'Utang Sub-Ordinasi', 0, 1),
(204, 9, 18, 'Total Modal Kerja Bersih\n(Baris 15 ditambah Baris 17)', 0, 2),
(205, 9, 19, 'MODAL KERJA BERSIH DISESUAIKAN', 0, 0),
(206, 9, 20, 'Total  Modal Kerja Bersih (Baris 18)', 0, 1),
(207, 9, 21, 'Kurang:', 0, 1),
(208, 9, 22, 'Penyesuaian Risiko Likuiditas', 0, 1),
(209, 9, 23, 'Deposito Bank Dalam Negeri', 0, 2),
(210, 9, 24, 'Deposito Bank Umum dengan jangka waktu kurang  atau sama dengan 3 (tiga) bulan', 0, 3),
(211, 9, 25, 'Deposito Bank Umum dengan jangka waktu lebih dari 3 (tiga) bulan', 0, 3),
(212, 9, 26, 'Dijamin oleh Lembaga Penjamin Simpanan', 0, 4),
(213, 9, 27, 'Tidak Dijamin oleh Lembaga Penjamin Simpanan', 0, 4),
(214, 9, 28, '', 0, 4),
(215, 9, 29, '', 0, 4),
(216, 9, 30, 'Deposito pada Bank Perkreditan Rakyat', 0, 3),
(217, 9, 31, 'Deposito Bank di Luar Negeri', 0, 2),
(218, 9, 32, 'Penyesuaian Risiko Pasar', 0, 1),
(219, 9, 33, 'Sertifikat Bank Indonesia', 0, 2),
(220, 9, 34, 'Surat Berharga Negara', 0, 2),
(221, 9, 35, '0-7 tahun', 0, 3),
(222, 9, 36, '7-15 tahun', 0, 3),
(223, 9, 37, '15 tahun ke atas', 0, 3),
(224, 9, 38, 'Obligasi Korporasi, Sukuk Korporasi, atau Efek Beragun Aset Arus Kas Tetap yang tercatat di Bursa Ef', 0, 2),
(225, 9, 39, 'Peringkat setara dengan AAA', 0, 3),
(226, 9, 40, 'Peringkat setara dengan AA hingga kurang dari setara dengan AAA ', 0, 3),
(227, 9, 41, 'Peringkat setara dengan A atau hingga kurang dari setara dengan AA ', 0, 3),
(228, 9, 42, 'Peringkat setara dengan BBB- atau hingga kurang dari setara dengan A ', 0, 3),
(229, 9, 43, 'Peringkat kurang dari setara dengan BBB-', 0, 3),
(230, 9, 44, 'Efek Bersifat Ekuitas yang tercatat di Bursa Efek di Indonesia, Reksa Dana yang Unit Penyertaannya d', 0, 2),
(231, 9, 45, 'Haircut Komite 5% dan 10%', 0, 3),
(232, 9, 46, 'Haircut Komite 15% dan 20%', 0, 3),
(233, 9, 47, 'Haircut Komite 25%', 0, 3),
(234, 9, 48, 'Haircut Komite 30% ', 0, 3),
(235, 9, 49, 'Haircut Komite 35% ', 0, 3),
(236, 9, 50, 'Haircut Komite 40% ', 0, 3),
(237, 9, 51, 'Haircut Komite 45% ', 0, 3),
(238, 9, 52, 'Haircut Komite 50% ', 0, 3),
(239, 9, 53, 'Haircut Komite 55% sd 80%', 0, 3),
(240, 9, 54, 'Haircut Komite 85% sd100%', 0, 3),
(241, 9, 55, 'Efek Bersifat Ekuitas yang tidak lagi tercatat  pada Bursa Efek di Indonesia (delist)', 0, 2),
(242, 9, 56, 'Efek Luar Negeri', 0, 2),
(243, 9, 57, 'Unit Penyertaan Reksa Dana', 0, 2),
(244, 9, 58, 'Pasar uang', 0, 3),
(245, 9, 59, 'Terproteksi', 0, 3),
(246, 9, 60, 'Dengan Penjaminan', 0, 3),
(247, 9, 61, 'Pendapatan tetap', 0, 3),
(248, 9, 62, 'Campuran atau Saham', 0, 3),
(249, 9, 63, 'Indeks', 0, 3),
(250, 9, 64, 'Penyertaan Terbatas', 0, 3),
(251, 9, 65, 'Investasi yang Dikelola oleh Perusahaan Efek Lain', 0, 2),
(252, 9, 66, 'Unit Penyertaan Dana Investasi Real Estat', 0, 2),
(253, 9, 67, 'Kontrak Opsi', 0, 2),
(254, 9, 68, 'Kontrak Berjangka', 0, 2),
(255, 9, 69, 'Efek Lain yang Terdaftar di Bapepam dan LK', 0, 2),
(256, 9, 70, 'Efek Repo', 0, 2),
(257, 9, 71, 'Surat Berharga Negara', 0, 3),
(258, 9, 72, '0-7 tahun', 0, 4),
(259, 9, 73, '7-15 tahun', 0, 4),
(260, 9, 74, '15 tahun ke atas', 0, 4),
(261, 9, 75, 'Obligasi Korporasi', 0, 3),
(262, 9, 76, 'Peringkat setara dengan AAA', 0, 4),
(263, 9, 77, 'Peringkat setara dengan AA hingga kurang dari setara dengan AAA ', 0, 4),
(264, 9, 78, 'Peringkat setara dengan A atau hingga kurang dari setara dengan AA ', 0, 4),
(265, 9, 79, 'Peringkat setara dengan BBB- atau hingga kurang dari setara dengan A ', 0, 4),
(266, 9, 80, 'Peringkat kurang dari setara dengan BBB-', 0, 4),
(267, 9, 81, 'Efek Bersifat Ekuitas', 0, 3),
(268, 9, 82, 'Haircut Komite 5% dan 10%', 0, 4),
(269, 9, 83, 'Haircut Komite 15% dan 20%', 0, 4),
(270, 9, 84, 'Haircut Komite 25%', 0, 4),
(271, 9, 85, 'Haircut Komite 30% ', 0, 4),
(272, 9, 86, 'Haircut Komite 35% ', 0, 4),
(273, 9, 87, 'Haircut Komite 40% ', 0, 4),
(274, 9, 88, 'Haircut Komite 45% ', 0, 4),
(275, 9, 89, 'Haircut Komite 50% ', 0, 4),
(276, 9, 90, 'Haircut Komite 55% sd 80%', 0, 4),
(277, 9, 91, 'Haircut Komite 85% sd100%', 0, 4),
(278, 9, 92, 'Penyesuaian Risiko Kredit', 0, 1),
(279, 9, 93, 'Gagal Serah - Nasabah Kelembagaan', 0, 2),
(280, 9, 94, 'Gagal Serah - Perusahaan Efek', 0, 2),
(281, 9, 95, 'Penyesuaian Risiko Kegiatan Usaha', 0, 1),
(282, 9, 96, 'Kelebihan V.D.5-6 baris 10 dibanding V.D.5.6 baris 20 kolom D', 0, 2),
(283, 9, 97, 'Kelebihan V.D.5-2 baris 146 dibanding V.D.5.1 baris 12', 0, 2),
(284, 9, 98, 'Kelebihan V.D.5-7 baris 11 kolom B dibanding V.D.5.7 baris 33 kolom D', 0, 2),
(285, 9, 99, 'Kelebihan V.D.5-7 baris 62 kolom E dibanding V.D.5.1 baris 11 kolom B', 0, 2),
(286, 9, 100, 'Tambah : ', 0, 1),
(287, 9, 101, 'Pengembalian Haircut atas Efek yang Ditutup dengan Lindung Nilai', 0, 1),
(288, 9, 102, 'Total Modal Kerja Bersih Disesuaikan ', 0, 2),
(289, 9, 103, 'NILAI MKBD YANG DIWAJIBKAN', 0, 0),
(290, 9, 104, 'LEBIH (KURANG) MKBD', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_mkbd_coa`
--

CREATE TABLE `0_mkbd_coa` (
  `id` int(11) NOT NULL,
  `mkbd_id` int(11) NOT NULL,
  `account` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `0_no_faktur`
--

CREATE TABLE `0_no_faktur` (
  `id` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `no_urut` int(11) NOT NULL,
  `no_faktur` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `0_payment_terms`
--

CREATE TABLE `0_payment_terms` (
  `terms_indicator` int(11) NOT NULL,
  `terms` char(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `days_before_due` smallint(6) NOT NULL DEFAULT '0',
  `day_in_following_month` smallint(6) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_payment_terms`
--

INSERT INTO `0_payment_terms` (`terms_indicator`, `terms`, `days_before_due`, `day_in_following_month`, `inactive`) VALUES
(1, '14 Hari Kerja', 20, 0, 0),
(2, 'Akhir bulan berikutnya', 0, 30, 0),
(3, '10 Hari Kalender', 10, 0, 0),
(4, 'Cash Only', 0, 0, 0),
(5, '7 Hari setelah invoice diterima', 7, 0, 0),
(6, 'Pay Terms I', -1, 0, 0),
(7, 'Setiap tanggal 5 di periode berikutnya', 0, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_prices`
--

CREATE TABLE `0_prices` (
  `id` int(11) NOT NULL,
  `stock_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sales_type_id` int(11) NOT NULL DEFAULT '0',
  `curr_abrev` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `price` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_printers`
--

CREATE TABLE `0_printers` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `queue` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `host` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `port` smallint(11) UNSIGNED NOT NULL,
  `timeout` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_printers`
--

INSERT INTO `0_printers` (`id`, `name`, `description`, `queue`, `host`, `port`, `timeout`) VALUES
(1, 'QL500', 'Label printer', 'QL500', 'server', 127, 20),
(2, 'Samsung', 'Main network printer', 'scx4521F', 'server', 515, 5),
(3, 'Local', 'Local print server at user IP', 'lp', '', 515, 10);

-- --------------------------------------------------------

--
-- Table structure for table `0_print_profiles`
--

CREATE TABLE `0_print_profiles` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `profile` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `report` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `printer` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_print_profiles`
--

INSERT INTO `0_print_profiles` (`id`, `profile`, `report`, `printer`) VALUES
(1, 'Out of office', NULL, 0),
(2, 'Sales Department', NULL, 0),
(3, 'Central', NULL, 2),
(4, 'Sales Department', '104', 2),
(5, 'Sales Department', '105', 2),
(6, 'Sales Department', '107', 2),
(7, 'Sales Department', '109', 2),
(8, 'Sales Department', '110', 2),
(9, 'Sales Department', '201', 2);

-- --------------------------------------------------------

--
-- Table structure for table `0_purch_data`
--

CREATE TABLE `0_purch_data` (
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `stock_id` char(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `price` double NOT NULL DEFAULT '0',
  `suppliers_uom` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `conversion_factor` double NOT NULL DEFAULT '1',
  `supplier_description` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_purch_orders`
--

CREATE TABLE `0_purch_orders` (
  `order_no` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `comments` tinytext COLLATE utf8_unicode_ci,
  `ord_date` date NOT NULL DEFAULT '0000-00-00',
  `reference` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `requisition_no` tinytext COLLATE utf8_unicode_ci,
  `into_stock_location` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `delivery_address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `total` double NOT NULL DEFAULT '0',
  `prep_amount` double NOT NULL DEFAULT '0',
  `alloc` double NOT NULL DEFAULT '0',
  `tax_included` tinyint(1) NOT NULL DEFAULT '0',
  `flag` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_purch_order_details`
--

CREATE TABLE `0_purch_order_details` (
  `po_detail_item` int(11) NOT NULL,
  `order_no` int(11) NOT NULL DEFAULT '0',
  `item_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` tinytext COLLATE utf8_unicode_ci,
  `delivery_date` date NOT NULL DEFAULT '0000-00-00',
  `qty_invoiced` double NOT NULL DEFAULT '0',
  `unit_price` double NOT NULL DEFAULT '0',
  `act_price` double NOT NULL DEFAULT '0',
  `std_cost_unit` double NOT NULL DEFAULT '0',
  `quantity_ordered` double NOT NULL DEFAULT '0',
  `quantity_received` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_quick_entries`
--

CREATE TABLE `0_quick_entries` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `usage` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `base_amount` double NOT NULL DEFAULT '0',
  `base_desc` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bal_type` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_quick_entries`
--

INSERT INTO `0_quick_entries` (`id`, `type`, `description`, `usage`, `base_amount`, `base_desc`, `bal_type`) VALUES
(1, 3, 'Bayar Telepon', 'Listrik Bulanan', 0, '64001', 1),
(2, 0, 'Bayar Listrik', 'Listrik Bulanan', 1000000, 'Amount', 0),
(3, 0, 'Internet', 'Internet kantor bulanan', 3000000, 'pokok', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_quick_entry_lines`
--

CREATE TABLE `0_quick_entry_lines` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `qid` smallint(6) UNSIGNED NOT NULL,
  `amount` double DEFAULT '0',
  `memo` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `dest_id` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dimension_id` smallint(6) UNSIGNED DEFAULT NULL,
  `dimension2_id` smallint(6) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_quick_entry_lines`
--

INSERT INTO `0_quick_entry_lines` (`id`, `qid`, `amount`, `memo`, `action`, `dest_id`, `dimension_id`, `dimension2_id`) VALUES
(1, 1, 10, 'PPN Masukan', '%+', '11703', 0, 0),
(2, 1, 5000000, 'Telepon ', 'a', '64001', 0, 0),
(3, 1, 2, 'PPh 23', '%-', '21220', 0, 0),
(4, 2, 1000000, 'Biaya Listrik Bulanan', 'a', '64002', 0, 0),
(7, 2, 10, 'PPN 10 %', '%+', '11703', 0, 0),
(8, 2, 2, 'PPh 2%', '%-', '21220', 0, 0),
(10, 3, 3000000, 'Internet bulanan', 'a', '64001', 0, 0),
(11, 3, 10, 'Internet bulanan', '%+', '11703', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_recurrent_invoices`
--

CREATE TABLE `0_recurrent_invoices` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `description` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `order_no` int(11) UNSIGNED NOT NULL,
  `debtor_no` int(11) UNSIGNED DEFAULT NULL,
  `group_no` smallint(6) UNSIGNED DEFAULT NULL,
  `days` int(11) NOT NULL DEFAULT '0',
  `monthly` int(11) NOT NULL DEFAULT '0',
  `begin` date NOT NULL DEFAULT '0000-00-00',
  `end` date NOT NULL DEFAULT '0000-00-00',
  `last_sent` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_reflines`
--

CREATE TABLE `0_reflines` (
  `id` int(11) NOT NULL,
  `trans_type` int(11) NOT NULL,
  `prefix` char(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pattern` varchar(35) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `description` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_reflines`
--

INSERT INTO `0_reflines` (`id`, `trans_type`, `prefix`, `pattern`, `description`, `default`, `inactive`) VALUES
(1, 0, '', 'MI-1707002', '', 1, 0),
(2, 1, '', '', '', 1, 0),
(3, 2, '', '', '', 1, 0),
(4, 4, '', '2', '', 1, 0),
(5, 10, '', '19', '', 1, 0),
(6, 11, '', '1', '', 1, 0),
(7, 12, '', '', '', 1, 0),
(8, 13, '', '2', '', 1, 0),
(9, 16, '', '1', '', 1, 0),
(10, 17, '', '1', '', 1, 0),
(11, 18, '', '16', '', 1, 0),
(12, 20, '', '-1701008', '', 1, 0),
(13, 21, '', '1', '', 1, 0),
(14, 22, '', '', '', 1, 0),
(15, 25, '', '10', '', 1, 0),
(16, 26, '', '1', '', 1, 0),
(17, 28, '', '1', '', 1, 0),
(18, 29, '', '1', '', 1, 0),
(19, 30, '', '3', '', 1, 0),
(20, 32, '', '1', '', 1, 0),
(21, 35, '', '1', '', 1, 0),
(22, 40, '', '8', '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_refs`
--

CREATE TABLE `0_refs` (
  `id` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `reference` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_salesman`
--

CREATE TABLE `0_salesman` (
  `salesman_code` int(11) NOT NULL,
  `salesman_name` char(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `salesman_phone` char(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `salesman_fax` char(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `salesman_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `provision` double NOT NULL DEFAULT '0',
  `break_pt` double NOT NULL DEFAULT '0',
  `provision2` double NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_salesman`
--

INSERT INTO `0_salesman` (`salesman_code`, `salesman_name`, `salesman_phone`, `salesman_fax`, `salesman_email`, `provision`, `break_pt`, `provision2`, `inactive`) VALUES
(1, 'Sales 1', '', '', '', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_sales_orders`
--

CREATE TABLE `0_sales_orders` (
  `order_no` int(11) NOT NULL,
  `trans_type` smallint(6) NOT NULL DEFAULT '30',
  `version` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `debtor_no` int(11) NOT NULL DEFAULT '0',
  `branch_code` int(11) NOT NULL DEFAULT '0',
  `reference` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `customer_ref` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `comments` tinytext COLLATE utf8_unicode_ci,
  `ord_date` date NOT NULL DEFAULT '0000-00-00',
  `order_type` int(11) NOT NULL DEFAULT '0',
  `ship_via` int(11) NOT NULL DEFAULT '0',
  `delivery_address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deliver_to` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `freight_cost` double NOT NULL DEFAULT '0',
  `from_stk_loc` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `delivery_date` date NOT NULL DEFAULT '0000-00-00',
  `payment_terms` int(11) DEFAULT NULL,
  `total` double NOT NULL DEFAULT '0',
  `prep_amount` double NOT NULL DEFAULT '0',
  `alloc` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_sales_order_details`
--

CREATE TABLE `0_sales_order_details` (
  `id` int(11) NOT NULL,
  `order_no` int(11) NOT NULL DEFAULT '0',
  `trans_type` smallint(6) NOT NULL DEFAULT '30',
  `stk_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` tinytext COLLATE utf8_unicode_ci,
  `qty_sent` double NOT NULL DEFAULT '0',
  `unit_price` double NOT NULL DEFAULT '0',
  `quantity` double NOT NULL DEFAULT '0',
  `invoiced` double NOT NULL DEFAULT '0',
  `discount_percent` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_sales_pos`
--

CREATE TABLE `0_sales_pos` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `pos_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `cash_sale` tinyint(1) NOT NULL,
  `credit_sale` tinyint(1) NOT NULL,
  `pos_location` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `pos_account` smallint(6) UNSIGNED NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_sales_pos`
--

INSERT INTO `0_sales_pos` (`id`, `pos_name`, `cash_sale`, `credit_sale`, `pos_location`, `pos_account`, `inactive`) VALUES
(1, 'Default', 1, 1, 'DEF', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_sales_types`
--

CREATE TABLE `0_sales_types` (
  `id` int(11) NOT NULL,
  `sales_type` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_included` int(1) NOT NULL DEFAULT '0',
  `factor` double NOT NULL DEFAULT '1',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_sales_types`
--

INSERT INTO `0_sales_types` (`id`, `sales_type`, `tax_included`, `factor`, `inactive`) VALUES
(1, 'Pendapatan Jasa', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_security_roles`
--

CREATE TABLE `0_security_roles` (
  `id` int(11) NOT NULL,
  `role` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sections` text COLLATE utf8_unicode_ci,
  `areas` text COLLATE utf8_unicode_ci,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_security_roles`
--

INSERT INTO `0_security_roles` (`id`, `role`, `description`, `sections`, `areas`, `inactive`) VALUES
(1, 'Pencarian', 'Pencarian', '768;2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15872;16128', '257;258;259;260;513;514;515;516;517;518;519;520;521;522;523;524;525;773;774;2822;3073;3075;3076;3077;3329;3330;3331;3332;3333;3334;3335;5377;5633;5640;5889;5890;5891;7937;7938;7939;7940;8193;8194;8450;8451;10497;10753;11009;11010;11012;13313;13315;15617;15618;15619;15620;15621;15622;15623;15624;15625;15626;15873;15882;16129;16130;16131;16132;775', 0),
(2, 'Admin Sistem', 'Admin Sistem', '256;512;768;2816;3072;3328;5376;5632;5888;7936;8192;8448;9216;9472;9728;10496;10752;11008;13056;13312;15616;15872;16128;1467392', '257;258;259;260;513;514;515;516;517;518;519;520;521;522;523;524;525;526;769;770;771;772;773;774;775;2817;2818;2819;2820;2821;2822;2823;3073;3083;3074;3082;3075;3076;3077;3078;3079;3080;3081;3329;3330;3331;3332;3333;3334;3335;5377;5633;5634;5641;5635;5636;5637;5638;5639;5640;5889;5890;5891;7937;7938;7939;7940;8193;8194;8195;8196;8197;8449;8450;8451;9217;9218;9220;9473;9474;9475;9476;9729;10497;10753;10754;10755;10756;10757;11009;11010;11011;11012;13057;13313;13314;13315;15617;15618;15619;15620;15621;15622;15623;15624;15628;15625;15626;15627;15630;15629;15873;15874;15875;15876;15877;15878;15879;15880;15883;15881;15882;15884;16129;16130;16131;16132;1467492', 0),
(3, 'Mgr Sales', 'Mgr Sales', '768;3072;5632;8192;15872', '773;774;3073;3075;3081;5633;8194;15873;775', 0),
(4, 'Mgr Gudang', 'Mgr Gudang', '2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15872;16128;768', '775', 0),
(5, 'Mgr Produksi', 'Mgr Produksi', '512;2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128;768', '775', 0),
(6, 'Pembelian', 'Pembelian', '512;2816;3072;3328;5376;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128;768', '775', 0),
(7, 'Penagihan', 'Penagihan', '512;768;2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128', '521;523;524;771;773;774;2818;2819;2820;2821;2822;2823;3073;3073;3074;3075;3076;3077;3078;3079;3080;3081;3081;3329;3330;3330;3330;3331;3331;3332;3333;3334;3335;5633;5633;5634;5637;5638;5639;5640;5640;5889;5890;5891;8193;8194;8194;8196;8197;8450;8451;10753;10755;11009;11010;11012;13313;13315;15617;15619;15620;15621;15624;15624;15873;15876;15877;15878;15880;15882;16129;16130;16131;16132;775', 0),
(8, 'Pembayaran', 'Pembayaran', '512;2816;3072;3328;5376;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128;768', '775', 0),
(9, 'Akuntan', 'Akuntan Baru', '512;768;2816;3072;3328;5376;5632;5888;8192;8448;9216;9472;9728;13312;15616;15872;16128', '513;519;521;523;524;771;772;773;774;775;2817;2818;2819;2820;2821;2822;2823;3073;3074;3082;3075;3076;3077;3078;3079;3080;3081;3329;3330;3331;3332;3333;3334;3335;5377;5633;5634;5635;5637;5638;5639;5640;5889;5890;5891;8193;8194;8196;8197;8449;8450;8451;9217;9218;9220;9473;9474;9475;9476;9729;13313;13314;13315;15617;15618;15619;15620;15621;15622;15623;15624;15628;15625;15626;15627;15630;15873;15874;15875;15876;15877;15878;15879;15880;15883;15881;15882;15884;16129;16130;16131;16132;257;258;259;260;7937;7938;7939;7940;10497;10753;10755;11009;11010;11012', 0),
(10, 'Sub Admin', 'Sub Admin', '512;768;2816;3072;3328;5376;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128', '257;258;259;260;521;523;524;771;772;773;774;2818;2819;2820;2821;2822;2823;3073;3074;3082;3075;3076;3077;3078;3079;3080;3081;3329;3330;3331;3332;3333;3334;3335;5377;5633;5634;5635;5637;5638;5639;5640;5889;5890;5891;7937;7938;7939;7940;8193;8194;8196;8197;8449;8450;8451;10497;10753;10755;11009;11010;11012;13057;13313;13315;15617;15619;15620;15621;15624;15873;15874;15876;15877;15878;15879;15880;15882;16129;16130;16131;16132;775', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_shippers`
--

CREATE TABLE `0_shippers` (
  `shipper_id` int(11) NOT NULL,
  `shipper_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone2` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_shippers`
--

INSERT INTO `0_shippers` (`shipper_id`, `shipper_name`, `phone`, `phone2`, `contact`, `address`, `inactive`) VALUES
(1, 'Default', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_sql_trail`
--

CREATE TABLE `0_sql_trail` (
  `id` int(11) UNSIGNED NOT NULL,
  `sql` text COLLATE utf8_unicode_ci NOT NULL,
  `result` tinyint(1) NOT NULL,
  `msg` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_stock_category`
--

CREATE TABLE `0_stock_category` (
  `category_id` int(11) NOT NULL,
  `description` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dflt_tax_type` int(11) NOT NULL DEFAULT '1',
  `dflt_units` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'each',
  `dflt_mb_flag` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'B',
  `dflt_sales_act` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dflt_cogs_act` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dflt_inventory_act` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dflt_adjustment_act` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dflt_wip_act` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dflt_dim1` int(11) DEFAULT NULL,
  `dflt_dim2` int(11) DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `dflt_no_sale` tinyint(1) NOT NULL DEFAULT '0',
  `dflt_no_purchase` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_stock_fa_class`
--

CREATE TABLE `0_stock_fa_class` (
  `fa_class_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `long_description` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `depreciation_rate` double NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_stock_fa_class`
--

INSERT INTO `0_stock_fa_class` (`fa_class_id`, `parent_id`, `description`, `long_description`, `depreciation_rate`, `inactive`) VALUES
('1.0', '1', 'Penyusutan', '', 1, 0),
('2', '', 'Tanpa Penyusutan', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_stock_master`
--

CREATE TABLE `0_stock_master` (
  `stock_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `tax_type_id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `long_description` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `units` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'each',
  `mb_flag` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'B',
  `sales_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cogs_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inventory_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `adjustment_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `wip_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dimension_id` int(11) DEFAULT NULL,
  `dimension2_id` int(11) DEFAULT NULL,
  `purchase_cost` double NOT NULL DEFAULT '0',
  `material_cost` double NOT NULL DEFAULT '0',
  `labour_cost` double NOT NULL DEFAULT '0',
  `overhead_cost` double NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `no_sale` tinyint(1) NOT NULL DEFAULT '0',
  `no_purchase` tinyint(1) NOT NULL DEFAULT '0',
  `editable` tinyint(1) NOT NULL DEFAULT '0',
  `depreciation_method` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `depreciation_rate` double NOT NULL DEFAULT '0',
  `depreciation_factor` double NOT NULL DEFAULT '0',
  `depreciation_start` date NOT NULL DEFAULT '0000-00-00',
  `depreciation_date` date NOT NULL DEFAULT '0000-00-00',
  `fa_class_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cip` tinyint(4) NOT NULL,
  `cipamount` double(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_stock_moves`
--

CREATE TABLE `0_stock_moves` (
  `trans_id` int(11) NOT NULL,
  `trans_no` int(11) NOT NULL DEFAULT '0',
  `stock_id` char(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` smallint(6) NOT NULL DEFAULT '0',
  `loc_code` char(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tran_date` date DEFAULT NULL,
  `price` double NOT NULL DEFAULT '0',
  `reference` char(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `qty` double NOT NULL DEFAULT '1',
  `standard_cost` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_suppliers`
--

CREATE TABLE `0_suppliers` (
  `supplier_id` int(11) NOT NULL,
  `supp_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `supp_ref` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `supp_address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `gst_no` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `supp_account_no` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `website` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bank_account` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `curr_code` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_terms` int(11) DEFAULT NULL,
  `tax_included` tinyint(1) NOT NULL DEFAULT '0',
  `dimension_id` int(11) DEFAULT '0',
  `dimension2_id` int(11) DEFAULT '0',
  `tax_group_id` int(11) DEFAULT NULL,
  `credit_limit` double NOT NULL DEFAULT '0',
  `purchase_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payable_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payment_discount_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `notes` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_supp_allocations`
--

CREATE TABLE `0_supp_allocations` (
  `id` int(11) NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  `amt` double UNSIGNED DEFAULT NULL,
  `date_alloc` date NOT NULL DEFAULT '0000-00-00',
  `trans_no_from` int(11) DEFAULT NULL,
  `trans_type_from` int(11) DEFAULT NULL,
  `trans_no_to` int(11) DEFAULT NULL,
  `trans_type_to` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_supp_invoice_items`
--

CREATE TABLE `0_supp_invoice_items` (
  `id` int(11) NOT NULL,
  `supp_trans_no` int(11) DEFAULT NULL,
  `supp_trans_type` int(11) DEFAULT NULL,
  `gl_code` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `grn_item_id` int(11) DEFAULT NULL,
  `po_detail_item_id` int(11) DEFAULT NULL,
  `stock_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` tinytext COLLATE utf8_unicode_ci,
  `quantity` double NOT NULL DEFAULT '0',
  `unit_price` double NOT NULL DEFAULT '0',
  `unit_tax` double NOT NULL DEFAULT '0',
  `memo_` tinytext COLLATE utf8_unicode_ci,
  `dimension_id` int(11) NOT NULL DEFAULT '0',
  `dimension2_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_supp_trans`
--

CREATE TABLE `0_supp_trans` (
  `trans_no` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `type` smallint(6) UNSIGNED NOT NULL DEFAULT '0',
  `supplier_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `reference` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `supp_reference` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `due_date` date NOT NULL DEFAULT '0000-00-00',
  `ov_amount` double NOT NULL DEFAULT '0',
  `ov_discount` double NOT NULL DEFAULT '0',
  `ov_gst` double NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '1',
  `alloc` double NOT NULL DEFAULT '0',
  `tax_included` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_sys_prefs`
--

CREATE TABLE `0_sys_prefs` (
  `name` varchar(35) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `category` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `length` smallint(6) DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_sys_prefs`
--

INSERT INTO `0_sys_prefs` (`name`, `category`, `type`, `length`, `value`) VALUES
('accounts_alpha', 'glsetup.general', 'tinyint', 1, '0'),
('accumulate_shipping', 'glsetup.customer', 'tinyint', 1, '0'),
('add_pct', 'setup.company', 'int', 5, '-1'),
('allow_negative_prices', 'glsetup.inventory', 'tinyint', 1, '1'),
('allow_negative_stock', 'glsetup.inventory', 'tinyint', 1, '1'),
('alternative_tax_include_on_docs', 'setup.company', 'tinyint', 1, '1'),
('auto_curr_reval', 'setup.company', 'smallint', 6, ''),
('bank_charge_act', 'glsetup.general', 'varchar', 15, '51002'),
('base_sales', 'setup.company', 'int', 11, '1'),
('bcc_email', 'setup.company', 'varchar', 100, ''),
('coy_logo', 'setup.company', 'varchar', 100, 'logo_frontaccounting.jpg'),
('coy_name', 'setup.company', 'varchar', 60, 'PT. Kresna Asset Management'),
('coy_no', 'setup.company', 'varchar', 25, ''),
('creditors_act', 'glsetup.purchase', 'varchar', 15, '11401'),
('curr_default', 'setup.company', 'char', 3, 'IDR'),
('debtors_act', 'glsetup.sales', 'varchar', 15, '11401'),
('default_adj_act', 'glsetup.items', 'varchar', 15, '89999'),
('default_cogs_act', 'glsetup.items', 'varchar', 15, '11401'),
('default_credit_limit', 'glsetup.customer', 'int', 11, '1000000'),
('default_delivery_required', 'glsetup.sales', 'smallint', 6, '1'),
('default_dim_required', 'glsetup.dims', 'int', 11, '20'),
('default_inv_sales_act', 'glsetup.items', 'varchar', 15, '11401'),
('default_inventory_act', 'glsetup.items', 'varchar', 15, '89999'),
('default_loss_on_asset_disposal_act', 'glsetup.items', 'varchar', 15, '66001'),
('default_prompt_payment_act', 'glsetup.sales', 'varchar', 15, '89999'),
('default_quote_valid_days', 'glsetup.sales', 'smallint', 6, '30'),
('default_receival_required', 'glsetup.purchase', 'smallint', 6, '10'),
('default_sales_act', 'glsetup.sales', 'varchar', 15, ''),
('default_sales_discount_act', 'glsetup.sales', 'varchar', 15, '99999'),
('default_wip_act', 'glsetup.items', 'varchar', 15, '11401'),
('default_workorder_required', 'glsetup.manuf', 'int', 11, '20'),
('deferred_income_act', 'glsetup.sales', 'varchar', 15, ''),
('depreciation_period', 'glsetup.company', 'tinyint', 1, '0'),
('domicile', 'setup.company', 'varchar', 55, 'Jakarta'),
('email', 'setup.company', 'varchar', 100, 'jakarta2@kresnainsurance.com'),
('exchange_diff_act', 'glsetup.general', 'varchar', 15, '39999'),
('f_year', 'setup.company', 'int', 11, '3'),
('fax', 'setup.company', 'varchar', 30, '(021) 6531 1160/61 '),
('freight_act', 'glsetup.customer', 'varchar', 15, '99999'),
('gl_closing_date', 'setup.closing_date', 'date', 8, '2016-12-31'),
('grn_clearing_act', 'glsetup.purchase', 'varchar', 15, ''),
('gst_no', 'setup.company', 'varchar', 25, ''),
('legal_text', 'glsetup.customer', 'tinytext', 0, ''),
('loc_notification', 'glsetup.inventory', 'tinyint', 1, ''),
('login_tout', 'setup.company', 'smallint', 6, '80000'),
('no_customer_list', 'setup.company', 'tinyint', 1, '1'),
('no_item_list', 'setup.company', 'tinyint', 1, '1'),
('no_supplier_list', 'setup.company', 'tinyint', 1, '1'),
('no_zero_lines_amount', 'glsetup.sales', 'tinyint', 1, '1'),
('past_due_days', 'glsetup.general', 'int', 11, '30'),
('phone', 'setup.company', 'varchar', 30, '(021) 2939 1941'),
('po_over_charge', 'glsetup.purchase', 'int', 11, '10'),
('po_over_receive', 'glsetup.purchase', 'int', 11, '10'),
('postal_address', 'setup.company', 'tinytext', 0, 'Jl. Widya Chandra V, RT.5/RW.1, Senayan, Kby. Baru, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12190'),
('print_invoice_no', 'glsetup.sales', 'tinyint', 1, '0'),
('print_item_images_on_quote', 'glsetup.inventory', 'tinyint', 1, ''),
('profit_loss_year_act', 'glsetup.general', 'varchar', 15, '99999'),
('pyt_discount_act', 'glsetup.purchase', 'varchar', 15, '89999'),
('retained_earnings_act', 'glsetup.general', 'varchar', 15, '38000'),
('round_to', 'setup.company', 'int', 5, '1'),
('show_po_item_codes', 'glsetup.purchase', 'tinyint', 1, ''),
('suppress_tax_rates', 'setup.company', 'tinyint', 1, '1'),
('tax_algorithm', 'glsetup.customer', 'tinyint', 1, '1'),
('tax_last', 'setup.company', 'int', 11, '1'),
('tax_prd', 'setup.company', 'int', 11, '1'),
('time_zone', 'setup.company', 'tinyint', 1, '1'),
('use_dimension', 'setup.company', 'tinyint', 1, '2'),
('use_fixed_assets', 'setup.company', 'tinyint', 1, '1'),
('use_manufacturing', 'setup.company', 'tinyint', 1, ''),
('version_id', 'system', 'varchar', 11, '2.4.1');

-- --------------------------------------------------------

--
-- Table structure for table `0_sys_types`
--

CREATE TABLE `0_sys_types` (
  `type_id` smallint(6) NOT NULL DEFAULT '0',
  `type_no` int(11) NOT NULL DEFAULT '1',
  `next_reference` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_sys_types`
--

INSERT INTO `0_sys_types` (`type_id`, `type_no`, `next_reference`) VALUES
(0, 17, '1'),
(1, 7, '1'),
(2, 4, '1'),
(4, 3, '1'),
(10, 16, '1'),
(11, 2, '1'),
(12, 6, '1'),
(13, 1, '1'),
(16, 2, '1'),
(17, 2, '1'),
(18, 1, '1'),
(20, 6, '1'),
(21, 1, '1'),
(22, 3, '1'),
(25, 1, '1'),
(26, 1, '1'),
(28, 1, '1'),
(29, 1, '1'),
(30, 0, '1'),
(32, 0, '1'),
(35, 1, '1'),
(40, 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `0_tags`
--

CREATE TABLE `0_tags` (
  `id` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_tag_associations`
--

CREATE TABLE `0_tag_associations` (
  `record_id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_tax_groups`
--

CREATE TABLE `0_tax_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_tax_groups`
--

INSERT INTO `0_tax_groups` (`id`, `name`, `inactive`) VALUES
(1, 'PPN dan PPH Ps 23', 0),
(2, 'Tax Exempt', 0),
(3, 'PPH Ps 23', 0),
(4, 'PPN', 0),
(5, 'PPN dan PPh Ps 4(2)', 0),
(6, 'PPN dan PPh Ps 21', 1),
(7, 'PPN dan PPH Ps 26', 0),
(8, 'PPH Ps 21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_tax_group_items`
--

CREATE TABLE `0_tax_group_items` (
  `tax_group_id` int(11) NOT NULL DEFAULT '0',
  `tax_type_id` int(11) NOT NULL DEFAULT '0',
  `tax_shipping` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_tax_group_items`
--

INSERT INTO `0_tax_group_items` (`tax_group_id`, `tax_type_id`, `tax_shipping`) VALUES
(1, 1, 0),
(1, 2, 0),
(3, 2, 0),
(4, 1, 0),
(5, 1, 0),
(5, 3, 0),
(6, 1, 0),
(6, 4, 0),
(7, 1, 0),
(7, 5, 0),
(8, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_tax_jasa`
--

CREATE TABLE `0_tax_jasa` (
  `id` int(11) NOT NULL,
  `tax_type_id` int(11) NOT NULL,
  `jenis` varchar(100) NOT NULL,
  `tarif` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_tax_jasa`
--

INSERT INTO `0_tax_jasa` (`id`, `tax_type_id`, `jenis`, `tarif`) VALUES
(1, 2, 'Sewa dan Penghasilan lain sehubungan dengan penggunaan harta', 10),
(2, 2, 'Obligasi', 10),
(3, 2, 'Pialang', 10),
(4, 2, 'Jasa Manajemen', 10),
(5, 2, 'Jasa Software', 10),
(6, 2, 'Jasa Profesional', 10),
(7, 2, 'Media Masa', 10),
(8, 2, 'Instalasi & Pemasangan', 10),
(9, 2, 'Sewa dan Penghasilan lain sehubungan dengan penggunaan harta', 10),
(10, 2, 'Jasa Penyelenggaraan Kegiatan', 10),
(11, 2, 'Perawatan/Perbaikan/Pemeliharaan mesin/peralatan', 10),
(12, 2, 'Jasa Informasi', 10),
(13, 2, 'Jasa Percetakan', 10),
(14, 2, 'Jasa Internet', 10),
(15, 2, 'Jasa Penyediaan Tempat dan/ waktu dalam media masa', 10),
(16, 3, 'Rental', 10),
(17, 3, 'Service Charge', 10),
(18, 3, 'Chilled Water', 10),
(19, 3, 'AC', 10),
(20, 3, 'Listrik', 10),
(21, 3, 'Allocation Car Park Reserved', 10),
(22, 3, 'Dividen', 10),
(23, 3, 'Renovasi', 10),
(24, 2, 'Jasa Pembuatan Website', 10),
(25, 2, 'Jasa Outsource', 10),
(26, 2, 'Jasa Pelatihan', 10),
(27, 3, 'Hadiah', 25),
(28, 3, 'Sewa Lapangan', 10),
(29, 0, 'Pembayaran Pajak', 10),
(30, 3, 'Barang Elektronik', 10);

-- --------------------------------------------------------

--
-- Table structure for table `0_tax_types`
--

CREATE TABLE `0_tax_types` (
  `id` int(11) NOT NULL,
  `rate` double NOT NULL DEFAULT '0',
  `sales_gl_code` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `purchasing_gl_code` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `wth` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_tax_types`
--

INSERT INTO `0_tax_types` (`id`, `rate`, `sales_gl_code`, `purchasing_gl_code`, `name`, `inactive`, `wth`) VALUES
(1, 10, '21240', '11703', 'PPN 10%', 0, 0),
(2, 2, '11510', '21220', 'Prepaid PPh 23', 0, 1),
(3, 10, '11520', '21230', 'PPh Ps 4(2)', 0, 1),
(4, 2.5, '11540', '21210', 'PPh Ps 21', 0, 1),
(5, 20, '21270', '21270', 'PPH Ps 26', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `0_trans_tax_details`
--

CREATE TABLE `0_trans_tax_details` (
  `id` int(11) NOT NULL,
  `trans_type` smallint(6) DEFAULT NULL,
  `trans_no` int(11) DEFAULT NULL,
  `tran_date` date NOT NULL,
  `tax_type_id` int(11) NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  `ex_rate` double NOT NULL DEFAULT '1',
  `included_in_price` tinyint(1) NOT NULL DEFAULT '0',
  `net_amount` double NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `memo` tinytext COLLATE utf8_unicode_ci,
  `reg_type` tinyint(1) DEFAULT NULL,
  `jasa` int(11) DEFAULT NULL,
  `fp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_useronline`
--

CREATE TABLE `0_useronline` (
  `id` int(11) NOT NULL,
  `timestamp` int(15) NOT NULL DEFAULT '0',
  `ip` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `file` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_users`
--

CREATE TABLE `0_users` (
  `id` smallint(6) NOT NULL,
  `user_id` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `real_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `role_id` int(11) NOT NULL DEFAULT '1',
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_format` tinyint(1) NOT NULL DEFAULT '0',
  `date_sep` tinyint(1) NOT NULL DEFAULT '0',
  `tho_sep` tinyint(1) NOT NULL DEFAULT '0',
  `dec_sep` tinyint(1) NOT NULL DEFAULT '0',
  `theme` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `page_size` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A4',
  `prices_dec` smallint(6) NOT NULL DEFAULT '2',
  `qty_dec` smallint(6) NOT NULL DEFAULT '2',
  `rates_dec` smallint(6) NOT NULL DEFAULT '4',
  `percent_dec` smallint(6) NOT NULL DEFAULT '1',
  `show_gl` tinyint(1) NOT NULL DEFAULT '1',
  `show_codes` tinyint(1) NOT NULL DEFAULT '0',
  `show_hints` tinyint(1) NOT NULL DEFAULT '0',
  `last_visit_date` datetime DEFAULT NULL,
  `query_size` tinyint(1) UNSIGNED NOT NULL DEFAULT '10',
  `graphic_links` tinyint(1) DEFAULT '1',
  `pos` smallint(6) DEFAULT '1',
  `print_profile` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `rep_popup` tinyint(1) DEFAULT '1',
  `sticky_doc_date` tinyint(1) DEFAULT '0',
  `startup_tab` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `transaction_days` int(6) NOT NULL DEFAULT '30' COMMENT 'Transaction days',
  `save_report_selections` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Save Report Selection Days',
  `use_date_picker` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Use Date Picker for all Date Values',
  `def_print_destination` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Default Report Destination',
  `def_print_orientation` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Default Report Orientation',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_users`
--

INSERT INTO `0_users` (`id`, `user_id`, `password`, `real_name`, `role_id`, `phone`, `email`, `language`, `date_format`, `date_sep`, `tho_sep`, `dec_sep`, `theme`, `page_size`, `prices_dec`, `qty_dec`, `rates_dec`, `percent_dec`, `show_gl`, `show_codes`, `show_hints`, `last_visit_date`, `query_size`, `graphic_links`, `pos`, `print_profile`, `rep_popup`, `sticky_doc_date`, `startup_tab`, `transaction_days`, `save_report_selections`, `use_date_picker`, `def_print_destination`, `def_print_orientation`, `inactive`) VALUES
(1, 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', 'Admin', 2, '085780886610', 'ade3187@praisindo.com', 'C', 1, 0, 1, 1, 'default', 'Letter', 2, 2, 2, 0, 1, 0, 1, '2018-07-11 22:15:37', 10, 1, 1, '', 1, 0, 'GL', 30, 0, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_voided`
--

CREATE TABLE `0_voided` (
  `type` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL DEFAULT '0',
  `date_` date NOT NULL DEFAULT '0000-00-00',
  `memo_` tinytext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_workcentres`
--

CREATE TABLE `0_workcentres` (
  `id` int(11) NOT NULL,
  `name` char(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_workorders`
--

CREATE TABLE `0_workorders` (
  `id` int(11) NOT NULL,
  `wo_ref` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `loc_code` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `units_reqd` double NOT NULL DEFAULT '1',
  `stock_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `date_` date NOT NULL DEFAULT '0000-00-00',
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `required_by` date NOT NULL DEFAULT '0000-00-00',
  `released_date` date NOT NULL DEFAULT '0000-00-00',
  `units_issued` double NOT NULL DEFAULT '0',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `released` tinyint(1) NOT NULL DEFAULT '0',
  `additional_costs` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_wo_costing`
--

CREATE TABLE `0_wo_costing` (
  `id` int(11) NOT NULL,
  `workorder_id` int(11) NOT NULL DEFAULT '0',
  `cost_type` tinyint(1) NOT NULL DEFAULT '0',
  `trans_type` int(11) NOT NULL DEFAULT '0',
  `trans_no` int(11) NOT NULL DEFAULT '0',
  `factor` double NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_wo_issues`
--

CREATE TABLE `0_wo_issues` (
  `issue_no` int(11) NOT NULL,
  `workorder_id` int(11) NOT NULL DEFAULT '0',
  `reference` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `loc_code` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `workcentre_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_wo_issue_items`
--

CREATE TABLE `0_wo_issue_items` (
  `id` int(11) NOT NULL,
  `stock_id` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `issue_id` int(11) DEFAULT NULL,
  `qty_issued` double DEFAULT NULL,
  `unit_cost` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_wo_manufacture`
--

CREATE TABLE `0_wo_manufacture` (
  `id` int(11) NOT NULL,
  `reference` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `workorder_id` int(11) NOT NULL DEFAULT '0',
  `quantity` double NOT NULL DEFAULT '0',
  `date_` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_wo_requirements`
--

CREATE TABLE `0_wo_requirements` (
  `id` int(11) NOT NULL,
  `workorder_id` int(11) NOT NULL DEFAULT '0',
  `stock_id` char(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `workcentre` int(11) NOT NULL DEFAULT '0',
  `units_req` double NOT NULL DEFAULT '1',
  `unit_cost` double NOT NULL DEFAULT '0',
  `loc_code` char(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `units_issued` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `0_areas`
--
ALTER TABLE `0_areas`
  ADD PRIMARY KEY (`area_code`),
  ADD UNIQUE KEY `description` (`description`);

--
-- Indexes for table `0_attachments`
--
ALTER TABLE `0_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_no` (`type_no`,`trans_no`);

--
-- Indexes for table `0_audit_trail`
--
ALTER TABLE `0_audit_trail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Seq` (`fiscal_year`,`gl_date`,`gl_seq`),
  ADD KEY `Type_and_Number` (`type`,`trans_no`);

--
-- Indexes for table `0_bank_accounts`
--
ALTER TABLE `0_bank_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bank_account_name` (`bank_account_name`),
  ADD KEY `bank_account_number` (`bank_account_number`),
  ADD KEY `account_code` (`account_code`);

--
-- Indexes for table `0_bank_trans`
--
ALTER TABLE `0_bank_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bank_act` (`bank_act`,`ref`),
  ADD KEY `type` (`type`,`trans_no`),
  ADD KEY `bank_act_2` (`bank_act`,`reconciled`),
  ADD KEY `bank_act_3` (`bank_act`,`trans_date`);

--
-- Indexes for table `0_bom`
--
ALTER TABLE `0_bom`
  ADD PRIMARY KEY (`parent`,`component`,`workcentre_added`,`loc_code`),
  ADD KEY `component` (`component`),
  ADD KEY `id` (`id`),
  ADD KEY `loc_code` (`loc_code`),
  ADD KEY `parent` (`parent`,`loc_code`),
  ADD KEY `workcentre_added` (`workcentre_added`);

--
-- Indexes for table `0_budget_trans`
--
ALTER TABLE `0_budget_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Account` (`account`,`tran_date`,`dimension_id`,`dimension2_id`);

--
-- Indexes for table `0_chart_class`
--
ALTER TABLE `0_chart_class`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `0_chart_master`
--
ALTER TABLE `0_chart_master`
  ADD PRIMARY KEY (`account_code`),
  ADD KEY `account_name` (`account_name`),
  ADD KEY `accounts_by_type` (`account_type`,`account_code`);

--
-- Indexes for table `0_chart_types`
--
ALTER TABLE `0_chart_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `class_id` (`class_id`);

--
-- Indexes for table `0_comments`
--
ALTER TABLE `0_comments`
  ADD KEY `type_and_id` (`type`,`id`);

--
-- Indexes for table `0_credit_status`
--
ALTER TABLE `0_credit_status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reason_description` (`reason_description`);

--
-- Indexes for table `0_crm_categories`
--
ALTER TABLE `0_crm_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`,`action`),
  ADD UNIQUE KEY `type_2` (`type`,`name`);

--
-- Indexes for table `0_crm_contacts`
--
ALTER TABLE `0_crm_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`,`action`);

--
-- Indexes for table `0_crm_persons`
--
ALTER TABLE `0_crm_persons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ref` (`ref`);

--
-- Indexes for table `0_currencies`
--
ALTER TABLE `0_currencies`
  ADD PRIMARY KEY (`curr_abrev`);

--
-- Indexes for table `0_cust_allocations`
--
ALTER TABLE `0_cust_allocations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `person_id` (`person_id`,`trans_type_from`,`trans_no_from`,`trans_type_to`,`trans_no_to`),
  ADD KEY `From` (`trans_type_from`,`trans_no_from`),
  ADD KEY `To` (`trans_type_to`,`trans_no_to`);

--
-- Indexes for table `0_cust_branch`
--
ALTER TABLE `0_cust_branch`
  ADD PRIMARY KEY (`branch_code`,`debtor_no`),
  ADD KEY `branch_ref` (`branch_ref`),
  ADD KEY `group_no` (`group_no`);

--
-- Indexes for table `0_debtors_master`
--
ALTER TABLE `0_debtors_master`
  ADD PRIMARY KEY (`debtor_no`),
  ADD UNIQUE KEY `debtor_ref` (`debtor_ref`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `0_debtor_trans`
--
ALTER TABLE `0_debtor_trans`
  ADD PRIMARY KEY (`type`,`trans_no`,`debtor_no`),
  ADD KEY `debtor_no` (`debtor_no`,`branch_code`),
  ADD KEY `tran_date` (`tran_date`);

--
-- Indexes for table `0_debtor_trans_details`
--
ALTER TABLE `0_debtor_trans_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Transaction` (`debtor_trans_type`,`debtor_trans_no`),
  ADD KEY `src_id` (`src_id`);

--
-- Indexes for table `0_dimensions`
--
ALTER TABLE `0_dimensions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reference` (`reference`),
  ADD KEY `date_` (`date_`),
  ADD KEY `due_date` (`due_date`),
  ADD KEY `type_` (`type_`);

--
-- Indexes for table `0_dimension_link`
--
ALTER TABLE `0_dimension_link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_exchange_rates`
--
ALTER TABLE `0_exchange_rates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `curr_code` (`curr_code`,`date_`);

--
-- Indexes for table `0_fiscal_year`
--
ALTER TABLE `0_fiscal_year`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `begin` (`begin`),
  ADD UNIQUE KEY `end` (`end`);

--
-- Indexes for table `0_fp_trans`
--
ALTER TABLE `0_fp_trans`
  ADD PRIMARY KEY (`id`,`type`),
  ADD KEY `Type_and_Reference` (`type`,`reference`);

--
-- Indexes for table `0_gl_trans`
--
ALTER TABLE `0_gl_trans`
  ADD PRIMARY KEY (`counter`),
  ADD KEY `Type_and_Number` (`type`,`type_no`),
  ADD KEY `dimension_id` (`dimension_id`),
  ADD KEY `dimension2_id` (`dimension2_id`),
  ADD KEY `tran_date` (`tran_date`),
  ADD KEY `account_and_tran_date` (`account`,`tran_date`);

--
-- Indexes for table `0_grn_batch`
--
ALTER TABLE `0_grn_batch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_date` (`delivery_date`),
  ADD KEY `purch_order_no` (`purch_order_no`);

--
-- Indexes for table `0_grn_items`
--
ALTER TABLE `0_grn_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grn_batch_id` (`grn_batch_id`);

--
-- Indexes for table `0_groups`
--
ALTER TABLE `0_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `description` (`description`);

--
-- Indexes for table `0_item_codes`
--
ALTER TABLE `0_item_codes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `stock_id` (`stock_id`,`item_code`),
  ADD KEY `item_code` (`item_code`);

--
-- Indexes for table `0_item_tax_types`
--
ALTER TABLE `0_item_tax_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `0_item_tax_type_exemptions`
--
ALTER TABLE `0_item_tax_type_exemptions`
  ADD PRIMARY KEY (`item_tax_type_id`,`tax_type_id`);

--
-- Indexes for table `0_item_units`
--
ALTER TABLE `0_item_units`
  ADD PRIMARY KEY (`abbr`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `0_journal`
--
ALTER TABLE `0_journal`
  ADD PRIMARY KEY (`type`,`trans_no`),
  ADD KEY `tran_date` (`tran_date`);

--
-- Indexes for table `0_locations`
--
ALTER TABLE `0_locations`
  ADD PRIMARY KEY (`loc_code`);

--
-- Indexes for table `0_loc_stock`
--
ALTER TABLE `0_loc_stock`
  ADD PRIMARY KEY (`loc_code`,`stock_id`),
  ADD KEY `stock_id` (`stock_id`);

--
-- Indexes for table `0_marketing_trans`
--
ALTER TABLE `0_marketing_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_mkbd`
--
ALTER TABLE `0_mkbd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_mkbd_coa`
--
ALTER TABLE `0_mkbd_coa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_no_faktur`
--
ALTER TABLE `0_no_faktur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_payment_terms`
--
ALTER TABLE `0_payment_terms`
  ADD PRIMARY KEY (`terms_indicator`),
  ADD UNIQUE KEY `terms` (`terms`);

--
-- Indexes for table `0_prices`
--
ALTER TABLE `0_prices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `price` (`stock_id`,`sales_type_id`,`curr_abrev`);

--
-- Indexes for table `0_printers`
--
ALTER TABLE `0_printers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `0_print_profiles`
--
ALTER TABLE `0_print_profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profile` (`profile`,`report`);

--
-- Indexes for table `0_purch_data`
--
ALTER TABLE `0_purch_data`
  ADD PRIMARY KEY (`supplier_id`,`stock_id`);

--
-- Indexes for table `0_purch_orders`
--
ALTER TABLE `0_purch_orders`
  ADD PRIMARY KEY (`order_no`),
  ADD KEY `ord_date` (`ord_date`);

--
-- Indexes for table `0_purch_order_details`
--
ALTER TABLE `0_purch_order_details`
  ADD PRIMARY KEY (`po_detail_item`),
  ADD KEY `order` (`order_no`,`po_detail_item`),
  ADD KEY `itemcode` (`item_code`);

--
-- Indexes for table `0_quick_entries`
--
ALTER TABLE `0_quick_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `description` (`description`);

--
-- Indexes for table `0_quick_entry_lines`
--
ALTER TABLE `0_quick_entry_lines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `qid` (`qid`);

--
-- Indexes for table `0_recurrent_invoices`
--
ALTER TABLE `0_recurrent_invoices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `description` (`description`);

--
-- Indexes for table `0_reflines`
--
ALTER TABLE `0_reflines`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `prefix` (`trans_type`,`prefix`);

--
-- Indexes for table `0_refs`
--
ALTER TABLE `0_refs`
  ADD PRIMARY KEY (`id`,`type`),
  ADD KEY `Type_and_Reference` (`type`,`reference`);

--
-- Indexes for table `0_salesman`
--
ALTER TABLE `0_salesman`
  ADD PRIMARY KEY (`salesman_code`),
  ADD UNIQUE KEY `salesman_name` (`salesman_name`);

--
-- Indexes for table `0_sales_orders`
--
ALTER TABLE `0_sales_orders`
  ADD PRIMARY KEY (`trans_type`,`order_no`);

--
-- Indexes for table `0_sales_order_details`
--
ALTER TABLE `0_sales_order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sorder` (`trans_type`,`order_no`),
  ADD KEY `stkcode` (`stk_code`);

--
-- Indexes for table `0_sales_pos`
--
ALTER TABLE `0_sales_pos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pos_name` (`pos_name`);

--
-- Indexes for table `0_sales_types`
--
ALTER TABLE `0_sales_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sales_type` (`sales_type`);

--
-- Indexes for table `0_security_roles`
--
ALTER TABLE `0_security_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role` (`role`);

--
-- Indexes for table `0_shippers`
--
ALTER TABLE `0_shippers`
  ADD PRIMARY KEY (`shipper_id`),
  ADD UNIQUE KEY `name` (`shipper_name`);

--
-- Indexes for table `0_sql_trail`
--
ALTER TABLE `0_sql_trail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_stock_category`
--
ALTER TABLE `0_stock_category`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `description` (`description`);

--
-- Indexes for table `0_stock_fa_class`
--
ALTER TABLE `0_stock_fa_class`
  ADD PRIMARY KEY (`fa_class_id`);

--
-- Indexes for table `0_stock_master`
--
ALTER TABLE `0_stock_master`
  ADD PRIMARY KEY (`stock_id`);

--
-- Indexes for table `0_stock_moves`
--
ALTER TABLE `0_stock_moves`
  ADD PRIMARY KEY (`trans_id`),
  ADD KEY `type` (`type`,`trans_no`),
  ADD KEY `Move` (`stock_id`,`loc_code`,`tran_date`);

--
-- Indexes for table `0_suppliers`
--
ALTER TABLE `0_suppliers`
  ADD PRIMARY KEY (`supplier_id`),
  ADD KEY `supp_ref` (`supp_ref`);

--
-- Indexes for table `0_supp_allocations`
--
ALTER TABLE `0_supp_allocations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `person_id` (`person_id`,`trans_type_from`,`trans_no_from`,`trans_type_to`,`trans_no_to`),
  ADD KEY `From` (`trans_type_from`,`trans_no_from`),
  ADD KEY `To` (`trans_type_to`,`trans_no_to`);

--
-- Indexes for table `0_supp_invoice_items`
--
ALTER TABLE `0_supp_invoice_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Transaction` (`supp_trans_type`,`supp_trans_no`,`stock_id`);

--
-- Indexes for table `0_supp_trans`
--
ALTER TABLE `0_supp_trans`
  ADD PRIMARY KEY (`type`,`trans_no`,`supplier_id`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `tran_date` (`tran_date`);

--
-- Indexes for table `0_sys_prefs`
--
ALTER TABLE `0_sys_prefs`
  ADD PRIMARY KEY (`name`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `0_sys_types`
--
ALTER TABLE `0_sys_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `0_tags`
--
ALTER TABLE `0_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`,`name`);

--
-- Indexes for table `0_tag_associations`
--
ALTER TABLE `0_tag_associations`
  ADD UNIQUE KEY `record_id` (`record_id`,`tag_id`);

--
-- Indexes for table `0_tax_groups`
--
ALTER TABLE `0_tax_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `0_tax_group_items`
--
ALTER TABLE `0_tax_group_items`
  ADD PRIMARY KEY (`tax_group_id`,`tax_type_id`);

--
-- Indexes for table `0_tax_jasa`
--
ALTER TABLE `0_tax_jasa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_tax_types`
--
ALTER TABLE `0_tax_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_trans_tax_details`
--
ALTER TABLE `0_trans_tax_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tran_date` (`tran_date`);

--
-- Indexes for table `0_useronline`
--
ALTER TABLE `0_useronline`
  ADD PRIMARY KEY (`id`),
  ADD KEY `timestamp` (`timestamp`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `0_users`
--
ALTER TABLE `0_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `0_voided`
--
ALTER TABLE `0_voided`
  ADD UNIQUE KEY `id` (`type`,`id`);

--
-- Indexes for table `0_workcentres`
--
ALTER TABLE `0_workcentres`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `0_workorders`
--
ALTER TABLE `0_workorders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `wo_ref` (`wo_ref`);

--
-- Indexes for table `0_wo_costing`
--
ALTER TABLE `0_wo_costing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_wo_issues`
--
ALTER TABLE `0_wo_issues`
  ADD PRIMARY KEY (`issue_no`),
  ADD KEY `workorder_id` (`workorder_id`);

--
-- Indexes for table `0_wo_issue_items`
--
ALTER TABLE `0_wo_issue_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_wo_manufacture`
--
ALTER TABLE `0_wo_manufacture`
  ADD PRIMARY KEY (`id`),
  ADD KEY `workorder_id` (`workorder_id`);

--
-- Indexes for table `0_wo_requirements`
--
ALTER TABLE `0_wo_requirements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `workorder_id` (`workorder_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `0_areas`
--
ALTER TABLE `0_areas`
  MODIFY `area_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `0_attachments`
--
ALTER TABLE `0_attachments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_audit_trail`
--
ALTER TABLE `0_audit_trail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_bank_accounts`
--
ALTER TABLE `0_bank_accounts`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_bank_trans`
--
ALTER TABLE `0_bank_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_bom`
--
ALTER TABLE `0_bom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_budget_trans`
--
ALTER TABLE `0_budget_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_credit_status`
--
ALTER TABLE `0_credit_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `0_crm_categories`
--
ALTER TABLE `0_crm_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'pure technical key', AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `0_crm_contacts`
--
ALTER TABLE `0_crm_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- AUTO_INCREMENT for table `0_crm_persons`
--
ALTER TABLE `0_crm_persons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_cust_allocations`
--
ALTER TABLE `0_cust_allocations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_cust_branch`
--
ALTER TABLE `0_cust_branch`
  MODIFY `branch_code` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_debtors_master`
--
ALTER TABLE `0_debtors_master`
  MODIFY `debtor_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_debtor_trans_details`
--
ALTER TABLE `0_debtor_trans_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_dimensions`
--
ALTER TABLE `0_dimensions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_dimension_link`
--
ALTER TABLE `0_dimension_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_exchange_rates`
--
ALTER TABLE `0_exchange_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_fiscal_year`
--
ALTER TABLE `0_fiscal_year`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `0_gl_trans`
--
ALTER TABLE `0_gl_trans`
  MODIFY `counter` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_grn_batch`
--
ALTER TABLE `0_grn_batch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_marketing_trans`
--
ALTER TABLE `0_marketing_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_mkbd_coa`
--
ALTER TABLE `0_mkbd_coa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_suppliers`
--
ALTER TABLE `0_suppliers`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_tax_jasa`
--
ALTER TABLE `0_tax_jasa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `0_trans_tax_details`
--
ALTER TABLE `0_trans_tax_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
