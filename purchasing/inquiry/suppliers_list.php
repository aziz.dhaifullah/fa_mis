<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
/**********************************************************************
  Page for searching supplier list and select it to supplier selection
  in pages that have the supplier dropdown lists.
  Author: bogeyman2007 from Discussion Forum. Modified by Joe Hunt
***********************************************************************/
$page_security = "SA_PURCHASEORDER";
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/purchasing/includes/db/suppliers_db.inc");

$mode = get_company_pref('no_supplier_list');
if ($mode != 0)
	$js = get_js_set_combo_item();
else
	$js = get_js_select_combo_item();

page(_($help_context = "Suppliers"), true, false, "", $js);

if(get_post("search")) {
  $Ajax->activate("supplier_tbl");
}

start_form(false, false, $_SERVER['PHP_SELF'] . "?" . $_SERVER['QUERY_STRING']);

start_table(TABLESTYLE_NOBORDER);

start_row();

text_cells(_("Supplier"), "supplier");
submit_cells("search", _("Search"), "", _("Search suppliers"), "default");
echo '<td><button onclick="createform()" name="create" id="create" value="Create Supplier" title="Create suppliers"><span>New Supplier</span></button></td>';
echo '<td><button onclick="createform2()" name="upload" id="upload" value="Upload Supplier" title="Upload suppliers"><span>Upload Supplier</span></button></td>';
end_row();

end_table();

end_form();



div_start("supplier_tbl");
if(@get_post("submitsupplier")!=''){
	//echo get_post("supp_name"); 
	add_supplier2(get_post("supp_name"), get_post("address"), get_post("gst_no"));
}
if(@get_post("uplsupplier")!=''){
	include_once ( "../../excel_reader2.php");
	$file=$_FILES["uplfile"]["tmp_name"];
	$data = new Spreadsheet_Excel_Reader($file);
	$j = 0;
	//print_r($data);exit;
	$ttl=0;
	for ($i=2; $i <= ($data->rowcount($sheet_index=0)); $i++){ 
		$nama   = $data->val($i, 1);
		$npwp   = $data->val($i, 2);
		$alamat   = $data->val($i, 3);
		if($nama!=''){
			$result0 = get_suppliers_search(@$nama);
			$myrow0 = db_fetch_assoc($result0);
			if(trim(@$myrow0["supp_name"])=='')
			add_supplier2(@$nama, @$alamat, @$npwp);
			else{
				@$myrow0["supplier_id"];
				update_supplier(@$myrow0["supplier_id"], @$nama, @$myrow0['supp_ref'], @$alamat,
				@$alamat, @$npwp,
				@$myrow0['website'], @$myrow0['supp_account_no'], @$myrow0['bank_account'], 
				0, @$myrow0['dimension_id'], @$myrow0['dimension2_id'], @$myrow0['curr_code'],
				@$myrow0['payment_terms'], @$myrow0['payable_account'], @$myrow0['purchase_account'], @$myrow0['payment_discount_account'],
				@$myrow0['notes'], @$myrow0['tax_group_id'], 0);
			}
		}
	}

	//echo get_post("supp_name"); 
	//add_supplier2(get_post("supp_name"), get_post("address"), get_post("gst_no"));
}
if(@get_post("create")!=''){
	
echo '
<script type="text/javascript">
function createform(){
	document.getElementById("createtable").style.display = "block";
}
</script>
	<center><form method="post"><table id="createtable" class="tablestyle_inner">
	<tbody><tr><td colspan="2" class="tableheader">Basic Data</td></tr>
	<tr><td class="label">Supplier Name:</td><td><input type="text" name="supp_name" size="42" maxlength="40" value=""></td>
	</tr>
	<tr><td class="label">GSTNo:</td><td><input type="text" name="gst_no" size="42" value=""></td>
	</tr>
	<tr><td class="label">Address:</td><td><textarea name="address" cols="35" rows="5"></textarea></td>
	</tr>
	<tr><td class="label"></td><td><input type="submit" name="submitsupplier" value="Save" class="export"></td>
	</tr>
	</tbody></table></center>';
}
if(@get_post("upload")!=''){
	
echo '
<script type="text/javascript">
function createform2(){
	document.getElementById("upltable").style.display = "block";
}
</script>
	<center><form method="post" enctype="multipart/form-data" method="post"><table id="upltable" class="tablestyle_inner">
	<tbody><tr><td colspan="2" class="tableheader">Basic Data</td></tr>
	<tr><td class="label">Pilih File:</td><td><input type="file" name="uplfile" size="42" value=""></td>
	</tr>
	<tr><td class="label"></td><td><input type="submit" name="uplsupplier" value="Upload" class="export"></td>
	</tr>
	</tbody></table></center>';
}
start_table(TABLESTYLE);

$th = array("", _("Supplier"), _("Short Name"), _("Address"), _("Tax ID"));

table_header($th);
$k = 0;
$name = $_GET["client_id"];
$result = get_suppliers_search(get_post("supplier"));
while ($myrow = db_fetch_assoc($result)) {
	alt_table_row_color($k);
	$value = $myrow['supplier_id'];
	if ($mode != 0) {
		$text = $myrow['supp_name'];
  		ahref_cell(_("Select"), 'javascript:void(0)', '', 'setComboItem(window.opener.document, "'.$name.'",  "'.$value.'", "'.$text.'")');
	}
	else {
  		ahref_cell(_("Select"), 'javascript:void(0)', '', 'selectComboItem(window.opener.document, "'.$name.'", "'.$value.'")');
	}
  	label_cell($myrow["supp_name"]);
  	label_cell($myrow["supp_ref"]);
  	label_cell($myrow["address"]);
  	label_cell($myrow["gst_no"]);
	end_row();
}

end_table(1);
div_end();
end_page(true);
