<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_DIMTRANSVIEW';
$path_to_root = "../..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/dimensions/includes/dimensions_db.inc");

//-------------------------------------------------------------------------------------------------
if($_POST['tipe']=='agent')
$isi=get_client($_POST['id']);
else
$isi=get_agent($_POST['id']);
$data=array();
if($result=db_query($isi)){
	while($row=db_fetch($result)){
		$data[]=$row;
	}
}

//$data[] = get_client($_POST['id']);
header('Content-Type: application/json');
echo json_encode($data);



