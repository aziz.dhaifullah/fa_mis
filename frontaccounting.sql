-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2018 at 10:05 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `frontaccounting`
--

-- --------------------------------------------------------

--
-- Table structure for table `0_areas`
--

CREATE TABLE `0_areas` (
  `area_code` int(11) NOT NULL,
  `description` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_areas`
--

INSERT INTO `0_areas` (`area_code`, `description`, `inactive`) VALUES
(1, 'Jakarta', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_attachments`
--

CREATE TABLE `0_attachments` (
  `id` int(11) UNSIGNED NOT NULL,
  `description` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type_no` int(11) NOT NULL DEFAULT '0',
  `trans_no` int(11) NOT NULL DEFAULT '0',
  `unique_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `filename` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `filesize` int(11) NOT NULL DEFAULT '0',
  `filetype` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_audit_trail`
--

CREATE TABLE `0_audit_trail` (
  `id` int(11) NOT NULL,
  `type` smallint(6) UNSIGNED NOT NULL DEFAULT '0',
  `trans_no` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `user` smallint(6) UNSIGNED NOT NULL DEFAULT '0',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fiscal_year` int(11) NOT NULL DEFAULT '0',
  `gl_date` date NOT NULL DEFAULT '0000-00-00',
  `gl_seq` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_bank_accounts`
--

CREATE TABLE `0_bank_accounts` (
  `account_code` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `account_type` smallint(6) NOT NULL DEFAULT '0',
  `bank_account_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bank_account_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bank_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bank_address` tinytext COLLATE utf8_unicode_ci,
  `bank_curr_code` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dflt_curr_act` tinyint(1) NOT NULL DEFAULT '0',
  `id` smallint(6) NOT NULL,
  `bank_charge_act` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_reconciled_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ending_reconcile_balance` double NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `kode` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_bank_accounts`
--

INSERT INTO `0_bank_accounts` (`account_code`, `account_type`, `bank_account_name`, `bank_account_number`, `bank_name`, `bank_address`, `bank_curr_code`, `dflt_curr_act`, `id`, `bank_charge_act`, `last_reconciled_date`, `ending_reconcile_balance`, `inactive`, `kode`) VALUES
('11151', 0, 'Bank Ekonomi-2041862699', '2041862699', 'Bank Ekonomi', '', 'IDR', 0, 1, '99999', '0000-00-00 00:00:00', 0, 1, ''),
('11152', 0, 'Bank Ekonomi-2161889299', '2161889299', 'Bank Ekonomi', '', 'IDR', 0, 2, '51002', '0000-00-00 00:00:00', 0, 0, 'Bo'),
('11153', 0, 'Bank Ekonomi-2161880909', '2161880909', 'Bank Ekonomi', '', 'IDR', 0, 3, '51002', '0000-00-00 00:00:00', 0, 1, ''),
('11154', 0, 'Bank Ekonomi -2165001969', '2165001969', 'Bank Ekonomi ', '', 'USD', 0, 4, '51002', '0000-00-00 00:00:00', 0, 0, 'B$o'),
('11155', 0, 'Bank Maybank - 2.145.258.793', '2.145.258.793', 'Bank BII', '', 'IDR', 0, 5, '51002', '0000-00-00 00:00:00', 0, 0, 'Boi'),
('11156', 0, 'Bank Danamon - 351.8939.883', '351.8939.883', 'Bank Danamon ', '', 'IDR', 0, 6, '51002', '0000-00-00 00:00:00', 0, 0, 'Bo3'),
('11157', 0, 'Bank Permata - 701.316.290', '701.316.290', 'Bank Permata', '', 'IDR', 0, 7, '51002', '0000-00-00 00:00:00', 0, 0, 'Bo4'),
('11158', 0, 'Permata Maxima 701.316.142', '701.316.142', 'Permata Maxima', '', 'IDR', 0, 8, '51002', '0000-00-00 00:00:00', 0, 0, 'Bo5'),
('11159', 0, 'Bank Ekonomi -2163812099', '2163812099', 'Bank Ekonomi ', '', 'IDR', 0, 9, '51002', '0000-00-00 00:00:00', 0, 0, 'Bot'),
('11160', 0, 'Taseto BPTN', '', 'Taseto BPTN', '', 'IDR', 0, 10, '51002', '0000-00-00 00:00:00', 0, 0, 'Bo6'),
('11161', 0, 'CIMB Niaga 9250100851000', '9250100851000', 'CIMB Niaga', '', 'IDR', 0, 11, '51002', '0000-00-00 00:00:00', 0, 0, 'Bo7'),
('11162', 0, 'Bank Mega', '', 'Bank Mega', '', 'USD', 0, 12, '51002', '0000-00-00 00:00:00', 0, 1, 'Bmo'),
('11163', 0, 'Bank BRI 0671.01.00276309', '0671.01.00276309', 'Bank BRI ', '', 'IDR', 0, 13, '51002', '0000-00-00 00:00:00', 0, 0, 'Bro'),
('11164', 0, 'Bank Mega 01.074.001123.897.6', '01.074.001123.897.6', 'Bank Mega ', '', 'IDR', 0, 14, '51002', '0000-00-00 00:00:00', 0, 0, 'Bmo'),
('11111', 3, 'Petty Cash - Kas Kecil', '', 'Petty Cash - Kas Kecil', '', 'IDR', 0, 15, '99999', '0000-00-00 00:00:00', 0, 0, 'Pco'),
('11140', 3, 'Petty Cash - Kas Besar', '', 'Petty Cash - Kas Besar', '', 'IDR', 0, 16, '99999', '0000-00-00 00:00:00', 0, 0, 'Pc2'),
('11112', 3, 'Petty Cash - Kas Operasional', '', 'Petty Cash - Kas Operasional', '', 'IDR', 0, 17, '99999', '0000-00-00 00:00:00', 0, 0, 'Pc3'),
('11114', 3, 'Petty Cash - Kas Sekretaris', '', 'Petty Cash - Kas Sekretaris', '', 'IDR', 0, 18, '99999', '0000-00-00 00:00:00', 0, 0, 'Pc5'),
('11165', 0, 'BCA RDN Lotus 4584311632', '4584311632', 'BCA RDN Lotus 4584311632', '', 'IDR', 0, 19, '51002', '0000-00-00 00:00:00', 0, 0, 'Bco');

-- --------------------------------------------------------

--
-- Table structure for table `0_bank_trans`
--

CREATE TABLE `0_bank_trans` (
  `id` int(11) NOT NULL,
  `type` smallint(6) DEFAULT NULL,
  `trans_no` int(11) DEFAULT NULL,
  `bank_act` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ref` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trans_date` date NOT NULL DEFAULT '0000-00-00',
  `amount` double DEFAULT NULL,
  `dimension_id` int(11) NOT NULL DEFAULT '0',
  `dimension2_id` int(11) NOT NULL DEFAULT '0',
  `person_type_id` int(11) NOT NULL DEFAULT '0',
  `person_id` tinyblob,
  `reconciled` date DEFAULT NULL,
  `ref2` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_bom`
--

CREATE TABLE `0_bom` (
  `id` int(11) NOT NULL,
  `parent` char(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `component` char(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `workcentre_added` int(11) NOT NULL DEFAULT '0',
  `loc_code` char(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `quantity` double NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_budget_trans`
--

CREATE TABLE `0_budget_trans` (
  `id` int(11) NOT NULL,
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `amount` double NOT NULL DEFAULT '0',
  `dimension_id` int(11) DEFAULT '0',
  `dimension2_id` int(11) DEFAULT '0',
  `memo_` tinytext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_chart_class`
--

CREATE TABLE `0_chart_class` (
  `cid` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `class_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ctype` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_chart_class`
--

INSERT INTO `0_chart_class` (`cid`, `class_name`, `ctype`, `inactive`) VALUES
('1', 'ASET', 1, 0),
('2', 'LIABILITAS', 2, 0),
('3', 'EQUITY', 3, 0),
('4', 'PENDAPATAN USAHA', 4, 0),
('5', 'COST OF SALES', 5, 0),
('6', 'BIAYA OPERASIONAL', 6, 0),
('8', 'OTHER INCOME', 4, 0),
('9', 'OTHER EXPENSES', 6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_chart_master`
--

CREATE TABLE `0_chart_master` (
  `account_code` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `account_code2` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `account_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `account_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_chart_types`
--

CREATE TABLE `0_chart_types` (
  `id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `class_id` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-1',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_chart_types`
--

INSERT INTO `0_chart_types` (`id`, `name`, `class_id`, `parent`, `inactive`) VALUES
('1-0000', 'ASET', '1', '', 0),
('1-1000', 'ASET LANCAR', '1', '1-0000', 0),
('1-1100', 'KAS', '1', '1-1000', 0),
('1-1150', 'BANK', '1', '1-1000', 0),
('1-1200', 'DEPOSITO BERJANGKA', '1', '1-1000', 0),
('1-1300', 'PORTOFOLIO EFEK', '1', '1-1000', 0),
('1-1400', 'PIUTANG USAHA', '1', '1-1000', 0),
('1-1500', 'PIUTANG LAIN-LAIN', '1', '1-1000', 0),
('1-1598', 'PIUTANG Pihak Ketiga', '1', '1-1500', 0),
('1-1599', 'PIUTANG Berelasi', '1', '1-1500', 0),
('1-1600', 'BIAYA DI BAYAR DIMUKA', '1', '1-1000', 0),
('1-1699', 'Down Payment', '1', '1-1600', 0),
('1-1700', 'PAJAK DIBAYAR DIMUKA', '1', '1-1000', 0),
('1-2000', 'AKTIVA TIDAK LANCAR', '1', '1-0000', 0),
('1-2100', 'INVESTASI', '1', '1-2000', 0),
('1-2200', 'ASET TETAP', '1', '1-2000', 0),
('1-2210', 'RENOVASI', '1', '1-2200', 0),
('1-2220', 'PERALATAN KANTOR', '1', '1-2200', 0),
('1-2230', 'FURNITURE AND FIXTURE', '1', '1-2200', 0),
('1-2240', 'KENDARAAN', '1', '1-2200', 0),
('1-2250', 'TANAH', '1', '1-2200', 0),
('1-2260', 'BANGUNAN', '1', '1-2200', 0),
('1-2300', 'ASET PAJAK TANGGUHAN', '1', '1-2000', 0),
('1-2400', 'GOODWILL', '1', '1-2000', 0),
('1-2900', 'ASETLAIN-LAIN', '1', '1-2000', 0),
('1-2999', 'Aktiva Lain-lain', '1', '1-2900', 0),
('2-0000', 'LIABILITAS', '2', '', 0),
('2-1000', 'LIABILITAS LANCAR', '2', '2-0000', 0),
('2-1100', 'HUTANG PERUSAHAAN', '2', '2-1000', 0),
('2-1200', 'HUTANG PAJAK', '2', '2-1000', 0),
('2-1300', 'PENERBITAN OBLIGASI', '2', '2-1000', 0),
('2-1600', 'ESTIMASI LIAB IMBALAN KERJA', '2', '2-1000', 0),
('2-1700', 'SELISIH BAGIAN RUGI ENTITAS', '2', '2-1000', 0),
('2-1900', 'HUTANG LAIN-LAIN LANCAR', '2', '2-1000', 0),
('2-2000', 'LIABILITAS JANGKA PANJANG', '2', '2-0000', 0),
('2-2100', 'LIABILITAS PAJAK', '2', '2-2000', 0),
('2-2900', 'HUTANG JANGKA PANJANG', '2', '2-2000', 0),
('3-0000', 'EQUITY', '3', '', 0),
('3-9800', 'Laba/(Rugi) belum direalisasi', '3', '3-0000', 0),
('4-0000', 'PENDAPATAN USAHA', '4', '', 0),
('5-0000', 'COST OF SALES', '5', '', 0),
('6-0000', 'BIAYA OPERASIONAL', '6', '', 0),
('6-1000', 'GAJI', '6', '6-0000', 0),
('6-2000', 'IKLAN DAN PROMOSI', '6', '6-0000', 0),
('6-2002', 'Biaya Entertainment', '6', '6-2000', 0),
('6-2003', 'Biaya Marketing', '6', '6-2000', 0),
('6-3000', 'SEWA', '6', '6-0000', 0),
('6-3003', 'Biaya Sewa Lainnya', '6', '6-5000', 0),
('6-4000', 'TELEKOMUNIKASI &amp; LISTRIK', '6', '6-0000', 0),
('6-5000', 'UMUM &amp; ADMINISTRASI', '6', '6-0000', 0),
('6-6000', 'DEPRESIASI', '6', '6-0000', 0),
('6-7000', 'PELATIHAN &amp; SEMINAR', '6', '6-0000', 0),
('6-8000', 'JASA PROFESSIONAL', '6', '6-0000', 0),
('6-8001', 'Biaya Profesional', '6', '6-8000', 0),
('6-9000', 'BIAYA PENJUALAN', '6', '6-0000', 0),
('6-9101', 'Perjalanan Dinas', '6', '6-9000', 0),
('6-9102', 'Hadiah &amp; Sumbangan', '6', '6-9000', 0),
('6-9104', 'Biaya Kantor', '6', '6-9000', 0),
('65900', 'Biaya Umum', '6', '6-5000', 0),
('8-0000', 'OTHER INCOME', '8', '', 0),
('8-9999', 'Pendapatan Lain Lain', '8', '8-0000', 0),
('9-0000', 'OTHER EXPENSES', '9', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_comments`
--

CREATE TABLE `0_comments` (
  `type` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL DEFAULT '0',
  `date_` date DEFAULT '0000-00-00',
  `memo_` tinytext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_credit_status`
--

CREATE TABLE `0_credit_status` (
  `id` int(11) NOT NULL,
  `reason_description` char(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dissallow_invoices` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_credit_status`
--

INSERT INTO `0_credit_status` (`id`, `reason_description`, `dissallow_invoices`, `inactive`) VALUES
(1, 'Good History', 0, 0),
(3, 'No more work until payment received', 1, 0),
(4, 'In liquidation', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_crm_categories`
--

CREATE TABLE `0_crm_categories` (
  `id` int(11) NOT NULL COMMENT 'pure technical key',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `description` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `system` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'nonzero for core system usage',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_crm_categories`
--

INSERT INTO `0_crm_categories` (`id`, `type`, `action`, `name`, `description`, `system`, `inactive`) VALUES
(1, 'cust_branch', 'general', 'General', 'General contact data for customer branch (overrides company setting)', 1, 0),
(2, 'cust_branch', 'invoice', 'Invoices', 'Invoice posting (overrides company setting)', 1, 0),
(3, 'cust_branch', 'order', 'Orders', 'Order confirmation (overrides company setting)', 1, 0),
(4, 'cust_branch', 'delivery', 'Deliveries', 'Delivery coordination (overrides company setting)', 1, 0),
(5, 'customer', 'general', 'General', 'General contact data for customer', 1, 0),
(6, 'customer', 'order', 'Orders', 'Order confirmation', 1, 0),
(7, 'customer', 'delivery', 'Deliveries', 'Delivery coordination', 1, 0),
(8, 'customer', 'invoice', 'Invoices', 'Invoice posting', 1, 0),
(9, 'supplier', 'general', 'General', 'General contact data for supplier', 1, 0),
(10, 'supplier', 'order', 'Orders', 'Order confirmation', 1, 0),
(11, 'supplier', 'delivery', 'Deliveries', 'Delivery coordination', 1, 0),
(12, 'supplier', 'invoice', 'Invoices', 'Invoice posting', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_crm_contacts`
--

CREATE TABLE `0_crm_contacts` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL DEFAULT '0' COMMENT 'foreign key to crm_contacts',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `entity_id` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_crm_contacts`
--

INSERT INTO `0_crm_contacts` (`id`, `person_id`, `type`, `action`, `entity_id`) VALUES
(1, 1, 'cust_branch', 'general', '1'),
(2, 1, 'customer', 'general', '1'),
(3, 2, 'cust_branch', 'general', '2'),
(4, 2, 'customer', 'general', '2'),
(5, 3, 'cust_branch', 'general', '3'),
(6, 3, 'customer', 'general', '3'),
(7, 4, 'cust_branch', 'general', '4'),
(8, 4, 'customer', 'general', '4'),
(9, 5, 'cust_branch', 'general', '5'),
(10, 5, 'customer', 'general', '5'),
(11, 6, 'cust_branch', 'general', '6'),
(12, 6, 'customer', 'general', '6'),
(13, 7, 'cust_branch', 'general', '7'),
(14, 7, 'customer', 'general', '7'),
(15, 8, 'cust_branch', 'general', '8'),
(16, 8, 'customer', 'general', '8'),
(17, 9, 'cust_branch', 'general', '9'),
(18, 9, 'customer', 'general', '9'),
(19, 10, 'cust_branch', 'general', '10'),
(20, 10, 'customer', 'general', '10'),
(21, 11, 'cust_branch', 'general', '11'),
(22, 11, 'customer', 'general', '11'),
(23, 12, 'cust_branch', 'general', '12'),
(24, 12, 'customer', 'general', '12'),
(25, 13, 'cust_branch', 'general', '13'),
(26, 13, 'customer', 'general', '13'),
(27, 14, 'cust_branch', 'general', '14'),
(28, 14, 'customer', 'general', '14'),
(29, 15, 'cust_branch', 'general', '15'),
(30, 15, 'customer', 'general', '15'),
(31, 16, 'cust_branch', 'general', '16'),
(32, 16, 'customer', 'general', '16'),
(33, 17, 'cust_branch', 'general', '17'),
(34, 17, 'customer', 'general', '17'),
(35, 18, 'cust_branch', 'general', '18'),
(36, 18, 'customer', 'general', '18'),
(37, 19, 'cust_branch', 'general', '19'),
(38, 19, 'customer', 'general', '19'),
(39, 20, 'cust_branch', 'general', '20'),
(40, 20, 'customer', 'general', '20'),
(41, 21, 'cust_branch', 'general', '21'),
(42, 21, 'customer', 'general', '21'),
(43, 22, 'cust_branch', 'general', '22'),
(44, 22, 'customer', 'general', '22'),
(45, 23, 'cust_branch', 'general', '23'),
(46, 23, 'customer', 'general', '23'),
(47, 24, 'cust_branch', 'general', '24'),
(48, 24, 'customer', 'general', '24'),
(49, 25, 'cust_branch', 'general', '25'),
(50, 25, 'customer', 'general', '25'),
(51, 26, 'cust_branch', 'general', '26'),
(52, 26, 'customer', 'general', '26'),
(53, 27, 'cust_branch', 'general', '27'),
(54, 27, 'customer', 'general', '27'),
(55, 28, 'cust_branch', 'general', '28'),
(56, 28, 'customer', 'general', '28'),
(57, 29, 'cust_branch', 'general', '29'),
(58, 29, 'customer', 'general', '29'),
(59, 30, 'cust_branch', 'general', '30'),
(60, 30, 'customer', 'general', '30'),
(61, 31, 'cust_branch', 'general', '31'),
(62, 31, 'customer', 'general', '31'),
(63, 32, 'cust_branch', 'general', '32'),
(64, 32, 'customer', 'general', '32'),
(65, 33, 'cust_branch', 'general', '33'),
(66, 33, 'customer', 'general', '33'),
(67, 34, 'cust_branch', 'general', '34'),
(68, 34, 'customer', 'general', '34'),
(69, 36, 'cust_branch', 'general', '36'),
(70, 36, 'customer', 'general', '36'),
(71, 37, 'cust_branch', 'general', '37'),
(72, 37, 'customer', 'general', '37'),
(73, 38, 'cust_branch', 'general', '38'),
(74, 38, 'customer', 'general', '38'),
(75, 39, 'cust_branch', 'general', '39'),
(76, 39, 'customer', 'general', '39'),
(77, 40, 'cust_branch', 'general', '40'),
(78, 40, 'customer', 'general', '40'),
(79, 41, 'cust_branch', 'general', '41'),
(80, 41, 'customer', 'general', '41'),
(81, 42, 'cust_branch', 'general', '42'),
(82, 42, 'customer', 'general', '42'),
(83, 43, 'cust_branch', 'general', '43'),
(84, 43, 'customer', 'general', '43'),
(85, 44, 'cust_branch', 'general', '44'),
(86, 44, 'customer', 'general', '44'),
(87, 45, 'cust_branch', 'general', '45'),
(88, 45, 'customer', 'general', '45'),
(89, 46, 'cust_branch', 'general', '46'),
(90, 46, 'customer', 'general', '46'),
(91, 47, 'cust_branch', 'general', '47'),
(92, 47, 'customer', 'general', '47'),
(93, 48, 'cust_branch', 'general', '48'),
(94, 48, 'customer', 'general', '48'),
(95, 49, 'cust_branch', 'general', '49'),
(96, 49, 'customer', 'general', '49'),
(97, 50, 'cust_branch', 'general', '50'),
(98, 50, 'customer', 'general', '50'),
(99, 51, 'cust_branch', 'general', '51'),
(100, 51, 'customer', 'general', '51'),
(101, 52, 'cust_branch', 'general', '52'),
(102, 52, 'customer', 'general', '52'),
(103, 53, 'cust_branch', 'general', '53'),
(104, 53, 'customer', 'general', '53'),
(105, 54, 'cust_branch', 'general', '54'),
(106, 54, 'customer', 'general', '54'),
(107, 55, 'cust_branch', 'general', '55'),
(108, 55, 'customer', 'general', '55'),
(109, 56, 'cust_branch', 'general', '56'),
(110, 56, 'customer', 'general', '56'),
(111, 58, 'cust_branch', 'general', '58'),
(112, 58, 'customer', 'general', '58'),
(113, 59, 'cust_branch', 'general', '59'),
(114, 59, 'customer', 'general', '59'),
(115, 60, 'cust_branch', 'general', '60'),
(116, 60, 'customer', 'general', '60'),
(117, 61, 'cust_branch', 'general', '61'),
(118, 61, 'customer', 'general', '61'),
(119, 62, 'cust_branch', 'general', '62'),
(120, 62, 'customer', 'general', '62'),
(121, 63, 'cust_branch', 'general', '63'),
(122, 63, 'customer', 'general', '63'),
(123, 64, 'supplier', 'general', '42'),
(124, 65, 'cust_branch', 'general', '64'),
(125, 65, 'customer', 'general', '64'),
(126, 66, 'supplier', 'general', '44'),
(127, 67, 'supplier', 'general', '0'),
(128, 68, 'supplier', 'general', '0'),
(129, 69, 'supplier', 'general', '47'),
(130, 70, 'supplier', 'general', '48'),
(131, 71, 'supplier', 'general', '49'),
(132, 72, 'supplier', 'general', '50'),
(133, 73, 'supplier', 'general', '51'),
(134, 74, 'supplier', 'general', '52'),
(135, 75, 'supplier', 'general', '53'),
(136, 76, 'supplier', 'general', '54'),
(137, 77, 'supplier', 'general', '55'),
(138, 78, 'supplier', 'general', '56'),
(139, 79, 'supplier', 'general', '57'),
(140, 80, 'cust_branch', 'general', '75'),
(141, 80, 'customer', 'general', '104'),
(142, 81, 'cust_branch', 'general', '76'),
(143, 81, 'customer', 'general', '105'),
(144, 82, 'cust_branch', 'general', '77'),
(145, 82, 'customer', 'general', '106'),
(146, 83, 'cust_branch', 'general', '78'),
(147, 83, 'customer', 'general', '107'),
(148, 84, 'cust_branch', 'general', '79'),
(149, 84, 'customer', 'general', '108');

-- --------------------------------------------------------

--
-- Table structure for table `0_crm_persons`
--

CREATE TABLE `0_crm_persons` (
  `id` int(11) NOT NULL,
  `ref` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `name2` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` tinytext COLLATE utf8_unicode_ci,
  `phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone2` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_crm_persons`
--

INSERT INTO `0_crm_persons` (`id`, `ref`, `name`, `name2`, `address`, `phone`, `phone2`, `fax`, `email`, `lang`, `notes`, `inactive`) VALUES
(1, '', 'REKSA DANA KAM EQUITY OPPORTUNITY FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(2, '', 'PL.KIK KAM BALANCED OPPORTUNITY FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(3, '', 'REKSA DANA KAM FIXED INCOME FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(4, '', 'RD T KAM CAPITAL PROTECTED  FUND 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(5, '', 'REKSA DANA KAM OPTI GROWTH FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(6, '', 'KIK P TERBATAS KAM CAPITAL GROWTH FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(7, '', 'KIK KAM REGULAR INCOME FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(8, '', 'REKSA DANA KAM EQUITY ALPHA FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(9, '', 'REKSA DANA KAM EQUITY GROWTH FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(10, '', 'REKSA DANA KAM MULTI STATEGY FUND 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(11, '', 'REKSA DANA KAM MULTI STATEGY FUND 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(12, '', 'Yay Kesejahteraan Kary Bank Indonesia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(13, '', 'JONI  DHARMAWAN - Standchart', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(14, '', 'Darwin Sutanto - Standchart', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(15, '', 'Irwan Sudjono - Standchart', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(16, '', 'Lindrawati - Standchart', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(17, '', 'Suwantara Gotama  - Standchart', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(18, '', 'Chandra Natalie Widjaya - Standchart', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(19, '', 'PT. SINARMAS SEKURITAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(20, '', 'PT. Multidaya Hutama Indokarunia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(21, '', 'DP Rajawali Nusindo ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(22, '', 'REKSA DANA KAM INDO BALANCE FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(23, '', 'RD T KAM CAPITAL PROTECTED  FUND 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(24, '', 'PT. Perusahaan Pengelola Aset (Persero)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(25, '', 'Noble Investment Corporation - Standchart', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(26, '', 'YAY Dana Pensiun Angkasa Pura I', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(27, '', 'YAY Kesehatan Pegawai Telkom', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(28, '', 'Dana Pensiun Angkasa Pura II', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(29, '', 'DANA PENSIUN KOMPAS GRAMEDIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(30, '', 'PERS. DANA PENSIUN SMART', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(31, '', 'Yay. Dana Pensiun Rajawali Nusantara Indonesia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(32, '', 'PT. Dana Pensiun Triputra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(33, '', 'Persek Dana Pensiun Badan Pendidikan Kristen Penabur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(34, '', 'YAY Pensiun Karyawan Bank Bukopin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(35, '', 'Koperasi Kesehatan Pegawai Dan Pensiun Bank Mandiri (Mandiri', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(36, '', 'Dana Pensiun Semen Padang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(37, '', 'RD T KAM CAPITAL PROTECTED  FUND 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(38, '', 'PT. Mandiri Sekuritas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(39, '', 'RD T KAM CAPITAL PROTECTED  FUND 6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(40, '', 'REKSA DANA  KAM LIBERTY  FUND ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(41, '', 'REKSA DANA KAM MIDCAP ALPHA FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(42, '', 'REKSA DANA PENYERTAAN TERBATAS KAM TELCO DOLLAR FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(43, '', 'PT. Niran Lumbung Sejahtera', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(44, '', 'REKSA DANA KAM DANA EKUITAS SEJAHTERA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(45, '', 'KAM MONEY MARKET FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(46, '', 'KAM DANA EKUITAS PLUS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(47, '', 'RD T KAM CAPITAL PROTECTED  FUND 7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(48, '', 'RD T KAM CAPITAL PROTECTED  FUND 10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(49, '', 'RD KAM DANA KAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(50, '', 'RD KAM DANA INVESTASI DINAMIS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(51, '', 'RD T KAM CAPITAL PROTECTED  FUND 11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(52, '', 'RD KAM MARKET LEADER FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(53, '', 'REKSA DANA TERPROTEKSI KAM TERPROTEKSI DOLLAR 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(54, '', 'RD T KAM CAPITAL PROTECTED  FUND 12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(55, '', 'RD KAM EQUITY MOMENTUM FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(56, '', 'REKSA DANA KAM DANA EKUITAS DINAMIS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(57, '', 'REKSA DANA SYARIAH PENYERTAAN TERBATAS KAM ORCHID PRO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(58, '', 'REKSA DANA KAM EQUITY PLATINUM FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(59, '', 'REKSA DANA KAM STEADY INCOME FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(60, '', 'REKSA DANA KAM DANA ALAM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(61, '', 'REKSA DANA KAM PREMIUM EQUITY FUND', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 0),
(62, 'test', 'test', '', '', '', '', '', '', '', '', 0),
(63, 'D-JP', 'Jos Parengkuan', '', 'Kemayoran', '021-51400888', '', '', '', '', '', 0),
(64, 'Buchari', '', '', '', '', '', '', '', '', '', 0),
(65, 'SYAICUS', 'SYAICUS', '', 'Jl.', '', '', '', '', '', '', 0),
(66, 'CV. Saraswati Flora IDR', '', '', 'Telavera Office Park Lt.28, Jl. TB. Simatupang Kav.22-26, Cilandak, Jaksel.', '', '', '', '', '', '', 0),
(67, 'PT. Astra Graphia Tbk', '', '', 'Jl. Kramat Raya No. 43, Kramat, Jakarta Pusat', '', '', '', '', '', '', 0),
(68, 'Leolin Jayayanti, SH', '', '', '', '', '', '', '', '', '', 0),
(69, 'PT. Bumianyar Futuria', '', '', 'Jl. Bima 18, Tanah Tinggi Jakarta Pusat', '', '', '', '', '', '', 0),
(70, 'CV. Kalawatu Prima', '', '', '', '', '', '', '', '', '', 0),
(71, 'PT. Mandiri Sekuritas', '', '', '', '', '', '', '', '', '', 0),
(72, 'PT. Pelangi Global Perkasa', '', '', '', '', '', '', '', '', '', 0),
(73, 'Bloomberg', '', '', '', '', '', '', '', '', '', 0),
(74, 'PT. Vidya Artha Tama', '', '', 'Jl. Raya Bogor No. 56 Km. 19, Kramat Jati, Jakarta Timur\n', '', '', '', '', '', '', 0),
(75, 'PT. Top Cars Gallery', '', '', 'Jl. Suryopranotono 10 RT 000 RW 000, Petojo Utara, Gambir, Jakarta Pusat\n', '', '', '', '', '', '', 0),
(76, 'Perhimpunan Pedagang ', '', '', 'Gd. Lina Lantai IV Ruang 411, Jl. H.R Rasuna Said Kav. B-7, Setiabudi, Jakarta Selatan, DKI Jakarta\n', '', '', '', '', '', '', 0),
(77, 'PT. Lotus Andalan Sekuritas', '', '', 'Wisma KEIAI Lt. 15, Jl, Jend. Sudirman Kav.m 3, Karet Tengsin, Karet Tengsin Tanah Abang, Jakarta Pusat, DKI Jakarta\n', '', '', '', '', '', '', 0),
(78, 'PT. Duta Distribusi Servisindo', '', '', 'Gd. RoxySquare Lt. Ground Blok B07 No. 01, Jl. Kyai Tapa No. 1, Jakarta Barat\n', '', '', '', '', '', '', 0),
(79, 'PT. Sinar Elok Abadi', '', '', 'Graha WoW, No.60B, Jl. Kesehatan, RT.2/RW.4, Petojo Sel., Gambir, DKI Jakarta, Daerah Khusus Ibukota Jakarta 10160', '', '', '', '', '', '', 0),
(80, 'BRI', 'Bank Rakyat Indonesia (Persero) Tbk', '', '', '', '', '', '', '', '', 0),
(81, 'Indopremier', 'PT. Indo Premier Securities', '', '', '', '', '', '', '', '', 0),
(82, 'Bareksa', 'PT. Bareksa Portal Investasi', '', '', '', '', '', '', '', '', 0),
(83, 'BJB', 'PT. Bank Pembangunan Daerah Jawa Barat dan Banten (BANK BJB)', '', '', '', '', '', '', '', '', 0),
(84, 'BNI', 'Bank Negara Indonesia (Persero) Tbk', '', '', '', '', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_currencies`
--

CREATE TABLE `0_currencies` (
  `currency` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `curr_abrev` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `curr_symbol` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `hundreds_name` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `auto_update` tinyint(1) NOT NULL DEFAULT '1',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_currencies`
--

INSERT INTO `0_currencies` (`currency`, `curr_abrev`, `curr_symbol`, `country`, `hundreds_name`, `auto_update`, `inactive`) VALUES
('Euro', 'EUR', '?', 'Europe', 'Cents', 1, 0),
('Rupiah', 'IDR', 'Rp', 'Indonesia', 'Sen', 1, 0),
('Ringgit Malaysia', 'MYR', 'RM', 'Malaysia', 'Sen', 1, 0),
('US Dollars', 'USD', '$', 'United States', 'Cents', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_cust_allocations`
--

CREATE TABLE `0_cust_allocations` (
  `id` int(11) NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  `amt` double UNSIGNED DEFAULT NULL,
  `date_alloc` date NOT NULL DEFAULT '0000-00-00',
  `trans_no_from` int(11) DEFAULT NULL,
  `trans_type_from` int(11) DEFAULT NULL,
  `trans_no_to` int(11) DEFAULT NULL,
  `trans_type_to` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_cust_branch`
--

CREATE TABLE `0_cust_branch` (
  `branch_code` int(11) NOT NULL,
  `debtor_no` int(11) NOT NULL DEFAULT '0',
  `br_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `branch_ref` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `br_address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `area` int(11) DEFAULT NULL,
  `salesman` int(11) NOT NULL DEFAULT '0',
  `default_location` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_group_id` int(11) DEFAULT NULL,
  `sales_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sales_discount_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `receivables_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payment_discount_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `default_ship_via` int(11) NOT NULL DEFAULT '1',
  `br_post_address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `group_no` int(11) NOT NULL DEFAULT '0',
  `notes` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `bank_account` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_cust_branch`
--

INSERT INTO `0_cust_branch` (`branch_code`, `debtor_no`, `br_name`, `branch_ref`, `br_address`, `area`, `salesman`, `default_location`, `tax_group_id`, `sales_account`, `sales_discount_account`, `receivables_account`, `payment_discount_account`, `default_ship_via`, `br_post_address`, `group_no`, `notes`, `bank_account`, `inactive`) VALUES
(1, 1, 'REKSA DANA KAM EQUITY OPPORTUNITY FUND', 'SEOF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '', 0, '', NULL, 0),
(2, 2, 'PL.KIK KAM BALANCED OPPORTUNITY FUND', 'SBOF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(3, 3, 'REKSA DANA KAM FIXED INCOME FUND', 'SFIF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(4, 4, 'RD T KAM CAPITAL PROTECTED  FUND 4', 'SCPF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(5, 5, 'REKSA DANA KAM OPTI GROWTH FUND', 'SOGF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(6, 6, 'KIK P TERBATAS KAM CAPITAL GROWTH FUND', 'SCGF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(7, 7, 'KIK KAM REGULAR INCOME FUND', 'SRIF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(8, 8, 'REKSA DANA KAM EQUITY ALPHA FUND', 'SEAF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(9, 9, 'REKSA DANA KAM EQUITY GROWTH FUND', 'SEGF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(10, 10, 'REKSA DANA KAM MULTI STATEGY FUND 1', 'SMSF.I', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(11, 11, 'REKSA DANA KAM MULTI STATEGY FUND 2', 'SMSF.II', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(12, 12, 'Yay Kesejahteraan Kary Bank Indonesia', 'KPD-YKKBI', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(13, 13, 'JONI  DHARMAWAN - Standchart', 'KPD-JD', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(14, 14, 'KPS-DS', 'KPD-DS', 'Jakarta', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, 'Jakarta', 0, '', NULL, 0),
(15, 15, 'Irwan Sudjono - Standchart', 'Irwan Sudjono - Standchart', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '', 0, '', NULL, 0),
(16, 16, 'Lindrawati - Standchart', 'Lindrawati - Standchart', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(17, 17, 'Suwantara Gotama  - Standchart', 'KPD-SG', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(18, 18, 'Chandra Natalie Widjaya - Standchart', 'KPD-CN', 'Jakarta', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(19, 19, 'PT. SINARMAS SEKURITAS', 'PT. Sinarmas Sekuritas - Niaga', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(20, 20, 'PT. Multidaya Hutama Indokarunia', 'KPD-MULTI', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(21, 21, 'DP Rajawali Nusindo ', 'KPD-RNI-SI', 'KEBAYORAN BARU', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, 'KEBAYORAN BARU', 0, '', NULL, 0),
(22, 22, 'REKSA DANA KAM INDO BALANCE FUND', 'SIBF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(23, 23, 'RD T KAM CAPITAL PROTECTED  FUND 3', 'SCPF 3  - Standchart', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(24, 24, 'PT. Perusahaan Pengelola Aset (Persero)', 'PT. PPA  - Standchart', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(25, 25, 'Noble Investment Corporation - Standchart', 'KPD-NOBLE', 'Jakarta', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(26, 26, 'YAY Dana Pensiun Angkasa Pura I', 'Dapen Angkasa Pura I - Mandiri', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(27, 27, 'YAY Kesehatan Pegawai Telkom', 'Yakes Telkom - Mandiri', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(28, 28, 'Dana Pensiun Angkasa Pura II', 'Dapen Angkasa Pura II - Mandir', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(29, 29, 'DANA PENSIUN KOMPAS GRAMEDIA', 'DP Kompas - Citibank', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(30, 30, 'PERS. DANA PENSIUN SMART', 'KPD-SMART', 'Gedung JITC Lt.9 Mangga Dua Ancol - Pademangan, Jakarta Utara - 14430\n', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, 'Gedung JITC Lt.9 Mangga Dua Ancol - Pademangan, Jakarta Utara - 14430\n', 0, '', NULL, 0),
(31, 31, 'Yay. Dana Pensiun Rajawali Nusantara Indonesia', 'KPD-RNI-SC', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(32, 32, 'PT. Dana Pensiun Triputra', 'DP Triputra - Mandiri', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(33, 33, 'Persek Dana Pensiun Badan Pendidikan Kristen Penabur', 'BPK Penabur - BCA', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(34, 34, 'YAY Pensiun Karyawan Bank Bukopin', 'DP Bukopin - Bukopin', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '', 0, '', NULL, 0),
(35, 35, 'Koperasi Kesehatan Pegawai Dan Pensiun Bank Mandiri (Mandiri', 'KPD-MCARE', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(36, 36, 'Dana Pensiun Semen Padang', 'KPD-SPAD', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(37, 37, 'RD T KAM CAPITAL PROTECTED  FUND 5', 'SCPF.5', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(38, 38, 'PT. Mandiri Sekuritas', 'Mandiri Sekuritas', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(39, 39, 'RD T KAM CAPITAL PROTECTED  FUND 6', 'SCPF.6', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(40, 40, 'REKSA DANA  KAM LIBERTY  FUND ', 'SLF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(41, 41, 'REKSA DANA KAM MIDCAP ALPHA FUND', 'SMAF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(42, 42, 'REKSA DANA PENYERTAAN TERBATAS KAM TELCO DOLLAR FUND', 'STDF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(43, 43, 'PT. Niran Lumbung Sejahtera', 'KPD-NILU', 'Jakarta', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, 'Jakarta', 0, '', NULL, 0),
(44, 44, 'REKSA DANA KAM DANA EKUITAS SEJAHTERA', 'SDES', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(45, 45, 'KAM MONEY MARKET FUND', 'SMMF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(46, 46, 'KAM DANA EKUITAS PLUS', 'SDEP', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(47, 47, 'RD T KAM CAPITAL PROTECTED  FUND 7', 'SCPF.7', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(48, 48, 'RD T KAM CAPITAL PROTECTED  FUND 10', 'SCPF.10', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(49, 49, 'RD KAM DANA KAS', 'SDK', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(50, 50, 'RD KAM DANA INVESTASI DINAMIS', 'SDID', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(51, 51, 'RD T KAM CAPITAL PROTECTED  FUND 11', 'SCPF.11', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(52, 52, 'RD KAM MARKET LEADER FUND', 'SMLF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(53, 53, 'REKSA DANA TERPROTEKSI KAM TERPROTEKSI DOLLAR 1', 'STD.1', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(54, 54, 'RD T KAM CAPITAL PROTECTED  FUND 12', 'SCPF.12', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(55, 55, 'RD KAM EQUITY MOMENTUM FUND', 'SEMF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(56, 56, 'REKSA DANA KAM DANA EKUITAS DINAMIS', 'SDED', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(57, 57, 'REKSA DANA SYARIAH PENYERTAAN TERBATAS KAM ORCHID PRO', 'SPTSOPS', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(58, 58, 'REKSA DANA KAM EQUITY PLATINUM FUND', 'SEPF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(59, 59, 'REKSA DANA KAM STEADY INCOME FUND', 'SSIF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(60, 60, 'REKSA DANA KAM DANA ALAM', 'SDA', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 0, '', NULL, 0),
(61, 61, 'REKSA DANA KAM PREMIUM EQUITY FUND', 'SPEF', '', 1, 1, 'DEF', 1, '41000', '99999', '11401', '43004', 1, '.', 0, '', NULL, 0),
(62, 62, 'test', 'test', '', 1, 1, 'DEF', 1, '', '99999', '11401', '89999', 1, '', 0, '', NULL, 0),
(63, 63, 'Jos Parengkuan', 'D-JP', 'Kemayoran', 1, 1, 'DEF', 1, '', '99999', '11401', '89999', 1, 'Kemayoran', 0, '', NULL, 0),
(64, 64, 'SYAICUS', 'SYAICUS', 'Jl.', 1, 1, 'DEF', 1, '', '99999', '11401', '89999', 1, 'Jl.', 0, '', NULL, 0),
(65, 65, 'Johnny Darmawan', 'DFJO', 'Jl. Bungur Besar No.54 E RT.003 RW.001 gunung sahari Selatan - Kemayoran Jakarta Pusat', 1, 1, 'DEF', 1, '', '', '', '', 1, 'Jl. Bungur Besar No.54 E RT.003 RW.001 gunung sahari Selatan - Kemayoran Jakarta Pusat', 0, '', NULL, 0),
(66, 66, 'Reksa Dana KAM Dana Kas ', 'MSDK', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 1, 1, 'DEF', 1, '', '', '', '', 1, 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 0, '', NULL, 0),
(67, 67, 'Reksa Dana KAM Fixed Income Fund', 'MSFIF', 'n/a', 1, 1, 'DEF', 1, '', '', '', '', 1, 'n/a', 0, '', NULL, 0),
(68, 68, 'KAM Balanced Opportunity Fund', 'MSBOF', 'n/a', 1, 1, 'DEF', 1, '', '', '', '', 1, 'n/a', 0, '', NULL, 0),
(69, 69, 'KAM Equity Opportunity Fund', 'MSEOF', 'n/a', 1, 1, 'DEF', 1, '', '', '', '', 1, 'n/a', 0, '', NULL, 0),
(70, 70, 'KAM Product', 'SYAIPROD', 'n/a', 1, 1, 'DEF', 1, '', '', '', '', 1, 'n/a', 0, '', NULL, 0),
(71, 71, 'Reksa Dana KAM Midcap Alpha Fund', 'MSMAF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 1, 1, 'DEF', 1, '', '', '', '', 1, 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 0, '', NULL, 0),
(72, 72, 'JOHNNY HENDRAWAN', 'DFJHEN', '', 1, 1, 'DEF', 1, '', '', '', '', 1, '', 0, '', NULL, 0),
(73, 73, 'PINEHILLS GROUP LIMITED', 'DFPHIL', '', 1, 1, 'DEF', 1, '', '', '', '', 1, '', 0, '', NULL, 0),
(74, 74, 'REKSA DANA KAM EQUITY BUMN PLUS', 'MSEBP', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 1, 1, 'DEF', 1, '', '', '', '', 1, 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 0, '', NULL, 0),
(75, 104, 'Bank Rakyat Indonesia (Persero) Tbk', 'BRI', '', 0, 0, '0', 0, '', '99999', '11401', '89999', 0, '', 0, '', NULL, 0),
(76, 105, 'PT. Indo Premier Securities', 'Indopremier', '', 0, 0, '0', 0, '', '99999', '11401', '89999', 0, '', 0, '', NULL, 0),
(77, 106, 'PT. Bareksa Portal Investasi', 'Bareksa', '', 0, 0, '0', 0, '', '99999', '11401', '89999', 0, '', 0, '', NULL, 0),
(78, 107, 'PT. Bank Pembangunan Daerah Jawa Barat dan Banten (BANK BJB)', 'BJB', '', 0, 0, '0', 0, '', '99999', '11401', '89999', 0, '', 0, '', NULL, 0),
(79, 108, 'Bank Negara Indonesia (Persero) Tbk', 'BNI', '', 0, 0, '0', 0, '', '99999', '11401', '89999', 0, '', 0, '', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_debtors_master`
--

CREATE TABLE `0_debtors_master` (
  `debtor_no` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `debtor_ref` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address` tinytext COLLATE utf8_unicode_ci,
  `address2` tinytext COLLATE utf8_unicode_ci,
  `tax_id` varchar(55) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `curr_code` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sales_type` int(11) NOT NULL DEFAULT '1',
  `dimension_id` int(11) NOT NULL DEFAULT '0',
  `dimension2_id` int(11) NOT NULL DEFAULT '0',
  `credit_status` int(11) NOT NULL DEFAULT '0',
  `payment_terms` int(11) DEFAULT NULL,
  `discount` double NOT NULL DEFAULT '0',
  `pymt_discount` double NOT NULL DEFAULT '0',
  `credit_limit` float NOT NULL DEFAULT '1000',
  `notes` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `kode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_debtors_master`
--

INSERT INTO `0_debtors_master` (`debtor_no`, `name`, `debtor_ref`, `address`, `address2`, `tax_id`, `curr_code`, `sales_type`, `dimension_id`, `dimension2_id`, `credit_status`, `payment_terms`, `discount`, `pymt_discount`, `credit_limit`, `notes`, `inactive`, `kode`, `kode2`, `tgl`) VALUES
(1, 'REKSA DANA KAM EQUITY OPPORTUNITY FUND', 'SEOF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '02.596.727.4-054.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSEOF', '001', '2007-06-01'),
(2, 'PL.KIK KAM BALANCED OPPORTUNITY FUND', 'SBOF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '02.741.929.0-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSBOF', '002', '2008-05-01'),
(3, 'REKSA DANA KAM FIXED INCOME FUND', 'SFIF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '03.163.132.8-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSFIF', '003', '2008-08-01'),
(4, 'RD T KAM CAPITAL PROTECTED  FUND 4', 'SCPF 4', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '03.163.132.8-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, 'PSCPF4', NULL, NULL),
(5, 'RDPT KAM OPTI GROWTH FUND', 'SOGF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '03.037.157.9-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'RSOGF', '004', '2008-08-01'),
(6, 'KIK P TERBATAS KAM CAPITAL GROWTH FUND', 'SCGF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '02.914.268.4-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'RSCGF', '005', '2008-08-02'),
(7, 'KIK KAM REGULAR INCOME FUND', 'SRIF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '02.914.192.6-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'RSRIF', '006', '2008-08-03'),
(8, 'REKSA DANA KAM EQUITY ALPHA FUND', 'SEAF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '03.257.567.2-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSEAF', '007', '2008-08-04'),
(9, 'REKSA DANA KAM EQUITY GROWTH FUND', 'SEGF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '03.037.346.8-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'RSEGF', '008', NULL),
(10, 'REKSA DANA KAM MULTI STRATEGY FUND 1', 'SMSF.I', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '03.037.358.3-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, NULL, '009', NULL),
(11, 'REKSA DANA KAM MULTI STATEGY FUND 2', 'SMSF.II', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '03.037.359.1-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'RSMSF2', '010', '2008-08-05'),
(12, 'Yay Kesejahteraan Kary Bank Indonesia', 'KPD-YKKBI', 'Komp. Bidakara Jl. Deposito VI No.12-14 Menteng Dalam - Tebet  Jakarta Selatan\n', 'Komp. Bidakara Jl. Deposito VI No.12-14 Menteng Dalam - Tebet  Jakarta Selatan\n', '01.585.723.8-062.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DYBIND', '011', '2008-09-01'),
(13, 'JONI  DHARMAWAN - Standchart', 'KPD-JD', 'Jl. Bungur Besar No.54 E RT.003 RW.001 gunung sahari Selatan - Kemayoran Jakarta Pusat\n', 'Jl. Bungur Besar No.54 E RT.003 RW.001 gunung sahari Selatan - Kemayoran Jakarta Pusat\n', '00.000.000.0-000.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DIJONN', '012', '2009-01-01'),
(14, 'Darwin Sutanto - Standchart', 'KPD-DS', 'Jl. AM Sangaji No.29 A RT/RW 011/007 Petojo Utara - Gambir Jakarta Pusat\n', 'Jl. AM Sangaji No.29 A RT/RW 011/007 Petojo Utara - Gambir Jakarta Pusat\n', '00.000.000.0-000.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DIDRWS', '013', '2009-01-02'),
(15, 'Irwan Sudjono - Standchart', 'Irwan Sudjono - Standchart', '', '', '000000000000000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DIIRWS3', NULL, NULL),
(16, 'Lindrawati - Standchart', 'Lindrawati - Standchart', NULL, NULL, '00.000.000.0-000.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, NULL, NULL, NULL),
(17, 'Suwantara Gotama  - Standchart', 'KPD-SG', 'Jl. Indramayu No.11 RT 001/005 Menteng, Menteng - Jakarta Pusat\n', 'Jl. Indramayu No.11 RT 001/005 Menteng, Menteng - Jakarta Pusat\n', '00.000.000.0-000.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DISGTM', '015', '2009-01-03'),
(18, 'Chandra Natalie Widjaya - Standchart', 'KPD-CN', 'Kemang Timur XV 22 RT/RW 010/009 Bangka - Mampang Prapatan Jakarta Selatan 12730\n', 'Kemang Timur XV 22 RT/RW 010/009 Bangka - Mampang Prapatan Jakarta Selatan 12730', '00.000.000.0-000.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DICHAN', '016', '2009-01-04'),
(19, 'PT. SINARMAS SEKURITAS', 'PT. Sinarmas Sekuritas - Niaga', NULL, NULL, '01.331.409.1-054.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, NULL, NULL, NULL),
(20, 'PT. Multidaya Hutama Indokarunia', 'KPD-MULTI', 'Darmawangsa 7 No.3 Rt.11 Rw.007 Pulo-  Kebayoran Baru- Jakarta Selatan\n', 'Darmawangsa 7 No.3 Rt.11 Rw.007 Pulo-  Kebayoran Baru- Jakarta Selatan\n', '02.183.864.4-019.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DLMHIN', '018', '2010-12-01'),
(21, 'DP Rajawali Nusindo ', 'KPD-RNI-SI', 'Jl. Anyer XI No.1 Menteng , MentengJakarta Pusat 10310\n', 'Jl. Anyer XI No.1 Menteng , MentengJakarta Pusat 10310\n', '02.261.710.4-063.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DDRNUS', '019', '2011-11-01'),
(22, 'REKSA DANA KAM INDO BALANCE FUND', 'SIBF', 'Gd. Bursa efek IndonesiaTower II Lt.23 Suite 2303, Jl. Widya Chandra V, Kebayoran Baru, Jakarta Selatan 12190\n', 'Gd. Bursa efek IndonesiaTower II Lt.23 Suite 2303, Jl. Widya Chandra V, Kebayoran Baru, Jakarta Selatan 12190\n', '03.037.482.1-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSIBF', '020', '2011-11-02'),
(23, 'RD T KAM CAPITAL PROTECTED  FUND 3', 'SCPF.3', NULL, NULL, '03.163.003.1-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, 'PSCPF3', '021', NULL),
(24, 'PT. Perusahaan Pengelola Aset (Persero)', 'PT. PPA  - Standchart', NULL, NULL, '01.061.242.2-051.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, NULL, NULL, NULL),
(25, 'Noble Investment Corporation - Standchart', 'KPD-NOBLE', 'Jakarta', 'Jakarta', '00.000.000.0-000.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DFNICO', '022', '2012-12-01'),
(26, 'YAY Dana Pensiun Angkasa Pura I', 'Dapen Angkasa Pura I - Mandiri', NULL, NULL, '01.643.543.0-027.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, 'DDAPII', NULL, NULL),
(27, 'YAY Kesehatan Pegawai Telkom', 'Yakes Telkom - Mandiri', NULL, NULL, '01.827.387.0-441.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, NULL, NULL, NULL),
(28, 'Dana Pensiun Angkasa Pura II', 'Dapen Angkasa Pura II - Mandir', NULL, NULL, '01.095.615.9-415.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, NULL, NULL, NULL),
(29, 'DANA PENSIUN KOMPAS GRAMEDIA', 'DP Kompas - Citibank', NULL, NULL, '01.370.805.2-038.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, 'DDDPKG', NULL, NULL),
(30, 'PERS. DP SMART', 'KPD-SMART', 'Gedung JITC Lt.9 Mangga Dua Ancol - Pademangan, Jakarta Utara - 14430\n', 'Gedung JITC Lt.9 Mangga Dua Ancol - Pademangan, Jakarta Utara - 14430\n', '01.644.505.8-044.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DDSMAT', '025', '2013-12-01'),
(31, 'Yay. Dana Pensiun Rajawali Nusantara Indonesia', 'KPD-RNI-SC', 'Jl. Anyer XI No.1 Menteng , MentengJakarta Pusat 10310\n', 'Jl. Anyer XI No.1 Menteng , MentengJakarta Pusat 10310\n', '01.224.324.2-071.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DDRANI', '026', '2013-12-02'),
(32, 'PT. Dana Pensiun Triputra', 'DP Triputra - Mandiri', NULL, NULL, '02.546.106.2-063.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, NULL, NULL, NULL),
(33, 'Persek Dana Pensiun Badan Pendidikan Kristen Penabur', 'BPK Penabur - BCA', NULL, NULL, '01.540.927.9-039.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, 'DDBPKP', NULL, NULL),
(34, 'YAY Pensiun Karyawan Bank Bukopin', 'DP Bukopin - Bukopin', '', '', '013325568061000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DDBKPN', NULL, NULL),
(35, 'Koperasi Kesehatan Pegawai Dan Pensiun Bank Mandiri (Mandiri Healthcare)', 'KPD-MCARE', 'Jl. Cikini Raya No.34-36, Cikini, Menteng, Jakarta Pusat, DKI Jakarta Raya \n', 'Jl. Cikini Raya No.34-36, Cikini, Menteng, Jakarta Pusat, DKI Jakarta Raya \n', '31.281.005.4-071.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, NULL, NULL, NULL),
(36, 'Dana Pensiun Semen Padang', 'KPD-SPAD', 'Komplek PT. Semen Padang , Indarung, Lubuk Kilangan, Padang\n', 'Komplek PT. Semen Padang , Indarung, Lubuk Kilangan, Padang', '01.138.899.8-201.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DFSMPD', '030', '2013-12-03'),
(37, 'RD T KAM CAPITAL PROTECTED  FUND 5', 'SCPF.5', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '03.312.878.6-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF5', '031', '2013-12-04'),
(38, 'PT. Mandiri Sekuritas', 'Mandiri Sekuritas', NULL, NULL, '01.565.217.5-093.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, NULL, NULL, NULL),
(39, 'RD T KAM CAPITAL PROTECTED  FUND 6', 'SCPF.6', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '70.062.537.9-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF6', '033', '2014-12-01'),
(40, 'REKSA DANA  KAM LIBERTY  FUND ', 'SLF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '66.624.520.4-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSLF', '034', '2014-12-02'),
(41, 'REKSA DANA KAM MIDCAP ALPHA FUND', 'SMAF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '70.503.917.0-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSMAF', '035', '2014-12-03'),
(42, 'REKSA DANA PENYERTAAN TERBATAS KAM TELCO DOLLAR FUND', 'STDF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '66.245.328.1-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSTDF', '036', '2014-12-04'),
(43, 'PT. Niran Lumbung Sejahtera', 'KPD-NILU', 'Gd. Graha Kapital Lt.4 Jl. Kemang Raya No.4, Bangka - Mampang Prapatan, Jakarta Selatan, DKI Jakarta Raya 12730\n', 'Gd. Graha Kapital Lt.4 Jl. Kemang Raya No.4, Bangka - Mampang Prapatan, Jakarta Selatan, DKI Jakarta Raya 12730', '03.118.088.8-014.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DFKNLS', '037', '2014-12-05'),
(44, 'REKSA DANA KAM DANA EKUITAS SEJAHTERA', 'SDES', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '71.345.441.1-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSDES', '038', '2015-12-01'),
(45, 'KAM MONEY MARKET FUND', 'SMMF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '71.700.857.7-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSMMF', '039', '2015-12-02'),
(46, 'KAM DANA EKUITAS PLUS', 'SDEP', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '72.213.228.9-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSDEP', '040', '2015-12-03'),
(47, 'RD T KAM CAPITAL PROTECTED  FUND 7', 'SCPF.7', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '72.048.885.7-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF7', '041', '2015-12-04'),
(48, 'RD T KAM CAPITAL PROTECTED  FUND 10', 'SCPF.10', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '72.709.327.0-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF10', '014', '2015-12-05'),
(49, 'RD KAM DANA KAS', 'SDK', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '72.568.260.3-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSDK', '042', '2015-12-06'),
(50, 'RD KAM DANA INVESTASI DINAMIS', 'SDID', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '73.386.299.9-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSDID', '043', '2015-12-07'),
(51, 'RD T KAM CAPITAL PROTECTED  FUND 11', 'SCPF.11', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '73.428.173.6-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF11', '032', '2015-12-08'),
(52, 'RD KAM MARKET LEADER FUND', 'SMLF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '73.771.681.1-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSMLF', '024', NULL),
(53, 'REKSA DANA TERPROTEKSI KAM TERPROTEKSI DOLLAR 1', 'STD.1', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '74.673.186.8-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'RSTDI', '009', '2016-12-01'),
(54, 'RD T KAM CAPITAL PROTECTED  FUND 12', 'SCPF.12', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '75.637.619.0-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF12', '017', '2016-12-02'),
(55, 'RD KAM EQUITY MOMENTUM FUND', 'SEMF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '75.820.117.2-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSEMF', '027', '2016-12-03'),
(56, 'REKSA DANA KAM DANA EKUITAS DINAMIS', 'SDED', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '76.315.645.2-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSDED', '044', '2016-12-04'),
(57, 'REKSA DANA SYARIAH PENYERTAAN TERBATAS KAM ORCHID PROPERTI SYARIAH', 'SPTSOPS', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '75.930.603.8-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, NULL, '028', '2016-12-05'),
(58, 'REKSA DANA KAM EQUITY PLATINUM FUND', 'SEPF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '76.441.286.2-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSEPF', '021', '2016-12-06'),
(59, 'REKSA DANA KAM STEADY INCOME FUND', 'SSIF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '76.913.562.5-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSSIF', '045', '2016-12-07'),
(60, 'REKSA DANA KAM DANA ALAM', 'SDA', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '76.235.780.4-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSDAA', '023', '2016-12-08'),
(61, 'REKSA DANA KAM PREMIUM EQUITY FUND', 'SPEF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru\n', '76.526.991.5-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSPEF', '018', NULL),
(62, 'test', 'test', '', '', '90675785', 'IDR', 1, 0, 0, 1, 3, 0, 0, 1000000, '', 1, NULL, NULL, NULL),
(63, 'Jos Parengkuan', 'D-JP', 'Kebayoran Baru', 'Gedung BEJ', '030371579012000', 'IDR', 1, 0, 0, 1, 3, 0, 0, 1000000, '', 0, 'DIJOSP', '029', '2017-01-01'),
(64, 'SYAICUS', 'SYAICUS', 'Jl.', 'Jl.', '', 'IDR', 1, 0, 0, 1, 3, 0, 0, 1000000, '', 1, 'SYTS', NULL, NULL),
(65, 'Joni Darmawan', 'DFJO', 'Jl. Bungur Besar No.54 E RT.003 RW.001 gunung sahari Selatan - Kemayoran Jakarta Pusat', 'Jl. Bungur Besar No.54 E RT.003 RW.001 gunung sahari Selatan - Kemayoran Jakarta Pusat', '000000000000000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DJON-USD', '050', '2017-01-03'),
(66, 'Reksa Dana KAM Dana Kas ', 'MSDK', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '725682603012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSDK', NULL, NULL),
(67, 'Reksa Dana KAM Fixed Income Fund', 'MSFIF', 'Gd. BEI Tower II Lt.23, Jl. Jend. Sudirman Kav.52-...', 'Gd. BEI Tower II Lt.23, Jl. Jend. Sudirman Kav.52-...', '031631328012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, 'MSFIF', NULL, NULL),
(68, 'KAM Balanced Opportunity Fund', 'MSBOF', 'Gd. BEI Tower II Lt.23, Jl. Jend. Sudirman Kav.52-...', 'Gd. BEI Tower II Lt.23, Jl. Jend. Sudirman Kav.52-...', '027419290012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, 'MSBOF', NULL, NULL),
(69, 'KAM Equity Opportunity Fund', 'MSEOF', 'Gd. BEI Tower II Lt.23, Jl. Jend. Sudirman Kav.52-...', 'Gd. BEI Tower II Lt.23, Jl. Jend. Sudirman Kav.52-...', '02.596.727.4-054.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, 'MSEOF', NULL, NULL),
(70, 'KAM Product', 'SYAIPROD', '', '', '', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 1, NULL, NULL, NULL),
(71, 'Reksa Dana KAM Midcap Alpha Fund', 'MSMAF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '705039170012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSMAF', NULL, NULL),
(72, 'JOHNNY HENDRAWAN', 'DFJHEN', '', '', '000000000000000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DFJHEN', NULL, '2016-12-09'),
(73, 'PINEHILLS GROUP LIMITED', 'DFPHIL', '', '', '000000000000000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DFPHIL', NULL, '2016-12-05'),
(74, 'REKSA DANA KAM EQUITY BUMN PLUS', 'SEBP', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '816213755012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSEBP', NULL, '2017-05-01'),
(75, 'KAM STRATEGIC INCOME FUND', 'SIF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '812126035012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSIF', NULL, '2017-06-01'),
(76, 'RDPT KAM ORCHID PROPERTY SYARIAH', 'SOPS', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '759306038012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSOPS', NULL, '2016-12-05'),
(77, 'REKSA DANA KAM PENDAPATAN TETAP OPTIMA', 'SPTO', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '822743787012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSPTO', NULL, '2017-09-02'),
(78, 'KAM PENDAPATAN TETAP OPTIMA SYARIAH', 'SPTOS', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '825453814012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSPTOS', NULL, '2017-09-03'),
(79, 'KAM PENDAPATAN TETAP PREMIUM', 'SPTP', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '808637540012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSPTP', NULL, '2017-03-02'),
(80, 'KAM SHARIA MONEY MARKET FUND', 'SSMMF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '82737452101200', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSSMMF', NULL, '2015-12-02'),
(81, 'KAM CAPITAL PROTECTED FUND 15', 'SCPF15', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '808637540012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF15', NULL, '2017-03-01'),
(82, 'KAM CAPITAL PROTECTED FUND 16', 'SCPF16', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '808750962012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF16', NULL, '2016-12-10'),
(83, 'KAM CAPITAL PROTECTED FUND 17', 'SCPF17', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '809676067012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF17', NULL, '2017-04-01'),
(84, 'KAM CAPITAL PROTECTED FUND 19', 'SCPF19', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '819990086012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF19', NULL, '2017-05-02'),
(85, 'KAM CAPITAL PROTECTED FUND 20', 'SCPF20', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '824824619012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF20', NULL, '2017-09-04'),
(86, 'KAM CAPITAL PROTECTED FUND 21', 'SCPF21', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '824825566012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF21', NULL, '2017-09-04'),
(87, 'RDPT KAM CAPITAL PROTECTED FUND SYARIAH 1', 'SCPFS1', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '824823942012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPFS1', NULL, '2017-09-01'),
(88, 'RDPT KAM MULTIFINANCE RUPIAH 1', 'SMR1', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '03.037.358.3-012.000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'RSMSF1', NULL, '2017-01-02'),
(89, 'KPD HANS NARPATI', 'DFHANS', '', '', '831223961012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'DFHANS', NULL, NULL),
(90, 'KAM EQUITY GARUDA FUND', 'EQSGAF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '829979152012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'EQSGAF', NULL, NULL),
(91, 'RD SYARIAH KAM SHARIA EQUITY FUND', 'EQSSEF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '827374190012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'EQSSEF', NULL, NULL),
(92, 'REKSA DANA SYARIAH INDEKS KAM SHARIA INDEX JII', 'EQSSIJ', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '829977917012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'EQSSIJ', NULL, NULL),
(93, 'PT PELABUHAN INDONESIA INVESTAMA', 'KPDPII', '', '', '835818808093000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'KPDPII', NULL, NULL),
(94, 'REKSA DANA KAM BALANCED GROWTH FUND', 'MSBGF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '829978790012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSBGF', NULL, NULL),
(95, 'KAM INDEX IDX30', 'MSIDX30', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '836343434012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSIDX30', NULL, NULL),
(96, 'REKSA DANA KAM MONEY MARKET FUND 2', 'MSMMF2', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '833375280012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSMMF2', NULL, NULL),
(97, 'REKSA DANA KAM PROVIDENTIA FIXED INCOME FUND', 'MSPFI', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '829978501012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSPFI', NULL, NULL),
(98, 'KAM PROVIDENTIA MONEY MARKET FUND', 'MSPMMF', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '829979681012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'MSPMMF', NULL, NULL),
(99, 'KAM CAPITAL PROTECTED FUND 22', 'PSCPF22', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '831730007012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF22', NULL, NULL),
(100, 'KAM CAPITAL PROTECTED FUND 23', 'PSCPF23', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '831730379012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF23', NULL, NULL),
(101, 'KAM CAPITAL PROTECTED FUND 24', 'PSCPF24', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '834881138012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF24', NULL, NULL),
(102, 'KAM CAPITAL PROTECTED FUND 26', 'PSCPF26', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '836124636012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPF26', NULL, NULL),
(103, 'KAM CAPITAL PROTECTED FUND USD 2', 'PSCPFUSD2', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', 'Gd. BEI Tower II Lt.23, Jl. Widya Chandra V, Kel.Senayan-Kebayoran Baru', '831223961012000', 'IDR', 1, 0, 0, 1, 4, 0, 0, 1000, '', 0, 'PSCPFUSD2', NULL, NULL),
(104, 'Bank Rakyat Indonesia (Persero) Tbk', 'BRI', '', '', '010016087093000', 'IDR', 0, 0, 0, 0, 0, 0, 0, 1000000, '', 0, NULL, NULL, NULL),
(105, 'PT. Indo Premier Securities', 'Indopremier', '', '', '017602673054000', 'IDR', 0, 0, 0, 0, 0, 0, 0, 1000000, '', 0, NULL, NULL, NULL),
(106, 'PT. Bareksa Portal Investasi', 'Bareksa', '', '', '032786337014000', 'IDR', 0, 0, 0, 0, 0, 0, 0, 1000000, '', 0, NULL, NULL, NULL),
(107, 'PT. Bank Pembangunan Daerah Jawa Barat dan Banten (BANK BJB)', 'BJB', '', '', '011186053054000', 'IDR', 0, 0, 0, 0, 0, 0, 0, 1000000, '', 0, NULL, NULL, NULL),
(108, 'Bank Negara Indonesia (Persero) Tbk', 'BNI', '', '', '010016061093000', 'IDR', 0, 0, 0, 0, 0, 0, 0, 1000000, '', 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `0_debtor_trans`
--

CREATE TABLE `0_debtor_trans` (
  `trans_no` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `type` smallint(6) UNSIGNED NOT NULL DEFAULT '0',
  `version` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `debtor_no` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `branch_code` int(11) NOT NULL DEFAULT '-1',
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `due_date` date NOT NULL DEFAULT '0000-00-00',
  `reference` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tpe` int(11) NOT NULL DEFAULT '0',
  `order_` int(11) NOT NULL DEFAULT '0',
  `ov_amount` double NOT NULL DEFAULT '0',
  `ov_gst` double NOT NULL DEFAULT '0',
  `ov_freight` double NOT NULL DEFAULT '0',
  `ov_freight_tax` double NOT NULL DEFAULT '0',
  `ov_discount` double NOT NULL DEFAULT '0',
  `alloc` double NOT NULL DEFAULT '0',
  `prep_amount` double NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '1',
  `ship_via` int(11) DEFAULT NULL,
  `dimension_id` int(11) NOT NULL DEFAULT '0',
  `dimension2_id` int(11) NOT NULL DEFAULT '0',
  `payment_terms` int(11) DEFAULT NULL,
  `tax_included` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_debtor_trans_details`
--

CREATE TABLE `0_debtor_trans_details` (
  `id` int(11) NOT NULL,
  `debtor_trans_no` int(11) DEFAULT NULL,
  `debtor_trans_type` int(11) DEFAULT NULL,
  `stock_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` tinytext COLLATE utf8_unicode_ci,
  `unit_price` double NOT NULL DEFAULT '0',
  `unit_tax` double NOT NULL DEFAULT '0',
  `quantity` double NOT NULL DEFAULT '0',
  `discount_percent` double NOT NULL DEFAULT '0',
  `standard_cost` double NOT NULL DEFAULT '0',
  `qty_done` double NOT NULL DEFAULT '0',
  `src_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_dimensions`
--

CREATE TABLE `0_dimensions` (
  `id` int(11) NOT NULL,
  `reference` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type_` tinyint(1) NOT NULL DEFAULT '1',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `date_` date NOT NULL DEFAULT '0000-00-00',
  `due_date` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_dimensions`
--

INSERT INTO `0_dimensions` (`id`, `reference`, `name`, `type_`, `closed`, `date_`, `due_date`) VALUES
(1, '020.1', 'ILHAM', 2, 0, '0000-00-00', '0000-00-00'),
(2, '020.2', 'LESTRINA', 2, 0, '0000-00-00', '0000-00-00'),
(3, '020.3', 'KADARISMAN', 2, 0, '0000-00-00', '0000-00-00'),
(4, '020.4', 'BUDI', 2, 0, '0000-00-00', '0000-00-00'),
(5, '020.5', 'ALMANIA', 2, 0, '0000-00-00', '0000-00-00'),
(6, '020.6', 'YUSLISAR', 2, 0, '0000-00-00', '0000-00-00'),
(7, '020.7', 'ANI', 2, 0, '0000-00-00', '0000-00-00'),
(8, '020.8', 'ALVIENA', 2, 0, '0000-00-00', '0000-00-00'),
(9, '020.9', 'SUSAN', 2, 0, '0000-00-00', '0000-00-00'),
(10, '020.10', 'HANNY', 2, 0, '0000-00-00', '0000-00-00'),
(11, '020.11', 'PELITA', 2, 0, '0000-00-00', '0000-00-00'),
(12, '020.12', 'PUTRI', 2, 0, '0000-00-00', '0000-00-00'),
(13, '020.13', 'IGNASIUS', 2, 0, '0000-00-00', '0000-00-00'),
(14, '020.14', 'DENNY', 2, 0, '0000-00-00', '0000-00-00'),
(15, '020.15', 'TASIK', 2, 0, '0000-00-00', '0000-00-00'),
(16, '020.16', 'RAMADHAN', 2, 0, '0000-00-00', '0000-00-00'),
(17, '020.17', 'NYI', 2, 0, '0000-00-00', '0000-00-00'),
(18, '020.18', 'MERU', 2, 0, '0000-00-00', '0000-00-00'),
(19, '020.19', 'LENNY', 2, 0, '0000-00-00', '0000-00-00'),
(20, '020.20', 'KUSNADY', 2, 0, '0000-00-00', '0000-00-00'),
(21, '020.21', 'MARLINDA', 2, 0, '0000-00-00', '0000-00-00'),
(22, '020.22', 'ALVIN', 2, 0, '0000-00-00', '0000-00-00'),
(23, '020.23', 'ANDRIANTO', 2, 0, '0000-00-00', '0000-00-00'),
(24, '020.24', 'YANNI', 2, 0, '0000-00-00', '0000-00-00'),
(25, '020.25', 'NORVIN', 2, 0, '0000-00-00', '0000-00-00'),
(26, '020.26', 'HERY', 2, 0, '0000-00-00', '0000-00-00'),
(27, '020.27', 'UNTUNG', 2, 0, '0000-00-00', '0000-00-00'),
(28, '020.28', 'GIANTO', 2, 0, '0000-00-00', '0000-00-00'),
(29, '020.29', 'YOSAFAT', 2, 0, '0000-00-00', '0000-00-00'),
(30, '020.30', 'MEILLIANNY', 2, 0, '0000-00-00', '0000-00-00'),
(31, '020.31', 'LEONY', 2, 0, '0000-00-00', '0000-00-00'),
(32, '020.32', 'RUDY', 2, 0, '0000-00-00', '0000-00-00'),
(33, '020.33', 'MEILLIANNY', 2, 0, '0000-00-00', '0000-00-00'),
(34, '020.34', 'ANISA', 2, 0, '0000-00-00', '0000-00-00'),
(35, '020.35', 'YULIAWATY', 2, 0, '0000-00-00', '0000-00-00'),
(36, '020.36', 'EDDY', 2, 0, '0000-00-00', '0000-00-00'),
(37, '020.37', 'MERLINE', 2, 0, '0000-00-00', '0000-00-00'),
(38, '020.38', 'CHANDRA', 2, 0, '0000-00-00', '0000-00-00'),
(39, '020.39', 'PRAWOTO', 2, 0, '0000-00-00', '0000-00-00'),
(40, '020.40', 'ELISKA', 2, 0, '0000-00-00', '0000-00-00'),
(41, '020.41', 'TIMOTHY', 2, 0, '0000-00-00', '0000-00-00'),
(42, '020.42', 'BRIAN', 2, 0, '0000-00-00', '0000-00-00'),
(43, '020.43', 'NADYA', 2, 0, '0000-00-00', '0000-00-00'),
(44, '020.44', 'LIOE', 2, 0, '0000-00-00', '0000-00-00'),
(45, '020.45', 'MIRA', 2, 0, '0000-00-00', '0000-00-00'),
(46, '020.46', 'DJONI', 2, 0, '0000-00-00', '0000-00-00'),
(47, '020.47', 'NURINDAH', 2, 0, '0000-00-00', '0000-00-00'),
(48, '020.48', 'AHMAD', 2, 0, '0000-00-00', '0000-00-00'),
(49, '020.49', 'KUSMANTORO', 2, 0, '0000-00-00', '0000-00-00'),
(50, '020.50', 'NELTJE', 2, 0, '0000-00-00', '0000-00-00'),
(51, '020.51', 'TISHA', 2, 0, '0000-00-00', '0000-00-00'),
(52, '020.52', 'SULISTIYO', 2, 0, '0000-00-00', '0000-00-00'),
(53, '020.53', 'WILLYS', 2, 0, '0000-00-00', '0000-00-00'),
(54, '020.54', 'YUPITER', 2, 0, '0000-00-00', '0000-00-00'),
(55, '020.55', 'ESTEFAN', 2, 0, '0000-00-00', '0000-00-00'),
(56, '020.56', 'BETTY', 2, 0, '0000-00-00', '0000-00-00'),
(57, '020.57', 'SURYA', 2, 0, '0000-00-00', '0000-00-00'),
(58, '020.58', 'RENDI', 2, 0, '0000-00-00', '0000-00-00'),
(59, '020.59', 'EIDI', 2, 0, '0000-00-00', '0000-00-00'),
(60, '020.60', 'INAYAT', 2, 0, '0000-00-00', '0000-00-00'),
(61, '020.61', 'ALIFIA', 2, 0, '0000-00-00', '0000-00-00'),
(62, '020.62', 'LUSIA', 2, 0, '0000-00-00', '0000-00-00'),
(63, '020.63', 'IPPIANDI', 2, 0, '0000-00-00', '0000-00-00'),
(64, '020.64', 'PUJI', 2, 0, '0000-00-00', '0000-00-00'),
(65, '020.65', 'LASTIN', 2, 0, '0000-00-00', '0000-00-00'),
(66, '020.66', 'NATALIE', 2, 0, '0000-00-00', '0000-00-00'),
(67, '020.67', 'EDY', 2, 0, '0000-00-00', '0000-00-00'),
(68, '020.68', 'SUARNI', 2, 0, '0000-00-00', '0000-00-00'),
(69, '020.69', 'JATU', 2, 0, '0000-00-00', '0000-00-00'),
(70, '020.70', 'ARIE', 2, 0, '0000-00-00', '0000-00-00'),
(71, '020.71', 'DHAMAYANTI', 2, 0, '0000-00-00', '0000-00-00'),
(72, '020.72', 'RENY', 2, 0, '0000-00-00', '0000-00-00'),
(73, '020.73', 'I', 2, 0, '0000-00-00', '0000-00-00'),
(74, '020.74', 'WAHYU', 2, 0, '0000-00-00', '0000-00-00'),
(75, '020.75', 'JUSUF', 2, 0, '0000-00-00', '0000-00-00'),
(76, '020.76', 'ALI', 2, 0, '0000-00-00', '0000-00-00'),
(77, '020.77', 'JAMES', 2, 0, '0000-00-00', '0000-00-00'),
(78, '020.78', 'THERESIA', 2, 0, '0000-00-00', '0000-00-00'),
(79, '020.79', 'SOO', 2, 0, '0000-00-00', '0000-00-00'),
(80, '020.80', 'HASANUDIN', 2, 0, '0000-00-00', '0000-00-00'),
(81, '020.81', 'SANTOLIE', 2, 0, '0000-00-00', '0000-00-00'),
(82, '020.82', 'SAFRIZAL', 2, 0, '0000-00-00', '0000-00-00'),
(83, '020.83', 'FITRI', 2, 0, '0000-00-00', '0000-00-00'),
(84, '020.84', 'SUGITO', 2, 0, '0000-00-00', '0000-00-00'),
(85, '020.85', 'IKA', 2, 0, '0000-00-00', '0000-00-00'),
(86, '020.86', 'HARRY', 2, 0, '0000-00-00', '0000-00-00'),
(87, '020.87', 'ANGLIR', 2, 0, '0000-00-00', '0000-00-00'),
(88, '020.88', 'BACHTIAR', 2, 0, '0000-00-00', '0000-00-00'),
(89, '020.89', 'JANE', 2, 0, '0000-00-00', '0000-00-00'),
(90, '020.90', 'TANIA', 2, 0, '0000-00-00', '0000-00-00'),
(91, '020.91', 'PRAKOSO', 2, 0, '0000-00-00', '0000-00-00'),
(92, '020.92', 'DEWI', 2, 0, '0000-00-00', '0000-00-00'),
(93, '020.93', 'DEWI', 2, 0, '0000-00-00', '0000-00-00'),
(94, '020.94', 'DIDIT', 2, 0, '0000-00-00', '0000-00-00'),
(95, '020.95', 'HADI', 2, 0, '0000-00-00', '0000-00-00'),
(96, '020.96', 'SUBAGIO', 2, 0, '0000-00-00', '0000-00-00'),
(97, '020.97', 'ASEP', 2, 0, '0000-00-00', '0000-00-00'),
(98, '020.98', 'TRIESYE', 2, 0, '0000-00-00', '0000-00-00'),
(99, '020.99', 'ALEX', 2, 0, '0000-00-00', '0000-00-00'),
(100, '020.100', 'DIPO', 2, 0, '0000-00-00', '0000-00-00'),
(101, '020.101', 'SANTYA', 2, 0, '0000-00-00', '0000-00-00'),
(102, '020.102', 'NURTARANTO', 2, 0, '0000-00-00', '0000-00-00'),
(103, '020.103', 'BARLIANA', 2, 0, '0000-00-00', '0000-00-00'),
(104, '020.104', 'SYUKRI', 2, 0, '0000-00-00', '0000-00-00'),
(105, '020.105', 'MUAMER', 2, 0, '0000-00-00', '0000-00-00'),
(106, '020.106', 'EDWIND', 2, 0, '0000-00-00', '0000-00-00'),
(107, '020.107', 'NI', 2, 0, '0000-00-00', '0000-00-00'),
(108, '020.108', 'NIA', 2, 0, '0000-00-00', '0000-00-00'),
(109, '020.109', 'MERI', 2, 0, '0000-00-00', '0000-00-00'),
(110, '020.110', 'SRI', 2, 0, '0000-00-00', '0000-00-00'),
(111, '020.111', 'HERNANDI', 2, 0, '0000-00-00', '0000-00-00'),
(112, '020.112', 'HERNANTI', 2, 0, '0000-00-00', '0000-00-00'),
(113, '020.113', 'SRI', 2, 0, '0000-00-00', '0000-00-00'),
(114, '020.114', 'MOCHAMAD', 2, 0, '0000-00-00', '0000-00-00'),
(115, '020.115', 'CHRISTINA', 2, 0, '0000-00-00', '0000-00-00'),
(116, '020.116', 'NASIRULLAH', 2, 0, '0000-00-00', '0000-00-00'),
(117, '020.117', 'INAWATI', 2, 0, '0000-00-00', '0000-00-00'),
(118, '020.118', 'IRENE', 2, 0, '0000-00-00', '0000-00-00'),
(119, '020.119', 'NESYA', 2, 0, '0000-00-00', '0000-00-00'),
(120, '020.120', 'SHIRLEY', 2, 0, '0000-00-00', '0000-00-00'),
(121, '020.121', 'FRANSISCA', 2, 0, '0000-00-00', '0000-00-00'),
(122, '020.122', 'RIDWAN', 2, 0, '0000-00-00', '0000-00-00'),
(123, '020.123', 'HERLIN', 2, 0, '0000-00-00', '0000-00-00'),
(124, '020.124', 'TJAHJO', 2, 0, '0000-00-00', '0000-00-00'),
(125, '020.125', 'WIDYANTO', 2, 0, '0000-00-00', '0000-00-00'),
(126, '020.126', 'ERIKA', 2, 0, '0000-00-00', '0000-00-00'),
(127, '020.127', 'DARWIN', 2, 0, '0000-00-00', '0000-00-00'),
(128, '020.128', 'AGUS', 2, 0, '0000-00-00', '0000-00-00'),
(129, '020.129', 'MUHAMMAD', 2, 0, '0000-00-00', '0000-00-00'),
(130, '020.130', 'IWAN', 2, 0, '0000-00-00', '0000-00-00'),
(131, '020.131', 'FARDILLA', 2, 0, '0000-00-00', '0000-00-00'),
(132, '020.132', 'SITI', 2, 0, '0000-00-00', '0000-00-00'),
(133, '020.133', 'IRWAN', 2, 0, '0000-00-00', '0000-00-00'),
(134, '020.134', 'ALEXANDER', 2, 0, '0000-00-00', '0000-00-00'),
(135, '020.135', 'GERRY', 2, 0, '0000-00-00', '0000-00-00'),
(136, '020.136', 'JEFFREY', 2, 0, '0000-00-00', '0000-00-00'),
(137, '020.137', 'ABDUL', 2, 0, '0000-00-00', '0000-00-00'),
(138, '020.138', 'FELICIA', 2, 0, '0000-00-00', '0000-00-00'),
(139, '020.139', 'ANDY', 2, 0, '0000-00-00', '0000-00-00'),
(140, '020.140', 'W', 2, 0, '0000-00-00', '0000-00-00'),
(141, '020.141', 'SETIAWAN', 2, 0, '0000-00-00', '0000-00-00'),
(142, '020.142', 'BAMBANG', 2, 0, '0000-00-00', '0000-00-00'),
(143, '020.143', 'TITI', 2, 0, '0000-00-00', '0000-00-00'),
(144, '020.144', 'HELEN', 2, 0, '0000-00-00', '0000-00-00'),
(145, '020.145', 'LESTARI', 2, 0, '0000-00-00', '0000-00-00'),
(146, '020.146', 'NADIMAH', 2, 0, '0000-00-00', '0000-00-00'),
(147, '020.147', 'LENNA', 2, 0, '0000-00-00', '0000-00-00'),
(148, '020.148', 'YUDI', 2, 0, '0000-00-00', '0000-00-00'),
(149, '020.149', 'FRANKIE', 2, 0, '0000-00-00', '0000-00-00'),
(150, '020.150', 'PIPIN', 2, 0, '0000-00-00', '0000-00-00'),
(151, '020.151', 'IYON', 2, 0, '0000-00-00', '0000-00-00'),
(152, '020.152', 'ARISKA', 2, 0, '0000-00-00', '0000-00-00'),
(153, '020.153', 'RICHELA', 2, 0, '0000-00-00', '0000-00-00'),
(154, '020.154', 'H', 2, 0, '0000-00-00', '0000-00-00'),
(155, '020.155', 'ANDY', 2, 0, '0000-00-00', '0000-00-00'),
(156, '020.156', 'AFLAH', 2, 0, '0000-00-00', '0000-00-00'),
(157, '020.157', 'EMIL', 2, 0, '0000-00-00', '0000-00-00'),
(158, '020.158', 'DHANY', 2, 0, '0000-00-00', '0000-00-00'),
(159, '020.159', 'AMELIA', 2, 0, '0000-00-00', '0000-00-00'),
(160, '020.160', 'JENPINO', 2, 0, '0000-00-00', '0000-00-00'),
(161, '020.161', 'ALOYSIUS', 2, 0, '0000-00-00', '0000-00-00'),
(162, '020.162', 'AMI', 2, 0, '0000-00-00', '0000-00-00'),
(163, '020.163', 'LILIES', 2, 0, '0000-00-00', '0000-00-00'),
(164, '020.164', 'AGUSTINUS', 2, 0, '0000-00-00', '0000-00-00'),
(165, '020.165', 'EMILDA', 2, 0, '0000-00-00', '0000-00-00'),
(166, '020.166', 'KURNIA', 2, 0, '0000-00-00', '0000-00-00'),
(167, '020.167', 'TENTAMINARTO', 2, 0, '0000-00-00', '0000-00-00'),
(168, '020.168', 'STEPHANUS', 2, 0, '0000-00-00', '0000-00-00'),
(169, '020.169', 'WIRAHADI', 2, 0, '0000-00-00', '0000-00-00'),
(170, '020.170', 'YUNI', 2, 0, '0000-00-00', '0000-00-00'),
(171, '020.171', 'ABDUL', 2, 0, '0000-00-00', '0000-00-00'),
(172, '020.172', 'WIDIANTI', 2, 0, '0000-00-00', '0000-00-00'),
(173, '020.173', 'MONICA', 2, 0, '0000-00-00', '0000-00-00'),
(174, '020.174', 'SULHAN', 2, 0, '0000-00-00', '0000-00-00'),
(175, '020.175', 'MIRAH', 2, 0, '0000-00-00', '0000-00-00'),
(176, '020.176', 'PWS', 2, 0, '0000-00-00', '0000-00-00'),
(177, '020.177', 'DEBBY', 2, 0, '0000-00-00', '0000-00-00'),
(178, '020.178', 'EVY', 2, 0, '0000-00-00', '0000-00-00'),
(179, '020.179', 'ROBBY', 2, 0, '0000-00-00', '0000-00-00'),
(180, '020.180', 'MARTA', 2, 0, '0000-00-00', '0000-00-00'),
(181, '020.181', 'AYU', 2, 0, '0000-00-00', '0000-00-00'),
(182, '020.182', 'AHMAD', 2, 0, '0000-00-00', '0000-00-00'),
(183, '020.183', 'OKTO', 2, 0, '0000-00-00', '0000-00-00'),
(184, '020.184', 'KEMALA', 2, 0, '0000-00-00', '0000-00-00'),
(185, '020.185', 'WASTY', 2, 0, '0000-00-00', '0000-00-00'),
(186, '020.186', 'ADIN', 2, 0, '0000-00-00', '0000-00-00'),
(187, '020.187', 'EDAH', 2, 0, '0000-00-00', '0000-00-00'),
(188, '020.188', 'NDARU', 2, 0, '0000-00-00', '0000-00-00'),
(189, '020.189', 'MUH.', 2, 0, '0000-00-00', '0000-00-00'),
(190, '020.190', 'MAS', 2, 0, '0000-00-00', '0000-00-00'),
(191, '020.191', 'YOGA', 2, 0, '0000-00-00', '0000-00-00'),
(192, '020.192', 'TRI', 2, 0, '0000-00-00', '0000-00-00'),
(193, '020.193', 'BAMBANG', 2, 0, '0000-00-00', '0000-00-00'),
(194, '020.194', 'ELIZABETH', 2, 0, '0000-00-00', '0000-00-00'),
(195, '020.195', 'Y', 2, 0, '0000-00-00', '0000-00-00'),
(196, '020.196', 'ARINTA', 2, 0, '0000-00-00', '0000-00-00'),
(197, '020.197', 'ENI', 2, 0, '0000-00-00', '0000-00-00'),
(198, '020.198', 'VEER', 2, 0, '0000-00-00', '0000-00-00'),
(199, '020.199', 'MASTIUR', 2, 0, '0000-00-00', '0000-00-00'),
(200, '020.200', 'YUZRI', 2, 0, '0000-00-00', '0000-00-00'),
(201, '020.201', 'MUHAMAD', 2, 0, '0000-00-00', '0000-00-00'),
(202, '020.202', 'ROBBY', 2, 0, '0000-00-00', '0000-00-00'),
(203, '020.203', 'NAFRIANI', 2, 0, '0000-00-00', '0000-00-00'),
(204, '020.204', 'EMILIA', 2, 0, '0000-00-00', '0000-00-00'),
(205, '020.205', 'IDRUS', 2, 0, '0000-00-00', '0000-00-00'),
(206, '020.206', 'JENNY', 2, 0, '0000-00-00', '0000-00-00'),
(207, '020.207', 'ANINDITYO', 2, 0, '0000-00-00', '0000-00-00'),
(208, '020.208', 'SAPTA', 2, 0, '0000-00-00', '0000-00-00'),
(209, '020.209', 'NOFITA', 2, 0, '0000-00-00', '0000-00-00'),
(210, '020.210', 'YENITA', 2, 0, '0000-00-00', '0000-00-00'),
(211, '020.211', 'LIE', 2, 0, '0000-00-00', '0000-00-00'),
(212, '020.212', 'TIURMA', 2, 0, '0000-00-00', '0000-00-00'),
(213, '020.213', 'WIDJAYA', 2, 0, '0000-00-00', '0000-00-00'),
(214, '020.214', 'YENNI', 2, 0, '0000-00-00', '0000-00-00'),
(215, '020.215', 'ONG', 2, 0, '0000-00-00', '0000-00-00'),
(216, '020.216', 'PIPIT', 2, 0, '0000-00-00', '0000-00-00'),
(217, '020.217', 'ANNA', 2, 0, '0000-00-00', '0000-00-00'),
(218, '020.218', 'MOHAMMAD', 2, 0, '0000-00-00', '0000-00-00'),
(219, '020.219', 'SORDAME', 2, 0, '0000-00-00', '0000-00-00'),
(220, '020.220', 'ACHMAD', 2, 0, '0000-00-00', '0000-00-00'),
(221, '020.221', 'HANS', 2, 0, '0000-00-00', '0000-00-00'),
(222, '020.222', 'DAVID', 2, 0, '0000-00-00', '0000-00-00'),
(223, '020.223', 'MARWATI', 2, 0, '0000-00-00', '0000-00-00'),
(224, '020.224', 'INDRAYANI', 2, 0, '0000-00-00', '0000-00-00'),
(225, '020.225', 'MAWARTI', 2, 0, '0000-00-00', '0000-00-00'),
(226, '020.226', 'PIPIN', 2, 0, '0000-00-00', '0000-00-00'),
(227, '020.227', 'MUSTOFA', 2, 0, '0000-00-00', '0000-00-00'),
(228, '020.228', 'FREDY', 2, 0, '0000-00-00', '0000-00-00'),
(229, '020.229', 'ANITA', 2, 0, '0000-00-00', '0000-00-00'),
(230, '020.230', 'TEDDY', 2, 0, '0000-00-00', '0000-00-00'),
(231, '020.231', 'KUTTA', 2, 0, '0000-00-00', '0000-00-00'),
(232, '020.232', 'KIAGOOS', 2, 0, '0000-00-00', '0000-00-00'),
(233, '020.233', 'FAOZAN', 2, 0, '0000-00-00', '0000-00-00'),
(234, '020.234', 'ANDY', 2, 0, '0000-00-00', '0000-00-00'),
(235, '020.235', 'NISITA', 2, 0, '0000-00-00', '0000-00-00'),
(236, '020.236', 'IRVAN', 2, 0, '0000-00-00', '0000-00-00'),
(237, '020.237', 'SUBOWO', 2, 0, '0000-00-00', '0000-00-00'),
(238, '020.238', 'MOHAMMAD', 2, 0, '0000-00-00', '0000-00-00'),
(239, '020.239', 'GIANTI', 2, 0, '0000-00-00', '0000-00-00'),
(240, '020.240', 'DANIEL', 2, 0, '0000-00-00', '0000-00-00'),
(241, '020.241', 'NUGROHO', 2, 0, '0000-00-00', '0000-00-00'),
(242, '020.242', 'DIAN', 2, 0, '0000-00-00', '0000-00-00'),
(243, '020.243', 'KASSANDRA', 2, 0, '0000-00-00', '0000-00-00'),
(244, '020.244', 'LOCDHA', 2, 0, '0000-00-00', '0000-00-00'),
(245, '020.245', 'ARI', 2, 0, '0000-00-00', '0000-00-00'),
(246, '020.246', 'HARTONO', 2, 0, '0000-00-00', '0000-00-00'),
(247, '020.247', 'FARIDA', 2, 0, '0000-00-00', '0000-00-00'),
(248, '020.248', 'KATARINA', 2, 0, '0000-00-00', '0000-00-00'),
(249, '020.249', 'STANISLAUS', 2, 0, '0000-00-00', '0000-00-00'),
(250, '020.250', 'NURHADI', 2, 0, '0000-00-00', '0000-00-00'),
(251, '020.251', 'EVA', 2, 0, '0000-00-00', '0000-00-00'),
(252, '020.252', 'DIANA', 2, 0, '0000-00-00', '0000-00-00'),
(253, '020.253', 'LINDAWATI', 2, 0, '0000-00-00', '0000-00-00'),
(254, '020.254', 'A', 2, 0, '0000-00-00', '0000-00-00'),
(255, '020.255', 'ANDJARSARI', 2, 0, '0000-00-00', '0000-00-00'),
(256, '020.256', 'ISMI', 2, 0, '0000-00-00', '0000-00-00'),
(257, '020.257', 'RISMA', 2, 0, '0000-00-00', '0000-00-00'),
(258, '020.258', 'ERLY', 2, 0, '0000-00-00', '0000-00-00'),
(259, '020.259', 'HEDY', 2, 0, '0000-00-00', '0000-00-00'),
(260, '020.260', 'ANITA', 2, 0, '0000-00-00', '0000-00-00'),
(261, '020.261', 'WELLY', 2, 0, '0000-00-00', '0000-00-00'),
(262, '020.262', 'I', 2, 0, '0000-00-00', '0000-00-00'),
(263, '020.263', 'LIZZA', 2, 0, '0000-00-00', '0000-00-00'),
(264, '020.264', 'HADI', 2, 0, '0000-00-00', '0000-00-00'),
(265, '020.265', 'ANANG', 2, 0, '0000-00-00', '0000-00-00'),
(266, '020.266', 'NATALIA', 2, 0, '0000-00-00', '0000-00-00'),
(267, '020.267', 'AGUS', 2, 0, '0000-00-00', '0000-00-00'),
(268, '020.268', 'ENRIKO', 2, 0, '0000-00-00', '0000-00-00'),
(269, '020.269', 'AGUSTA', 2, 0, '0000-00-00', '0000-00-00'),
(270, '020.270', 'SUNDFITRIS', 2, 0, '0000-00-00', '0000-00-00'),
(271, '020.271', 'TEDDY', 2, 0, '0000-00-00', '0000-00-00'),
(272, '020.272', 'WINARNI', 2, 0, '0000-00-00', '0000-00-00'),
(273, '020.273', 'SATRIA', 2, 0, '0000-00-00', '0000-00-00'),
(274, '020.274', 'DEVI', 2, 0, '0000-00-00', '0000-00-00'),
(275, '020.275', 'ANANDA', 2, 0, '0000-00-00', '0000-00-00'),
(276, '020.276', 'DENNY', 2, 0, '0000-00-00', '0000-00-00'),
(277, '020.277', 'ARMANDO', 2, 0, '0000-00-00', '0000-00-00'),
(278, '020.278', 'IMAN', 2, 0, '0000-00-00', '0000-00-00'),
(279, '020.279', 'PRADONO', 2, 0, '0000-00-00', '0000-00-00'),
(280, '020.280', 'PAULINA', 2, 0, '0000-00-00', '0000-00-00'),
(281, '020.281', 'SISKA', 2, 0, '0000-00-00', '0000-00-00'),
(282, '020.282', 'DJONNY', 2, 0, '0000-00-00', '0000-00-00'),
(283, '020.283', 'M.', 2, 0, '0000-00-00', '0000-00-00'),
(284, '020.284', 'SUWANDI', 2, 0, '0000-00-00', '0000-00-00'),
(285, '020.285', 'ERMAWATI', 2, 0, '0000-00-00', '0000-00-00'),
(286, '020.286', 'SHAMIR', 2, 0, '0000-00-00', '0000-00-00'),
(287, '020.287', 'THERESIA', 2, 0, '0000-00-00', '0000-00-00'),
(288, '020.288', 'SONNY', 2, 0, '0000-00-00', '0000-00-00'),
(289, '020.289', 'KHO', 2, 0, '0000-00-00', '0000-00-00'),
(290, '020.290', 'TOMMY', 2, 0, '0000-00-00', '0000-00-00'),
(291, '020.291', 'LIM', 2, 0, '0000-00-00', '0000-00-00'),
(292, '020.292', 'ANNY', 2, 0, '0000-00-00', '0000-00-00'),
(293, '020.293', 'AHMAD', 2, 0, '0000-00-00', '0000-00-00'),
(294, '020.294', 'LINA', 2, 0, '0000-00-00', '0000-00-00'),
(295, '020.295', 'YUSMA', 2, 0, '0000-00-00', '0000-00-00'),
(296, '020.296', 'MULYOTITO', 2, 0, '0000-00-00', '0000-00-00'),
(297, '020.297', 'ENDANG', 2, 0, '0000-00-00', '0000-00-00'),
(298, '020.298', 'DWI', 2, 0, '0000-00-00', '0000-00-00'),
(299, '020.299', 'KIANTI', 2, 0, '0000-00-00', '0000-00-00'),
(300, '020.300', 'RETNO', 2, 0, '0000-00-00', '0000-00-00'),
(301, '020.301', 'DIANA', 2, 0, '0000-00-00', '0000-00-00'),
(302, '020.302', 'TONI', 2, 0, '0000-00-00', '0000-00-00'),
(303, '020.303', 'MURNI', 2, 0, '0000-00-00', '0000-00-00'),
(304, '020.304', 'HIDAYAT', 2, 0, '0000-00-00', '0000-00-00'),
(305, '020.305', 'WICAHYO', 2, 0, '0000-00-00', '0000-00-00'),
(306, '020.306', 'ELY', 2, 0, '0000-00-00', '0000-00-00'),
(307, '020.307', 'HERU', 2, 0, '0000-00-00', '0000-00-00'),
(308, '020.308', 'JENAWATI', 2, 0, '0000-00-00', '0000-00-00'),
(309, '020.309', 'BUDHI', 2, 0, '0000-00-00', '0000-00-00'),
(310, '020.310', 'AJI', 2, 0, '0000-00-00', '0000-00-00'),
(311, '020.311', 'OEY', 2, 0, '0000-00-00', '0000-00-00'),
(312, '020.312', 'WONG', 2, 0, '0000-00-00', '0000-00-00'),
(313, '020.313', 'HENRY', 2, 0, '0000-00-00', '0000-00-00'),
(314, '020.314', 'YENNY', 2, 0, '0000-00-00', '0000-00-00'),
(315, '020.315', 'SAMUEL', 2, 0, '0000-00-00', '0000-00-00'),
(316, '020.316', 'TITO', 2, 0, '0000-00-00', '0000-00-00'),
(317, '020.317', 'TRIWIRA', 2, 0, '0000-00-00', '0000-00-00'),
(318, '020.318', 'JUSRAN', 2, 0, '0000-00-00', '0000-00-00'),
(319, '020.319', 'SATYA', 2, 0, '0000-00-00', '0000-00-00'),
(320, '020.320', 'HERY', 2, 0, '0000-00-00', '0000-00-00'),
(321, '020.321', 'INGE', 2, 0, '0000-00-00', '0000-00-00'),
(322, '020.322', 'TARI', 2, 0, '0000-00-00', '0000-00-00'),
(323, '020.323', 'AMELIA', 2, 0, '0000-00-00', '0000-00-00'),
(324, '020.324', 'RONNY', 2, 0, '0000-00-00', '0000-00-00'),
(325, '020.325', 'FENDY', 2, 0, '0000-00-00', '0000-00-00'),
(326, '020.326', 'SONJA', 2, 0, '0000-00-00', '0000-00-00'),
(327, '020.327', 'SUSI', 2, 0, '0000-00-00', '0000-00-00'),
(328, '020.328', 'LIAN', 2, 0, '0000-00-00', '0000-00-00'),
(329, '020.329', 'PETRUS', 2, 0, '0000-00-00', '0000-00-00'),
(330, '020.330', 'JL.KARANG', 2, 0, '0000-00-00', '0000-00-00'),
(331, '020.331', 'RITA', 2, 0, '0000-00-00', '0000-00-00'),
(332, '020.332', 'WILLIAM', 2, 0, '0000-00-00', '0000-00-00'),
(333, '020.333', 'CHAERUL', 2, 0, '0000-00-00', '0000-00-00'),
(334, '020.334', 'TAN', 2, 0, '0000-00-00', '0000-00-00'),
(335, '020.335', 'PERRY', 2, 0, '0000-00-00', '0000-00-00'),
(336, '020.336', 'VINOLIA', 2, 0, '0000-00-00', '0000-00-00'),
(337, '020.337', 'SIGIT', 2, 0, '0000-00-00', '0000-00-00'),
(338, '020.338', 'MULJATI', 2, 0, '0000-00-00', '0000-00-00'),
(339, '020.339', 'FAZIKKA', 2, 0, '0000-00-00', '0000-00-00'),
(340, '020.340', 'JULIA', 2, 0, '0000-00-00', '0000-00-00'),
(341, '020.341', 'ELVIA', 2, 0, '0000-00-00', '0000-00-00'),
(342, '020.342', 'ANTONIS', 2, 0, '0000-00-00', '0000-00-00'),
(343, '020.343', 'BONA', 2, 0, '0000-00-00', '0000-00-00'),
(344, '020.344', 'R.D', 2, 0, '0000-00-00', '0000-00-00'),
(345, '020.345', 'JOY', 2, 0, '0000-00-00', '0000-00-00'),
(346, '020.346', 'FERRY', 2, 0, '0000-00-00', '0000-00-00'),
(347, '020.347', 'SUHAILA', 2, 0, '0000-00-00', '0000-00-00'),
(348, '020.348', 'DESELFOD', 2, 0, '0000-00-00', '0000-00-00'),
(349, '020.349', 'LOEMONGGA', 2, 0, '0000-00-00', '0000-00-00'),
(350, '020.350', 'HENDRY', 2, 0, '0000-00-00', '0000-00-00'),
(351, '020.351', 'MUH', 2, 0, '0000-00-00', '0000-00-00'),
(352, '020.352', 'VINTYA', 2, 0, '0000-00-00', '0000-00-00'),
(353, '020.353', 'IMANUEL', 2, 0, '0000-00-00', '0000-00-00'),
(354, '020.354', 'MOCHAMMAD', 2, 0, '0000-00-00', '0000-00-00'),
(355, '020.355', 'MELIA', 2, 0, '0000-00-00', '0000-00-00'),
(356, '020.356', 'NENCY', 2, 0, '0000-00-00', '0000-00-00'),
(357, '020.357', 'MEGA', 2, 0, '0000-00-00', '0000-00-00'),
(358, '020.358', 'AMALIA', 2, 0, '0000-00-00', '0000-00-00'),
(359, '020.359', 'E.P', 2, 0, '0000-00-00', '0000-00-00'),
(360, '020.360', 'HARRY', 2, 0, '0000-00-00', '0000-00-00'),
(361, '020.361', 'FERRY', 2, 0, '0000-00-00', '0000-00-00'),
(362, '020.362', 'ROY', 2, 0, '0000-00-00', '0000-00-00'),
(363, '020.363', 'M.', 2, 0, '0000-00-00', '0000-00-00'),
(364, '020.364', 'JULIAN', 2, 0, '0000-00-00', '0000-00-00'),
(365, '020.365', 'GAFUR', 2, 0, '0000-00-00', '0000-00-00'),
(366, '020.366', 'YULIANDINI', 2, 0, '0000-00-00', '0000-00-00'),
(367, '020.367', 'RYAN', 2, 0, '0000-00-00', '0000-00-00'),
(368, '020.368', 'M.', 2, 0, '0000-00-00', '0000-00-00'),
(369, '020.369', 'JINNY', 2, 0, '0000-00-00', '0000-00-00'),
(370, '020.370', 'SONG', 2, 0, '0000-00-00', '0000-00-00'),
(371, '020.371', 'HADI', 2, 0, '0000-00-00', '0000-00-00'),
(372, '020.372', 'MUHHAMAD', 2, 0, '0000-00-00', '0000-00-00'),
(373, '020.373', 'NIKOLAS', 2, 0, '0000-00-00', '0000-00-00'),
(374, '020.374', 'HONNY', 2, 0, '0000-00-00', '0000-00-00'),
(375, '020.375', 'HENKY', 2, 0, '0000-00-00', '0000-00-00'),
(376, '020.376', 'RATNA', 2, 0, '0000-00-00', '0000-00-00'),
(377, '020.377', 'T', 2, 0, '0000-00-00', '0000-00-00'),
(378, '020.378', 'TJINIWATY', 2, 0, '0000-00-00', '0000-00-00'),
(379, '020.379', 'AGNES', 2, 0, '0000-00-00', '0000-00-00'),
(380, '020.380', 'CHANDRA', 2, 0, '0000-00-00', '0000-00-00'),
(381, '020.381', 'BRIAN', 2, 0, '0000-00-00', '0000-00-00'),
(382, '020.382', 'SULIATIN', 2, 0, '0000-00-00', '0000-00-00'),
(383, '020.383', 'MIRANTI', 2, 0, '0000-00-00', '0000-00-00'),
(384, '020.384', 'SUMINTO', 2, 0, '0000-00-00', '0000-00-00'),
(385, '020.385', 'JAY', 2, 0, '0000-00-00', '0000-00-00'),
(386, '020.386', 'ISBIA', 2, 0, '0000-00-00', '0000-00-00'),
(387, '020.387', 'AUGUST', 2, 0, '0000-00-00', '0000-00-00'),
(388, '020.388', 'HENDRY', 2, 0, '0000-00-00', '0000-00-00'),
(389, '020.389', 'ARMADETA', 2, 0, '0000-00-00', '0000-00-00'),
(390, '020.390', 'YUNANTI', 2, 0, '0000-00-00', '0000-00-00'),
(391, '020.391', 'IRENE', 2, 0, '0000-00-00', '0000-00-00'),
(392, '020.392', 'ROMINA', 2, 0, '0000-00-00', '0000-00-00'),
(393, '020.393', 'HERU', 2, 0, '0000-00-00', '0000-00-00'),
(394, '020.394', 'HO', 2, 0, '0000-00-00', '0000-00-00'),
(395, '020.395', 'TITO', 2, 0, '0000-00-00', '0000-00-00'),
(396, '020.396', 'JANUAR', 2, 0, '0000-00-00', '0000-00-00'),
(397, '020.397', 'RONNY', 2, 0, '0000-00-00', '0000-00-00'),
(398, '020.398', 'CHARLES', 2, 0, '0000-00-00', '0000-00-00'),
(399, '020.399', 'REVYKA', 2, 0, '0000-00-00', '0000-00-00'),
(400, '020.400', 'MUSTOFA', 2, 0, '0000-00-00', '0000-00-00'),
(401, '020.401', 'SETIAWAN', 2, 0, '0000-00-00', '0000-00-00'),
(402, '020.402', 'LIE', 2, 0, '0000-00-00', '0000-00-00'),
(403, '020.403', 'ERIKA', 2, 0, '0000-00-00', '0000-00-00'),
(404, '020.404', 'ROSA', 2, 0, '0000-00-00', '0000-00-00'),
(405, '020.405', 'MURNIATY', 2, 0, '0000-00-00', '0000-00-00'),
(406, '020.406', 'SAPTARI', 2, 0, '0000-00-00', '0000-00-00'),
(407, '020.407', 'KAPLER', 2, 0, '0000-00-00', '0000-00-00'),
(408, '020.408', 'ALEXANDER', 2, 0, '0000-00-00', '0000-00-00'),
(409, '020.409', 'SUDIRMAN', 2, 0, '0000-00-00', '0000-00-00'),
(410, '020.410', 'YOHANES', 2, 0, '0000-00-00', '0000-00-00'),
(411, '020.411', 'NANNY', 2, 0, '0000-00-00', '0000-00-00'),
(412, '020.412', 'SAMUEL', 2, 0, '0000-00-00', '0000-00-00'),
(413, '020.413', 'NYOO', 2, 0, '0000-00-00', '0000-00-00'),
(414, '020.414', 'IMAM', 2, 0, '0000-00-00', '0000-00-00'),
(415, '020.415', 'DEDY', 2, 0, '0000-00-00', '0000-00-00'),
(416, '020.416', 'RONNY', 2, 0, '0000-00-00', '0000-00-00'),
(417, '020.417', 'ERWIN', 2, 0, '0000-00-00', '0000-00-00'),
(418, '020.418', 'AGNES', 2, 0, '0000-00-00', '0000-00-00'),
(419, '020.419', 'DEWI', 2, 0, '0000-00-00', '0000-00-00'),
(420, '020.420', 'ENY', 2, 0, '0000-00-00', '0000-00-00'),
(421, '020.421', 'EDWIN', 2, 0, '0000-00-00', '0000-00-00'),
(422, '020.422', 'VANESSA', 2, 0, '0000-00-00', '0000-00-00'),
(423, '020.423', 'RENALDO', 2, 0, '0000-00-00', '0000-00-00'),
(424, '020.424', 'AMERTA', 2, 0, '0000-00-00', '0000-00-00'),
(425, '020.425', 'KRISTIANTINI', 2, 0, '0000-00-00', '0000-00-00'),
(426, '020.426', 'ALIA', 2, 0, '0000-00-00', '0000-00-00'),
(427, '020.427', 'LYDIA', 2, 0, '0000-00-00', '0000-00-00'),
(428, '020.428', 'FIRMAN', 2, 0, '0000-00-00', '0000-00-00'),
(429, '020.429', 'WISMAR', 2, 0, '0000-00-00', '0000-00-00'),
(430, '020.430', 'DION', 2, 0, '0000-00-00', '0000-00-00'),
(431, '020.431', 'AMOS', 2, 0, '0000-00-00', '0000-00-00'),
(432, '020.432', 'EDY', 2, 0, '0000-00-00', '0000-00-00'),
(433, '020.433', 'PRIHANTOMO', 2, 0, '0000-00-00', '0000-00-00'),
(434, '020.434', 'DZULKARNAIN', 2, 0, '0000-00-00', '0000-00-00'),
(435, '020.435', 'TEDDY', 2, 0, '0000-00-00', '0000-00-00'),
(436, '020.436', 'ISAAC', 2, 0, '0000-00-00', '0000-00-00'),
(437, '020.437', 'IRENE', 2, 0, '0000-00-00', '0000-00-00'),
(438, '020.438', 'OLIVIA', 2, 0, '0000-00-00', '0000-00-00'),
(439, '020.439', 'VANESSA', 2, 0, '0000-00-00', '0000-00-00'),
(440, '020.440', 'MIRTATI', 2, 0, '0000-00-00', '0000-00-00'),
(441, '020.441', 'MUNGKI', 2, 0, '0000-00-00', '0000-00-00'),
(442, '020.442', 'LEO', 2, 0, '0000-00-00', '0000-00-00'),
(443, '020.443', 'MARIA', 2, 0, '0000-00-00', '0000-00-00'),
(444, '020.444', 'OLIVIA', 2, 0, '0000-00-00', '0000-00-00'),
(445, '020.445', 'LUTHFIA', 2, 0, '0000-00-00', '0000-00-00'),
(446, '020.446', 'ANGGRAINI', 2, 0, '0000-00-00', '0000-00-00'),
(447, '020.447', 'JOS', 2, 0, '0000-00-00', '0000-00-00'),
(448, '020.448', 'RENY', 2, 0, '0000-00-00', '0000-00-00'),
(449, '020.449', 'IPPIANDI', 2, 0, '0000-00-00', '0000-00-00'),
(450, '020.450', 'ALI', 2, 0, '0000-00-00', '0000-00-00'),
(451, '020.451', 'MAHMUDDIN', 2, 0, '0000-00-00', '0000-00-00'),
(452, '020.452', 'LIM', 2, 0, '0000-00-00', '0000-00-00'),
(453, '020.453', 'ONG', 2, 0, '0000-00-00', '0000-00-00'),
(454, '020.454', 'JHONNY', 2, 0, '0000-00-00', '0000-00-00'),
(455, '020.455', 'PURWOSO', 2, 0, '0000-00-00', '0000-00-00'),
(456, '020.456', 'FRANKY', 2, 0, '0000-00-00', '0000-00-00'),
(457, '020.457', 'PRAMESTI', 2, 0, '0000-00-00', '0000-00-00'),
(458, '020.458', 'HOO', 2, 0, '0000-00-00', '0000-00-00'),
(459, '020.459', 'KWA', 2, 0, '0000-00-00', '0000-00-00'),
(460, '020.460', 'FARID', 2, 0, '0000-00-00', '0000-00-00'),
(461, '020.461', 'FRANSISKA', 2, 0, '0000-00-00', '0000-00-00'),
(462, '020.462', 'ALEXANDER', 2, 0, '0000-00-00', '0000-00-00'),
(463, '020.463', 'ASTRID', 2, 0, '0000-00-00', '0000-00-00'),
(464, '020.464', 'DWI', 2, 0, '0000-00-00', '0000-00-00'),
(465, '020.465', 'BACHTIAR', 2, 0, '0000-00-00', '0000-00-00'),
(466, '020.466', 'INDIRA', 2, 0, '0000-00-00', '0000-00-00'),
(467, '020.467', 'ASKALANI', 2, 0, '0000-00-00', '0000-00-00'),
(468, '020.468', 'R', 2, 0, '0000-00-00', '0000-00-00'),
(469, '020.469', 'R', 2, 0, '0000-00-00', '0000-00-00'),
(470, '020.470', 'LIA', 2, 0, '0000-00-00', '0000-00-00'),
(471, '020.471', 'RESI', 2, 0, '0000-00-00', '0000-00-00'),
(472, '020.472', 'INKE', 2, 0, '0000-00-00', '0000-00-00'),
(473, '020.473', 'SUSANA', 2, 0, '0000-00-00', '0000-00-00'),
(474, '020.474', 'OEI', 2, 0, '0000-00-00', '0000-00-00'),
(475, '020.475', 'EKO', 2, 0, '0000-00-00', '0000-00-00'),
(476, '020.476', 'ANANTA', 2, 0, '0000-00-00', '0000-00-00'),
(477, '020.477', 'SOFJAN', 2, 0, '0000-00-00', '0000-00-00'),
(478, '020.478', 'ADIE', 2, 0, '0000-00-00', '0000-00-00'),
(479, '020.479', 'EKA', 2, 0, '0000-00-00', '0000-00-00'),
(480, '020.480', 'TENG', 2, 0, '0000-00-00', '0000-00-00'),
(481, '020.481', 'WAHYUDI', 2, 0, '0000-00-00', '0000-00-00'),
(482, '020.482', 'JULIA', 2, 0, '0000-00-00', '0000-00-00'),
(483, '020.483', 'MELIK', 2, 0, '0000-00-00', '0000-00-00'),
(484, '020.484', 'LEO', 2, 0, '0000-00-00', '0000-00-00'),
(485, '020.485', 'CHRYSOLOGUS', 2, 0, '0000-00-00', '0000-00-00'),
(486, '020.486', 'LINA', 2, 0, '0000-00-00', '0000-00-00'),
(487, '020.487', 'JOSEPHINE', 2, 0, '0000-00-00', '0000-00-00'),
(488, '020.488', 'ZULYAH', 2, 0, '0000-00-00', '0000-00-00'),
(489, '020.489', 'DINAH', 2, 0, '0000-00-00', '0000-00-00'),
(490, '020.490', 'MUHAMMAD', 2, 0, '0000-00-00', '0000-00-00'),
(491, '020.491', 'ATIRAH', 2, 0, '0000-00-00', '0000-00-00'),
(492, '020.492', 'PEPPI', 2, 0, '0000-00-00', '0000-00-00'),
(493, '020.493', 'ERIYANTO', 2, 0, '0000-00-00', '0000-00-00'),
(494, '020.494', 'THERESIA', 2, 0, '0000-00-00', '0000-00-00'),
(495, '020.495', 'LISA', 2, 0, '0000-00-00', '0000-00-00'),
(496, '020.496', 'VINDYARNI', 2, 0, '0000-00-00', '0000-00-00'),
(497, '020.497', 'HARIJIN', 2, 0, '0000-00-00', '0000-00-00'),
(498, '020.498', 'A', 2, 0, '0000-00-00', '0000-00-00'),
(499, '020.499', 'ANTONIUS', 2, 0, '0000-00-00', '0000-00-00'),
(500, '020.500', 'ASTRID', 2, 0, '0000-00-00', '0000-00-00'),
(501, '020.501', 'KARLIMAN', 2, 0, '0000-00-00', '0000-00-00'),
(502, '020.502', 'WENDY', 2, 0, '0000-00-00', '0000-00-00'),
(503, '020.503', 'OKING', 2, 0, '0000-00-00', '0000-00-00'),
(504, '020.504', 'FENTY', 2, 0, '0000-00-00', '0000-00-00'),
(505, '020.505', 'MARCIO', 2, 0, '0000-00-00', '0000-00-00'),
(506, '020.506', 'BENYAMIN', 2, 0, '0000-00-00', '0000-00-00'),
(507, '020.507', 'PENI', 2, 0, '0000-00-00', '0000-00-00'),
(508, '020.508', 'GUNAWAN', 2, 0, '0000-00-00', '0000-00-00'),
(509, '020.509', 'NORMANSYAH', 2, 0, '0000-00-00', '0000-00-00'),
(510, '020.510', 'SEPTI', 2, 0, '0000-00-00', '0000-00-00'),
(511, '020.511', 'YUDEA', 2, 0, '0000-00-00', '0000-00-00'),
(512, '020.512', 'SURYADI', 2, 0, '0000-00-00', '0000-00-00'),
(513, '020.513', 'IVAN', 2, 0, '0000-00-00', '0000-00-00'),
(514, '020.514', 'AMIN', 2, 0, '0000-00-00', '0000-00-00'),
(515, '020.515', 'SUPANDI', 2, 0, '0000-00-00', '0000-00-00'),
(516, '020.516', 'BUDIMAN', 2, 0, '0000-00-00', '0000-00-00'),
(517, '020.517', 'IRA', 2, 0, '0000-00-00', '0000-00-00'),
(518, '020.518', 'LILIES', 2, 0, '0000-00-00', '0000-00-00'),
(519, '020.519', 'CLARA', 2, 0, '0000-00-00', '0000-00-00'),
(520, '020.520', 'HOLILIR', 2, 0, '0000-00-00', '0000-00-00'),
(521, '020.521', 'H.', 2, 0, '0000-00-00', '0000-00-00'),
(522, '020.522', 'SRI', 2, 0, '0000-00-00', '0000-00-00'),
(523, '020.523', 'MAHADIKA', 2, 0, '0000-00-00', '0000-00-00'),
(524, '020.524', 'HARATUN', 2, 0, '0000-00-00', '0000-00-00'),
(525, '020.525', 'FX', 2, 0, '0000-00-00', '0000-00-00'),
(526, '020.526', 'ADE', 2, 0, '0000-00-00', '0000-00-00'),
(527, '020.527', 'ADIMAS', 2, 0, '0000-00-00', '0000-00-00'),
(528, '020.528', 'YURRI', 2, 0, '0000-00-00', '0000-00-00'),
(529, '020.529', 'CHRISTIN', 2, 0, '0000-00-00', '0000-00-00'),
(530, '020.530', 'FADRIAN', 2, 0, '0000-00-00', '0000-00-00'),
(531, '020.531', 'REINALD', 2, 0, '0000-00-00', '0000-00-00'),
(532, '020.532', 'HANDAYA', 2, 0, '0000-00-00', '0000-00-00'),
(533, '020.533', 'ERLINA', 2, 0, '0000-00-00', '0000-00-00'),
(534, '020.534', 'DEA', 2, 0, '0000-00-00', '0000-00-00'),
(535, '020.535', 'RR', 2, 0, '0000-00-00', '0000-00-00'),
(536, '020.536', 'ROSELLINIA', 2, 0, '0000-00-00', '0000-00-00'),
(537, '020.537', 'JOSHI', 2, 0, '0000-00-00', '0000-00-00'),
(538, '020.538', 'YUSUF', 2, 0, '0000-00-00', '0000-00-00'),
(539, '020.539', 'SUKMO', 2, 0, '0000-00-00', '0000-00-00'),
(540, '020.540', 'SANTOSO', 2, 0, '0000-00-00', '0000-00-00'),
(541, '020.541', 'ENDI', 2, 0, '0000-00-00', '0000-00-00'),
(542, '020.542', 'ASTRID', 2, 0, '0000-00-00', '0000-00-00'),
(543, '020.543', 'REZA', 2, 0, '0000-00-00', '0000-00-00'),
(544, '020.544', 'SUKIYO', 2, 0, '0000-00-00', '0000-00-00'),
(545, '020.545', 'RIZKY', 2, 0, '0000-00-00', '0000-00-00'),
(546, '020.546', 'RM', 2, 0, '0000-00-00', '0000-00-00'),
(547, '020.547', 'KRISNO', 2, 0, '0000-00-00', '0000-00-00'),
(548, '020.548', 'FEBRI', 2, 0, '0000-00-00', '0000-00-00'),
(549, '020.549', 'IRWAN', 2, 0, '0000-00-00', '0000-00-00'),
(550, '020.550', 'FELICIA', 2, 0, '0000-00-00', '0000-00-00'),
(551, '020.551', 'SANDRAWATI', 2, 0, '0000-00-00', '0000-00-00'),
(552, '020.552', 'EDY', 2, 0, '0000-00-00', '0000-00-00'),
(553, '020.553', 'AGUSTINUS', 2, 0, '0000-00-00', '0000-00-00'),
(554, '020.554', 'SURIPTO', 2, 0, '0000-00-00', '0000-00-00'),
(555, '020.555', 'RITHA', 2, 0, '0000-00-00', '0000-00-00'),
(556, '020.556', 'ASTRID', 2, 0, '0000-00-00', '0000-00-00'),
(557, '020.557', 'PEGGY', 2, 0, '0000-00-00', '0000-00-00'),
(558, '020.558', 'AFRIYANTI', 2, 0, '0000-00-00', '0000-00-00'),
(559, '020.559', 'SOFIA', 2, 0, '0000-00-00', '0000-00-00'),
(560, '020.560', 'SANTOSO', 2, 0, '0000-00-00', '0000-00-00'),
(561, '020.561', 'HENDRIK', 2, 0, '0000-00-00', '0000-00-00'),
(562, '020.562', 'RITA', 2, 0, '0000-00-00', '0000-00-00'),
(563, '020.563', 'RULLY', 2, 0, '0000-00-00', '0000-00-00'),
(564, '020.564', 'RITA', 2, 0, '0000-00-00', '0000-00-00'),
(565, '020.565', 'KARINA', 2, 0, '0000-00-00', '0000-00-00'),
(566, '020.566', 'FARIAL', 2, 0, '0000-00-00', '0000-00-00'),
(567, '020.567', 'ALI', 2, 0, '0000-00-00', '0000-00-00'),
(568, '020.568', 'JULIA', 2, 0, '0000-00-00', '0000-00-00'),
(569, '020.569', 'ERNA', 2, 0, '0000-00-00', '0000-00-00'),
(570, '020.570', 'OCTAVIANUS', 2, 0, '0000-00-00', '0000-00-00'),
(571, '020.571', 'DARREN', 2, 0, '0000-00-00', '0000-00-00'),
(572, '020.572', 'SUWANTARA', 2, 0, '0000-00-00', '0000-00-00'),
(573, '020.573', 'FELICIA', 2, 0, '0000-00-00', '0000-00-00'),
(574, '020.574', 'SANDY', 2, 0, '0000-00-00', '0000-00-00'),
(575, '020.575', 'AHMAD', 2, 0, '0000-00-00', '0000-00-00'),
(576, '020.576', 'BUDIYANTO', 2, 0, '0000-00-00', '0000-00-00'),
(577, '020.577', 'LIA', 2, 0, '0000-00-00', '0000-00-00'),
(578, '020.578', 'SUNATA', 2, 0, '0000-00-00', '0000-00-00'),
(579, '020.579', 'SURYANTO', 2, 0, '0000-00-00', '0000-00-00'),
(580, '020.580', 'MARGARET', 2, 0, '0000-00-00', '0000-00-00'),
(581, '020.581', 'ZAINAL', 2, 0, '0000-00-00', '0000-00-00'),
(582, '020.582', 'DEWI', 2, 0, '0000-00-00', '0000-00-00'),
(583, '020.583', 'NICOLAS', 2, 0, '0000-00-00', '0000-00-00'),
(584, '020.584', 'MADDA', 2, 0, '0000-00-00', '0000-00-00'),
(585, '020.585', 'CAHYANTI', 2, 0, '0000-00-00', '0000-00-00'),
(586, '020.586', 'CAHYANTI', 2, 0, '0000-00-00', '0000-00-00'),
(587, '020.587', 'RUSSEL', 2, 0, '0000-00-00', '0000-00-00'),
(588, '020.588', 'ZAINAL', 2, 0, '0000-00-00', '0000-00-00'),
(589, '020.589', 'IRMA', 2, 0, '0000-00-00', '0000-00-00'),
(590, '020.590', 'SUSAN', 2, 0, '0000-00-00', '0000-00-00'),
(591, '020.591', 'DITO', 2, 0, '0000-00-00', '0000-00-00'),
(592, '020.592', 'HINARTO', 2, 0, '0000-00-00', '0000-00-00'),
(593, '020.593', 'SUNDARI', 2, 0, '0000-00-00', '0000-00-00'),
(594, '020.594', 'ADITYA', 2, 0, '0000-00-00', '0000-00-00'),
(595, '020.595', 'JOEDIANTO', 2, 0, '0000-00-00', '0000-00-00'),
(596, '020.596', 'AMAD', 2, 0, '0000-00-00', '0000-00-00'),
(597, '020.597', 'LIAH', 2, 0, '0000-00-00', '0000-00-00'),
(598, '020.598', 'JEREMY', 2, 0, '0000-00-00', '0000-00-00'),
(599, '020.599', 'FEBRINO', 2, 0, '0000-00-00', '0000-00-00'),
(600, '020.600', 'JEFFRY', 2, 0, '0000-00-00', '0000-00-00'),
(601, '020.601', 'A', 2, 0, '0000-00-00', '0000-00-00'),
(602, '020.602', 'RAHMAT', 2, 0, '0000-00-00', '0000-00-00'),
(603, '020.603', 'TRESY', 2, 0, '0000-00-00', '0000-00-00'),
(604, '020.604', 'LILIYANA', 2, 0, '0000-00-00', '0000-00-00'),
(605, '020.605', 'KRISTINA', 2, 0, '0000-00-00', '0000-00-00'),
(606, '020.606', 'YUNITA', 2, 0, '0000-00-00', '0000-00-00'),
(607, '020.607', 'IRZA', 2, 0, '0000-00-00', '0000-00-00'),
(608, '020.608', 'TITA', 2, 0, '0000-00-00', '0000-00-00'),
(609, '020.609', 'SUDARTO', 2, 0, '0000-00-00', '0000-00-00'),
(610, '020.610', 'FAIBY', 2, 0, '0000-00-00', '0000-00-00'),
(611, '020.611', 'PARULIAN', 2, 0, '0000-00-00', '0000-00-00'),
(612, '020.612', 'ANTONIUS', 2, 0, '0000-00-00', '0000-00-00'),
(613, '020.613', 'SUPIADI', 2, 0, '0000-00-00', '0000-00-00'),
(614, '020.614', 'MELINDA', 2, 0, '0000-00-00', '0000-00-00'),
(615, '020.615', 'SOEPARNO', 2, 0, '0000-00-00', '0000-00-00'),
(616, '020.616', 'MARDI', 2, 0, '0000-00-00', '0000-00-00'),
(617, '020.617', 'ROY', 2, 0, '0000-00-00', '0000-00-00'),
(618, '020.618', 'SHANTICA', 2, 0, '0000-00-00', '0000-00-00'),
(619, '020.619', 'ARDIARINA', 2, 0, '0000-00-00', '0000-00-00'),
(620, '020.620', 'IRAWATI', 2, 0, '0000-00-00', '0000-00-00'),
(621, '020.621', 'HERIYADI', 2, 0, '0000-00-00', '0000-00-00'),
(622, '020.622', 'LYDIA', 2, 0, '0000-00-00', '0000-00-00'),
(623, '020.623', 'MONA', 2, 0, '0000-00-00', '0000-00-00'),
(624, '020.624', 'L', 2, 0, '0000-00-00', '0000-00-00'),
(625, '020.625', 'ATIKA', 2, 0, '0000-00-00', '0000-00-00'),
(626, '020.626', 'KENNETH', 2, 0, '0000-00-00', '0000-00-00'),
(627, '020.627', 'JOHNNY', 2, 0, '0000-00-00', '0000-00-00'),
(628, '020.628', 'ANASTASIA', 2, 0, '0000-00-00', '0000-00-00'),
(629, '020.629', 'DIAH', 2, 0, '0000-00-00', '0000-00-00'),
(630, '020.630', 'ANDI', 2, 0, '0000-00-00', '0000-00-00'),
(631, '020.631', 'RUTH', 2, 0, '0000-00-00', '0000-00-00'),
(632, '020.632', 'ADIK', 2, 0, '0000-00-00', '0000-00-00'),
(633, '020.633', 'SONNY', 2, 0, '0000-00-00', '0000-00-00'),
(634, '020.634', 'SRI', 2, 0, '0000-00-00', '0000-00-00'),
(635, '020.635', 'LINDRAWATI', 2, 0, '0000-00-00', '0000-00-00'),
(636, '020.636', 'CHARLES', 2, 0, '0000-00-00', '0000-00-00'),
(637, '020.637', 'GAFUR', 2, 0, '0000-00-00', '0000-00-00'),
(638, '020.638', 'SOETIKNO', 2, 0, '0000-00-00', '0000-00-00'),
(639, '020.639', 'RIFKI', 2, 0, '0000-00-00', '0000-00-00'),
(640, '020.640', 'JOLANDA', 2, 0, '0000-00-00', '0000-00-00'),
(641, '020.641', 'NOVI', 2, 0, '0000-00-00', '0000-00-00'),
(642, '020.642', 'TEGUH', 2, 0, '0000-00-00', '0000-00-00'),
(643, '020.643', 'HARTONO', 2, 0, '0000-00-00', '0000-00-00'),
(644, '020.644', 'BERNARDUS', 2, 0, '0000-00-00', '0000-00-00'),
(645, '020.645', 'SARJONO', 2, 0, '0000-00-00', '0000-00-00'),
(646, '020.646', 'Y', 2, 0, '0000-00-00', '0000-00-00'),
(647, '020.647', 'JATI', 2, 0, '0000-00-00', '0000-00-00'),
(648, '020.648', 'SATRI', 2, 0, '0000-00-00', '0000-00-00'),
(649, '020.649', 'SOERYONO', 2, 0, '0000-00-00', '0000-00-00'),
(650, '020.650', 'GOUW', 2, 0, '0000-00-00', '0000-00-00'),
(651, '020.651', 'ABIPRAYADI', 2, 0, '0000-00-00', '0000-00-00'),
(652, '020.652', 'HERMAN', 2, 0, '0000-00-00', '0000-00-00'),
(653, '020.653', 'HARRY', 2, 0, '0000-00-00', '0000-00-00'),
(654, '020.654', 'IKA', 2, 0, '0000-00-00', '0000-00-00'),
(655, '020.655', 'MAHATMA', 2, 0, '0000-00-00', '0000-00-00'),
(656, '020.656', 'SUTANTO', 2, 0, '0000-00-00', '0000-00-00'),
(657, '020.657', 'PRADANA', 2, 0, '0000-00-00', '0000-00-00'),
(658, '020.658', 'FARDIANSYAH', 2, 0, '0000-00-00', '0000-00-00'),
(659, '020.659', 'BUDI', 2, 0, '0000-00-00', '0000-00-00'),
(660, '020.660', 'SHANTY', 2, 0, '0000-00-00', '0000-00-00'),
(661, '020.661', 'ARVIATI', 2, 0, '0000-00-00', '0000-00-00'),
(662, '020.662', 'ANDRIANI', 2, 0, '0000-00-00', '0000-00-00'),
(663, '020.663', 'ANANTO', 2, 0, '0000-00-00', '0000-00-00'),
(664, '020.664', 'SITI', 2, 0, '0000-00-00', '0000-00-00'),
(665, '020.665', 'TOMY', 2, 0, '0000-00-00', '0000-00-00'),
(666, '020.666', 'LAKSMI', 2, 0, '0000-00-00', '0000-00-00'),
(667, '020.667', 'MALVIN', 2, 0, '0000-00-00', '0000-00-00'),
(668, '020.668', 'NOVA', 2, 0, '0000-00-00', '0000-00-00'),
(669, '020.669', 'LIE', 2, 0, '0000-00-00', '0000-00-00'),
(670, '020.670', 'JOSHUA', 2, 0, '0000-00-00', '0000-00-00'),
(671, '020.671', 'IKA', 2, 0, '0000-00-00', '0000-00-00'),
(672, '020.672', 'ITA', 2, 0, '0000-00-00', '0000-00-00'),
(673, '020.673', 'DINI', 2, 0, '0000-00-00', '0000-00-00'),
(674, '020.674', 'ADRI', 2, 0, '0000-00-00', '0000-00-00'),
(675, '020.675', 'ANDANG', 2, 0, '0000-00-00', '0000-00-00'),
(676, '020.676', 'LILY', 2, 0, '0000-00-00', '0000-00-00'),
(677, '020.677', 'IBNU', 2, 0, '0000-00-00', '0000-00-00'),
(678, '020.678', 'THIO', 2, 0, '0000-00-00', '0000-00-00'),
(679, '020.679', 'YABES', 2, 0, '0000-00-00', '0000-00-00'),
(680, '020.680', 'ARIEL', 2, 0, '0000-00-00', '0000-00-00'),
(681, '020.681', 'IDHAM', 2, 0, '0000-00-00', '0000-00-00'),
(682, '020.682', 'RENNY', 2, 0, '0000-00-00', '0000-00-00'),
(683, '020.683', 'LELI', 2, 0, '0000-00-00', '0000-00-00'),
(684, '020.684', 'BRAMANTYO', 2, 0, '0000-00-00', '0000-00-00'),
(685, '020.685', 'PRIYO', 2, 0, '0000-00-00', '0000-00-00'),
(686, '020.686', 'SHESHA', 2, 0, '0000-00-00', '0000-00-00'),
(687, '020.687', 'ALWIN', 2, 0, '0000-00-00', '0000-00-00'),
(688, '020.688', 'RAYMOND', 2, 0, '0000-00-00', '0000-00-00'),
(689, '020.689', 'OLFIANA', 2, 0, '0000-00-00', '0000-00-00'),
(690, '020.690', 'YUDHA', 2, 0, '0000-00-00', '0000-00-00'),
(691, '020.691', 'ARIE', 2, 0, '0000-00-00', '0000-00-00'),
(692, '020.692', 'DEWI', 2, 0, '0000-00-00', '0000-00-00'),
(693, '020.693', 'ARNOLD', 2, 0, '0000-00-00', '0000-00-00'),
(694, '020.694', 'VALENCIENES', 2, 0, '0000-00-00', '0000-00-00'),
(695, '020.695', 'IMELDA', 2, 0, '0000-00-00', '0000-00-00'),
(696, '020.696', 'LIZA', 2, 0, '0000-00-00', '0000-00-00'),
(697, '020.697', 'LODY', 2, 0, '0000-00-00', '0000-00-00'),
(698, '020.698', 'ANANG', 2, 0, '0000-00-00', '0000-00-00'),
(699, '020.699', 'JANTINI', 2, 0, '0000-00-00', '0000-00-00'),
(700, '020.700', 'PURWANTO', 2, 0, '0000-00-00', '0000-00-00'),
(701, '020.701', 'ANDRI', 2, 0, '0000-00-00', '0000-00-00'),
(702, '020.702', 'HANIFAL', 2, 0, '0000-00-00', '0000-00-00'),
(703, '020.703', 'SRI', 2, 0, '0000-00-00', '0000-00-00'),
(704, '020.704', 'HARI', 2, 0, '0000-00-00', '0000-00-00'),
(705, '020.705', 'HARDI', 2, 0, '0000-00-00', '0000-00-00'),
(706, '020.706', 'GRACE', 2, 0, '0000-00-00', '0000-00-00'),
(707, '020.707', 'SUGENG', 2, 0, '0000-00-00', '0000-00-00'),
(708, '020.708', 'NURUS', 2, 0, '0000-00-00', '0000-00-00'),
(709, '020.709', 'DANIEL', 2, 0, '0000-00-00', '0000-00-00'),
(710, '020.710', 'IWAN', 2, 0, '0000-00-00', '0000-00-00'),
(711, '020.711', 'WIDJANA', 2, 0, '0000-00-00', '0000-00-00'),
(712, '020.712', 'ANY', 2, 0, '0000-00-00', '0000-00-00'),
(713, '020.713', 'SOFIA', 2, 0, '0000-00-00', '0000-00-00'),
(714, '020.714', 'NICKO', 2, 0, '0000-00-00', '0000-00-00'),
(715, '020.715', 'MOHAMMAD', 2, 0, '0000-00-00', '0000-00-00'),
(716, '020.716', 'HERY', 2, 0, '0000-00-00', '0000-00-00'),
(717, '020.717', 'SITI', 2, 0, '0000-00-00', '0000-00-00'),
(718, '020.718', 'IMAM', 2, 0, '0000-00-00', '0000-00-00'),
(719, '020.719', 'NATALIA', 2, 0, '0000-00-00', '0000-00-00'),
(720, '020.720', 'EDI', 2, 0, '0000-00-00', '0000-00-00'),
(721, '020.721', 'AGUNG', 2, 0, '0000-00-00', '0000-00-00'),
(722, '020.722', 'CHESA', 2, 0, '0000-00-00', '0000-00-00'),
(723, '020.723', 'FIRHANSYAH', 2, 0, '0000-00-00', '0000-00-00'),
(724, '020.724', 'FADLI', 2, 0, '0000-00-00', '0000-00-00'),
(725, '020.725', 'JAYA', 2, 0, '0000-00-00', '0000-00-00'),
(726, '020.726', 'FATIHA', 2, 0, '0000-00-00', '0000-00-00'),
(727, '020.727', 'DEWI', 2, 0, '0000-00-00', '0000-00-00'),
(728, '020.728', 'ACHMAD', 2, 0, '0000-00-00', '0000-00-00'),
(729, '020.729', 'ANGGORO', 2, 0, '0000-00-00', '0000-00-00'),
(730, '020.730', 'MARJORIE', 2, 0, '0000-00-00', '0000-00-00'),
(731, '020.731', 'ALI', 2, 0, '0000-00-00', '0000-00-00'),
(732, '020.732', 'KEVIN', 2, 0, '0000-00-00', '0000-00-00'),
(733, '020.733', 'ALVIN', 2, 0, '0000-00-00', '0000-00-00'),
(734, '020.734', 'DESY', 2, 0, '0000-00-00', '0000-00-00'),
(735, '020.735', 'SAYED', 2, 0, '0000-00-00', '0000-00-00'),
(736, '020.736', 'PINARDI', 2, 0, '0000-00-00', '0000-00-00'),
(737, '020.737', 'ACHMAD', 2, 0, '0000-00-00', '0000-00-00'),
(738, '020.738', 'ADE', 2, 0, '0000-00-00', '0000-00-00'),
(739, '020.739', 'MARIA', 2, 0, '0000-00-00', '0000-00-00'),
(740, '020.740', 'ANDINE', 2, 0, '0000-00-00', '0000-00-00'),
(741, '020.741', 'PUSPITA', 2, 0, '0000-00-00', '0000-00-00'),
(742, '020.742', 'KARIM', 2, 0, '0000-00-00', '0000-00-00'),
(743, '020.743', 'REZKY', 2, 0, '0000-00-00', '0000-00-00'),
(744, '020.744', 'MAYA', 2, 0, '0000-00-00', '0000-00-00'),
(745, '020.745', 'ELIS', 2, 0, '0000-00-00', '0000-00-00'),
(746, '020.746', 'JULIANI', 2, 0, '0000-00-00', '0000-00-00'),
(747, '020.747', 'JAHJA', 2, 0, '0000-00-00', '0000-00-00'),
(748, '020.748', 'YOHAN', 2, 0, '0000-00-00', '0000-00-00'),
(749, '020.749', 'HARYOTO', 2, 0, '0000-00-00', '0000-00-00'),
(750, '020.750', 'TRI', 2, 0, '0000-00-00', '0000-00-00'),
(751, '020.751', 'EUIS', 2, 0, '0000-00-00', '0000-00-00'),
(752, '020.752', 'LILY', 2, 0, '0000-00-00', '0000-00-00'),
(753, '020.753', 'RERRI', 2, 0, '0000-00-00', '0000-00-00'),
(754, '020.754', 'ELLYA', 2, 0, '0000-00-00', '0000-00-00'),
(755, '020.755', 'YUSUP', 2, 0, '0000-00-00', '0000-00-00'),
(756, '020.756', 'YUDI', 2, 0, '0000-00-00', '0000-00-00'),
(757, '020.757', 'HERMAWAN', 2, 0, '0000-00-00', '0000-00-00'),
(758, '020.758', 'NAZLI', 2, 0, '0000-00-00', '0000-00-00'),
(759, '020.759', 'CYINTHIA', 2, 0, '0000-00-00', '0000-00-00'),
(760, '020.760', 'HARIO', 2, 0, '0000-00-00', '0000-00-00'),
(761, '020.761', 'DEWI', 2, 0, '0000-00-00', '0000-00-00'),
(762, '020.762', 'ANA', 2, 0, '0000-00-00', '0000-00-00'),
(763, '020.763', 'M', 2, 0, '0000-00-00', '0000-00-00'),
(764, '020.764', 'ASRI', 2, 0, '0000-00-00', '0000-00-00'),
(765, '020.765', 'ANG', 2, 0, '0000-00-00', '0000-00-00'),
(766, '020.766', 'AGUSTIN', 2, 0, '0000-00-00', '0000-00-00'),
(767, '020.767', 'JAYA', 2, 0, '0000-00-00', '0000-00-00'),
(768, '020.768', 'DESI', 2, 0, '0000-00-00', '0000-00-00'),
(769, '020.769', 'HARI', 2, 0, '0000-00-00', '0000-00-00'),
(770, '020.770', 'HERNI', 2, 0, '0000-00-00', '0000-00-00'),
(771, '020.771', 'SITI', 2, 0, '0000-00-00', '0000-00-00'),
(772, '020.772', 'UNTUNG', 2, 0, '0000-00-00', '0000-00-00'),
(773, '020.773', 'BERTHOLD', 2, 0, '0000-00-00', '0000-00-00'),
(774, '020.774', 'TEDDY', 2, 0, '0000-00-00', '0000-00-00'),
(775, '020.775', 'R', 2, 0, '0000-00-00', '0000-00-00'),
(776, '020.776', 'SYAMSUL', 2, 0, '0000-00-00', '0000-00-00'),
(777, '020.777', 'IRA', 2, 0, '0000-00-00', '0000-00-00'),
(778, '020.778', 'DIAN', 2, 0, '0000-00-00', '0000-00-00'),
(779, '020.779', 'NELVITIRIZA', 2, 0, '0000-00-00', '0000-00-00'),
(780, '020.780', 'DEATRI', 2, 0, '0000-00-00', '0000-00-00'),
(781, '020.781', 'LISA', 2, 0, '0000-00-00', '0000-00-00'),
(782, '020.782', 'DIAN', 2, 0, '0000-00-00', '0000-00-00'),
(783, '020.783', 'MOHAMAD', 2, 0, '0000-00-00', '0000-00-00'),
(784, '020.784', 'PATRICK', 2, 0, '0000-00-00', '0000-00-00'),
(785, '020.785', 'HESTININGTIJAS', 2, 0, '0000-00-00', '0000-00-00'),
(786, '020.786', 'NADIA', 2, 0, '0000-00-00', '0000-00-00'),
(787, '020.787', 'THOMAS', 2, 0, '0000-00-00', '0000-00-00'),
(788, '020.788', 'LANAWATI', 2, 0, '0000-00-00', '0000-00-00'),
(789, '020.789', 'GILMAN', 2, 0, '0000-00-00', '0000-00-00'),
(790, '020.790', 'MERRY', 2, 0, '0000-00-00', '0000-00-00'),
(791, '020.791', 'INA', 2, 0, '0000-00-00', '0000-00-00'),
(792, '020.792', 'DODY', 2, 0, '0000-00-00', '0000-00-00'),
(793, '020.793', 'SYAFRIDA', 2, 0, '0000-00-00', '0000-00-00'),
(794, '020.794', 'INKA', 2, 0, '0000-00-00', '0000-00-00'),
(795, '020.795', 'DIAH', 2, 0, '0000-00-00', '0000-00-00'),
(796, '020.796', 'ARYOSO', 2, 0, '0000-00-00', '0000-00-00'),
(797, '020.797', 'JUWITA', 2, 0, '0000-00-00', '0000-00-00'),
(798, '020.798', 'HALIM', 2, 0, '0000-00-00', '0000-00-00'),
(799, '020.799', 'VERENA', 2, 0, '0000-00-00', '0000-00-00'),
(800, '020.800', 'GERARD', 2, 0, '0000-00-00', '0000-00-00'),
(801, '020.801', 'GERAL', 2, 0, '0000-00-00', '0000-00-00'),
(802, '020.802', 'LAY', 2, 0, '0000-00-00', '0000-00-00'),
(803, '020.803', 'IMELDA', 2, 0, '0000-00-00', '0000-00-00'),
(804, '020.804', 'MUHAMMAD', 2, 0, '0000-00-00', '0000-00-00'),
(805, '020.805', 'MONICA', 2, 0, '0000-00-00', '0000-00-00'),
(806, '020.806', 'ROSALINA', 2, 0, '0000-00-00', '0000-00-00'),
(807, '020.807', 'TJIA', 2, 0, '0000-00-00', '0000-00-00'),
(808, '020.808', 'JOSIA', 2, 0, '0000-00-00', '0000-00-00'),
(809, '020.809', 'TIANNI', 2, 0, '0000-00-00', '0000-00-00'),
(810, '020.810', 'GAHRANI', 2, 0, '0000-00-00', '0000-00-00'),
(811, '020.811', 'HERLIANA', 2, 0, '0000-00-00', '0000-00-00'),
(812, '020.812', 'LISA', 2, 0, '0000-00-00', '0000-00-00'),
(813, '020.813', 'MARGARIET', 2, 0, '0000-00-00', '0000-00-00'),
(814, '020.814', 'THE', 2, 0, '0000-00-00', '0000-00-00'),
(815, '020.815', 'FINNY', 2, 0, '0000-00-00', '0000-00-00'),
(816, '020.816', 'ANWAR', 2, 0, '0000-00-00', '0000-00-00'),
(817, '020.817', 'DWIANTO', 2, 0, '0000-00-00', '0000-00-00'),
(818, '020.818', 'SANTOSO', 2, 0, '0000-00-00', '0000-00-00'),
(819, '020.819', 'MELIN', 2, 0, '0000-00-00', '0000-00-00'),
(820, '020.820', 'TOTONG', 2, 0, '0000-00-00', '0000-00-00'),
(821, '020.821', 'ADE', 2, 0, '0000-00-00', '0000-00-00'),
(822, '020.822', 'SITA', 2, 0, '0000-00-00', '0000-00-00'),
(823, '020.823', 'ANDRE', 2, 0, '0000-00-00', '0000-00-00'),
(824, '020.824', 'NADIA', 2, 0, '0000-00-00', '0000-00-00'),
(825, '020.825', 'JOHANNES', 2, 0, '0000-00-00', '0000-00-00'),
(826, '020.826', 'WIDI', 2, 0, '0000-00-00', '0000-00-00'),
(827, '020.827', 'ML', 2, 0, '0000-00-00', '0000-00-00'),
(828, '020.828', 'IDA', 2, 0, '0000-00-00', '0000-00-00'),
(829, '020.829', 'JEFFRY', 2, 0, '0000-00-00', '0000-00-00'),
(830, '020.830', 'MARWAN', 2, 0, '0000-00-00', '0000-00-00'),
(831, '020.831', 'ELLY', 2, 0, '0000-00-00', '0000-00-00'),
(832, '020.832', 'KARANIYA', 2, 0, '0000-00-00', '0000-00-00'),
(833, '020.833', 'AMELIA', 2, 0, '0000-00-00', '0000-00-00'),
(834, '020.834', 'EIN', 2, 0, '0000-00-00', '0000-00-00'),
(835, '020.835', 'ELISABETH', 2, 0, '0000-00-00', '0000-00-00'),
(836, '020.836', 'YURITA', 2, 0, '0000-00-00', '0000-00-00'),
(837, '020.837', 'DENY', 2, 0, '0000-00-00', '0000-00-00'),
(838, '020.838', 'BENING', 2, 0, '0000-00-00', '0000-00-00'),
(839, '020.839', 'TEGUH', 2, 0, '0000-00-00', '0000-00-00'),
(840, '020.840', 'DHIKA', 2, 0, '0000-00-00', '0000-00-00');
INSERT INTO `0_dimensions` (`id`, `reference`, `name`, `type_`, `closed`, `date_`, `due_date`) VALUES
(841, '020.841', 'RT', 2, 0, '0000-00-00', '0000-00-00'),
(842, '020.842', 'MAXIMILIAN', 2, 0, '0000-00-00', '0000-00-00'),
(843, '020.843', 'FRENGKY', 2, 0, '0000-00-00', '0000-00-00'),
(844, '020.844', 'ARSIANTO', 2, 0, '0000-00-00', '0000-00-00'),
(845, '020.845', 'MARIO', 2, 0, '0000-00-00', '0000-00-00'),
(846, '020.846', 'WAHJU', 2, 0, '0000-00-00', '0000-00-00'),
(847, '020.847', 'ROBIN', 2, 0, '0000-00-00', '0000-00-00'),
(848, '020.848', 'STELLA', 2, 0, '0000-00-00', '0000-00-00'),
(849, '020.849', 'RETNOASTUTI', 2, 0, '0000-00-00', '0000-00-00'),
(850, '020.850', 'DJOKO', 2, 0, '0000-00-00', '0000-00-00'),
(851, '020.851', 'TJOA', 2, 0, '0000-00-00', '0000-00-00'),
(852, '020.852', 'LYDIA', 2, 0, '0000-00-00', '0000-00-00'),
(853, '020.853', 'RAYMOND', 2, 0, '0000-00-00', '0000-00-00'),
(854, '020.854', 'I', 2, 0, '0000-00-00', '0000-00-00'),
(855, '020.855', 'F', 2, 0, '0000-00-00', '0000-00-00'),
(856, '020.856', 'HARNUGAMA', 2, 0, '0000-00-00', '0000-00-00'),
(857, '020.857', 'OKA', 2, 0, '0000-00-00', '0000-00-00'),
(858, '020.858', 'FRESKA', 2, 0, '0000-00-00', '0000-00-00'),
(859, '020.859', 'LIA', 2, 0, '0000-00-00', '0000-00-00'),
(860, '020.860', 'YOVITA', 2, 0, '0000-00-00', '0000-00-00'),
(861, '020.861', 'TUTIK', 2, 0, '0000-00-00', '0000-00-00'),
(862, '020.862', 'WAHYUTIK', 2, 0, '0000-00-00', '0000-00-00'),
(863, '020.863', 'DIANA', 2, 0, '0000-00-00', '0000-00-00'),
(864, '020.864', 'VIVI', 2, 0, '0000-00-00', '0000-00-00'),
(865, '020.865', 'I', 2, 0, '0000-00-00', '0000-00-00'),
(866, '020.866', 'MULIA', 2, 0, '0000-00-00', '0000-00-00'),
(867, '020.867', 'KADEK', 2, 0, '0000-00-00', '0000-00-00'),
(868, '020.868', 'CHOI', 2, 0, '0000-00-00', '0000-00-00'),
(869, '020.869', 'YADI', 2, 0, '0000-00-00', '0000-00-00'),
(870, '020.870', 'SUTINI', 2, 0, '0000-00-00', '0000-00-00'),
(871, '020.871', 'HARTINI', 2, 0, '0000-00-00', '0000-00-00'),
(872, '020.872', 'C.', 2, 0, '0000-00-00', '0000-00-00'),
(873, '020.873', 'JERRY', 2, 0, '0000-00-00', '0000-00-00'),
(874, '020.874', 'RATNA', 2, 0, '0000-00-00', '0000-00-00'),
(875, '020.875', 'YUKO', 2, 0, '0000-00-00', '0000-00-00'),
(876, '020.876', 'EDISON', 2, 0, '0000-00-00', '0000-00-00'),
(877, '020.877', 'GOH', 2, 0, '0000-00-00', '0000-00-00'),
(878, '020.878', 'JEFFRY', 2, 0, '0000-00-00', '0000-00-00'),
(879, '020.879', 'PETRINA', 2, 0, '0000-00-00', '0000-00-00'),
(880, '020.880', 'MAH', 2, 0, '0000-00-00', '0000-00-00'),
(881, '020.881', 'SALINA', 2, 0, '0000-00-00', '0000-00-00'),
(882, '020.882', 'DJONNY', 2, 0, '0000-00-00', '0000-00-00'),
(883, '020.883', 'SENJAYA', 2, 0, '0000-00-00', '0000-00-00'),
(884, '020.884', 'SENJAYA', 2, 0, '0000-00-00', '0000-00-00'),
(885, '020.885', 'HARRIS', 2, 0, '0000-00-00', '0000-00-00'),
(886, '020.886', 'ERWIN', 2, 0, '0000-00-00', '0000-00-00'),
(887, '020.887', 'MAHFUD', 2, 0, '0000-00-00', '0000-00-00'),
(888, '020.888', 'DWI', 2, 0, '0000-00-00', '0000-00-00'),
(889, '020.889', 'M', 2, 0, '0000-00-00', '0000-00-00'),
(890, '020.890', 'ROY', 2, 0, '0000-00-00', '0000-00-00'),
(891, '020.891', 'IRAWATI', 2, 0, '0000-00-00', '0000-00-00'),
(892, '020.892', 'M.', 2, 0, '0000-00-00', '0000-00-00'),
(893, '020.893', 'NIKI', 2, 0, '0000-00-00', '0000-00-00'),
(894, '020.894', 'CHRISTOPHER', 2, 0, '0000-00-00', '0000-00-00'),
(895, '020.1784', 'JONI', 2, 0, '0000-00-00', '0000-00-00'),
(896, '020.1785', 'HENDRI', 2, 0, '0000-00-00', '0000-00-00'),
(897, '020.1786', 'REZA', 2, 0, '0000-00-00', '0000-00-00'),
(898, '020.1788', 'SANTOLIE', 2, 0, '0000-00-00', '0000-00-00'),
(899, '020.1789', 'EVY', 2, 0, '0000-00-00', '0000-00-00'),
(900, '020.1794', 'SUNATAN', 2, 0, '0000-00-00', '0000-00-00'),
(901, '020.2695', 'DANIEL', 2, 0, '0000-00-00', '0000-00-00'),
(902, '020.2696', 'STANLEY', 2, 0, '0000-00-00', '0000-00-00'),
(903, '020.2697', 'TIUR', 2, 0, '0000-00-00', '0000-00-00'),
(904, '020.2698', 'SURYA', 2, 0, '0000-00-00', '0000-00-00'),
(905, '020.3603', 'FIRMAN', 2, 0, '0000-00-00', '0000-00-00'),
(906, '020.3604', 'IMAN', 2, 0, '0000-00-00', '0000-00-00'),
(907, '020.3605', 'xxxx', 2, 0, '0000-00-00', '0000-00-00'),
(908, '020.4512', 'BANK TABUNGAN NEGARA', 2, 0, '0000-00-00', '0000-00-00'),
(909, '020.4513', 'STACO JASAPRATAMA PT', 2, 0, '0000-00-00', '0000-00-00'),
(910, '020.4514', 'BPJS KESEHATAN', 2, 0, '0000-00-00', '0000-00-00'),
(911, '020.4515', 'MANDIRI SEKURITAS PT', 2, 0, '0000-00-00', '0000-00-00'),
(912, '020.4516', 'BRANTWOOD INTERNATIONAL LIMITED', 2, 0, '0000-00-00', '0000-00-00'),
(913, '020.4517', 'CALAMANDER LIMITED', 2, 0, '0000-00-00', '0000-00-00'),
(914, '020.4518', 'DELMONT INVESTMENTS PTE LTD', 2, 0, '0000-00-00', '0000-00-00'),
(915, '020.4519', 'ENERCOM GLOBAL LIMITED', 2, 0, '0000-00-00', '0000-00-00'),
(916, '020.4520', 'FLEMINGTON ASSET MANAGEMENT LIMITED', 2, 0, '0000-00-00', '0000-00-00'),
(917, '020.4521', 'KRESNA GRAHA SEKURINDO PT', 2, 0, '0000-00-00', '0000-00-00'),
(918, '020.4522', 'LIMEX INDONESIA PT', 2, 0, '0000-00-00', '0000-00-00'),
(919, '020.4523', 'MATAHARI PUTRA PRIMA TBK PT', 2, 0, '0000-00-00', '0000-00-00'),
(920, '020.4524', 'MELFORT OVERSEAS CORPORATION', 2, 0, '0000-00-00', '0000-00-00'),
(921, '020.4525', 'MITRA CIPTA KREATIKA PT', 2, 0, '0000-00-00', '0000-00-00'),
(922, '020.4526', 'MULTIPOLAR TBK PT', 2, 0, '0000-00-00', '0000-00-00'),
(923, '020.4527', 'NISP SEKURITAS', 2, 0, '0000-00-00', '0000-00-00'),
(924, '020.4528', 'PADMA INVESTMENT PTE LTD', 2, 0, '0000-00-00', '0000-00-00'),
(925, '020.4529', 'PERUM PERHUTANI', 2, 0, '0000-00-00', '0000-00-00'),
(926, '020.4530', 'PRUDENT CAPITAL LIMITED', 2, 0, '0000-00-00', '0000-00-00'),
(927, '020.4531', 'PT JAMSOSTEK', 2, 0, '0000-00-00', '0000-00-00'),
(928, '020.4532', 'REASURANSI NASIONAL INDONESIA PT', 2, 0, '0000-00-00', '0000-00-00'),
(929, '020.4533', 'RIYADI REKSASE NUSANTARA PT', 2, 0, '0000-00-00', '0000-00-00'),
(930, '020.4534', 'SRIBOGA RATURAYA PT', 2, 0, '0000-00-00', '0000-00-00'),
(931, '020.4535', 'CIPTADANA SEKURITAS PT', 2, 0, '0000-00-00', '0000-00-00'),
(932, '020.4536', 'SINARMAS SEKURITAS 1 PT', 2, 0, '0000-00-00', '0000-00-00'),
(933, '020.4537', 'PHILADEL TERRA LESTARI PT', 2, 0, '0000-00-00', '0000-00-00'),
(934, '020.4538', 'KOMPAS MEDIA NUSANTARA PT', 2, 0, '0000-00-00', '0000-00-00'),
(935, '020.4539', 'NIAGA MANAJEMEN CITRA PT', 2, 0, '0000-00-00', '0000-00-00'),
(936, '020.4540', 'CIPTADANA SEKURITAS 2', 2, 0, '0000-00-00', '0000-00-00'),
(937, '020.4541', 'BANK JULIUS BAER CO LTD SWITZERLAND', 2, 0, '0000-00-00', '0000-00-00'),
(938, '020.4542', 'YAYASAN KESEJAHTERAAN KARYAWAN BANK INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(939, '020.4543', 'YAYASAN KESEJAHTERAAN PEKERJA BRI', 2, 0, '0000-00-00', '0000-00-00'),
(940, '020.4544', 'YAYASAN KESEHATAN BANK MANDIRI', 2, 0, '0000-00-00', '0000-00-00'),
(941, '020.4545', 'YAYASAN KESEHATAN PEGAWAI TELKOM', 2, 0, '0000-00-00', '0000-00-00'),
(942, '020.4546', 'YAYASAN MITRA SEJAHTERA', 2, 0, '0000-00-00', '0000-00-00'),
(943, '020.4547', 'YAYASAN BAKTI BCA', 2, 0, '0000-00-00', '0000-00-00'),
(944, '020.4548', 'BANK JULIUS BAER CO LTD', 2, 0, '0000-00-00', '0000-00-00'),
(945, '020.4549', 'MNC LIFE ASSURANCE PT', 2, 0, '0000-00-00', '0000-00-00'),
(946, '020.4550', 'DANA PENSIUN DIREKSI DAN KARYAWAN ASURANSI PAROLAMAS', 2, 0, '0000-00-00', '0000-00-00'),
(947, '020.4551', 'DANA PENSIUN HUTAMA KARYA', 2, 0, '0000-00-00', '0000-00-00'),
(948, '020.4552', 'DANA PENSIUN IGLAS', 2, 0, '0000-00-00', '0000-00-00'),
(949, '020.4553', 'DANA PENSIUN IPTN', 2, 0, '0000-00-00', '0000-00-00'),
(950, '020.4554', 'DANA PENSIUN KARYAWAN PUPUK KUJANG', 2, 0, '0000-00-00', '0000-00-00'),
(951, '020.4555', 'DANA PENSIUN KIMIA FARMA', 2, 0, '0000-00-00', '0000-00-00'),
(952, '020.4556', 'DANA PENSIUN LEN INDUSTRI', 2, 0, '0000-00-00', '0000-00-00'),
(953, '020.4557', 'DANA PENSIUN LKBN ANTARA', 2, 0, '0000-00-00', '0000-00-00'),
(954, '020.4558', 'DANA PENSIUN PPPK PETRA', 2, 0, '0000-00-00', '0000-00-00'),
(955, '020.4559', 'DANA PENSIUN TIGARAKSA SATRIA', 2, 0, '0000-00-00', '0000-00-00'),
(956, '020.4560', 'DANA PENSIUN TIRTA NUSANTARA', 2, 0, '0000-00-00', '0000-00-00'),
(957, '020.4561', 'DANA PENSIUN WIJAYA KARYA', 2, 0, '0000-00-00', '0000-00-00'),
(958, '020.4562', 'DANA PENSIUN PEGAWAI UNIV MUHAMMADIYAH MALANG', 2, 0, '0000-00-00', '0000-00-00'),
(959, '020.4563', 'RODA MITRA LESTARI CV', 2, 0, '0000-00-00', '0000-00-00'),
(960, '020.4564', 'SUCORINVEST CENTRAL GANI PT', 2, 0, '0000-00-00', '0000-00-00'),
(961, '020.4565', 'KANTOR WALIGEREJA INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(962, '020.4566', 'AJB BUMIPUTERA 1912', 2, 0, '0000-00-00', '0000-00-00'),
(963, '020.4567', 'DANA PENSIUN PEGAWAI PERUM PERURI', 2, 0, '0000-00-00', '0000-00-00'),
(964, '020.4568', 'BANK QNB INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(965, '020.4569', 'ASURANSI SIMAS NET PT', 2, 0, '0000-00-00', '0000-00-00'),
(966, '020.4570', 'EKATRIADI KUSUMA PT', 2, 0, '0000-00-00', '0000-00-00'),
(967, '020.4571', 'ASURANSI JIWASRAYA PT', 2, 0, '0000-00-00', '0000-00-00'),
(968, '020.4572', 'ASABRI PERSERO PT', 2, 0, '0000-00-00', '0000-00-00'),
(969, '020.4573', 'ANGKASA PURA I PERSERO PT', 2, 0, '0000-00-00', '0000-00-00'),
(970, '020.4574', 'ASURANSI JIWA RECAPITAL PT', 2, 0, '0000-00-00', '0000-00-00'),
(971, '020.4575', 'ASURANSI KESEHATAN INDONESIA KOMERSIAL PT', 2, 0, '0000-00-00', '0000-00-00'),
(972, '020.4576', 'MULTIDAYA HUTAMA INDOKARUNIA PT', 2, 0, '0000-00-00', '0000-00-00'),
(973, '020.4577', 'GRAMEDIA PT', 2, 0, '0000-00-00', '0000-00-00'),
(974, '020.4578', 'ASURANSI JASA INDONESIA PT', 2, 0, '0000-00-00', '0000-00-00'),
(975, '020.4579', 'PERUSAHAAN PENGELOLA ASET PT', 2, 0, '0000-00-00', '0000-00-00'),
(976, '020.4580', 'ANGKASA PURA II PT', 2, 0, '0000-00-00', '0000-00-00'),
(977, '020.4581', 'MULTISTRADA ARAH SARANA TBK PT', 2, 0, '0000-00-00', '0000-00-00'),
(978, '020.4582', 'NOBLE INVESTMENT CORPORATION (SPC) LTD', 2, 0, '0000-00-00', '0000-00-00'),
(979, '020.4583', 'BURSA EFEK INDONESIA PT', 2, 0, '0000-00-00', '0000-00-00'),
(980, '020.4584', 'KAM CAPITAL PT', 2, 0, '0000-00-00', '0000-00-00'),
(981, '020.4585', 'ORIENT PACIFIC ENTERPRISE INC', 2, 0, '0000-00-00', '0000-00-00'),
(982, '020.4586', 'YAYASAN KESEJAHTERAAN PEGAWAI PT BANK TABUNGAN NEGARA', 2, 0, '0000-00-00', '0000-00-00'),
(983, '020.4587', 'YAYASAN KESEJAHTERAAN PENSIUNAN BANK EXIM', 2, 0, '0000-00-00', '0000-00-00'),
(984, '020.4588', 'PT BANK RAKYAT INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(985, '020.4589', 'BANK JABAR BANTEN', 2, 0, '0000-00-00', '0000-00-00'),
(986, '020.4590', 'ASURANSI JIWASRAYA  SA JS LINK BALANCED FUND PT', 2, 0, '0000-00-00', '0000-00-00'),
(987, '020.4591', 'ASURANSI BANGUN ASKRIDA PT', 2, 0, '0000-00-00', '0000-00-00'),
(988, '020.4592', 'ASURANSI JIWA GENERALI INDONESIA PT GENERALI EQUITY I', 2, 0, '0000-00-00', '0000-00-00'),
(989, '020.4593', 'ASURANSI JIWA GENERALI INDONESIA PT GENERALI EQUITY II', 2, 0, '0000-00-00', '0000-00-00'),
(990, '020.4594', 'ASURANSI JIWA GENERALI INDONESIA PT GENERALI EQUITY IV', 2, 0, '0000-00-00', '0000-00-00'),
(991, '020.4595', 'ASURANSI JIWA GENERALI INDONESIA PT GENERALI EQUITY V', 2, 0, '0000-00-00', '0000-00-00'),
(992, '020.4596', 'ASURANSI JIWA SINARMAS MSIG PT STABLE FUND RUPIAH', 2, 0, '0000-00-00', '0000-00-00'),
(993, '020.4597', 'ASURANSI ASTRA BUANA PT', 2, 0, '0000-00-00', '0000-00-00'),
(994, '020.4598', 'ASURANSI SINAR MAS PT', 2, 0, '0000-00-00', '0000-00-00'),
(995, '020.4599', 'AJB BUMIPUTERA 1912 KBN', 2, 0, '0000-00-00', '0000-00-00'),
(996, '020.4600', 'AJB BUMIPUTERA 1912 SYARIAH', 2, 0, '0000-00-00', '0000-00-00'),
(997, '020.4601', 'MNC ASURANSI INDONESIA PT', 2, 0, '0000-00-00', '0000-00-00'),
(998, '020.4602', 'TUGU REASURANSI INDONESIA PT', 2, 0, '0000-00-00', '0000-00-00'),
(999, '020.4603', 'ASURANSI JIWA SINARMAS MSIG PT QQ EXCELLINK AGGRESSIVE FUND ', 2, 0, '0000-00-00', '0000-00-00'),
(1000, '020.4604', 'ASURANSI JIWA SINARMAS MSIG PT QQ EXCELLINK DYNAMIC FUND', 2, 0, '0000-00-00', '0000-00-00'),
(1001, '020.4605', 'ASURANSI JIWA RECAPITAL SA RELIFE PRIMELINK EQUITY FUND PT', 2, 0, '0000-00-00', '0000-00-00'),
(1002, '020.4606', 'SLFI  AGGRESSIVE MULTI PLUS FUND', 2, 0, '0000-00-00', '0000-00-00'),
(1003, '020.4607', 'ASURANSI JIWA BRINGIN JIWA SEJAHTERA PT', 2, 0, '0000-00-00', '0000-00-00'),
(1004, '020.4608', 'ASURANSI JIWA SINARMAS MSIG PT', 2, 0, '0000-00-00', '0000-00-00'),
(1005, '020.4609', 'ASURANSI JIWA RECAPITAL SA RELIFE PRIMELINK BALANCED FUND', 2, 0, '0000-00-00', '0000-00-00'),
(1006, '020.4610', 'PT ASURANSI JIWA TUGU MANDIRI', 2, 0, '0000-00-00', '0000-00-00'),
(1007, '020.4611', 'PT ASURANSI JIWA TUGU MANDIRI  7', 2, 0, '0000-00-00', '0000-00-00'),
(1008, '020.4612', 'PT ASURANSI JIWA TUGU MANDIRI  8', 2, 0, '0000-00-00', '0000-00-00'),
(1009, '020.4613', 'ASURANSI JIWA INHEALTH INDONESIA PT', 2, 0, '0000-00-00', '0000-00-00'),
(1010, '020.4614', 'DANA PENSIUN NATOUR', 2, 0, '0000-00-00', '0000-00-00'),
(1011, '020.4615', 'BUANA CAPITAL PT', 2, 0, '0000-00-00', '0000-00-00'),
(1012, '020.4616', 'DANA PENSIUN BANK MANDIRI', 2, 0, '0000-00-00', '0000-00-00'),
(1013, '020.4617', 'DANA PENSIUN BTN', 2, 0, '0000-00-00', '0000-00-00'),
(1014, '020.4618', 'DANA PENSIUN POS INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(1015, '020.4619', 'DANA PENSIUN CITRA LINTAS INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(1016, '020.4620', 'DANA PENSIUN LEMBAGA ALKITAB INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(1017, '020.4621', 'DANA PENSIUN PERTANI', 2, 0, '0000-00-00', '0000-00-00'),
(1018, '020.4622', 'DANA PENSIUN BRANTAS ABIPRAYA PT', 2, 0, '0000-00-00', '0000-00-00'),
(1019, '020.4623', 'DANA PENSIUN KARYAWAN PT PINDAD', 2, 0, '0000-00-00', '0000-00-00'),
(1020, '020.4624', 'DANA PENSIUN LIA', 2, 0, '0000-00-00', '0000-00-00'),
(1021, '020.4625', 'DANA PENSIUN GPIB', 2, 0, '0000-00-00', '0000-00-00'),
(1022, '020.4626', 'DANA PENSIUN GKJW', 2, 0, '0000-00-00', '0000-00-00'),
(1023, '020.4627', 'DANA PENSIUN HII', 2, 0, '0000-00-00', '0000-00-00'),
(1024, '020.4628', 'DANA PENSIUN UNISBA', 2, 0, '0000-00-00', '0000-00-00'),
(1025, '020.4629', 'DANA PENSIUN PGI', 2, 0, '0000-00-00', '0000-00-00'),
(1026, '020.4630', 'DANA PENSIUN ASKRIDA', 2, 0, '0000-00-00', '0000-00-00'),
(1027, '020.4631', 'DANA PENSIUN MUHAMMADIYAH', 2, 0, '0000-00-00', '0000-00-00'),
(1028, '020.4632', 'DANA PENSIUN PEGAWAI UNIV ISLAM INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(1029, '020.4633', 'DPLK TUGU MANDIRI', 2, 0, '0000-00-00', '0000-00-00'),
(1030, '020.4634', 'DANA PENSIUN UNIVERSITAS SURABAYA', 2, 0, '0000-00-00', '0000-00-00'),
(1031, '020.4635', 'DANA PENSIUN KRAKATAU STEEL', 2, 0, '0000-00-00', '0000-00-00'),
(1032, '020.4636', 'DANA PENSIUN BUKIT ASAM', 2, 0, '0000-00-00', '0000-00-00'),
(1033, '020.4637', 'DANA PENSIUN ANGKASA PURA II', 2, 0, '0000-00-00', '0000-00-00'),
(1034, '020.4638', 'DANA PENSIUN BANK INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(1035, '020.4639', 'DANA PENSIUN ANGKASA PURA I', 2, 0, '0000-00-00', '0000-00-00'),
(1036, '020.4640', 'DANA PENSIUN KARYAWAN BPJS KETENAGAKERJAAN', 2, 0, '0000-00-00', '0000-00-00'),
(1037, '020.4641', 'DANA PENSIUN BANK DKI', 2, 0, '0000-00-00', '0000-00-00'),
(1038, '020.4642', 'DANA PENSIUN PERUMNAS', 2, 0, '0000-00-00', '0000-00-00'),
(1039, '020.4643', 'DANA PENSIUN ASDP', 2, 0, '0000-00-00', '0000-00-00'),
(1040, '020.4644', 'DANA PENSIUN JASA MARGA', 2, 0, '0000-00-00', '0000-00-00'),
(1041, '020.4645', 'DAPEN BANK BJB', 2, 0, '0000-00-00', '0000-00-00'),
(1042, '020.4646', 'DANA PENSIUN JASA TIRTA II', 2, 0, '0000-00-00', '0000-00-00'),
(1043, '020.4647', 'DANA PENSIUN PERHUTANI', 2, 0, '0000-00-00', '0000-00-00'),
(1044, '020.4648', 'DANA PENSIUN PUSRI DAPENSRI', 2, 0, '0000-00-00', '0000-00-00'),
(1045, '020.4649', 'DANA PENSIUN TELKOM', 2, 0, '0000-00-00', '0000-00-00'),
(1046, '020.4650', 'DANA PENSIUN KARYAWAN TASPEN', 2, 0, '0000-00-00', '0000-00-00'),
(1047, '020.4651', 'DANA PENSIUN BPD JATENG', 2, 0, '0000-00-00', '0000-00-00'),
(1048, '020.4652', 'DPLK BANK RAKYAT INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(1049, '020.4653', 'DANA PENSIUN DANAREKSA', 2, 0, '0000-00-00', '0000-00-00'),
(1050, '020.4654', 'DANA PENSIUN PT BPD SUMATERA BARAT', 2, 0, '0000-00-00', '0000-00-00'),
(1051, '020.4655', 'DANA PENSIUN BRI', 2, 0, '0000-00-00', '0000-00-00'),
(1052, '020.4656', 'DANA PENSIUN PERTAMINA', 2, 0, '0000-00-00', '0000-00-00'),
(1053, '020.4657', 'DANA PENSIUN PEGAWAI PT BANK PEMBANGUNA DAERAH JATIM', 2, 0, '0000-00-00', '0000-00-00'),
(1054, '020.4658', 'DANA PENSIUN ASTRA DUA', 2, 0, '0000-00-00', '0000-00-00'),
(1055, '020.4659', 'DANA PENSIUN ASTRA SATU', 2, 0, '0000-00-00', '0000-00-00'),
(1056, '020.4660', 'DANA PENSIUN ASURANSI JASA INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(1057, '020.4661', 'DANA PENSIUN RAJAWALI NUSANTARA INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(1058, '020.4662', 'DANA PENSIUN BCA', 2, 0, '0000-00-00', '0000-00-00'),
(1059, '020.4663', 'DANA PENSIUN GARUDA INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(1060, '020.4664', 'DANA PENSIUN PEMBERI KERJA JIWASRAYA', 2, 0, '0000-00-00', '0000-00-00'),
(1061, '020.4665', 'DANA PENSIUN RUMAH SAKIT ISLAM JAKARTA', 2, 0, '0000-00-00', '0000-00-00'),
(1062, '020.4666', 'DANA PENSIUN KOMPAS GRAMEDIA', 2, 0, '0000-00-00', '0000-00-00'),
(1063, '020.4667', 'DANA PENSIUN BANK CIMB NIAGA', 2, 0, '0000-00-00', '0000-00-00'),
(1064, '020.4668', 'DANA PENSIUN MITRA KRAKATAU', 2, 0, '0000-00-00', '0000-00-00'),
(1065, '020.4669', 'DANA PENSIUN CARDIG GROUP', 2, 0, '0000-00-00', '0000-00-00'),
(1066, '020.4670', 'DANA PENSIUN BAPTIS INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(1067, '020.4671', 'DANA PENSIUN AEROWISATA', 2, 0, '0000-00-00', '0000-00-00'),
(1068, '020.4672', 'DANA PENSIUN INTI', 2, 0, '0000-00-00', '0000-00-00'),
(1069, '020.4673', 'DANA PENSIUN SMART', 2, 0, '0000-00-00', '0000-00-00'),
(1070, '020.4674', 'DANA PENSIUN PUPUK KALTIM', 2, 0, '0000-00-00', '0000-00-00'),
(1071, '020.4675', 'DANA PENSIUN YAKKUM', 2, 0, '0000-00-00', '0000-00-00'),
(1072, '020.4676', 'DANA PENSIUN BANK BUKOPIN', 2, 0, '0000-00-00', '0000-00-00'),
(1073, '020.4677', 'DANA PENSIUN GEREJA KRISTEN INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(1074, '020.4678', 'DANA PENSIUN RAJAWALI NUSINDO', 2, 0, '0000-00-00', '0000-00-00'),
(1075, '020.4679', 'DANA PENSIUN KARYAWAN STAF PT KEBON AGUNG', 2, 0, '0000-00-00', '0000-00-00'),
(1076, '020.4680', 'DANA PENSIUN ELNUSA', 2, 0, '0000-00-00', '0000-00-00'),
(1077, '020.4681', 'DANA PENSIUN SEMEN TONASA', 2, 0, '0000-00-00', '0000-00-00'),
(1078, '020.4682', 'DANA PENSIUN SEMEN GRESIK', 2, 0, '0000-00-00', '0000-00-00'),
(1079, '020.4683', 'DANA PENSIUN SEMEN PADANG', 2, 0, '0000-00-00', '0000-00-00'),
(1080, '020.4684', 'DANA PENSIUN DANAPERA', 2, 0, '0000-00-00', '0000-00-00'),
(1081, '020.4685', 'DANA PENSIUN BPK PENABUR', 2, 0, '0000-00-00', '0000-00-00'),
(1082, '020.4686', 'DANA PENSIUN TRIPUTRA', 2, 0, '0000-00-00', '0000-00-00'),
(1083, '020.4687', 'DPLK BUMIPUTERA', 2, 0, '0000-00-00', '0000-00-00'),
(1084, '020.4688', 'BAPELKES KRAKATAU STEEL', 2, 0, '0000-00-00', '0000-00-00'),
(1085, '020.4689', 'PT TASPEN PERSERO', 2, 0, '0000-00-00', '0000-00-00'),
(1086, '020.4690', 'BPJS KETENAGAKERJAAN', 2, 0, '0000-00-00', '0000-00-00'),
(1087, '020.4691', 'BPJS KETENAGAKERJAAN JHT', 2, 0, '0000-00-00', '0000-00-00'),
(1088, '020.4692', 'BPJS KETENAGAKERJAAN JKK', 2, 0, '0000-00-00', '0000-00-00'),
(1089, '020.4693', 'BPJS KETENAGAKERJAAN JK', 2, 0, '0000-00-00', '0000-00-00'),
(1090, '020.4694', 'BPJS KETENAGAKERJAAN BPJS', 2, 0, '0000-00-00', '0000-00-00'),
(1091, '020.4695', 'YAYASAN KESEHATAN PENSIUNAN ANEKA TAMBANG', 2, 0, '0000-00-00', '0000-00-00'),
(1092, '020.4696', 'YAYASAN KESEJAHTERAAN KARYAWAN JIWASRAYA', 2, 0, '0000-00-00', '0000-00-00'),
(1093, '020.4697', 'YAYASAN KESEHATAN GARUDA INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(1094, '020.4698', 'ZICO ALLSHORES TRUST PTE LTD ATO ASIA PREMIER GLOBAL FUND', 2, 0, '0000-00-00', '0000-00-00'),
(1095, '020.4699', 'SLFI  XTRA AGGRESSIVE FUND', 2, 0, '0000-00-00', '0000-00-00'),
(1096, '020.4700', 'PT ASURANSI JIWASRAYA PERSERO JS LINK EKUITAS', 2, 0, '0000-00-00', '0000-00-00'),
(1097, '020.4701', 'DANA PENSIUN SINT CAROLUS', 2, 0, '0000-00-00', '0000-00-00'),
(1098, '020.4702', 'PERKUMPULAN ADPI', 2, 0, '0000-00-00', '0000-00-00'),
(1099, '020.4703', 'BADAN PENGELOLA USAHA DANA LESTARI BPUDL ITB', 2, 0, '0000-00-00', '0000-00-00'),
(1100, '020.4704', 'KOPERASI KESEHATAN PEGAWAI DAN PENSIUNAN BANK MANDIRI', 2, 0, '0000-00-00', '0000-00-00'),
(1101, '020.4705', 'PERUM JAMINAN KREDIT INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(1102, '020.4706', 'ASURANSI JIWA RECAPITAL SA RELIFE PRIMELINK PT', 2, 0, '0000-00-00', '0000-00-00'),
(1103, '020.4707', 'ASURANSI UMUM BUMIPUTERA MUDA 1967 ', 2, 0, '0000-00-00', '0000-00-00'),
(1104, '020.4708', 'BANK RAKYAT INDONESIA AGRONIAGA TBK PT', 2, 0, '0000-00-00', '0000-00-00'),
(1105, '020.4709', 'PT BANK PEMBANGUNAN DAERAH JAWA BARAT DAN BANTEN', 2, 0, '0000-00-00', '0000-00-00'),
(1106, '020.4710', 'ASURANSI PURNA ARTANUGRAHA PT', 2, 0, '0000-00-00', '0000-00-00'),
(1107, '020.4711', 'ASURANSI WAHANA TATA PT', 2, 0, '0000-00-00', '0000-00-00'),
(1108, '020.4712', 'ASURANSI TRI PAKARTA PT', 2, 0, '0000-00-00', '0000-00-00'),
(1109, '020.4713', 'PT ASURANSI JIWA BRINGIN JIWA SEJAHTERA  KUMPULAN', 2, 0, '0000-00-00', '0000-00-00'),
(1110, '020.4714', 'YAYASAN SOS DESA TARUNA INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(1111, '020.4715', 'YAYASAN KESEJAHTERAAN KARYAWAN ANGKASA PURA I ', 2, 0, '0000-00-00', '0000-00-00'),
(1112, '020.4716', 'BANK VICTORIA INTERNATIONAL PT TBK', 2, 0, '0000-00-00', '0000-00-00'),
(1113, '020.4717', 'BANK KEB HANA INDONESIA PT', 2, 0, '0000-00-00', '0000-00-00'),
(1114, '020.4718', 'DANA PENSIUN PERKEBUNAN', 2, 0, '0000-00-00', '0000-00-00'),
(1115, '020.4719', 'DANAREKSA SEKURITAS PT', 2, 0, '0000-00-00', '0000-00-00'),
(1116, '020.4720', 'SINODE GEREJA KRISTEN JAWA', 2, 0, '0000-00-00', '0000-00-00'),
(1117, '020.4721', 'FULAI HOLDINGS LIMITED', 2, 0, '0000-00-00', '0000-00-00'),
(1118, '020.4722', 'JADE JUBILEE HOLDINGS LIMITED', 2, 0, '0000-00-00', '0000-00-00'),
(1119, '020.4723', 'NIRAN LUMBUNG SEJAHTERA PT', 2, 0, '0000-00-00', '0000-00-00'),
(1120, '020.4724', 'PINEHILLS GROUP LIMITED', 2, 0, '0000-00-00', '0000-00-00'),
(1121, '020.4725', 'SINARMAS SEKURITAS PT', 2, 0, '0000-00-00', '0000-00-00'),
(1122, '020.4726', 'YAYASAN BENTARA RAKYAT', 2, 0, '0000-00-00', '0000-00-00'),
(1123, '020.4727', 'VIRTUE INVESTMENTS HOLDINGS LIMITED', 2, 0, '0000-00-00', '0000-00-00'),
(1124, '020.4728', 'BANK MEGA TBK PT', 2, 0, '0000-00-00', '0000-00-00'),
(1125, '020.4729', 'DANA PENSIUN PLN', 2, 0, '0000-00-00', '0000-00-00'),
(1126, '020.4730', 'INDO PREMIER SECURITIES PT', 2, 0, '0000-00-00', '0000-00-00'),
(1127, '020.4731', 'A PSY', 2, 0, '0000-00-00', '0000-00-00'),
(1128, '020.4732', 'B PSY', 2, 0, '0000-00-00', '0000-00-00'),
(1129, '020.4733', 'C PSY', 2, 0, '0000-00-00', '0000-00-00'),
(1130, '020.4734', 'D PSY', 2, 0, '0000-00-00', '0000-00-00'),
(1131, '020.4735', 'rere', 2, 0, '0000-00-00', '0000-00-00'),
(1132, '020.14735', 'btpn bank', 2, 0, '0000-00-00', '0000-00-00'),
(1133, '020.14736', 'hsbc bank', 2, 0, '0000-00-00', '0000-00-00'),
(1134, '020.14737', 'BPJS PSY A', 2, 0, '0000-00-00', '0000-00-00'),
(1135, '020.14738', 'BPJS PSY B', 2, 0, '0000-00-00', '0000-00-00'),
(1136, '020.24735', 'PERMATA BANK', 2, 0, '0000-00-00', '0000-00-00'),
(1137, '020.24736', 'reksadana utama', 2, 0, '0000-00-00', '0000-00-00'),
(1138, '020.24737', 'new2', 2, 0, '0000-00-00', '0000-00-00'),
(1139, '020.34736', 'syai6', 2, 0, '0000-00-00', '0000-00-00'),
(1140, '020.44739', 'JOHNNY', 2, 0, '0000-00-00', '0000-00-00'),
(1141, '020.44740', 'PARAMITA', 2, 0, '0000-00-00', '0000-00-00'),
(1142, '020.44742', 'ROBERT', 2, 0, '0000-00-00', '0000-00-00'),
(1143, '020.44746', 'JOHNNY', 2, 0, '0000-00-00', '0000-00-00'),
(1144, '020.44747', 'ANNA', 2, 0, '0000-00-00', '0000-00-00'),
(1145, '020.44748', 'PT PENJAMIN KREDIT PENGUSAHA INDONESIA', 2, 0, '0000-00-00', '0000-00-00'),
(1146, '020.44749', 'DPLK TUGU MANDIRI - PPUKP PERTAMINA', 2, 0, '0000-00-00', '0000-00-00'),
(1147, '020.44750', 'PT ASURANSI BINTANG TBK', 2, 0, '0000-00-00', '0000-00-00'),
(1148, '020.54735', 'reza', 2, 0, '0000-00-00', '0000-00-00'),
(1149, '020.54736', 'indo premier securities', 2, 0, '0000-00-00', '0000-00-00'),
(1150, '020.54737', 'Bareksa Portal Investasi', 2, 0, '0000-00-00', '0000-00-00'),
(1151, '020.54738', 'Intan', 2, 0, '0000-00-00', '0000-00-00'),
(1152, '010.1', 'Cholis Baidowi', 1, 0, '0000-00-00', '0000-00-00'),
(1153, '010.2', 'David Tanuri', 1, 0, '0000-00-00', '0000-00-00'),
(1154, '010.3', 'Fajar Rahman Hidajat', 1, 0, '0000-00-00', '0000-00-00'),
(1155, '010.4', 'Gunanta', 1, 0, '0000-00-00', '0000-00-00'),
(1156, '010.5', 'Harnugama', 1, 0, '0000-00-00', '0000-00-00'),
(1157, '010.6', 'Jos Parengkuan', 1, 0, '0000-00-00', '0000-00-00'),
(1158, '010.7', 'Roy Himawan', 1, 0, '0000-00-00', '0000-00-00'),
(1159, '010.8', 'Ade Rizky', 1, 0, '0000-00-00', '0000-00-00'),
(1160, '010.9', 'Ade Saida', 1, 0, '0000-00-00', '0000-00-00'),
(1161, '010.10', 'Agnes Ekaristianty', 1, 0, '0000-00-00', '0000-00-00'),
(1162, '010.11', 'Aima Mawadah', 1, 0, '0000-00-00', '0000-00-00'),
(1163, '010.12', 'Alviena', 1, 0, '0000-00-00', '0000-00-00'),
(1164, '010.13', 'Anastasia Pritasari', 1, 0, '0000-00-00', '0000-00-00'),
(1165, '010.14', 'Anthony', 1, 0, '0000-00-00', '0000-00-00'),
(1166, '010.15', 'Artha Sekuritas', 1, 0, '0000-00-00', '0000-00-00'),
(1167, '010.16', 'Bachtiar', 1, 0, '0000-00-00', '0000-00-00'),
(1168, '010.17', 'Bank Jabar', 1, 0, '0000-00-00', '0000-00-00'),
(1169, '010.18', 'Bank QNB', 1, 0, '0000-00-00', '0000-00-00'),
(1170, '010.19', 'BOD - Ex Prita', 1, 0, '0000-00-00', '0000-00-00'),
(1171, '010.20', 'Buana Capital', 1, 0, '0000-00-00', '0000-00-00'),
(1172, '010.21', 'Danareksa Sekuritas', 1, 0, '0000-00-00', '0000-00-00'),
(1173, '010.22', 'Dian Anggita', 1, 0, '0000-00-00', '0000-00-00'),
(1174, '010.23', 'Dini Trirahmawati', 1, 0, '0000-00-00', '0000-00-00'),
(1175, '010.24', 'Dodi Kristandhio', 1, 0, '0000-00-00', '0000-00-00'),
(1176, '010.25', 'Edwin Teintang', 1, 0, '0000-00-00', '0000-00-00'),
(1177, '010.26', 'Febry', 1, 0, '0000-00-00', '0000-00-00'),
(1178, '010.27', 'Fian Pusparini', 1, 0, '0000-00-00', '0000-00-00'),
(1179, '010.28', 'Gauw Linardi', 1, 0, '0000-00-00', '0000-00-00'),
(1180, '010.29', 'Icha', 1, 0, '0000-00-00', '0000-00-00'),
(1181, '010.30', 'Iin', 1, 0, '0000-00-00', '0000-00-00'),
(1182, '010.31', 'Indo Premier Sec', 1, 0, '0000-00-00', '0000-00-00'),
(1183, '010.32', 'Inke - Ex Prita', 1, 0, '0000-00-00', '0000-00-00'),
(1184, '010.33', 'Inke Anggunsari', 1, 0, '0000-00-00', '0000-00-00'),
(1185, '010.34', 'Mandiri Sekuritas', 1, 0, '0000-00-00', '0000-00-00'),
(1186, '010.35', 'Margaret', 1, 0, '0000-00-00', '0000-00-00'),
(1187, '010.36', 'Margareth - Ex Prita', 1, 0, '0000-00-00', '0000-00-00'),
(1188, '010.37', 'Marketing Retail', 1, 0, '0000-00-00', '0000-00-00'),
(1189, '010.38', 'Merry Paso paso', 1, 0, '0000-00-00', '0000-00-00'),
(1190, '010.39', 'Natassa Tita', 1, 0, '0000-00-00', '0000-00-00'),
(1191, '010.40', 'Non Updated', 1, 0, '0000-00-00', '0000-00-00'),
(1192, '010.41', 'Office Income', 1, 0, '0000-00-00', '0000-00-00'),
(1193, '010.42', 'Rully Intan Agustian', 1, 0, '0000-00-00', '0000-00-00'),
(1194, '010.43', 'SA - Ex Prita', 1, 0, '0000-00-00', '0000-00-00'),
(1195, '010.44', 'Saida Jusuff - Ex Prita', 1, 0, '0000-00-00', '0000-00-00'),
(1196, '010.45', 'KAM Asia', 1, 0, '0000-00-00', '0000-00-00'),
(1197, '010.46', 'Vacant', 1, 0, '0000-00-00', '0000-00-00'),
(1198, '010.47', 'Yuni Melanie', 1, 0, '0000-00-00', '0000-00-00'),
(1199, '010.48', 'Zulyah', 1, 0, '0000-00-00', '0000-00-00'),
(1200, '010.49', 'Zulyah - Ex Prita', 1, 0, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `0_dimension_link`
--

CREATE TABLE `0_dimension_link` (
  `id` int(11) NOT NULL,
  `clientid` varchar(32) DEFAULT NULL,
  `agentid` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_dimension_link`
--

INSERT INTO `0_dimension_link` (`id`, `clientid`, `agentid`) VALUES
(1, '020.4735', '010.2'),
(2, '020.4725', '010.6'),
(3, '020.4536', '010.6'),
(4, '020.4584', '010.6'),
(5, '020.4585', '010.6'),
(6, '020.681', '010.6'),
(7, '020.34', '010.6'),
(8, '020.227', '010.6'),
(9, '020.447', '010.6'),
(10, '020.572', '010.7'),
(11, '020.613', '010.7'),
(12, '020.617', '010.7'),
(13, '020.221', '010.7'),
(14, '020.171', '010.7'),
(15, '020.54', '010.7'),
(16, '020.127', '010.7'),
(17, '020.133', '010.7'),
(18, '020.882', '010.7'),
(19, '020.4520', '010.7'),
(20, '020.4521', '010.7'),
(21, '020.4522', '010.7'),
(22, '020.4527', '010.7'),
(23, '020.4582', '010.7'),
(24, '020.4576', '010.7'),
(25, '020.4535', '010.7'),
(26, '020.4548', '010.7'),
(27, '020.4597', '010.7'),
(28, '020.4727', '010.7'),
(29, '020.4721', '010.7'),
(30, '020.4722', '010.7'),
(31, '020.44746', '010.7'),
(32, '020.4724', '010.7'),
(33, '020.4708', '010.9'),
(34, '020.4709', '010.9'),
(35, '020.4728', '010.9'),
(36, '020.4672', '010.9'),
(37, '020.4673', '010.9'),
(38, '020.4675', '010.9'),
(39, '020.4681', '010.9'),
(40, '020.4678', '010.9'),
(41, '020.4690', '010.9'),
(42, '020.4691', '010.9'),
(43, '020.4692', '010.9'),
(44, '020.4693', '010.9'),
(45, '020.4694', '010.9'),
(46, '020.4705', '010.9'),
(47, '020.4598', '010.9'),
(48, '020.4603', '010.9'),
(49, '020.4604', '010.9'),
(50, '020.4608', '010.9'),
(51, '020.4613', '010.9'),
(52, '020.4631', '010.9'),
(53, '020.4632', '010.9'),
(54, '020.4637', '010.9'),
(55, '020.4640', '010.9'),
(56, '020.4645', '010.9'),
(57, '020.4649', '010.9'),
(58, '020.4651', '010.9'),
(59, '020.4655', '010.9'),
(60, '020.4663', '010.9'),
(61, '020.4553', '010.9'),
(62, '020.4556', '010.9'),
(63, '020.4544', '010.9'),
(64, '020.4545', '010.9'),
(65, '020.4531', '010.9'),
(66, '020.4532', '010.9'),
(67, '020.4578', '010.9'),
(68, '020.4569', '010.9'),
(69, '020.4580', '010.9'),
(70, '020.4596', '010.9'),
(71, '020.848', '010.9'),
(72, '020.126', '010.9'),
(73, '020.23', '010.9'),
(74, '020.470', '010.9'),
(75, '020.471', '010.9'),
(76, '020.426', '010.9'),
(77, '020.286', '010.10'),
(78, '020.257', '010.10'),
(79, '020.4586', '010.11'),
(80, '020.4587', '010.11'),
(81, '020.4551', '010.11'),
(82, '020.4668', '010.11'),
(83, '020.4658', '010.11'),
(84, '020.4659', '010.11'),
(85, '020.4635', '010.11'),
(86, '020.4627', '010.11'),
(87, '020.4616', '010.11'),
(88, '020.4617', '010.11'),
(89, '020.4622', '010.11'),
(90, '020.4614', '010.11'),
(91, '020.4697', '010.11'),
(92, '020.4688', '010.11'),
(93, '020.4680', '010.11'),
(94, '020.4704', '010.11'),
(95, '020.44748', '010.13'),
(96, '020.44749', '010.13'),
(97, '020.44750', '010.13'),
(98, '020.4552', '010.13'),
(99, '020.4526', '010.13'),
(100, '020.4534', '010.13'),
(101, '020.4540', '010.13'),
(102, '020.4583', '010.13'),
(103, '020.4564', '010.13'),
(104, '020.4579', '010.13'),
(105, '020.819', '010.13'),
(106, '020.788', '010.13'),
(107, '020.801', '010.13'),
(108, '020.4528', '010.13'),
(109, '020.4529', '010.13'),
(110, '020.4530', '010.13'),
(111, '020.4514', '010.13'),
(112, '020.4524', '010.13'),
(113, '020.4516', '010.13'),
(114, '020.4517', '010.13'),
(115, '020.4518', '010.13'),
(116, '020.4519', '010.13'),
(117, '020.892', '010.13'),
(118, '020.1784', '010.13'),
(119, '020.4512', '010.13'),
(120, '020.873', '010.13'),
(121, '020.869', '010.13'),
(122, '020.669', '010.13'),
(123, '020.635', '010.13'),
(124, '020.641', '010.13'),
(125, '020.263', '010.13'),
(126, '020.264', '010.13'),
(127, '020.174', '010.13'),
(128, '020.175', '010.13'),
(129, '020.58', '010.13'),
(130, '020.60', '010.13'),
(131, '020.76', '010.13'),
(132, '020.3', '010.13'),
(133, '020.1', '010.13'),
(134, '020.10', '010.13'),
(135, '020.287', '010.13'),
(136, '020.282', '010.13'),
(137, '020.290', '010.13'),
(138, '020.349', '010.13'),
(139, '020.365', '010.13'),
(140, '020.368', '010.13'),
(141, '020.454', '010.13'),
(142, '020.466', '010.13'),
(143, '020.451', '010.13'),
(144, '020.477', '010.13'),
(145, '020.506', '010.13'),
(146, '020.591', '010.13'),
(147, '020.578', '010.13'),
(148, '020.559', '010.14'),
(149, '020.493', '010.14'),
(150, '020.370', '010.14'),
(151, '020.326', '010.14'),
(152, '020.284', '010.14'),
(153, '020.344', '010.14'),
(154, '020.56', '010.14'),
(155, '020.125', '010.14'),
(156, '020.198', '010.14'),
(157, '020.244', '010.14'),
(158, '020.626', '010.14'),
(159, '020.662', '010.14'),
(160, '020.864', '010.14'),
(161, '020.3603', '010.14'),
(162, '020.2697', '010.14'),
(163, '020.4589', '010.17'),
(164, '020.4568', '010.18'),
(165, '020.4581', '010.19'),
(166, '020.4541', '010.19'),
(167, '020.881', '010.19'),
(168, '020.4537', '010.19'),
(169, '020.877', '010.19'),
(170, '020.637', '010.19'),
(171, '020.638', '010.19'),
(172, '020.294', '010.19'),
(173, '020.138', '010.19'),
(174, '020.26', '010.19'),
(175, '020.446', '010.19'),
(176, '020.440', '010.19'),
(177, '020.385', '010.19'),
(178, '020.453', '010.19'),
(179, '020.501', '010.19'),
(180, '020.531', '010.19'),
(181, '020.537', '010.19'),
(182, '020.4615', '010.20'),
(183, '020.4719', '010.21'),
(184, '020.4588', '010.22'),
(185, '020.648', '010.22'),
(186, '020.664', '010.22'),
(187, '020.697', '010.22'),
(188, '020.704', '010.22'),
(189, '020.718', '010.22'),
(190, '020.728', '010.22'),
(191, '020.769', '010.22'),
(192, '020.793', '010.22'),
(193, '020.833', '010.22'),
(194, '020.583', '010.22'),
(195, '020.532', '010.22'),
(196, '020.503', '010.22'),
(197, '020.411', '010.22'),
(198, '020.418', '010.22'),
(199, '020.382', '010.22'),
(200, '020.432', '010.22'),
(201, '020.445', '010.22'),
(202, '020.61', '010.22'),
(203, '020.11', '010.22'),
(204, '020.137', '010.22'),
(205, '020.109', '010.22'),
(206, '020.87', '010.22'),
(207, '020.95', '010.22'),
(208, '020.235', '010.22'),
(209, '020.180', '010.22'),
(210, '020.190', '010.22'),
(211, '020.157', '010.23'),
(212, '020.212', '010.23'),
(213, '020.82', '010.23'),
(214, '020.124', '010.23'),
(215, '020.155', '010.23'),
(216, '020.151', '010.23'),
(217, '020.152', '010.23'),
(218, '020.15', '010.23'),
(219, '020.71', '010.23'),
(220, '020.399', '010.23'),
(221, '020.524', '010.23'),
(222, '020.489', '010.23'),
(223, '020.582', '010.23'),
(224, '020.539', '010.23'),
(225, '020.589', '010.23'),
(226, '020.816', '010.23'),
(227, '020.755', '010.23'),
(228, '020.746', '010.23'),
(229, '020.686', '010.23'),
(230, '020.710', '010.23'),
(231, '020.665', '010.23'),
(232, '020.658', '010.23'),
(233, '020.675', '010.23'),
(234, '020.684', '010.23'),
(235, '020.656', '010.23'),
(236, '020.616', '010.23'),
(237, '020.4689', '010.27'),
(238, '020.4657', '010.27'),
(239, '020.4723', '010.28'),
(240, '020.546', '010.28'),
(241, '020.4730', '010.31'),
(242, '020.222', '010.32'),
(243, '020.223', '010.33'),
(244, '020.224', '010.33'),
(245, '020.225', '010.33'),
(246, '020.226', '010.33'),
(247, '020.217', '010.33'),
(248, '020.218', '010.33'),
(249, '020.219', '010.33'),
(250, '020.220', '010.33'),
(251, '020.237', '010.33'),
(252, '020.238', '010.33'),
(253, '020.228', '010.33'),
(254, '020.229', '010.33'),
(255, '020.230', '010.33'),
(256, '020.231', '010.33'),
(257, '020.232', '010.33'),
(258, '020.213', '010.33'),
(259, '020.214', '010.33'),
(260, '020.215', '010.33'),
(261, '020.202', '010.33'),
(262, '020.210', '010.33'),
(263, '020.211', '010.33'),
(264, '020.199', '010.33'),
(265, '020.204', '010.33'),
(266, '020.205', '010.33'),
(267, '020.206', '010.33'),
(268, '020.158', '010.33'),
(269, '020.159', '010.33'),
(270, '020.160', '010.33'),
(271, '020.161', '010.33'),
(272, '020.162', '010.33'),
(273, '020.166', '010.33'),
(274, '020.167', '010.33'),
(275, '020.168', '010.33'),
(276, '020.169', '010.33'),
(277, '020.170', '010.33'),
(278, '020.177', '010.33'),
(279, '020.178', '010.33'),
(280, '020.179', '010.33'),
(281, '020.172', '010.33'),
(282, '020.173', '010.33'),
(283, '020.164', '010.33'),
(284, '020.191', '010.33'),
(285, '020.188', '010.33'),
(286, '020.193', '010.33'),
(287, '020.194', '010.33'),
(288, '020.195', '010.33'),
(289, '020.196', '010.33'),
(290, '020.181', '010.33'),
(291, '020.182', '010.33'),
(292, '020.183', '010.33'),
(293, '020.184', '010.33'),
(294, '020.185', '010.33'),
(295, '020.186', '010.33'),
(296, '020.295', '010.33'),
(297, '020.296', '010.33'),
(298, '020.297', '010.33'),
(299, '020.298', '010.33'),
(300, '020.299', '010.33'),
(301, '020.300', '010.33'),
(302, '020.301', '010.33'),
(303, '020.302', '010.33'),
(304, '020.303', '010.33'),
(305, '020.304', '010.33'),
(306, '020.305', '010.33'),
(307, '020.306', '010.33'),
(308, '020.307', '010.33'),
(309, '020.308', '010.33'),
(310, '020.309', '010.33'),
(311, '020.310', '010.33'),
(312, '020.311', '010.33'),
(313, '020.312', '010.33'),
(314, '020.313', '010.33'),
(315, '020.314', '010.33'),
(316, '020.315', '010.33'),
(317, '020.316', '010.33'),
(318, '020.317', '010.33'),
(319, '020.318', '010.33'),
(320, '020.319', '010.33'),
(321, '020.320', '010.33'),
(322, '020.321', '010.33'),
(323, '020.322', '010.33'),
(324, '020.323', '010.33'),
(325, '020.324', '010.33'),
(326, '020.325', '010.33'),
(327, '020.245', '010.33'),
(328, '020.246', '010.33'),
(329, '020.247', '010.33'),
(330, '020.248', '010.33'),
(331, '020.249', '010.33'),
(332, '020.250', '010.33'),
(333, '020.251', '010.33'),
(334, '020.252', '010.33'),
(335, '020.253', '010.33'),
(336, '020.254', '010.33'),
(337, '020.255', '010.33'),
(338, '020.256', '010.33'),
(339, '020.258', '010.33'),
(340, '020.259', '010.33'),
(341, '020.260', '010.33'),
(342, '020.261', '010.33'),
(343, '020.240', '010.33'),
(344, '020.241', '010.33'),
(345, '020.242', '010.33'),
(346, '020.243', '010.33'),
(347, '020.265', '010.33'),
(348, '020.266', '010.33'),
(349, '020.267', '010.33'),
(350, '020.268', '010.33'),
(351, '020.269', '010.33'),
(352, '020.270', '010.33'),
(353, '020.271', '010.33'),
(354, '020.272', '010.33'),
(355, '020.273', '010.33'),
(356, '020.274', '010.33'),
(357, '020.275', '010.33'),
(358, '020.276', '010.33'),
(359, '020.277', '010.33'),
(360, '020.278', '010.33'),
(361, '020.279', '010.33'),
(362, '020.280', '010.33'),
(363, '020.281', '010.33'),
(364, '020.72', '010.33'),
(365, '020.73', '010.33'),
(366, '020.77', '010.33'),
(367, '020.78', '010.33'),
(368, '020.79', '010.33'),
(369, '020.80', '010.33'),
(370, '020.81', '010.33'),
(371, '020.65', '010.33'),
(372, '020.66', '010.33'),
(373, '020.67', '010.33'),
(374, '020.68', '010.33'),
(375, '020.27', '010.33'),
(376, '020.28', '010.33'),
(377, '020.29', '010.33'),
(378, '020.30', '010.33'),
(379, '020.31', '010.33'),
(380, '020.32', '010.33'),
(381, '020.33', '010.33'),
(382, '020.57', '010.33'),
(383, '020.59', '010.33'),
(384, '020.24', '010.33'),
(385, '020.25', '010.33'),
(386, '020.55', '010.33'),
(387, '020.16', '010.33'),
(388, '020.13', '010.33'),
(389, '020.14', '010.33'),
(390, '020.18', '010.33'),
(391, '020.19', '010.33'),
(392, '020.20', '010.33'),
(393, '020.21', '010.33'),
(394, '020.22', '010.33'),
(395, '020.2', '010.33'),
(396, '020.4', '010.33'),
(397, '020.5', '010.33'),
(398, '020.6', '010.33'),
(399, '020.7', '010.33'),
(400, '020.8', '010.33'),
(401, '020.9', '010.33'),
(402, '020.35', '010.33'),
(403, '020.36', '010.33'),
(404, '020.37', '010.33'),
(405, '020.38', '010.33'),
(406, '020.39', '010.33'),
(407, '020.40', '010.33'),
(408, '020.41', '010.33'),
(409, '020.42', '010.33'),
(410, '020.43', '010.33'),
(411, '020.44', '010.33'),
(412, '020.45', '010.33'),
(413, '020.46', '010.33'),
(414, '020.47', '010.33'),
(415, '020.48', '010.33'),
(416, '020.49', '010.33'),
(417, '020.50', '010.33'),
(418, '020.51', '010.33'),
(419, '020.52', '010.33'),
(420, '020.53', '010.33'),
(421, '020.153', '010.33'),
(422, '020.154', '010.33'),
(423, '020.150', '010.33'),
(424, '020.139', '010.33'),
(425, '020.140', '010.33'),
(426, '020.141', '010.33'),
(427, '020.143', '010.33'),
(428, '020.144', '010.33'),
(429, '020.145', '010.33'),
(430, '020.146', '010.33'),
(431, '020.147', '010.33'),
(432, '020.123', '010.33'),
(433, '020.110', '010.33'),
(434, '020.111', '010.33'),
(435, '020.112', '010.33'),
(436, '020.134', '010.33'),
(437, '020.135', '010.33'),
(438, '020.136', '010.33'),
(439, '020.128', '010.33'),
(440, '020.129', '010.33'),
(441, '020.130', '010.33'),
(442, '020.131', '010.33'),
(443, '020.132', '010.33'),
(444, '020.83', '010.33'),
(445, '020.75', '010.33'),
(446, '020.96', '010.33'),
(447, '020.97', '010.33'),
(448, '020.98', '010.33'),
(449, '020.99', '010.33'),
(450, '020.88', '010.33'),
(451, '020.89', '010.33'),
(452, '020.90', '010.33'),
(453, '020.91', '010.33'),
(454, '020.92', '010.33'),
(455, '020.93', '010.33'),
(456, '020.101', '010.33'),
(457, '020.102', '010.33'),
(458, '020.103', '010.33'),
(459, '020.85', '010.33'),
(460, '020.86', '010.33'),
(461, '020.106', '010.33'),
(462, '020.107', '010.33'),
(463, '020.108', '010.33'),
(464, '020.115', '010.33'),
(465, '020.116', '010.33'),
(466, '020.117', '010.33'),
(467, '020.118', '010.33'),
(468, '020.119', '010.33'),
(469, '020.120', '010.33'),
(470, '020.121', '010.33'),
(471, '020.547', '010.33'),
(472, '020.548', '010.33'),
(473, '020.549', '010.33'),
(474, '020.550', '010.33'),
(475, '020.551', '010.33'),
(476, '020.552', '010.33'),
(477, '020.553', '010.33'),
(478, '020.554', '010.33'),
(479, '020.555', '010.33'),
(480, '020.556', '010.33'),
(481, '020.557', '010.33'),
(482, '020.558', '010.33'),
(483, '020.525', '010.33'),
(484, '020.526', '010.33'),
(485, '020.527', '010.33'),
(486, '020.528', '010.33'),
(487, '020.529', '010.33'),
(488, '020.530', '010.33'),
(489, '020.533', '010.33'),
(490, '020.534', '010.33'),
(491, '020.504', '010.33'),
(492, '020.505', '010.33'),
(493, '020.502', '010.33'),
(494, '020.507', '010.33'),
(495, '020.508', '010.33'),
(496, '020.509', '010.33'),
(497, '020.511', '010.33'),
(498, '020.512', '010.33'),
(499, '020.513', '010.33'),
(500, '020.514', '010.33'),
(501, '020.515', '010.33'),
(502, '020.516', '010.33'),
(503, '020.517', '010.33'),
(504, '020.518', '010.33'),
(505, '020.519', '010.33'),
(506, '020.520', '010.33'),
(507, '020.521', '010.33'),
(508, '020.522', '010.33'),
(509, '020.492', '010.33'),
(510, '020.478', '010.33'),
(511, '020.452', '010.33'),
(512, '020.494', '010.33'),
(513, '020.495', '010.33'),
(514, '020.496', '010.33'),
(515, '020.497', '010.33'),
(516, '020.498', '010.33'),
(517, '020.499', '010.33'),
(518, '020.500', '010.33'),
(519, '020.467', '010.33'),
(520, '020.468', '010.33'),
(521, '020.469', '010.33'),
(522, '020.472', '010.33'),
(523, '020.473', '010.33'),
(524, '020.474', '010.33'),
(525, '020.475', '010.33'),
(526, '020.476', '010.33'),
(527, '020.480', '010.33'),
(528, '020.481', '010.33'),
(529, '020.482', '010.33'),
(530, '020.483', '010.33'),
(531, '020.484', '010.33'),
(532, '020.485', '010.33'),
(533, '020.486', '010.33'),
(534, '020.487', '010.33'),
(535, '020.488', '010.33'),
(536, '020.590', '010.33'),
(537, '020.609', '010.33'),
(538, '020.610', '010.33'),
(539, '020.611', '010.33'),
(540, '020.612', '010.33'),
(541, '020.618', '010.33'),
(542, '020.619', '010.33'),
(543, '020.621', '010.33'),
(544, '020.622', '010.33'),
(545, '020.623', '010.33'),
(546, '020.624', '010.33'),
(547, '020.625', '010.33'),
(548, '020.592', '010.33'),
(549, '020.593', '010.33'),
(550, '020.594', '010.33'),
(551, '020.595', '010.33'),
(552, '020.596', '010.33'),
(553, '020.597', '010.33'),
(554, '020.598', '010.33'),
(555, '020.600', '010.33'),
(556, '020.601', '010.33'),
(557, '020.602', '010.33'),
(558, '020.603', '010.33'),
(559, '020.604', '010.33'),
(560, '020.605', '010.33'),
(561, '020.606', '010.33'),
(562, '020.607', '010.33'),
(563, '020.540', '010.33'),
(564, '020.541', '010.33'),
(565, '020.542', '010.33'),
(566, '020.543', '010.33'),
(567, '020.544', '010.33'),
(568, '020.545', '010.33'),
(569, '020.569', '010.33'),
(570, '020.570', '010.33'),
(571, '020.571', '010.33'),
(572, '020.560', '010.33'),
(573, '020.561', '010.33'),
(574, '020.562', '010.33'),
(575, '020.563', '010.33'),
(576, '020.564', '010.33'),
(577, '020.565', '010.33'),
(578, '020.566', '010.33'),
(579, '020.567', '010.33'),
(580, '020.536', '010.33'),
(581, '020.587', '010.33'),
(582, '020.588', '010.33'),
(583, '020.584', '010.33'),
(584, '020.585', '010.33'),
(585, '020.580', '010.33'),
(586, '020.581', '010.33'),
(587, '020.573', '010.33'),
(588, '020.574', '010.33'),
(589, '020.575', '010.33'),
(590, '020.576', '010.33'),
(591, '020.400', '010.33'),
(592, '020.401', '010.33'),
(593, '020.402', '010.33'),
(594, '020.403', '010.33'),
(595, '020.393', '010.33'),
(596, '020.394', '010.33'),
(597, '020.395', '010.33'),
(598, '020.396', '010.33'),
(599, '020.397', '010.33'),
(600, '020.398', '010.33'),
(601, '020.386', '010.33'),
(602, '020.387', '010.33'),
(603, '020.388', '010.33'),
(604, '020.389', '010.33'),
(605, '020.390', '010.33'),
(606, '020.391', '010.33'),
(607, '020.419', '010.33'),
(608, '020.420', '010.33'),
(609, '020.421', '010.33'),
(610, '020.422', '010.33'),
(611, '020.423', '010.33'),
(612, '020.424', '010.33'),
(613, '020.425', '010.33'),
(614, '020.412', '010.33'),
(615, '020.413', '010.33'),
(616, '020.414', '010.33'),
(617, '020.415', '010.33'),
(618, '020.416', '010.33'),
(619, '020.405', '010.33'),
(620, '020.406', '010.33'),
(621, '020.407', '010.33'),
(622, '020.408', '010.33'),
(623, '020.409', '010.33'),
(624, '020.410', '010.33'),
(625, '020.234', '010.33'),
(626, '020.438', '010.33'),
(627, '020.439', '010.33'),
(628, '020.450', '010.33'),
(629, '020.448', '010.33'),
(630, '020.455', '010.33'),
(631, '020.456', '010.33'),
(632, '020.457', '010.33'),
(633, '020.458', '010.33'),
(634, '020.459', '010.33'),
(635, '020.460', '010.33'),
(636, '020.461', '010.33'),
(637, '020.462', '010.33'),
(638, '020.463', '010.33'),
(639, '020.464', '010.33'),
(640, '020.465', '010.33'),
(641, '020.429', '010.33'),
(642, '020.430', '010.33'),
(643, '020.431', '010.33'),
(644, '020.383', '010.33'),
(645, '020.427', '010.33'),
(646, '020.441', '010.33'),
(647, '020.442', '010.33'),
(648, '020.443', '010.33'),
(649, '020.444', '010.33'),
(650, '020.434', '010.33'),
(651, '020.435', '010.33'),
(652, '020.436', '010.33'),
(653, '020.345', '010.33'),
(654, '020.346', '010.33'),
(655, '020.347', '010.33'),
(656, '020.348', '010.33'),
(657, '020.331', '010.33'),
(658, '020.332', '010.33'),
(659, '020.333', '010.33'),
(660, '020.336', '010.33'),
(661, '020.337', '010.33'),
(662, '020.338', '010.33'),
(663, '020.339', '010.33'),
(664, '020.340', '010.33'),
(665, '020.341', '010.33'),
(666, '020.342', '010.33'),
(667, '020.343', '010.33'),
(668, '020.285', '010.33'),
(669, '020.283', '010.33'),
(670, '020.288', '010.33'),
(671, '020.289', '010.33'),
(672, '020.327', '010.33'),
(673, '020.328', '010.33'),
(674, '020.329', '010.33'),
(675, '020.291', '010.33'),
(676, '020.292', '010.33'),
(677, '020.293', '010.33'),
(678, '020.371', '010.33'),
(679, '020.372', '010.33'),
(680, '020.373', '010.33'),
(681, '020.374', '010.33'),
(682, '020.375', '010.33'),
(683, '020.376', '010.33'),
(684, '020.369', '010.33'),
(685, '020.366', '010.33'),
(686, '020.367', '010.33'),
(687, '020.378', '010.33'),
(688, '020.379', '010.33'),
(689, '020.380', '010.33'),
(690, '020.381', '010.33'),
(691, '020.350', '010.33'),
(692, '020.351', '010.33'),
(693, '020.352', '010.33'),
(694, '020.353', '010.33'),
(695, '020.354', '010.33'),
(696, '020.355', '010.33'),
(697, '020.356', '010.33'),
(698, '020.357', '010.33'),
(699, '020.358', '010.33'),
(700, '020.359', '010.33'),
(701, '020.360', '010.33'),
(702, '020.361', '010.33'),
(703, '020.362', '010.33'),
(704, '020.363', '010.33'),
(705, '020.364', '010.33'),
(706, '020.44739', '010.33'),
(707, '020.44740', '010.33'),
(708, '020.44742', '010.33'),
(709, '020.4714', '010.33'),
(710, '020.44747', '010.33'),
(711, '020.615', '010.33'),
(712, '020.631', '010.33'),
(713, '020.632', '010.33'),
(714, '020.633', '010.33'),
(715, '020.634', '010.33'),
(716, '020.639', '010.33'),
(717, '020.640', '010.33'),
(718, '020.636', '010.33'),
(719, '020.628', '010.33'),
(720, '020.629', '010.33'),
(721, '020.657', '010.33'),
(722, '020.645', '010.33'),
(723, '020.646', '010.33'),
(724, '020.647', '010.33'),
(725, '020.642', '010.33'),
(726, '020.643', '010.33'),
(727, '020.649', '010.33'),
(728, '020.650', '010.33'),
(729, '020.651', '010.33'),
(730, '020.652', '010.33'),
(731, '020.653', '010.33'),
(732, '020.654', '010.33'),
(733, '020.677', '010.33'),
(734, '020.679', '010.33'),
(735, '020.680', '010.33'),
(736, '020.688', '010.33'),
(737, '020.689', '010.33'),
(738, '020.690', '010.33'),
(739, '020.691', '010.33'),
(740, '020.692', '010.33'),
(741, '020.693', '010.33'),
(742, '020.694', '010.33'),
(743, '020.663', '010.33'),
(744, '020.670', '010.33'),
(745, '020.671', '010.33'),
(746, '020.672', '010.33'),
(747, '020.673', '010.33'),
(748, '020.659', '010.33'),
(749, '020.660', '010.33'),
(750, '020.661', '010.33'),
(751, '020.666', '010.33'),
(752, '020.667', '010.33'),
(753, '020.668', '010.33'),
(754, '020.711', '010.33'),
(755, '020.712', '010.33'),
(756, '020.713', '010.33'),
(757, '020.699', '010.33'),
(758, '020.701', '010.33'),
(759, '020.723', '010.33'),
(760, '020.725', '010.33'),
(761, '020.721', '010.33'),
(762, '020.705', '010.33'),
(763, '020.707', '010.33'),
(764, '020.740', '010.33'),
(765, '020.743', '010.33'),
(766, '020.733', '010.33'),
(767, '020.734', '010.33'),
(768, '020.735', '010.33'),
(769, '020.736', '010.33'),
(770, '020.709', '010.33'),
(771, '020.727', '010.33'),
(772, '020.738', '010.33'),
(773, '020.765', '010.33'),
(774, '020.817', '010.33'),
(775, '020.798', '010.33'),
(776, '020.829', '010.33'),
(777, '020.830', '010.33'),
(778, '020.821', '010.33'),
(779, '020.822', '010.33'),
(780, '020.823', '010.33'),
(781, '020.824', '010.33'),
(782, '020.825', '010.33'),
(783, '020.826', '010.33'),
(784, '020.827', '010.33'),
(785, '020.834', '010.33'),
(786, '020.800', '010.33'),
(787, '020.832', '010.33'),
(788, '020.837', '010.33'),
(789, '020.839', '010.33'),
(790, '020.842', '010.33'),
(791, '020.843', '010.33'),
(792, '020.844', '010.33'),
(793, '020.845', '010.33'),
(794, '020.846', '010.33'),
(795, '020.847', '010.33'),
(796, '020.794', '010.33'),
(797, '020.795', '010.33'),
(798, '020.796', '010.33'),
(799, '020.790', '010.33'),
(800, '020.791', '010.33'),
(801, '020.802', '010.33'),
(802, '020.803', '010.33'),
(803, '020.804', '010.33'),
(804, '020.805', '010.33'),
(805, '020.806', '010.33'),
(806, '020.807', '010.33'),
(807, '020.808', '010.33'),
(808, '020.809', '010.33'),
(809, '020.810', '010.33'),
(810, '020.811', '010.33'),
(811, '020.812', '010.33'),
(812, '020.813', '010.33'),
(813, '020.814', '010.33'),
(814, '020.815', '010.33'),
(815, '020.776', '010.33'),
(816, '020.752', '010.33'),
(817, '020.753', '010.33'),
(818, '020.774', '010.33'),
(819, '020.784', '010.33'),
(820, '020.785', '010.33'),
(821, '020.786', '010.33'),
(822, '020.878', '010.33'),
(823, '020.879', '010.33'),
(824, '020.880', '010.33'),
(825, '020.874', '010.33'),
(826, '020.875', '010.33'),
(827, '020.876', '010.33'),
(828, '020.883', '010.33'),
(829, '020.884', '010.33'),
(830, '020.885', '010.33'),
(831, '020.886', '010.33'),
(832, '020.887', '010.33'),
(833, '020.888', '010.33'),
(834, '020.889', '010.33'),
(835, '020.890', '010.33'),
(836, '020.865', '010.33'),
(837, '020.866', '010.33'),
(838, '020.867', '010.33'),
(839, '020.862', '010.33'),
(840, '020.863', '010.33'),
(841, '020.852', '010.33'),
(842, '020.853', '010.33'),
(843, '020.854', '010.33'),
(844, '020.855', '010.33'),
(845, '020.856', '010.33'),
(846, '020.850', '010.33'),
(847, '020.859', '010.33'),
(848, '020.860', '010.33'),
(849, '020.2698', '010.33'),
(850, '020.2695', '010.33'),
(851, '020.2696', '010.33'),
(852, '020.3604', '010.33'),
(853, '020.3605', '010.33'),
(854, '020.1785', '010.33'),
(855, '020.1786', '010.33'),
(856, '020.1788', '010.33'),
(857, '020.1789', '010.33'),
(858, '020.893', '010.33'),
(859, '020.894', '010.33'),
(860, '020.871', '010.33'),
(861, '020.872', '010.33'),
(862, '020.4525', '010.33'),
(863, '020.4523', '010.35'),
(864, '020.4513', '010.35'),
(865, '020.891', '010.35'),
(866, '020.630', '010.35'),
(867, '020.4715', '010.35'),
(868, '020.4716', '010.35'),
(869, '020.4707', '010.35'),
(870, '020.4713', '010.35'),
(871, '020.4718', '010.35'),
(872, '020.4729', '010.35'),
(873, '020.4726', '010.35'),
(874, '020.4677', '010.35'),
(875, '020.4666', '010.35'),
(876, '020.4667', '010.35'),
(877, '020.4679', '010.35'),
(878, '020.4682', '010.35'),
(879, '020.4696', '010.35'),
(880, '020.4702', '010.35'),
(881, '020.4592', '010.35'),
(882, '020.4593', '010.35'),
(883, '020.4594', '010.35'),
(884, '020.4595', '010.35'),
(885, '020.4573', '010.35'),
(886, '020.4577', '010.35'),
(887, '020.4565', '010.35'),
(888, '020.4555', '010.35'),
(889, '020.4570', '010.35'),
(890, '020.4538', '010.35'),
(891, '020.4539', '010.35'),
(892, '020.4533', '010.35'),
(893, '020.4546', '010.35'),
(894, '020.4547', '010.35'),
(895, '020.4543', '010.35'),
(896, '020.4562', '010.35'),
(897, '020.4558', '010.35'),
(898, '020.4610', '010.35'),
(899, '020.4611', '010.35'),
(900, '020.4612', '010.35'),
(901, '020.4607', '010.35'),
(902, '020.4601', '010.35'),
(903, '020.4625', '010.35'),
(904, '020.4626', '010.35'),
(905, '020.4633', '010.35'),
(906, '020.4634', '010.35'),
(907, '020.4638', '010.35'),
(908, '020.4639', '010.35'),
(909, '020.4641', '010.35'),
(910, '020.4642', '010.35'),
(911, '020.4650', '010.35'),
(912, '020.4647', '010.35'),
(913, '020.4648', '010.35'),
(914, '020.4621', '010.35'),
(915, '020.4644', '010.35'),
(916, '020.4656', '010.35'),
(917, '020.4652', '010.35'),
(918, '020.4669', '010.35'),
(919, '020.4664', '010.35'),
(920, '020.4661', '010.35'),
(921, '020.4662', '010.35'),
(922, '020.377', '010.35'),
(923, '020.334', '010.35'),
(924, '020.335', '010.35'),
(925, '020.428', '010.35'),
(926, '020.384', '010.35'),
(927, '020.577', '010.35'),
(928, '020.579', '010.35'),
(929, '020.586', '010.35'),
(930, '020.620', '010.35'),
(931, '020.142', '010.35'),
(932, '020.262', '010.35'),
(933, '020.189', '010.35'),
(934, '020.176', '010.35'),
(935, '020.4542', '010.36'),
(936, '020.4575', '010.36'),
(937, '020.674', '010.37'),
(938, '020.676', '010.37'),
(939, '020.695', '010.37'),
(940, '020.696', '010.37'),
(941, '020.678', '010.37'),
(942, '020.685', '010.37'),
(943, '020.682', '010.37'),
(944, '020.683', '010.37'),
(945, '020.766', '010.37'),
(946, '020.767', '010.37'),
(947, '020.768', '010.37'),
(948, '020.770', '010.37'),
(949, '020.771', '010.37'),
(950, '020.772', '010.37'),
(951, '020.773', '010.37'),
(952, '020.756', '010.37'),
(953, '020.757', '010.37'),
(954, '020.758', '010.37'),
(955, '020.759', '010.37'),
(956, '020.760', '010.37'),
(957, '020.761', '010.37'),
(958, '020.762', '010.37'),
(959, '020.763', '010.37'),
(960, '020.764', '010.37'),
(961, '020.739', '010.37'),
(962, '020.737', '010.37'),
(963, '020.744', '010.37'),
(964, '020.745', '010.37'),
(965, '020.741', '010.37'),
(966, '020.742', '010.37'),
(967, '020.747', '010.37'),
(968, '020.748', '010.37'),
(969, '020.749', '010.37'),
(970, '020.750', '010.37'),
(971, '020.751', '010.37'),
(972, '020.708', '010.37'),
(973, '020.706', '010.37'),
(974, '020.722', '010.37'),
(975, '020.719', '010.37'),
(976, '020.720', '010.37'),
(977, '020.726', '010.37'),
(978, '020.724', '010.37'),
(979, '020.729', '010.37'),
(980, '020.730', '010.37'),
(981, '020.731', '010.37'),
(982, '020.732', '010.37'),
(983, '020.702', '010.37'),
(984, '020.703', '010.37'),
(985, '020.700', '010.37'),
(986, '020.687', '010.37'),
(987, '020.698', '010.37'),
(988, '020.714', '010.37'),
(989, '020.715', '010.37'),
(990, '020.716', '010.37'),
(991, '020.717', '010.37'),
(992, '020.861', '010.37'),
(993, '020.851', '010.37'),
(994, '020.857', '010.37'),
(995, '020.858', '010.37'),
(996, '020.870', '010.37'),
(997, '020.787', '010.37'),
(998, '020.775', '010.37'),
(999, '020.754', '010.37'),
(1000, '020.777', '010.37'),
(1001, '020.778', '010.37'),
(1002, '020.779', '010.37'),
(1003, '020.780', '010.37'),
(1004, '020.781', '010.37'),
(1005, '020.782', '010.37'),
(1006, '020.783', '010.37'),
(1007, '020.792', '010.37'),
(1008, '020.789', '010.37'),
(1009, '020.797', '010.37'),
(1010, '020.840', '010.37'),
(1011, '020.841', '010.37'),
(1012, '020.849', '010.37'),
(1013, '020.838', '010.37'),
(1014, '020.835', '010.37'),
(1015, '020.836', '010.37'),
(1016, '020.828', '010.37'),
(1017, '020.831', '010.37'),
(1018, '020.820', '010.37'),
(1019, '020.799', '010.37'),
(1020, '020.818', '010.37'),
(1021, '020.165', '010.37'),
(1022, '020.163', '010.37'),
(1023, '020.192', '010.37'),
(1024, '020.197', '010.37'),
(1025, '020.207', '010.37'),
(1026, '020.208', '010.37'),
(1027, '020.209', '010.37'),
(1028, '020.200', '010.37'),
(1029, '020.201', '010.37'),
(1030, '020.203', '010.37'),
(1031, '020.216', '010.37'),
(1032, '020.233', '010.37'),
(1033, '020.239', '010.37'),
(1034, '020.236', '010.37'),
(1035, '020.148', '010.37'),
(1036, '020.149', '010.37'),
(1037, '020.156', '010.37'),
(1038, '020.113', '010.37'),
(1039, '020.114', '010.37'),
(1040, '020.122', '010.37'),
(1041, '020.94', '010.37'),
(1042, '020.100', '010.37'),
(1043, '020.64', '010.37'),
(1044, '020.84', '010.37'),
(1045, '020.12', '010.37'),
(1046, '020.17', '010.37'),
(1047, '020.69', '010.37'),
(1048, '020.70', '010.37'),
(1049, '020.62', '010.37'),
(1050, '020.74', '010.37'),
(1051, '020.608', '010.37'),
(1052, '020.599', '010.37'),
(1053, '020.538', '010.37'),
(1054, '020.523', '010.37'),
(1055, '020.510', '010.37'),
(1056, '020.535', '010.37'),
(1057, '020.433', '010.37'),
(1058, '020.437', '010.37'),
(1059, '020.404', '010.37'),
(1060, '020.392', '010.39'),
(1061, '020.479', '010.39'),
(1062, '020.187', '010.39'),
(1063, '020.330', '010.40'),
(1064, '020.1794', '010.40'),
(1065, '020.4698', '010.42'),
(1066, '020.644', '010.43'),
(1067, '020.4515', '010.44'),
(1068, '020.4699', '010.44'),
(1069, '020.4606', '010.44'),
(1070, '020.655', '010.45'),
(1071, '020.627', '010.45'),
(1072, '020.417', '010.45'),
(1073, '020.490', '010.45'),
(1074, '020.491', '010.45'),
(1075, '020.614', '010.45'),
(1076, '020.568', '010.48'),
(1077, '020.449', '010.48'),
(1078, '020.63', '010.48'),
(1079, '020.104', '010.48'),
(1080, '020.105', '010.48'),
(1081, '020.868', '010.48'),
(1082, '020.4609', '010.48'),
(1083, '020.4602', '010.48'),
(1084, '020.4605', '010.48'),
(1085, '020.4628', '010.48'),
(1086, '020.4629', '010.48'),
(1087, '020.4630', '010.48'),
(1088, '020.4623', '010.48'),
(1089, '020.4624', '010.48'),
(1090, '020.4618', '010.48'),
(1091, '020.4619', '010.48'),
(1092, '020.4620', '010.48'),
(1093, '020.4665', '010.48'),
(1094, '020.4670', '010.48'),
(1095, '020.4671', '010.48'),
(1096, '020.4653', '010.48'),
(1097, '020.4654', '010.48'),
(1098, '020.4660', '010.48'),
(1099, '020.4646', '010.48'),
(1100, '020.4643', '010.48'),
(1101, '020.4636', '010.48'),
(1102, '020.4574', '010.48'),
(1103, '020.4567', '010.48'),
(1104, '020.4591', '010.48'),
(1105, '020.4559', '010.48'),
(1106, '020.4560', '010.48'),
(1107, '020.4561', '010.48'),
(1108, '020.4563', '010.48'),
(1109, '020.4557', '010.48'),
(1110, '020.4550', '010.48'),
(1111, '020.4554', '010.48'),
(1112, '020.4700', '010.48'),
(1113, '020.4701', '010.48'),
(1114, '020.4703', '010.48'),
(1115, '020.4706', '010.48'),
(1116, '020.4695', '010.48'),
(1117, '020.4683', '010.48'),
(1118, '020.4685', '010.48'),
(1119, '020.4686', '010.48'),
(1120, '020.4687', '010.48'),
(1121, '020.4676', '010.48'),
(1122, '020.4720', '010.48'),
(1123, '020.4717', '010.48'),
(1124, '020.4710', '010.48'),
(1125, '020.4711', '010.48'),
(1126, '020.4712', '010.48'),
(1127, '020.4674', '010.49'),
(1128, '020.4684', '010.49'),
(1129, '020.4549', '010.49'),
(1130, '020.4590', '010.49'),
(1131, '020.4566', '010.49'),
(1132, '020.4571', '010.49'),
(1133, '020.4572', '010.49'),
(1134, '020.4599', '010.49'),
(1135, '020.4600', '010.49'),
(1136, '020.4731', '010.110'),
(1137, '020.4733', '010.110'),
(1138, '020.14737', '010.110'),
(1139, '020.14738', '010.111'),
(1140, '020.4734', '010.111'),
(1141, '020.4732', '010.111'),
(1142, '020.54735', '010.1116'),
(1143, '020.54738', '010.1116'),
(1144, '020.54736', '010.1117'),
(1145, '020.54737', '010.1118');

-- --------------------------------------------------------

--
-- Table structure for table `0_exchange_rates`
--

CREATE TABLE `0_exchange_rates` (
  `id` int(11) NOT NULL,
  `curr_code` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rate_buy` double NOT NULL DEFAULT '0',
  `rate_sell` double NOT NULL DEFAULT '0',
  `date_` date NOT NULL DEFAULT '0000-00-00',
  `tax_rate` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_fiscal_year`
--

CREATE TABLE `0_fiscal_year` (
  `id` int(11) NOT NULL,
  `begin` date DEFAULT '0000-00-00',
  `end` date DEFAULT '0000-00-00',
  `closed` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_fiscal_year`
--

INSERT INTO `0_fiscal_year` (`id`, `begin`, `end`, `closed`) VALUES
(1, '2016-01-01', '2016-12-31', 1),
(2, '2017-01-01', '2017-12-31', 0),
(3, '2018-01-01', '2018-12-31', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_fp_trans`
--

CREATE TABLE `0_fp_trans` (
  `id` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `reference` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_gl_trans`
--

CREATE TABLE `0_gl_trans` (
  `counter` int(11) NOT NULL,
  `type` smallint(6) NOT NULL DEFAULT '0',
  `type_no` int(11) NOT NULL DEFAULT '0',
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `memo_` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `dimension_id` int(11) NOT NULL DEFAULT '0',
  `dimension2_id` int(11) NOT NULL DEFAULT '0',
  `person_type_id` int(11) DEFAULT NULL,
  `person_id` tinyblob,
  `posting` tinyint(1) NOT NULL,
  `jasa` int(11) DEFAULT NULL,
  `gross` double DEFAULT NULL,
  `rate` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_grn_batch`
--

CREATE TABLE `0_grn_batch` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `purch_order_no` int(11) DEFAULT NULL,
  `reference` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `delivery_date` date NOT NULL DEFAULT '0000-00-00',
  `loc_code` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rate` double DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_grn_items`
--

CREATE TABLE `0_grn_items` (
  `id` int(11) NOT NULL,
  `grn_batch_id` int(11) DEFAULT NULL,
  `po_detail_item` int(11) NOT NULL DEFAULT '0',
  `item_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` tinytext COLLATE utf8_unicode_ci,
  `qty_recd` double NOT NULL DEFAULT '0',
  `quantity_inv` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_groups`
--

CREATE TABLE `0_groups` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `description` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_item_codes`
--

CREATE TABLE `0_item_codes` (
  `id` int(11) UNSIGNED NOT NULL,
  `item_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `stock_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `category_id` smallint(6) UNSIGNED NOT NULL,
  `quantity` double NOT NULL DEFAULT '1',
  `is_foreign` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_item_codes`
--

INSERT INTO `0_item_codes` (`id`, `item_code`, `stock_id`, `description`, `category_id`, `quantity`, `is_foreign`, `inactive`) VALUES
(1, 'FF-001', 'FF-001', 'Pelunasan perkerjaan Furniture Credensa R. Analis', 1, 1, 0, 0),
(2, 'FF-002', 'FF-002', 'Workstation', 1, 1, 0, 0),
(3, 'FF-003', 'FF-003', 'Credenza Meja 1500x500x850', 1, 1, 0, 0),
(4, 'FF-004', 'FF-004', 'Kursi Bp Gama', 1, 1, 0, 0),
(5, 'FF-005', 'FF-005', 'Lemari Cabinet, Supply & Instal Sand Blast Sticker Ruang Bp. Gama', 1, 1, 0, 0),
(6, 'FF-006', 'FF-006', 'Furniture (credenza) di ruangan Bp. Gama', 1, 1, 0, 0),
(7, 'FF-007', 'FF-007', 'Kursi Tamu di ruangan Bp. Gama', 1, 1, 0, 0),
(8, 'FF-008', 'FF-008', 'Office Furniture (Runway Desk & Director Chair)', 1, 1, 0, 0),
(9, 'FF-009', 'FF-009', 'Rak Server (Furniture IT) - PT. Multipro Jaya Prima', 1, 1, 0, 0),
(10, 'FF-0010', 'FF-0010', '4 Kursi Leather Boss (Direksi, Head Marketing, 2 org Fund Manager)', 1, 1, 0, 0),
(11, 'FF-011', 'FF-011', 'Furniture (2 Meja Dealer, 6 Meja Analis dan 6 Kursi Hadap)', 1, 1, 0, 0),
(12, 'FF-012', 'FF-012', 'Office Furniture (1 Credenza dan 10 kursi untuk ruang meeting kecil)', 1, 1, 0, 0),
(13, 'FF-013', 'FF-013', 'Pembelian Furniture (Credenza, Coffee table dan Lemari file)', 1, 1, 0, 0),
(14, 'FF-014', 'FF-014', '3 Pcs Gantungan Jas', 1, 1, 0, 0),
(15, 'FF-015', 'FF-015', 'Furniture (3 unit Credenza dan Coffe Table utk Bp. Fajar, Gunanta dan Cholis)', 1, 1, 0, 0),
(16, 'FF-016', 'FF-016', 'Furniture - 3 Unit Sofa (Bp. Fajar, Gunanta dan Cholis)', 1, 1, 0, 0),
(17, 'FF-017', 'FF-017', '1 Unit Lemari Kabinet utk Bp. Gunanta', 1, 1, 0, 0),
(18, 'OE-001', 'OE-001', 'PC HP, Ms.Windows 7, Ms.Office, Antivirus - Enos', 2, 1, 0, 0),
(19, 'OE-002', 'OE-002', 'PC HP  - Alvin', 2, 1, 0, 0),
(20, 'OE-003', 'OE-003', 'PC HP - Deasty', 2, 1, 0, 0),
(21, 'OE-004', 'OE-004', 'PC Pavillion, Ms.Office, Ms.Windows, Kaspersky - Monica', 2, 1, 0, 0),
(22, 'OE-005', 'OE-005', '2 Server + 4 Harddisk', 2, 1, 0, 0),
(23, 'OE-006', 'OE-006', 'UPS APC', 2, 1, 0, 0),
(24, 'OE-007', 'OE-007', 'Windows Server ', 2, 1, 0, 0),
(25, 'OE-008', 'OE-008', 'Printer HP Laser Jet Pro P1102 - Mulia', 2, 1, 0, 0),
(26, 'OE-009', 'OE-009', 'Laptop Asus, win 7, Office 2010, Kaspersky -u/Training', 2, 1, 0, 0),
(27, 'OE-0010', 'OE-0010', 'Asus Wireless + Hub d-link', 2, 1, 0, 0),
(28, 'OE-011', 'OE-011', 'Harddisk server HP 2 unit', 2, 1, 0, 0),
(29, 'OE-012', 'OE-012', 'Printer Warna u/ Back office', 2, 1, 0, 0),
(30, 'OE-013', 'OE-013', 'UPS u/ server', 2, 1, 0, 0),
(31, 'OE-014', 'OE-014', 'Panasonic Laser Fax', 2, 1, 0, 0),
(32, 'OE-015', 'OE-015', 'Harddisk ', 2, 1, 0, 0),
(33, 'OE-016', 'OE-016', '2 PC Iin & Eko, Windows 7, MS Office', 2, 1, 0, 0),
(34, 'OE-017', 'OE-017', 'WinPro 8,1 5 unit - Yudha, Yusuf, Liantie, Hery, P.David', 2, 1, 0, 0),
(35, 'OE-018', 'OE-018', 'Software win 7, office 2013 - Zara & R. Meeting', 2, 1, 0, 0),
(36, 'OE-019', 'OE-019', 'Air Cleaner Austin (Penghirup Asap utk ruangan P.David)', 2, 1, 0, 0),
(37, 'OE-020', 'OE-020', 'PC Komputer Asus Desktop, Monitor LED, Ms.Office, Windows 7 - Prita', 2, 1, 0, 0),
(38, 'OE-021', 'OE-021', 'Laptop Asus, win 7, Office 2013, Mouse -u/Marketing (No. Ref Transaksi: BO401043 dan BO401044)', 2, 1, 0, 0),
(39, 'OE-022', 'OE-022', 'HP Pavilion 500-330d DT PC, Office 2013, OS 7 Pro 64 bit, RAM 8 GB DDR 3, Kaspersky - untuk RH', 2, 1, 0, 0),
(40, 'OE-023', 'OE-023', 'Nokia 1520, Wireless Charging Plate - Untuk JP', 2, 1, 0, 0),
(41, 'OE-024', 'OE-024', 'Alcatel - Lucent IP Touch 4068 Phone (Untuk Bp. Gama), Alcatel - Lucent IP Digital 4019 Phone (Untuk Back Up)', 2, 1, 0, 0),
(42, 'OE-025', 'OE-025', 'HP Pavilion Slimline 400-326X, Office 2013, Windows 7 32 Bit, Kaspersky - Computer untuk Elvia', 2, 1, 0, 0),
(43, 'OE-026', 'OE-026', 'HP Pavilion Slimline, Office 2013, Windows 8, Kaspersky, VGA, Mouse Logitech dan Mikrotik Wireless utk server (2 PC untuk Marketing dan 1 PC untuk ruang meeting)', 2, 1, 0, 0),
(44, 'OE-027', 'OE-027', 'Pembelian 1 Unit Computer Dell Inspiron 3847MT, Windows 7 dan Office 2013 - Alvin Michael (Analis)', 2, 1, 0, 0),
(45, 'OE-028', 'OE-028', '2 Unit Pesawat Telepon Panasonic (Dhini dan Alvin M)', 2, 1, 0, 0),
(46, 'OE-029', 'OE-029', 'BenQ Projector MX620ST', 2, 1, 0, 0),
(47, 'OE-030', 'OE-030', '3 Unit PC HP Pavilion 550-127D untuk BOD, 1 Unit PC HP Pavilion 450-122D untuk Fund Manager, 4 Pcs Office Home&Business 2013, 4 Unit Monitor Led 20\'', 2, 1, 0, 0),
(48, 'OE-031', 'OE-031', 'Atmosphere Air Treatment System', 2, 1, 0, 0),
(49, 'OE-032', 'OE-032', '1 unit HP Desktop 251-122d dan 1 pcs Office Home&Business 2013 untuk Head Finance and Accounting', 2, 1, 0, 0),
(50, 'OE-033', 'OE-033', '1 unit UPS APC SMT 3000I untuk BOD', 2, 1, 0, 0),
(51, 'OE-034', 'OE-034', '1 unit UPS APC SMT 3000I untuk Head Finance and Accounting', 2, 1, 0, 0),
(52, 'OE-035', 'OE-035', '4 Unit Atmosphere Air Treatment System @Rp17.226.000', 2, 1, 0, 0),
(53, 'OE-036', 'OE-036', 'Pembelian Peralatan Kantor - 3 Unit Paper Shredder (Bp. Fajar, Gunanta & Cholis), 1 Unit TV (Bp. Fajar) dan 3 Unit Telepon (Bp. Fajar, Gunanta & Cholis)', 2, 1, 0, 0),
(54, 'OE-037', 'OE-037', '3 Unit Printer HP Laserjet CP1025 (Bp. Fajar, Gunanta dan Cholis)', 2, 1, 0, 0),
(55, 'OE-038', 'OE-038', 'embelian Server (HP Proliant DL380p-G9/E5-2640v3 dan DL380p-G9/2p e5-2640v3)', 2, 1, 0, 0),
(56, 'OE-039', 'OE-039', 'DP 50% Jasa Pembuatan Sistem KAM - PT. Praisindo Teknologi', 2, 1, 0, 0),
(57, 'OE-040', 'OE-040', '5 Unit HP Pavilion 550-0201, 7 Unit Microsoft Office 2013, 2 Unit Kaspersky, 2 Unit HP Envy 750-101D, 5 Unit CORSAIR Memory PC 4 GB DDR3 dan 2 Unit CORSAIR Memory PC 1 x 4 GB DDR4', 2, 1, 0, 0),
(58, 'OE-041', 'OE-041', 'PT.Bhinneka Mentaridimensi - Peralatan Kantor -1 Unit Vacuum Cleaner', 2, 1, 0, 0),
(59, 'OE-042', 'OE-042', '2 unit Alcatel Lucent 4019 Digital Phone Set (Tito dan Ratih) - PT. Jaringan Intech Indonesia', 2, 1, 0, 0),
(60, 'OE-043', 'OE-043', 'Camera Mirrorless - Sony Alpha A6000 Kit 16-50mm', 2, 1, 0, 0),
(61, 'OE-044', 'OE-044', '1 Unit TV - JP', 2, 1, 0, 0),
(62, 'IMP-001', 'IMP-001', 'Material Raised Floor dan Biaya pasang', 3, 1, 0, 0),
(63, 'IMP-002', 'IMP-002', 'Material Raised Floor dan Biaya pasang', 3, 1, 0, 0),
(64, 'IMP-003', 'IMP-003', 'Instalasi FCU Chiller (30% DP)', 3, 1, 0, 0),
(65, 'IMP-004', 'IMP-004', 'Relokasi Instalasi Sprinkkler, Smoke Detector, Evacuation Speaker dan AC Gedung (50% DP)', 3, 1, 0, 0),
(66, 'IMP-005', 'IMP-005', 'Pekerjaan Listrik dan Data Voice (50% DP)', 3, 1, 0, 0),
(67, 'IMP-006', 'IMP-006', 'Pekerjaan Interior - Renovasi kantor - PT. Fertikal Horizontal Citrakarya Selaras', 3, 1, 0, 0),
(68, 'IMP-007', 'IMP-007', 'Pekerjaan Listrik dan Data Voice (Termin II)', 3, 1, 0, 0),
(69, 'IMP-008', 'IMP-008', 'Pembayaran Instalasi FCU Chiller (Termin II dan III) dan Pekerjaan tambahan', 3, 1, 0, 0),
(70, 'IMP-009', 'IMP-009', 'Pembayaran Instalasi FCU Chiller (Termin II dan III) dan Pekerjaan tambahan', 3, 1, 0, 0),
(71, 'IMP-0010', 'IMP-0010', 'Pekerjaan Listrik dan Data Voice (Termin III)', 3, 1, 0, 0),
(72, 'IMP-011', 'IMP-011', 'Pekerjaan Interior - Renovasi kantor (Termin II) - PT. Fertikal Horizontal Citrakarya Selaras', 3, 1, 0, 0),
(73, 'IMP-012', 'IMP-012', 'Pekerjaan Interior - Renovasi kantor (Termin III) - PT. Fertikal Horizontal Citrakarya Selaras', 3, 1, 0, 0),
(74, 'IMP-013', 'IMP-013', 'Pelunasan Pembayaran Relokasi Instalasi Sprinkkler, Smoke Detector, Evacuation Speaker dan Penambahan KWH Listrik - Achmad Subchi', 3, 1, 0, 0),
(75, 'IMP-014', 'IMP-014', 'Tambahan Pekerjaan Listrik dan Data Voice - Nurul Priatin', 3, 1, 0, 0),
(76, 'VHC-001', 'VHC-001', 'Toyota Fortuner', 4, 1, 0, 0),
(77, 'VHC-002', 'VHC-002', 'Toyota Alphard', 4, 1, 0, 0),
(78, 'VHC-003', 'VHC-003', 'Mercedes Benz (B165 HIM)', 4, 1, 0, 0),
(79, 'VHC-004', 'VHC-004', 'Odysey', 4, 1, 0, 0),
(80, 'VHC-005', 'VHC-005', 'Subaru Forester', 4, 1, 0, 0),
(81, 'VHC-006', 'VHC-006', 'Mobil Harrier', 4, 1, 0, 0),
(82, 'VHC-007', 'VHC-007', 'BMW M4 Coupe', 4, 1, 0, 0),
(83, 'VHC-008', 'VHC-008', 'Bangunan', 4, 1, 0, 0),
(84, 'ATK01', 'ATK01', 'Mesin Fotokopi', 5, 1, 0, 0),
(85, 'PDPT001', 'PDPT001', 'Jasa Manajemen', 6, 1, 0, 0),
(86, '1554545', '1554545', 'Komputer', 1, 1, 0, 0),
(87, '23', '23', 'Computer', 5, 1, 0, 0),
(88, 'GE001', 'GE001', 'ATK General', 5, 1, 0, 0),
(92, 'BLD01', 'BLD01', 'Ruang Finance', 1, 1, 0, 0),
(93, 'BLD02', 'BLD02', 'Ruang HRD', 1, 1, 0, 0),
(94, 'SOF01', 'SOF01', 'Software Accounting', 2, 1, 0, 0),
(95, 'Genset', 'Genset', 'Genset ', 5, 1, 0, 0),
(96, 'PDPT002', 'PDPT002', 'Sharing Fee', 6, 1, 0, 0),
(97, 'GE002', 'GE002', 'Keperluan Dapur', 5, 1, 0, 0),
(98, 'Akta', 'Akta', 'Akta', 5, 1, 0, 0),
(99, 'Tanaman', 'Tanaman', 'Tanaman', 5, 1, 0, 0),
(100, 'Bloomberg', 'Bloomberg', 'Bloomberg', 5, 1, 0, 0),
(101, 'genset2', 'genset2', 'genset2', 5, 1, 0, 0),
(102, 'M', 'M', 'Management Fee', 6, 1, 0, 0),
(103, 'P', 'P', 'Sharing Fee', 6, 1, 0, 0),
(104, 'TB', 'TB', 'Tanah Test', 1, 1, 0, 0),
(105, '001', '001', 'ATK', 5, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_item_tax_types`
--

CREATE TABLE `0_item_tax_types` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `exempt` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_item_tax_types`
--

INSERT INTO `0_item_tax_types` (`id`, `name`, `exempt`, `inactive`) VALUES
(1, 'PPN dan PPH Ps 23', 0, 1),
(2, 'PPN &amp; WTH', 0, 1),
(3, 'PPH Ps 23', 0, 0),
(4, 'PPN.', 0, 1),
(6, 'PPN dan PPh Ps 4(2)', 0, 0),
(7, 'PPN dan PPh Ps 21', 0, 1),
(8, 'PPH Ps 21', 0, 0),
(9, 'PPH Ps 26', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `0_item_tax_type_exemptions`
--

CREATE TABLE `0_item_tax_type_exemptions` (
  `item_tax_type_id` int(11) NOT NULL DEFAULT '0',
  `tax_type_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_item_tax_type_exemptions`
--

INSERT INTO `0_item_tax_type_exemptions` (`item_tax_type_id`, `tax_type_id`) VALUES
(3, 2),
(4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `0_item_units`
--

CREATE TABLE `0_item_units` (
  `abbr` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `decimals` tinyint(2) NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_item_units`
--

INSERT INTO `0_item_units` (`abbr`, `name`, `decimals`, `inactive`) VALUES
('each', 'Each', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_journal`
--

CREATE TABLE `0_journal` (
  `type` smallint(6) NOT NULL DEFAULT '0',
  `trans_no` int(11) NOT NULL DEFAULT '0',
  `tran_date` date DEFAULT '0000-00-00',
  `reference` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `source_ref` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `event_date` date DEFAULT '0000-00-00',
  `doc_date` date NOT NULL DEFAULT '0000-00-00',
  `currency` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `amount` double NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '1',
  `posting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_locations`
--

CREATE TABLE `0_locations` (
  `loc_code` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `location_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `delivery_address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone2` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fax` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fixed_asset` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `memo` tinytext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_locations`
--

INSERT INTO `0_locations` (`loc_code`, `location_name`, `delivery_address`, `phone`, `phone2`, `fax`, `email`, `contact`, `fixed_asset`, `inactive`, `memo`) VALUES
('DEF', 'Default', 'N/A', '', '', '', '', '', 0, 0, ''),
('OF01', 'Office Empu Sendok', 'Jln. Empu Sendok', '', '', '', '', 'Panito', 1, 1, NULL),
('OF02', 'Finance and Accounting', 'PT. Kresna Asset Management, Gedung Bursa Efek Indonesia, Tower II, Lantai 23, Suite 2303, Jl. Jendral Sudirman, Kav. 52-53', '2151400888', '02151400888', '2151400888', 'julia@KAMcapital.com', 'Julia', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `0_loc_stock`
--

CREATE TABLE `0_loc_stock` (
  `loc_code` char(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `stock_id` char(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reorder_level` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_loc_stock`
--

INSERT INTO `0_loc_stock` (`loc_code`, `stock_id`, `reorder_level`) VALUES
('DEF', '1554545', 0),
('DEF', '23', 0),
('DEF', 'Akta', 0),
('DEF', 'ATK01', 0),
('DEF', 'BLD01', 0),
('DEF', 'BLD02', 0),
('DEF', 'Bloomberg', 0),
('DEF', 'GE001', 0),
('DEF', 'GE002', 0),
('DEF', 'Genset', 0),
('DEF', 'genset2', 0),
('DEF', 'M', 0),
('DEF', 'P', 0),
('DEF', 'PDPT001', 0),
('DEF', 'PDPT002', 0),
('DEF', 'SOF01', 0),
('DEF', 'Tanaman', 0),
('DEF', 'TB', 0),
('DEF', 'tes', 0),
('OF01', '1554545', 0),
('OF01', '23', 0),
('OF01', 'Akta', 0),
('OF01', 'ATK01', 0),
('OF01', 'BLD01', 0),
('OF01', 'BLD02', 0),
('OF01', 'Bloomberg', 0),
('OF01', 'GE001', 0),
('OF01', 'GE002', 0),
('OF01', 'Genset', 0),
('OF01', 'genset2', 0),
('OF01', 'M', 0),
('OF01', 'P', 0),
('OF01', 'PDPT001', 0),
('OF01', 'PDPT002', 0),
('OF01', 'SOF01', 0),
('OF01', 'Tanaman', 0),
('OF01', 'TB', 0),
('OF01', 'tes', 0),
('OF02', '1554545', 0),
('OF02', '23', 0),
('OF02', 'Akta', 0),
('OF02', 'ATK01', 0),
('OF02', 'BLD01', 0),
('OF02', 'BLD02', 0),
('OF02', 'Bloomberg', 0),
('OF02', 'GE001', 0),
('OF02', 'GE002', 0),
('OF02', 'Genset', 0),
('OF02', 'genset2', 0),
('OF02', 'M', 0),
('OF02', 'P', 0),
('OF02', 'PDPT001', 0),
('OF02', 'PDPT002', 0),
('OF02', 'SOF01', 0),
('OF02', 'Tanaman', 0),
('OF02', 'TB', 0),
('OF02', 'tes', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_marketing_trans`
--

CREATE TABLE `0_marketing_trans` (
  `id` int(11) NOT NULL,
  `ref` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marketing_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marketing_place` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marketing_address` text COLLATE utf8_unicode_ci,
  `marketing_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marketing_position` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marketing_company` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marketing_business` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npwp` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bpotong` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sklien` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ssyai` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `line` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_mkbd`
--

CREATE TABLE `0_mkbd` (
  `id` int(11) NOT NULL,
  `lamp` int(11) NOT NULL,
  `no` int(11) NOT NULL,
  `label` varchar(100) NOT NULL,
  `parent` int(11) NOT NULL,
  `level` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_mkbd`
--

INSERT INTO `0_mkbd` (`id`, `lamp`, `no`, `label`, `parent`, `level`) VALUES
(1, 1, 8, 'Aset Lancar', 0, 0),
(2, 1, 9, 'Kas dan Setara Kas', 0, 0),
(3, 1, 10, 'Simpanan Giro Bank', 0, 1),
(4, 1, 11, 'Kas yang Dibatasi Penggunaannya', 0, 0),
(5, 1, 12, 'Kas yang Dipisahkan', 0, 1),
(6, 1, 13, 'Rekening qq. Efek Nasabah', 0, 1),
(7, 1, 14, 'Deposito Berjangka', 0, 0),
(8, 1, 15, 'Deposito Bank Dalam Negeri', 0, 1),
(9, 1, 16, 'Deposito Bank Umum dengan jangka waktu kurang  atau sama dengan 3 (tiga) bulan', 0, 2),
(10, 1, 17, 'Deposito Bank Umum dengan jangka waktu lebih dari 3 (tiga) bulan', 0, 2),
(11, 1, 18, '     Dijamin oleh Lembaga Penjamin Simpanan', 0, 2),
(12, 1, 19, '     Tidak Dijamin oleh Lembaga Penjamin Simpanan', 0, 2),
(13, 1, 20, 'Tidak Sedang Diajukan atau Dinyatakan Pailit/Dilikuidasi', 0, 3),
(14, 1, 21, 'Sedang diajukan atau Dinyatakan Pailit/Dilikuidasi', 0, 3),
(15, 1, 22, 'Deposito pada Bank Perkreditan Rakyat', 0, 2),
(16, 1, 23, 'Deposito Bank di Luar Negeri', 0, 1),
(17, 1, 24, 'Piutang Reverse Repo', 0, 0),
(18, 1, 25, 'Reverse Repo Surat Berharga Negara', 0, 1),
(19, 1, 26, 'Reverse Repo Obligasi Korporasi', 0, 1),
(20, 1, 27, 'Reverse Repo Efek Bersifat Ekuitas', 0, 1),
(21, 1, 28, 'Piutang Lembaga Kliring dan Penjaminan', 0, 0),
(22, 1, 29, 'Uang Jaminan Lembaga Kliring dan Penjaminan', 0, 1),
(23, 1, 30, 'Piutang Transaksi Bursa', 0, 1),
(24, 1, 31, 'Piutang Komisi', 0, 1),
(25, 1, 32, 'Piutang Nasabah', 0, 0),
(26, 1, 33, 'Piutang Nasabah Pemilik Rekening Efek', 0, 1),
(27, 1, 34, 'Transaksi Beli Efek', 0, 2),
(28, 1, 35, 'Saldo Debit Rekening Efek Nasabah', 0, 2),
(29, 1, 36, 'Piutang Nasabah Umum', 0, 1),
(30, 1, 37, 'Piutang Nasabah Kelembagaan', 0, 1),
(31, 1, 38, 'Transaksi Beli Efek', 0, 2),
(32, 1, 39, 'Gagal Serah - Nasabah Kelembagaan', 0, 2),
(33, 1, 40, 'Piutang Perusahaan Efek Lain', 0, 0),
(34, 1, 41, 'Uang Jaminan untuk Peminjaman Efek ', 0, 1),
(35, 1, 42, 'Uang Jaminan pada Anggota Kliring', 0, 1),
(36, 1, 43, 'Transaksi Jual Efek', 0, 1),
(37, 1, 44, 'Gagal Serah - Perusahaan Efek', 0, 1),
(38, 1, 45, 'Piutang Komisi ', 0, 1),
(39, 1, 46, 'Dana Pesanan Efek Dibayar Dimuka', 0, 1),
(40, 1, 47, 'Piutang Kegiatan Penjaminan Emisi Efek', 0, 0),
(41, 1, 48, 'Piutang Jasa Emisi Efek', 0, 1),
(42, 1, 49, 'Piutang Jasa Arranger Penerbitan Efek', 0, 1),
(43, 1, 50, 'Piutang Jasa Penasihat Keuangan ', 0, 1),
(44, 1, 51, 'Piutang Biaya Talangan - Penjamin Emisi Efek', 0, 1),
(45, 1, 52, 'Piutang Kegiatan Manajer Investasi', 0, 0),
(46, 1, 53, 'Piutang Manajemen Fee ', 0, 1),
(47, 1, 54, 'Piutang Selling Fee dan Redemption Fee', 0, 1),
(48, 1, 55, 'Piutang Biaya Talangan – Manajer Investasi', 0, 1),
(49, 1, 56, 'Piutang Transaksi Jual Efek Lainnya', 0, 0),
(50, 1, 57, 'Piutang Dividen dan Bunga', 0, 0),
(51, 1, 58, 'Portofolio Efek', 0, 0),
(52, 1, 59, 'Sertifikat Bank Indonesia', 0, 1),
(53, 1, 60, 'Surat Berharga Negara', 0, 1),
(54, 1, 61, '0-7 tahun', 0, 2),
(55, 1, 62, '7-15 tahun', 0, 2),
(56, 1, 63, '15 tahun ke atas', 0, 2),
(57, 1, 64, 'Obligasi Korporasi, Sukuk Korporasi, atau Efek Beragun Aset Arus Kas Tetap yang tercatat di Bursa Ef', 0, 1),
(58, 1, 65, 'Peringkat setara dengan AAA ', 0, 2),
(59, 1, 66, 'Peringkat setara dengan AA hingga kurang dari setara dengan AAA ', 0, 2),
(60, 1, 67, 'Peringkat setara dengan A atau hingga kurang dari setara dengan AA ', 0, 2),
(61, 1, 68, 'Peringkat setara dengan BBB- atau hingga kurang dari setara dengan A ', 0, 2),
(62, 1, 69, 'Peringkat kurang dari setara dengan BBB-', 0, 2),
(63, 1, 70, 'Efek Bersifat Ekuitas atau Efek Beragun Aset Arus Kas Tidak Tetap yang tercatat di Bursa Efek di Ind', 0, 1),
(64, 1, 71, 'Haircut Komite 5% dan 10%', 0, 2),
(65, 1, 72, 'Haircut Komite 15% dan 20%', 0, 2),
(66, 1, 73, 'Haircut Komite 25%', 0, 2),
(67, 1, 74, 'Haircut Komite 30% ', 0, 2),
(68, 1, 75, 'Haircut Komite 35% ', 0, 2),
(69, 1, 76, 'Haircut Komite 40% ', 0, 2),
(70, 1, 77, 'Haircut Komite 45% ', 0, 2),
(71, 1, 78, 'Haircut Komite 50% ', 0, 2),
(72, 1, 79, 'Haircut Komite 55% sd 80%', 0, 2),
(73, 1, 80, 'Haircut Komite 85% sd100%', 0, 2),
(74, 1, 81, 'Efek Bersifat Ekuitas yang tidak lagi tercatat  pada Bursa Efek di Indonesia (delist)', 0, 1),
(75, 1, 82, 'Efek Luar Negeri', 0, 1),
(76, 1, 83, 'Unit Penyertaan Reksa Dana', 0, 1),
(77, 1, 84, 'Pasar uang', 0, 2),
(78, 1, 85, 'Terproteksi', 0, 2),
(79, 1, 86, 'Dengan Penjaminan', 0, 2),
(80, 1, 87, 'Pendapatan tetap', 0, 2),
(81, 1, 88, 'Campuran atau Saham', 0, 2),
(82, 1, 89, 'Indeks', 0, 2),
(83, 1, 90, 'Penyertaan Terbatas', 0, 2),
(84, 1, 91, 'Investasi yang Dikelola oleh Perusahaan Efek Lain', 0, 1),
(85, 1, 92, 'Unit Penyertaan Dana Investasi Real Estat', 0, 1),
(86, 1, 93, 'Kontrak Opsi', 0, 1),
(87, 1, 94, 'Kontrak Berjangka', 0, 1),
(88, 1, 95, 'Efek Lain yang Terdaftar di Bapepam dan LK', 0, 1),
(89, 1, 96, 'Efek Repo, Dijaminkan, atau Dipinjamkan', 0, 1),
(90, 1, 97, 'Surat Berharga Negara', 0, 2),
(91, 1, 98, 'Obligasi Korporasi ', 0, 2),
(92, 1, 99, 'Efek Bersifat Ekuitas', 0, 2),
(93, 1, 100, 'Total Aset Lancar :', 0, 1),
(94, 1, 101, 'Aset Keuangan Lainnya', 0, 0),
(95, 1, 102, 'Piutang Kepada Pihak Istimewa lainnya', 0, 1),
(96, 1, 103, 'Piutang Nasabah Pemilik Rekening Efek untuk transaksi beli Efek sejak tanggal penyelesaian transaksi', 0, 1),
(97, 1, 104, 'Piutang lainnya', 0, 1),
(98, 1, 105, 'Pajak dibayar di muka', 0, 1),
(99, 1, 106, 'Biaya dibayar di muka', 0, 1),
(100, 1, 107, 'Jaminan lainnya', 0, 1),
(101, 1, 108, 'Investasi Jangka Panjang ', 0, 0),
(102, 1, 109, 'Aset Tetap', 0, 0),
(103, 1, 110, 'Aset Pajak Tangguhan', 0, 0),
(104, 1, 111, 'Aset Lain - lain', 0, 0),
(105, 1, 112, 'Total Aset Tetap dan Aset Lainnya :', 0, 1),
(106, 1, 113, 'TOTAL ASET', 0, 1),
(107, 2, 121, 'LIABILITAS', 0, 0),
(108, 2, 122, 'Utang Jangka Pendek', 0, 0),
(109, 2, 123, 'Surat Utang Jangka Pendek', 0, 0),
(110, 2, 124, 'Utang Repo', 0, 0),
(111, 2, 125, 'Repo Surat Berharga Negara', 0, 1),
(112, 2, 126, 'Repo Obligasi Korporasi', 0, 1),
(113, 2, 127, 'Repo Efek Bersifat Ekuitas', 0, 1),
(114, 2, 128, 'Utang Lembaga Kliring Penjaminan', 0, 0),
(115, 2, 129, 'Utang Transaksi Bursa', 0, 1),
(116, 2, 130, 'Utang Komisi', 0, 1),
(117, 2, 131, 'Utang Nasabah ', 0, 0),
(118, 2, 132, 'Utang Nasabah Pemilik Rekening Efek', 0, 1),
(119, 2, 133, 'Transaksi Jual Efek', 0, 2),
(120, 2, 134, 'Saldo Kredit', 0, 2),
(121, 2, 135, 'Utang Nasabah Kelembagaan', 0, 1),
(122, 2, 136, 'Transaksi Jual Efek', 0, 2),
(123, 2, 137, 'Gagal Terima - Nasabah Kelembagaan', 0, 2),
(124, 2, 138, 'Utang Perusahaan Efek Lain', 0, 0),
(125, 2, 139, 'Uang Jaminan untuk Peminjaman Efek', 0, 1),
(126, 2, 140, 'Uang Jaminan dari PE non Anggota Kliring', 0, 1),
(127, 2, 141, 'Transaksi Beli Efek', 0, 1),
(128, 2, 142, 'Gagal Terima - Perusahaan Efek', 0, 1),
(129, 2, 143, 'Utang Komisi', 0, 1),
(130, 2, 144, 'Utang Kegiatan Penjaminan Emisi Efek', 0, 0),
(131, 2, 145, 'Utang Dalam Rangka Penawaran Umum/Penawaran Terbatas', 0, 1),
(132, 2, 146, 'Utang Nasabah Umum', 0, 2),
(133, 2, 147, 'Utang Emiten', 0, 2),
(134, 2, 148, 'Utang Kepada Penerbit Efek', 0, 2),
(135, 2, 149, 'Utang Jasa Emisi Efek', 0, 1),
(136, 2, 150, 'Utang Kegiatan Manajer Investasi', 0, 0),
(137, 2, 151, 'Utang Komisi Agen Penjual', 0, 1),
(138, 2, 152, 'Utang Transaksi Beli Efek Lainnya', 0, 0),
(139, 2, 153, 'Utang Efek Posisi Short – Sendiri ', 0, 0),
(140, 2, 154, 'Surat Berharga Negara', 0, 1),
(141, 2, 155, 'Efek Bersifat Utang yang tercatat di Bursa Efek di Indonesia', 0, 1),
(142, 2, 156, 'Efek Bersifat Ekuitas yang tercatat di Bursa Efek di Indonesia, atau Reksa Dana yang Unit Penyertaan', 0, 1),
(143, 2, 157, 'Efek Lain yang Terdaftar di Bapepam dan LK', 0, 1),
(144, 2, 158, 'Efek Luar Negeri', 0, 1),
(145, 2, 159, 'Utang Jangka Pendek Lainnya', 0, 0),
(146, 2, 160, 'Utang Jangka Panjang', 0, 0),
(147, 2, 161, 'Utang Obligasi', 0, 0),
(148, 2, 162, 'Utang Lain-lain', 0, 0),
(149, 2, 163, 'Utang Sub-Ordinasi', 0, 0),
(150, 2, 164, 'TOTAL LIABILITAS', 0, 0),
(151, 2, 165, 'EKUITAS', 0, 0),
(152, 2, 166, 'Ekuitas Yang Dapat Diatribusikan Kepada Pemilik Entitas Induk', 0, 0),
(153, 2, 167, 'Modal Saham', 0, 1),
(154, 2, 168, 'Tambahan Modal Disetor', 0, 1),
(155, 2, 169, 'Ekuitas Lainnya', 0, 1),
(156, 2, 170, 'Saldo Laba', 0, 1),
(157, 2, 171, 'Kepentingan Non Pengendali', 0, 0),
(158, 2, 172, 'TOTAL EKUITAS', 0, 0),
(159, 2, 173, 'TOTAL LIABILITAS DAN EKUITAS', 0, 0),
(160, 6, 8, 'Dana Milik Perusahaan Efek ', 0, 0),
(161, 6, 9, 'Dana Milik Nasabah Pemilik Rekening', 0, 0),
(162, 6, 10, 'Dana Bebas', 0, 1),
(163, 6, 11, 'Dana yang Dijaminkan', 0, 1),
(164, 6, 12, 'Dana Milik Nasabah Umum', 0, 0),
(165, 6, 13, 'Dana Pemesanan Efek', 0, 1),
(166, 6, 14, 'Selisih Dana Positif ', 0, 0),
(167, 6, 15, 'Total Debit ', 0, 1),
(168, 6, 17, 'Dana yang disimpan di Unit Kerja yang Menjalankan Fungsi Pembukuan', 0, 0),
(169, 6, 18, 'Dana yang disimpan pada Bank', 0, 0),
(170, 6, 19, 'Dana Milik Perusahaan Efek ', 0, 1),
(171, 6, 20, 'Dana Milik Nasabah Pemilik Rekening', 0, 1),
(172, 6, 21, 'Dana Milik Nasabah Umum', 0, 1),
(173, 6, 22, 'Selisih Dana Negatif', 0, 0),
(174, 6, 23, 'Total Kredit ', 0, 1),
(175, 8, 8, 'Total Liabilitas', 0, 0),
(176, 8, 9, 'Total Ranking Liabilities', 0, 0),
(177, 8, 10, 'Total Liabilitas dan Ranking Liabilities \n(Baris 8 + Baris 9)', 0, 0),
(178, 8, 11, 'Dikurangi Utang Sub-Ordinasi', 0, 0),
(179, 8, 12, 'Dikurangi Utang Dalam Rangka Penawaran Umum/Penawaran Terbatas', 0, 0),
(180, 8, 13, 'Utang Nasabah Umum', 0, 1),
(181, 8, 14, 'Utang Emiten', 0, 1),
(182, 8, 15, 'Utang Kepada Penerbit Efek', 0, 1),
(183, 8, 16, 'Total Liabilitas dan Ranking Liabilities Tanpa Utang Subordinasi dan Utang Dalam Rangka Penawaran Um', 0, 0),
(184, 8, 17, 'Nilai MKBD yang diwajibkan untuk PPE atau PEE', 0, 0),
(185, 8, 18, 'Nilai Persyaratan Minimal MKBD*', 0, 1),
(186, 8, 19, '6,25% dari baris 16', 0, 1),
(187, 8, 20, 'MKBD yang dipersyaratkan \n(nilai yang lebih tinggi antara baris 18 dan baris 19)', 0, 1),
(188, 8, 21, 'Nilai MKBD yang diwajibkan untuk MI', 0, 0),
(189, 8, 22, 'Nilai Persyaratan Minimal MKBD**', 0, 1),
(190, 8, 23, 'Nilai dana yang dikelola oleh MI', 0, 1),
(191, 8, 24, '0,1 % dari baris 23', 0, 1),
(192, 8, 25, 'Nilai MKBD yang dipersyaratkan\n(baris 22 ditambah baris 24)', 0, 1),
(193, 8, 26, 'Nilai MKBD (baris 20 ditambah baris 25)', 0, 0),
(194, 9, 8, 'MODAL KERJA', 0, 0),
(195, 9, 9, 'Total Aset Lancar', 0, 1),
(196, 9, 10, 'Kurang :', 0, 1),
(197, 9, 11, 'Total Liabilitas', 0, 1),
(198, 9, 12, 'Total Ranking Liabilities', 0, 1),
(199, 9, 13, 'Total Modal Kerja\n(Baris 9 dikurangi Baris 11 dan Baris 12)', 0, 2),
(200, 9, 14, 'MODAL KERJA BERSIH', 0, 0),
(201, 9, 15, 'Total Modal Kerja (Baris 13)', 0, 1),
(202, 9, 16, 'Tambah :', 0, 1),
(203, 9, 17, 'Utang Sub-Ordinasi', 0, 1),
(204, 9, 18, 'Total Modal Kerja Bersih\n(Baris 15 ditambah Baris 17)', 0, 2),
(205, 9, 19, 'MODAL KERJA BERSIH DISESUAIKAN', 0, 0),
(206, 9, 20, 'Total  Modal Kerja Bersih (Baris 18)', 0, 1),
(207, 9, 21, 'Kurang:', 0, 1),
(208, 9, 22, 'Penyesuaian Risiko Likuiditas', 0, 1),
(209, 9, 23, 'Deposito Bank Dalam Negeri', 0, 2),
(210, 9, 24, 'Deposito Bank Umum dengan jangka waktu kurang  atau sama dengan 3 (tiga) bulan', 0, 3),
(211, 9, 25, 'Deposito Bank Umum dengan jangka waktu lebih dari 3 (tiga) bulan', 0, 3),
(212, 9, 26, 'Dijamin oleh Lembaga Penjamin Simpanan', 0, 4),
(213, 9, 27, 'Tidak Dijamin oleh Lembaga Penjamin Simpanan', 0, 4),
(214, 9, 28, '', 0, 4),
(215, 9, 29, '', 0, 4),
(216, 9, 30, 'Deposito pada Bank Perkreditan Rakyat', 0, 3),
(217, 9, 31, 'Deposito Bank di Luar Negeri', 0, 2),
(218, 9, 32, 'Penyesuaian Risiko Pasar', 0, 1),
(219, 9, 33, 'Sertifikat Bank Indonesia', 0, 2),
(220, 9, 34, 'Surat Berharga Negara', 0, 2),
(221, 9, 35, '0-7 tahun', 0, 3),
(222, 9, 36, '7-15 tahun', 0, 3),
(223, 9, 37, '15 tahun ke atas', 0, 3),
(224, 9, 38, 'Obligasi Korporasi, Sukuk Korporasi, atau Efek Beragun Aset Arus Kas Tetap yang tercatat di Bursa Ef', 0, 2),
(225, 9, 39, 'Peringkat setara dengan AAA', 0, 3),
(226, 9, 40, 'Peringkat setara dengan AA hingga kurang dari setara dengan AAA ', 0, 3),
(227, 9, 41, 'Peringkat setara dengan A atau hingga kurang dari setara dengan AA ', 0, 3),
(228, 9, 42, 'Peringkat setara dengan BBB- atau hingga kurang dari setara dengan A ', 0, 3),
(229, 9, 43, 'Peringkat kurang dari setara dengan BBB-', 0, 3),
(230, 9, 44, 'Efek Bersifat Ekuitas yang tercatat di Bursa Efek di Indonesia, Reksa Dana yang Unit Penyertaannya d', 0, 2),
(231, 9, 45, 'Haircut Komite 5% dan 10%', 0, 3),
(232, 9, 46, 'Haircut Komite 15% dan 20%', 0, 3),
(233, 9, 47, 'Haircut Komite 25%', 0, 3),
(234, 9, 48, 'Haircut Komite 30% ', 0, 3),
(235, 9, 49, 'Haircut Komite 35% ', 0, 3),
(236, 9, 50, 'Haircut Komite 40% ', 0, 3),
(237, 9, 51, 'Haircut Komite 45% ', 0, 3),
(238, 9, 52, 'Haircut Komite 50% ', 0, 3),
(239, 9, 53, 'Haircut Komite 55% sd 80%', 0, 3),
(240, 9, 54, 'Haircut Komite 85% sd100%', 0, 3),
(241, 9, 55, 'Efek Bersifat Ekuitas yang tidak lagi tercatat  pada Bursa Efek di Indonesia (delist)', 0, 2),
(242, 9, 56, 'Efek Luar Negeri', 0, 2),
(243, 9, 57, 'Unit Penyertaan Reksa Dana', 0, 2),
(244, 9, 58, 'Pasar uang', 0, 3),
(245, 9, 59, 'Terproteksi', 0, 3),
(246, 9, 60, 'Dengan Penjaminan', 0, 3),
(247, 9, 61, 'Pendapatan tetap', 0, 3),
(248, 9, 62, 'Campuran atau Saham', 0, 3),
(249, 9, 63, 'Indeks', 0, 3),
(250, 9, 64, 'Penyertaan Terbatas', 0, 3),
(251, 9, 65, 'Investasi yang Dikelola oleh Perusahaan Efek Lain', 0, 2),
(252, 9, 66, 'Unit Penyertaan Dana Investasi Real Estat', 0, 2),
(253, 9, 67, 'Kontrak Opsi', 0, 2),
(254, 9, 68, 'Kontrak Berjangka', 0, 2),
(255, 9, 69, 'Efek Lain yang Terdaftar di Bapepam dan LK', 0, 2),
(256, 9, 70, 'Efek Repo', 0, 2),
(257, 9, 71, 'Surat Berharga Negara', 0, 3),
(258, 9, 72, '0-7 tahun', 0, 4),
(259, 9, 73, '7-15 tahun', 0, 4),
(260, 9, 74, '15 tahun ke atas', 0, 4),
(261, 9, 75, 'Obligasi Korporasi', 0, 3),
(262, 9, 76, 'Peringkat setara dengan AAA', 0, 4),
(263, 9, 77, 'Peringkat setara dengan AA hingga kurang dari setara dengan AAA ', 0, 4),
(264, 9, 78, 'Peringkat setara dengan A atau hingga kurang dari setara dengan AA ', 0, 4),
(265, 9, 79, 'Peringkat setara dengan BBB- atau hingga kurang dari setara dengan A ', 0, 4),
(266, 9, 80, 'Peringkat kurang dari setara dengan BBB-', 0, 4),
(267, 9, 81, 'Efek Bersifat Ekuitas', 0, 3),
(268, 9, 82, 'Haircut Komite 5% dan 10%', 0, 4),
(269, 9, 83, 'Haircut Komite 15% dan 20%', 0, 4),
(270, 9, 84, 'Haircut Komite 25%', 0, 4),
(271, 9, 85, 'Haircut Komite 30% ', 0, 4),
(272, 9, 86, 'Haircut Komite 35% ', 0, 4),
(273, 9, 87, 'Haircut Komite 40% ', 0, 4),
(274, 9, 88, 'Haircut Komite 45% ', 0, 4),
(275, 9, 89, 'Haircut Komite 50% ', 0, 4),
(276, 9, 90, 'Haircut Komite 55% sd 80%', 0, 4),
(277, 9, 91, 'Haircut Komite 85% sd100%', 0, 4),
(278, 9, 92, 'Penyesuaian Risiko Kredit', 0, 1),
(279, 9, 93, 'Gagal Serah - Nasabah Kelembagaan', 0, 2),
(280, 9, 94, 'Gagal Serah - Perusahaan Efek', 0, 2),
(281, 9, 95, 'Penyesuaian Risiko Kegiatan Usaha', 0, 1),
(282, 9, 96, 'Kelebihan V.D.5-6 baris 10 dibanding V.D.5.6 baris 20 kolom D', 0, 2),
(283, 9, 97, 'Kelebihan V.D.5-2 baris 146 dibanding V.D.5.1 baris 12', 0, 2),
(284, 9, 98, 'Kelebihan V.D.5-7 baris 11 kolom B dibanding V.D.5.7 baris 33 kolom D', 0, 2),
(285, 9, 99, 'Kelebihan V.D.5-7 baris 62 kolom E dibanding V.D.5.1 baris 11 kolom B', 0, 2),
(286, 9, 100, 'Tambah : ', 0, 1),
(287, 9, 101, 'Pengembalian Haircut atas Efek yang Ditutup dengan Lindung Nilai', 0, 1),
(288, 9, 102, 'Total Modal Kerja Bersih Disesuaikan ', 0, 2),
(289, 9, 103, 'NILAI MKBD YANG DIWAJIBKAN', 0, 0),
(290, 9, 104, 'LEBIH (KURANG) MKBD', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_mkbd_coa`
--

CREATE TABLE `0_mkbd_coa` (
  `id` int(11) NOT NULL,
  `mkbd_id` int(11) NOT NULL,
  `account` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_mkbd_coa`
--

INSERT INTO `0_mkbd_coa` (`id`, `mkbd_id`, `account`) VALUES
(3, 168, 11111),
(4, 168, 11112),
(5, 168, 11113),
(6, 168, 11114),
(7, 168, 11140),
(8, 170, 11151),
(9, 170, 11152),
(10, 170, 11153),
(11, 170, 11154),
(12, 170, 11155),
(13, 170, 11156),
(14, 170, 11157),
(15, 170, 11158),
(16, 170, 11159),
(17, 170, 11160),
(18, 170, 11161),
(19, 170, 11162),
(20, 170, 11163),
(21, 170, 11164),
(22, 174, 11111),
(23, 174, 11112),
(24, 174, 11113),
(25, 174, 11114),
(26, 174, 11140),
(27, 174, 11151),
(28, 174, 11152),
(29, 174, 11153),
(30, 174, 11154),
(31, 174, 11155),
(32, 174, 11156),
(33, 174, 11157),
(34, 174, 11158),
(35, 174, 11159),
(36, 174, 11160),
(37, 174, 11161),
(38, 174, 11162),
(39, 174, 11163),
(40, 174, 11164),
(41, 160, 11111),
(42, 160, 11112),
(43, 160, 11113),
(44, 160, 11114),
(45, 160, 11140),
(46, 160, 11151),
(47, 160, 11152),
(48, 160, 11153),
(49, 160, 11154),
(50, 160, 11155),
(51, 160, 11156),
(52, 160, 11157),
(53, 160, 11158),
(54, 160, 11159),
(55, 160, 11160),
(56, 160, 11161),
(57, 160, 11162),
(58, 160, 11163),
(59, 160, 11164),
(79, 9, 11201),
(80, 9, 11202),
(81, 9, 11205),
(82, 9, 11206),
(83, 9, 11207),
(84, 46, 11401),
(85, 73, 11304),
(86, 83, 11303),
(87, 97, 11580),
(88, 97, 11581),
(89, 97, 11582),
(90, 97, 11583),
(91, 97, 11584),
(92, 97, 11590),
(93, 98, 11701),
(94, 98, 11702),
(95, 98, 11703),
(96, 98, 11704),
(97, 98, 11705),
(118, 100, 12901),
(119, 101, 12102),
(130, 103, 12301),
(134, 145, 21110),
(135, 145, 21130),
(136, 145, 21140),
(137, 145, 21210),
(138, 145, 21220),
(139, 145, 21230),
(140, 145, 21240),
(141, 145, 21250),
(142, 145, 21260),
(143, 145, 21270),
(144, 145, 21280),
(145, 145, 21401),
(146, 145, 21510),
(147, 145, 21901),
(148, 145, 21902),
(149, 145, 21903),
(150, 146, 22901),
(151, 148, 21601),
(152, 148, 21701),
(153, 153, 31100),
(154, 155, 39700),
(155, 155, 39800),
(156, 156, 38000),
(157, 156, 39000),
(158, 93, 11111),
(159, 93, 11112),
(160, 93, 11113),
(161, 93, 11114),
(162, 93, 11140),
(163, 93, 11151),
(164, 93, 11152),
(165, 93, 11153),
(166, 93, 11154),
(167, 93, 11155),
(168, 93, 11156),
(169, 93, 11157),
(170, 93, 11158),
(171, 93, 11159),
(172, 93, 11160),
(173, 93, 11161),
(174, 93, 11162),
(175, 93, 11163),
(176, 93, 11164),
(177, 93, 11303),
(178, 93, 11304),
(179, 93, 11305),
(180, 93, 11401),
(184, 154, 32000),
(185, 240, 11304),
(186, 250, 11303),
(187, 64, 11304),
(188, 65, 11304),
(189, 66, 11304),
(190, 67, 11304),
(191, 68, 11304),
(192, 69, 11304),
(193, 70, 11304),
(194, 71, 11304),
(195, 72, 11304),
(196, 3, 11111),
(197, 3, 11112),
(198, 3, 11113),
(199, 3, 11114),
(200, 3, 11140),
(201, 3, 11151),
(202, 3, 11152),
(203, 3, 11153),
(204, 3, 11154),
(205, 3, 11155),
(206, 3, 11156),
(207, 3, 11157),
(208, 3, 11158),
(209, 3, 11159),
(210, 3, 11160),
(211, 3, 11161),
(212, 3, 11162),
(213, 3, 11163),
(214, 3, 11164),
(215, 3, 11165),
(216, 99, 11601),
(217, 99, 11602),
(218, 99, 11603),
(219, 99, 11604),
(220, 99, 11605),
(221, 99, 11606),
(222, 99, 11607),
(223, 99, 11608),
(224, 99, 11609),
(225, 99, 11610),
(226, 99, 11611),
(227, 99, 11612),
(228, 99, 11613),
(229, 99, 11614),
(230, 99, 11615),
(231, 99, 11616),
(232, 99, 11617),
(233, 99, 11618),
(234, 99, 11619),
(235, 99, 11699),
(236, 99, 11699001),
(237, 99, 11699002),
(238, 99, 11699003),
(239, 99, 11699004),
(240, 99, 11699005),
(241, 99, 11699006),
(242, 102, 12211),
(243, 102, 12212),
(244, 102, 12221),
(245, 102, 12222),
(246, 102, 12231),
(247, 102, 12232),
(248, 102, 12241),
(249, 102, 12242),
(250, 102, 12251),
(251, 102, 12261),
(252, 102, 12262),
(263, 104, 12902),
(264, 104, 12903),
(265, 104, 12904),
(266, 104, 12999),
(267, 104, 12999010),
(268, 104, 12999020),
(269, 104, 12999021),
(270, 104, 12999030),
(271, 104, 12999031),
(272, 104, 12999040),
(273, 104, 12999041);

-- --------------------------------------------------------

--
-- Table structure for table `0_no_faktur`
--

CREATE TABLE `0_no_faktur` (
  `id` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `no_urut` int(11) NOT NULL,
  `no_faktur` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_no_faktur`
--

INSERT INTO `0_no_faktur` (`id`, `tgl`, `no_urut`, `no_faktur`, `status`) VALUES
(1, '2017-01-01', 1, '1111111111111', 0),
(2, '2017-01-01', 2, '1111111111112', 0),
(3, '2017-01-01', 3, '1111111111113', 0),
(4, '2017-01-01', 4, '1111111111114', 0),
(5, '2017-01-01', 5, '1111111111115', 0),
(6, '2017-01-01', 6, '1111111111116', 0),
(7, '2017-05-16', 1, '018.17.62237419', 0),
(8, '2017-05-16', 2, '018.17.62237420', 0),
(9, '2017-05-16', 3, '018.17.62237421', 0),
(10, '2017-05-16', 4, '018.17.62237422', 0),
(11, '2017-05-16', 5, '018.17.62237423', 0),
(12, '2017-05-16', 6, '018.17.62237424', 0),
(13, '2017-05-16', 7, '018.17.62237425', 0),
(14, '2017-05-16', 8, '018.17.62237426', 0),
(15, '2017-05-16', 9, '018.17.62237427', 0),
(16, '2017-05-16', 10, '018.17.62237428', 0),
(17, '2017-05-16', 11, '018.17.62237429', 0),
(18, '2017-05-16', 12, '018.17.62237430', 0),
(19, '2017-05-16', 13, '018.17.62237431', 0),
(20, '2017-05-16', 14, '018.17.62237432', 0),
(21, '2017-05-16', 15, '018.17.62237433', 0),
(22, '2017-05-16', 16, '018.17.62237434', 0),
(23, '2017-05-16', 17, '018.17.62237435', 0),
(24, '2017-05-16', 18, '018.17.62237436', 0),
(25, '2017-05-16', 19, '018.17.62237437', 0),
(26, '2017-05-16', 20, '018.17.62237438', 0),
(27, '2017-05-16', 21, '018.17.62237439', 0),
(28, '2017-05-16', 22, '018.17.62237440', 0),
(29, '2017-05-16', 23, '018.17.62237441', 0),
(30, '2017-05-16', 24, '018.17.62237442', 0),
(31, '2017-05-16', 25, '018.17.62237443', 0),
(32, '2017-05-16', 26, '018.17.62237444', 0),
(33, '2017-05-16', 27, '018.17.62237445', 0),
(34, '2017-05-16', 28, '018.17.62237446', 0),
(35, '2017-05-16', 29, '018.17.62237447', 0),
(36, '2017-05-16', 30, '018.17.62237448', 0),
(37, '2017-05-16', 31, '018.17.62237449', 0),
(38, '2017-05-16', 32, '018.17.62237450', 0),
(39, '2017-05-16', 33, '018.17.62237451', 0),
(40, '2017-05-16', 34, '018.17.62237452', 0),
(41, '2017-05-16', 35, '018.17.62237453', 0),
(42, '2017-05-16', 36, '018.17.62237454', 0),
(43, '2017-05-16', 37, '018.17.62237455', 0),
(44, '2017-05-16', 38, '018.17.62237456', 0),
(45, '2017-05-16', 39, '018.17.62237457', 0),
(46, '2017-05-16', 40, '018.17.62237458', 0),
(47, '2017-05-16', 41, '018.17.62237459', 0),
(48, '2017-05-16', 42, '018.17.62237460', 0),
(49, '2017-05-16', 43, '018.17.62237461', 0),
(50, '2017-05-16', 44, '018.17.62237462', 0),
(51, '2017-05-16', 45, '018.17.62237463', 0),
(52, '2017-05-16', 46, '018.17.62237464', 0),
(53, '2017-05-16', 47, '018.17.62237465', 0),
(54, '2017-05-16', 48, '018.17.62237466', 0),
(55, '2017-05-16', 49, '018.17.62237467', 0),
(56, '2017-05-16', 50, '018.17.62237468', 0),
(57, '2017-05-16', 51, '018.17.62237469', 0),
(58, '2017-05-16', 52, '018.17.62237470', 0),
(59, '2017-05-16', 53, '018.17.62237471', 0),
(60, '2017-05-16', 54, '018.17.62237472', 0),
(61, '2017-05-16', 55, '018.17.62237473', 0),
(62, '2017-05-16', 56, '018.17.62237474', 0),
(63, '2017-05-16', 57, '018.17.62237475', 0),
(64, '2017-05-16', 58, '018.17.62237476', 0),
(65, '2017-05-16', 59, '018.17.62237477', 0),
(66, '2017-05-16', 60, '018.17.62237478', 0),
(67, '2017-05-16', 61, '018.17.62237479', 0),
(68, '2017-05-16', 62, '018.17.62237480', 0),
(69, '2017-05-16', 63, '018.17.62237481', 0),
(70, '2017-05-16', 64, '018.17.62237482', 0),
(71, '2017-05-16', 65, '018.17.62237483', 0),
(72, '2017-05-16', 66, '018.17.62237484', 0),
(73, '2017-05-16', 67, '018.17.62237485', 0),
(74, '2017-05-16', 68, '018.17.62237486', 0),
(75, '2017-05-16', 69, '018.17.62237487', 0),
(76, '2017-05-16', 70, '018.17.62237488', 0),
(77, '2017-05-16', 71, '018.17.62237489', 0),
(78, '2017-05-16', 72, '018.17.62237490', 0),
(79, '2017-05-16', 73, '018.17.62237491', 0),
(80, '2017-05-16', 74, '018.17.62237492', 0),
(81, '2017-05-16', 75, '018.17.62237493', 0),
(82, '2017-05-16', 76, '018.17.62237494', 0),
(83, '2017-05-16', 77, '018.17.62237495', 0),
(84, '2017-05-16', 78, '018.17.62237496', 0),
(85, '2017-05-16', 79, '018.17.62237497', 0),
(86, '2017-05-16', 80, '018.17.62237498', 0),
(87, '2017-05-16', 81, '018.17.62237499', 0),
(88, '2017-05-16', 82, '018.17.62237500', 0),
(89, '2017-05-16', 83, '018.17.62237501', 0),
(90, '2017-05-16', 84, '018.17.62237502', 0),
(91, '2017-05-16', 85, '018.17.62237503', 0),
(92, '2017-05-16', 86, '018.17.62237504', 0),
(93, '2017-05-16', 87, '018.17.62237505', 0),
(94, '2017-05-16', 88, '018.17.62237506', 0),
(95, '2017-05-16', 89, '018.17.62237507', 0),
(96, '2017-05-16', 90, '018.17.62237508', 0),
(97, '2017-05-16', 91, '018.17.62237509', 0),
(98, '2017-05-16', 92, '018.17.62237510', 0),
(99, '2017-05-16', 93, '018.17.62237511', 0),
(100, '2017-05-16', 94, '018.17.62237512', 0),
(101, '2017-05-16', 95, '018.17.62237513', 0),
(102, '2017-05-16', 96, '018.17.62237514', 0),
(103, '2017-05-16', 97, '018.17.62237515', 0),
(104, '2017-05-16', 98, '018.17.62237516', 0),
(105, '2017-05-16', 99, '018.17.62237517', 0),
(106, '2017-05-16', 100, '018.17.62237518', 0),
(107, '2017-05-16', 101, '018.17.62237519', 0),
(108, '2017-05-16', 102, '018.17.62237520', 0),
(109, '2017-05-16', 103, '018.17.62237521', 0),
(110, '2017-05-16', 104, '018.17.62237522', 0),
(111, '2017-05-16', 105, '018.17.62237523', 0),
(112, '2017-05-16', 106, '018.17.62237524', 0),
(113, '2017-05-16', 107, '018.17.62237525', 0),
(114, '2017-05-16', 108, '018.17.62237526', 0),
(115, '2017-05-16', 109, '018.17.62237527', 0),
(116, '2017-05-16', 110, '018.17.62237528', 0),
(117, '2017-05-16', 111, '018.17.62237529', 0),
(118, '2017-05-16', 112, '018.17.62237530', 0),
(119, '2017-05-16', 113, '018.17.62237531', 0),
(120, '2017-05-16', 114, '018.17.62237532', 0),
(121, '2017-05-16', 115, '018.17.62237533', 0),
(122, '2017-05-16', 116, '018.17.62237534', 0),
(123, '2017-05-16', 117, '018.17.62237535', 0),
(124, '2017-05-16', 118, '018.17.62237536', 0),
(125, '2017-05-16', 119, '018.17.62237537', 0),
(126, '2017-05-16', 120, '018.17.62237538', 0),
(127, '2017-05-16', 121, '018.17.62237539', 0),
(128, '2017-05-16', 122, '018.17.62237540', 0),
(129, '2017-05-16', 123, '018.17.62237541', 0),
(130, '2017-05-16', 124, '018.17.62237542', 0),
(131, '2017-05-16', 125, '018.17.62237543', 0),
(132, '2017-05-16', 126, '018.17.62237544', 0),
(133, '2017-05-16', 127, '018.17.62237545', 0),
(134, '2017-05-16', 128, '018.17.62237546', 0),
(135, '2017-05-16', 129, '018.17.62237547', 0),
(136, '2017-05-16', 130, '018.17.62237548', 0),
(137, '2017-05-16', 131, '018.17.62237549', 0),
(138, '2017-05-16', 132, '018.17.62237550', 0),
(139, '2017-05-16', 133, '018.17.62237551', 0),
(140, '2017-05-16', 134, '018.17.62237552', 0),
(141, '2017-05-16', 135, '018.17.62237553', 0),
(142, '2017-05-16', 136, '018.17.62237554', 0),
(143, '2017-05-16', 137, '018.17.62237555', 0),
(144, '2017-05-16', 138, '018.17.62237556', 0),
(145, '2017-05-16', 139, '018.17.62237557', 0),
(146, '2017-05-16', 140, '018.17.62237558', 0),
(147, '2017-05-16', 141, '018.17.62237559', 0),
(148, '2017-05-16', 142, '018.17.62237560', 0),
(149, '2017-05-16', 143, '018.17.62237561', 0),
(150, '2017-05-16', 144, '018.17.62237562', 0),
(151, '2017-05-16', 145, '018.17.62237563', 0),
(152, '2017-05-16', 146, '018.17.62237564', 0),
(153, '2017-05-16', 147, '018.17.62237565', 0),
(154, '2017-05-16', 148, '018.17.62237566', 0),
(155, '2017-05-16', 149, '018.17.62237567', 0),
(156, '2017-05-16', 150, '018.17.62237568', 0),
(157, '2017-05-16', 151, '018.17.62237569', 0),
(158, '2017-05-16', 152, '018.17.62237570', 0),
(159, '2017-05-16', 153, '018.17.62237571', 0),
(160, '2017-05-16', 154, '018.17.62237572', 0),
(161, '2017-05-16', 155, '018.17.62237573', 0),
(162, '2017-05-16', 156, '018.17.62237574', 0),
(163, '2017-05-16', 157, '018.17.62237575', 0),
(164, '2017-05-16', 158, '018.17.62237576', 0),
(165, '2017-05-16', 159, '018.17.62237577', 0),
(166, '2017-05-16', 160, '018.17.62237578', 0),
(167, '2017-05-16', 161, '018.17.62237579', 0),
(168, '2017-05-16', 162, '018.17.62237580', 0),
(169, '2017-05-16', 163, '018.17.62237581', 0),
(170, '2017-05-16', 164, '018.17.62237582', 0),
(171, '2017-05-16', 165, '018.17.62237583', 0),
(172, '2017-05-16', 166, '018.17.62237584', 0),
(173, '2017-05-16', 167, '018.17.62237585', 0),
(174, '2017-05-16', 168, '018.17.62237586', 0),
(175, '2017-05-16', 169, '018.17.62237587', 0),
(176, '2017-05-16', 170, '018.17.62237588', 0),
(177, '2017-05-16', 171, '018.17.62237589', 0),
(178, '2017-05-16', 172, '018.17.62237590', 0),
(179, '2017-05-16', 173, '018.17.62237591', 0),
(180, '2017-05-16', 174, '018.17.62237592', 0),
(181, '2017-05-16', 175, '018.17.62237593', 0),
(182, '2017-05-16', 176, '018.17.62237594', 0),
(183, '2017-05-16', 177, '018.17.62237595', 0),
(184, '2017-05-16', 178, '018.17.62237596', 0),
(185, '2017-05-16', 179, '018.17.62237597', 0),
(186, '2017-05-16', 180, '018.17.62237598', 0),
(187, '2017-05-16', 181, '018.17.62237599', 0),
(188, '2017-05-16', 182, '018.17.62237600', 0),
(189, '2017-05-16', 183, '018.17.62237601', 0),
(190, '2017-05-16', 184, '018.17.62237602', 0),
(191, '2017-05-16', 185, '018.17.62237603', 0),
(192, '2017-05-16', 186, '018.17.62237604', 0),
(193, '2017-05-16', 187, '018.17.62237605', 0),
(194, '2017-05-16', 188, '018.17.62237606', 0),
(195, '2017-05-16', 189, '018.17.62237607', 0),
(196, '2017-05-16', 190, '018.17.62237608', 0),
(197, '2017-05-16', 191, '018.17.62237609', 0),
(198, '2017-05-16', 192, '018.17.62237610', 0),
(199, '2017-05-16', 193, '018.17.62237611', 0),
(200, '2017-05-16', 194, '018.17.62237612', 0),
(201, '2017-05-16', 195, '018.17.62237613', 0),
(202, '2017-05-16', 196, '018.17.62237614', 0),
(203, '2017-05-16', 197, '018.17.62237615', 0),
(204, '2017-05-16', 198, '018.17.62237616', 0),
(205, '2017-05-16', 199, '018.17.62237617', 0),
(206, '2017-05-16', 200, '018.17.62237618', 0),
(207, '2017-05-16', 201, '018.17.62237619', 0),
(208, '2017-05-16', 202, '018.17.62237620', 0),
(209, '2017-05-16', 203, '018.17.62237621', 0),
(210, '2017-05-16', 204, '018.17.62237622', 0),
(211, '2017-05-16', 205, '018.17.62237623', 0),
(212, '2017-05-16', 206, '018.17.62237624', 0),
(213, '2017-05-16', 207, '018.17.62237625', 0),
(214, '2017-05-16', 208, '018.17.62237626', 0),
(215, '2017-05-16', 209, '018.17.62237627', 0),
(216, '2017-05-16', 210, '018.17.62237628', 0),
(217, '2017-05-16', 211, '018.17.62237629', 0),
(218, '2017-05-16', 212, '018.17.62237630', 0),
(219, '2017-05-16', 213, '018.17.62237631', 0),
(220, '2017-05-16', 214, '018.17.62237632', 0),
(221, '2017-05-16', 215, '018.17.62237633', 0),
(222, '2017-05-16', 216, '018.17.62237634', 0),
(223, '2017-05-16', 217, '018.17.62237635', 0),
(224, '2017-05-16', 218, '018.17.62237636', 0),
(225, '2017-05-16', 219, '018.17.62237637', 0),
(226, '2017-05-16', 220, '018.17.62237638', 0),
(227, '2017-05-16', 221, '018.17.62237639', 0),
(228, '2017-05-16', 222, '018.17.62237640', 0),
(229, '2017-05-16', 223, '018.17.62237641', 0),
(230, '2017-05-16', 224, '018.17.62237642', 0),
(231, '2017-05-16', 225, '018.17.62237643', 0),
(232, '2017-05-16', 226, '018.17.62237644', 0),
(233, '2017-05-16', 227, '018.17.62237645', 0),
(234, '2017-05-16', 228, '018.17.62237646', 0),
(235, '2017-05-16', 229, '018.17.62237647', 0),
(236, '2017-05-16', 230, '018.17.62237648', 0),
(237, '2017-05-16', 231, '018.17.62237649', 0),
(238, '2017-05-16', 232, '018.17.62237650', 0),
(239, '2017-05-16', 233, '018.17.62237651', 0),
(240, '2017-05-16', 234, '018.17.62237652', 0),
(241, '2017-05-16', 235, '018.17.62237653', 0),
(242, '2017-05-16', 236, '018.17.62237654', 0),
(243, '2017-05-16', 237, '018.17.62237655', 0),
(244, '2017-05-16', 238, '018.17.62237656', 0),
(245, '2017-05-16', 239, '018.17.62237657', 0),
(246, '2017-05-16', 240, '018.17.62237658', 0),
(247, '2017-05-16', 241, '018.17.62237659', 0),
(248, '2017-05-16', 242, '018.17.62237660', 0),
(249, '2017-05-16', 243, '018.17.62237661', 0),
(250, '2017-05-16', 244, '018.17.62237662', 0),
(251, '2017-05-16', 245, '018.17.62237663', 0),
(252, '2017-05-16', 246, '018.17.62237664', 0),
(253, '2017-05-16', 247, '018.17.62237665', 0),
(254, '2017-05-16', 248, '018.17.62237666', 0),
(255, '2017-05-16', 249, '018.17.62237667', 0),
(256, '2017-05-16', 250, '018.17.62237668', 0),
(257, '2017-05-16', 251, '018.17.62237669', 0),
(258, '2017-05-16', 252, '018.17.62237670', 0),
(259, '2017-05-16', 253, '018.17.62237671', 0),
(260, '2017-05-16', 254, '018.17.62237672', 0),
(261, '2017-05-16', 255, '018.17.62237673', 0),
(262, '2017-05-16', 256, '018.17.62237674', 0),
(263, '2017-05-16', 257, '018.17.62237675', 0),
(264, '2017-05-16', 258, '018.17.62237676', 0),
(265, '2017-05-16', 259, '018.17.62237677', 0),
(266, '2017-05-16', 260, '018.17.62237678', 0),
(267, '2017-05-16', 261, '018.17.62237679', 0),
(268, '2017-05-16', 262, '018.17.62237680', 0),
(269, '2017-05-16', 263, '018.17.62237681', 0),
(270, '2017-05-16', 264, '018.17.62237682', 0),
(271, '2017-05-16', 265, '018.17.62237683', 0),
(272, '2017-05-16', 266, '018.17.62237684', 0),
(273, '2017-05-16', 267, '018.17.62237685', 0),
(274, '2017-05-16', 268, '018.17.62237686', 0),
(275, '2017-05-16', 269, '018.17.62237687', 0),
(276, '2017-05-16', 270, '018.17.62237688', 0),
(277, '2017-05-16', 271, '018.17.62237689', 0),
(278, '2017-05-16', 272, '018.17.62237690', 0),
(279, '2017-05-16', 273, '018.17.62237691', 0),
(280, '2017-05-16', 274, '018.17.62237692', 0),
(281, '2017-05-16', 275, '018.17.62237693', 0),
(282, '2017-05-16', 276, '018.17.62237694', 0),
(283, '2017-05-16', 277, '018.17.62237695', 0),
(284, '2017-05-16', 278, '018.17.62237696', 0),
(285, '2017-05-16', 279, '018.17.62237697', 0),
(286, '2017-05-16', 280, '018.17.62237698', 0),
(287, '2017-05-16', 281, '018.17.62237699', 0),
(288, '2017-05-16', 282, '018.17.62237700', 0),
(289, '2017-05-16', 283, '018.17.62237701', 0),
(290, '2017-05-16', 284, '018.17.62237702', 0),
(291, '2017-05-16', 285, '018.17.62237703', 0),
(292, '2017-05-16', 286, '018.17.62237704', 0),
(293, '2017-05-16', 287, '018.17.62237705', 0),
(294, '2017-05-16', 288, '018.17.62237706', 0),
(295, '2017-05-16', 289, '018.17.62237707', 0),
(296, '2017-05-16', 290, '018.17.62237708', 0),
(297, '2017-05-16', 291, '018.17.62237709', 0),
(298, '2017-05-16', 292, '018.17.62237710', 0),
(299, '2017-05-16', 293, '018.17.62237711', 0),
(300, '2017-05-16', 294, '018.17.62237712', 0),
(301, '2017-05-16', 295, '018.17.62237713', 0),
(302, '2017-05-16', 296, '018.17.62237714', 0),
(303, '2017-05-16', 297, '018.17.62237715', 0),
(304, '2017-05-16', 298, '018.17.62237716', 0),
(305, '2017-05-16', 299, '018.17.62237717', 0),
(306, '2017-05-16', 300, '018.17.62237718', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_payment_terms`
--

CREATE TABLE `0_payment_terms` (
  `terms_indicator` int(11) NOT NULL,
  `terms` char(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `days_before_due` smallint(6) NOT NULL DEFAULT '0',
  `day_in_following_month` smallint(6) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_payment_terms`
--

INSERT INTO `0_payment_terms` (`terms_indicator`, `terms`, `days_before_due`, `day_in_following_month`, `inactive`) VALUES
(1, '14 Hari Kerja', 20, 0, 0),
(2, 'Akhir bulan berikutnya', 0, 30, 0),
(3, '10 Hari Kalender', 10, 0, 0),
(4, 'Cash Only', 0, 0, 0),
(5, '7 Hari setelah invoice diterima', 7, 0, 0),
(6, 'Pay Terms I', -1, 0, 0),
(7, 'Setiap tanggal 5 di periode berikutnya', 0, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_prices`
--

CREATE TABLE `0_prices` (
  `id` int(11) NOT NULL,
  `stock_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sales_type_id` int(11) NOT NULL DEFAULT '0',
  `curr_abrev` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `price` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_printers`
--

CREATE TABLE `0_printers` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `queue` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `host` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `port` smallint(11) UNSIGNED NOT NULL,
  `timeout` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_printers`
--

INSERT INTO `0_printers` (`id`, `name`, `description`, `queue`, `host`, `port`, `timeout`) VALUES
(1, 'QL500', 'Label printer', 'QL500', 'server', 127, 20),
(2, 'Samsung', 'Main network printer', 'scx4521F', 'server', 515, 5),
(3, 'Local', 'Local print server at user IP', 'lp', '', 515, 10);

-- --------------------------------------------------------

--
-- Table structure for table `0_print_profiles`
--

CREATE TABLE `0_print_profiles` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `profile` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `report` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `printer` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_print_profiles`
--

INSERT INTO `0_print_profiles` (`id`, `profile`, `report`, `printer`) VALUES
(1, 'Out of office', NULL, 0),
(2, 'Sales Department', NULL, 0),
(3, 'Central', NULL, 2),
(4, 'Sales Department', '104', 2),
(5, 'Sales Department', '105', 2),
(6, 'Sales Department', '107', 2),
(7, 'Sales Department', '109', 2),
(8, 'Sales Department', '110', 2),
(9, 'Sales Department', '201', 2);

-- --------------------------------------------------------

--
-- Table structure for table `0_purch_data`
--

CREATE TABLE `0_purch_data` (
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `stock_id` char(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `price` double NOT NULL DEFAULT '0',
  `suppliers_uom` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `conversion_factor` double NOT NULL DEFAULT '1',
  `supplier_description` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_purch_data`
--

INSERT INTO `0_purch_data` (`supplier_id`, `stock_id`, `price`, `suppliers_uom`, `conversion_factor`, `supplier_description`) VALUES
(6, 'OE-033', 20100000, '', 1, '1 unit UPS APC SMT 3000I untuk BOD'),
(7, 'GE001', 500000, '', 1, 'ATK General'),
(7, 'IMP-004', 2500000, '', 1, 'Relokasi Instalasi Sprinkkler, Smoke Detector, Eva'),
(9, 'BLD01', 30000000, '', 1, 'Ruang Finance'),
(9, 'BLD02', 35000000, '', 1, 'Ruang HRD'),
(11, 'ATK01', 1000000, '', 1, 'Mesin Fotokopi'),
(11, 'BLD01', 30000000, '', 1, 'Ruang Finance'),
(11, 'FF-014', 1000, '', 1, '3 Pcs Gantungan Jas'),
(11, 'FF-017', 10000, '', 1, '1 Unit Lemari Kabinet utk Bp. Gunanta'),
(11, 'GE001', 550000, '', 1, 'ATK General'),
(11, 'OE-032', 888888, '', 1, '1 unit HP Desktop 251-122d dan 1 pcs Office Home&a'),
(12, 'BLD01', 22000000, '', 1, 'Ruang Finance'),
(12, 'FF-017', 900000, '', 1, '1 Unit Lemari Kabinet utk Bp. Gunanta'),
(12, 'GE001', 400000, '', 1, 'ATK General'),
(12, 'OE-032', 2500000, '', 1, '1 unit HP Desktop 251-122d dan 1 pcs Office Home&a'),
(13, 'ATK01', 800000, '', 1, 'Mesin Fotokopi'),
(13, 'OE-005', 100000, '', 1, '2 Server + 4 Harddisk'),
(13, 'OE-028', 700000, '', 1, '2 Unit Pesawat Telepon Panasonic (Dhini dan Alvin'),
(13, 'OE-031', 10000000, '', 1, 'Atmosphere Air Treatment System'),
(15, 'FF-017', 888888, '', 1, '1 Unit Lemari Kabinet utk Bp. Gunanta'),
(15, 'OE-028', 600000, '', 1, '2 Unit Pesawat Telepon Panasonic (Dhini dan Alvin'),
(15, 'OE-044', 3000000, '', 1, '1 Unit TV - JP'),
(16, 'ATK01', 5000000, '', 1, 'Mesin Fotokopi'),
(19, '23', 5000000, '', 1, 'Computer'),
(38, '23', 5000000, '', 1, 'Computer'),
(38, 'SOF01', 30000000, '', 1, 'Software Accounting'),
(39, '23', 5000000, '', 1, 'Computer'),
(39, 'GE001', 600000, '', 1, 'ATK General'),
(39, 'Genset', 5000000, '', 1, 'Genset'),
(39, 'genset2', 3500000, '', 1, 'genset2'),
(42, '1554545', 500000, '', 1, 'Komputer'),
(42, 'Akta', 6500000, '', 1, 'Akta'),
(42, 'BLD02', 0, '', 1, 'Ruang HRD'),
(42, 'FF-004', 10000, '', 1, 'Kursi Bp Gama'),
(44, '1554545', 1000, '', 1, 'Komputer'),
(44, 'FF-0010', 1000, '', 1, '4 Kursi Leather Boss (Direksi, Head Marketing, 2 o');

-- --------------------------------------------------------

--
-- Table structure for table `0_purch_orders`
--

CREATE TABLE `0_purch_orders` (
  `order_no` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `comments` tinytext COLLATE utf8_unicode_ci,
  `ord_date` date NOT NULL DEFAULT '0000-00-00',
  `reference` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `requisition_no` tinytext COLLATE utf8_unicode_ci,
  `into_stock_location` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `delivery_address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `total` double NOT NULL DEFAULT '0',
  `prep_amount` double NOT NULL DEFAULT '0',
  `alloc` double NOT NULL DEFAULT '0',
  `tax_included` tinyint(1) NOT NULL DEFAULT '0',
  `flag` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_purch_order_details`
--

CREATE TABLE `0_purch_order_details` (
  `po_detail_item` int(11) NOT NULL,
  `order_no` int(11) NOT NULL DEFAULT '0',
  `item_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` tinytext COLLATE utf8_unicode_ci,
  `delivery_date` date NOT NULL DEFAULT '0000-00-00',
  `qty_invoiced` double NOT NULL DEFAULT '0',
  `unit_price` double NOT NULL DEFAULT '0',
  `act_price` double NOT NULL DEFAULT '0',
  `std_cost_unit` double NOT NULL DEFAULT '0',
  `quantity_ordered` double NOT NULL DEFAULT '0',
  `quantity_received` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_quick_entries`
--

CREATE TABLE `0_quick_entries` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `usage` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `base_amount` double NOT NULL DEFAULT '0',
  `base_desc` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bal_type` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_quick_entries`
--

INSERT INTO `0_quick_entries` (`id`, `type`, `description`, `usage`, `base_amount`, `base_desc`, `bal_type`) VALUES
(1, 3, 'Bayar Telepon', 'Listrik Bulanan', 0, '64001', 1),
(2, 0, 'Bayar Listrik', 'Listrik Bulanan', 1000000, 'Amount', 0),
(3, 0, 'Internet', 'Internet kantor bulanan', 3000000, 'pokok', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_quick_entry_lines`
--

CREATE TABLE `0_quick_entry_lines` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `qid` smallint(6) UNSIGNED NOT NULL,
  `amount` double DEFAULT '0',
  `memo` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `dest_id` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dimension_id` smallint(6) UNSIGNED DEFAULT NULL,
  `dimension2_id` smallint(6) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_quick_entry_lines`
--

INSERT INTO `0_quick_entry_lines` (`id`, `qid`, `amount`, `memo`, `action`, `dest_id`, `dimension_id`, `dimension2_id`) VALUES
(1, 1, 10, 'PPN Masukan', '%+', '11703', 0, 0),
(2, 1, 5000000, 'Telepon ', 'a', '64001', 0, 0),
(3, 1, 2, 'PPh 23', '%-', '21220', 0, 0),
(4, 2, 1000000, 'Biaya Listrik Bulanan', 'a', '64002', 0, 0),
(7, 2, 10, 'PPN 10 %', '%+', '11703', 0, 0),
(8, 2, 2, 'PPh 2%', '%-', '21220', 0, 0),
(10, 3, 3000000, 'Internet bulanan', 'a', '64001', 0, 0),
(11, 3, 10, 'Internet bulanan', '%+', '11703', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_recurrent_invoices`
--

CREATE TABLE `0_recurrent_invoices` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `description` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `order_no` int(11) UNSIGNED NOT NULL,
  `debtor_no` int(11) UNSIGNED DEFAULT NULL,
  `group_no` smallint(6) UNSIGNED DEFAULT NULL,
  `days` int(11) NOT NULL DEFAULT '0',
  `monthly` int(11) NOT NULL DEFAULT '0',
  `begin` date NOT NULL DEFAULT '0000-00-00',
  `end` date NOT NULL DEFAULT '0000-00-00',
  `last_sent` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_reflines`
--

CREATE TABLE `0_reflines` (
  `id` int(11) NOT NULL,
  `trans_type` int(11) NOT NULL,
  `prefix` char(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pattern` varchar(35) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `description` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_reflines`
--

INSERT INTO `0_reflines` (`id`, `trans_type`, `prefix`, `pattern`, `description`, `default`, `inactive`) VALUES
(1, 0, '', 'MI-1707002', '', 1, 0),
(2, 1, '', '', '', 1, 0),
(3, 2, '', '', '', 1, 0),
(4, 4, '', '2', '', 1, 0),
(5, 10, '', '19', '', 1, 0),
(6, 11, '', '1', '', 1, 0),
(7, 12, '', '', '', 1, 0),
(8, 13, '', '2', '', 1, 0),
(9, 16, '', '1', '', 1, 0),
(10, 17, '', '1', '', 1, 0),
(11, 18, '', '16', '', 1, 0),
(12, 20, '', '-1701008', '', 1, 0),
(13, 21, '', '1', '', 1, 0),
(14, 22, '', '', '', 1, 0),
(15, 25, '', '10', '', 1, 0),
(16, 26, '', '1', '', 1, 0),
(17, 28, '', '1', '', 1, 0),
(18, 29, '', '1', '', 1, 0),
(19, 30, '', '3', '', 1, 0),
(20, 32, '', '1', '', 1, 0),
(21, 35, '', '1', '', 1, 0),
(22, 40, '', '8', '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_refs`
--

CREATE TABLE `0_refs` (
  `id` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `reference` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_salesman`
--

CREATE TABLE `0_salesman` (
  `salesman_code` int(11) NOT NULL,
  `salesman_name` char(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `salesman_phone` char(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `salesman_fax` char(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `salesman_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `provision` double NOT NULL DEFAULT '0',
  `break_pt` double NOT NULL DEFAULT '0',
  `provision2` double NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_salesman`
--

INSERT INTO `0_salesman` (`salesman_code`, `salesman_name`, `salesman_phone`, `salesman_fax`, `salesman_email`, `provision`, `break_pt`, `provision2`, `inactive`) VALUES
(1, 'Sales 1', '', '', '', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_sales_orders`
--

CREATE TABLE `0_sales_orders` (
  `order_no` int(11) NOT NULL,
  `trans_type` smallint(6) NOT NULL DEFAULT '30',
  `version` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `debtor_no` int(11) NOT NULL DEFAULT '0',
  `branch_code` int(11) NOT NULL DEFAULT '0',
  `reference` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `customer_ref` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `comments` tinytext COLLATE utf8_unicode_ci,
  `ord_date` date NOT NULL DEFAULT '0000-00-00',
  `order_type` int(11) NOT NULL DEFAULT '0',
  `ship_via` int(11) NOT NULL DEFAULT '0',
  `delivery_address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deliver_to` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `freight_cost` double NOT NULL DEFAULT '0',
  `from_stk_loc` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `delivery_date` date NOT NULL DEFAULT '0000-00-00',
  `payment_terms` int(11) DEFAULT NULL,
  `total` double NOT NULL DEFAULT '0',
  `prep_amount` double NOT NULL DEFAULT '0',
  `alloc` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_sales_order_details`
--

CREATE TABLE `0_sales_order_details` (
  `id` int(11) NOT NULL,
  `order_no` int(11) NOT NULL DEFAULT '0',
  `trans_type` smallint(6) NOT NULL DEFAULT '30',
  `stk_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` tinytext COLLATE utf8_unicode_ci,
  `qty_sent` double NOT NULL DEFAULT '0',
  `unit_price` double NOT NULL DEFAULT '0',
  `quantity` double NOT NULL DEFAULT '0',
  `invoiced` double NOT NULL DEFAULT '0',
  `discount_percent` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_sales_pos`
--

CREATE TABLE `0_sales_pos` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `pos_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `cash_sale` tinyint(1) NOT NULL,
  `credit_sale` tinyint(1) NOT NULL,
  `pos_location` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `pos_account` smallint(6) UNSIGNED NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_sales_pos`
--

INSERT INTO `0_sales_pos` (`id`, `pos_name`, `cash_sale`, `credit_sale`, `pos_location`, `pos_account`, `inactive`) VALUES
(1, 'Default', 1, 1, 'DEF', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_sales_types`
--

CREATE TABLE `0_sales_types` (
  `id` int(11) NOT NULL,
  `sales_type` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_included` int(1) NOT NULL DEFAULT '0',
  `factor` double NOT NULL DEFAULT '1',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_sales_types`
--

INSERT INTO `0_sales_types` (`id`, `sales_type`, `tax_included`, `factor`, `inactive`) VALUES
(1, 'Pendapatan Jasa', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_security_roles`
--

CREATE TABLE `0_security_roles` (
  `id` int(11) NOT NULL,
  `role` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sections` text COLLATE utf8_unicode_ci,
  `areas` text COLLATE utf8_unicode_ci,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_security_roles`
--

INSERT INTO `0_security_roles` (`id`, `role`, `description`, `sections`, `areas`, `inactive`) VALUES
(1, 'Pencarian', 'Pencarian', '768;2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15872;16128', '257;258;259;260;513;514;515;516;517;518;519;520;521;522;523;524;525;773;774;2822;3073;3075;3076;3077;3329;3330;3331;3332;3333;3334;3335;5377;5633;5640;5889;5890;5891;7937;7938;7939;7940;8193;8194;8450;8451;10497;10753;11009;11010;11012;13313;13315;15617;15618;15619;15620;15621;15622;15623;15624;15625;15626;15873;15882;16129;16130;16131;16132;775', 0),
(2, 'Admin Sistem', 'Admin Sistem', '256;512;768;2816;3072;3328;5376;5632;5888;7936;8192;8448;9216;9472;9728;10496;10752;11008;13056;13312;15616;15872;16128;1467392', '257;258;259;260;513;514;515;516;517;518;519;520;521;522;523;524;525;526;769;770;771;772;773;774;775;2817;2818;2819;2820;2821;2822;2823;3073;3083;3074;3082;3075;3076;3077;3078;3079;3080;3081;3329;3330;3331;3332;3333;3334;3335;5377;5633;5634;5641;5635;5636;5637;5638;5639;5640;5889;5890;5891;7937;7938;7939;7940;8193;8194;8195;8196;8197;8449;8450;8451;9217;9218;9220;9473;9474;9475;9476;9729;10497;10753;10754;10755;10756;10757;11009;11010;11011;11012;13057;13313;13314;13315;15617;15618;15619;15620;15621;15622;15623;15624;15628;15625;15626;15627;15630;15629;15873;15874;15875;15876;15877;15878;15879;15880;15883;15881;15882;15884;16129;16130;16131;16132;1467492', 0),
(3, 'Mgr Sales', 'Mgr Sales', '768;3072;5632;8192;15872', '773;774;3073;3075;3081;5633;8194;15873;775', 0),
(4, 'Mgr Gudang', 'Mgr Gudang', '2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15872;16128;768', '775', 0),
(5, 'Mgr Produksi', 'Mgr Produksi', '512;2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128;768', '775', 0),
(6, 'Pembelian', 'Pembelian', '512;2816;3072;3328;5376;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128;768', '775', 0),
(7, 'Penagihan', 'Penagihan', '512;768;2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128', '521;523;524;771;773;774;2818;2819;2820;2821;2822;2823;3073;3073;3074;3075;3076;3077;3078;3079;3080;3081;3081;3329;3330;3330;3330;3331;3331;3332;3333;3334;3335;5633;5633;5634;5637;5638;5639;5640;5640;5889;5890;5891;8193;8194;8194;8196;8197;8450;8451;10753;10755;11009;11010;11012;13313;13315;15617;15619;15620;15621;15624;15624;15873;15876;15877;15878;15880;15882;16129;16130;16131;16132;775', 0),
(8, 'Pembayaran', 'Pembayaran', '512;2816;3072;3328;5376;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128;768', '775', 0),
(9, 'Akuntan', 'Akuntan Baru', '512;768;2816;3072;3328;5376;5632;5888;8192;8448;9216;9472;9728;13312;15616;15872;16128', '513;519;521;523;524;771;772;773;774;775;2817;2818;2819;2820;2821;2822;2823;3073;3074;3082;3075;3076;3077;3078;3079;3080;3081;3329;3330;3331;3332;3333;3334;3335;5377;5633;5634;5635;5637;5638;5639;5640;5889;5890;5891;8193;8194;8196;8197;8449;8450;8451;9217;9218;9220;9473;9474;9475;9476;9729;13313;13314;13315;15617;15618;15619;15620;15621;15622;15623;15624;15628;15625;15626;15627;15630;15873;15874;15875;15876;15877;15878;15879;15880;15883;15881;15882;15884;16129;16130;16131;16132;257;258;259;260;7937;7938;7939;7940;10497;10753;10755;11009;11010;11012', 0),
(10, 'Sub Admin', 'Sub Admin', '512;768;2816;3072;3328;5376;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128', '257;258;259;260;521;523;524;771;772;773;774;2818;2819;2820;2821;2822;2823;3073;3074;3082;3075;3076;3077;3078;3079;3080;3081;3329;3330;3331;3332;3333;3334;3335;5377;5633;5634;5635;5637;5638;5639;5640;5889;5890;5891;7937;7938;7939;7940;8193;8194;8196;8197;8449;8450;8451;10497;10753;10755;11009;11010;11012;13057;13313;13315;15617;15619;15620;15621;15624;15873;15874;15876;15877;15878;15879;15880;15882;16129;16130;16131;16132;775', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_shippers`
--

CREATE TABLE `0_shippers` (
  `shipper_id` int(11) NOT NULL,
  `shipper_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone2` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_shippers`
--

INSERT INTO `0_shippers` (`shipper_id`, `shipper_name`, `phone`, `phone2`, `contact`, `address`, `inactive`) VALUES
(1, 'Default', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_sql_trail`
--

CREATE TABLE `0_sql_trail` (
  `id` int(11) UNSIGNED NOT NULL,
  `sql` text COLLATE utf8_unicode_ci NOT NULL,
  `result` tinyint(1) NOT NULL,
  `msg` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_stock_category`
--

CREATE TABLE `0_stock_category` (
  `category_id` int(11) NOT NULL,
  `description` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dflt_tax_type` int(11) NOT NULL DEFAULT '1',
  `dflt_units` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'each',
  `dflt_mb_flag` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'B',
  `dflt_sales_act` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dflt_cogs_act` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dflt_inventory_act` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dflt_adjustment_act` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dflt_wip_act` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dflt_dim1` int(11) DEFAULT NULL,
  `dflt_dim2` int(11) DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `dflt_no_sale` tinyint(1) NOT NULL DEFAULT '0',
  `dflt_no_purchase` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_stock_category`
--

INSERT INTO `0_stock_category` (`category_id`, `description`, `dflt_tax_type`, `dflt_units`, `dflt_mb_flag`, `dflt_sales_act`, `dflt_cogs_act`, `dflt_inventory_act`, `dflt_adjustment_act`, `dflt_wip_act`, `dflt_dim1`, `dflt_dim2`, `inactive`, `dflt_no_sale`, `dflt_no_purchase`) VALUES
(1, 'Furnitures &amp; Fixtures (Group I)', 2, 'each', 'F', '84000', '12232', '12231', '66001', '11401', 0, 0, 0, 1, 0),
(2, 'Office Equipments (Group I)', 2, 'each', 'F', '84000', '12222', '12221', '66001', '11401', 0, 0, 0, 1, 0),
(3, 'Improvements (Group I)', 2, 'each', 'F', '84000', '12212', '12211', '66001', '11401', 0, 0, 0, 1, 0),
(4, 'Vehicles', 2, 'each', 'F', '84000', '12242', '12241', '66001', '11401', 0, 0, 0, 1, 0),
(5, 'Peralatan Kantor', 2, 'each', 'B', '11401', '11401', '12221', '12221', '11401', 0, 0, 0, 1, 0),
(6, 'Pendapatan', 2, 'each', 'B', '11401', '41000', '89999', '89999', '11401', 0, 0, 0, 0, 1),
(8, 'Jasa Manajemen', 2, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 0, 0),
(9, 'Jasa Profesional', 1, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 0, 0),
(10, 'Jasa Internet', 1, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 0, 0),
(11, 'Jasa Software', 2, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 0, 0),
(12, 'Media Massa', 2, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 0, 0),
(13, 'Jasa Percetakan', 2, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 0, 0),
(14, 'Jasa Penyelenggara Kegiatan', 2, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 0, 0),
(15, 'Jasa Design', 2, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 0, 0),
(16, 'Jasa Penjualan SB', 2, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 0, 0),
(17, 'Jasa penilai', 3, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 1, 0),
(43, 'Jasa perawatan', 3, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 1, 0),
(44, 'Jasa maklon', 3, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 1, 0),
(45, 'Jasa penyelidikan ', 3, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 1, 0),
(46, 'Jasa penyelenggara ', 3, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 1, 0),
(47, 'Jasa pengepakan', 3, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 1, 0),
(48, 'Jasa media', 3, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 1, 0),
(49, 'Jasa pembasmian hama', 3, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 1, 0),
(50, 'Jasa kebersihan', 3, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 1, 0),
(51, 'Jasa katering', 3, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 1, 0),
(52, 'Jasa instalasi', 3, 'each', 'D', '11401', '11401', '89999', '89999', '11401', 0, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_stock_fa_class`
--

CREATE TABLE `0_stock_fa_class` (
  `fa_class_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `long_description` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `depreciation_rate` double NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_stock_fa_class`
--

INSERT INTO `0_stock_fa_class` (`fa_class_id`, `parent_id`, `description`, `long_description`, `depreciation_rate`, `inactive`) VALUES
('1.0', '1', 'Penyusutan', '', 1, 0),
('2', '', 'Tanpa Penyusutan', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_stock_master`
--

CREATE TABLE `0_stock_master` (
  `stock_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `tax_type_id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `long_description` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `units` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'each',
  `mb_flag` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'B',
  `sales_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cogs_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inventory_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `adjustment_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `wip_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dimension_id` int(11) DEFAULT NULL,
  `dimension2_id` int(11) DEFAULT NULL,
  `purchase_cost` double NOT NULL DEFAULT '0',
  `material_cost` double NOT NULL DEFAULT '0',
  `labour_cost` double NOT NULL DEFAULT '0',
  `overhead_cost` double NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `no_sale` tinyint(1) NOT NULL DEFAULT '0',
  `no_purchase` tinyint(1) NOT NULL DEFAULT '0',
  `editable` tinyint(1) NOT NULL DEFAULT '0',
  `depreciation_method` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `depreciation_rate` double NOT NULL DEFAULT '0',
  `depreciation_factor` double NOT NULL DEFAULT '0',
  `depreciation_start` date NOT NULL DEFAULT '0000-00-00',
  `depreciation_date` date NOT NULL DEFAULT '0000-00-00',
  `fa_class_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cip` tinyint(4) NOT NULL,
  `cipamount` double(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_stock_master`
--

INSERT INTO `0_stock_master` (`stock_id`, `category_id`, `tax_type_id`, `description`, `long_description`, `units`, `mb_flag`, `sales_account`, `cogs_account`, `inventory_account`, `adjustment_account`, `wip_account`, `dimension_id`, `dimension2_id`, `purchase_cost`, `material_cost`, `labour_cost`, `overhead_cost`, `inactive`, `no_sale`, `no_purchase`, `editable`, `depreciation_method`, `depreciation_rate`, `depreciation_factor`, `depreciation_start`, `depreciation_date`, `fa_class_id`, `cip`, `cipamount`) VALUES
('001', 5, 0, 'ATK', 'Peralatan Kantor', 'each', 'B', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '', 0, 0, '0000-00-00', '0000-00-00', '', 0, 0.00),
('1554545', 1, 2, 'Komputer', '', 'each', 'F', '84000', '12232', '12231', '66001', '11401', 0, 0, 500000, 1685555.5566667, 0, 0, 0, 1, 0, 0, 'D', 50, 0, '2017-01-01', '2017-12-31', '1.0', 0, 0.00),
('23', 5, 3, 'Computer', 'Computer Julia', 'each', 'F', '11699', '12222', '12221', '66001', '11699', 0, 0, 5000000, 5000000, 0, 0, 0, 1, 0, 1, '', 0, 0, '2017-01-25', '2017-01-25', '', 1, 10000000.00),
('Akta', 5, 7, 'Akta', '', 'each', 'D', '', '', '', '', '', 0, 0, 6500000, 0, 0, 0, 0, 1, 0, 0, '', 0, 0, '0000-00-00', '0000-00-00', '', 0, 0.00),
('ATK01', 5, 3, 'Mesin Fotokopi', '', 'each', 'B', '11401', '11401', '12221', '12221', '11401', 0, 0, 800000, 2266666.6666667, 0, 0, 0, 0, 0, 0, '', 0, 0, '0000-00-00', '0000-00-00', '', 0, 0.00),
('BLD01', 1, 3, 'Ruang Finance', 'Renovasi ruang finance', 'each', 'F', '', '12232', '12231', '66001', '', 0, 0, 22000000, 26000000, 0, 0, 0, 1, 0, 1, 'S', 20, 0, '2017-02-01', '2017-02-01', '1.0', 1, 50000000.00),
('BLD02', 1, 3, 'Ruang HRD', 'Renovasi ruang HRD', 'each', 'F', '', '12232', '12231', '66001', '', 0, 0, 35000000, 25000000, 0, 0, 0, 1, 0, 1, 'S', 20, 0, '2017-02-01', '2017-02-01', '1.0', 1, 45000000.00),
('Bloomberg', 5, 7, 'Bloomberg', '', 'each', 'D', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '', 0, 0, '0000-00-00', '0000-00-00', '', 0, 0.00),
('FF-001', 1, 2, 'Pelunasan perkerjaan Furniture Credensa R. Analis', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2013-07-03', '0000-00-00', '1', 0, 0.00),
('FF-0010', 1, 2, '4 Kursi Leather Boss (Direksi, Head Marketing, 2 org Fund Manager)', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 13356000, 13356000, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2016-07-20', '0000-00-00', '1', 0, 0.00),
('FF-002', 1, 2, 'Workstation', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2014-01-29', '0000-00-00', '1', 0, 0.00),
('FF-003', 1, 2, 'Credenza Meja 1500x500x850', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2014-10-15', '0000-00-00', '1', 0, 0.00),
('FF-004', 1, 2, 'Kursi Bp Gama', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 10000, 2954722.2233333, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2014-12-16', '2017-12-31', '1', 0, 0.00),
('FF-005', 1, 2, 'Lemari Cabinet, Supply & Instal Sand Blast Sticker Ruang Bp. Gama', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2015-02-04', '0000-00-00', '1', 0, 0.00),
('FF-006', 1, 2, 'Furniture (credenza) di ruangan Bp. Gama', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2015-03-30', '0000-00-00', '1', 0, 0.00),
('FF-007', 1, 2, 'Kursi Tamu di ruangan Bp. Gama', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2015-03-31', '0000-00-00', '1', 0, 0.00),
('FF-008', 1, 2, 'Office Furniture (Runway Desk & Director Chair)', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2015-07-08', '0000-00-00', '1', 0, 0.00),
('FF-009', 1, 2, 'Rak Server (Furniture IT) - PT. Multipro Jaya Prima', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2016-06-10', '0000-00-00', '1', 0, 0.00),
('FF-011', 1, 2, 'Furniture (2 Meja Dealer, 6 Meja Analis dan 6 Kursi Hadap)', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2016-07-29', '0000-00-00', '1', 0, 0.00),
('FF-012', 1, 2, 'Office Furniture (1 Credenza dan 10 kursi untuk ruang meeting kecil)', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2016-08-18', '0000-00-00', '1', 0, 0.00),
('FF-013', 1, 2, 'Pembelian Furniture (Credenza, Coffee table dan Lemari file)', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2016-08-23', '0000-00-00', '1', 0, 0.00),
('FF-014', 1, 2, '3 Pcs Gantungan Jas', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2016-09-13', '0000-00-00', '1', 0, 0.00),
('FF-015', 1, 2, 'Furniture (3 unit Credenza dan Coffe Table utk Bp. Fajar, Gunanta dan Cholis)', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2016-09-22', '0000-00-00', '1', 0, 0.00),
('FF-016', 1, 2, 'Furniture - 3 Unit Sofa (Bp. Fajar, Gunanta dan Cholis)', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2016-09-28', '0000-00-00', '1', 0, 0.00),
('FF-017', 1, 2, '1 Unit Lemari Kabinet utk Bp. Gunanta', '', 'each', 'F', '84000', '12232', '12231', '66001', '14041', NULL, NULL, 7000000, 2929629.3333333, 0, 0, 0, 0, 0, 0, 'S', 25, 0, '2016-10-28', '0000-00-00', '1', 0, 0.00),
('GE001', 5, 3, 'ATK General', '', 'each', 'B', '', '', '', '', '', 0, 0, 5348983, 5348983, 0, 0, 0, 0, 0, 1, '', 0, 0, '0000-00-00', '0000-00-00', '', 0, 0.00),
('GE002', 5, 3, 'Keperluan Dapur', 'Keperluan dapur kantor', 'each', 'B', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '', 0, 0, '0000-00-00', '0000-00-00', '', 0, 0.00),
('Genset', 5, 3, 'Genset ', 'Genset 5000 watt', 'each', 'F', '', '', '', '', '', 0, 0, 5000000, 5000000, 0, 0, 0, 1, 0, 1, 'S', 25, 0, '2017-02-01', '2017-02-01', '', 1, 30000000.00),
('genset2', 5, 2, 'genset2', '', 'each', 'M', '', '', '', '', '', 0, 0, 3500000, 3500000, 0, 0, 0, 1, 0, 0, '', 0, 0, '2017-02-01', '2017-02-01', '', 1, 7500000.00),
('IMP-001', 3, 2, 'Material Raised Floor dan Biaya pasang', '', 'each', 'F', '84000', '12212', '12211', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.33, 0, '2016-06-07', '0000-00-00', '1', 0, 0.00),
('IMP-0010', 3, 2, 'Pekerjaan Listrik dan Data Voice (Termin III)', '', 'each', 'F', '84000', '12212', '12211', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.33, 0, '2016-06-29', '0000-00-00', '1', 0, 0.00),
('IMP-002', 3, 2, 'Material Raised Floor dan Biaya pasang', '', 'each', 'F', '84000', '12212', '12211', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.33, 0, '2016-06-07', '0000-00-00', '1', 0, 0.00),
('IMP-003', 3, 2, 'Instalasi FCU Chiller (30% DP)', '', 'each', 'F', '84000', '12212', '12211', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.33, 0, '2016-06-07', '0000-00-00', '1', 0, 0.00),
('IMP-004', 3, 2, 'Relokasi Instalasi Sprinkkler, Smoke Detector, Evacuation Speaker dan AC Gedung (50% DP)', '', 'each', 'F', '84000', '12212', '12211', '66001', '14041', NULL, NULL, 0, 2500000, 0, 0, 0, 0, 0, 0, 'S', 0.33, 0, '2016-06-07', '0000-00-00', '1', 0, 0.00),
('IMP-005', 3, 2, 'Pekerjaan Listrik dan Data Voice (50% DP)', '', 'each', 'F', '84000', '12212', '12211', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.33, 0, '2016-06-07', '0000-00-00', '1', 0, 0.00),
('IMP-006', 3, 2, 'Pekerjaan Interior - Renovasi kantor - PT. Fertikal Horizontal Citrakarya Selaras', '', 'each', 'F', '84000', '12212', '12211', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.33, 0, '2016-06-07', '0000-00-00', '1', 0, 0.00),
('IMP-007', 3, 2, 'Pekerjaan Listrik dan Data Voice (Termin II)', '', 'each', 'F', '84000', '12212', '12211', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.33, 0, '2016-06-22', '0000-00-00', '1', 0, 0.00),
('IMP-008', 3, 2, 'Pembayaran Instalasi FCU Chiller (Termin II dan III) dan Pekerjaan tambahan', '', 'each', 'F', '84000', '12212', '12211', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.33, 0, '2016-06-29', '0000-00-00', '1', 0, 0.00),
('IMP-009', 3, 2, 'Pembayaran Instalasi FCU Chiller (Termin II dan III) dan Pekerjaan tambahan', '', 'each', 'F', '84000', '12212', '12211', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.33, 0, '2016-06-29', '0000-00-00', '1', 0, 0.00),
('IMP-011', 3, 2, 'Pekerjaan Interior - Renovasi kantor (Termin II) - PT. Fertikal Horizontal Citrakarya Selaras', '', 'each', 'F', '84000', '12212', '12211', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.33, 0, '2016-06-29', '0000-00-00', '1', 0, 0.00),
('IMP-012', 3, 2, 'Pekerjaan Interior - Renovasi kantor (Termin III) - PT. Fertikal Horizontal Citrakarya Selaras', '', 'each', 'F', '84000', '12212', '12211', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.33, 0, '2016-08-10', '0000-00-00', '1', 0, 0.00),
('IMP-013', 3, 2, 'Pelunasan Pembayaran Relokasi Instalasi Sprinkkler, Smoke Detector, Evacuation Speaker dan Penambahan KWH Listrik - Achmad Subchi', '', 'each', 'F', '84000', '12212', '12211', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.33, 0, '2016-08-10', '0000-00-00', '1', 0, 0.00),
('IMP-014', 3, 2, 'Tambahan Pekerjaan Listrik dan Data Voice - Nurul Priatin', '', 'each', 'F', '84000', '12212', '12211', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.33, 0, '2016-08-10', '0000-00-00', '1', 0, 0.00),
('M', 6, 2, 'Management Fee', '', 'each', 'D', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '', 0, 0, '0000-00-00', '0000-00-00', '', 0, 0.00),
('OE-001', 2, 2, 'PC HP, Ms.Windows 7, Ms.Office, Antivirus - Enos', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-02-11', '0000-00-00', '1', 0, 0.00),
('OE-0010', 2, 2, 'Asus Wireless + Hub d-link', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-06-24', '0000-00-00', '1', 0, 0.00),
('OE-002', 2, 2, 'PC HP  - Alvin', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-02-20', '0000-00-00', '1', 0, 0.00),
('OE-003', 2, 2, 'PC HP - Deasty', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-03-05', '0000-00-00', '1', 0, 0.00),
('OE-004', 2, 2, 'PC Pavillion, Ms.Office, Ms.Windows, Kaspersky - Monica', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-04-03', '0000-00-00', '1', 0, 0.00),
('OE-005', 2, 2, '2 Server + 4 Harddisk', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 100000, 100000, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-04-12', '0000-00-00', '1', 0, 0.00),
('OE-006', 2, 2, 'UPS APC', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-04-15', '0000-00-00', '1', 0, 0.00),
('OE-007', 2, 2, 'Windows Server ', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-05-08', '0000-00-00', '1', 0, 0.00),
('OE-008', 2, 2, 'Printer HP Laser Jet Pro P1102 - Mulia', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-05-31', '0000-00-00', '1', 0, 0.00),
('OE-009', 2, 2, 'Laptop Asus, win 7, Office 2010, Kaspersky -u/Training', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-06-18', '0000-00-00', '1', 0, 0.00),
('OE-011', 2, 2, 'Harddisk server HP 2 unit', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-07-23', '0000-00-00', '1', 0, 0.00),
('OE-012', 2, 2, 'Printer Warna u/ Back office', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-10-09', '0000-00-00', '1', 0, 0.00),
('OE-013', 2, 2, 'UPS u/ server', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-10-24', '0000-00-00', '1', 0, 0.00),
('OE-014', 2, 2, 'Panasonic Laser Fax', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-11-08', '0000-00-00', '1', 0, 0.00),
('OE-015', 2, 2, 'Harddisk ', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-12-23', '0000-00-00', '1', 0, 0.00),
('OE-016', 2, 2, '2 PC Iin & Eko, Windows 7, MS Office', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2013-09-03', '0000-00-00', '1', 0, 0.00),
('OE-017', 2, 2, 'WinPro 8,1 5 unit - Yudha, Yusuf, Liantie, Hery, P.David', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2014-03-18', '0000-00-00', '1', 0, 0.00),
('OE-018', 2, 2, 'Software win 7, office 2013 - Zara & R. Meeting', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 50000000, 50000000, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2014-04-18', '0000-00-00', '1', 0, 0.00),
('OE-019', 2, 2, 'Air Cleaner Austin (Penghirup Asap utk ruangan P.David)', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2014-06-03', '0000-00-00', '1', 0, 0.00),
('OE-020', 2, 2, 'PC Komputer Asus Desktop, Monitor LED, Ms.Office, Windows 7 - Prita', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2014-08-18', '0000-00-00', '1', 0, 0.00),
('OE-021', 2, 2, 'Laptop Asus, win 7, Office 2013, Mouse -u/Marketing (No. Ref Transaksi: BO401043 dan BO401044)', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2015-01-28', '0000-00-00', '1', 0, 0.00),
('OE-022', 2, 2, 'HP Pavilion 500-330d DT PC, Office 2013, OS 7 Pro 64 bit, RAM 8 GB DDR 3, Kaspersky - untuk RH', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2015-02-06', '0000-00-00', '1', 0, 0.00),
('OE-023', 2, 2, 'Nokia 1520, Wireless Charging Plate - Untuk JP', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2015-02-10', '0000-00-00', '1', 0, 0.00),
('OE-024', 2, 2, 'Alcatel - Lucent IP Touch 4068 Phone (Untuk Bp. Gama), Alcatel - Lucent IP Digital 4019 Phone (Untuk Back Up)', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2015-02-25', '0000-00-00', '1', 0, 0.00),
('OE-025', 2, 2, 'HP Pavilion Slimline 400-326X, Office 2013, Windows 7 32 Bit, Kaspersky - Computer untuk Elvia', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2015-02-25', '0000-00-00', '1', 0, 0.00),
('OE-026', 2, 2, 'HP Pavilion Slimline, Office 2013, Windows 8, Kaspersky, VGA, Mouse Logitech dan Mikrotik Wireless utk server (2 PC untuk Marketing dan 1 PC untuk ruang meeting)', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2015-03-24', '0000-00-00', '1', 0, 0.00),
('OE-027', 2, 2, 'Pembelian 1 Unit Computer Dell Inspiron 3847MT, Windows 7 dan Office 2013 - Alvin Michael (Analis)', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2015-08-31', '0000-00-00', '1', 0, 0.00),
('OE-028', 2, 2, '2 Unit Pesawat Telepon Panasonic (Dhini dan Alvin M)', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 600000, 866666.66666667, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2015-09-02', '0000-00-00', '1', 0, 0.00),
('OE-029', 2, 2, 'BenQ Projector MX620ST', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-02-17', '0000-00-00', '1', 0, 0.00),
('OE-030', 2, 2, '3 Unit PC HP Pavilion 550-127D untuk BOD, 1 Unit PC HP Pavilion 450-122D untuk Fund Manager, 4 Pcs Office Home&Business 2013, 4 Unit Monitor Led 20\'', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-06-02', '0000-00-00', '1', 0, 0.00),
('OE-031', 2, 2, 'Atmosphere Air Treatment System', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 10000000, 10000000, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-06-03', '0000-00-00', '1', 0, 0.00),
('OE-032', 2, 2, '1 unit HP Desktop 251-122d dan 1 pcs Office Home&Business 2013 untuk Head Finance and Accounting', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 2500000, 1694444, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-06-24', '0000-00-00', '1', 0, 0.00),
('OE-033', 2, 2, '1 unit UPS APC SMT 3000I untuk BOD', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 20100000, 20100000, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-06-24', '0000-00-00', '1', 0, 0.00),
('OE-034', 2, 2, '1 unit UPS APC SMT 3000I untuk Head Finance and Accounting', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-06-24', '0000-00-00', '1', 0, 0.00),
('OE-035', 2, 2, '4 Unit Atmosphere Air Treatment System @Rp17.226.000', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-07-27', '0000-00-00', '1', 0, 0.00),
('OE-036', 2, 2, 'Pembelian Peralatan Kantor - 3 Unit Paper Shredder (Bp. Fajar, Gunanta & Cholis), 1 Unit TV (Bp. Fajar) dan 3 Unit Telepon (Bp. Fajar, Gunanta & Cholis)', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-09-09', '0000-00-00', '1', 0, 0.00),
('OE-037', 2, 2, '3 Unit Printer HP Laserjet CP1025 (Bp. Fajar, Gunanta dan Cholis)', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-09-13', '0000-00-00', '1', 0, 0.00),
('OE-038', 2, 2, 'embelian Server (HP Proliant DL380p-G9/E5-2640v3 dan DL380p-G9/2p e5-2640v3)', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-09-21', '0000-00-00', '1', 0, 0.00),
('OE-039', 2, 2, 'DP 50% Jasa Pembuatan Sistem KAM - PT. Praisindo Teknologi', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-10-10', '0000-00-00', '1', 0, 0.00),
('OE-040', 2, 2, '5 Unit HP Pavilion 550-0201, 7 Unit Microsoft Office 2013, 2 Unit Kaspersky, 2 Unit HP Envy 750-101D, 5 Unit CORSAIR Memory PC 4 GB DDR3 dan 2 Unit CORSAIR Memory PC 1 x 4 GB DDR4', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-10-28', '0000-00-00', '1', 0, 0.00),
('OE-041', 2, 2, 'PT.Bhinneka Mentaridimensi - Peralatan Kantor -1 Unit Vacuum Cleaner', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-10-28', '0000-00-00', '1', 0, 0.00),
('OE-042', 2, 2, '2 unit Alcatel Lucent 4019 Digital Phone Set (Tito dan Ratih) - PT. Jaringan Intech Indonesia', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-11-04', '0000-00-00', '1', 0, 0.00),
('OE-043', 2, 2, 'Camera Mirrorless - Sony Alpha A6000 Kit 16-50mm', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-11-11', '0000-00-00', '1', 0, 0.00),
('OE-044', 2, 2, '1 Unit TV - JP', '', 'each', 'F', '84000', '12222', '12221', '66001', '14041', NULL, NULL, 3000000, 3000000, 0, 0, 0, 0, 0, 0, 'S', 0.25, 0, '2016-11-11', '0000-00-00', '1', 0, 0.00),
('P', 6, 2, 'Sharing Fee', '', 'each', 'D', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '', 0, 0, '0000-00-00', '0000-00-00', '', 0, 0.00),
('PDPT001', 6, 2, 'Jasa Manajemen', 'Jasa Managemen bulanan', 'each', 'D', '11401', '41000', '89999', '89999', '11401', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '', 0, 0, '0000-00-00', '0000-00-00', '', 0, 0.00),
('PDPT002', 6, 2, 'Sharing Fee', 'Jasa sharing fee penjualan reksadana', 'each', 'D', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '', 0, 0, '0000-00-00', '0000-00-00', '', 0, 0.00),
('SOF01', 2, 2, 'Software Accounting', 'Front Accounting', 'each', 'F', '', '12222', '12221', '66001', '', 0, 0, 3000000, 3000000, 0, 0, 0, 1, 0, 0, 'S', 20, 0, '2017-03-01', '2017-03-01', '1.0', 1, 80000000.00),
('Tanaman', 5, 7, 'Tanaman', '', 'each', 'B', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '', 0, 0, '0000-00-00', '0000-00-00', '', 0, 0.00),
('TB', 1, 3, 'Tanah Test', 'Tanah Test', 'each', 'F', '', '', '12251', '', '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '', 0, 0, '0000-00-00', '0000-00-00', '2', 0, 0.00),
('VHC-001', 4, 2, 'Toyota Fortuner', '', 'each', 'F', '84000', '12242', '12241', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.125, 0, '2009-11-02', '0000-00-00', '1', 0, 0.00),
('VHC-002', 4, 2, 'Toyota Alphard', '', 'each', 'F', '84000', '12242', '12241', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.125, 0, '2012-03-20', '0000-00-00', '1', 0, 0.00),
('VHC-003', 4, 2, 'Mercedes Benz (B165 HIM)', '', 'each', 'F', '84000', '12242', '12241', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.125, 0, '2012-04-09', '0000-00-00', '1', 0, 0.00),
('VHC-004', 4, 2, 'Odysey', '', 'each', 'F', '84000', '12242', '12241', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.125, 0, '2013-08-30', '0000-00-00', '1', 0, 0.00),
('VHC-005', 4, 2, 'Subaru Forester', '', 'each', 'F', '84000', '12242', '12241', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.125, 0, '2013-10-21', '0000-00-00', '1', 0, 0.00),
('VHC-006', 4, 2, 'Mobil Harrier', '', 'each', 'F', '84000', '12242', '12241', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.125, 0, '2014-12-01', '0000-00-00', '1', 0, 0.00),
('VHC-007', 4, 2, 'BMW M4 Coupe', '', 'each', 'F', '84000', '12242', '12241', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.125, 0, '2015-02-24', '0000-00-00', '1', 0, 0.00),
('VHC-008', 4, 2, 'Bangunan', '', 'each', 'F', '84000', '12242', '12241', '66001', '14041', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 'S', 0.05, 0, '2011-01-10', '0000-00-00', '1', 0, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `0_stock_moves`
--

CREATE TABLE `0_stock_moves` (
  `trans_id` int(11) NOT NULL,
  `trans_no` int(11) NOT NULL DEFAULT '0',
  `stock_id` char(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` smallint(6) NOT NULL DEFAULT '0',
  `loc_code` char(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tran_date` date DEFAULT NULL,
  `price` double NOT NULL DEFAULT '0',
  `reference` char(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `qty` double NOT NULL DEFAULT '1',
  `standard_cost` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_suppliers`
--

CREATE TABLE `0_suppliers` (
  `supplier_id` int(11) NOT NULL,
  `supp_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `supp_ref` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `supp_address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `gst_no` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `supp_account_no` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `website` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bank_account` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `curr_code` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_terms` int(11) DEFAULT NULL,
  `tax_included` tinyint(1) NOT NULL DEFAULT '0',
  `dimension_id` int(11) DEFAULT '0',
  `dimension2_id` int(11) DEFAULT '0',
  `tax_group_id` int(11) DEFAULT NULL,
  `credit_limit` double NOT NULL DEFAULT '0',
  `purchase_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payable_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payment_discount_account` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `notes` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_suppliers`
--

INSERT INTO `0_suppliers` (`supplier_id`, `supp_name`, `supp_ref`, `address`, `supp_address`, `gst_no`, `contact`, `supp_account_no`, `website`, `bank_account`, `curr_code`, `payment_terms`, `tax_included`, `dimension_id`, `dimension2_id`, `tax_group_id`, `credit_limit`, `purchase_account`, `payable_account`, `payment_discount_account`, `notes`, `inactive`) VALUES
(1, 'PT. Sun Life Financial Indonesia', 'PT. Sun Life Financial Indones', 'Gd. Menara Sun Life Lt. 12, Jl. Dr. Ide Anak Agung GDE Agung, Kawasan Mega Kuningan Blok 6.3, Kel. Kuningan Timur, Kec. Setabudi, Jakarta Selatan-DKI Jakarta', 'Gd. Menara Sun Life Lt. 12, Jl. Dr. Ide Anak Agung GDE Agung, Kawasan Mega Kuningan Blok 6.3, Kel. Kuningan Timur, Kec. Setabudi, Jakarta Selatan-DKI Jakarta', '01.358.937.9-062.000', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '', '', 0),
(2, 'PT. Bank Pembangunan Daerah Jawa Barat Dan Banten Tbk', 'PT. Bank Pembangunan Daerah Ja', 'Jl. Naripan No. 12-14 Bandung 40111', 'Jl. Naripan No. 12-14 Bandung 40111', '01.118.605.3-054.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(3, 'PT. Indo Premier Securities', 'PT. Indo Premier Securities', 'Wisma GKBI Lt. 7 Suite 718, Jl. Jend. Sudirman No. 28 Jakarta 10210', 'Wisma GKBI Lt. 7 Suite 718, Jl. Jend. Sudirman No. 28 Jakarta 10210', '01.760.267.3-054.000', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '', '', 0),
(4, 'PT. Bareksa Portal Investasi', 'PT. Bareksa Portal Investasi', 'Jl. Bangka Raya No. 27 BLK G-H RT/RW: 004/007 Pela Mampang, Jakarta Selatan', 'Jl. Bangka Raya No. 27 BLK G-H RT/RW: 004/007 Pela Mampang, Jakarta Selatan', '03.278.633.7-014.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(5, 'PT. Asuransi Jiwa Sinarmas MSIG', 'PT. Asuransi Jiwa Sinarmas MSI', 'Jl. Mangga dua raya wisma eka jiwa lt. 8, Mangga dua selatan', 'Jl. Mangga dua raya wisma eka jiwa lt. 8, Mangga dua selatan', '01.391.150.8-073.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(6, 'Persek Prime Consult', 'Persek Prime Consult', 'Gd. Multivision Tower Lt. 3, Jl. Kuningan Mulia Lot 9B, Jakarta Selatan', 'Gd. Multivision Tower Lt. 3, Jl. Kuningan Mulia Lot 9B, Jakarta Selatan', '02.545.560.1-062.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(7, 'Persek. Ardianto &amp; Masniari', 'Persek. Ardianto &amp; Masniar', 'Gd. One Pasific Place Lt.11, SCBD. Jl. Jend. Sudirman Kav. 52-53, Senayan, Jakarta Selatan', 'Gd. One Pasific Place Lt.11, SCBD. Jl. Jend. Sudirman Kav. 52-53, Senayan, Jakarta Selatan', '03.312.841.4-012.000', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '0', '', 0),
(8, 'PT. Multifiling Mitra Indonesia ', 'PT. Multifiling Mitra Indonesi', 'Jl. Akasia 2 BLOK A.7 4A Delta Silicon Ind. Park, Cikarang, Kota Cikarang Utara, Bekasi, Jawa Barat 17630', 'Jl. Akasia 2 BLOK A.7 4A Delta Silicon Ind. Park, Cikarang, Kota Cikarang Utara, Bekasi, Jawa Barat 17630', '01.585.910.1-054.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(9, 'Hasbullah AR, SH', 'Hasbullah AR, SH', 'Gd. Menara Gracia Lt. 5, Jl. HR. Rasuna Said Blok Kav C-17 Karet Kuningan, Setiabudi, Jakarta Selatan, DKI Jakarta Raya 12940', 'Gd. Menara Gracia Lt. 5, Jl. HR. Rasuna Said Blok Kav C-17 Karet Kuningan, Setiabudi, Jakarta Selatan, DKI Jakarta Raya 12940', '07.099.499.1-011.001', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '', '', 0),
(10, 'PT. TMF Indonesia', 'PT. TMF Indonesia', 'Wisma GKBI Lt. 17 Suite 1710, Jl. Jend. Sudirman No. 28, Bendungan Hilir, Tanah Abang, Jakarta Pusat', 'Wisma GKBI Lt. 17 Suite 1710, Jl. Jend. Sudirman No. 28, Bendungan Hilir, Tanah Abang, Jakarta Pusat', '02.624.320.4-071.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(11, 'CV. Saraswati Flora', 'CV. Saraswati Flora', 'Telavera Office Park Lt.28, Jl. TB. Simatupang Kav.22-26, Cilandak, Jaksel.', 'Telavera Office Park Lt.28, Jl. TB. Simatupang Kav.22-26, Cilandak, Jaksel.', '03.018.725.6-016.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(12, 'Gading semesta Transwisata', 'Gading semesta Transwisata', '', '', '', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '11111', '', 0),
(13, 'Indonesia Website Services (Indosite)', 'Indonesia Website Services (In', '', '', '', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '', '', 0),
(14, 'PT. Maxindo Network', 'PT. Maxindo Network', 'Marina Raya RKC Blok H No.77, Bukit Golf Mediterania PIK, Kamal Muara, Jakarta Utara', 'Marina Raya RKC Blok H No.77, Bukit Golf Mediterania PIK, Kamal Muara, Jakarta Utara', '03.040.785.2-047.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(15, 'PT. Artha Telekomindo', 'PT. Artha Telekomindo', 'Control Building, Jl. Jendral Sudirman No. 52-53, Senayan, Jakarta Selatan 12190', 'Control Building, Jl. Jendral Sudirman No. 52-53, Senayan, Jakarta Selatan 12190', '01.614.973.4-062.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(16, 'PT. Tujuh Langit Solusindo', 'PT. Tujuh Langit Solusindo', 'Jl. Danau Toba No. 104, Bendungan Hilir, Jakarta Pusat', 'Jl. Danau Toba No. 104, Bendungan Hilir, Jakarta Pusat', '66.651.642.2-077.000', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '11111', '', 0),
(17, 'PT. Prima Mitra Setia Sejati', 'PT. Prima Mitra Setia Sejati', 'Jl. Harapan Jaya Raya No. Lt. 1, Cempaka Baru - Kemayoran, Jakarta Pusat', 'Jl. Harapan Jaya Raya No. Lt. 1, Cempaka Baru - Kemayoran, Jakarta Pusat', '01.992.576.7-027.000', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '', '', 0),
(18, 'PT. Citra Asri Davindo', 'PT. Citra Asri Davindo', 'Jl. Raya Lenteng Agung No. 39 RT 003 RW 001, Lenteng Agung, Jakarta Selatan', 'Jl. Raya Lenteng Agung No. 39 RT 003 RW 001, Lenteng Agung, Jakarta Selatan', '02.173.981.8-017.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(19, 'PT. Askrindo (Persero)', 'PT. Askrindo (Persero)', '', '', '', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '', '', 0),
(20, 'PT. Indo Human Resource', 'PT. Indo Human Resource', 'Epicentrum Walk Strata Office Suite Lt. 7, Unit 0709A, Komp. Rasuna Epicentru, Jl. HR Rasuna Said, Karet, Kuningan - Setiabudi, Jakarta Selatan', 'Epicentrum Walk Strata Office Suite Lt. 7, Unit 0709A, Komp. Rasuna Epicentru, Jl. HR Rasuna Said, Karet, Kuningan - Setiabudi, Jakarta Selatan', '02.436.412.7-011.000', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '', '', 0),
(21, 'PT. Asuransi Jiwasraya', 'PT. Asuransi Jiwasraya', 'Jl. Juanda Ir.H. No. 34, Kebon Kelapa, Gambir, Jakarta Pusat, DKI Jakarta Raya - 10120', 'Jl. Juanda Ir.H. No. 34, Kebon Kelapa, Gambir, Jakarta Pusat, DKI Jakarta Raya - 10120', '01.001.600.4-093.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(22, 'PT. Limas Indonesia Makmur Tbk', 'PT. Limas Indonesia Makmur Tbk', 'Gedung Plaza Asia lt.22, Jl. Jend Sudirman Kav. 59 Kebayoran Baru, Jakarta Selatan, DKI Jakarta Raya 12190', 'Gedung Plaza Asia lt.22, Jl. Jend Sudirman Kav. 59 Kebayoran Baru, Jakarta Selatan, DKI Jakarta Raya 12190', '01.788.077.4-054.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(23, 'PT. Pemeringkat Efek Indonesia (PEFINDO)', 'PT. Pemeringkat Efek Indonesia', 'Panin Tower Senayan City Lt. 17, Jl. Asia Afrika Lot. 19, Jakarta Pusat', 'Panin Tower Senayan City Lt. 17, Jl. Asia Afrika Lot. 19, Jakarta Pusat', '01.679.104.8-062.000', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '', '', 0),
(24, 'PT. First Jakarta International', 'PT. First Jakarta Internationa', 'Gedung Bursa Efek Indonesia Menara II Lt.15 Lot 2, Jl.Jend.Sudirman Kav. 52-53, Senayan, Kebayoran Baru, Jakarta Selatan, DKI Jakarta Raya', 'Gedung Bursa Efek Indonesia Menara II Lt.15 Lot 2, Jl.Jend.Sudirman Kav. 52-53, Senayan, Kebayoran Baru, Jakarta Selatan, DKI Jakarta Raya', '01.570.102.2-059.000', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '', '', 0),
(25, 'PT. Plaza Auto Prima', 'PT. Plaza Auto Prima', 'Kapten Piere Tendean No. 9A, Kuningan Barat, Jakarta Selatan', 'Kapten Piere Tendean No. 9A, Kuningan Barat, Jakarta Selatan', '02.139.624.7-062.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(26, 'PT. Beng Space Kreatifindo', 'PT. Beng Space Kreatifindo', '', '', '02.703.271.3-003.000', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '', '', 0),
(27, 'PT. Jaringan Intech Indonesia', 'PT. Jaringan Intech Indonesia', '', '', '02.024.464.6-023.000', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '', '', 0),
(30, 'PT. Birotika Semesta', 'PT. Birotika Semesta', '', '', '01.310.774.3-062.000', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '', '', 0),
(31, 'PT. RTI Infokom', 'PT. RTI Infokom', 'Wisma Antara Lt.16 Ruang 1606, Jl. Medan Merdeka Selatan No. 17, Jakarta 10110', 'Wisma Antara Lt.16 Ruang 1606, Jl. Medan Merdeka Selatan No. 17, Jakarta 10110', '01.354.752.6-073.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(32, 'PT. IQ Plus Prima', 'PT. IQ Plus Prima', 'Jl. Bukit Gading Indah Raya Blok H 24, Kelapa Gading Barat, Jakarta Utara', 'Jl. Bukit Gading Indah Raya Blok H 24, Kelapa Gading Barat, Jakarta Utara', '01.949.744.5-043.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(33, 'PT. First Jakarta International', 'PT. First Jakarta Internationa', 'Gedung Bursa Efek Indonesia Menara II Lt.15 Lot 2, Jl.Jend.Sudirman Kav. 52-53, Senayan, Kebayoran Baru, Jakarta Selatan, DKI Jakarta Raya', 'Gedung Bursa Efek Indonesia Menara II Lt.15 Lot 2, Jl.Jend.Sudirman Kav. 52-53, Senayan, Kebayoran Baru, Jakarta Selatan, DKI Jakarta Raya', '01.570.102.2-059.000', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '0', '', 1),
(36, 'PT. Asa Tuai Sejahtera', 'PT. Asa Tuai Sejahtera', 'Jl. Lembah Hibrida Blok I/9 No. 1, Rt.006 Rw.009 Pondok Kelapa, Jakarta Timur', 'Jl. Lembah Hibrida Blok I/9 No. 1, Rt.006 Rw.009 Pondok Kelapa, Jakarta Timur', '02.508.711.5-008.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(37, 'PT. Citra Van Titipan Kilat', 'PT. Citra Van Titipan Kilat', '', '', '01.308.521.2-073.000', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '', '', 0),
(38, 'PT. Praisindo Teknologi', 'PT. Praisindo Teknologi', 'Jl. Prof. Dr. Satrio No. 164 RT 003/ RW 004, Jakarta Selatan', 'Jl. Prof. Dr. Satrio No. 164 RT 003/ RW 004, Jakarta Selatan', '02.174.317.4-063.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(39, 'PT. Andalan Nusantara Teknologi ', 'PT. Andalan Nusantara Teknolog', 'Gd. Intiland Tower Lt. 11, Jl. Jend. Sudirman Kav.32, Karet Tengsin, Jakarta Pusat', 'Gd. Intiland Tower Lt. 11, Jl. Jend. Sudirman Kav.32, Karet Tengsin, Jakarta Pusat', '01.961.867.7-022.000', '', '', '', '', 'IDR', 3, 0, 0, 0, 1, 0, '', '21110', '0', '', 0),
(42, 'Buchari Hanafi', 'Buchari', '', '', '091805812402000', '', '', '', 'BCA', 'IDR', 3, 0, 0, 0, 6, 0, '', '11111', '11111', '', 0),
(44, 'CV. Saraswati Flora IDR', 'CV. Saraswati Flora IDR', 'Telavera Office Park Lt.28, Jl. TB. Simatupang Kav.22-26, Cilandak, Jaksel.', 'Telavera Office Park Lt.28, Jl. TB. Simatupang Kav.22-26, Cilandak, Jaksel.', '03.018.725.6-016.000', '', '', '', '', 'USD', 3, 0, 0, 0, 1, 0, '', '21110', '11111', '', 0),
(45, 'PT. Astra Graphia Xprins Indonesia', 'PT. Astra Graphia Xprins ', 'Jl. Kramat Raya No. 43, Kramat, Jakarta Pusat', 'Jl. Kramat Raya No. 43, Kramat, Jakarta Pusat', '664735024023000', '', '', '', '', 'IDR', 0, 0, 0, 0, 0, 0, '', '0', '0', 'Jl. Kramat Raya No. 43, Kramat, Jakarta Pusat', 0),
(46, 'Leolin Jayayanti, SH', 'Leolin Jayayanti, SH', '', '', '07.121.845.7-064.000', '', '', '', '', 'IDR', 0, 0, 0, 0, 0, 0, '', '0', '0', '', 0),
(47, 'PT. Bumianyar Futuria', 'PT. Bumianyar Futuria', 'Jl. Bima 18, Tanah Tinggi Jakarta Pusat', 'Jl. Bima 18, Tanah Tinggi Jakarta Pusat', '01.566.646.4-024.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(48, 'CV. Kalawatu Prima', 'CV. Kalawatu Prima', 'WTC Mangga Dua Lt. LG Blok C No. 36, Jl. Mangga Dua Raya No. 8, Jakarta Utara', 'WTC Mangga Dua Lt. LG Blok C No. 36, Jl. Mangga Dua Raya No. 8, Jakarta Utara', '03.014.533.8-008.000', '', '', '', '', 'IDR', 0, 0, 0, 0, 0, 0, '', '0', '0', '', 0),
(49, 'PT. Mandiri Sekuritas', 'PT. Mandiri Sekuritas', 'Plaza Mandiri Lt. 28, Jl. Gatoto Subroto Kav. 36-38, Senayan, Jakarta Selatan', 'Plaza Mandiri Lt. 28, Jl. Gatoto Subroto Kav. 36-38, Senayan, Jakarta Selatan', '01.565.217.5-093.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(50, 'PT. Pelangi Global Perkasa', 'PT. Pelangi Global Perkasa', '', '', '010.000-15.96909590', '', '', '', '', 'IDR', 0, 0, 0, 0, 0, 0, '', '0', '0', '', 0),
(51, 'Bloomberg', 'Bloomberg', '', '', '', '', '', '', '', 'IDR', 0, 0, 0, 0, 0, 0, '', '0', '0', '', 0),
(52, 'PT. Vidya Artha Tama', 'PT. Vidya Artha Tama', 'Jl. Raya Bogor No. 56 Km. 19, Kramat Jati, Jakarta Timur', 'Jl. Raya Bogor No. 56 Km. 19, Kramat Jati, Jakarta Timur', '02.173.410.8-005.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(53, 'PT. Top Cars Gallery', 'PT. Top Cars Gallery', 'Jl. Suryopranotono 10 RT 000 RW 000, Petojo Utara, Gambir, Jakarta Pusat', 'Jl. Suryopranotono 10 RT 000 RW 000, Petojo Utara, Gambir, Jakarta Pusat', '31.245.944.9-029.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(54, 'Perhimpunan Pedagang Surat Utang Negara', 'Perhimpunan Pedagang ', 'Gd. Lina Lantai IV Ruang 411, Jl. H.R Rasuna Said Kav. B-7, Setiabudi, Jakarta Selatan, DKI Jakarta', 'Gd. Lina Lantai IV Ruang 411, Jl. H.R Rasuna Said Kav. B-7, Setiabudi, Jakarta Selatan, DKI Jakarta', '02.285.284.2-054.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(55, 'PT. Lotus Andalan Sekuritas', 'PT. Lotus Andalan Sekuritas', 'Wisma KEIAI Lt. 15, Jl, Jend. Sudirman Kav.m 3, Karet Tengsin, Karet Tengsin Tanah Abang, Jakarta Pusat, DKI Jakarta', 'Wisma KEIAI Lt. 15, Jl, Jend. Sudirman Kav.m 3, Karet Tengsin, Karet Tengsin Tanah Abang, Jakarta Pusat, DKI Jakarta', '01.359.131.8-054.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(56, 'PT. Duta Distribusi Servisindo', 'PT. Duta Distribusi Servisindo', 'Gd. RoxySquare Lt. Ground Blok B07 No. 01, Jl. Kyai Tapa No. 1, Jakarta Barat', 'Gd. RoxySquare Lt. Ground Blok B07 No. 01, Jl. Kyai Tapa No. 1, Jakarta Barat', '31.325.943.4-036.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(57, 'PT. Sinar Elok Abadi', 'PT. Sinar Elok Abadi', 'Jl. Kesehatan No. 60.B RT 002 RW 004, Jakarta Pusat', 'Jl. Kesehatan No. 60.B RT 002 RW 004, Jakarta Pusat', '317103109028000', '', '', '', '', 'IDR', 0, 0, 0, 0, 0, 0, '', '0', '0', '', 0),
(58, 'Yunarto Wijaya', 'Yunarto Wijaya', 'Jl. Cipinang Elok 2, Blok AQ 12, RT 13 RW 10, Cipinang Muara, Jatinegara, Jakarta Timur', 'Jl. Cipinang Elok 2, Blok AQ 12, RT 13 RW 10, Cipinang Muara, Jatinegara, Jakarta Timur', '248916397002000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(59, 'PT. Pacific Place Jakarta', 'PT. Pacific Place Jakarta', 'Jl. Jenderal Sudirman Kav. 52-53, Jakarta Selatan 12190', 'Jl. Jenderal Sudirman Kav. 52-53, Jakarta Selatan 12190', '0000000000000000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(60, 'Drs. Budiarto Shambazy, MA', 'Drs. Budiarto Shambazy, MA', 'Jl. Soka Lestari 3/BLok F No. 31, RT 005/RW 007, Lebak Bulus, Cilandak, Jakarta Selatan 12440', 'Jl. Soka Lestari 3/BLok F No. 31, RT 005/RW 007, Lebak Bulus, Cilandak, Jakarta Selatan 12440', '489033001016000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(61, 'Robby Ubaidillah', 'Robby Ubaidillah', '', '', '767409998031000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(62, 'Oktofin Ridiarsih', 'Oktofin Ridiarsih', 'Jl. Kebalen VII No. 10 RT 07 RW 05 Rawa Barat - Kebayoran Baru Jakarta Selatan', '', '970313300012000', '', '', '', '', 'IDR', 0, 0, 0, 0, 0, 0, '', '0', '0', '', 0),
(63, 'PT. Pola Aneka Sejahtera', 'PT. Pola Aneka Sejahtera', 'Komp. Pertokoan Sunrise Garden Blok A2 No. 5 RT 002 RW 005, Kedoya Utara, Kebon Jeruk, Jakarta Barat, DKI Jakarta Raya 11520', 'Komp. Pertokoan Sunrise Garden Blok A2 No. 5 RT 002 RW 005, Kedoya Utara, Kebon Jeruk, Jakarta Barat, DKI Jakarta Raya 11520', '013825112039000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(64, 'Lestari Witias Asih', 'Lestari Witias Asih', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(65, 'Najwa Shihab', 'Najwa Shihab', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(66, 'Lani', 'Lani', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(67, 'Hilda Rahmawita', 'Hilda Rahmawita', '', '', '45.751.021.2-045.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(68, 'Yoga Pradipta Ramadhan', 'Yoga Pradipta Ramadhan', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(69, 'Maria Margaretha Maru', 'Maria Margaretha Maru', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(70, 'Leolin Jayayanti, SH', 'Leolin Jayayanti, SH', '', '', '07.121.845.7-064.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(71, 'Habibi Hadi Wibowo', 'Habibi Hadi Wibowo', '', '', '25.766.222.1-009.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(72, 'Chaky Production', 'Chaky Production', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(73, 'Bubble Production', 'Bubble Production', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(74, 'Fuad', 'Fuad', '', '', '49.914.132.3-008.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(75, 'Achmad Subchi', 'Achmad Subchi', '', '', '24.641.917.0-045-000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(76, 'Lina Yusi Anggrawati', 'Lina Yusi Anggrawati', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(77, 'Moh Khoirul Anwar', 'Moh Khoirul Anwar', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(78, 'Cak Budi', 'Cak Budi', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(79, 'Fajar Rachmat Hidajat', 'Fajar Rachmat Hidajat', '', '', '49.716.769.2-411.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(80, 'Leolin Jayanti, SH', 'Leolin Jayanti, SH', '', '', '07.121.845.7-064.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(82, 'Trys Hartono Tandiono', 'Trys Hartono Tandiono', '', '', '44.435.918.6-113.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(83, 'Robi Kusadri', 'Robi Kusadri', '', '', '44.224.503.1-615.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(84, 'Riki A', 'Riki A', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(85, 'Kuntoro', 'Kuntoro', '', '', '71.480.087.7-086.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(86, 'Surya Anugrah ST', 'Surya Anugrah ST', '', '', '78.150.236.4-609.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(87, 'Ayu Nurul Huda ', 'Ayu Nurul Huda ', '', '', '71.071.924.6-013.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(88, 'Fajar Rachmat Hidajat', 'Fajar Rachmat Hidajat', '', '', '49.716.769.2-411.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(89, 'Tatag Adi Sasono', 'Tatag Adi Sasono', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(90, 'Devy Anggraini', 'Devy Anggraini', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(91, 'Crisesa Kinanti Ramadhanti', 'Crisesa Kinanti Ramadhanti', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(92, 'Marselina', 'Marselina', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(93, 'Asmaul Husna', 'Asmaul Husna', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(94, 'Moh Sulthony Yahya', 'Moh Sulthony Yahya', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(95, 'Firhan Hidayat', 'Firhan Hidayat', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(96, 'Showfil Widad Herdiana', 'Showfil Widad Herdiana', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(97, 'Fariz P K', 'Fariz P K', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(98, 'Maria Debora Siallagan', 'Maria Debora Siallagan', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(99, 'Virginia Aurelia', 'Virginia Aurelia', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(100, 'Fadil Muhammad Irham', 'Fadil Muhammad Irham', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(101, 'Wawan Riswandi', 'Wawan Riswandi', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(102, 'Joko Widodo', 'Joko Widodo', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(103, 'Yusuf', 'Yusuf', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(104, 'Kuntoro', 'Kuntoro', '', '', '71.480.087.7-086.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(105, 'Rudi Siswantoro', 'Rudi Siswantoro', '', '', '57.565.897.6-013.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(106, 'Sinar 99', 'Sinar 99', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(107, 'Atiya Nurul Jusih Dani', 'Atiya Nurul Jusih Dani', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(108, 'Itqon Harokah Harahap', 'Itqon Harokah Harahap', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(109, 'Atma Dewita', 'Atma Dewita', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(110, 'Ike Rosana Astuti AMD.SE', 'Ike Rosana Astuti AMD.SE', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(111, 'M. Andri Zumain', 'M. Andri Zumain', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(112, 'Darsono', 'Darsono', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(113, 'Togi Pangaribuan S.H., LL. M', 'Togi Pangaribuan S.H., LL. M', '', '', '24.904.020.5-023.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(114, 'Karsid', 'Karsid', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(115, 'Herman', 'Herman', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(116, 'Yudi Arpan', 'Yudi Arpan', '', '', '59.087.403.8-016.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(117, 'Pandam Kuntaswari', 'Pandam Kuntaswari', '', '', '64.114.674.1-013.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(118, 'Gede Sinaya', 'Gede Sinaya', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(119, 'Deselfod D Manno', 'Deselfod D Manno', '', '', '47.205.779.3-042.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(120, 'Sirla', 'Sirla', '', '', '16.365.634.1-301.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(121, 'Wilbert Sembiring', 'Wilbert Sembiring', '', '', '35.727.023.0-421.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(122, 'Nur Anita Rahmawati', 'Nur Anita Rahmawati', '', '', '79.283.104.2-201.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(123, 'Siti Mahmudah', 'Siti Mahmudah', '', '', '66.865.377.7-407.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(124, 'Febry Olivia', 'Febry Olivia', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(125, 'Riza Lisdiyanti Devi', 'Riza Lisdiyanti Devi', '', '', '09.698.602.1-412.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(127, 'Andre Farid Zulkarnain', 'Andre Farid Zulkarnain', '', '', '24.618.069.9-432.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(128, 'Kevin Denowarsyah Widaya', 'Kevin Denowarsyah Widaya', '', '', '80.121.650.8-453.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(129, 'Astry Novita Sutono', 'Astry Novita Sutono', '', '', '54.622.415.5-008.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(130, 'Rini Yulianti, SH', 'Rini Yulianti, SH', '', '', '09.613.783.1-016.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(131, 'CV. Bali Oke', 'CV. Bali Oke', 'Bukit Pratama Jl. Gong Kebyar No. 42, Jimbaran-Kuta Selatan, Badung-Bali 80361', 'Bukit Pratama Jl. Gong Kebyar No. 42, Jimbaran-Kuta Selatan, Badung-Bali 80361', '02.787.140.9-905.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(132, 'PT. Danareksa Sekuritas', 'PT. Danareksa Sekuritas', 'Jl Medan Merdeka Selatan No 14 Jakarta Pusat 10110', 'Jl Medan Merdeka Selatan No 14 Jakarta Pusat 10110', '01.595.706.1-054.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(133, 'PT. Indo Premier Sekuritas', 'PT. Indo Premier Sekuritas', 'Wisma GKBI Lt. 7 Suite 718, Jl. Jend. Sudirman Kav. 28, Bendungan Hilir Tanah Abang, Jakarta Pusat', 'Wisma GKBI Lt. 7 Suite 718, Jl. Jend. Sudirman Kav. 28, Bendungan Hilir Tanah Abang, Jakarta Pusat', '01.760.267.3-054.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(134, 'PT. Penilai Harga Efek Indonesia', 'PT. Penilai Harga Efek Indones', '0', '0', '02.741.857.3-063.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(135, 'Persek. Ardianto &amp; Masniari', 'Persek. Ardianto &amp; Masniar', 'Gd. One Pasific Place Lt.11, SCBD. Jl. Jend. Sudirman Kav. 52-53, Senayan, Jakarta Selatan', 'Gd. One Pasific Place Lt.11, SCBD. Jl. Jend. Sudirman Kav. 52-53, Senayan, Jakarta Selatan', '03.312.841.4-012.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '0', '0', '', 0),
(136, 'PT. Astra Graphia Tbk', 'PT. Astra Graphia Tbk', 'Jl. Kramat Raya No. 43, Kramat, Jakarta Pusat', 'Jl. Kramat Raya No. 43, Kramat, Jakarta Pusat', '01.307.261.6-054.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(137, 'PT Arkacipta Global Media', 'PT Arkacipta Global Media', 'Jl. Hartono Raya Ruko Yellow Tower The Apartment Golf BA No 1, Rt. Rw., Kel. Kelapa Indah, Kec. Tangerang, Banten', 'Jl. Hartono Raya Ruko Yellow Tower The Apartment Golf BA No 1, Rt. Rw., Kel. Kelapa Indah, Kec. Tangerang, Banten', '72.729.967.9-416.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(138, 'Dana Pensiun BTN', 'Dana Pensiun BTN', '00.000.000.0-000.000', '00.000.000.0-000.000', '00.000.000.0-000.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(139, 'PT. Lestari Karya Gemilang', 'PT. Lestari Karya Gemilang', 'Jl. Tebet timur Dalam raya No. 79 RT.001 RW 006 Tebet Timur Tebet Jakarta Selatan 12820', 'Jl. Tebet timur Dalam raya No. 79 RT.001 RW 006 Tebet Timur Tebet Jakarta Selatan 12820', '03.200.602.5-015.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(140, 'CV. Prima Trans Nusa', 'CV. Prima Trans Nusa', 'Jl. BTP Blok A Bo. 620A, Tamalanrea, Makassar', 'Jl. BTP Blok A Bo. 620A, Tamalanrea, Makassar', '03.280.841.2-801.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(141, 'Johan Malonda Mustika &amp; Rekan', 'Johan Malonda Mustika &amp; Re', 'Jl. Pluit Raya 200 Blok V No. 1-5 RT 016 RW 008, Jakarta Utara', 'Jl. Pluit Raya 200 Blok V No. 1-5 RT 016 RW 008, Jakarta Utara', '02.574.849.2-041.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(142, 'CV. Global Zone Group', 'CV. Global Zone Group', 'Gambiran UH.5/359 RT/RW: 043/011, Pandeyan Umbulharjo Yogyakarta', 'Gambiran UH.5/359 RT/RW: 043/011, Pandeyan Umbulharjo Yogyakarta', '02.961.454.2-541.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(143, 'Perhimpunan Pensiunan Pertamina (HIMPANA)', 'Perhimpunan Pensiunan Pertamin', 'Jl. Medan Merdeka Timur No.11 Gambir, Jakarta Pusat', 'Jl. Medan Merdeka Timur No.11 Gambir, Jakarta Pusat', '02.553.884.4-025.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(144, 'Yayasan IKAFE Unand', 'Yayasan IKAFE Unand', 'Pusat Niaga Cempaka Mas Blok M1 No. 36, Jl. Letjend Suprapto, Kemayoran', 'Pusat Niaga Cempaka Mas Blok M1 No. 36, Jl. Letjend Suprapto, Kemayoran', '81.400.382.8-027.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(145, 'PT. Bank Victoria International Tbk', 'PT. Bank Victoria Internationa', '00.000.000.0-000.000', '00.000.000.0-000.000', '00.000.000.0-000.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(146, 'BEM FEB UI', 'BEM FEB UI', '00.000.000.0-000.000', '00.000.000.0-000.000', '00.000.000.0-000.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(147, 'BPJS Ketenagakerjaan Futsal Challenge (BFC)', 'BPJS Ketenagakerjaan Futsal Ch', '00.000.000.0-000.000', '00.000.000.0-000.000', '00.000.000.0-000.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(148, 'PT. Heritage Amanah International', 'PT. Heritage Amanah Internatio', 'Gd. Bursa Efek Indonesia Tower 1 Lt. 28 SUIT 2801, Jl. Jend Sudirman Kav 52-53, Senayan, Kebayoran Baru, Jakarta Selatan', 'Gd. Bursa Efek Indonesia Tower 1 Lt. 28 SUIT 2801, Jl. Jend Sudirman Kav 52-53, Senayan, Kebayoran Baru, Jakarta Selatan', '75.616.626.0-012.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(149, 'PT. Kamadjaja Logistic', 'PT. Kamadjaja Logistic', 'Jl. Kalianak Barat No. 66, Kalianak-Asemrowo, Surabaya, Jawatimur', 'Jl. Kalianak Barat No. 66, Kalianak-Asemrowo, Surabaya, Jawatimur', '01.544.448.2-631.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(150, 'PT. Nusantara Sejahtera Investama', 'PT. Nusantara Sejahtera Invest', 'Batu Tulis Raya No. 24A RT 03 RW 02, Kebon Kelapa, Gambir, Jakarta Pusat', 'Batu Tulis Raya No. 24A RT 03 RW 02, Kebon Kelapa, Gambir, Jakarta Pusat', '74.804.154.8-074.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(151, 'PT. Buana Capital Sekuritas', 'PT. Buana Capital Sekuritas', 'Gedung Bursa Efek Indonesia, Tower II Lt. 26, Jl. Jend. Sudirman Kav. 52-53, Senayan, Jakarta Selatan', 'Gedung Bursa Efek Indonesia, Tower II Lt. 26, Jl. Jend. Sudirman Kav. 52-53, Senayan, Jakarta Selatan', '01.347.694.0-054.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(152, 'FA KJPP Martokoesoemo, Prasetyo &amp; Rekan', 'FA KJPP Martokoesoemo, Prasety', 'Gd. Plaza Abda Lt 10-11 Jl. Jend Sudirman Kav 59, Jakarta Selatan', 'Gd. Plaza Abda Lt 10-11 Jl. Jend Sudirman Kav 59, Jakarta Selatan', '02.596.830.6-012.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(153, 'CV. Riau Prima Mandiri', 'CV. Riau Prima Mandiri', 'Ko Seirama Village Blok A No. 1 RT 03/ RW 07, Sidomulyo Timur Marpoyan Damai, Kota Pekan Baru Riau, 28125', 'Ko Seirama Village Blok A No. 1 RT 03/ RW 07, Sidomulyo Timur Marpoyan Damai, Kota Pekan Baru Riau, 28125', '75.235.490.2-216.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(154, 'CV. Wisata Jatim', 'CV. Wisata Jatim', 'Graha Tirta Akasia 29 RT 001 RW 014, Kureksari, Waru - Sidoardjo', 'Graha Tirta Akasia 29 RT 001 RW 014, Kureksari, Waru - Sidoardjo', '03.200.396.4-643.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(155, 'Persekutuan Perdata Armand Yapsunto Muharamsyah &amp; Partne', 'Persekutuan Perdata Armand Yap', 'Gd. Permata Kuningan Lt.25 (Penthouse), Jl. Kuningan Mulia Kav. 9C Guntur Setiabudi. Jakarta Selatan, DKI Jakarta', 'Gd. Permata Kuningan Lt.25 (Penthouse), Jl. Kuningan Mulia Kav. 9C Guntur Setiabudi. Jakarta Selatan, DKI Jakarta', '02.183.303.3-018.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(156, 'Perkumpulan Pelaku Reksadana dan Investasi Indonesia', 'Perkumpulan Pelaku Reksadana d', 'Gedung Artha Graha Lantai 31, Jl. Jend Sudirman Kav 52-53, Senayan, Kebayoran Baru, Jakarta Selatan, DKI Jakarta', 'Gedung Artha Graha Lantai 31, Jl. Jend Sudirman Kav 52-53, Senayan, Kebayoran Baru, Jakarta Selatan, DKI Jakarta', '02.579.267.2-019.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(157, 'Yay. Bakti Bank Central Asia', 'Yay. Bakti Bank Central Asia', 'Wisma Asia Lt. 16 Jl. Letjend S Parman Kav.79 Kel. Kota Bambu Selatan, Kec. Palmerah Jakarta Barat, DKI Jakarta Raya 11420', 'Wisma Asia Lt. 16 Jl. Letjend S Parman Kav.79 Kel. Kota Bambu Selatan, Kec. Palmerah Jakarta Barat, DKI Jakarta Raya 11420', '02.262.067.8-031.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(158, 'Pers. Perkumpulan Pensiunan Bank Indonesia', 'Pers. Perkumpulan Pensiunan Ba', 'Jl. Rasamala Raya No. 2 Menteng Dalam, Tebet, Jakarta Selatan', 'Jl. Rasamala Raya No. 2 Menteng Dalam, Tebet, Jakarta Selatan', '01.792.022.4-015.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(159, 'PL The Institute Of Internal Auditors Indonesia Chapter ', 'PL The Institute Of Internal A', 'Gd. S. Widjojo Lt. 4 Jl. Jend. Sudirman Kav. 71 Kebayoran Baru, Jakarta Selatan, DKI Jakarta Raya 12190', 'Gd. S. Widjojo Lt. 4 Jl. Jend. Sudirman Kav. 71 Kebayoran Baru, Jakarta Selatan, DKI Jakarta Raya 12190', '02.742.203.9-012.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(160, 'PT. Niji Promo ', 'PT. Niji Promo ', 'Jl. Kelapa No. 19 RT 005/ RW 09, Rawamangun- Pulo', 'Jl. Kelapa No. 19 RT 005/ RW 09, Rawamangun- Pulo', '02.458.685.1-023.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(161, 'Badan Eksekutif Mahasiswa FE UI', 'Badan Eksekutif Mahasiswa FE U', '0', '0', '00.000.000.0-000.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(162, 'Yay. Dana Pensiun Karyawan Bank Bukopin', 'Yay. Dana Pensiun Karyawan Ban', 'Jl. Prof. Dr. Soepomo No. 176 D, Menteng Dalam, Tebet, Jakarta Selatan', 'Jl. Prof. Dr. Soepomo No. 176 D, Menteng Dalam, Tebet, Jakarta Selatan', '01.332.556.8-015.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(163, ' UI Biofest', ' UI Biofest', '0', '0', '00.000.000.0-000.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(164, 'PT. Sentra Jasa Aktuaria', 'PT. Sentra Jasa Aktuaria', 'Jl. RC. Veteran No. 11 B, Bintaro, Jakarta Selatan', 'Jl. RC. Veteran No. 11 B, Bintaro, Jakarta Selatan', '01.996.856.9-013.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(165, 'KAP Warnoyo dan Mennix', 'KAP Warnoyo dan Mennix', 'Ruko Ifolia HY 46 No. 11  Harapan Indah - Bekasi Utara 17214', 'Ruko Ifolia HY 46 No. 11  Harapan Indah - Bekasi Utara 17214', '70.745.617.4-006.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(166, 'PT. Reasuransi Nasional Indonesia', 'PT. Reasuransi Nasional Indone', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(167, 'Asosiasi Dana Pensiun Indonesia', 'Asosiasi Dana Pensiun Indonesi', 'Gd. Arthaloka Lt.16, Jl. Jend. Sudirman Kav. 2', 'Gd. Arthaloka Lt.16, Jl. Jend. Sudirman Kav. 2', '02.426.504.3-022.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(168, 'PL Kantor Akuntan Publik Doli, Bambang, Sulistiyanto, Dadang, &amp; Ali', 'PL Kantor Akuntan Publik Doli,', 'Jl. Mampang Prapatan VIII No. R 25 B, Jakarta Selatan', 'Jl. Mampang Prapatan VIII No. R 25 B, Jakarta Selatan', '01.980.796.5-014.001', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '0', '0', '', 0),
(169, 'PT. Arka Cipta Global Media', 'PT. Arka Cipta Global Media', 'Jl. Hartono Raya Ruko Yellow Tower The Apartment Golf BA No 1, Rt. Rw., Kel. Kelapa Indah, Kec. Tangerang, Banten', 'Jl. Hartono Raya Ruko Yellow Tower The Apartment Golf BA No 1, Rt. Rw., Kel. Kelapa Indah, Kec. Tangerang, Banten', '72.729.967.9-416.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(170, 'Yayasan Bina Sepakbola Asa Utama', 'Yayasan Bina Sepakbola Asa Uta', 'Jl. Purnawarman No. 1 RT 06 RW 016, Pisangan Ciputat Timur', 'Jl. Purnawarman No. 1 RT 06 RW 016, Pisangan Ciputat Timur', '80.732.504.8-453.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(171, 'PT. Primatama Mandiri Lestari', 'PT. Primatama Mandiri Lestari', 'Jl. Harapan Jaya Raya No. 46, Cempaka Baru - Kemayoran, Jakarta Pusat', 'Jl. Harapan Jaya Raya No. 46, Cempaka Baru - Kemayoran, Jakarta Pusat', '03.031.432.2-027.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(172, 'Telkom Golf Community', 'Telkom Golf Community', '0', '0', 'Tidak Ada NPWP', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(173, 'PT. Master Web Network', 'PT. Master Web Network', 'Kuningan Barat No. 08, Gd. Elektrindo Lt. 10', 'Kuningan Barat No. 08, Gd. Elektrindo Lt. 10', '02.160.756.9-014.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(174, 'PT. Indonesia Capital Market Electronic Library', 'PT. Indonesia Capital Market E', 'Jl. Jend. Sudirman Kav. 52-53, Gd. BEI Menara II Lt. 1, Senayan, Kebayoran Baru', 'Jl. Jend. Sudirman Kav. 52-53, Gd. BEI Menara II Lt. 1, Senayan, Kebayoran Baru', '03.163.151.8-012.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(175, 'PT. Sentra Jasa Aktuaria ', 'PT. Sentra Jasa Aktuaria ', 'Jl. RC Veteran No.11 F Bintaro, Jakarta 12330', 'Jl. RC Veteran No.11 F Bintaro, Jakarta 12330', '01.996.856.9-013.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(176, 'Yay. Kesejahteraan Kary. Bank Indonesia', 'Yay. Kesejahteraan Kary. Bank ', 'Komp. Bidakara I, Jl. Deposito VI No. 12-14, Menteng Dalam, Tebet, Jakarta Selatan, DKI Jakarta', 'Komp. Bidakara I, Jl. Deposito VI No. 12-14, Menteng Dalam, Tebet, Jakarta Selatan, DKI Jakarta', '01.585.723.8-062.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(177, 'PT. Binaman Utama', 'PT. Binaman Utama', 'Jl. Menteng Raya No. 9 Kebun Sirih, Jakarta Pusat', 'Jl. Menteng Raya No. 9 Kebun Sirih, Jakarta Pusat', '01.304.219.7-073.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(178, 'Yayasan Badan Pengelola Kesejahteraan Krakatau Steel', 'Yayasan Badan Pengelola Keseja', 'Jl. KH. Yasin Beji No 29-33 RT 05 RW 01, Cilegon - Banten', 'Jl. KH. Yasin Beji No 29-33 RT 05 RW 01, Cilegon - Banten', '31.174.188.8-417.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(179, 'Badan Pembina Kerohanian Islam Perum Jamkrindo', 'Badan Pembina Kerohanian Islam', '', '', 'NON NPWP', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(180, 'PT. Agranet Multicitra Siberkom', 'PT. Agranet Multicitra Siberko', 'Gedung Trans Media Lt. 8, Jl. Kapten Tendean Kav. 12-14A, Jakarta Selatan', 'Gedung Trans Media Lt. 8, Jl. Kapten Tendean Kav. 12-14A, Jakarta Selatan', '01.761.556.8-058.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(181, 'PT. Tugu Reasuransi Indonesia', 'PT. Tugu Reasuransi Indonesia', 'Jl. Raden Saleh No. 50, Cikini, Menteng', 'Jl. Raden Saleh No. 50, Cikini, Menteng', '01.325.698.7-073.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(182, 'PT. Asuransi Jiwa Tugu Mandiri', 'PT. Asuransi Jiwa Tugu Mandiri', 'Wisma Tugu Lt 1-2, Raden Saleh No. 44', 'Wisma Tugu Lt 1-2, Raden Saleh No. 44', '01.394.517.5-071.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(183, 'PT. Trimitra Multi Kreasi', 'PT. Trimitra Multi Kreasi', 'De Market, Jl. Boulevard Grand City Blok B No. 5 RT 01/RW 04, Tirtajaya Sukmajaya, Kota Depok, Jawa Barat 16412', 'De Market, Jl. Boulevard Grand City Blok B No. 5 RT 01/RW 04, Tirtajaya Sukmajaya, Kota Depok, Jawa Barat 16412', '71.734.099.6-412.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(184, 'Perkumpulan Klub Alumni Padjajaran', 'Perkumpulan Klub Alumni Padjaj', 'Gd. BEI, Tower II Lt. 17, SCBD, Jl. Widya Chandra V, Senayan, Kebayoran Baru', 'Gd. BEI, Tower II Lt. 17, SCBD, Jl. Widya Chandra V, Senayan, Kebayoran Baru', '74.539.416.3-012.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(185, 'PT. Nirwana Zhaktan Edventures', 'PT. Nirwana Zhaktan Edventures', 'Komp. Ruko Citra Nusa Niaga Blok D2 No.17 Batu Besar, Kota Batam Kepulauan Riau', 'Komp. Ruko Citra Nusa Niaga Blok D2 No.17 Batu Besar, Kota Batam Kepulauan Riau', '81.105.323.0-215.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(186, 'PT. Finera Prosperindo', 'PT. Finera Prosperindo', 'Gedung Menara Rajawali Lt. 7-1, Jl. Dr. Ide Anak Agung Gde Agung Lot #5.1 Kawasan Mega Kuningan, Kuningan Timur, Setiabudi, Jakarta Selatan 12950', 'Gedung Menara Rajawali Lt. 7-1, Jl. Dr. Ide Anak Agung Gde Agung Lot #5.1 Kawasan Mega Kuningan, Kuningan Timur, Setiabudi, Jakarta Selatan 12950', '03.278.029.8-063.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(187, 'Kantor Notaris IR. Nanette Cahyanie Hendari Adi Warsito', 'Kantor Notaris IR. Nanette Cah', 'Jl. Panglima Polim V No. 11, Jakarta Selatan', 'Jl. Panglima Polim V No. 11, Jakarta Selatan', '08.091.329.6-064.001', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(188, 'PT. Aliansi Mitra Mandiri', 'PT. Aliansi Mitra Mandiri', 'Jl. Pramuka Raya III No.33 A RT 04/ RW 003, Kayu Manis, Matraman', 'Jl. Pramuka Raya III No.33 A RT 04/ RW 003, Kayu Manis, Matraman', '01.749.356.0-001.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(189, 'PT. Ananda Citta Amarapura', 'PT. Ananda Citta Amarapura', 'Prof. Soepomo SH No. 55 D Blok Z Persil No. 25 RT 13 RW 003, Tebet Barat', 'Prof. Soepomo SH No. 55 D Blok Z Persil No. 25 RT 13 RW 003, Tebet Barat', '66.697.670.9-015.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(190, 'PT. Atrindo Asia Global', 'PT. Atrindo Asia Global', 'Wisma Pede Lt. 5 Unit No. B 506 Jl. MT Haryono Kav. 17 RT 010 RW 005', 'Wisma Pede Lt. 5 Unit No. B 506 Jl. MT Haryono Kav. 17 RT 010 RW 005', '01.885.406.7-013.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(191, 'CV. Semberani Nusantara', 'CV. Semberani Nusantara', 'DK Krasak RT 002 RW 003, Rowosari, Tembalang, Kota Semarang, Jawa Tengah', 'DK Krasak RT 002 RW 003, Rowosari, Tembalang, Kota Semarang, Jawa Tengah', '73.470.499.2-517.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(192, 'CV. Indonesia Website Services', 'CV. Indonesia Website Services', 'Taman Narogong Indah Blok C 23 No.5 RT/RW:007/013 Pengasinan, Rawa Lumbu, Kotamadya Bekasi', 'Taman Narogong Indah Blok C 23 No.5 RT/RW:007/013 Pengasinan, Rawa Lumbu, Kotamadya Bekasi', '01.876.151.0-432.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(193, 'PT. Jobs DB Indonesia', 'PT. Jobs DB Indonesia', 'Prudential Tower, 18th Floor, Jl. Jendral Sudirman Kav. 79, Jakarta 12910', 'Prudential Tower, 18th Floor, Jl. Jendral Sudirman Kav. 79, Jakarta 12910', '01.957.706.3-058.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(194, 'PT. Arkacipta Global Media', 'PT. Arkacipta Global Media', 'Jl. Hartono Raya Ruko Yellow Tower The Apartment Golf BA No 1, Rt. Rw., Kel. Kelapa Indah, Kec. Tangerang, Banten', 'Jl. Hartono Raya Ruko Yellow Tower The Apartment Golf BA No 1, Rt. Rw., Kel. Kelapa Indah, Kec. Tangerang, Banten', '72.729.967.9-416.000', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0),
(195, 'PT. Kualasagi Prima', 'PT. Kualasagi Prima', 'Jl. Raya Perjuangan No. 21, Gd. Sastra Graha Lt.5, Kebon Jeruk, Jakarta Barat', 'Jl. Raya Perjuangan No. 21, Gd. Sastra Graha Lt.5, Kebon Jeruk, Jakarta Barat', '018256511035000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(196, 'PT Cyberindo Mega Persada', 'PT Cyberindo Mega Persada', 'Cyber 2 Tower Lantai 33, Jl. H.R. Rasuna Said Blok X-5 No. 13, Kuningan Timur, Setiabudi, Jakarta Selatan', 'Cyber 2 Tower Lantai 33, Jl. H.R. Rasuna Said Blok X-5 No. 13, Kuningan Timur, Setiabudi, Jakarta Selatan', '032779977063000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(197, 'Ferry Khusaeri Lauw', 'Ferry Khusaeri Lauw', 'Taman Ratu Indah BL.I.4 No. 3, Kedoya Selatan-Kebon Jeruk, Jakarta Barat', 'Taman Ratu Indah BL.I.4 No. 3, Kedoya Selatan-Kebon Jeruk, Jakarta Barat', '494620297039000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(198, 'PT. Biometrik Citra Solusi', 'PT. Biometrik Citra Solusi', 'Jl. Mangga dua raya, Mangga dua Mall Lt.4 Blok B-11, Jakarta Pusat', '', '026701615026001', '', '', '', '', 'IDR', 0, 0, 0, 0, 0, 0, '', '0', '0', '', 0),
(199, 'Dewan Syariah Nasional', 'Dewan Syariah Nasional', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(200, 'PT. Bank Negara Indonesia (Persero) Tbk', 'PT. Bank Negara Indonesia (Per', 'Jl. Jend Sudirman Kav. 1 Jakarta 10220', 'Jl. Jend Sudirman Kav. 1 Jakarta 10220', '01.001.606.1.093.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(201, 'PT. MARULI ARIO TAMPUBOLON', 'PT. MARULI ARIO TAMPUBOLON', 'Wisma MRA Lt. 10, Jl. TB Simatupang No. 19 RT 007 RW 009, Cilandak Barat, Cilandak, Jakarta Selatan, DKI Jakarta', 'Wisma MRA Lt. 10, Jl. TB Simatupang No. 19 RT 007 RW 009, Cilandak Barat, Cilandak, Jakarta Selatan, DKI Jakarta', '74.069.526.7-016.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(202, 'Aditya Krishna Mayandrs', 'Aditya Krishna Mayandrs', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(203, 'PT. HIGIENIS INDONESIA', 'PT. HIGIENIS INDONESIA', 'Gd. Plaza Permata LT. 7, Jl. M.H Thamrin No. 57 RT 009 RW 005, Gondangdia, Jakarta Pusat', 'Gd. Plaza Permata LT. 7, Jl. M.H Thamrin No. 57 RT 009 RW 005, Gondangdia, Jakarta Pusat', '02.275.417.0-076.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(204, 'Kantor Akuntan Publik Widianto &amp; Sumbogo', 'Kantor Akuntan Publik Widianto', 'Jl. Raya Kalimalang Blok E, No 4F, RT 002 RW 016, Jakarta Timur', 'Jl. Raya Kalimalang Blok E, No 4F, RT 002 RW 016, Jakarta Timur', '741626113008000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(205, 'PT. Karunia Artha Pratama', 'PT. Karunia Artha Pratama', 'Ged. Office 8 Lt. Basement 1, SCBD Lot 28, Jl. Widya Chandra V, Senayan, Kebayoran Baru, Jakarta Selatan, DKI Jakarta', 'Ged. Office 8 Lt. Basement 1, SCBD Lot 28, Jl. Widya Chandra V, Senayan, Kebayoran Baru, Jakarta Selatan, DKI Jakarta', '840706279012000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(206, 'Pembayaran Pajak', 'Pembayaran Pajak', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(207, 'Orang Pribadi', 'Orang Pribadi', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(208, 'CV Hascaryo Auto Benjaya', 'CV Hascaryo Auto Benjaya', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(209, 'PT. Enkamanunggal MandiriTour', 'PT. Enkamanunggal MandiriTour', 'Gading Bukit Indah Blok G No. 07, Kelapa Gading Barat, Jakarta Utara', 'Gading Bukit Indah Blok G No. 07, Kelapa Gading Barat, Jakarta Utara', '01.735.218.8-043.000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(210, 'PT Asuransi Bintang Tbk', 'PT Asuransi Bintang Tbk', 'Jl. RS. Fatmawati No. 32, Cilandak Barat - Cilandak, Jakarta Selatan 12430', 'Jl. RS. Fatmawati No. 32, Cilandak Barat - Cilandak, Jakarta Selatan 12430', '01306897805400', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(211, 'PT Deo Volen Indopratama', 'PT Deo Volen Indopratama', 'Jln. Alur Laut No. 14A, RT 006, RW 003, Rawa Badak, Koja, Jakarta Utara', 'Jln. Alur Laut No. 14A, RT 006, RW 003, Rawa Badak, Koja, Jakarta Utara', '026963199045000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(212, 'PT. Jobstreet Indonesia', 'PT. Jobstreet Indonesia', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(213, 'Agus Budianto', 'Agus Budianto', 'Plelen RT 04, RW 33, Radipiro, Banjarsari, Surakarta', 'Plelen RT 04, RW 33, Radipiro, Banjarsari, Surakarta', '640042586526000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(214, 'PT. Bhinneka MentariDimensi', 'PT. Bhinneka MentariDimensi', 'Jl. Gunung Sahari Raya 73C/5-6, Gunung Sahari Selatan, Jakarta Pusat', 'Jl. Gunung Sahari Raya 73C/5-6, Gunung Sahari Selatan, Jakarta Pusat', '016089419027000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(215, 'PT RST AGS Konsultasi', 'PT RST AGS Konsultasi', 'Gd. Noble House Lt. 27, Jl. Dr. Ide Anak Agung Gde Agung, Kav. E.4.2 No. 2, Kuningan Timur, Setiabudi, Jakarta Selatan - DKI Jakarta', 'Gd. Noble House Lt. 27, Jl. Dr. Ide Anak Agung Gde Agung, Kav. E.4.2 No. 2, Kuningan Timur, Setiabudi, Jakarta Selatan - DKI Jakarta', '819566985067000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(216, 'Non NPWP', 'Non NPWP', '', '', '0000000000000000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(217, 'Aditya Krishna Mayandra', 'Aditya Krishna Mayandra', '', '', '', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(219, 'Persek KAP Ellya Noorlisyati &amp; Rekan', 'Persek KAP Ellya Noorlisyati &', 'Jl. Cempaka Putih Tengah No. 41 vB, Cempaka Putih Timur, Jakarta Pusat', 'Jl. Cempaka Putih Tengah No. 41 vB, Cempaka Putih Timur, Jakarta Pusat', '023108301024000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(220, 'KAP Bharata Arifin Mumajad &amp; Sayuti', 'KAP Bharata Arifin Mumajad &am', 'Jl. Rawa Bambu Raya No. 17 D RT 0013/005 Jakarta Selatan', 'Jl. Rawa Bambu Raya No. 17 D RT 0013/005 Jakarta Selatan', '031991599017000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(221, 'KAP Noor Salim &amp; Rekan', 'KAP Noor Salim &amp; Rekan', 'Jl. Anggrek III Komp. Larangan Indah No. 28 RT 003 RW 005, Kota Tangerang ', 'Jl. Anggrek III Komp. Larangan Indah No. 28 RT 003 RW 005, Kota Tangerang ', '023312770416000', '', '', '', '', 'IDR', NULL, 0, 0, 0, NULL, 0, '', '', '', '', 0),
(222, 'Herry Hermawan', 'Herry Hermawan', 'Jakarta', 'Jakarta', '', '', '', '', '', 'IDR', 0, 0, 0, 0, 0, 0, '', '0', '0', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_supp_allocations`
--

CREATE TABLE `0_supp_allocations` (
  `id` int(11) NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  `amt` double UNSIGNED DEFAULT NULL,
  `date_alloc` date NOT NULL DEFAULT '0000-00-00',
  `trans_no_from` int(11) DEFAULT NULL,
  `trans_type_from` int(11) DEFAULT NULL,
  `trans_no_to` int(11) DEFAULT NULL,
  `trans_type_to` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_supp_invoice_items`
--

CREATE TABLE `0_supp_invoice_items` (
  `id` int(11) NOT NULL,
  `supp_trans_no` int(11) DEFAULT NULL,
  `supp_trans_type` int(11) DEFAULT NULL,
  `gl_code` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `grn_item_id` int(11) DEFAULT NULL,
  `po_detail_item_id` int(11) DEFAULT NULL,
  `stock_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` tinytext COLLATE utf8_unicode_ci,
  `quantity` double NOT NULL DEFAULT '0',
  `unit_price` double NOT NULL DEFAULT '0',
  `unit_tax` double NOT NULL DEFAULT '0',
  `memo_` tinytext COLLATE utf8_unicode_ci,
  `dimension_id` int(11) NOT NULL DEFAULT '0',
  `dimension2_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_supp_trans`
--

CREATE TABLE `0_supp_trans` (
  `trans_no` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `type` smallint(6) UNSIGNED NOT NULL DEFAULT '0',
  `supplier_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `reference` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `supp_reference` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `due_date` date NOT NULL DEFAULT '0000-00-00',
  `ov_amount` double NOT NULL DEFAULT '0',
  `ov_discount` double NOT NULL DEFAULT '0',
  `ov_gst` double NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '1',
  `alloc` double NOT NULL DEFAULT '0',
  `tax_included` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_sys_prefs`
--

CREATE TABLE `0_sys_prefs` (
  `name` varchar(35) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `category` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `length` smallint(6) DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_sys_prefs`
--

INSERT INTO `0_sys_prefs` (`name`, `category`, `type`, `length`, `value`) VALUES
('accounts_alpha', 'glsetup.general', 'tinyint', 1, '0'),
('accumulate_shipping', 'glsetup.customer', 'tinyint', 1, '0'),
('add_pct', 'setup.company', 'int', 5, '-1'),
('allow_negative_prices', 'glsetup.inventory', 'tinyint', 1, '1'),
('allow_negative_stock', 'glsetup.inventory', 'tinyint', 1, '1'),
('alternative_tax_include_on_docs', 'setup.company', 'tinyint', 1, '1'),
('auto_curr_reval', 'setup.company', 'smallint', 6, ''),
('bank_charge_act', 'glsetup.general', 'varchar', 15, '51002'),
('base_sales', 'setup.company', 'int', 11, '1'),
('bcc_email', 'setup.company', 'varchar', 100, ''),
('coy_logo', 'setup.company', 'varchar', 100, 'logo_frontaccounting.jpg'),
('coy_name', 'setup.company', 'varchar', 60, 'PT. Kresna Asset Management'),
('coy_no', 'setup.company', 'varchar', 25, ''),
('creditors_act', 'glsetup.purchase', 'varchar', 15, '11401'),
('curr_default', 'setup.company', 'char', 3, 'IDR'),
('debtors_act', 'glsetup.sales', 'varchar', 15, '11401'),
('default_adj_act', 'glsetup.items', 'varchar', 15, '89999'),
('default_cogs_act', 'glsetup.items', 'varchar', 15, '11401'),
('default_credit_limit', 'glsetup.customer', 'int', 11, '1000000'),
('default_delivery_required', 'glsetup.sales', 'smallint', 6, '1'),
('default_dim_required', 'glsetup.dims', 'int', 11, '20'),
('default_inv_sales_act', 'glsetup.items', 'varchar', 15, '11401'),
('default_inventory_act', 'glsetup.items', 'varchar', 15, '89999'),
('default_loss_on_asset_disposal_act', 'glsetup.items', 'varchar', 15, '66001'),
('default_prompt_payment_act', 'glsetup.sales', 'varchar', 15, '89999'),
('default_quote_valid_days', 'glsetup.sales', 'smallint', 6, '30'),
('default_receival_required', 'glsetup.purchase', 'smallint', 6, '10'),
('default_sales_act', 'glsetup.sales', 'varchar', 15, ''),
('default_sales_discount_act', 'glsetup.sales', 'varchar', 15, '99999'),
('default_wip_act', 'glsetup.items', 'varchar', 15, '11401'),
('default_workorder_required', 'glsetup.manuf', 'int', 11, '20'),
('deferred_income_act', 'glsetup.sales', 'varchar', 15, ''),
('depreciation_period', 'glsetup.company', 'tinyint', 1, '0'),
('domicile', 'setup.company', 'varchar', 55, 'Jakarta'),
('email', 'setup.company', 'varchar', 100, 'jakarta2@kresnainsurance.com'),
('exchange_diff_act', 'glsetup.general', 'varchar', 15, '39999'),
('f_year', 'setup.company', 'int', 11, '3'),
('fax', 'setup.company', 'varchar', 30, '(021) 6531 1160/61 '),
('freight_act', 'glsetup.customer', 'varchar', 15, '99999'),
('gl_closing_date', 'setup.closing_date', 'date', 8, '2016-12-31'),
('grn_clearing_act', 'glsetup.purchase', 'varchar', 15, ''),
('gst_no', 'setup.company', 'varchar', 25, ''),
('legal_text', 'glsetup.customer', 'tinytext', 0, ''),
('loc_notification', 'glsetup.inventory', 'tinyint', 1, ''),
('login_tout', 'setup.company', 'smallint', 6, '80000'),
('no_customer_list', 'setup.company', 'tinyint', 1, '1'),
('no_item_list', 'setup.company', 'tinyint', 1, '1'),
('no_supplier_list', 'setup.company', 'tinyint', 1, '1'),
('no_zero_lines_amount', 'glsetup.sales', 'tinyint', 1, '1'),
('past_due_days', 'glsetup.general', 'int', 11, '30'),
('phone', 'setup.company', 'varchar', 30, '(021) 2939 1941'),
('po_over_charge', 'glsetup.purchase', 'int', 11, '10'),
('po_over_receive', 'glsetup.purchase', 'int', 11, '10'),
('postal_address', 'setup.company', 'tinytext', 0, 'Jl. Widya Chandra V, RT.5/RW.1, Senayan, Kby. Baru, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12190'),
('print_invoice_no', 'glsetup.sales', 'tinyint', 1, '0'),
('print_item_images_on_quote', 'glsetup.inventory', 'tinyint', 1, ''),
('profit_loss_year_act', 'glsetup.general', 'varchar', 15, '99999'),
('pyt_discount_act', 'glsetup.purchase', 'varchar', 15, '89999'),
('retained_earnings_act', 'glsetup.general', 'varchar', 15, '38000'),
('round_to', 'setup.company', 'int', 5, '1'),
('show_po_item_codes', 'glsetup.purchase', 'tinyint', 1, ''),
('suppress_tax_rates', 'setup.company', 'tinyint', 1, '1'),
('tax_algorithm', 'glsetup.customer', 'tinyint', 1, '1'),
('tax_last', 'setup.company', 'int', 11, '1'),
('tax_prd', 'setup.company', 'int', 11, '1'),
('time_zone', 'setup.company', 'tinyint', 1, '1'),
('use_dimension', 'setup.company', 'tinyint', 1, '2'),
('use_fixed_assets', 'setup.company', 'tinyint', 1, '1'),
('use_manufacturing', 'setup.company', 'tinyint', 1, ''),
('version_id', 'system', 'varchar', 11, '2.4.1');

-- --------------------------------------------------------

--
-- Table structure for table `0_sys_types`
--

CREATE TABLE `0_sys_types` (
  `type_id` smallint(6) NOT NULL DEFAULT '0',
  `type_no` int(11) NOT NULL DEFAULT '1',
  `next_reference` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_sys_types`
--

INSERT INTO `0_sys_types` (`type_id`, `type_no`, `next_reference`) VALUES
(0, 17, '1'),
(1, 7, '1'),
(2, 4, '1'),
(4, 3, '1'),
(10, 16, '1'),
(11, 2, '1'),
(12, 6, '1'),
(13, 1, '1'),
(16, 2, '1'),
(17, 2, '1'),
(18, 1, '1'),
(20, 6, '1'),
(21, 1, '1'),
(22, 3, '1'),
(25, 1, '1'),
(26, 1, '1'),
(28, 1, '1'),
(29, 1, '1'),
(30, 0, '1'),
(32, 0, '1'),
(35, 1, '1'),
(40, 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `0_tags`
--

CREATE TABLE `0_tags` (
  `id` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_tag_associations`
--

CREATE TABLE `0_tag_associations` (
  `record_id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_tax_groups`
--

CREATE TABLE `0_tax_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_tax_groups`
--

INSERT INTO `0_tax_groups` (`id`, `name`, `inactive`) VALUES
(1, 'PPN dan PPH Ps 23', 0),
(2, 'Tax Exempt', 0),
(3, 'PPH Ps 23', 0),
(4, 'PPN', 0),
(5, 'PPN dan PPh Ps 4(2)', 0),
(6, 'PPN dan PPh Ps 21', 1),
(7, 'PPN dan PPH Ps 26', 0),
(8, 'PPH Ps 21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_tax_group_items`
--

CREATE TABLE `0_tax_group_items` (
  `tax_group_id` int(11) NOT NULL DEFAULT '0',
  `tax_type_id` int(11) NOT NULL DEFAULT '0',
  `tax_shipping` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_tax_group_items`
--

INSERT INTO `0_tax_group_items` (`tax_group_id`, `tax_type_id`, `tax_shipping`) VALUES
(1, 1, 0),
(1, 2, 0),
(3, 2, 0),
(4, 1, 0),
(5, 1, 0),
(5, 3, 0),
(6, 1, 0),
(6, 4, 0),
(7, 1, 0),
(7, 5, 0),
(8, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_tax_jasa`
--

CREATE TABLE `0_tax_jasa` (
  `id` int(11) NOT NULL,
  `tax_type_id` int(11) NOT NULL,
  `jenis` varchar(100) NOT NULL,
  `tarif` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_tax_jasa`
--

INSERT INTO `0_tax_jasa` (`id`, `tax_type_id`, `jenis`, `tarif`) VALUES
(1, 2, 'Sewa dan Penghasilan lain sehubungan dengan penggunaan harta', 10),
(2, 2, 'Obligasi', 10),
(3, 2, 'Pialang', 10),
(4, 2, 'Jasa Manajemen', 10),
(5, 2, 'Jasa Software', 10),
(6, 2, 'Jasa Profesional', 10),
(7, 2, 'Media Masa', 10),
(8, 2, 'Instalasi & Pemasangan', 10),
(9, 2, 'Sewa dan Penghasilan lain sehubungan dengan penggunaan harta', 10),
(10, 2, 'Jasa Penyelenggaraan Kegiatan', 10),
(11, 2, 'Perawatan/Perbaikan/Pemeliharaan mesin/peralatan', 10),
(12, 2, 'Jasa Informasi', 10),
(13, 2, 'Jasa Percetakan', 10),
(14, 2, 'Jasa Internet', 10),
(15, 2, 'Jasa Penyediaan Tempat dan/ waktu dalam media masa', 10),
(16, 3, 'Rental', 10),
(17, 3, 'Service Charge', 10),
(18, 3, 'Chilled Water', 10),
(19, 3, 'AC', 10),
(20, 3, 'Listrik', 10),
(21, 3, 'Allocation Car Park Reserved', 10),
(22, 3, 'Dividen', 10),
(23, 3, 'Renovasi', 10),
(24, 2, 'Jasa Pembuatan Website', 10),
(25, 2, 'Jasa Outsource', 10),
(26, 2, 'Jasa Pelatihan', 10),
(27, 3, 'Hadiah', 25),
(28, 3, 'Sewa Lapangan', 10),
(29, 0, 'Pembayaran Pajak', 10),
(30, 3, 'Barang Elektronik', 10);

-- --------------------------------------------------------

--
-- Table structure for table `0_tax_types`
--

CREATE TABLE `0_tax_types` (
  `id` int(11) NOT NULL,
  `rate` double NOT NULL DEFAULT '0',
  `sales_gl_code` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `purchasing_gl_code` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `wth` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_tax_types`
--

INSERT INTO `0_tax_types` (`id`, `rate`, `sales_gl_code`, `purchasing_gl_code`, `name`, `inactive`, `wth`) VALUES
(1, 10, '21240', '11703', 'PPN 10%', 0, 0),
(2, 2, '11510', '21220', 'Prepaid PPh 23', 0, 1),
(3, 10, '11520', '21230', 'PPh Ps 4(2)', 0, 1),
(4, 2.5, '11540', '21210', 'PPh Ps 21', 0, 1),
(5, 20, '21270', '21270', 'PPH Ps 26', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `0_trans_tax_details`
--

CREATE TABLE `0_trans_tax_details` (
  `id` int(11) NOT NULL,
  `trans_type` smallint(6) DEFAULT NULL,
  `trans_no` int(11) DEFAULT NULL,
  `tran_date` date NOT NULL,
  `tax_type_id` int(11) NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  `ex_rate` double NOT NULL DEFAULT '1',
  `included_in_price` tinyint(1) NOT NULL DEFAULT '0',
  `net_amount` double NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `memo` tinytext COLLATE utf8_unicode_ci,
  `reg_type` tinyint(1) DEFAULT NULL,
  `jasa` int(11) DEFAULT NULL,
  `fp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_useronline`
--

CREATE TABLE `0_useronline` (
  `id` int(11) NOT NULL,
  `timestamp` int(15) NOT NULL DEFAULT '0',
  `ip` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `file` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_users`
--

CREATE TABLE `0_users` (
  `id` smallint(6) NOT NULL,
  `user_id` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `real_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `role_id` int(11) NOT NULL DEFAULT '1',
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_format` tinyint(1) NOT NULL DEFAULT '0',
  `date_sep` tinyint(1) NOT NULL DEFAULT '0',
  `tho_sep` tinyint(1) NOT NULL DEFAULT '0',
  `dec_sep` tinyint(1) NOT NULL DEFAULT '0',
  `theme` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `page_size` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A4',
  `prices_dec` smallint(6) NOT NULL DEFAULT '2',
  `qty_dec` smallint(6) NOT NULL DEFAULT '2',
  `rates_dec` smallint(6) NOT NULL DEFAULT '4',
  `percent_dec` smallint(6) NOT NULL DEFAULT '1',
  `show_gl` tinyint(1) NOT NULL DEFAULT '1',
  `show_codes` tinyint(1) NOT NULL DEFAULT '0',
  `show_hints` tinyint(1) NOT NULL DEFAULT '0',
  `last_visit_date` datetime DEFAULT NULL,
  `query_size` tinyint(1) UNSIGNED NOT NULL DEFAULT '10',
  `graphic_links` tinyint(1) DEFAULT '1',
  `pos` smallint(6) DEFAULT '1',
  `print_profile` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `rep_popup` tinyint(1) DEFAULT '1',
  `sticky_doc_date` tinyint(1) DEFAULT '0',
  `startup_tab` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `transaction_days` int(6) NOT NULL DEFAULT '30' COMMENT 'Transaction days',
  `save_report_selections` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Save Report Selection Days',
  `use_date_picker` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Use Date Picker for all Date Values',
  `def_print_destination` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Default Report Destination',
  `def_print_orientation` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Default Report Orientation',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `0_users`
--

INSERT INTO `0_users` (`id`, `user_id`, `password`, `real_name`, `role_id`, `phone`, `email`, `language`, `date_format`, `date_sep`, `tho_sep`, `dec_sep`, `theme`, `page_size`, `prices_dec`, `qty_dec`, `rates_dec`, `percent_dec`, `show_gl`, `show_codes`, `show_hints`, `last_visit_date`, `query_size`, `graphic_links`, `pos`, `print_profile`, `rep_popup`, `sticky_doc_date`, `startup_tab`, `transaction_days`, `save_report_selections`, `use_date_picker`, `def_print_destination`, `def_print_orientation`, `inactive`) VALUES
(0, 'inne', '7d18048b8635f2ead5b871a6e07db301', 'Inneke Octalia Putri', 2, '085780886610', 'inneke@KAMcapital.com', 'C', 1, 0, 1, 1, 'default', 'Letter', 2, 2, 2, 0, 1, 0, 1, '2018-04-26 11:02:59', 10, 1, 1, '', 1, 0, 'GL', 30, 0, 1, 0, 0, 0),
(1, 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', 'Admin Sistem', 2, '', 'adm@adm.com', 'C', 1, 0, 1, 1, 'default', 'Letter', 2, 2, 2, 0, 1, 0, 1, '2018-06-26 09:04:39', 10, 1, 1, '', 1, 0, 'GL', 30, 0, 1, 0, 0, 0),
(6, 'julia', 'ac115d62e319a52527d7b4131e669c83', 'Julia Lombo', 2, '081212100560', 'julia@KAMcapital.com', 'C', 1, 0, 1, 1, 'default', 'Letter', 2, 2, 2, 0, 1, 0, 1, '2018-04-27 04:46:03', 10, 1, 1, '', 1, 0, 'GL', 30, 0, 1, 0, 0, 0),
(7, 'inneke', '1e80fd2fc3bfa02942f812440923efdd', 'Inneke Octalia Putri', 2, '085780886610', 'inneke@KAMcapital.com', 'C', 1, 0, 1, 1, 'default', 'Letter', 2, 2, 2, 0, 1, 0, 1, NULL, 10, 1, 1, '', 1, 0, 'GL', 30, 0, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_voided`
--

CREATE TABLE `0_voided` (
  `type` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL DEFAULT '0',
  `date_` date NOT NULL DEFAULT '0000-00-00',
  `memo_` tinytext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_workcentres`
--

CREATE TABLE `0_workcentres` (
  `id` int(11) NOT NULL,
  `name` char(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_workorders`
--

CREATE TABLE `0_workorders` (
  `id` int(11) NOT NULL,
  `wo_ref` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `loc_code` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `units_reqd` double NOT NULL DEFAULT '1',
  `stock_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `date_` date NOT NULL DEFAULT '0000-00-00',
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `required_by` date NOT NULL DEFAULT '0000-00-00',
  `released_date` date NOT NULL DEFAULT '0000-00-00',
  `units_issued` double NOT NULL DEFAULT '0',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `released` tinyint(1) NOT NULL DEFAULT '0',
  `additional_costs` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_wo_costing`
--

CREATE TABLE `0_wo_costing` (
  `id` int(11) NOT NULL,
  `workorder_id` int(11) NOT NULL DEFAULT '0',
  `cost_type` tinyint(1) NOT NULL DEFAULT '0',
  `trans_type` int(11) NOT NULL DEFAULT '0',
  `trans_no` int(11) NOT NULL DEFAULT '0',
  `factor` double NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_wo_issues`
--

CREATE TABLE `0_wo_issues` (
  `issue_no` int(11) NOT NULL,
  `workorder_id` int(11) NOT NULL DEFAULT '0',
  `reference` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `loc_code` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `workcentre_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_wo_issue_items`
--

CREATE TABLE `0_wo_issue_items` (
  `id` int(11) NOT NULL,
  `stock_id` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `issue_id` int(11) DEFAULT NULL,
  `qty_issued` double DEFAULT NULL,
  `unit_cost` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_wo_manufacture`
--

CREATE TABLE `0_wo_manufacture` (
  `id` int(11) NOT NULL,
  `reference` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `workorder_id` int(11) NOT NULL DEFAULT '0',
  `quantity` double NOT NULL DEFAULT '0',
  `date_` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `0_wo_requirements`
--

CREATE TABLE `0_wo_requirements` (
  `id` int(11) NOT NULL,
  `workorder_id` int(11) NOT NULL DEFAULT '0',
  `stock_id` char(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `workcentre` int(11) NOT NULL DEFAULT '0',
  `units_req` double NOT NULL DEFAULT '1',
  `unit_cost` double NOT NULL DEFAULT '0',
  `loc_code` char(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `units_issued` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `0_areas`
--
ALTER TABLE `0_areas`
  ADD PRIMARY KEY (`area_code`),
  ADD UNIQUE KEY `description` (`description`);

--
-- Indexes for table `0_attachments`
--
ALTER TABLE `0_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_no` (`type_no`,`trans_no`);

--
-- Indexes for table `0_audit_trail`
--
ALTER TABLE `0_audit_trail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Seq` (`fiscal_year`,`gl_date`,`gl_seq`),
  ADD KEY `Type_and_Number` (`type`,`trans_no`);

--
-- Indexes for table `0_bank_accounts`
--
ALTER TABLE `0_bank_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bank_account_name` (`bank_account_name`),
  ADD KEY `bank_account_number` (`bank_account_number`),
  ADD KEY `account_code` (`account_code`);

--
-- Indexes for table `0_bank_trans`
--
ALTER TABLE `0_bank_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bank_act` (`bank_act`,`ref`),
  ADD KEY `type` (`type`,`trans_no`),
  ADD KEY `bank_act_2` (`bank_act`,`reconciled`),
  ADD KEY `bank_act_3` (`bank_act`,`trans_date`);

--
-- Indexes for table `0_bom`
--
ALTER TABLE `0_bom`
  ADD PRIMARY KEY (`parent`,`component`,`workcentre_added`,`loc_code`),
  ADD KEY `component` (`component`),
  ADD KEY `id` (`id`),
  ADD KEY `loc_code` (`loc_code`),
  ADD KEY `parent` (`parent`,`loc_code`),
  ADD KEY `workcentre_added` (`workcentre_added`);

--
-- Indexes for table `0_budget_trans`
--
ALTER TABLE `0_budget_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Account` (`account`,`tran_date`,`dimension_id`,`dimension2_id`);

--
-- Indexes for table `0_chart_class`
--
ALTER TABLE `0_chart_class`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `0_chart_master`
--
ALTER TABLE `0_chart_master`
  ADD PRIMARY KEY (`account_code`),
  ADD KEY `account_name` (`account_name`),
  ADD KEY `accounts_by_type` (`account_type`,`account_code`);

--
-- Indexes for table `0_chart_types`
--
ALTER TABLE `0_chart_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `class_id` (`class_id`);

--
-- Indexes for table `0_comments`
--
ALTER TABLE `0_comments`
  ADD KEY `type_and_id` (`type`,`id`);

--
-- Indexes for table `0_credit_status`
--
ALTER TABLE `0_credit_status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reason_description` (`reason_description`);

--
-- Indexes for table `0_crm_categories`
--
ALTER TABLE `0_crm_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`,`action`),
  ADD UNIQUE KEY `type_2` (`type`,`name`);

--
-- Indexes for table `0_crm_contacts`
--
ALTER TABLE `0_crm_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`,`action`);

--
-- Indexes for table `0_crm_persons`
--
ALTER TABLE `0_crm_persons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ref` (`ref`);

--
-- Indexes for table `0_currencies`
--
ALTER TABLE `0_currencies`
  ADD PRIMARY KEY (`curr_abrev`);

--
-- Indexes for table `0_cust_allocations`
--
ALTER TABLE `0_cust_allocations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `person_id` (`person_id`,`trans_type_from`,`trans_no_from`,`trans_type_to`,`trans_no_to`),
  ADD KEY `From` (`trans_type_from`,`trans_no_from`),
  ADD KEY `To` (`trans_type_to`,`trans_no_to`);

--
-- Indexes for table `0_cust_branch`
--
ALTER TABLE `0_cust_branch`
  ADD PRIMARY KEY (`branch_code`,`debtor_no`),
  ADD KEY `branch_ref` (`branch_ref`),
  ADD KEY `group_no` (`group_no`);

--
-- Indexes for table `0_debtors_master`
--
ALTER TABLE `0_debtors_master`
  ADD PRIMARY KEY (`debtor_no`),
  ADD UNIQUE KEY `debtor_ref` (`debtor_ref`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `0_debtor_trans`
--
ALTER TABLE `0_debtor_trans`
  ADD PRIMARY KEY (`type`,`trans_no`,`debtor_no`),
  ADD KEY `debtor_no` (`debtor_no`,`branch_code`),
  ADD KEY `tran_date` (`tran_date`);

--
-- Indexes for table `0_debtor_trans_details`
--
ALTER TABLE `0_debtor_trans_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Transaction` (`debtor_trans_type`,`debtor_trans_no`),
  ADD KEY `src_id` (`src_id`);

--
-- Indexes for table `0_dimensions`
--
ALTER TABLE `0_dimensions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reference` (`reference`),
  ADD KEY `date_` (`date_`),
  ADD KEY `due_date` (`due_date`),
  ADD KEY `type_` (`type_`);

--
-- Indexes for table `0_dimension_link`
--
ALTER TABLE `0_dimension_link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_exchange_rates`
--
ALTER TABLE `0_exchange_rates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `curr_code` (`curr_code`,`date_`);

--
-- Indexes for table `0_fiscal_year`
--
ALTER TABLE `0_fiscal_year`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `begin` (`begin`),
  ADD UNIQUE KEY `end` (`end`);

--
-- Indexes for table `0_fp_trans`
--
ALTER TABLE `0_fp_trans`
  ADD PRIMARY KEY (`id`,`type`),
  ADD KEY `Type_and_Reference` (`type`,`reference`);

--
-- Indexes for table `0_gl_trans`
--
ALTER TABLE `0_gl_trans`
  ADD PRIMARY KEY (`counter`),
  ADD KEY `Type_and_Number` (`type`,`type_no`),
  ADD KEY `dimension_id` (`dimension_id`),
  ADD KEY `dimension2_id` (`dimension2_id`),
  ADD KEY `tran_date` (`tran_date`),
  ADD KEY `account_and_tran_date` (`account`,`tran_date`);

--
-- Indexes for table `0_grn_batch`
--
ALTER TABLE `0_grn_batch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_date` (`delivery_date`),
  ADD KEY `purch_order_no` (`purch_order_no`);

--
-- Indexes for table `0_grn_items`
--
ALTER TABLE `0_grn_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grn_batch_id` (`grn_batch_id`);

--
-- Indexes for table `0_groups`
--
ALTER TABLE `0_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `description` (`description`);

--
-- Indexes for table `0_item_codes`
--
ALTER TABLE `0_item_codes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `stock_id` (`stock_id`,`item_code`),
  ADD KEY `item_code` (`item_code`);

--
-- Indexes for table `0_item_tax_types`
--
ALTER TABLE `0_item_tax_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `0_item_tax_type_exemptions`
--
ALTER TABLE `0_item_tax_type_exemptions`
  ADD PRIMARY KEY (`item_tax_type_id`,`tax_type_id`);

--
-- Indexes for table `0_item_units`
--
ALTER TABLE `0_item_units`
  ADD PRIMARY KEY (`abbr`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `0_journal`
--
ALTER TABLE `0_journal`
  ADD PRIMARY KEY (`type`,`trans_no`),
  ADD KEY `tran_date` (`tran_date`);

--
-- Indexes for table `0_locations`
--
ALTER TABLE `0_locations`
  ADD PRIMARY KEY (`loc_code`);

--
-- Indexes for table `0_loc_stock`
--
ALTER TABLE `0_loc_stock`
  ADD PRIMARY KEY (`loc_code`,`stock_id`),
  ADD KEY `stock_id` (`stock_id`);

--
-- Indexes for table `0_marketing_trans`
--
ALTER TABLE `0_marketing_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_mkbd`
--
ALTER TABLE `0_mkbd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_mkbd_coa`
--
ALTER TABLE `0_mkbd_coa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_no_faktur`
--
ALTER TABLE `0_no_faktur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_payment_terms`
--
ALTER TABLE `0_payment_terms`
  ADD PRIMARY KEY (`terms_indicator`),
  ADD UNIQUE KEY `terms` (`terms`);

--
-- Indexes for table `0_prices`
--
ALTER TABLE `0_prices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `price` (`stock_id`,`sales_type_id`,`curr_abrev`);

--
-- Indexes for table `0_printers`
--
ALTER TABLE `0_printers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `0_print_profiles`
--
ALTER TABLE `0_print_profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profile` (`profile`,`report`);

--
-- Indexes for table `0_purch_data`
--
ALTER TABLE `0_purch_data`
  ADD PRIMARY KEY (`supplier_id`,`stock_id`);

--
-- Indexes for table `0_purch_orders`
--
ALTER TABLE `0_purch_orders`
  ADD PRIMARY KEY (`order_no`),
  ADD KEY `ord_date` (`ord_date`);

--
-- Indexes for table `0_purch_order_details`
--
ALTER TABLE `0_purch_order_details`
  ADD PRIMARY KEY (`po_detail_item`),
  ADD KEY `order` (`order_no`,`po_detail_item`),
  ADD KEY `itemcode` (`item_code`);

--
-- Indexes for table `0_quick_entries`
--
ALTER TABLE `0_quick_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `description` (`description`);

--
-- Indexes for table `0_quick_entry_lines`
--
ALTER TABLE `0_quick_entry_lines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `qid` (`qid`);

--
-- Indexes for table `0_recurrent_invoices`
--
ALTER TABLE `0_recurrent_invoices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `description` (`description`);

--
-- Indexes for table `0_reflines`
--
ALTER TABLE `0_reflines`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `prefix` (`trans_type`,`prefix`);

--
-- Indexes for table `0_refs`
--
ALTER TABLE `0_refs`
  ADD PRIMARY KEY (`id`,`type`),
  ADD KEY `Type_and_Reference` (`type`,`reference`);

--
-- Indexes for table `0_salesman`
--
ALTER TABLE `0_salesman`
  ADD PRIMARY KEY (`salesman_code`),
  ADD UNIQUE KEY `salesman_name` (`salesman_name`);

--
-- Indexes for table `0_sales_orders`
--
ALTER TABLE `0_sales_orders`
  ADD PRIMARY KEY (`trans_type`,`order_no`);

--
-- Indexes for table `0_sales_order_details`
--
ALTER TABLE `0_sales_order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sorder` (`trans_type`,`order_no`),
  ADD KEY `stkcode` (`stk_code`);

--
-- Indexes for table `0_sales_pos`
--
ALTER TABLE `0_sales_pos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pos_name` (`pos_name`);

--
-- Indexes for table `0_sales_types`
--
ALTER TABLE `0_sales_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sales_type` (`sales_type`);

--
-- Indexes for table `0_security_roles`
--
ALTER TABLE `0_security_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role` (`role`);

--
-- Indexes for table `0_shippers`
--
ALTER TABLE `0_shippers`
  ADD PRIMARY KEY (`shipper_id`),
  ADD UNIQUE KEY `name` (`shipper_name`);

--
-- Indexes for table `0_sql_trail`
--
ALTER TABLE `0_sql_trail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_stock_category`
--
ALTER TABLE `0_stock_category`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `description` (`description`);

--
-- Indexes for table `0_stock_fa_class`
--
ALTER TABLE `0_stock_fa_class`
  ADD PRIMARY KEY (`fa_class_id`);

--
-- Indexes for table `0_stock_master`
--
ALTER TABLE `0_stock_master`
  ADD PRIMARY KEY (`stock_id`);

--
-- Indexes for table `0_stock_moves`
--
ALTER TABLE `0_stock_moves`
  ADD PRIMARY KEY (`trans_id`),
  ADD KEY `type` (`type`,`trans_no`),
  ADD KEY `Move` (`stock_id`,`loc_code`,`tran_date`);

--
-- Indexes for table `0_suppliers`
--
ALTER TABLE `0_suppliers`
  ADD PRIMARY KEY (`supplier_id`),
  ADD KEY `supp_ref` (`supp_ref`);

--
-- Indexes for table `0_supp_allocations`
--
ALTER TABLE `0_supp_allocations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `person_id` (`person_id`,`trans_type_from`,`trans_no_from`,`trans_type_to`,`trans_no_to`),
  ADD KEY `From` (`trans_type_from`,`trans_no_from`),
  ADD KEY `To` (`trans_type_to`,`trans_no_to`);

--
-- Indexes for table `0_supp_invoice_items`
--
ALTER TABLE `0_supp_invoice_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Transaction` (`supp_trans_type`,`supp_trans_no`,`stock_id`);

--
-- Indexes for table `0_supp_trans`
--
ALTER TABLE `0_supp_trans`
  ADD PRIMARY KEY (`type`,`trans_no`,`supplier_id`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `tran_date` (`tran_date`);

--
-- Indexes for table `0_sys_prefs`
--
ALTER TABLE `0_sys_prefs`
  ADD PRIMARY KEY (`name`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `0_sys_types`
--
ALTER TABLE `0_sys_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `0_tags`
--
ALTER TABLE `0_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`,`name`);

--
-- Indexes for table `0_tag_associations`
--
ALTER TABLE `0_tag_associations`
  ADD UNIQUE KEY `record_id` (`record_id`,`tag_id`);

--
-- Indexes for table `0_tax_groups`
--
ALTER TABLE `0_tax_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `0_tax_group_items`
--
ALTER TABLE `0_tax_group_items`
  ADD PRIMARY KEY (`tax_group_id`,`tax_type_id`);

--
-- Indexes for table `0_tax_jasa`
--
ALTER TABLE `0_tax_jasa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_tax_types`
--
ALTER TABLE `0_tax_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_trans_tax_details`
--
ALTER TABLE `0_trans_tax_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tran_date` (`tran_date`);

--
-- Indexes for table `0_useronline`
--
ALTER TABLE `0_useronline`
  ADD PRIMARY KEY (`id`),
  ADD KEY `timestamp` (`timestamp`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `0_users`
--
ALTER TABLE `0_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `0_voided`
--
ALTER TABLE `0_voided`
  ADD UNIQUE KEY `id` (`type`,`id`);

--
-- Indexes for table `0_workcentres`
--
ALTER TABLE `0_workcentres`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `0_workorders`
--
ALTER TABLE `0_workorders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `wo_ref` (`wo_ref`);

--
-- Indexes for table `0_wo_costing`
--
ALTER TABLE `0_wo_costing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_wo_issues`
--
ALTER TABLE `0_wo_issues`
  ADD PRIMARY KEY (`issue_no`),
  ADD KEY `workorder_id` (`workorder_id`);

--
-- Indexes for table `0_wo_issue_items`
--
ALTER TABLE `0_wo_issue_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_wo_manufacture`
--
ALTER TABLE `0_wo_manufacture`
  ADD PRIMARY KEY (`id`),
  ADD KEY `workorder_id` (`workorder_id`);

--
-- Indexes for table `0_wo_requirements`
--
ALTER TABLE `0_wo_requirements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `workorder_id` (`workorder_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `0_areas`
--
ALTER TABLE `0_areas`
  MODIFY `area_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `0_attachments`
--
ALTER TABLE `0_attachments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_audit_trail`
--
ALTER TABLE `0_audit_trail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_bank_accounts`
--
ALTER TABLE `0_bank_accounts`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `0_bank_trans`
--
ALTER TABLE `0_bank_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_bom`
--
ALTER TABLE `0_bom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_budget_trans`
--
ALTER TABLE `0_budget_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_credit_status`
--
ALTER TABLE `0_credit_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `0_crm_categories`
--
ALTER TABLE `0_crm_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'pure technical key', AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `0_crm_contacts`
--
ALTER TABLE `0_crm_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- AUTO_INCREMENT for table `0_crm_persons`
--
ALTER TABLE `0_crm_persons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `0_cust_allocations`
--
ALTER TABLE `0_cust_allocations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_cust_branch`
--
ALTER TABLE `0_cust_branch`
  MODIFY `branch_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `0_debtors_master`
--
ALTER TABLE `0_debtors_master`
  MODIFY `debtor_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `0_debtor_trans_details`
--
ALTER TABLE `0_debtor_trans_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_dimensions`
--
ALTER TABLE `0_dimensions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1201;

--
-- AUTO_INCREMENT for table `0_dimension_link`
--
ALTER TABLE `0_dimension_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1146;

--
-- AUTO_INCREMENT for table `0_exchange_rates`
--
ALTER TABLE `0_exchange_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_fiscal_year`
--
ALTER TABLE `0_fiscal_year`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `0_gl_trans`
--
ALTER TABLE `0_gl_trans`
  MODIFY `counter` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_grn_batch`
--
ALTER TABLE `0_grn_batch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_marketing_trans`
--
ALTER TABLE `0_marketing_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_mkbd_coa`
--
ALTER TABLE `0_mkbd_coa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=274;

--
-- AUTO_INCREMENT for table `0_suppliers`
--
ALTER TABLE `0_suppliers`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;

--
-- AUTO_INCREMENT for table `0_tax_jasa`
--
ALTER TABLE `0_tax_jasa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `0_trans_tax_details`
--
ALTER TABLE `0_trans_tax_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
