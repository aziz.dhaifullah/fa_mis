<?php
//============================================================+
// File name   : example_021.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 021 for TCPDF class
//               WriteHTML text flow
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML text flow.
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('KAM');
$pdf->SetTitle('KAM');
$pdf->SetSubject('KAM');

// set default header data
//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

// add a page
$pdf->AddPage();
$html= "<table>";
$html.="<tr><td width='400'><table class='callout_main' border='0' cellpadding='0' cellspacing='0'>";
$html.= "<tr><td>";
$html.= "<img src='../themes/default/images/logo_frontaccounting.png' alt='FrontAccounting' height='20'  border='0' style='margin: auto;display: block;'>";
$html.= "</td></tr></table></td>";
$html.= '<td><table align="right">';
$html.= "<tr><td>Date</td><td></td></tr>";
$html.= "<tr><td>Reff Number</td><td></td></tr>";
$html.= "</td></table>";
$html.= "</tr>";
$html.= '<tr><td colspan="2" style="text-align:center;"><strong> Transaction Slip</strong></td></tr>';
$html.= '</table>';

$html='<table border="0" cellpadding="10">
<tr>
<td width="450" height="30" rowspan="2" valign="middle"><img src="../themes/default/images/logo_frontaccounting.png"></td>
<td width="90" height="30" >Date</td>
<td width="110" height="30" ></td>
</tr>
<tr>
<td width="90" height="30">Reff Number</td>
<td width="110" height="30" ></td>
</tr>
<tr>
<td height="50" colspan="3" style="text-align:center;font-size:14px;">Slip Transaction</td>
</tr>
<tr>
<td width="270" height="30">A/C No. </td>
<td width="190" height="30">Chq/BG No. </td>
<td width="190" height="30">Amount Rp. </td>
</tr>
</table>';

$html.='<table border="1" cellpadding="10">
<tr>
<th width="150" height="40" style="text-align:center;">No. Account</th>
<th width="190" height="40" style="text-align:center;" >Amount</th>
<th width="310" height="40"  style="text-align:center;">Description</th>
</tr>';
$html.='
<tr>
<td height="20" ></td>
<td height="20"></td>
<td height="20"></td>
</tr>';
$html.='</table>';
$html.='<table border="1" cellspacing="8"  cellpadding="3">
<tr>
<th width="120" height="30" style="text-align:center;">Prepared by:
<br><br><br><br>
(..............................)
</th>
<th width="120" height="30" style="text-align:center;" >Checked by:
<br><br><br><br>
(..............................)</th>
<th width="120" height="30"  style="text-align:center;">Approved by:
<br><br><br><br>
(..............................)</th>
<th width="120" height="30" style="text-align:center;" >Input by:
<br><br><br><br>
(..............................)</th>
<th width="122" height="30"  style="text-align:center;">Received by:
<br><br><br><br>
(..............................)</th>
</tr>';
$html.='</table>';

// output the HTML content
$pdf->writeHTML($html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_voucher.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
