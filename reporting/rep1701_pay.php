<?php
//============================================================+
// File name   : example_021.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 021 for TCPDF class
//               WriteHTML text flow
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML text flow.
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('KAM');
$pdf->SetTitle('KAM');
$pdf->SetSubject('KAM');

// set default header data
//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

$path_to_root = "..";
include_once($path_to_root . "/includes/session.inc");

//page(_($help_context = "General Ledger Transaction Details"), true);

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
$result = get_gl_trans2($_GET['type_id'], $_GET['trans_no']);
$account=array();$amount=array();$memo=array();
$amnt=0;$debit=0;
$ref='';$docdate='';$bank='';
while ($myrow = db_fetch($result)) 
{
	//print_r($myrow);
	$account[]=$myrow['account2'];
    $amnt=($myrow['amount']*(-1));
	$amount[]=$amnt;
	$memo[]=$myrow['memo_'];
	$debit += $amnt;
	$ref=$myrow["kode"].'-'.$myrow["reference"];
	$ref=$myrow["reference"];
	$docdate=sql2date($myrow["doc_date"]);
	$bank=$myrow["bank_name"];
	$bank_account_number=$myrow["bank_account_number"];
}

$cash_acc=$_GET['cash_acc'];
if($cash_acc!=''){
	$bnk=get_bank($cash_acc);
	$brow = db_fetch($bnk);
	$bank=$brow["bank_name"];
	$bank_account_number=$brow["bank_account_number"];
}
// add a page
$pdf->AddPage();
$html='<table border="0" cellpadding="5" style="font-size:11px;">
<tr>
<td width="440" height="30" rowspan="2" valign="middle"><img src="../themes/default/images/logo_frontaccounting.png"></td>
<td width="100" height="30" style="border-left:solid 1px #000;border-top:solid 1px #000;" >Date :</td>
<td width="110" height="30" style="border-right:solid 1px #000;border-top:solid 1px #000;" >'.$docdate.'</td>
</tr>
<tr>
<td width="100" height="30" style="border-left:solid 1px #000;border-bottom:solid 1px #000;">Reff Number :</td>
<td width="110" height="30"  style="border-right:solid 1px #000;border-bottom:solid 1px #000;">'.$ref.'</td>
</tr>
<tr>
<td height="50" colspan="3" style="text-align:center;font-size:14px;">'.$bank.' Transaction Slip</td>
</tr>
<tr>
<td width="270" height="20" style="border-left:solid 1px #000;border-top:solid 1px #000;">A/C No. '.$bank_account_number.'</td>
<td width="190" height="20" style="border-top:solid 1px #000;">Chq/BG No. </td>
<td width="190" height="20" style="border-right:solid 1px #000;border-top:solid 1px #000;">Amount Rp. '.price_format($debit).'</td>
</tr>
</table>';

$html.='<table border="1" cellpadding="5" style="font-size:9px;">
<tr>
<th width="150" height="20" style="text-align:center;">No. Account</th>
<th width="190" height="20" style="text-align:center;" >Amount</th>
<th width="310" height="20"  style="text-align:center;">Description</th>
</tr>';
$rowisi=count($account);
$rownormal=9;
$rowmax=19;
/*
for($i=0;$i<$rowisi;$i++){
	$html.='
	<tr>
	<td height="10" align="center">'.$account[$i].'</td>
	<td height="10" align="right">'.price_format($amount[$i]).'</td>
	<td height="10">'.$memo[$i].'</td>
	</tr>';
}
*/
$result2 = get_gl_trans($_GET['type_id'], $_GET['trans_no']);
while ($row = db_fetch($result2)) 
{
	$html.='
	<tr>
	<td height="10" align="center">'.$row['account'].'</td>
	<td height="10" align="right">'.price_format($row['amount']).'</td>
	<td height="10">'.($row['memo_']!=''?$row['memo_']:$row['account_name']).'</td>
	</tr>';
}		
if($rowisi<$rownormal){	
	for($i=0;$i<($rownormal-$rowisi);$i++){
		$html.='
		<tr>
		<td height="10" ></td>
		<td height="10"></td>
		<td height="10"></td>
		</tr>';
	}
}
$html.='</table>';
$html.='<table border="1" cellspacing="8"  cellpadding="3">
<tr>
<th width="120" height="30" style="text-align:center;">Prepared by:
<br><br><br><br>
(..............................)
</th>
<th width="120" height="30" style="text-align:center;" >Checked by:
<br><br><br><br>
(..............................)</th>
<th width="120" height="30"  style="text-align:center;">Approved by:
<br><br><br><br>
(..............................)</th>
<th width="120" height="30" style="text-align:center;" >Input by:
<br><br><br><br>
(..............................)</th>
<th width="122" height="30"  style="text-align:center;">Received by:
<br><br><br><br>
(..............................)</th>
</tr>';
$html.='</table>';

// output the HTML content
$pdf->writeHTML($html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_voucher.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
