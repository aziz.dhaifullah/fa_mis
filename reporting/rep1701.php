<?php
//============================================================+
// File name   : example_021.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 021 for TCPDF class
//               WriteHTML text flow
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML text flow.
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('KAM');
$pdf->SetTitle('KAM');
$pdf->SetSubject('KAM');

// set default header data
//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

$path_to_root = "..";
include_once($path_to_root . "/includes/session.inc");

//page(_($help_context = "General Ledger Transaction Details"), true);

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
$result = get_gl_trans2($_GET['type_id'], $_GET['trans_no']);
$account=array();$amount=array();$memo=array();
$amnt=0;$debit=0;
$ref='';$docdate='';$bank='';
while ($myrow = db_fetch($result)) 
{
	//print_r($myrow);exit;
	$account[]=$myrow['account2'];
    $amnt=($myrow['amount']*(-1));
	$amount[]=$amnt;
	$memo[]=$myrow['memo_'];
	$ref=$myrow["kode"].'-'.$myrow["reference"];
	$ref=$myrow["reference"];
	$docdate=sql2date($myrow["doc_date"]);
	$bank=$myrow["bank_name"];
	$bank_account_number=$myrow["bank_account_number"];
}
$bnk='';$cek='';
$cash_acc=$_GET['cash_acc'];
//and b.account_type!=3
$wh1='';
if($_GET['type_id']=='1')
	$wh1='and amount<0';
$sql  = "SELECT b.bank_name,b.bank_account_number,a.tran_date,c.reference,b.account_code FROM ".TB_PREF."gl_trans a
inner join  ".TB_PREF."bank_accounts b on a.account=b.account_code 
inner join  ".TB_PREF."refs c on a.type_no=c.id and a.type=c.type
WHERE a.type=".$_GET['type_id']." and a.type_no like '".$_GET['trans_no']."' ".$wh1 ;
$result = db_query($sql);
$result2 = db_fetch_row($result);
$bank = $result2[0];
$bank_account_number = $result2[1];
$bankacc = $result2[4];

if($cash_acc!=''){
	$bnk=get_bank($cash_acc);
	$brow = db_fetch($bnk);
	$bank=$brow["bank_name"];

	$bank_account_number=$brow["bank_account_number"];
	
}

	//print_r($result2);
if(@$bank_account_number!=''){

	$bnk='A/C No. ';
	$cek='Chq/BG No. ';
}
// add a page
$pdf->AddPage();
$htmltengah='';
$htmlakhir='';
$result2 = get_gl_trans_vc($_GET['type_id'], $_GET['trans_no']);
$account2=array();
$i=0;
$nama='';
$no=1;
$ttldebit=0;$ttlkredit=0;
while ($row = db_fetch($result2)) 
{	
	if($row['amount']!=0){
		$nama=$row['real_name'];
		$account2[]=$row['account'];
		if($row['account']!=$bankacc){
			
			$desc=get_comments_string($row['type'], $row['type_no']);

			$amnt=$row['amount'];
			if($_GET['type_id']==10){
				$amnt=0-$row['amount'];
			}
			if($_GET['type_id']==2){
				$amnt=-$row['amount'];
			}

			if($row['amount']<0)
				$ttlkredit=$ttlkredit+$amnt;
			if($row['amount']>0)
				$ttldebit=$ttldebit+($amnt);
			
			$debit += $amnt;
			$memo=(trim($row['memo_'])!=''?$row['memo_']:'-');
			if($i==0){
				$memo0=$memo;
			}
			else{
				if($memo0==$memo)
				$memo=$row['account_name'];
			}
			if($row['account']=='93000'){

				$htmlakhir.='
				<tr>
				<td height="10" align="center">'.$no.'</td>
				<td height="10" align="center">'.$row['account'].'</td>
				<td height="10" align="center">'.$row['account_name'].'</td>
				<td height="10">'.$memo.'</td>
				<td height="10" align="right">'.price_format_pr($amnt).'</td>
				</tr>';
			}else{
			//if($amnt>0){
				$htmltengah.='
				<tr>
				<td height="10" align="center">'.$no.'</td>
				<td height="10" align="center">'.$row['account'].'</td>
				<td height="10" align="center">'.$row['account_name'].'</td>
				<td height="10">'.$memo.'</td>
				<td height="10" align="right">'.price_format_pr($amnt).'</td>
				</tr>';
			}
			$i++;
			$no++;
		}
	}
}		
$image='../themes/default/images/logo_frontaccounting.png';
$com=@$_SESSION["wa_current_user"]->com_id;
if($com!=''){
	$sql = "SELECT * FROM 0_companies where id='".$_SESSION["wa_current_user"]->com_id."' ;";
			//print_r($_SESSION["wa_current_user"]->com_id);exit;
	
	if($result=db_query($sql)){
		$image='../images/';$img='';
		while($row=db_fetch($result)){
			//print_r($row);
			$img=$row['logo'];
			if (@$_SESSION["wa_current_user"]->com_id == 0) {
				$headtitle = 'Mega Inti Supra';
			}else{
				$headtitle = $row['nama'];
			}
		}
		if ($_SESSION["wa_current_user"]->com_id == 0) {
			$img = 'logo_kresna.png';
		}
		$image.=$img;
	}


}

/*<td width="440" height="30" valign="middle"><img src="'.$image.'" width="100" ></td>*/
$html='<table border="0" cellpadding="5" style="font-size:11px;">
<tr>
<td width="440" height="30" valign="middle"><h1>'.$headtitle.'</h1></td>
</tr>
<tr>
<td width="80" height="30" valign="middle"></td>
<td width="110" height="30" valign="middle"></td>
<td width="240" height="30" valign="middle"></td>
<td width="80" height="30" style="border-top:solid 1px #000;border-left:solid 1px #000;border-right:solid 1px #000;border-bottom:solid 1px #000;">Voucher No. </td>
<td width="110" height="30"  style="border-top:solid 1px #000;border-right:solid 1px #000;border-bottom:solid 1px #000;text-align:right;">'.$ref.'</td></tr>
<tr>
<td width="80" height="30" valign="middle"></td>
<td width="110" height="30" valign="middle"></td>
<td width="240" height="30" valign="middle"></td>
<td width="80" height="30" style="border-left:solid 1px #000;border-right:solid 1px #000;border-bottom:solid 1px #000;" >Voucher Date </td>
<td width="110" height="30" style="border-right:solid 1px #000;border-top:solid 1px #000;border-bottom:solid 1px #000;text-align:right;" >'.$docdate.'</td>
</tr>
<tr>
<td width="80" height="30" valign="middle"></td>
<td width="110" height="30" valign="middle"></td>
<td width="240" height="30" valign="middle"></td>
<td width="80" height="30" style="border-left:solid 1px #000;border-right:solid 1px #000;border-bottom:solid 1px #000;">Journal No. </td>
<td width="110" height="30"  style="border-right:solid 1px #000;border-bottom:solid 1px #000;text-align:right;">'.@$jur.'</td></tr>
<tr>
<td width="80" height="30" valign="middle"></td>
<td width="110" height="30" valign="middle"></td>
<td width="240" height="30" valign="middle"></td>
<td width="80" height="30" style="border-left:solid 1px #000;border-right:solid 1px #000;border-bottom:solid 1px #000;">Reference No. </td>
<td width="110" height="30"  style="border-right:solid 1px #000;border-bottom:solid 1px #000;text-align:right;">'.@$ref2.'</td></tr>
<tr>
<td width="80" height="30" valign="middle">Cash Account</td>
<td width="350" colspan="2" height="30" valign="middle">'.$bank.'</td>
<td width="80" height="30" style="border-left:solid 1px #000;border-right:solid 1px #000;border-bottom:solid 1px #000;">Cheque No. </td>
<td width="110" height="30"  style="border-right:solid 1px #000;border-bottom:solid 1px #000;text-align:right;">'.@$cek.'</td></tr>
<tr>
<td height="30" colspan="3" style="text-align:center;font-size:14px;"></td>
</tr>
</table>';
/*
<tr>
<td height="50" colspan="3" style="text-align:center;font-size:14px;">'.$bank.' Transaction Slip</td>
</tr>
<tr>
<td width="270" height="20" style="border-left:solid 1px #000;border-top:solid 1px #000;">'.$bnk.' '.$bank_account_number.'</td>
<td width="190" height="20" style="border-top:solid 1px #000;">'.$cek.'</td>
<td width="190" height="20" style="border-right:solid 1px #000;border-top:solid 1px #000;">Amount Rp. '.price_format_pr($debit).'</td>
</tr>
*/
$html.='<table border="1" cellpadding="5" style="font-size:9px;">
<tr>
<th width="30" height="20" style="text-align:center;">No</th>
<th width="80" height="20" style="text-align:center;">No. Account</th>
<th width="140" height="20" style="text-align:center;">Account Description</th>
<th width="210" height="20"  style="text-align:center;">Description</th>
<th width="190" height="20" style="text-align:center;" >Amount</th>
</tr>';
$rowisi=count($account2);
$rownormal=9;
$rowmax=20;
/*
for($i=0;$i<$rowisi;$i++){
	$html.='
	<tr>
	<td height="10" align="center">'.$account[$i].'</td>
	<td height="10" align="right">'.price_format_pr($amount[$i]).'</td>
	<td height="10">'.$memo[$i].'</td>
	</tr>';
}
*/
$html.=$htmltengah;
$html.=$htmlakhir;
if($rowisi>$rowmax){
	$html.='
	<tr>
	<td height="10" ></td>
	<td height="10"></td>
	<td height="10" ></td>
	<td height="10"></td>
	<td height="10"></td>
	</tr>';
}else{
	if($rowisi<$rownormal){	
		for($i=0;$i<($rownormal-$rowisi);$i++){
			$html.='
			<tr>
			<td height="10" ></td>
			<td height="10"></td>
			<td height="10" ></td>
			<td height="10"></td>
			<td height="10"></td>
			</tr>';
		}
	}
}
$html.='
	<tr>
		<td height="10" colspan="3">&nbsp;</td>
		<td height="10">Sub Total Debet</td>
		<td height="10" align="right">'.price_format_pr($ttldebit).'</td>
	</tr>';
$html.='
	<tr>
		<td height="10" colspan="3">&nbsp;</td>
		<td height="10">Sub Total Kredit</td>
		<td height="10" align="right">'.price_format_pr($ttlkredit).'</td>
	</tr>';
$html.='
	<tr>
		<td height="10" colspan="3">&nbsp;</td>
		<td height="10">Total</td>
		<td height="10" align="right">'.price_format_pr($ttldebit+$ttlkredit).'</td>
	</tr>';
$html.='</table>';
$spasi='&nbsp;';
$html.='
<table>
<tr><td width="500" colspan="2"></td></tr>
<tr><td width="500">
<table border="1" cellspacing="0"  cellpadding="3">
<tr>
<th width="120" height="30" style="text-align:center;">Prepared by:
<br><br><br><br><br>
('.$spasi.$nama.$spasi.')
</th>
<th width="120" height="30" style="text-align:center;" >Checked by:
<br><br><br><br><br>
(..............................)</th>
<th width="120" height="30"  style="text-align:center;">Approved by:
<br><br><br><br><br>
(..............................)</th>
<th width="120" height="30" style="text-align:center;" >Posted by:
<br><br><br><br><br>
(..............................)</th>
</tr>
</table>
</td>';
$html.='
<td>
<table cellpadding="3">
<tr>
<th width="122" height="30"  style="text-align:center;">
Jakarta, '.$docdate.'
<br>
Receipt by:
<br><br><br><br>
(..............................)</th>
</tr>
</table>
</td></tr>	</table>';

// output the HTML content
$pdf->writeHTML($html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_voucher.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
