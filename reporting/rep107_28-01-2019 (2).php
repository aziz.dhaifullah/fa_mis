<?php
//============================================================+
// File name   : example_021.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 021 for TCPDF class
//               WriteHTML text flow
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML text flow.
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('KAM');
$pdf->SetTitle('KAM');
$pdf->SetSubject('KAM');

// set default header data
//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
$pdf->setFooterData('',0,'','',array(0,0,0), array(255,255,255) );
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

$path_to_root = "..";
include_once($path_to_root . "/includes/session.inc");

//page(_($help_context = "General Ledger Transaction Details"), true);

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/sales/includes/sales_db.inc");
include_once($path_to_root . "/sales/includes/db/sales_invoice_db.inc");
global $SysPrefs;
//print_r($SysPrefs);
$SysPrefs->prefs['gst_no'];
//exit();
	$transno = $_GET['PARAM_0'];
	$type = $_GET['type'];
		echo $so=get_sales_order($transno);
		$rowso = db_fetch($so);
		$result = get_invoice(@$transno);
		$myrow = db_fetch($result);
		//$myrow['reference'];
		$tgl=date('d M Y');
		$nomor=@$myrow['reference'];
		$jenis=@$myrow['description'];
		$produk=@$myrow['name'];
		$costumer=@$rowso['deliver_to'];
		$alamat=@$rowso['deliver_address'];
		//$kota='Jakarta';
		$kontak='';
		$curr=@$myrow['curr_code'];
		$amount=@$myrow['ov_amount'];
		$ttl=@$myrow['ov_amount']+@$myrow['ov_gst'];

// add a page
$pdf->AddPage();
/*
<tr>
<td height="30" colspan="3">Up : Bpk. '.$kontak.'</td>
</tr>
<tr>
<td height="30" colspan="3">Di. '.$kota.'</td>
</tr>
*/
$html='<table border="1" cellpadding="0">
<tr>
<td height="30" colspan="2"></td>
<td width="240" height="30" align="right" valign="top"><img src="../themes/default/images/logo_frontaccounting.png"></td>
</tr>
<tr>
<td height="35" colspan="3">Jakarta, '.$tgl.'</td>
</tr>
<tr>
<td height="20" width="45px">Nomor</td>
<td height="20" colspan="2">:&nbsp;&nbsp;&nbsp;'.$nomor.'</td>
</tr>
<tr>
<td height="30" style="margin-top:-20px;">Hal</td>
<td height="20" colspan="2">:&nbsp;&nbsp;&nbsp;Pembayaran Fee Manager Investasi atas '.$produk.'</td>
</tr>
<tr>
<td height="20" colspan="3">Kepada Yth.</td>
</tr>
<tr>
<td height="20" colspan="3">'.$costumer.'</td>
</tr>
<tr>
<td height="20" colspan="3">'.$alamat.'</td>
</tr>
<tr>
<td height="30" colspan="3">'.$kota.'</td>
</tr>
<tr>
<td height="30" colspan="3">Dengan Hormat,</td>
</tr>
<tr>
<td height="20" width="100%" colspan="3" style="text-align:justify;">Bersama surat ini, kami mohon agar pada tanggal '.@$duedate.' dapat mentransfer dana untuk pembayaran biaya '.@$jenis.' atas pengelolaan investasi '.@$produk.' setelah dipotong PPH 23 Pajak penghasilan periode '.@$bulan.'. Adapun perinciannya adalah sebagai berikut :</td>
</tr>
<tr>
<tr><td>&nbsp;</td></tr>
<tr>
<td width="400" height="40">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
'.@$jenis.'
</td>
<td width="400" height="40">
<td width="100" align="right" height="40" style="border-right:solid 1px #000;border-top:solid 1px #000;"> '.price_format($amount).'</td>
</tr>
<tr>
<td width="400" height="20">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
DPP '.@$jenis.'
</td>
<td width="50" height="20" style="border-top:solid 1px #000;">&nbsp;&nbsp;&nbsp;Rp.</td>
<td width="100" align="right" height="20" style="border-right:solid 1px #000;border-top:solid 1px #000;"> '.price_format($amount).'</td>
</tr>';

$result2 = get_invoice_tax(@$transno);
while($myrow2 = db_fetch($result2))
	{
$html.='<tr>
<td width="400" height="20">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
'.@$myrow2['name'].'
</td>
<td width="50" height="20" style="border-top:solid 1px #000;">&nbsp;&nbsp;&nbsp;Rp.</td>
<td width="100" align="right" height="20" style="border-right:solid 1px #000;border-top:solid 1px #000;"> '.price_format(@$myrow2['amount']).'</td>
</tr>';
}

$html.='
<tr>
<td width="400">&nbsp;</td>
<td width="153" height="10" colspan="2">&nbsp;&nbsp;&nbsp;--------------------------------------</td>
</tr>
<tr>
<td width="400" height="20">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Fee yang dibayarkan ke KAM Capital
</td>
<td width="50" height="30">&nbsp;&nbsp;&nbsp;Rp.</td>
<td width="100" align="right" height="30" style="border-top:solid 2px #00000;"> '.price_format($ttl).'</td>
</tr>
<tr>
<td height="30" width="100%" colspan="3">Kami harapkan agar total biaya Manager Investasi tersebut dapat ditransfer ke :</td>
</tr>';
if($curr=='IDR'){
	$bank='Permata';
	$bankcabang='BEJ';
	$ac='701.316.290';
}
if($curr=='USD'){
	$bank='Ekonomi';
	$bankcabang='-';
	$ac='2165001969';
}
$html.='
<tr>
<td width="300" height="20">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Bank
</td>
<td width="20" height="20" style="border-top:solid 1px #000;">:</td>
<td width="300" align="left" height="20" style="border-right:solid 1px #000;border-top:solid 1px #000;">'.$bank.'</td>
</tr>
<tr>
<td width="300" height="20">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Cabang
</td> 
<td width="20" height="20" style="border-top:solid 1px #000;">:</td>
<td width="300" align="left" height="20" style="border-right:solid 1px #000;border-top:solid 1px #000;">'.$bankcabang.'</td>
</tr>
<tr>
<td width="300" height="20">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
A/C
</td>
<td width="20" height="20" style="border-top:solid 1px #000;">:</td>
<td width="300" align="left" height="20" style="border-right:solid 1px #000;border-top:solid 1px #000;">'.$ac.'</td>
</tr>
<tr>
<td width="300" height="30">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Atas Nama
</td>
<td width="20" height="30" style="border-top:solid 1px #000;">:</td>
<td width="300" align="left" height="30" style="border-right:solid 1px #000;border-top:solid 1px #000;">'.$SysPrefs->prefs['coy_name'].'</td>
</tr>
<tr>
<td height="30" width="100%" colspan="3">Demikian kami sampaikan, terima kasih atas perhatiannya dan kerjasama yang diberikan.</td>
</tr>
<tr>
<td height="30" width="100%" colspan="3">Hormat Kami,</td>
</tr>
<tr>
<td height="90" width="100%" colspan="3">PT. Kresna Asset Management</td>
</tr>
<tr>
<td width="100%" colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gunanta Afrima</td>
</tr>
<tr>
<td width="100%" colspan="3">-------------------------------</td>
</tr>
<tr>
<td width="100%" colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Direktur</td>
</tr>
</table>';

// output the HTML content
$pdf->writeHTML($html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_invoice.pdf', 'I');


//============================================================+
// END OF FILE
//============================================================+
