<?php
//============================================================+
// File name   : example_021.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 021 for TCPDF class
//               WriteHTML text flow
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML text flow.
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf/tcpdf_include.php');
$path_to_root = "..";
include_once($path_to_root . "/includes/ui/items_cart.inc");
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/ui/gl_bank_ui.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/gl_ui.inc");

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('KAM');
$pdf->SetTitle('KAM');
$pdf->SetSubject('KAM');

// set default header data
//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

$path_to_root = "..";
include_once($path_to_root . "/includes/ui/items_cart.inc");
include_once($path_to_root . "/includes/session.inc");

//page(_($help_context = "General Ledger Transaction Details"), true);

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");


//print_r($_SESSION['pay_items']);
$order=$_SESSION['pay_items'];
$glcode=array();
$price=array();
$memo=array();
$total=0;
$totaldebit = 0;$totalkredit = 0;$amnt = 0;
$cash_acc=@$_GET['b'];
if($cash_acc!=''){
	$bnk=get_bank($cash_acc);
	$brow = db_fetch($bnk);
	$bank=@$brow["bank_name"];
	$bankacc=@$brow["account_code"];
	$bank_account_number=@$brow["bank_account_number"];
}
$nrow=0;
//$no = 1;
foreach ($order->gl_items as $line => $item)
{
	//print_r($item);
	//echo '<br>';
	//if($bankacc!=$item->code_id){
		//$glcode[]=$item->code_id;
		$glcode[]=$item->code_id;
		$gldesc[]=$item->description;
		$memo[]=$item->reference;
		if($order->trans_type==2){
			$price[] = -$item->amount;
			$amnt = -$item->amount;
			$total=$total+(-$item->amount);
		}
		else{
			$price[]=$item->amount;
			$amnt=$item->amount;
			$total=$total+($item->amount);
		}

		if ($amnt < 0) {
			$totalkredit+=$amnt;
		}else if($amnt > 0){
			$totaldebit+=$amnt;
		}

		$nrow++;
		//$taxes[]=$row;
	//}
}

$bnk='';$cek='';
if(@$bank_account_number!=''){

	$bnk='A/C No. ';
	$cek='Chq/BG No. ';
}
// add a page
$pdf->AddPage();


//print_r(($taxes));
/*
$tax=$_SESSION['Items']->tax_group_array;
//print_r($tax);
for($a=0;$a<count($tax);$a++) 
{
	echo $tax_type_id=$tax[$a]->tax_type_id;
	if(in_array($tax_type_id,$taxes)){
		echo $glcode[]=$tax[$a]->sales_gl_code;
		$memo[]=$tax[$a]->tax_type_name;	

		$price[]=$result3[$tax_type_id];
		$total=$total+$result3[$tax_type_id];
	}
	break;
}
/*
foreach($result3 as $row=>$value) 
{
	//print_r($tax[$row]);
	$glcode[]=$tax[$row]->sales_gl_code;
	$memo[]=$tax[$row]->tax_type_name;
	$price[]=$value;
	$total=$total+$row;
}
*/
$image='../themes/default/images/logo_frontaccounting.png';
$com=@$_SESSION["wa_current_user"]->com_id;
if($com!=''){
	$sql = "SELECT * FROM 0_companies where id='".$_SESSION["wa_current_user"]->com_id."' ;";
			//print_r($_SESSION["wa_current_user"]->com_id);exit;
	
	if($result=db_query($sql)){
		$image='../images/';$img='';
		while($row=db_fetch($result)){
			//print_r($row);
			$img=$row['logo'];
			if (@$_SESSION["wa_current_user"]->com_id == 0) {
				$headtitle = 'Mega Inti Supra';
			}else{
				$headtitle = $row['nama'];
			}
		//print_r($headtitle);
		}
		if ($_SESSION["wa_current_user"]->com_id == 0) {
			$img = 'logo_kresna.png';
		}
		$image.=$img;
	}

}

/*$html='<table border="0" cellpadding="5" style="font-size:11px;">
<tr>
<td width="440" height="30" rowspan="2" valign="middle"><img src="../themes/default/images/logo_frontaccounting.png"></td>
<td width="80" height="30" style="border-left:solid 1px #000;border-right:solid 1px #000;border-top:solid 1px #000;border-bottom:solid 1px #000;" >Date </td>
<td width="110" height="30" style="border-right:solid 1px #000;border-top:solid 1px #000;border-bottom:solid 1px #000;text-align:right;" >'.@$_SESSION['pay_items']->tran_date.'</td>
</tr>
<tr>
<td width="80" height="30" style="border-left:solid 1px #000;border-bottom:solid 1px #000;border-right:solid 1px #000;">Reff Number </td>
<td width="110" height="30"  style="border-right:solid 1px #000;border-bottom:solid 1px #000;text-align:right;">'.@$_GET['r'].'</td>
</tr>
<tr>
<td height="50" colspan="3" style="text-align:center;font-size:14px;">'.@$bank.' Transaction Slip</td>
</tr>
<tr>
<td width="270" height="20" style="border-left:solid 1px #000;border-top:solid 1px #000;">'.$bnk.@$bank_account_number.'</td>
<td width="190" height="20" style="border-top:solid 1px #000;">'.$cek.'</td>
<td width="190" height="20" style="border-right:solid 1px #000;border-top:solid 1px #000;">Amount Rp. '.price_format($total).'</td>
</tr>
</table>';*/

//echo $headtitle;
/*<td width="440" height="30" valign="middle"><img src="'.$image.'" width="100" ></td>*/
$html='<table border="0" cellpadding="5" style="font-size:11px;">
<tr>
<td width="440" height="30" valign="middle"><h1>'.$headtitle.'</h1></td>
</tr>
<tr>
<td width="80" height="30" valign="middle"></td>
<td width="110" height="30" valign="middle"></td>
<td width="240" height="30" valign="middle"></td>
<td width="80" height="30" style="border-top:solid 1px #000;border-left:solid 1px #000;border-right:solid 1px #000;border-bottom:solid 1px #000;">Voucher No. </td>
<td width="110" height="30"  style="border-top:solid 1px #000;border-right:solid 1px #000;border-bottom:solid 1px #000;text-align:right;">'.@$_GET['r'].'</td></tr>
<tr>
<td width="80" height="30" valign="middle"></td>
<td width="110" height="30" valign="middle"></td>
<td width="240" height="30" valign="middle"></td>
<td width="80" height="30" style="border-left:solid 1px #000;border-right:solid 1px #000;border-bottom:solid 1px #000;" >Voucher Date </td>
<td width="110" height="30" style="border-right:solid 1px #000;border-top:solid 1px #000;border-bottom:solid 1px #000;text-align:right;" >'.@$_SESSION['pay_items']->tran_date.'</td>
</tr>
<tr>
<td width="80" height="30" valign="middle"></td>
<td width="110" height="30" valign="middle"></td>
<td width="240" height="30" valign="middle"></td>
<td width="80" height="30" style="border-left:solid 1px #000;border-right:solid 1px #000;border-bottom:solid 1px #000;">Journal No. </td>
<td width="110" height="30"  style="border-right:solid 1px #000;border-bottom:solid 1px #000;text-align:right;">'.@$jur.'</td></tr>
<tr>
<td width="80" height="30" valign="middle"></td>
<td width="110" height="30" valign="middle"></td>
<td width="240" height="30" valign="middle"></td>
<td width="80" height="30" style="border-left:solid 1px #000;border-right:solid 1px #000;border-bottom:solid 1px #000;">Reference No. </td>
<td width="110" height="30"  style="border-right:solid 1px #000;border-bottom:solid 1px #000;text-align:right;">'.@$ref2.'</td></tr>
<tr>
<td width="80" height="30" valign="middle">Cash Account</td>
<td width="350" colspan="2" height="30" valign="middle">'.$bank.'</td>
<td width="80" height="30" style="border-left:solid 1px #000;border-right:solid 1px #000;border-bottom:solid 1px #000;">Cheque No. </td>
<td width="110" height="30"  style="border-right:solid 1px #000;border-bottom:solid 1px #000;text-align:right;">'.@$cek.'</td></tr>
<tr>
<td height="30" colspan="3" style="text-align:center;font-size:14px;"></td>
</tr>
</table>';

/*$html.='<table border="1" cellpadding="5" style="font-size:9px;">
<tr>
<th width="150" height="20" style="text-align:center;">No. Account</th>
<th width="190" height="20" style="text-align:center;" >Amount</th>
<th width="310" height="20"  style="text-align:center;">Description</th>
</tr>';*/
$html.='<table border="1" cellpadding="5" style="font-size:9px;">
<tr>
<th width="30" height="20" style="text-align:center;">No</th>
<th width="80" height="20" style="text-align:center;">No. Account</th>
<th width="140" height="20" style="text-align:center;">Account Description</th>
<th width="210" height="20"  style="text-align:center;">Description</th>
<th width="190" height="20" style="text-align:center;" >Amount</th>
</tr>';
$rowisi=count(@$glcode);
$rownormal=9;
$rowmax=19;
/*
for($i=0;$i<$rowisi;$i++){
	$html.='
	<tr>
	<td height="10" align="center">'.$account[$i].'</td>
	<td height="10" align="right">'.price_format($amount[$i]).'</td>
	<td height="10">'.$memo[$i].'</td>
	</tr>';
}
*/
$no = 1;
for($a=0;$a<($nrow);$a++) 
{
	if($bankacc!=$glcode[$a]){
		if(trim(@$memo[$a])==''){
			$sql  = "SELECT account_name FROM `0_chart_master` where account_code like '".$glcode[$a]."'" ;
			$result = db_query($sql);
			$result2 = db_fetch_row($result);
			$memo[$a] = $result2[0];
		}
		$html.='
		<tr>
		<td height="10" align="center">'.$no.'</td>
		<td height="10" align="center">'.$glcode[$a].'</td>
		<td height="10">'.$gldesc[$a].'</td>
		<td height="10">'.(@$memo[$a]).'</td>
		<td height="10" align="right">'.price_format_pr($price[$a]).'</td>
		</tr>';
	}
	$no+=1;
}		
if($rowisi<$rownormal){	
	for($i=0;$i<($rownormal-$rowisi);$i++){
		$html.='
		<tr>
		<td height="10" ></td>
		<td height="10"></td>
		<td height="10"></td>
		<td height="10"></td>
		<td height="10"></td>
		</tr>';
	}
}
$html.='
	<tr>
		<td height="10" colspan="3">&nbsp;</td>
		<td height="10">Sub Total Debet</td>
		<td height="10" align="right">'.price_format_pr($totaldebit).'</td>
	</tr>';
$html.='
	<tr>
		<td height="10" colspan="3">&nbsp;</td>
		<td height="10">Sub Total Kredit</td>
		<td height="10" align="right">'.price_format_pr($totalkredit).'</td>
	</tr>';
$html.='
	<tr>
		<td height="10" colspan="3">&nbsp;</td>
		<td height="10">Total</td>
		<td height="10" align="right">'.price_format_pr($totaldebit+$totalkredit).'</td>
	</tr>';
$html.='</table>';
/*$html.='<table border="1" cellspacing="8"  cellpadding="3">
<tr>
<th width="120" height="30" style="text-align:center;">Prepared by:
<br><br><br><br>
(..............................)
</th>
<th width="120" height="30" style="text-align:center;" >Checked by:
<br><br><br><br>
(..............................)</th>
<th width="120" height="30"  style="text-align:center;">Approved by:
<br><br><br><br>
(..............................)</th>
<th width="120" height="30" style="text-align:center;" >Input by:
<br><br><br><br>
(..............................)</th>
<th width="122" height="30"  style="text-align:center;">Received by:
<br><br><br><br>
(..............................)</th>
</tr>';
$html.='</table>';

// output the HTML content
$pdf->writeHTML($html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_voucher.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
*/
$spasi='&nbsp;';
$html.='
<table>
<tr><td width="500" colspan="2"></td></tr>
<tr><td width="500">
<table border="1" cellspacing="0"  cellpadding="3">
<tr>
<th width="120" height="30" style="text-align:center;">Prepared by:
<br><br><br><br><br>
('.$spasi.$_GET['u'].$spasi.')
</th>
<th width="120" height="30" style="text-align:center;" >Checked by:
<br><br><br><br><br>
(..............................)</th>
<th width="120" height="30"  style="text-align:center;">Approved by:
<br><br><br><br><br>
(..............................)</th>
<th width="120" height="30" style="text-align:center;" >Posted by:
<br><br><br><br><br>
(..............................)</th>
</tr>
</table>
</td>';
$html.='
<td>
<table cellpadding="3">
<tr>
<th width="122" height="30"  style="text-align:center;">
Jakarta, '.@$_SESSION['pay_items']->tran_date.'
<br>
Receipt by:
<br><br><br><br>
(..............................)</th>
</tr>
</table>
</td></tr>	</table>';

// output the HTML content
$pdf->writeHTML($html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_voucher.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
