<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_TAXRATES';
$path_to_root = "..";

include($path_to_root . "/includes/session.inc");
page(_($help_context = "Tax Types"));

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/taxes/db/tax_types_db.inc");

simple_page_mode(true);
//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------

if (isset($_POST['upload']))
{

	include_once ( "excel_reader2.php");
	$file=$_FILES["fileToUpload"]["tmp_name"];
	$data = new Spreadsheet_Excel_Reader($file);
	$j = 0;
	//print_r($data);exit;
	for ($i=2; $i <= ($data->rowcount($sheet_index=0)); $i++){ 
		$tgl   = $data->val($i, 1);
		if($tgl!=''){
			$tgls=explode('/',$tgl);
			$no_urut  = $data->val($i, 2);
			$no_faktur    = $data->val($i, 3);
			$cek=cek_no_faktur( str_replace(',','',$no_faktur));
			if($cek<1)
			add_no_faktur($tgls[2].'-'.$tgls[0].'-'.$tgls[1], $no_urut, str_replace(',','',$no_faktur));
			//print_r($values);
			$j++;
		} 
	}
	/*
	$wth=(@$_POST['wth']==''?'0':@$_POST['wth']);
	add_tax_type($_POST['name'], $_POST['sales_gl_code'],
		$_POST['purchasing_gl_code'], $_POST['rate'],@$wth );
	
	display_notification(_('New tax type has been added'));
	$Mode = 'RESET';
	*/
}

$result = get_no_faktur();

//display_note(_("To avoid problems with manual journal entry all tax types should have unique Sales/Purchasing GL accounts."), 0, 1);
start_table(TABLESTYLE,'',10);

$th = array(_("Tanggal"), _("No Urut"),
	_("No Faktur Pajak"),
	_("Masa"),
	_("&nbsp;"),
	_("&nbsp;"));
inactive_control_column($th);
table_header($th);

$k = 0;
while ($myrow = db_fetch($result))
{

	alt_table_row_color($k);
	$tgl=date('d M Y',strtotime($myrow["tgl"]));
	$tgl2='-';
	if(@$myrow["masa"]!='')
	$tgl2=date('d M Y',strtotime($myrow["masa"]));

	label_cell($tgl);
	label_cell($myrow["no_urut"]);
	label_cell($myrow["no_faktur"],'align="right"');
	label_cell($tgl2);
	label_cell('');
	label_cell('');

	end_row();
}

//inactive_control_row($th);
end_table(1);
//-----------------------------------------------------------------------------------

start_table(TABLESTYLE2);
echo '<form enctype="multipart/form-data" action="" method="post">';
echo '<tr><td class="label">Upload No Faktur Pajak:</td>
<td><input type="file" name="fileToUpload" id="fileToUpload" ></td>
</tr>';
echo '<tr><td class="label" colspan="2"><a href="template_upload_faktur_pajak.xls">Download Template</a></td></tr>';
echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>';
echo '<tr><td class="label" colspan="2" align="center"><input type="submit" class="export" name="upload" Value="Upload"></td></tr>';
echo '</form>';
end_table(1);

//submit_add_or_update_center($selected_id == -1, '', 'both');

end_page();

