<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_TAXRATES';
$path_to_root = "..";

include($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/taxes/db/tax_types_db.inc");

$js = "";
if (user_use_date_picker())
	$js = get_js_date_picker();

page(_($help_context = "No Faktur Pajak"), $js);
simple_page_mode(true);

//-----------------------------------------------------------------------------------

function can_process($new) 
{

	if (strlen($_POST['tanggal']) == 0 )
	{
		display_error( _("keycode entered max 32 characters long."));
		return false;
	}

	if (strlen($_POST['no_faktur_awal']) > 32 )
	{
		display_error( _("label entered max 32 characters long."));
		return false;
	}else if (strlen($_POST['no_faktur_awal']) < 1 ) {
		display_error( _("label cannot be empty"));
		return false;
	}

	if (strlen($_POST['no_faktur_awal']) > 32 )
	{
		display_error( _("value entered max 32 characters long."));
		return false;
	}else if (strlen($_POST['no_faktur_awal']) < 1 ) {
		display_error( _("value cannot be empty"));
		return false;
	}


	return true;
}

//-------------------------------------------------------------------------------------------------

if (($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') && check_csrf_token())
{
	if (can_process($Mode == 'ADD_ITEM'))
	{
    	if ($selected_id != -1) 
    	{
    		update_param($selected_id,$_POST['tanggal'], $_POST['no_urut'],$_POST['no_faktur']);

    		display_notification_centered(_("The selected parameter has been updated."));
    	} 
    	else 	
    	{	
    		$tanggal = explode("/", $_POST['tanggal']);
    		$tanggal2 = $tanggal[2]."-".$tanggal[1]."-".$tanggal[0];
    		//echo $tanggal2;
    		$noFakturLength = strlen($_POST['no_faktur_awal']);
    		$noFakturString = substr($_POST['no_faktur_awal'],0,$noFakturLength-3);
    		$noFkAwalInt = (int)substr($_POST['no_faktur_awal'],-3);
    		$noFkAkhirInt = (int)substr($_POST['no_faktur_akhir'],-3);;
    		$TotalDataFaktur = $noFkAkhirInt - $noFkAwalInt;
    		for ($i=0; $i <= $TotalDataFaktur; $i++) { 
    			$noFaktur = $noFakturString.(string)$noFkAwalInt;
				//echo $noFaktur."<br>";
				//echo $i+1;
    			add_no_faktur($tanggal2, $i+1, $noFaktur);
    			$noFkAwalInt+=1;
    		}
//			$id = db_insert_id();
			
			display_notification_centered(_("A new parameter has been added."));
    	}
		$Mode = 'RESET';
	}
}

//-------------------------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
	
    	delete_no_faktur($selected_id);
    	$Mode = 'RESET';
}
//-------------------------------------------------------------------------------------------------
if ($Mode == 'RESET')
{
 	$selected_id = -1;
	$sav = get_post('show_inactive', null);
	unset($_POST);	// clean all input fields
	$_POST['show_inactive'] = $sav;
}
//-----------------------------------------------------------------------------------

if (isset($_POST['upload']))
{

	include_once ( "excel_reader2.php");
	$file=$_FILES["fileToUpload"]["tmp_name"];
	$data = new Spreadsheet_Excel_Reader($file);
	$j = 0;
	//print_r($data);exit;
	for ($i=2; $i <= ($data->rowcount($sheet_index=0)); $i++){ 
		$tgl   = $data->val($i, 1);
		if($tgl!=''){
			$tgls=explode('/',$tgl);
			$no_urut  = $data->val($i, 2);
			$no_faktur    = $data->val($i, 3);
			$cek=cek_no_faktur( str_replace(',','',$no_faktur));
			if($cek<1)
			add_no_faktur($tgls[2].'-'.$tgls[0].'-'.$tgls[1], $no_urut, str_replace(',','',$no_faktur));
			//print_r($values);
			$j++;
		} 
	}
	/*
	$wth=(@$_POST['wth']==''?'0':@$_POST['wth']);
	add_tax_type($_POST['name'], $_POST['sales_gl_code'],
		$_POST['purchasing_gl_code'], $_POST['rate'],@$wth );
	
	display_notification(_('New tax type has been added'));
	$Mode = 'RESET';
	*/
}



$result = get_no_faktur();
start_form();
start_table(TABLESTYLE2);

if ($selected_id != -1) 
{
  	if ($Mode == 'Edit') {
		//editing an existing parameter
		$myrow = get_no_faktur_edit($selected_id);

		$_POST['tanggal'] = $myrow["tgl"];
		$_POST['no_urut'] = $myrow["no_urut"];
		$_POST['no_faktur'] = $myrow["no_faktur"];
	}
	hidden('selected_id', $selected_id);
	start_row();
	date_cells(_("Tanggal").":", 'tanggal');
	text_row_ex(_("Nomor Urut").":", 'no_urut',  100);
	text_row_ex(_("Nomor Faktur"), 'no_faktur', 100); 
}else{
//date_cells(_("&nbsp;"), 'TransFromDate');
	date_cells(_("Tanggal").":", 'tanggal');
	text_row_ex(_("Nomor Faktur Awal").":", 'no_faktur_awal',  100);

	text_row_ex(_("Nomor Faktur Akhir"), 'no_faktur_akhir', 100);
} 



echo '
	<style>
		.export{
		    align-items: flex-start;
		    text-align: center;
		    cursor: default;
		    color: buttontext;
		    background-color: buttonface;
		    box-sizing: border-box;
		    vertical-align: top;
		    -webkit-appearance: none;
		    border-style: solid;
		    border: 1px #0066cc solid;
		    padding-left: 10px;
		    padding-right: 10px;
		}
	</style>
	<script src="../jquery.min.js"></script>
	<script src="../jquery.table2excel.js"></script>';


end_table(1);
submit_add_or_update_center($selected_id == -1, '', 'both');


//display_note(_("To avoid problems with manual journal entry all tax types should have unique Sales/Purchasing GL accounts."), 0, 1);
start_table(TABLESTYLE,'',10);

$th = array(_("Tanggal"), _("No Urut"),
	_("No Faktur Pajak"),
	_("Masa"),
	_("Status"),
	_("&nbsp;"),
	_("&nbsp;"));
inactive_control_column($th);
table_header($th);

$k = 0;
$j = 0;
while ($myrow = db_fetch($result))
{

	alt_table_row_color($k);
	$tgl=date('d M Y',strtotime($myrow["tgl"]));
	$tgl2='-';
	if(@$myrow["masa"]!='')
	$tgl2=date('d M Y',strtotime($myrow["masa"]));

	label_cell($tgl);
	label_cell($myrow["no_urut"]);
	label_cell($myrow["no_faktur"],'align="right"');
	label_cell($tgl2);
	if ($myrow["status"] == 0) {
		$status = "Belum Dipakai";
	}elseif ($myrow["status"] == 1) {
		$status = "Sudah Dipakai";
	}elseif ($myrow["status"] == 2) {
		$status = "Dibatalkan";
	}
	label_cell($status);
	edit_button_cell("Edit".$myrow[0], _("Edit"));
	if ($myrow["id"] != 1)
 		delete_button_cell("Delete".$myrow[0], _("Delete"));
	else
		label_cell('');

	end_row();
}


//inactive_control_row($th);
end_table(1);
//-----------------------------------------------------------------------------------



end_form();
end_page();

