<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
class banking extends application
{
	function banking()
	{
		$this->application("BK", _($this->help_context = "&Banking"));

		
		$this->add_module(_("Transactions"));
		$this->add_lapp_function(0, _("&Spend Money"),	"gl/gl_bank.php?NewPayment=Yes", 'SA_PAYMENT', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("&Spend Money For Entertainment"),	"gl/gl_bank_entertain.php?NewPayment=Yes", 'SA_PAYMENT', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("&Spend Money For Iklan & Promosi"),	"gl/gl_bank_promosi.php?NewPayment=Yes", 'SA_PAYMENT', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("&Spend Money For Marketing"),	"gl/gl_bank_marketing.php?NewPayment=Yes", 'SA_PAYMENT', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("&Receive Money"),	"gl/gl_bank.php?NewDeposit=Yes", 'SA_DEPOSIT', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("Bank Account &Transfers"), "gl/bank_transfer.php?", 'SA_BANKTRANSFER', MENU_TRANSACTION);
		$this->add_rapp_function(0, _("&Journal Entry"),"gl/gl_journal.php?NewJournal=Yes", 'SA_JOURNALENTRY', MENU_TRANSACTION);
		$this->add_rapp_function(0, _("&Journal Entry Memorial"),"gl/gl_journal_memorial.php?NewJournal=Yes", 'SA_JOURNALENTRY', MENU_TRANSACTION);
		$this->add_rapp_function(0, _("&Budget Entry"),"gl/gl_budget.php?", 'SA_BUDGETENTRY', MENU_TRANSACTION);
		//$this->add_rapp_function(0, _("&Reconcile Bank Account"),"gl/bank_account_reconcile.php?", 'SA_RECONCILE', MENU_TRANSACTION);
		/*$this->add_rapp_function(0, _("&Export To MYOB (Disbursement)"),"gl/inquiry/export_myob_dirs.php?", 'SA_RECONCILE', MENU_TRANSACTION);
		$this->add_rapp_function(0, _("&Export To MYOB (Receipts)"),"gl/inquiry/export_myob_rec.php?", 'SA_RECONCILE', MENU_TRANSACTION);
		$this->add_rapp_function(0, _("&Export To MYOB (Memorial)"),"gl/inquiry/export_myob.php?", 'SA_RECONCILE', MENU_TRANSACTION);*/
		
		$this->add_module(_("Inquiries and Reports"));
		$this->add_lapp_function(1, _("Bank Account &Inquiry"),"gl/inquiry/bank_inquiry.php?", 'SA_BANKTRANSVIEW', MENU_INQUIRY);
		$this->add_lapp_function(1, _("&Journal Inquiry"),"gl/inquiry/journal_inquiry.php?", 'SA_GLANALYTIC', MENU_INQUIRY);
		
		$this->add_rapp_function(1, _("Banking &Reports"),"reporting/reports_main.php?Class=5", 'SA_BANKREP', MENU_REPORT);
		$this->add_rapp_function(1, _("General Ledger &Reports"),"reporting/reports_main.php?Class=6", 'SA_GLREP', MENU_REPORT);

		$this->add_module(_("Maintenance"));
		$this->add_lapp_function(2, _("&Quick Entries"),"gl/manage/gl_quick_entries.php?", 'SA_QUICKENTRY', MENU_MAINTENANCE);
		$this->add_lapp_function(2, _("Bank &Accounts"),"gl/manage/bank_accounts.php?", 'SA_BANKACCOUNT', MENU_MAINTENANCE);
		
		$this->add_extensions();
	}
}


