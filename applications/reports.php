<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
class report_app extends application
{
	function report_app()
	{
		$this->application("RPT", _($this->help_context = "&Reports"));

		$this->add_module(_("Sales"));

		$this->add_lapp_function(0, _("Sales Quotation I&nquiry"),
			"sales/inquiry/sales_orders_view.php?type=32", 'SA_SALESTRANSVIEW', MENU_INQUIRY);
		$this->add_lapp_function(0, _("Sales Order &Inquiry"),
			"sales/inquiry/sales_orders_view.php?type=30", 'SA_SALESTRANSVIEW', MENU_INQUIRY);
		$this->add_lapp_function(0, _("Customer Transaction &Inquiry"),
			"sales/inquiry/customer_inquiry.php?", 'SA_SALESTRANSVIEW', MENU_INQUIRY);
		$this->add_lapp_function(0, _("Customer Allocation &Inquiry"),
			"sales/inquiry/customer_allocation_inquiry.php?", 'SA_SALESALLOC', MENU_INQUIRY);
		
		$this->add_rapp_function(0, _("AR Management Fee"),
			"gl/inquiry/gl_ar.php", 'SA_SALESALLOC', MENU_INQUIRY);
		$this->add_rapp_function(0, _("<u>A</u>ged Customer Analyses"),
			"reporting/reports_main.php?Class=0&REP_ID=102", 'SA_SALESALLOC', MENU_INQUIRY);
		/*
		$this->add_rapp_function(0, _("Customer and Sales &Reports"),
			"reporting/reports_main.php?Class=0", 'SA_SALESTRANSVIEW', MENU_REPORT);
		$this->add_rapp_function(0, _("Inquiries and &Reports"),
			"gl/inquiry/invoice_inquiry.php?", 'SA_SALESTRANSVIEW', MENU_REPORT);
		*/
		
		$this->add_module(_("Purchase"));

		$this->add_lapp_function(1, _("Purchase Orders &Inquiry"),
			"purchasing/inquiry/po_search_completed.php?", 'SA_SUPPTRANSVIEW', MENU_INQUIRY);
		$this->add_lapp_function(1, _("Supplier Transaction &Inquiry"),
			"purchasing/inquiry/supplier_inquiry.php?", 'SA_SUPPTRANSVIEW', MENU_INQUIRY);
		$this->add_lapp_function(1, _("Supplier Allocation &Inquiry"),
			"purchasing/inquiry/supplier_allocation_inquiry.php?", 'SA_SUPPLIERALLOC', MENU_INQUIRY);

		$this->add_rapp_function(1, _("<u>A</u>ged Supplier Analyses"),
			"reporting/reports_main.php?Class=0&REP_ID=202'", 'SA_SALESALLOC', MENU_INQUIRY);
		/*
		$this->add_rapp_function(1, _("Supplier and Purchasing &Reports"),
			"reporting/reports_main.php?Class=1", 'SA_SUPPTRANSVIEW', MENU_REPORT);
		*/
		$this->add_module(_("Banking & GL"));
		$this->add_lapp_function(2, _("List Nominatif Marketing"),
			"gl/inquiry/gl_marketing.php?", 'SA_SALESALLOC', MENU_INQUIRY);
		$this->add_lapp_function(2, _("List Nominatif Entertainment"),
			"gl/inquiry/gl_entertaint.php?", 'SA_SALESALLOC', MENU_INQUIRY);
		$this->add_lapp_function(2, _("List Nominatif Sponsorship"),
			"gl/inquiry/gl_sponsor.php?", 'SA_SALESALLOC', MENU_INQUIRY);
		$this->add_lapp_function(2, _("&Journal Inquiry"),"gl/inquiry/journal_inquiry.php?", 'SA_GLANALYTIC', MENU_INQUIRY);
		$this->add_lapp_function(2, _("GL &Inquiry"),"gl/inquiry/gl_account_inquiry.php?", 'SA_GLTRANSVIEW', MENU_INQUIRY);
		$this->add_lapp_function(2, _("GL Subsidiaries &Inquiry"),"gl/inquiry/gl_subaccount_inquiry.php?", 'SA_GLTRANSVIEW', MENU_INQUIRY);
		$this->add_lapp_function(2, _("Bank Account &Inquiry"),"gl/inquiry/bank_inquiry.php?", 'SA_BANKTRANSVIEW', MENU_INQUIRY);
		$this->add_lapp_function(2, _("Budget "),"gl/inquiry/budget.php?", 'SA_GLANALYTIC', MENU_INQUIRY);
		$this->add_lapp_function(2, _("Actual Vs Actual"),"gl/inquiry/budget_actual.php?", 'SA_GLANALYTIC', MENU_INQUIRY);
		//$this->add_lapp_function(2, _("Ta&x Inquiry"),"gl/inquiry/tax_inquiry.php?", 'SA_TAXREP', MENU_INQUIRY);
		$this->add_lapp_function(2, _("Pajak Masukan"),"gl/inquiry/ppnm.php?", 'SA_TAXREP', MENU_INQUIRY);
		$this->add_lapp_function(2, _("Pajak Keluaran"),"gl/inquiry/ppnk.php?", 'SA_TAXREP', MENU_INQUIRY);
		$this->add_lapp_function(2, _("Rekap PPN"),"gl/inquiry/ppn.php?", 'SA_TAXREP', MENU_INQUIRY);
		$this->add_lapp_function(2, _("Rekap PPH"),"gl/inquiry/pph.php?", 'SA_TAXREP', MENU_INQUIRY);
		//$this->add_lapp_function(2, _("PPH Ps. 4(2)"),"gl/inquiry/pph4.php?", 'SA_TAXREP', MENU_INQUIRY);

		$this->add_rapp_function(2, _("Trial &Balance"),"gl/inquiry/gl_trial_balance.php?", 'SA_GLANALYTIC', MENU_INQUIRY);
		$this->add_rapp_function(2, _("Balance &Sheet Builder"),"gl/inquiry/balance_sheet.php?", 'SA_GLANALYTIC', MENU_INQUIRY);
		$this->add_rapp_function(2, _("Balance &Sheet Console"),"gl/inquiry/balance_sheet_detail.php?", 'SA_GLANALYTIC', MENU_INQUIRY);
		/*$this->add_rapp_function(2, _("Balance &Sheet (Myob)"),"gl/inquiry/balance_sheet_myob.php?", 'SA_GLANALYTIC', MENU_INQUIRY);*/
		/*$this->add_rapp_function(2, _("Laporan Posisi Keuangan "),"gl/inquiry/balance_sheet_summary.php?", 'SA_GLANALYTIC', MENU_INQUIRY);*/
		$this->add_rapp_function(2, _("&Profit and Loss Builder"),"gl/inquiry/profit_loss.php?", 'SA_GLANALYTIC', MENU_INQUIRY);
		$this->add_rapp_function(2, _("&Profit and Loss Console"),"gl/inquiry/profit_loss_detail.php?", 'SA_GLANALYTIC', MENU_INQUIRY);
		/*$this->add_rapp_function(2, _("&Profit and Loss (Myob)"),"gl/inquiry/profit_loss_myob.php?", 'SA_GLANALYTIC', MENU_INQUIRY);*/
		/*$this->add_rapp_function(2, _("Laporan Laba Rugi Komprehensif"),"gl/inquiry/profit_loss_summary.php?", 'SA_GLANALYTIC', MENU_INQUIRY);*/
		//$this->add_rapp_function(2, _("Cash Flow"),"gl/inquiry/gl_cash.php?", 'SA_GLANALYTIC', MENU_INQUIRY);
		/*$this->add_rapp_function(2, _("Cash Flow"),"gl/inquiry/cashflow.php?", 'SA_GLANALYTIC', MENU_INQUIRY);*/
		//$this->add_rapp_function(2, _("Cash Flow"),"reporting/reports_main.php?Class=5&REP_ID=_cash_flow_statement", 'SA_GLANALYTIC', MENU_INQUIRY);
		//Statement of Changes in Equity
		/*$this->add_rapp_function(2, _("Statement of Changes in Equity"),"gl/inquiry/equity.php", 'SA_GLANALYTIC', MENU_INQUIRY);*/
		//$this->add_rapp_function(1, _("Banking &Reports"),"reporting/reports_main.php?Class=5", 'SA_BANKREP', MENU_REPORT);
		//$this->add_rapp_function(1, _("General Ledger &Reports"),"reporting/reports_main.php?Class=6", 'SA_GLREP', MENU_REPORT);

		/*$this->add_module(_("From SIAR"));
		$this->add_lapp_function(3, _("AUM"),"gl/inquiry/gl_aum.php?", 'SA_SALESALLOC', MENU_INQUIRY);
		$this->add_lapp_function(3, _("NAB"),"gl/inquiry/gl_nab.php?", 'SA_SALESALLOC', MENU_INQUIRY);
		$this->add_lapp_function(3, _("NAB KAM"),"gl/inquiry/gl_nab_syai.php?", 'SA_SALESALLOC', MENU_INQUIRY);
		$this->add_lapp_function(3, _("Portofolio Efek"),"gl/inquiry/gl_saham.php?", 'SA_SALESALLOC', MENU_INQUIRY);
		$this->add_lapp_function(3, _("Deposito"),"gl/inquiry/gl_desposito.php?", 'SA_SALESALLOC', MENU_INQUIRY);
		$this->add_rapp_function(3, _("Management Fee"),"gl/inquiry/gl_mfee.php?", 'SA_GLANALYTIC', MENU_INQUIRY);
		$this->add_rapp_function(3, _("Management Fee per Customer"),"gl/inquiry/gl_mfee_percustomer.php?", 'SA_GLANALYTIC', MENU_INQUIRY);
		$this->add_rapp_function(3, _("Management Fee per Customer SIAR"),"gl/inquiry/gl_mfee2.php?", 'SA_GLANALYTIC', MENU_INQUIRY);
		$this->add_rapp_function(3, _("Sharing Fee"),"gl/inquiry/gl_shrfee.php?", 'SA_GLANALYTIC', MENU_INQUIRY);
		$this->add_rapp_function(3, _("PPH 23 Broker"),"gl/inquiry/gl_broker.php?", 'SA_SALESALLOC', MENU_INQUIRY);*/
		//$this->add_rapp_function(3, _("Other Fee"),"gl/inquiry/gl_other.php?", 'SA_GLANALYTIC', MENU_INQUIRY);

		/*$this->add_module(_("MKBD"));
		$this->add_lapp_function(4, _("Mapping COA - MKBD"),"gl/inquiry/mkbd_coa.php?", 'SA_SALESALLOC', MENU_INQUIRY);
		$this->add_lapp_function(4, _("Detail Other Asset & Liab"),"gl/inquiry/asset_liab.php?", 'SA_SALESALLOC', MENU_INQUIRY);
		$this->add_rapp_function(4, _("Excel"),"gl/inquiry/gl_mkbd.php?", 'SA_SALESALLOC', MENU_INQUIRY);
		$this->add_rapp_function(4, _("Txt"),"gl/inquiry/gl_mkbd_txt.php?", 'SA_SALESALLOC', MENU_INQUIRY);
		$this->add_extensions();*/
	}
}


