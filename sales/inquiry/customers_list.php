<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
/**********************************************************************
  Page for searching customer list and select it to customer selection
  in pages that have the supplier dropdown lists.
  Author: bogeyman2007 from Discussion Forum. Modified by Joe Hunt
***********************************************************************/
$page_security = "SA_SALESORDER";
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/sales/includes/db/customers_db.inc");
include_once($path_to_root . "/gl/includes/siar.inc");
include_once($path_to_root . "/gl/includes/siap.inc");
include_once($path_to_root . "/gl/includes/db/gl_db_banking.inc");

$mode = get_company_pref('no_customer_list');
if ($mode != 0)
	$js = get_js_set_combo_item();
else
	$js = get_js_select_combo_item();

page(_($help_context = "Customers"), true, false, "", $js);

if(get_post("search")) {
  $Ajax->activate("customer_tbl");
}

start_form(false, false, $_SERVER['PHP_SELF'] . "?" . $_SERVER['QUERY_STRING']);

start_table(TABLESTYLE_NOBORDER);

start_row();

text_cells(_("Customer"), "customer");
submit_cells("search", _("Search"), "", _("Search customers"), "default");
echo '<td><button onclick="createform()" name="create" id="create" value="Create Costumer" title="Create suppliers"><span>New Costumer</span></button></td>';
echo '<td><button type="submit" name="addsiar" id="addsiar" value="Add Costumer from SIAR" title="Add Costumer from SIAR"><span>Add Costumer from SIAR</span></button></td>';

end_row();

end_table();

end_form();

div_start("customer_tbl");

if(@get_post("submitcostumer")!=''){
  global $path_to_root, $Ajax, $SysPrefs;
  //echo get_post("supp_name");begin_transaction();
  add_customer2(get_post("CustName"), get_post("cust_ref"), get_post("address"), get_post("gst_no"));

    $selected_id = $_POST['customer_id'] = db_insert_id();
         
    if (isset($SysPrefs->auto_create_branch) && $SysPrefs->auto_create_branch == 1)
    {
          add_branch($selected_id, $_POST['CustName'], $_POST['cust_ref'],
                $_POST['address'], @$_POST['salesman'], @$_POST['area'], @$_POST['tax_group_id'], '',
                get_company_pref('default_sales_discount_act'), get_company_pref('debtors_act'), get_company_pref('default_prompt_payment_act'),
                @$_POST['location'], $_POST['address'], 0, @$_POST['ship_via'], @$_POST['notes'], @$_POST['bank_account']);
                
          $selected_branch = db_insert_id();
        
      add_crm_person($_POST['cust_ref'], $_POST['CustName'], '', $_POST['address'], 
        @$_POST['phone'], @$_POST['phone2'], @$_POST['fax'], @$_POST['email'], '', '');

      $pers_id = db_insert_id();
      add_crm_contact('cust_branch', 'general', $selected_branch, $pers_id);

      add_crm_contact('customer', 'general', $selected_id, $pers_id);
    }
    commit_transaction();
}
if(@get_post("addsiar")!=''){
  global $path_to_root, $Ajax, $SysPrefs;
  $result0=get_customers_search('');
  $isifa=array();
  while ($myrow0 = db_fetch_assoc($result0)) {
    $isifa[]=$myrow0["name"];
  }
  $prodsiar=get_prod_siap();
  $tambah=array();
  //print_r($prodsiar[3]);
  $a=0;
  for($i=0;$i<count($prodsiar[0]);$i++) {
     $name='';
    if(in_array($prodsiar[0][$i],$isifa)){
      continue;
    }else{
     // print_r($prodsiar[2]);echo '<br>';

      $name=@$prodsiar[0][$i];
      $code=@$prodsiar[2][$i];
      $npwp=@$prodsiar[3][$i];
    
      $custid=get_customer_id_kode($code);
      if($custid==''){
        $a++;
        //echo $a.'. '.$name.' :: '.$npwp;
        //echo '<br>';
          add_customer2($name, $code, null, $npwp);

         $selected_id = db_insert_id();
        /*
        if (isset($SysPrefs->auto_create_branch) && $SysPrefs->auto_create_branch == 1)
        {
          add_branch($selected_id, $name, $code,
                'n/a', 1, 1, 1, '',
                '', '', '','DEF', 'n/a', 0, 1, '', '');
                
          $selected_branch = db_insert_id();
          add_crm_person($code, $name, '', 'n/a', 
            '', '', '', '', '', '');

          $pers_id = db_insert_id();
          add_crm_contact('cust_branch', 'general', $selected_branch, $pers_id);

          add_crm_contact('customer', 'general', $selected_id, $pers_id);
        }
        */
        commit_transaction();
      }
      /*
      add_customer2($name, $code, null, null);

      $selected_id = db_insert_id();
      
      if (isset($SysPrefs->auto_create_branch) && $SysPrefs->auto_create_branch == 1)
      {
        add_branch($selected_id, $name, $code,
              'n/a', 1, 1, 1, '',
              '', '', '','DEF', 'n/a', 0, 1, '', '');
              
        $selected_branch = db_insert_id();
        add_crm_person($code, $name, '', 'n/a', 
          '', '', '', '', '', '');

        $pers_id = db_insert_id();
        add_crm_contact('cust_branch', 'general', $selected_branch, $pers_id);

        add_crm_contact('customer', 'general', $selected_id, $pers_id);
        $a++;
      }
      */
      
     
    }
  }
      if($a>0){
        echo '<div id="msgbox"><div class="">'.$a.' Data Customer from SIAR Added.</div></div>';
      }else{
        echo '<div id="msgbox"><div class="err_msg">No New Customer Data from SIAR.</div></div>';
      }
      $Ajax->activate("customer_tbl");
  //print_r($tambah);
}
if(@get_post("create")!=''){
  
echo '
<script type="text/javascript">
function createform(){
  document.getElementById("createtable").style.display = "block";
}
</script>
  <center><form method="post"><table id="createtable" class="tablestyle_inner">
  <tbody><tr><td colspan="2" class="tableheader">Basic Data</td></tr>
  <tr><td class="label">Costumer Name <font style="color:red;">*</font>:</td><td><input type="text" name="CustName" size="42" maxlength="40" value=""></td>
  </tr>
  <tr><td class="label">Customer Short Name <font style="color:red;">*</font>:</td><td><input type="text" name="cust_ref" size="30" maxlength="30" value=""></td>
</tr>
  <tr><td class="label">GSTNo:</td><td><input type="text" name="gst_no" size="42" maxlength="40" value=""></td>
  </tr>
  <tr><td class="label">Address:</td><td><textarea name="address" cols="35" rows="5"></textarea></td>
  </tr>
  <tr><td class="label"></td><td><input type="submit" name="submitcostumer" value="Save"></td>
  </tr>
  </tbody></table></center>';
}
start_table(TABLESTYLE);

$th = array("", _("Customer"), _("Short Name"), _("Address"), _("Tax ID"));

table_header($th);

$k = 0;
$name = $_GET["client_id"];
$result = get_customers_search(get_post("customer"));
while ($myrow = db_fetch_assoc($result)) {
	alt_table_row_color($k);
	$value = $myrow['debtor_no'];
	if ($mode != 0) {
		$text = $myrow['name'];
  		ahref_cell(_("Select"), 'javascript:void(0)', '', 'setComboItem(window.opener.document, "'.$name.'",  "'.$value.'", "'.$text.'")');
	}
	else {
  		ahref_cell(_("Select"), 'javascript:void(0)', '', 'selectComboItem(window.opener.document, "'.$name.'", "'.$value.'")');
	}
  	label_cell($myrow["name"]);
  	label_cell($myrow["debtor_ref"]);
  	label_cell($myrow["address"]);
  	label_cell($myrow["tax_id"]);
	end_row();
}

end_table(1);

div_end();

end_page(true);
