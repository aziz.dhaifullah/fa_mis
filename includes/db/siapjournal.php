<?php

$path_to_root="../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/ui/items_cart.inc");

function get_param($tipe='siap',$key=null,$code=null)
{
    global $SysPrefs;

    $sql = "SELECT keycode,value FROM ".TB_PREF."config_param ";

    if ($key != null)
        $sql .= " where keycode like '".$key."' ";
    else
        $sql .= " where keycode like '%".$tipe."' ";
    
    //echo $sql;
    $result=db_query($sql, "The transactions for could not be retrieved");
    $params=array();
    while ($myrow0 = db_fetch($result))
    {
        $params[]=$myrow0;
    }
    if(count($params)>1)
    return $params;
    else
    return $params[0]['value'];
}

function journal_siap_mis($date = null, $pfoliocode)
{
    /*$idcode = $pfoliocode;
    $sqlgetdimension = "SELECT kode FROM 0_companies WHERE id =".$pfoliocode;
    $result = db_query($sqlgetdimension);
    $data = db_fetch($result);
    $pfoliocode = $data['kode'];*/

    $comps = array();
    $sqlgetdimension = "select  kode
                        from    (select * from ".TB_PREF."companies
                                 order by parent, id) comps_sorted,
                                (select @pv := ".$pfoliocode.") initialisation
                        where   (CASE WHEN NOT EXISTS (SELECT id from ".TB_PREF."companies where parent = @pv) THEN
                                find_in_set(id, @pv) ELSE find_in_set(parent, @pv) END)
                        and     length(@pv := concat(@pv, ',', id))
                        group by parent";

    if($result=db_query($sqlgetdimension)){
        while($row=db_fetch($result)){
            //$id = $row['id'];
            //$label = $row['label'];
            /*$comps[]= array('id'=>$id, 'label'=>$label);*/
            array_push($comps, $row['kode']);
        }
    }
    //print_r($comps);exit;
    //$a = array('asiap');
    $pfoliocodeimplode = implode("','",$comps);


    //echo $pfoliocodeimplode;
    $serverName=get_param(null,'db_host_siap');
    $port=get_param(null,'db_port_siap');
    $username=get_param(null,'db_user_siap');
    $password=get_param(null,'db_pass_siap');
    $db=get_param(null,'db_name_siap');  
    $db_connections_siap = array( "UID"=>$username, "PWD"=>$password,"Database"=>$db);
    $conn = sqlsrv_connect( $serverName, $db_connections_siap);
    if( $conn === false )  
    {  
         echo "Could not connect.\n";  
         //die( print_r( sqlsrv_errors(), true));  
    }
    
    /*$sql = "
        SELECT PF.pfoliocode,COA.ExternalCode,Amount,IsDebit,convert(varchar, CashJournalDate, 23) as CashJournalDate,TDealID FROM dbo.TCashJournal
        INNER JOIN  dbo.TCOAExternal AS COA ON COA.TCOAID = TCashJournal.TCOAID
        INNER JOIN  dbo.TPFolio AS PF ON PF.TPFolioID = TCashJournal.TPFolioID
        WHERE IsReverse IS  NULL 
        AND PF.pfoliocode = '".$kodesiap."'
        AND CashJournalDate = '".$date."'
        GROUP BY TDealID, PF.pfoliocode, COA.ExternalCode, Amount, IsDebit, CashJournalDate
    ";*/
    $sql = "SELECT 
            DEALID          = DEAL.TDEALID,
            COA             = COA.TCOAID,
            TJournalScenario= journal.TJournalScenarioID,
            ScenarioName    = FLOW.CFlowTypeName, -- Penambahan Nama Cash flow type 28 Feb
            pfoliocode      = CASE WHEN PF.pfoliocode = 'MISSAK' THEN MAP.MapValue ELSE PF.PFolioCode END,
            STATUS          = deal.TstatusID,
            REMARKS         = CASE WHEN deal.Remarks = 'CUTOFF' AND SCENARION.TJournalScenarioID!=80 THEN 'A' ELSE 'B' END, /*A = CUTOFF TIDAK PERLU DITAMPILKAN */
            ExternalCode    = EXCOA.ExternalCode,
            SysModifiedDate = deal.SysModifiedDate,
            Amount          = Amount,
            IsDebit         = IsDebit,
            CashJournalDate =convert(varchar, JOURNAL.CashJournalDate, 23)

            FROM dbo.TCashJournal AS JOURNAL

            INNER JOIN  dbo. TCashFlowType AS FLOW ON FLOW.TCashFlowTypeID = JOURNAL.TCashFlowTypeID
            INNER JOIN  dbo. TJournalScenario AS SCENARION ON SCENARION.TJournalScenarioID = JOURNAL.TJournalScenarioID
            INNER JOIN  dbo. TDeal AS DEAL ON   JOURNAL.TDealID = DEAL.TDealID
            INNER JOIN  dbo. TCOA AS COA ON COA.TCOAID = JOURNAL.TCOAID
            INNER JOIN   dbo.TCOAExternal AS EXCOA ON EXCOA.TCOAID = COA.TCOAID
            INNER JOIN  dbo.TPFolio AS PF ON PF.TPFolioID = JOURNAL.TPFolioID
            LEFT JOIN   dbo.TPFolioMapNew AS MAP ON MAP.TPFolioID = PF.TPFolioID

            WHERE JOURNAL.IsReverse IS NULL
            AND PF.pfoliocode IN ('".$pfoliocodeimplode."')
            ORDER BY JOURNAL.TPFolioID  DESC";
    //echo "<br><br>";
    //exit;   
   /* SELECT PF.pfoliocode,COA.ExternalCode,Amount,IsDebit,convert(varchar, CashJournalDate, 23) as CashJournalDate,TCashJournal.TDealID,dl.Remarks FROM dbo.TCashJournal
        INNER JOIN  dbo.TCOAExternal AS COA ON COA.TCOAID = TCashJournal.TCOAID
        INNER JOIN  dbo.TPFolio AS PF ON PF.TPFolioID = TCashJournal.TPFolioID
        INNER JOIN  dbo.TDeal as dl ON dl.TDealID  = TCashJournal.TDealID
        WHERE IsReverse IS NULL
        AND dl.Remarks != 'CUTOFF'
        AND PF.pfoliocode IN ('".$pfoliocodeimplode."')
        GROUP BY TCashJournal.TDealID, PF.pfoliocode, COA.ExternalCode, Amount, IsDebit, CashJournalDate, dl.Remarks*/
    $data = array();
    $stmt = sqlsrv_query( $conn, $sql);
    if( $stmt === false )  
    {  
         echo "Error in executing query.</br>";  
         //die( print_r( sqlsrv_errors(), true));  
    }else{ 
        while($row = sqlsrv_fetch_array($stmt)){
            //print_r($row['pfoliocode']);
            /*$datacompcode['code'] = $row['pfoliocode'];
            $datacompcode['name'] = $row['pfoliofname'];*/
            if ($row['REMARKS'] != 'A') {  
            //if($journalscenario != 85){
                $journalscenario = $row['TJournalScenario'];
                $scenarioname = $row['ScenarioName'];
                $SysModifiedDate = $row['SysModifiedDate'];
                $tanggal = $row['CashJournalDate'];
                $coa = $row['ExternalCode'];
                $coa = str_replace('.', '', $coa);
                $dimension = $row['pfoliocode'];
                $dealid = $row['DEALID']."-".$scenarioname;
                $amount = $row['Amount'];
                if ($row['IsDebit'] == 0) {
                    $amount = $amount * (-1);
                }
                $data[$tanggal][$dealid][$coa] = array('amount'=>$amount,'dimension'=>$dimension,'modifiedDate'=>$SysModifiedDate);
            //}

            }
            //$data[] = array('tanggal'=>$tanggal, 'coa'=>$coa, 'dimension'=>$dimension, 'ref'=>$dealid, 'amount'=>$amount); 
            /*$datacomcode['pfoliocode'] = $row['pfoliocode'];
            $datacomcode['pfoliofname'] = $row['pfoliofname'];*/
        }
        sqlsrv_free_stmt( $stmt);  
    }
    sqlsrv_close($conn);
    /*print_r($data);
    exit;*/

    /*return $data;*/
    // print_r($_SESSION["wa_current_user"]->com_id);
    /*echo "a";exit;*/

    $date = array_keys($data);
    $countdate = count($date);
    //$memoupdate = array();
    for ($d=0; $d < $countdate; $d++) { 
        $transref = array_keys($data[$date[$d]]);
        $counttrans = count($transref);
        $datestring = explode('-', $date[$d]);
        $datestring = $datestring[2].'/'.$datestring[1].'/'.$datestring[0];
        //echo '<br>'.$datestring.'<br>';
        for ($i=0; $i < $counttrans; $i++) { 
            $cart = new items_cart(ST_JOURNAL);
            $memo = 'SIAP/'.$transref[$i].'/'.$date[$d];
            $reference = 'SIAP-'.$transref[$i];
            $memoupdate[] = $memo;
            
            $sqlchecktrans = "SELECT COUNT(memo_) as cntmemo,type,type_no FROM ".TB_PREF."gl_trans WHERE memo_ = '".$memo."' GROUP BY type_no";
            // /$sqlcheckreftrans = "SELECT COUNT(id) as cntid, type ,id FROM ".TB_PREF."gl_trans WHERE reference = '".$reference."' AND  GROUP BY type_no";
            //$sqlchecktrans = "SELECT COUNT(memo_) as cntmemo FROM ".TB_PREF."gl_trans WHERE memo_ = 'a' ";
            $result = db_query($sqlchecktrans);
            $fetch = db_fetch($result);
            //echo $fetch['cntmemo'];exit;
            //echo $memo.'<br>';
            if (@$fetch['cntmemo'] == 0) {
                

                $cart->memo_ = $memo;
                $cart->reference = $reference;
                $cart->tran_date = $cart->doc_date = $cart->event_date = $datestring;
                $coa = array_keys($data[$date[$d]][$transref[$i]]);
                $countcoa = count($coa);
                $coaimplode = implode("','",$coa);
                
                //echo 'SIAP-'.$transref[$i].'<br>';
                for ($a=0; $a < $countcoa; $a++) { 
                    //echo $coa[$a].'__';
                    //echo $data[$date[$d]][$transref[$i]][$coa[$a]]['dimension'].'<br>';
                        /*echo $datestring."_";
                        echo sql2date($datestring)."_";
                        echo date('Y-m-d',strtotime("-1 days",strtotime(sql2date($datestring)))).'__';*/
                    /*if ($coa[$a] == "1140201" OR $coa[$a] == "42201" ) {
                        echo $sqlgetcoa2siap = "SELECT amount FROM 0_gl_trans WHERE account='".$coa[$a]."' AND tran_date='".date('Y-m-d',strtotime("-1 days",strtotime(sql2date($datestring))))."'";
                        $resultcoa2 = db_query($sqlgetcoa2siap);
                        while ($amountcoabefore = db_fetch($resultcoa2)) {
                            print_r($amountcoabefore);
                            echo $amountafter = $data[$date[$d]][$transref[$i]][$coa[$a]]['amount'] - $amountcoabefore['amount'];
                        }
                        $cart->add_gl_item($coa[$a], $data[$date[$d]][$transref[$i]][$coa[$a]]['dimension'],'', $amountafter, $memo, null, null, $datestring); 
                    }else{*/
                        $cart->add_gl_item($coa[$a], $data[$date[$d]][$transref[$i]][$coa[$a]]['dimension'],'', $data[$date[$d]][$transref[$i]][$coa[$a]]['amount'], $memo, null, null, $datestring);
                    /*}*/
                    /*$cart->add_gl_item(get_post('res_act'), get_post('dimension_id'),
                    get_post('dimension2_id'), $am0, $cart->reference);*/
                }
                
                //print_r(expression)
                write_journal_entries_siap($cart);
            }else{
                $coa = array_keys($data[$date[$d]][$transref[$i]]);
                $countcoa = count($coa);
                $type[] = $fetch['type'];
                $type_no[] = $fetch['type_no'];
                //echo 'SIAP-'.$transref[$i].'<br>';
                $coaimplode = implode("','",$coa);
                $sqldeletetrans = "DELETE FROM ".TB_PREF."gl_trans WHERE memo_ = '".$memo."' AND account NOT IN ('".$coaimplode."')";
                db_query($sqldeletetrans);
                
                for ($a=0; $a < $countcoa; $a++) { 
                    //echo $coa[$a].'__';
                    //echo $data[$date[$d]][$transref[$i]][$coa[$a]]['dimension'].'<br>';
                    $sqlupdatetrans = "UPDATE ".TB_PREF."gl_trans SET amount = ".$data[$date[$d]][$transref[$i]][$coa[$a]]['amount']." WHERE memo_ = '".$memo."' AND account = '".$coa[$a]."' AND amount != ".$data[$date[$d]][$transref[$i]][$coa[$a]]['amount'];
                    db_query($sqlupdatetrans);

                    $sqlcheckcoa = "SELECT COUNT(account) as ac FROM ".TB_PREF."gl_trans WHERE memo_='".$memo."' AND dimension_id = '". $data[$date[$d]][$transref[$i]][$coa[$a]]['dimension']."' AND account = '".$coa[$a]."'";
                    $resultcoa = db_query($sqlcheckcoa);
                    $count = db_fetch($resultcoa);

                    if ($count['ac'] == 0) {
                        $sqlinserttrans = "INSERT INTO ".TB_PREF."gl_trans(counter,type,type_no,tran_date,account,memo_,amount,dimension_id,dimension2_id,person_type_id,person_id,posting,jasa,gross,rate)
                        VALUES(NULL,0,".$fetch['type_no'].",'".sql2date($datestring)."','".$coa[$a]."','".$memo."',".$data[$date[$d]][$transref[$i]][$coa[$a]]['amount'].",'".$data[$date[$d]][$transref[$i]][$coa[$a]]['dimension']."',0,NULL,NULL,0,0,NULL,NULL)";
                        db_query($sqlinserttrans,'cant insert');
                    }
                    /*echo $fetch['type_no']."-";
                    if(db_query($sqlinserttrans,'cant insert')){
                        echo "BERHASIL-";
                    }else{
                        echo "GAGAL-";
                    }*/

                    //$sqlcekdatatrans = "SELECT COUNT(memo_) as cntmemo FROM ".TB_PREF."gl_trans WHERE memo_ = '".$memo."'";                    

                    //$cart->add_gl_item($coa[$a], $data[$date[$d]][$transref[$i]][$coa[$a]]['dimension'],'', $data[$date[$d]][$transref[$i]][$coa[$a]]['amount'], $memo, null, null, $datestring);
                    /*$cart->add_gl_item(get_post('res_act'), get_post('dimension_id'),
                    get_post('dimension2_id'), $am0, $cart->reference);*/
                }
                

            }
            $cart->clear_items();
            /*$sql = 'UPDATE 0_gl_trans SET posting = 1 WHERE memo_ LIKE'.$memo;
            db_query($sql);*/
        }
    }
    //exit;
    $countupdate = count(@$memoupdate);
    //print_r($memoupdate);exit;
    if($countupdate > 0){
        $memoimplode = implode("','",$memoupdate);
        $typeimplode = implode("','",$type);
        $typenoimplode = implode("','",$type_no);
        $sqldeletetransbymemo = "DELETE FROM ".TB_PREF."gl_trans WHERE memo_ NOT IN ('".$memoimplode."') AND  memo_ LIKE '%SIAP%' ";
        db_query($sqldeletetransbymemo);
        $sqldeletetransbymemo = "DELETE FROM ".TB_PREF."journal WHERE type NOT IN ('".$typeimplode."') AND trans_no NOT IN ('".$typenoimplode."') AND reference LIKE '%SIAP%' ";
        db_query($sqldeletetransbymemo);
        for ($i=0; $i < $countupdate ; $i++) { 
            $sql = 'UPDATE 0_gl_trans SET posting = 1 WHERE memo_ ="'.$memoupdate[$i].'"';
            db_query($sql);
        }
        /*$sql = "SELECT amount - (SELECT amount WHERE ) WHERE memo_='".$memoupdate[$i]."' AND account='42201'";*/
    }

    $sqlgetcoa4 = "SELECT * FROM 0_gl_trans WHERE account IN ('42201','1140201') AND memo_ LIKE '%SIAP%' AND dimension_id IN ('".$pfoliocodeimplode."')";
    $resultcoa4 = db_query($sqlgetcoa4);
    while ($rowresultcoa4 = db_fetch($resultcoa4)) {
        //print_r($rowresultcoa4);
       // echo date('Y-m-d',strtotime("-1 days",strtotime($rowresultcoa4['tran_date'])));
        $memoexplode = explode("/",$rowresultcoa4['memo_']);
        $memoexplode = $memoexplode[0].'/'.$memoexplode[1].'/'.date('Y-m-d',strtotime("-1 days",strtotime($rowresultcoa4['tran_date'])));
        $sqlgetcoa4before = "SELECT amount as amountbefore FROM 0_gl_trans WHERE account='".$rowresultcoa4['account']."' AND tran_date='".date('Y-m-d',strtotime("-1 days",strtotime($rowresultcoa4['tran_date'])))."' AND memo_ ='".$memoexplode."'";
        $resultcoa4before = db_query($sqlgetcoa4before);
        $amountbefore = db_fetch($resultcoa4before);
        if ($amountbefore['amountbefore'] == NULL) {
            $amountbefore = 0;
        }else{
            $amountbefore = $amountbefore['amountbefore'];
        }
        $amountafter = $rowresultcoa4['amount'];
        $amountafter."-".$amountbefore."__"; 
        $amounttotal = $amountafter - $amountbefore;
        if ($amounttotal == 0) {
            $amounttotal = 0;
        }
        //echo $amounttotal."__"; 
        $sqlupdatecoa2 = "UPDATE 0_gl_trans SET amount=".$amounttotal." WHERE memo_ ='".$rowresultcoa4['memo_']."' AND account = '".$rowresultcoa4['account']."'";
        db_query($sqlupdatecoa2);
    }
}

//echo journal_siap_mis(null,11);